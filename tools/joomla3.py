#!/usr/bin/env python3
"""
    Este archivo hay que ejecutarlo desde la carpeta superior
    python3 ./tools/joomla.py
"""

import mysql.connector
import json
import yaml
import sys
from os import path, makedirs
from shutil import copy
from markdownify import markdownify as md
from bs4 import BeautifulSoup
from math import ceil
from time import sleep


class Joomla3():
    def __init__(self):

        # Configuración de la conexión de la BBDD
        self.conn = mysql.connector.connect(
            host="localhost",
            user="jeljekyll",
            password="bqtWOk6KUxG]LxyE",
            database="jeljekyll"
        )

        self.cursor = self.conn.cursor(dictionary=True)

        # El prefijo de las tablas de Joomla
        self.prefix = 'jugenlin_'

        # La ruta a donde están los archivos de Joomla
        self.joomla_root = '/home/son_link/public_html/jel/'

        if len(sys.argv) > 1:
            if sys.argv[1] == '--authors':
                self.getAuthors()
            elif sys.argv[1] == '--content':
                self.getContent()
        else:
            self.getContent()

    def getContent(self):
        # Primero vamos a obtener la cantidad de entradas y cuantas paginas son
        sql = f'''
            SELECT COUNT(*) AS total
            FROM {self.prefix}content AS cn
            WHERE (cn.state = 1 OR cn.state = 2) AND cn.catid != 2
        '''

        self.cursor.execute(sql)
        result = self.cursor.fetchall()[0]
        total_pages = ceil(result['total'] / 20)

        for page in range(total_pages):
            print(f"/rPágina {page} de {total_pages}")
            # Vamos a obtener las entradas publicadas y que no sean páginas
            sql = f"""
                SELECT cn.title, cn.alias, cn.introtext,
                CONCAT(cn.introtext,cn.fulltext) AS content,
                cn.created, cn.id, ct.title AS category, u.name AS author,
                cn.images AS images, cn.publish_up
                FROM {self.prefix}content AS cn
                JOIN {self.prefix}categories AS ct ON cn.catid = ct.id
                JOIN {self.prefix}users AS u ON cn.created_by = u.id
                WHERE (cn.state = 1 OR cn.state = 2)  AND cn.catid != 2
                ORDER BY cn.id DESC
                LIMIT {page * 20}, 20
            """

            self.cursor.execute(sql)
            posts = self.cursor.fetchall()

            for post in posts:
                date = post['created'] if post['created'] else post['publish_up']
                # Rellenamos varios de la información de cada
                data = {
                    "layout":       "post",
                    "title":        post['title'],
                    "joomla_id":    post['id'],
                    "joomla_url":   post['alias'],
                    "date":         date,
                    "author":       post['author'],
                    "excerpt":      post['introtext'],
                    "category":     post['category'],
                }

                # El nombre del fichero
                filedate = data['date'].strftime("%Y-%m-%d")
                filename = f"{filedate}-{post['alias']}.md"

                # Parseamos el JSON que contiene las imágenes, entre otras
                # la imagen destacada, que es la que nos interesa
                images = json.loads(post['images'])

                if images['image_intro']:
                    imagename, file_extension = path.splitext(images['image_intro'])
                    # data['image'] = '/' + images['image_intro']
                    data['image'] = '/' + imagename + '.webp'

                    self.copyImage(images['image_intro'])

                # Ahora vamos a obtener las etiquetas de la entrada
                sql = f"""
                    SELECT t.path, t.title
                    FROM {self.prefix}contentitem_tag_map AS ctm
                    JOIN {self.prefix}tags AS t ON t.id = ctm.tag_id
                    WHERE ctm.content_item_id = {post['id']}
                """
                self.cursor.execute(sql)
                result = self.cursor.fetchall()

                if result and len(result) > 0:
                    data['tags'] = []
                    for tag in result:
                        data['tags'].append(tag['path'])

                # Ahora vamos a realizar varias operaciones sobre el HTML
                # de la entrada

                soup = BeautifulSoup(post['content'], 'html.parser')
                # Primero vamos a eliminar todo párrafo vació
                '''for p in soup.find_all('p'):
                    if not p.contents:
                        p.decompose()'''

                # Ahora vamos a buscar todas las imágenes para poder
                # copiarlas a la carpeta correspondiente
                for image in soup.find_all('img'):
                    src = image['src']
                    self.copyImage(src)
                    # Cambiamos la extensión a webp ya que luego
                    # se convertirán a dicho formato
                    if not src.startswith('/'):
                        imagename, file_extension = path.splitext(src)
                        # data['image'] = '/' + filename + '.webp'
                        # image['src'] = '/' + src
                        image['src'] = '/' + imagename + '.webp'

                # Finalmente guardamos el fichero de la entrada
                with open(f'_posts/{filename}', 'w') as fp:
                    fp.write('---\n')
                    fp.write(yaml.dump(data))
                    fp.write('---\n')
                    fp.write(md(str(soup)))
                    fp.close()
                    
            sleep(10)

    def getAuthors(self):
        sql = f"""
            SELECT u.name, u.username, jup.profile_value
            FROM {self.prefix}users u
            INNER JOIN {self.prefix}content cn ON cn.created_by = u.id
            INNER JOIN {self.prefix}user_profiles jup ON jup.user_id = u.id
            WHERE jup.profile_key = 'profile.aboutme'
            AND (cn.state = 1 OR cn.state = 2)  AND cn.catid != 2
            GROUP BY u.id
        """

        self.cursor.execute(sql)
        result = self.cursor.fetchall()

        authors = {}
        for author in result:
            authors[author['username']] = {
                'name':         author['name'],
                'description':  author['profile_value']
            }

        with open('_data/authors.yml', 'w') as fp:
            fp.write(yaml.dump(authors))
            fp.close()

    def copyImage(self, image):
        """ Copia la imagen de Joomla a la misma ruta en el proyecto """
        # Primeros comprobamos que no sea una URL
        if image.startswith('http'):
            return

        # Obtenemos la ruta base de la imagen y si no existe la creamos
        image_path = path.dirname(image)
        if not path.exists(image_path):
            makedirs(image_path)

        # Copiamos la imagen si no se encuentra ya
        if not path.exists(image) and path.exists(f"{self.joomla_root}/{image}"):
            copy(
                f"{self.joomla_root}/{image}",
                image_path
            )
        elif not path.exists(f"{self.joomla_root}/{image}"):
            print(f"Falta la imagen {self.joomla_root}/{image}")


Joomla3()
