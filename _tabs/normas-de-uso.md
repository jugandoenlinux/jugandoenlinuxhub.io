---
layout: page
title: 'Normativa de jugandoenlinux.com'
icon: fas fa-book-journal-whills
order: 4
---

## En jugandoenlinux.com moderamos los comentarios.

Si quieres publicar comentarios en jugandoenlinux.com o sus diferentes canales por favor sigue estas normas:

- **No publiques enlaces a contenido pirata**. No toleraremos ningún tipo de contenido o comentario incitando a la piratería. Si de verdad amas este mundillo del videojuego en Linux por favor compra software legal y apoya a los desarrolladores que trabajan para los pocos que somos. Los comentarios acerca de la postura personal de cada uno respecto a la piratería si están permitidos.

- **No publiques contenido porno o para adultos**. No toleraremos ningún tipo de contenido que pueda herir sensibilidades. Nos visitan personas de todas las edades y jugandoenlinux.com debe ser apto para todas ellas. Si además es contenido pedófilo denunciaremos al responsable a las autoridades facilitando todos los datos a nuestro alcance en la denuncia.

- **Publica tus comentarios desde el respeto a los demás**. No toleraremos faltas de respeto, amenazas o insultos de ningún tipo. No ataques al creador del artículo o a otras personas. Si quieres defender tu postura hazlo de forma respetuosa. De lo contrario de cara a los demás perderás la razón, la tengas o no.

- **No crees comentarios para desacreditar los artículos de un autor**. Si crees que algo está mal, háznoslo saber mediante los cauces adecuados, enviandonos un mensaje a través de alguno de nuestros canales como Matrix, Mastodon, Telegram o Discord.

- **No toleraremos guerras de distribuciones, software libre o propietario o guerras entre sistemas**. Cada cual hace su elección y tendrá sus motivos personales para hacerlo. Cualquier comentario que trate de ofender, criticar o coaccionar a cualquier usuario por su elección o convicción será moderado y su autor sancionado.

- **GNU-Linux es GNU-Linux**. Cualquier guerra en cuanto a nomenclaturas no será tolerada.

- Hablamos de videojuegos, GNU-Linux y también de otras hierbas, pero **está expresamente prohibido tratar temas de índole político e ideológico** incluyendo también como tales fotos, gif's, memes y cualquier representación gráfica con intención política o ideológica con el fin de no herir sensibilidades. Cada cual tiene su opinión y creencias políticas e ideológicas, y ese no es un tema que debamos tratar aquí.

- **Los enlaces a noticias de otras webs que ya hayan sido tratadas en JugandoEnLinux.com** serán automáticamente eliminados. La práctica repetitiva de compartir enlaces y no participar de manera activa será motivo de expulsión.

Incumplir cualquiera de estas normas conllevará la moderación y eliminación de comentarios, advertencia al responsable de ellos y en casos graves el baneo incluso sin previo aviso.
