---
# the default layout is 'page'
icon: fas fa-info-circle
order: 4
---
Jugando en Linux somos un pequeño grupo de usuarios españoles aficionados a jugar en sistemas Linux, que nos conocimos a través de otras webs extranjeras referentes a los juegos en estos sistemas. Ante el evidente crecimiento e importancia que Linux está experimentando en el ámbito de la industria de los videojuegos, anhelábamos tener un espacio donde poder compartir la cada vez más ingente información específica relativa a los juegos en Linux, poder compartir nuestra afición con otros usuarios con intereses comunes y poder tener nuestros propias opciones, opiniones y comentarios en español.

Al principio estuvimos buscando alternativas a las webs en otros idiomas que visitábamos y que estuvieran en español, pero lamentablemente no las encontramos así que nos decidimos por lanzar una web nosotros mismos. Probablemente, si hubiera habido una web parecida a esta ahora mismo no estaríamos aquí.

En Jugando en Linux somos usuarios. Aunque no somos profesionales (no nos ganamos la vida con esto) si que tenemos cierta experiencia con la moderación de comunidades, publicación de información en blogs y mantenimiento de webs. Solemos tener un perfil técnico medio - elevado y nos gusta trastear con ordenadores, sistemas y aparatos varios. Y por supuesto jugamos en Linux. 

Pretendemos crear una Comunidad donde poder compartir nuestra afición, ofrecer nuestros conocimientos, información y ayuda a quien lo necesite y construir una comunidad sana que nos ayude también a crecer y disfrutar aún mas de este excitante mundo de los videojuegos en Linux. Si quieres colaborar con nosotros puedes hacerlo posteando información en nuestro foro, o mandándonos avisos y escribiendo tus propios artículos para que te los publiquemos. Tienes todas las opciones en el apartado "colabora". Si tienes inquietud y quieres entrar a formar parte del equipo de Jugando en Linux contacta con nosotros para evaluar tu perfil.

Bienvenido a tu web de videojuegos en Linux, en español.

{% assign authors_keys = "administrador,CansecoGPC,chema,Laegnur,leillo1975,lordgault,Odin,P_Vader,Pato,Serjor,Son Link,yodefuensa" | split: "," %}
{% for key in authors_keys %}
{% assign author = site.data.authors[key] %}
<div class="card author mb-3">
	<div class="card-body row flex flex-md-row">
		<div class="author-avatar col-2">
			{% if author.avatar %}
				<img src="{{ '/images/social/' | append: author.avatar | absolute_url }}" />
			{% endif %}
		</div>
		
		<div class="col-10">
			<h5 class="card-title">{{ author.name }}</h5>
			{% if  author.description %}
				<p class="card-text">{{ author.description | newline_to_br }}</p>
			{% endif %}
		</div>
	</div>
</div>
{% endfor %}