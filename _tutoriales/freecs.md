---
title: FreeCS
description: En este tutorial enseño como instalar y jugar FreeCS
layout: page
---
Hace un tiempo en el canal de Telegram, comente sobre FreeCS, un re-implantación libre del Counter Strike 1.5, ya que esta versión fue la ultima gratuita. Para ello usa un motor basado en Quake, FTEQW. Antes de nada un par de cosas:

* Al usar un motor diferente (GoldSrc) no es posible usar los mods, como el be añadir bots
* Ademas también por el punto anterior no es posible conectarse a servidores del original, y a día de hoy no hay servidores disponibles, por lo que tendrás que crear uno para poder jugar con otras personas interesadas (o alguna de esas personas que monte el servidor)
* En algunos mapas pueden faltar texturas, siendo para ello necesario contar con una copia legal de Half Life y copiar unos archivos. También explicare esta parte.
* Aun esta en desarrollo, por lo que te encontraras con problemas


Para empezar crea una carpeta llamada freecs, en la cual vamos a copiar los archivos.

Ahora vamos a necesitar los archivos de FreeCS. Podemos optar por la versión estable o bajar la ultima versión desde su repositorio, yo prefiero la segunda por que así pudo ir probando las novedades.
Descargar FreeCS

El fichero que tenemos que bajar es freecs-bin-2019-03-28.tar.gz (a la hora de escribir este post, puede haber cambiado). Una vez bajado descomprimimos en la carpeta que hemos creado:
Archivo Adjunto:


Si quieres optar por bajar la ultima versión vía GIT abre una terminal y ejecuta el siguiente comando:

```sh
git clone https://git.code.sf.net/p/freecs-1-5/code freecs-1-5-code freecs
```

Ahora vamos a bajar el motor desde su página de descargas seleccionando el que pone FTE Client, lo descargamos a la carpeta y le damos permisos de ejecución. En mi caso he bajado la versión Linux x64.

Ahora vamos a necesitar bajar desde el enlace FreeCS otro archivo, pak0.pk3. Una vez descargado tenemos que extraer su contenido en la carpeta cstrike (es un archivo zip con otra extensión). Una vez hecho debemos de renombrar algunos ficheros a minúsculas. Dejo adjunto un archivo que se encarga de ello. Solo descargarlo a la carpeta, cambia la extensión a .sh (ya que el foro no me permite subirlo con dicha extensión), dale permisos de ejecución y ejecútalo desde una terminal

Una vez hecho todo vamos a probar el juego. Para ello abrimos una terminal y ejecutamos:

`./fteqw64`

ó el que corresponda al que hayas bajado

Si vemos las letras pequeñas, o más grandes, dentro de la carpeta cstrike creamos un archivo llamado autoexec.cfg y ponemos lo siguiente:

```cfg
vid_conautoscale "2"            // menu/game font size
con_textsize "12"               // console font size
```

Y ya solo queda entrar y configurar el juego a tu gusto.

Ya para terminar, para el tema de las texturas faltantes tenemos que copiar la carpeta valve desde nuestra copia de Half Life (dentro de la carpeta de Steam en steamapps/common/Half-Life/). Esto es opcional, si bien en algunos mapas se verán cosas raras.

Dejo una captura del juego:

Archivo Adjunto:


Y esto es todo. Me faltaría como usar el servidor, pero eso más adelante, cuando pueda hacer pruebas con otras personas. 
