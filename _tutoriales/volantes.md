---
title: Volantes en Linux
description: Proyectos de Controladores para Volantes/Steering Wheel Driver Projects  
layout: page
---

Actualmente se puede habilitar el soporte de determinados volantes de conducción para juegos en Linux / Support for selected driving wheels can now be enabled for games on Linux:

## -Logitech:
* WingMan Formula GP (no FFB)> [new-lg4ff](https://github.com/berarma/new-lg4ff/)
* WingMan Formula Force GP > [new-lg4ff](https://github.com/berarma/new-lg4ff/)
* Driving Force > [new-lg4ff](https://github.com/berarma/new-lg4ff/)
* MOMO Force Feedback Racing Wheel > [new-lg4ff](https://github.com/berarma/new-lg4ff/)
* Driving Force Pro > [new-lg4ff](https://github.com/berarma/new-lg4ff/)
* G25 Racing Wheel > [new-lg4ff](https://github.com/berarma/new-lg4ff/)
* Driving Force GT (tested) > [new-lg4ff](https://github.com/berarma/new-lg4ff/)
* G27 Racing Wheel (tested) > [new-lg4ff](https://github.com/berarma/new-lg4ff/)
* G29 Driving Force (tested) > [new-lg4ff](https://github.com/berarma/new-lg4ff/)
* MOMO Racing > [new-lg4ff](https://github.com/berarma/new-lg4ff/)
* Speed Force Wireless Wheel for Wii > [new-lg4ff](https://github.com/berarma/new-lg4ff/)
* G923-PS Driving Force> [new-lg4ff](https://github.com/berarma/new-lg4ff/) ( [WIP](https://github.com/berarma/new-lg4ff/pull/50) )
* G920 Driving Force **(in-kernel driver)**
* G923-XBOX Driving Force **(in-kernel driver)**
* PRO Racing Wheel (Direct Drive, **WIP** ) > [hid-logitech-hidpp](https://github.com/JacKeTUs/hid-logitech-hidpp) . Also the wheel has a G923 compatibility mode , perhaps works with new-lg4ff (PS) or in-kernel driver (XBOX) 

## -Thrustmaster:
* T150 > [T150_driver](https://github.com/scarburato/t150_driver)
* TMX Racing Wheel > [T150_driver](https://github.com/scarburato/t150_driver)  y/and  [TMX-driver WIP (no FFB at the moment)](https://github.com/emtek995/TMX-driver)
* T300RS > [Hid-tmff2](https://github.com/Kimplul/hid-tmff2)
* T248 > [Hid-tmff2](https://github.com/Kimplul/hid-tmff2)
* T500RS > [Hid-tmff2 WIP](https://github.com/Kimplul/hid-tmff2)
* Another Thrustmaster driver (abandoned) > [TMDRV (no FFB)](https://gitlab.com/her0/tmdrv)

## -FANATEC:
* FANATEC CSL Elite Wheel Base > [hid-fanatecff](https://github.com/gotzl/hid-fanatecff)
* FANATEC CSL Elite Wheel Base PS4 > [hid-fanatecff](https://github.com/gotzl/hid-fanatecff)
* FANATEC CSL Elite Pedals > [hid-fanatecff](https://github.com/gotzl/hid-fanatecff)
* FANATEC ClubSport Wheel Base V2 (experimental) > [hid-fanatecff](https://github.com/gotzl/hid-fanatecff)
* FANATEC ClubSport Wheel Base V2.5 (experimental) > [hid-fanatecff](https://github.com/gotzl/hid-fanatecff)
* Podium Wheel Base DD1 (experimental) > [hid-fanatecff](https://github.com/gotzl/hid-fanatecff)
* Podium Wheel Base DD2 (experimental) > [hid-fanatecff](https://github.com/gotzl/hid-fanatecff)
* CSL Elite Wheel Base (experimental) > [hid-fanatecff](https://github.com/gotzl/hid-fanatecff)
* CSL Wheel Base DD (experimental) > [hid-fanatecff](https://github.com/gotzl/hid-fanatecff)

## -MOZA 
* MOZA R3 > [moza-ff](https://github.com/JacKeTUs/moza-ff) (WIP)
* MOZA R5 > [moza-ff](https://github.com/JacKeTUs/moza-ff) (WIP)
* MOZA R9 > [moza-ff](https://github.com/JacKeTUs/moza-ff) (WIP)
* MOZA R12 > [moza-ff](https://github.com/JacKeTUs/moza-ff) (WIP)
* MOZA R16 > [moza-ff](https://github.com/JacKeTUs/moza-ff) (WIP)
* MOZA R21 > [moza-ff](https://github.com/JacKeTUs/moza-ff) (WIP)


## -Otros proyectos de volantes Direct Drive/ Another Direct Drive wheel projects:

* **Cammus C5** > [Cammus-ff](https://github.com/spikerguy/cammus-ff)
* **Simagic** - needs driver and old fw, setup works with AlphaManager through wine (not SimPro) [simagic-ff](https://github.com/JacKeTUs/simagic-ff)
* **Simucube** - [native pidff driver](https://granitedevices.com/wiki/Using_Simucube_wheel_base_in_Linux) , [Simucube Firmware Releases](https://granitedevices.com/wiki/SimuCUBE_firmware_releases)
* **Asetek** - unknown


## -Utilidades para configurar volantes/Utilities for configuring steering wheels  :  
* [Oversteer](https://github.com/berarma/oversteer)
* [PyLinuxWheel](https://gitlab.com/OdinTdh/pyLinuxWheel)

