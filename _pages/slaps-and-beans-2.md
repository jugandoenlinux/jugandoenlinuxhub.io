---
layout: page
title: almente Slaps and Beans 2 esta disponible, ¡y con versión nativa!
permalink: index.php/homepage/accion/1545-slaps-and-beans-2-disponible-con-version-nativa
---

<script>
	window.location = '{% link _posts/2023-09-22-finalmente-slaps-and-beans-2-esta-disponible-y-con-version-nativa.md %}'
</script>
<noscript>
	Este articulo se movió a {% link _posts/2023-09-22-finalmente-slaps-and-beans-2-esta-disponible-y-con-version-nativa.md %}
</noscript>