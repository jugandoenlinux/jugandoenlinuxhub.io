---
author: Pato
category: "Acci\xF3n"
date: 2017-06-14 06:45:17
excerpt: "<p>Adem\xE1s del lanzamiento oficial, tambi\xE9n se ha anunciado la expansi\xF3\
  n 'Ragnarok'</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1ba6f013b03d3119f460bad3db0fbe5d.webp
joomla_id: 375
joomla_url: ark-survival-evolved-saldra-del-acceso-anticipado-y-se-lanzara-oficialmente-el-proximo-8-de-agosto
layout: post
tags:
- accion
- aventura
- supervivencia
- construccion
- dinosaurios
title: "ARK: Survival Evolved saldr\xE1 del acceso anticipado y se lanzar\xE1 oficialmente\
  \ el pr\xF3ximo 8 de Agosto"
---
Además del lanzamiento oficial, también se ha anunciado la expansión 'Ragnarok'

Después de un desarrollo de algo mas de dos años, por fin **ARK: Survival Evolved** [[web oficial](http://www.playark.com/)] pone fecha de lanzamiento. En concreto, el juego de Studio Wildcard llegará a nuestro sistema favorito **el próximo día 8 de Agosto**.


Para el que aún no lo conozca, ARK: Survival Evolved es un juego de mundo abierto en el que partimos de la premisa de que debemos desenvolvernos en una isla hostil en la que aparecemos desnudos y sin mas herramientas que nuestras propias manos. A partir de ahí, la asociación con otros jugadores, la creación de objetos, la construcción de edificios y la lucha por la supervivencia, mezclado todo con dinosaurios y otros habitantes a los que habrá que "cazar" ha hecho de este juego un desarrollo muy popular:



> 
> *Como hombre o mujer desnudo, congelado y muriendo de hambre en las costas de una isla misteriosa llamada ARK, debes cazar, recolectar, construir objetos, plantar cultivos, investigar tecnologías y construir refugios para soportar los elementos. Usa tu astucia y recursos para matar o domesticar dinosaurios y otros seres primitivos, y asóciate con otros jugadores o atácalos para sobrevivir, dominar... y escapar!*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/rn9S7XKFoNY" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


#### **Ragnarok**


 Además, también se ha anunciado el lanzamiento de una nueva expansión 'Ragnarok', un **DLC gratuito** que estará disponible para todos los jugadores y que básicamente consiste en un nuevo  y vasto escenario con nuevos objetos, recursos, cuevas, criaturas, ruinas, un océano con su ecosistema própio y mucho más:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/NfaOogXlG3g" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Si quieres saber más sobre ARK: Survivla Evolved y su expansión puedes verlo en su [anuncio oficial](http://steamcommunity.com/games/346110/announcements/detail/1324469570235634954), y puedes comprarlo en su página de Steam donde está traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/346110/" width="646"></iframe></div>


 

