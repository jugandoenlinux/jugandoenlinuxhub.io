---
author: Serjor
category: Rol
date: 2017-07-25 09:06:32
excerpt: "<p>Por fin lleg\xF3 el ansiado d\xEDa</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b208182456855b5680dad1bcd630c63f.webp
joomla_id: 418
joomla_url: pyre-de-supergiant-games-ya-disponible-para-gnu-linux
layout: post
tags:
- indie
- rol
- ethan-lee
- pyre
- supergiant-games
title: Pyre, de Supergiant Games, ya disponible para GNU/Linux
---
Por fin llegó el ansiado día

Ya os lo [contábamos](index.php/homepage/generos/rol/item/472-pyre-un-juego-de-rol-por-equipos-llegara-a-linux-steamos-el-proximo-25-de-julio) en su momento, y es que hoy se estrena Pyre, el nuevo juego de Supergiant Games, con versión para GNU/Linux desde el día 1, cosa que debemos agradecer enormemente a la desarrolladora por confiar en nuestro SO así como Ethan Lee por ser el encargado del port.


Pyre es un RPG por equipos dónde deberemos avanzara por un páramo y según vayamos avanzando nos tendremos que enfrentar a un equipo rival en una arena cuya disposición y configuración variará en función de las circunstancias.


La verdad es que es un juego con una mecánica jugable muy curiosa, ya que mezcla elementos de rol, pudiendo gestionar el progreso de los personajes, con un sistema de combates peculiar que recuerda más a un eSport que a un juego de rol, y es que parece una especie de baloncesto con magias. De hecho, no sería de extrañar que incluyeran un modo PVP pensado para competir on-line, ya que de momento solo tiene multijugador local.


Os dejamos un vídeo con el gameplay para que veáis cómo transcurren los combates, o los partidos (ojo con los spoilers):


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/KffBnlL8CFA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/462770/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Y tú, ¿piensas hacerte con este Pyre? Cuéntanoslo en los comentarios o en nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

