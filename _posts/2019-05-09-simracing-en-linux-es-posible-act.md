---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-05-09 10:52:19
excerpt: "<p>Veamos cual es la situaci\xF3n actual de este tipo de juegos en nuestro\
  \ sistema</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/af06806ddd1231b8086cf9d0c7570813.webp
joomla_id: 1041
joomla_url: simracing-en-linux-es-posible-act
layout: post
tags:
- wine
- open-source
- simulacion
- steam-play
- proton
- simracing
- nativos
title: "Simracing en Linux, \xBFes posible? (ACTUALIZADO)"
---
Veamos cual es la situación actual de este tipo de juegos en nuestro sistema


**ACTUALIZADO a 20-11-19:**


Como sabeis, ayer **@Bernat**, un miembro de nuestra comunidad, presentó una [nueva implementación del driver de volantes Logitech](index.php/homepage/generos/software/1131-new-lg4ff-un-nuevo-driver-mucho-mas-completo-para-nuestros-volantes-logitech), "[new-lg4ff](https://github.com/berarma/new-lg4ff)" que **habilita la mayor parte de los efectos de Force Feedback en Linux**. Antes solo estaba soportada la fuerza constante, por lo que en muchas ocasiones los juegos no tenían soporte completo o incluso era inexistente. Debido al lanzamiento de este nuevo controlador, **el panorama del SimRacing en Linux cambia completamente** y en JugandoEnLinux.com consideramos oportuno editar este artículo para ofrecer una información más actual que permita conocer al lector las posibilidades enormemente ampliadas que tiene a partir de ahora. Comenzamos....


La mayor parte de los simracers saben que casi todos los servidores a los que se conectan para jugar partidas online de sus juegos favoritos se ejecuta en Linux. Esto es debido a que este sistema operativo es mucho más eficiente, seguro y confiable que otros sistemas. Pero probablemente lo que muchos de los simracers desconocen es que hay posibilidades de jugar a algunos de sus juegos favoritos usando el sistema del pingüino. Aquí, en JugandoEnLinux os vamos humildemente detallar las posibilidades que teneis.


En primer lugar debeis saber que para iniciarse en este mundillo **es muy importante tener un volante**. Intentar adentrarse en la simulación deportiva con un mando, o peor aun, con teclado y ratón, es ir claramente en desventaja. Las sensaciones de conducción son tambien completamente diferentes, además de ser el control mucho más intuitivo y preciso.  Por desgracia, por ahora **la única marca con buen soporte en Linux es Logitech**. Hay que decir que esto no es gracias a la marca, sino a la dedicación de la comunidad del software libre, especialmente de [Simon Wood](https://twitter.com/mungewell), desarrollador también de Speed Dreams, y ahora gracias a [Bernat](https://twitter.com/bernat_arlandis), que le ha dado una nueva vuelta de tuerca ampliando el soporte a la mayor parte de los efectos. Volantes como el **Driving Force GT**, el **G27** o el **G29** funcionan a la perfección en nuestro sistema. Otras marcas como Thrustmaster tienen soporte parcial ([Tmdrv](https://github.com/her001/tmdrv)), funcionando pero sin Force Feedback. En la sección de Tutoriales de nuestro foro teneis bastante [información sobre su configuración](http://index.php/foro/dispositivos-de-control).


Por supuesto existe mucho más hardware en el mundo del simracing, pero como soy pobre os hablo del que conozco. Es muy probable que pedaleras, palancas de freno de mano, y demás artilugios funcionen cacharreando un poco, pero los experimentos mejor con gaseosa. Sobre esto último cualquier información que podais aportar sería muy bien recibida. Ahora vamos con la "chicha" del tema, y lo más importante, que son los juegos. Para no liarnos la cabeza vamos a clasificarlos en 4 categorías diferentes, y después un apartado de utilidades. Comencemos pues:


J**uegos Libres**
=================


 El en mundo del software libre no íbamos a ser menos y por supuesto existen juegos que tratan de emular la realidad en el mundo de las carreras. Hace poco publicábamos en nuestra web un [artículo sobre juegos de conducción libres](index.php/component/k2/2-carreras/1099-los-mejores-juegos-de-coches-libres) (arcade también) y estaban incluidos algunos de los que mencionaremos a continuación:


[TORCS](http://torcs.sourceforge.net/): Es el simulador pionero del software libre. Con un [desarrollo](https://en.wikipedia.org/wiki/TORCS#Development) con más de 20 años, y con varios nombres a sus espaldas (RCS, ORCS y finalmente TORCS), en este juego podremos disputar carreras únicas, campeonatos, resistencia... con una amplia variedad de coches y circuitos en un entorno completamente 3D.  Por norma general en casi todas las distribuciones podremos instalar facilmente sus paquetes (p.ej, en Ubuntu con "sudo apt install torcs"), pero también existe un [Flatpak](https://flathub.org/apps/details/net.sourceforge.torcs) y por supuesto su código en la [página del proyecto](https://sourceforge.net/projects/torcs/) si queremos compilarlo.


[Speed Dreams](http://torcs.sourceforge.net/): Es un fork de TORCS, y que nació como la necesidad de incluir muchas más funciones a este último. Los menús serían completamente rediseñados, añadiendo muchísimas más opciones y haciéndolos mucho más intuitivos; el juego adquirió el tiempo dinámico, mejoras en los reflejos, el modo carrera, un nuevo modo de simulación y multi-threading. Con el tiempo se fueron implementando muchas más opciones, como el modo multijugador local, el **Force Feedback**, y por supuesto más y mejores coches y pistas, entre otras cosas; convirtiéndolo en un juego muchísimo más completo que su predecesor. Recientemente han presentado la versión 2.2.2, de la que hablamos en un [artículo de nuestra web](index.php/homepage/generos/simulacion/14-simulacion/994-speed-dreams-disponible-en-flatpak). Hace algún tiempo publicamos una [entrevista con dos de sus creadores](index.php/homepage/entrevistas/35-entrevista/784-entrevista-speed-dreams-un-simulador-de-coches-libre) . **Si queréis compilarlo e instalarlo** podéis seguir un [Tutorial de nuestro Foro](index.php/foro/tutoriales/108-como-compilar-la-ultima-version-de-speed-dreams-gracias-gnuxero26). También podeis instalarlo en formato [Flatpak](https://flathub.org/apps/details/org.speed_dreams.SpeedDreams), creado por nuestro amigo @SonLink.


![speed dreams 20 car ls1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/speed-dreams-20-car-ls1.webp)


[Vdrift](https://vdrift.net/): Otro veterano juego, basado en el motor de físicas [Vamos](http://vamos.sourceforge.net/), y con unos **acabados realmente destacables**. Con casi 15 años a sus espaldas, el juego permite a los jugadores conducir montones de coches a través de **detallados escenarios basados en circuitos reales**. Las físicas del juego son bastante realistas como le corresponde a un simulador, y por supuesto podremos usarlo con múltiples dispositivos como teclados, mandos y volantes, incluyendo el soporte experimental de **Force Feedback**.  El proyecto que luce en ciertos aspectos como un juego comercial, está parado últimamente, con pequeñas correcciones en el código como podemos ver en la [página de su proyecto](https://github.com/VDrift/vdrift/). Según hemos sabido de su creador principal, **Joe Venzon**, **sería ideal que alguien retome el proyecto o haga un fork para crear algo completamente nuevo**. Esperemos que esto se produzca y que este proyecto tan destacable continúe su andadura y no caiga en el olvido. Si queréis **descargar y compilar Vdrift** podéis seguir el [tema que hemos preparado en la sección de Tutoriales](index.php/foro/tutoriales/141-compilar-e-instalar-vdrift-en-ubuntu) de nuestro Foro


 


J**uegos Nativos**
==================


Como podreis observar a continuación se trata de juegos recientes de Codemasters, traidos a nuestro sistema por Feral Interactive. Aunque no se puede decir que se trate de Simuladores propiamente dichos, si quitamos las ayudas se quedan muy cerca.


[GRID Autosport](https://www.humblebundle.com/store/grid-autosport?partner=jugandonelinux)**:** Aunque ya tiene un lustro, el juego luce bastante bien y resulta tremendamente entretenido tanto en el modo carrera como en el apartado multiplayer. Es el primer juego de Codemasters portado por Feral Interactive a nuestro sistema y es un título **muy variado con 5 disciplinas diferentes a escoger**, como son Turismos, Resistencia, Monoplazas, Tuning y Urbano.


[F1 2017](https://www.humblebundle.com/store/f1-2017?partner=jugandoenlinux): En esta ocasión Codemasters nos lleva a la categoría reina del Automobilismo, permitiéndonos disputar el campeonato del mundo de Formula 1. Aunque ya se había portado anteriormente a Linux [F1 2015](https://www.humblebundle.com/store/f1-2015?partner=jugandoenlinux), es esta versión la más completa, además de tener un rendimiento (gracias a Vulkan) y una respuesta de los controles mucho mejor. A parte de los típicos modos **destaca por su modo carrera y un multijugador muy trabajado**. Tenemos también la posibilidad de conducir monoplazas de otras épocas. Podeis ver el [análisis](index.php/homepage/analisis/20-analisis/660-analisis-f1-2017) que escribimmos en su día.


[DIRT Rally](https://www.humblebundle.com/store/dirt-rally?partner=jugandoenlinux)**:** Es sin duda el juego de Codemasters **más enfocado a la Simulación**, y conjuntamente con F1 2017, de los más recomendables. El juego, a pesar de estar portado con OpenGL tiene un **rendimiento espectacular** y las sensaciones que produce aumentan en gran medida la inmersión. Al igual que en el resto de las plataformas, el juego ha sido un **rotundo éxito en Linux/SteamOS**, tal y como podeis ver en el [análisis](index.php/homepage/analisis/20-analisis/362-analisis-dirt-rally) que le dedicamos en nuestra web.


![DRTR Announce 16E](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDIRTRally/DRTR_Announce_16E.webp)


[DIRT 4](https://www.humblebundle.com/store/dirt-4?partner=jugandoenlinux)**:** El últmo trabajo de conversión de Feral Interactive con una vertiente mucho más arcade, pero que **permite también jugar en modo Simulación**, consiguiendo unas sensaciones muy cercanas a DIRT Rally. En el modo Rally Incluye **localizaciones nuevas** como Australia, Tarragona y Michigan. También es posible jugar en modo **Rallycross**, del que tiene licencia oficial, y **LandRush**. Recientemente [lo hemos analizado](index.php/homepage/analisis/20-analisis/1143-analisis-dirt-4) en nuestra web.


 


**Steam Play – Proton**
=======================


Desde el verano pasado, Valve Software ha incluido en su cliente para Linux la utilidad Proton, que lleva la característica Steam Play a un nuevo nivel, **aumentando la compatibilidad de montones de títulos con nuestro sistema**. Para ello hace uso de un fork de la capa de compatibilidad Wine, conjuntamente con otras herramientas como DXVK o Faudio; permitiendo el uso de periféricos, Realidad Virtual o Steam Overaly como si de un juego nativo se tratase. Juegos con una buena compatibilidad son:


[Project Cars](https://www.humblebundle.com/store/project-cars?partner=jugandoenlinux): **Compatible al 100%** con Steam Play, permitiendo el uso del **Force Feedback** y el modo **online**, tiene un rendimiento excelente y proporciona una **experiencia altamente satisfactoria**. Mucho se hablo de la versión para Linux/SteamOS de este juego en su momento, pero ha finalmente ha tenido que ser Valve y no sus desarrolladores quienes nos dejasen disfrutarlo de forma decente. Con modo carrera, multiples opciones de configuración y dificultad, y una **variedad asombrosa de circuitos y coches**, este gran simulador nos hará disfrutar como si fuese nativo.


![Pcars1 Proton](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SimRacing/Pcars1_Proton.webp)


[Project Cars 2](https://www.humblebundle.com/store/project-cars-2?partner=jugandoenlinux): Al igual que la primera parte, sus desarrolladores [Slightly Mad](http://www.slightlymadstudios.com/), volvieron a prometer una versión para Linux que nunca llegó. El juego funciona correctemente, [a excepción del Force Feedback](https://github.com/ValveSoftware/Proton/issues/908)., funcionando con Force Feedback si instalamos [new-lg4ff](https://github.com/berarma/new-lg4ff) y lo combinamos con [fffbtools](https://github.com/berarma/ffbtools) (más [info](https://github.com/ValveSoftware/Proton/issues/908#issuecomment-543995804)). Previamente a la versión del mes de octubre del juego funcionaba perfecto al igual que la primera parte, pero trás sufrir una actualización hemos perdido esa importante funcionalidad. Esperemos que los desarrolladores de Proton (y Wine) den con la tecla y consigan volver a activarlo, pues al igual que su primera parte es un título completamente recomendable.


[Rfactor 2](https://store.steampowered.com/app/365960/rFactor_2/): Uno de los reyes de la simulación (por no decir el rey), tiene como puntos a favor tiene la gran cantidad de material que podermos comprar para él, así como un porrón de **elementos descargables desde Steam Workshop**, y por supuesto los **mods** creados por la comunidad. Uno de sus puntos fuertes en Windows es la fiel recreación del Force Feedback, pero por desgracia esta característica [no está presente con Proton](https://github.com/ValveSoftware/Proton/issues/245), funcionando sin esta característica y también el online y gracias a [new-lg4ff](https://github.com/berarma/new-lg4ff) podremos disfrutar de él sin problemas. Recientemente he comprobado que el online funciona correctamente, probablemente gracias a alguna actualización de Proton.


[Assetto Corsa Competizione](https://www.humblebundle.com/store/assetto-corsa-competizione?partner=jugandoenlinux): Desde el lanzamiento de su primera versión en Early Access, el juego de Kunos, [ha funcionado bastante bien con Proton](https://github.com/ValveSoftware/Proton/issues/1420), incluidos Online y Force Feedback. El juego nos permite conducir en el **Blancpain GT Series** con los impresionantes coches de la GT3. Como juego oficial encontraremos los circuitos, coches, equipos y conductores reales. Hay que decir que es un juego bastante exigente y ofrece una simulación detallada. Veremos que tal funciona tras su lanzamiento definitivo a finales de este mes. El juego funciona perfectamente tras salir del Early Access, aunque no muestra los videos de introducción.


![AssettoCC](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SimRacing/AssettoCC.webp)


[Race 07](https://www.humblebundle.com/store/race-07?partner=jugandoenlinux) y [GTR Evolution Expansion Pack](https://www.humblebundle.com/store/gtr-evolution-expansion-pack-for-race-07?partner=jugandoenlinux): Este otro veterano juego de conducción recrea el **campeonato oficial WTCC del año 2007**, asi como otros muchos coches (hasta 9 clases diferentes) tales como los de la Formula BMW y la Formula 3000, y hasta **32 circuitos reales**. El juego, a pesar de los años ofrece unas sensaciones muy reales y resulta tremendamente divertido, especialmente en el campeonato WTCC, donde las carreras son mucho más "encarnizadas". Su expansión GTR Evolution es altamente recomendable si queremos disfrutar de los **fantásticos coches GT** y de los siempre recurrentes **Nurburgring y Nordscheleife**. El juego [funciona perfecto con Proton, a exc](https://github.com/ValveSoftware/Proton/issues/1212)[n](https://github.com/ValveSoftware/Proton/issues/1212)[epción del Force Feedback](https://github.com/ValveSoftware/Proton/issues/1212) que solo se nota cuando chocamos. Tiene soporte completo de Force Feedaback instalando [new-lg4ff](https://github.com/berarma/new-lg4ff)


[DIRT Rally 2](https://www.humblebundle.com/store/dirt-rally-2?partner=jugandoenlinux)**:** La continuación del genial DIRT Rally ha sido una de las más esperadas por la comunidad SimRacing, y no es para menos porque el juego es una auténtica pasada. El título dispone de **nuevos escenarios** como Nueva Zelanda, Argentina y Polonia, y otros ya vistos en DIRT 4, pero con bastantes cambios**.** Por desgracia, con [Steam Play-Proton](https://github.com/ValveSoftware/Proton/issues/2366) funciona también, pero el volante es detectado como un mando y no hay Force Feedback, además de tener un mínimo retraso. Si uilizamos [new-lg4ff](https://github.com/berarma/new-lg4ff) y lo combinamos con [fffbtools](https://github.com/berarma/ffbtools) (más [info](https://github.com/ValveSoftware/Proton/issues/2366#issuecomment-543999619)) tendremos soporte de Force Feedback completo.Todos estos problemas que encontrarmos con Proton en este reciente juego se evitan con Wine, consiguiendo una simulación casi perfecta. **Esperemos que Feral se anime y nos lo traiga de forma nativa**.


**Wine**
========


Sin este proyecto no podríamos estar hablando ni de Proton, ni de DXVK. Es gracias a él que muchos linuxeros no tiramos la toalla y seguimos peleando por poder disfrutar de nuestros juegos favoritos en nuestro sistema. Estos son algunos de los títulos que funcionan:


[Assetto Corsa](https://www.humblebundle.com/store/assetto-corsa?partner=jugandoenlinux): Mucho nos hemos peleado con este juego para poder usarlo en Linux, pero finalmente tras la insistencia de muchos usuarios y las mejoras del proyecto Wine **funciona casi tan bien como si fuese nativo, incluyendo el Force Feedback y el online**. Para todos los que no lo conozcais debeis saber que **es un imprescindible** por las cotas de realismo que alcanza y sobre todo por tener **la comunidad de modders más extensa**, pudiendo encontrar casi de todo para él. Para facilitar su instalación se ha creado un instalador de [Lutris](https://lutris.net/games/assetto-corsa/).


![AssettoCorsa](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SimRacing/AssettoCorsa.webp)


[Automobilista](https://store.steampowered.com/app/431600/Automobilista/): El gran simulador Brasileño que utiliza el motor del primer rFactor, pero llevándolo al extremo, padece el mismo problema que rFactor 2, pudiendo unicamente disfrutarlo sin Force Feedback. , tiene soporte de Force Feedback usándo [new-lg4ff](https://github.com/berarma/new-lg4ff) . Este juego podría estar en la [sección de Proton](https://github.com/ValveSoftware/Proton/issues/246), pero con la herramienta de Valve no funciona online debido a que solo lo hace con una versión antigua (3.7). En el caso de Wine, es necesario usar versiones no posteriores a la 4,13, pues a partir de la 4.14 el menú principal no muestra todos los coches y circuitos. Una pena lo del Force Feedback, pues el juego **ofrece categorias y circuitos que habitualmente no encontrarmos en otros juegos**, y unas sensaciones muy fieles en la conducción. Aun así merece completamente la pena el jugarlo con Wine.


[Racer](http://racer.nl/): Si bien este **simulador gratuito** tenía hace años una versión nativa para Linux, este soporte fué abandonado y la ultima versión se puede disfrutar unicamente gracias a Wine. El juego solo viene con una circuito y un automóvil, pero si rebuscais un poco podeis encontrar multitud de [pistas](http://www.tracciontrasera.es/forum/racer/4-listado-circuitos-compatibles-racer-v0-90-rc10) y [coches](http://www.tracciontrasera.es/forum/racer/3-listado-coches-compatibles-racer-v0-90-rc10) para instalar. Dispone de soporte para Force Feedback y online.


[Live for Speed](https://www.lfs.net/): Otro veterano más, pero que continua aun en activo y con una pequeño pero fiel grupo de seguidores. **Funciona perfecto en Linux**, y gran parte de la culpa la tienen sus desarrolladores, que han facilitado mucho las cosas para que el juego funcione correctamente con Wine. El juego **permite ser descargado gratuitamente, pudiendo usar un circuito y 3 coches**, pero si de veras queremos disfrutar de él tendremos que adquirir [licencias](https://www.lfs.net/shop/licenseselect) que nos desbloquearán más contenido. Live for Speed se puede también instalar facilmente en Linux gracias a un [paquete Snap](https://snapcraft.io/liveforspeed).


![Live4Speed](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SimRacing/Live4Speed.webp)


[rFactor 1:](https://store.steampowered.com/app/339790/rFactor/?l=spanish) El primer rFactor **es un clásico más que merecido**, y con él mucha gente entró en el mundo del Simracing. Su calidad es tal que aún es completamente valido, además de contar con innumerables mods y un desempeño espectacular con Wine + D9VK. El soporte de Force Feedback funciona a la perfección utilizando [new-lg4ff](https://github.com/berarma/new-lg4ff). Parece que los años no pasan por él...


[GT Legends:](https://www.humblebundle.com/store/gt-legends?partner=jugandoenlinux) El juego de Simbin, previo a RACE 07, también funciona de maravilla si usamos [D9VK](https://github.com/Joshua-Ashton/d9vk) y con unos [pequeños ajustes](https://appdb.winehq.org/objectManager.php?sClass=version&iId=28706) lo tendremos corriendo en nuestro sistema sin problemas. El soporte de Force Feedback necesita de [new-lg4ff](https://github.com/berarma/new-lg4ff) para su funcionamiento. En él podremos competir con coches clásicos de todos los tipos y tamaños en los circuitos más icónicos. Realmente un “must have” que además suele estar muy bien de precio.


 


No queríamos dejar atrás al que posiblemente sea el juego más “Pro” de esta categoría, [iRacing](https://www.iracing.com/). Muchos sabrán que hace años tenía una versión funcional para jugar en Linux. Aunque no se trataba de una versión nativa, ya que esta lo hacía gracias a Wine. Por degracia con el tiempo, el juego avanzó hacía los 64 bits y este soporte se abandonó, pues en aquella época Wine solo funcionaba bien en 32 bits. **Es muy probable que si quisiesen el juego funcionase perfectemente ahora mismo**. Hace algún tiempo [hice algunas pruebas](https://appdb.winehq.org/objectManager.php?sClass=version&iId=36709) y conseguí que corriese sin problemas, pero el Online, al cabo de unos minutos fallaba, probablemente por algún software anti-trampas. También estaría muy bien poder jugar de forma nativa, o en su defecto con Proton o Wine, al fantástico [RaceRoom](https://www.raceroom.com/en/), un juego **Free To Play** con multitud de pistas y coches principalmente pensado para el online. En principio [parece que no se cierran en banda a esa posibilidad](https://forum.sector3studios.com/index.php?threads/linux-support.571/#post-178040), pero necesitarían de la ayuda de un programador experto en realizar ports. Por ahora, tanto con Wine como con Proton tiene problemas de conexión a los servidores del juego, por lo que puede tratarse de algún sistema de seguridad que no reconoce el sistema operativo usado y lo banea.


 


**Utilidades**
==============


[PyLinuxWheel](https://gitlab.com/OdinTdh/pyLinuxWheel)**:** Los volantes de Logitech (DFGT, DFPRO, G25, G27, G29 y G920) tienen buen soporte en nuestro sistema. Con esta utilidad, creada por **@OdinTdh** podemos cambiar los grados de giro facilmente.


[Oversteer](https://github.com/berarma/oversteer): Esta utilidad es más avanzada que la anterior y permite además de de poder cambiar el rango del volante, combinar pedales, crear perfiles para juegos, testear ejes y botones, y modificar automáticamente los permisos de las reglas de uso del volante. Esta creada por otro miembro de nuestra comunidad, **@berarma**.


![Captura de pantalla de 2019 02 20 21 17 39](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Oversteer/Captura%20de%20pantalla%20de%202019-02-20%2021-17-39.webp)


[Telemetría](http://www.ikorein.com/telemetry/): Es posible activar y consultar la telemetría en F1 2017 gracias a este software diseñado para ser utlizado con Java


 


Bueno, que os ha parecido este "pequeño" resumen. Espero que os haya gustado leerlo tanto como a mi escribirlo. Me gustaría mucho conocer vuestra opinión al respecto, y para ello podeis dejar un mensaje en los comentarios o charlar sobre el tema en nuestro canal de [Telegram](https://t.me/jugandoenlinux).


 


NOTA: Este artículo ha sido escrito tomando como base el post que hace unos días escribí para el [canal de SimRacing en Reddit](https://www.reddit.com/r/simracing/comments/ble2xg/current_state_of_simracing_in_linux/) (en inglés)

