---
author: leillo1975
category: Rol
date: 2018-02-15 14:50:49
excerpt: "<p>El juego pronto estar\xE1 disponible en Steam en Acceso Anticipado.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d7e3ca5cbffab97634af0e92f7679a11.webp
joomla_id: 646
joomla_url: project-gorgon-dispone-de-soporte-experimental-para-gnu-linux
layout: post
tags:
- steam
- acceso-anticipado
- mmorpg
- alpha
title: '"Project Gorgon" dispone de soporte experimental para GNU-Linux.'
---
El juego pronto estará disponible en Steam en Acceso Anticipado.

Recientemente **@Txema**, un integrante de nuestro grupo de [Telegram](https://t.me/jugandoenlinux), nos pasaba un [enlace a los foros](https://forum.projectgorgon.com/showthread.php?927-Project-Gorgon-Experimental-Linux-Launcher) de este proyecto donde se anunciaba el soporte, en fase experimental, de [Project Gorgon](https://www.projectgorgon.com/). Cierto es que el lanzador ya tiene casi un año, pero realmente hasta ahora no habíamos tenido conocimiento de la existencia de este MMORPG.


Actualmente este desarrollo se encuentra en estado Alpha y se puede jugar libremente, pero en unos días pasará a ser Beta y podrá ser descargado desde Steam, pasando a ser de pago. Los jugadores de la alpha que compren el juego podrán continuar usando su personje sin problemas. El juego desde ese momento pasará a tener soporte oficial para GNU-Linux. En un principio se pensaba otorgar al juego de soporte unicamente en Windows y Mac y más tarde para Linux; pero finalmente sus desarrolladores han decidido retrasar la salida en Steam (**Beta-Acceso Anticipado**) para incluir también nuestro sistema y hacer un lanzamiento simultaneo del juego, lo cual es un detalle a agradecer. En este momento podemos jugarlo usando un [**lanzador experimental**](http://client.projectgorgon.com/ProjectGorgonLauncherLinux.tgz) que nos permitirá descargar los archivos del juego y poder ejecutarlo.


El juego está desarrollado por **Eric Heimburg** y su esposa **Sandra Powers**. Ambos tienen sobrada experiencia como desarrolladores en proyectos tan conocidos como Asheron's Call, Star Trek Online o Everquest. Este proyecto que fué financiado por dos campañas de Crowfounding en [Kickstarter](https://www.kickstarter.com/projects/projectgorgon/project-gorgon-pc-mmo) e [Indiegogo](https://www.indiegogo.com/projects/project-gorgon-pc-mmo#/), también estuvo presente en el desaparecido [Steam Greenlight](https://steamcommunity.com/sharedfiles/filedetails/?l=spanish&id=306606334), donde pasó sin problemas. El juego nos lleva a un mundo abierto de Fantasía 3D donde podremos encarnar a **humanos, elfos o  rackshasa**, una especie de leones antropomorficos. Seremos libres de movernos a nuestro antojo. Podremos enfrentarnos a multitud de enemigos y criaturas pudiendo hacer uso de tanto de nuestras armas, como de nuestras habilidades y magia. Podeis ver algunos detalles del juego en su trailer:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/lCbqcWESWPo" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Sobre su precio aun no sabemos nada, pero intuimos que será similar al de otros juegos de este tipo. En cuanto esté disponible en Steam os informaremos de ello. Si quereis dejar vuestras impresiones sobre el juego podeis hacerlo en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

