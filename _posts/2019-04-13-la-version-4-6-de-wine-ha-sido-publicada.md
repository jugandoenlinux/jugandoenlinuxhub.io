---
author: Serjor
category: Software
date: 2019-04-13 07:24:44
excerpt: "<p>Descorchamos una nueva versi\xF3n de Wine</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6e110feeface5dc0c386188860a44f0a.webp
joomla_id: 1018
joomla_url: la-version-4-6-de-wine-ha-sido-publicada
layout: post
tags:
- wine
- vulkan
- winevulkan
- mono
title: "La versi\xF3n 4.6 de Wine ha sido publicada"
---
Descorchamos una nueva versión de Wine

Leemos en [phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Wine-4.6-Released) que la versión 4.6 de Wine ya está disponible.


Entre otros cambios y correcciones, lo más destacable de esta versión es que es la primera en incluir un backend propio para Vulkan. Al ser los inicios de este backend todavía no está lo suficientemente desarrollado como para poder ser usado directamente, pero es un hito importante ya que sienta las bases de cómo evolucionará a partir de aquí. Además del nuevo backend, otra interesante funcionalidad que nos trae esta versión es la capacidad de usar diferentes versiones de Mono de manera compartida. Actualmente si una aplicación o juego necesitaban una versión concreta de Mono, esta era descargada en el prefijo que usara dicho programa o juego, con lo que podíamos acabar teniendo varias veces la misma versión de Mono en nuestros discos duros. Ahora con este cambio podemos tener un único lugar donde descargar las versiones que necesitemos y que todas las aplicaciones o juegos usen de manera compartida estas librerías, ahorrándonos espacio en disco.


La lista completa de cambios podéis encontrarla [aquí](https://www.winehq.org/announce/4.6).


Y tú, ¿qué piensas de Wine y su influencia en el panorama de juegos para GNU/Linux? Cuéntanoslo en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

