---
author: Serjor
category: Carreras
date: 2018-01-07 15:45:52
excerpt: "<p>\xA1\xA1A calentar motores para este 2018!!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f63fb9fc1888bac2143cb5d1de3bea0f.webp
joomla_id: 596
joomla_url: este-viernes-en-la-partida-con-la-comunidad-especial-juegos-de-carreras
layout: post
tags:
- carreras
- comunidad
- eventos
title: 'Este viernes en la partida con la comunidad: Especial juegos de carreras'
---
¡¡A calentar motores para este 2018!!

Después de celebraciones, excesos y periplos por el mundo llevando regalos, las fiestas han llegado a su final, y todo vuelve a la normalidad. Afortunadamente eso quiere decir que la comunidad de [jugandoenlinux.com](https://jugandoenlinux.com) también, y esta semana volvemos, y lo hacemos con un especial coches.



Ya sabéis que si queréis participar en la elección del juego ganador podéis entrar en nuestro canal de [Telegram](https://telegram.me/jugandoenlinux) y votar por una de las opciones propuestas, y que el viernes a las 22:30 te puedes pasar por el canal de [Discord](https://discord.gg/ftcmBjD) para comentar las carreras mientras derrapas a toda velocidad. De todos modos, si tiendes a olvidarte de las cosas, apúntate en nuestro canal de [Steam](http://steamcommunity.com/groups/jugandoenlinux) para que te salte el aviso, y no perder así la oportunidad de participar en el evento.


Además, también ha vuelto la liga del Dirt Rally que organiza Jaime, a la que te puedes apuntar aquí [Liga Yodefuensa](https://www.dirtgame.com/es/leagues/league/92549/yodefuensa).


Así que ya sabes, si te gusta la velocidad, o simplemente quieres pasar un buen rato, no dudes en pasarte por el evento.

