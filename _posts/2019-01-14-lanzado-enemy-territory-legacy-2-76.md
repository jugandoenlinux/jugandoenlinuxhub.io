---
author: leillo1975
category: "Acci\xF3n"
date: 2019-01-14 08:25:48
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\">La nueva versi\xF3n de @ETLegacy incluye\
  \ importantes mejoras</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a60f95e6048a6076bc6ca88177633fab.webp
joomla_id: 948
joomla_url: lanzado-enemy-territory-legacy-2-76
layout: post
tags:
- open-source
- enemy-territory-legacy
- wolfenstein
- splash-damage
title: Lanzado Enemy Territory Legacy 2.76.
---
La nueva versión de @ETLegacy incluye importantes mejoras

Probablemente muchos de vosotros conozcais [Wolfenstein: Enemy Territory](https://www.splashdamage.com/games/wolfenstein-enemy-territory/) (Splash Damage, Activision), y probablemente también sepais que hace años, concretamente en el 2010, **fué liberado su código** como otros grandes juegos con el mismo motor de ID Software. El título fué en su día un conocido e hiperjugado multijugador freeware basado en la segunda guerra mundial que aprovechaba las virtuosas capacidades del motor de Quake 3. El proyecto Open Source [**Enemy Territory Legacy**](https://www.etlegacy.com/) recogió el guante y desde entonces apunta a crear un cliente/servidor compatible que permita continuar disfrutando de este clásico que sigue estando en buena forma y por el que parece que no pasan los años.


Hoy mismo, hace tan solo unas pocas horas, el equipo a cargo de su desarrollo anunciaba a través de las redes sociales la salida de la **nueva versión** , la **2.76** tal y como podemos ver aquí:



> 
> ET: Legacy 2.76 'The enemy is weakened!' has been released.  
> Read more about it here: <https://t.co/9F0d798bgx> [pic.twitter.com/JOxzVWkicp](https://t.co/JOxzVWkicp)
> 
> 
> — ET: Legacy (@ETLegacy) [13 de enero de 2019](https://twitter.com/ETLegacy/status/1084573260149415936?ref_src=twsrc%5Etfw)


  





Entre las [novedades](https://dev.etlegacy.com/projects/etlegacy/wiki/Changelog#276-The-enemy-is-weakened-released-13012019) más destacables de esta nueva versión apodada **"The Enemy Is Weakened"** (El enemigo está debilitado), que incluye esta nueva versión podemos encontrar las siguientes:


-El error "VM_Create on UI"  se resuelve, por lo que no se necesitan más soluciones manuales.  
 -Se implementa la clasificación de habilidades bayesiana, lo que permite una comparación global de las habilidades entre los jugadores.  
 -WolfAdmin es ahora el gestor de juegos por defecto, proporcionando muchos comandos de administración útiles.  
 -Nuestro propio servidor maestro está funcionando, así que incluso si el id master se cae, estás listo para hacerlo.  
 -"Renderer2 + assets" estará disponible en un abrir y cerrar de ojos, ofreciéndote los mejores gráficos para ET.


Es importante decir que también **es compatilble con la anterior versión, la 2.60**, por lo que podremos seguir jugando con la gente que aun no se ha actualizado sin problemas. El juego, que también es **multiplataforma** (Linux, Windows, MacOS y Android) , podemos adquirirlo desde la **[sección de descargas](https://www.etlegacy.com/download/platform/unix)** de su página web. Para que veais lo divertido que es podeis ver una partida que jugamos hace algún tiempo en nuestra comunidad: 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/Wwfo3M8879I" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Cuales son vuestras experiencias con Enemy Territory? ¿Os atreveis a echar unas partidillas? Opina en nuestros comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

