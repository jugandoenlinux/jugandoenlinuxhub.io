---
author: leillo1975
category: Estrategia
date: 2017-09-13 17:40:58
excerpt: "<p>La actualizaci\xF3n ha llegado con meses de retraso a nuestro sistema.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d6c28c6582af8287b0eef478e4a8f548.webp
joomla_id: 462
joomla_url: por-fin-ha-llegado-el-summer-update-de-civilization-vi
layout: post
tags:
- aspyr-media
- actualizacion
- civilization-vi
- firaxis
- 2k
title: Por fin ha llegado el "Summer Update" de Civilization VI.
---
La actualización ha llegado con meses de retraso a nuestro sistema.

Uno de los [grandes lanzamientos de este año](index.php/homepage/generos/estrategia/item/334-sid-meier-s-civilization-vi-ya-esta-disponible-en-linux-steamos) para nuestro sistema ha sido sin duda Civilization VI. Precedido por una quinta parte soberbia, el juego de [Firaxis games](http://www.firaxis.com/) mejoró esta obra maestra con el buen hacer que caracterizan los títulos de esta compañía. Aquí, en Jugando en Linux, le dedicamos un [completo análisis](index.php/homepage/analisis/item/352-analisis-sid-meier-s-civilization-vi) a la última parte de una saga que **es para muchos referente sin discusión** de la estrategia por turnos.


 


En esta ocasión requerimos vuestra atención para informaros que la gente de [Aspyr Media](http://www.aspyr.com/) ya tiene lista la ultima actualización para este juego. Se trata de la "Summer Update" e incluye novedades y mejoras bastante notables que describiremos ahora. El anuncio nos llegaba a través de twitter y rezaba lo siguiente:


 



> 
> The Summer Update and Nubia are now available for [@CivGame](https://twitter.com/CivGame) [#Linux](https://twitter.com/hashtag/Linux?src=hash)! [pic.twitter.com/TzpRGsYuXD](https://t.co/TzpRGsYuXD)
> 
> 
> — Aspyr Media (@AspyrMedia) [September 13, 2017](https://twitter.com/AspyrMedia/status/908011212956471297)



 


Este "Summer Update", que debido al **retraso en su lanzamiento en nuestro sistema** casi llega en Otoño, posee las **siguientes caracteristicas** ([teneis la lista completa aquí](https://civilization.com/news/entries/civilization-vi-summer-2017-update-available-now)):


- Inclusión de la Civiiización Nubia y su nueva dirigente Kandake Amanitore. Poseen unos maestros arqueros especiales llamados Pítati, la posibilidad de contruir pirámides nubias


- Nuevo escenario "El regalo del Nilo" donde Egipto y Nubia se disputaran el control del rio más largo de nuestro planeta.


- Añadido botón para reempezar mapas generados.


- Posibilidad de guardar ajustes de partida.


- Cambios en el balance del juego, como reducción del coste de ciertas unidades, edificios, características de las diferentes civilizaciones, etc


- Mejoras en la generación de escenarios, en el comportamiento de la Inteligencia Artificial del juego, en la interfaz del juego o en la diplomacia


- Corregido y mejorado el modo multijugador,


- Multitud de correcciones de Bugs


 


Como veis una jugosa y extensa actualización que hace si cabe más interesante a este juego. Como siempre, desde Jugando En Linux nos gustaría recordaros que seguiremos informando sobre todas las noticias relaccionadas con este título y que **os recomendamos encarecidamente** su adquisición si os va la estrategia pues no os va a defraudar. Podeis comprar Civilization VI en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/289070/123215/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 Os dejamos con el video que ha creado Aspyr Media para anunciarnos la llegada de esta actualización:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/WqDeHm2EOrw" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Habeis jugado a algún juego de la Saga? ¿Qué os parece esta actualización? Puedes dejar tus respuestas en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

