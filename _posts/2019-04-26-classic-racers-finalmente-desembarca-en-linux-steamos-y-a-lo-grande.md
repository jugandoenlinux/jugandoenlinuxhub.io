---
author: leillo1975
category: Carreras
date: 2019-04-26 22:01:10
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >@_VisionReelle_ <span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo\
  \ r-qvutc0\">@BFP_60s_RG ha lanzado los \"Donation DLCs\"</span><br /></span></p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1bcfbd07454f9ec45d6daa888b9a8a71.webp
joomla_id: 1031
joomla_url: classic-racers-finalmente-desembarca-en-linux-steamos-y-a-lo-grande
layout: post
tags:
- carreras
- unity3d
- classic-racers
- vision-reelle
- coches-clasicos
title: '"Classic Racers" finalmente desembarca en Linux (ACTUALIZADO 6).'
---
@_VisionReelle_ @BFP_60s_RG ha lanzado los "Donation DLCs"  



**ACTUALIZACIÓN 13-2-20**: Dios.... que vergüenza, se nos ha pasado completamente esta noticia, y eso que hace un mes os anunciamos las "[Donation DLC](https://steamcommunity.com/app/1037480/eventcomments/3667554077319739958)s" y deberíamos estar prevenidos, pero **desde el 21 de Enero** están disponibles para todos aquellos que querais colaborar con el desarrollo de este estupendo y actualmente gratuito juego. **Existen 3 modalidades de DLC:**


-"[Regional Sponsoring](https://store.steampowered.com/app/1230280/Classic_Racers__Regional_Sponsoring__Donation_DLC/?curator_clanid=34603378)" (**0.79€**): ¿Lo has jugado y te ha parecido genial? Deja una propina al desarrollador. Seguirá haciéndolo mejor.


-"[National Sponsoring](https://store.steampowered.com/app/1230290/Classic_Racers__National_Sponsoring__Donation_DLC/?curator_clanid=34603378)" (**1.59€**): Pasión y amor. Esto es lo que sentiste durante el juego. A pesar de sus problemas, puedes ver que este juego merece ir más allá. Patrocínalo para hacerlo mejor y más grande!


-"[International Sponsoring](https://store.steampowered.com/app/1230300/Classic_Racers__International_Sponsoring__Donation_DLC/?curator_clanid=34603378)"  (**3.99€**): Quieres hacer la apuesta e invertir en el próximo juego matador de carreras. ¡Quieres verlo en los eventos de ESport! ¡Haz una gran donación y patrocina oficialmente su desarrollo! Este juego será la próxima referencia mundial.


Es importante advertiros que **estas DLCs no contienen ningún tipo de material extra, nuevas características, evento especial o recompensa**, por lo que vas a encontrarte lo mismo aunque no compres estas DLCs. Su único propósito es ayudar al creador del juego gratuito "[Classic Racers](https://store.steampowered.com/app/1037480/Classic_Racers/)" a continuar el desarrollo  y premiarlo por todo lo que ha hecho hasta ahora en caso de que hayamos adquirido el juego de forma gratuita.




---


**ACTUALIZACIÓN 15-1-20:** Tras algunos meses sin noticias después de la poner gratis el juego en Steam por falta de ventas, el único desarrollador de Classic Racers (**[Vision Réelle](http://www.visionreelle.fr/)**) [acaba de anunciar](https://steamcommunity.com/app/1037480/eventcomments/3667553591698519468) hace unas horas que **el juego ha aumentado su base de jugadores de 2500 a 10000 aproximadament**e, y para celebrar este hecho ha estado trabajando (gratis) en el juego en sus ratos libres para mejorar sus gráficos **cambiando las técnicas de iluminación y sombreado a PBS completo**, y mejorando además su rendimiento (que ya no era malo, por cierto). Además también incluirá una característica muy útil y solicitada por los usuarios, como es el poder **redefinir los controles**. 


Además de esta inminente actualización, también ha anunciado que **incluirá en el juego una "Donation DLC"** donde cada usuario del juego podrá pagar la cantidad que considere adecuada por este juego, y de esta forma premiar al desararrollador por su trabajo, en este momento gratuito. Por lo tanto aquí, en JugandoEnLinux.com permaneceremos atentos a las noticias para que esteis informados en cuanto tengamos algo nuevo que contaros sobre el juego. Gracias a **@josegp26** por el aviso!




---


**ACTUALIZACIÓN 16-10-19:** Cuando hace tan solo unos días os hablábamos de la ultima actualización del juego, la 1.3 (justo debajo de esta actualización), acabamos de recibir la [triste noticia](https://steamcommunity.com/app/1037480/eventcomments/3159763879073359392) de que su desarrollador tira la toalla con el juego y a partir de ahora lo regalará en Steam. Las razones que se esgrimen es que no puede dedicar más tiempo  a este proyecto teniendo en cuenta el poco rendimiento que le ha sacado a nivel de ventas. Según comenta, ha intentado tratar con editoras pero ninguna ha mostrado interés por el juego. También aclara que los servidores donde se registran los tiempos permanecerán activos hasta final de año. Tampoco descarta intentarlo en consolas, esperemos que tenga mejor suerte.


El juego de momento tiene el mismo precio que hasta hace unos días, [**2.39€**](https://store.steampowered.com/app/1037480/Classic_Racers/), por lo que si quereis ehcar una mano, y de paso llevaros un muy buen juego por muy poco dinero, aun teneis la oportunidad de devolver de la mejor manera posible el esfuerzo realizado por este creador. A veces pasan cosas incomprensibles, que pequeñas joyas como esta pasen de largo para el gran público. Esperemos que el desarrollador venga pronto con noticias más alagüeñas y nuevos proyectos. Como decimos, una pena...


 




---


**ACTUALIZACIÓN 9-10-19:** Hacía bastante tiempo que no teníamos noticias de este juego, y más teniendo en cuenta que **[Vision Réelle](http://www.visionreelle.fr/)** había prometido una actualización con cambios importantes en el juego. Finalmente han cumplido lo prometido y hoy ya tenemos por fin disponible tan ansiado parche. Las novedades son muy jugosas y harán las delicias de los amantes de este juego. Los cambios de esta versión, [la 1.3](https://steamcommunity.com/games/1037480/announcements/detail/1608273623059562024),  son los siguientes:


*Gráficos:*  
*-Nuevo sombreado PBR para todos los coches*  
*-Reflexión en tiempo real sobre la pintura*  
*-Luces en tiempo real para el escape y los frenos*  
*-Incremento de la visibilidad máxima de 400 metros a 1 kilómetro (dependiendo de las necesidades de la pista).*  
*-**Nuevo sistema de interfaz de usuario y gráficos***  
*-Sistema de niebla lejana*


*Sonido:*  
*-No más sonidos de choque después de la carga de la pista*  
*-Sonidos de " Fred GT 50 " rehechos*  
*-*Sonidos de* " Citroen SD "* *rehechos*  
*-Sonidos "Bartha 1000 Barchetta" mejorados*  
*-Ajustes de sonidos globales para todos los coches*


*Juego:*  
*-Nueva cámara. Más dinámico, más divertido*  
*-Nuevos ajustes físicos globales para una simulación más controlable y estable*  
*-Nueva curva de dirección para más precisión a baja velocidad y más estabilidad a alta velocidad*  
*-Nueva configuración del neumático para una sensación de agarre más realista*


*Contenido:*  
*-Vegetación nueva para algunas pistas*  
*-Nuevo entorno de garaje para el menú principal y el menú de selección de vehículos*  
*-**Coche nuevo Persche 9080***  
*-**Nueva pista cerrada " Manoca "** con su variación invertida*  
*-**Nueva pista de carretera abierta " Col de la Turbine "** con su variación inversa*  
*-**Nuevo campeonato " Sur de Francia "** con las nuevas pistas*


*Desempeño:*  
*-Añadir una opción de VSync en el menú " Graphics Options*  
*-Máxima velocidad de fotogramas con tope a 120 fps*  
*-Optimizaciones menores*


El desarrollador también nos informa que **la próxima actualización se centrará más en las características**, intentando hacer un coche fantasma, tablas de clasificación de grupos de coches y tal vez un sistema PVP. También añadirá un poco de contenido (uno o dos coches y una pista). Habrá que permanecer atentos de nuevo, pues la siguiente, como veis, promete. La verdad es que es increible y muy de agradecer que un juego desarrollado por una sola persona y con un precio tan bajo tenga un soporte tan bueno y su creador siga trabajando en él... que tomen ejemplo algunos "grandes". Os recordamos que el juego **lo podeis comprar en Steam por tan solo 2.39€**:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1037480/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>





---


****ACTUALIZACIÓN 3-5-19:**** Finalmente hemos realizado el sorteo de las dos claves y ya tenemos la pareja de ganadores. Desde JugandoEnLinux.com nos encanta felicitar a **@Perracomax** y a **@BonJov75**, ganadores de una copia de este juego. También nos gustaría agradecer a **Jonathan** de [Vision Réelle](http://www.visionreelle.fr/) por facilitarnos las claves para crear los videos y posterior análisis y para el sorteo. Para que veais que no hubo trampa ni cartón os dejamos con el **video del sorteo**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/XQ71UDS1r04" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**ACTUALIZACIÓN 29-4-19:** ¿Qué tal? ¿Os ha gustado el juego? A nosotros nos ha encantado, tal y como pudisteis ver en el Streaming de este fin de semana. Nos gustaría anunciaros que el desarrollador de "[Classic Racers](https://store.steampowered.com/app/1037480/Classic_Racers/)", [Vision Réelle](http://www.visionreelle.fr/), nos ha facilitado las claves para realizar ese video y un posterior análisis, y también **un par de claves extra para que se las regaláramos a los miembros de nuestra comunidad**. Como seguro que sereis unos cuantos los interesados, vamos a sortearlas y os diremos quienes serán los ganadores a finales de semana, muy probablemente el viernes por la noche, por lo que estad atentos a nuestras redes sociales de [Twitter](https://twitter.com/JugandoenLinux) y [Mastodon](https://mastodon.social/@jugandoenlinux), pues haremos el anuncio del sorteo por estos medios.


Ahora os vamos a contar que es lo que teneis que hacer para participar, y es algo muy simple; **solo teneis que realizar un comentario en este artículo y ya estareis participando**. Hay que aclarar que aunque una persona haga más de un comentario solo contará una vez, y que los editores de JugandoEnLinux no contamos para este sorteo. Osea que ya sabeis, podeis decirnos que os parece este juego, si os gustó el Stream de este Domingo, o simplemente que pongais algo como "Quiero participar en el sorteo".


Os deseamos mucha suerte a todos y **os dejamos en compañía del video que emitimos ayer por la tarde**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/M1PAEACs1OY" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL:**


A veces puede más el que quiere que el que puede, y en este caso esta frase toma todo el sentido. Hace un par de semanas [os confirmábamos](index.php/homepage/generos/carreras/2-carreras/1138-el-juego-classic-racers-podria-llegar-a-linux-steamos-dentro-de-poco) que tras ponernos en contacto con su creador, **Classic Racers incluiría soporte para nuestro sistema en poco tiempo**, y así ha sido. Os he de contar un secreto, al día siguiente de que **la comunidad convenciese al desarrollador de que convirtiese a nuestro sistema su juego**, este ya tenía una versión completamente funcional de este, que tuvimos la oportunidad de probarlo como Betatesters. Hay que decir que gran culpa de esto lo tienen las bondades del motor **Unity3D**, que facilita mucho las cosas, pero como no, hay que decirlo con claridad, **que el creador acepte la petición y se ponga manos  a la obra no es menos importante**.


Probablemente podríamos tener una versión funcionando en Steam pocos días después, pero [Vision Réelle](http://www.visionreelle.fr/), que así se llama **la desarrolladora ha preferido lanzar nuestra versión conjuntamente con la actualización 1.2** que incluye importantísimas novedades, tal y como podreis leer dentro de un momento, por lo que, como veis, el creador ha querido asegurarse que el lanzamiento fuese sustancial y coordinado con el resto de versiones, para incrementar la calidad del juego.


Por lo que hemos podido probar, os podemos decir que **el juego se mueve con soltura y ofrece unos gráficos que aunque no muy detallados, son resolutivos, coloridos y resultones**. El diseño de escenarios, coches, HUD y efectos nos recuerda mucho a los **juegos de carreras de finales de los 90**, y hay que decirlo, su jugabilidad también, siendo esta tremendamente directa e intuitiva, por lo que no os costará haceros con el control del coche en los circuitos de Classic Racers. Debeis también tener en cuenta que **este juego está desarrollado por una compañía muy pequeña**, y que si no me equivoco, la conforma un solo desarrollador, por lo que debeis ser compresivos con lo que os vais a encontrar en el juego, que a nuestro juicio es muchísimo más que lo tendreis que pagar por el, pues no llega a los 5€. Os aseguraremos que pasareis un buen rato jugándolo.


En cuanto a las novedades que traerá la nueva versión 1.2, con respecto a la anterior, la lista de cambios es impresionante (Traducción Online):


***Controles -***


*-Añadido - Apoyo en el volante (implementación temprana).*  
*-Añadido - Rumores y vibraciones tanto para los controladores como para los volantes.-Añadido - Nueva configuración en el panel de opciones:*  
*Velocidad de dirección - para aumentar/disminuir la respuesta de la dirección.*  
*Ángulo de dirección VS Speed - para aumentar/disminuir el ángulo de dirección en relación con la velocidad del vehículo.*  
*Dirección Directa - para acortar todos los filtros y configurar la dirección para tener una entrada directa a las ruedas. Para los mandos de los volantes.*  
*-Modificado - El mapeo de control ha sido rehecho tanto para los controladores como para los teclados con el fin de implementar las nuevas características.*  
*-Modificado - Soporte de controladores tanto para el juego como para la interfaz de usuario. Ahora, usted puede jugar el juego desde el principio hasta el final sin tener que dejar las manos en el mando!*


  
***Gráficos -***  
*-Modificado - Interfaz de usuario totalmente a distancia. Más fluido, más sensible y controlable al 100% con los controladores.*  
*-Modificado - Los neumáticos Fred GT 50, que eran brillantes, han sido corregidos.*  
*-Modificado. Luces de freno nuevas y bonitas.*  
*-Añadido - Nuevos árboles lejanos (más realistas y más definidos).*


***Audio -***  
*-Modificado - Motor de audio para el Bartha 1000 Barchetta.*  
*-Modificado - Motor de audio para el Astun Macro.*  
*-Modificado - Motor de audio para el Cavallino F3.*


  
***Juego -***  
*-Añadido Freno de mano.*  
*-Añadido - Transmisión manual / automática.*  
*-Añadido - Reintento rápido. Ahora, mantenga pulsado el botón "Quick Retry" durante un segundo para reiniciar rápidamente la carrera.*  
*-Modificado - Los ajustes de la dirección base han sido mejorados. Ahora el coche responde de forma más progresiva.*  
*-Modificado - Afinaciones físicas para todos los coches. Se pueden sentir ligeras diferencias, pero el aspecto general de cada coche sigue siendo el mismo.*  
*-Modificado - La cuenta atrás se ha reducido en un 50% para entrar más rápidamente en la carrera.*  
*-Modificado - Los goles de los 2 primeros campeonatos han sido disminuidos para permitir que todos tengan éxito en los primeros pasos del juego.*


***Contenido -***  
*-Añadido - Nuevo campeonato del grupo 1: "Slalom Grupo 1".*  
*-Añadido - Nuevo campeonato del grupo 4: "Down Hill and Night".*  
*-Añadido - Nueva variación de pista. "Gliaire" al revés (cuesta abajo).*  
*-Añadido - Nueva variación de pista. "Gliaire" Slalom.*  
*-Añadido - Nueva variación de pista. "La Belle Bleue" al revés (cuesta abajo).*  
*-Añadido - Nueva variación de pista. "Route de Neve" al revés (cuesta abajo).*  
*-Añadido - Nueva variación de pista. "Lyon de Noche".*  
*-Añadido - Nueva variación de pista. "Monte del Altiplano" Slalom.*


***Miscelánea -***  
*-Modificado - Velocidad de cuadro de renderizado desbloqueada desde 60 fps hasta el infinito. :D*  
*-Otras pequeñas cosas que me has pedido pero que he olvidado. :D*


Si quereis comprar Classic Racers podeis hacerlo en [**su página de Steam**](https://store.steampowered.com/app/1037480/Classic_Racers/), tal y como os habíamos dicho antes a tan **solo 4,99€** (un regalo, teniendo en cuenta lo que ofrece). En JugandoEnLinux intentaremos hacer un Stream y/o grabar un Video para que lo podais ver en nuestros canales de [Twitch](https://www.twitch.tv/jugandoenlinux) o [Youtube](https://www.youtube.com/c/jugandoenlinuxcom), por lo que estad atentos a nuestras redes sociales ([Twitter](https://twitter.com/JugandoenLinux) y [Mastodon](https://mastodon.social/@jugandoenlinux)). Tambień, si nos es posible y nos dais algo de tiempo intentaremos realizar un análisis completo de este pequeño-gran juego. Os dejamos con el Trailer del Classic Racers:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/yuWZ2aFbS9g" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 Si os gustan los juegos de coches ya estais tardando en haceros con este "Classic Racers". Puedes comentar o preguntarnos lo que quieras en los comentarios de este artículo, o charlar con nosotros en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

