---
author: leillo1975
category: Puzzles
date: 2022-11-03 10:52:15
excerpt: "<p>El desarrollador @FlavioCalva de @ya2tech (YORG) nos presenta su nuevo\
  \ proyecto.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/pMachines/pMachines.webp
joomla_id: 1505
joomla_url: descubre-pmachines-un-juego-libre-de-logica-y-puzzles
layout: post
tags:
- puzzles
- ya2
- flavio-calva
- panda3d
- pmachines
title: "Descubre pMachines, un juego libre de l\xF3gica y puzzles."
---
El desarrollador @FlavioCalva de @ya2tech (YORG) nos presenta su nuevo proyecto.


 Es muy probable que "los mas viejos del lugar"  recordeis un fantástico juego libre del que escribimos en repetidas ocasiones en esta web. El juego en cuestión era [YORG](https://www.ya2.it/pages/yorg.html), y el género las carreras de coches arcade (podeis encontrar toneladas de información en nuestra en [este enlace]({{ "/tags/yorg" | absolute_url }})). Ahora su creador [Flavio Calva]({{ "/posts/entrevista-a-flavio-calva-de-ya2-yorg" | absolute_url }}), al que entrevistamos hace unos años, nos propone algo completamente diferente, mostrándonos lo que será un **prototipo** de su nuevo juego, [pMachines](https://www.ya2.it/pages/pmachines.html), donde deberemos devanarnos los sesos usar nuestra inteligencia, y una buena dosis de paciencia para resolver los puzzles y problemas que se nos muestran.


El juego está claramente inspirado por el clásico [Crazy Machines](https://en.wikipedia.org/wiki/Crazy_Machines), título del que podemos ver, salvando las distancias, claras influencias en superventas como Snipperclips, The Talos Principle,  o incluso la archiconocida saga Portal. Las herramientas que se han usado son todas libres y podemos encontrar Panda3D, Blender, GIMP y el motor de físicas Bullet. En todas ellas Flavio cuenta con experiencia al ser utilizadas en proyectos anteriores, lo cual es una garantía.


Podeis descargar **el prototipo de este videojuego en formato AppImage** en la sección de [descargas](https://www.ya2.it/download) de su página web. También es posible acceder a su página en Itch.io si seguís las instrucciones del enlace anterior. Si quereis haceros una idea de como será lo mejor es que lo jugueis, pero también os dejamos por aquí el trailer:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/Ss1MFxOQCpk" title="YouTube video player" width="780"></iframe></div>


¿Qué os ha parecido este pMachines? ¿Os gustan los juegos de puzzles y lógica? Puedes contarnos tu parecer sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux). 

