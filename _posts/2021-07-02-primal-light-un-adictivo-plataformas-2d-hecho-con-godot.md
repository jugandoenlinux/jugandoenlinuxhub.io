---
author: P_Vader
category: Plataformas
date: 2021-07-02 17:27:24
excerpt: "<p><span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\"><span\
  \ class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">Presentamos @PrimalLightGame\
  \ </span></span>un juego de plataformas retro de Fat Gem Games desarrollado con\
  \ @GodotEngine<span class=\"r-18u37iz\"></span><span class=\"css-901oao css-16my406\
  \ r-poiln3 r-bcqeeo r-qvutc0\"> </span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/PrimalLight/primal_light_banner_lg.webp
joomla_id: 1309
joomla_url: primal-light-un-adictivo-plataformas-2d-hecho-con-godot
layout: post
tags:
- plataformas
- godot
title: Primal Light un adictivo plataformas 2D hecho con Godot
---
Presentamos @PrimalLightGame un juego de plataformas retro de Fat Gem Games desarrollado con @GodotEngine 


Hace ya algunas semanas nuestro amigo Odintdh nos aviso por nuestro canal de Matrix de un llamativo juego hecho con [Godot](https://godotengine.org/) y la curiosidad me hizo contactar con los desarrolladores para que me hablasen un poco de su juego y rápidamente se ofrecieron para que lo probásemos.


Esta empresa compuesta por un programador y un artista, que ademas son buenos amigos, un día decidieron hacer realidad su juego, lo cual los llevó a trabajar durante 3 años. Y se nota que han puesto mucho empeño en hacerlo bien.


Shane, su programador, nos contaba que **tiene una fuerte inspiración en Blasphemous y Hollow Knight**. Ciertamente el aspecto y algunas mecánicas del juego recuerdan a estos dos juegos aunque se aleja del formato puramente metroidvania. Personalmente lo clasificaría de un plataformas 2D con ligeros toques de metroidvania. Como curiosidad me contaban que se han visto influenciados por sus juegos de cuando eran críos como el Abe's Oddysee.


![primal light temple](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/PrimalLight/primal_light_temple.webp)


Este **plataformas 2D no es del tipo duro, sin grandes inercias, complejos movimientos o combos. Llegas al sitio donde quieres ir y sin sorpresas.** A lo largo del juego vas **adquiriendo algunas nuevas habilidades** que van añadiendo . Se compone de **10 fases con sus respectivos jefes,** algunos de ellos endiabladamente difíciles, pero superables.


El arte grafico es de un **pixel art retro, que si que recuerda a Blasphemous**, aunque lógicamente sin llegar a su nivel, aun así diría que es muy bueno y con un gran merito si contamos que ha sido realizado por un solo artista. Buenas animaciones copiando fielmente ese estilo de los 16bits.


![primal light serpent](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/PrimalLight/primal_light_serpent.webp)


En el apartado sonoro se encarga el mismo diseñador gráfico, un aspecto que en juegos indies se suele descuidar, por suerte no ha sido así en este caso y **destaca la música con muy buen gusto durante todo el juego. Doble merito para Jeff, este artista polifacético en todos los sentidos.** Cada nivel cuenta con diferentes melodías sin llegar a hacerse repetitivas. Mas de una hora de ambientación muy bien llevada a lo largo del juego. Perfecto.


El juego con su motor Godot responde a la perfección, notándose que este motor en 2D está muy pulido. El único pero que le puedo poner es que al seleccionar el idioma en Español el tipo de letra cambia a una tipo Sans clásico, según me indicaba el desarrollador porque aún no ha podido implementar una fuente de letra que se ajuste a los diacríticos de cada idioma como las tildes en el español. Espera implementarla en un futuro.


En definitiva me alegro mucho que la casualidad de un comentario en nuestro canal de Matrix me haya cruzado con este videojuego en el que se demuestra a ver puesto mucho empeño para que se aun producto indie de calidad hasta en el mas mínimo detalle. Si te gustan los plataformas de la vieja escuela sin duda te lo recomiendo.


Aquí os dejo una video de las primeras fases cuando estuve probando del juego:


 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="480" src="https://www.youtube.com/embed/jYB63sWdAFs" title="YouTube video player" width="780"></iframe></div>


 


<div class="steam-iframe"><iframe data-preserve-html-node="true" height="200" src="https://store.steampowered.com/widget/771420/?t=Be%20sure%20to%20check%20out%20Primal%20Light%20on%20Steam!" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Te gusta es tipo de juego? ¿Qúe te ha parecido Pirmal Light? Cuentanoslo en nuestro canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) .


 


