---
author: Pato
category: Estrategia
date: 2017-02-20 16:04:01
excerpt: <p>El juego ha aparecido en SteamDB con apartados para Linux</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/59b514757c03f4e14c006ca63de02928.webp
joomla_id: 225
joomla_url: total-war-shogun-2-tiene-posibilidades-de-llegar-a-linux-steamos
layout: post
tags:
- steamos
- rts
- estrategia
- rumor
title: '''Total War: SHOGUN 2'' tiene posibilidades de llegar a Linux/SteamOS'
---
El juego ha aparecido en SteamDB con apartados para Linux

Gracias a un tweet de SteamDB nos enteramos hoy de que hay posibilidad de que 'Total War: SHOGUN 2' llegue a Linux/SteamOS:



> 
> New Game:  
> Total War: SHOGUN 2<https://t.co/0OOjBnMiHJ>
> 
> 
> — SteamDB Linux Update (@SteamDB_Linux) [February 20, 2017](https://twitter.com/SteamDB_Linux/status/833696978273959937)



 Una vez dentro de la [base de datos](https://steamdb.info/app/34330/depots/), podemos ver varios apartados con el logo de Linux aún vacíos, y en el apartado de "ramas" (Branches) encontramos apartados con el prefijo "domesticated". ¿A que nos suena esto? 


Pues nos suena a Feral Interactive. El estudio al que le debemos tantos ports ya, suele prefijar como "domesticated" las ramas de los juegos en los que trabaja, así que como se suele decir, "blanco y en botella". Otra pista es que la misma Feral fue la encargada de portar el juego a sistemas Mac, por lo que no es nada descabellado pensar que son ellos los que están detrás de estos cambios en la base de datos del juego.


De cualquier forma, aún no podemos echar las campanas al vuelo pues no es la primera vez que en SteamDB aparecen juegos con apartados para Linux y luego no han terminado llegando a nuestro sistema. Pero como soñar es gratis y hay buenos indicios... 


'Total War: SHOGUN 2' [[web oficial](https://www.totalwar.com/)] es un juego de estrategia ambientado en el Japón feudal donde podrás gestionar y construir tu imperio, y luchar contra tu enemigos en un entorno 3D mejorado.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/W48QJP710-c" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


¿Te gustaría poder jugar a 'Total War: SHOGUN 2' en Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

