---
author: Jugando en Linux
category: Editorial
date: 2022-12-31 13:23:05
excerpt: "<p>Y con este art\xEDculo llegamos ya al final de la serie, y esperemos\
  \ os hayan gustado nuestras recomendaciones de este a\xF1o.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/LoopHero/ss01.webp
joomla_id: 1519
joomla_url: nuestros-juegos-de-2022-tercera-parte
layout: post
tags:
- steam
- goty
- proton
- steam-deck
- beamng-drive
- selaco
- loop-hero
- ara-fell
- path-of-exile
- cyberpunk-2077
title: Nuestros Juegos de 2022 (Tercera Parte)
---
Y con este artículo llegamos ya al final de la serie, y esperemos os hayan gustado nuestras recomendaciones de este año.


Pato
----


Este año, (al igual que el anterior) no he podido dedicar todo el tiempo que quisiera al vicio videojueguil, pero aún así intento sacar minutillos al final del día y antes de que se me caigan los párpados al suelo para lanzar algún juego que me saque de la monotonía diaria. Entre mis destacados de este año está [Loop Hero](https://store.steampowered.com/app/1282730/Loop_Hero/), un juego de corte indie nativo para Linux que puedes dejar de fondo mientras te quedas durmiendo. Aparte de eso es un juego táctico con tintes roguelike... bueno, es difícil de clasificar.


![ss01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/LoopHero/ss01.webp)


 Otra de las cosas que ha cambiado mis hábitos videojueguiles ha sido la **Steam Deck**. Un cacharro maravilloso que me ha permitido revisitar el fondo de armario de todo lo que tengo sin jugar, o medio abandonado. Últimamente ando retomando mi vena rolera en Steam Deck con [Ara Fell](https://store.steampowered.com/app/440540/Ara_Fell_Enhanced_Edition/), un juego de rol nativo que funciona a las mil maravillas en la portatil y que me está retrotrayendo a tiempos pasados.


![Ara Fell](https://cdn.cloudflare.steamstatic.com/steam/apps/440540/ss_a7f8bbf3e7509bc8426fd94873daedfc19141899.jpg)


Por último, y siguiendo con el Rol, como buen fan de Diablo II que fui en su día, he retomado también un juego que se puede considerar su heredero espiritual. Estoy hablando de [Path of Exile](https://store.steampowered.com/app/238960/Path_of_Exile/), un **ARPG** de la vieja escuela que aunque ya tiene unos años encima sigue manteniendo la esencia y además está gratis. Como buen ARPG necesita teclado y ratón, por lo que lo ejecuto en el PC de sobremesa, y con Proton va de fábula.


![Path of Exile](https://cdn.cloudflare.steamstatic.com/steam/apps/238960/ss_26a455ad97d62bc249ea4c05ce0a0a9459874509.jpg)


Leillo
------


Sinceramente este año por determinadas razones no he podido jugar todo lo que debería para poder tener más donde elegir, pero sin duda para mi el mejor ha sido un **título absolutamente mainstream** que llevaba años esperando, y que **debería haber sido el juego del año pasado**, pero que por culpa de las prisas y multitud de bugs salió a destiempo y con unas críticas que lo lastraron más que justificadas. Ese juego es por supuesto [Cyberpunk 2077](https://www.cyberpunk.net/es/es/) y este año ha alcanzado por fin la dimensión y repercusión que se merece. Tengo que decir también que en parte es gracias a mi actual configuración que me ha permitido disfrutarlo con los gráficos a tope y con FSR, pues con la del año pasado tenía serios problemas, y no por potencia, sino por el mal funcionamiento de VKD3D con mi anterior gráfica. Actualmente y **gracias a Proton el juego funciona perfecto** y es una **auténtica delicia disfrutar de las calles y parajes de Night City**, de las que uno no se cansa nunca. Llevo más de 175 horas y actualmente estoy jugando con otro personaje, y aun así sigo encontrando cosas nuevas e interesantes, y reviviendo otras absolutamente épicas con otra "versión" de "V".


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/DP_tBTj72h4" title="YouTube video player" width="780"></iframe></div>


Otro juego que me ha cautivado es un título que lleva años en Early Access pero que poco a poco va tomando forma y este año estrenó versión nativa (experimental). Se trata de [BeamNG.Drive](https://www.beamng.com/game/), de los antiguos creadores de Rigs of Rods (del que [os acabamos de hablar]({{ "/posts/rigs-of-rods-cierra-el-ano-con-una-nueva-version" | absolute_url }})). Como este se trata de un juego en el cual **podremos experimentar con las físicas, incluidas las colisiones, de todo tipo de vehículos con ruedas**, y donde están haciendo un trabajo soberbio. El juego no se ejecuta nativamente desde dentro de Steam, sino que hay que hacerlo directamente en la carpeta del juego en su ejecutable, y aunque como os digo se trata de una **versión experimental**, no noto grandes diferencias con la versión de Proton. La primera versión tenía algunos errores conocidos como la falta de sonido o el no guardado de las opciones gráficas, pero estas ya han sido subsanadas, y ahora ya es muy disfrutable.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/CwiC1YoTriQ" title="YouTube video player" width="780"></iframe></div>


En cuanto a lo que espero del año que viene, al igual que **SonLink**, estoy muy pendiente de [Selaco](https://store.steampowered.com/app/1592280/Selaco/), que todo lo que he visto hasta ahora me parece increíble, y por supuesto la **nueva versión 2.3 definitiva** (actualmente está en su [primera beta]({{ "/posts/ya-esta-aqui-la-primera-beta-de-la-nueva-version-de-speed-dreams" | absolute_url }})) de [Speed Dreams](https://www.speed-dreams.net), proyecto de **Software Libre** con el que colaboramos ambos.

¿Qué te parecen los videojuegos que han escogido nuestros colaboradores? Cuéntanos cuáles son los videojuegos a los que más caña has dado durante este 2022 en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

