---
author: leillo1975
category: "Acci\xF3n"
date: 2018-11-20 14:21:32
excerpt: "<p>Como era de esperar, @feralgames ser\xE1n los encargados del port</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7c1e447b255bbf211e80f00dade07568.webp
joomla_id: 905
joomla_url: shadow-of-the-tomb-raider-llegara-a-linux-steamos-el-proximo-ano
layout: post
tags:
- feral-interactive
- square-enix
- port
- shadow-of-the-tomb-raider
- eidos
title: "\"Shadow of the Tomb Raider\" llegar\xE1 a Linux/SteamOS en 2019."
---
Como era de esperar, @feralgames serán los encargados del port

En estos tiempos inciertos para las compañías que se dedican a portar títulos a nuestro sistema, da gusto ver como una de las más destacadas sigue produciendo espectación con sus anuncios. En este caso, los chicos de la Británica **Feral Interactive** acaban de anunciar a través de sus redes sociales y listas de correo la llegada de un juego que todos llevábamos presagiando y esperando jugar. Por supuesto en esta ocasión se trata de "Shadow of Tomb Raider", que llegará el año que viene después del éxito de "Tomb Raider" (v.2013) y "Rise of Tomb Raider". Podeis ver el tweet dándonos la noticia:



> 
> Shadow of the Tomb Raider coming to macOS and Linux in 2019.  
>   
> Next year, uncover the final piece of the puzzle and experience the conclusion to Lara Croft's thrilling origin story.<https://t.co/tNHdsIb3GP> [pic.twitter.com/NIV223pbzN](https://t.co/NIV223pbzN)
> 
> 
> — Feral Interactive (@feralgames) [November 20, 2018](https://twitter.com/feralgames/status/1064888121504997377?ref_src=twsrc%5Etfw)


  





El juego fué originalmente **publicado por Square Enix** y **desarrollado por Eidos-Montreal**, y es la culminación de la historia del origen de Lara Croft:



> 
> *"En Shadow of the Tomb Raider, Lara debe dominar una vibrante jungla, superar tumbas desafiantes y perseverar en su momento más oscuro. Mientras corre para salvar al mundo de un apocalipsis maya, Lara será forjada en el Tomb Raider® que está destinada a ser.*
> 
> 
> *La búsqueda de Lara la llevará desde el bullicioso pueblo mexicano de Cozumel hasta el oscuro corazón de la selva peruana donde la ciudad oculta de Paititi la espera. Navegará por complejas cavernas submarinas, atravesará paisajes espectaculares y descubrirá tumbas llenas de trampas.*
> 
> 
> *Superada en número y en armas por la Trinidad, Lara debe convertirse en una con la jungla e infundir miedo en sus enemigos usando el barro como camuflaje, desapareciendo en las enredaderas de la jungla o golpeando desde arriba mientras está posada en el dosel."*
> 
> 
> 


Por ahora no tenemos más datos sobre la fecha de salida, si usará Vulkan (eso seguro) y por supuesto sus requisitos mínimos, pero en cuanto vayamos sabiendo más iremos actualizando la información sobre este título. Por aquí ya se nos está haciendo la boca agua solo en pensar en jugarlo. Os dejamos con el emocionante trailer del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/cPr3b8U7WeA" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Esperabais que se portase este título? ¿Qué os parecen los anteriores Tomb Raider? Déjanos tu opinión en los comentarios o charla con nosotros en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

