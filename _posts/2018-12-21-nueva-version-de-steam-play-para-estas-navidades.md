---
author: leillo1975
category: Software
date: 2018-12-21 22:40:09
excerpt: "<p>Proton alcanza la versi\xF3n 3.16-6 Beta</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c541f0d622d8e202dd3df852fd92a95d.webp
joomla_id: 935
joomla_url: nueva-version-de-steam-play-para-estas-navidades
layout: post
tags:
- steam
- valve
- steam-play
- dxvk
- proton
title: "Nueva versi\xF3n de Steam Play para estas navidades."
---
Proton alcanza la versión 3.16-6 Beta

Para que luego hablen del "Valve Time".... hace unos 10 días [os informábamos](index.php/homepage/generos/software/12-software/1051-steam-play-se-actualiza-de-nuevo-con-la-inclusion-de-faudio) de una nueva versión cuya principal mejora era la inclusión de FAudio, y hace un momentito de nada **Pierre-Loup A. Griffais** ([@**Plagman2**](https://twitter.com/Plagman2)) nos dejaba la lista de cambios de la 3.16-6 Beta en su cuenta de Twitter:  
  




> 
> One more and final Proton 3.16 update for 2018! See you in 2019! [pic.twitter.com/MxzkbfYI4F](https://t.co/MxzkbfYI4F)
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [21 de diciembre de 2018](https://twitter.com/Plagman2/status/1076238550608171008?ref_src=twsrc%5Etfw)







Este "[changelog](https://github.com/ValveSoftware/Proton/wiki/Changelog)" traducido sería algo así como: 


* **Soporte para gnutls** >= 3.0, que **debería solucionar muchos problemas de red** con los juegos. Tened en cuenta que el "runtime" de Steam aún no lo ha incluido, por lo que su distribución tendrá que proporcionárselo. Para los usuarios en distribuciones que no proporcionen esto, Steam Play seguirá utilizando la librería del runtime de Steam como antes. Cuando el runtime de Steam se actualice, estará disponible para todos los usuarios. En nuestras pruebas, vimos mejoras en Eve Online, DOOM (2016) multijugador y HELLDIVERS.
* **Otros arreglos de red**, también. En particular, Hitman 2 y Metal Gear Solid 5 deberían estar trabajando ahora en línea.
* **DXVK se actualiza a 0,94**. Además de las notas de la versión DXVK, Dishonored 2 y Middle-earth: Shadow of War parece estar trabajando con esta versión.
* Soporte configurable para **forzar el modo LARGE_ADDRESS_AWARE**. Esto puede ayudar a algunos juegos que se quedan sin memoria cuando se ejecutan en Steam Play, por ejemplo Bayonetta.
* Nuevas **mejoras en FAudio**.


En cuanto este último apartado yo he podido comprobar que ahora **Fallout 4 ya no se congela al inicio**, y por informaciones que leímos en redes sociales **Skyrim tampoco**. También es importante conocer los [cambios](https://twitter.com/flibitijibibo/status/1076239409408868354) que ha realizado **Ethan Lee** ([@**flibitijibibo**](https://twitter.com/flibitijibibo)) en **FAudio**:


* Soporta PCM de 24 bits
* Corregir la clasificación de la etapa de procesamiento de submezcla
* Retroceder al silencio cuando la AMM no tiene apoyo
* Un montón de correcciones para XACT file I/O
* Un montón de correcciones para el procesamiento de la cadena de efectos


Bueno, pues no me entretengo más que voy a probar las novedades. Vosotros podeis hacer lo mismo y dejar vuestros tests e impresiones en los comentarios, o charlar sobre ello en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

