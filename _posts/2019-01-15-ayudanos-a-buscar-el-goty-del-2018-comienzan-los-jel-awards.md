---
author: Serjor
category: Web
date: 2019-01-15 20:39:19
excerpt: "<p>La b\xFAsqueda del mejor juego del 2018 ha dado comienzo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/823d7f7cd8e199f1a5c37ae82ff6053d.webp
joomla_id: 950
joomla_url: ayudanos-a-buscar-el-goty-del-2018-comienzan-los-jel-awards
layout: post
tags:
- goty
- jel-awards
- '2018'
title: "Ay\xFAdanos a buscar el GOTY del 2018: Comienzan los JEL Awards"
---
La búsqueda del mejor juego del 2018 ha dado comienzo

Con el 2019 ya en marcha toca mirar al pasado y pararnos a pensar en todo lo que el 2018 nos ha traído a nuestras distros linuxeras preferidas, por eso en jugandoenlinux hemos pensando en buscar esos juegos que durante el año pasado nos han hecho estar pegados a las pantallas de nuestros ordenadores más horas de las que pensábamos.


Para ello queremos contar con vuestra colaboración, y como en muchas democracias, buscaremos al ganador a través de dos rondas. En esta primera vuelta, que termina el 31 de enero a las 23:59, os pedimos que nos digáis cuales han sido vuestros juegos preferidos del 2018. Con todos los juegos que consideréis que deben entrar en la lucha por el juego del año haremos una nueva encuesta en la que tocará escoger el mejor juego libre, el mejor juego privativo, y el GOTY, el juego del año (libre o privativo), ese que supera todas las expectativas.


Como en esta primera vuelta tenemos el reto de no añadir dos veces el mismo juego de manera diferente, os pedimos por favor que cuando vayáis a añadir un juego, cojáis el nombre oficial de la web y hagáis copy/paste. Intentaremos normalizar los nombres de los juegos y eliminar duplicados, pero como indicamos en el ejemplo de la encuesta, 0 A.D. se puede ver escrito de todas las maneras posibles, así que en aras de que los juegos por los que apostéis tengan más opciones, cuanto menos riesgo de duplicados haya, mejor para ellos.


Podéis encontrar la encuesta de la primera vuelta en <https://jugandoenlinux.limequery.org/362872>, donde os pedimos dos juegos libres y dos juegos privativos. En principio lo ideal es que sean juegos del 2018, aunque no vamos a ser muy mirados con ese tema, sobretodo porque en los juegos libres la norma es ir actualizando el juego constantemente, aunque lo dicho, si os centráis en juegos que hayan tenido una versión nueva durante el 2018 o hayan sido publicados el año pasado, la encuesta reflejará más qué nos ha dejado el año pasado.


Dato anecdótico. Una de las primeras opciones que barajábamos para los juegos privativos era extraer de Steam y otras plataformas los juegos publicados para GNU/Linux durante el año pasado, pero cuando vimos que solamente en Steam había más de 1200 juegos, el número nos pareció tan desproporcionado que vimos mejor preguntaros cuales son vuestras elecciones particulares.


¡¡A votar!!


Si tienes alguna duda, o quieres influir en las propuestas de la gente para que salga tu juego preferido, escribe un comentario, o pásate por los canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

