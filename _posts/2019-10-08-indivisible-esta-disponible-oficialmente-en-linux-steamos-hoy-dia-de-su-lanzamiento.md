---
author: Pato
category: Rol
date: 2019-10-08 16:07:12
excerpt: "<p>De los creadores de Skullgirls, @LabZeroGames nos ofrece plataformas,\
  \ rol y acci\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/09b87d8495c8cf32b64735c816312dfe.webp
joomla_id: 1116
joomla_url: indivisible-esta-disponible-oficialmente-en-linux-steamos-hoy-dia-de-su-lanzamiento
layout: post
tags:
- accion
- indie
- plataformas
- rol
title: "Indivisible est\xE1 disponible oficialmente en Linux/SteamOS hoy d\xEDa de\
  \ su lanzamiento"
---
De los creadores de Skullgirls, @LabZeroGames nos ofrece plataformas, rol y acción

Tal y como [anunciamos](https://jugandoenlinux.com/index.php/homepage/generos/accion/5-accion/1219-indivisible-anuncia-su-lanzamiento-para-octubre-y-ya-se-puede-pre-comprar), los chicos de Lab Zero Games han cumplido su palabra y ya tenemos disponible en nuestro sistema favorito lo nuevo de los creadores de Skullgirls, llamado "**Indivisible**".


*"Indivisible es un juego de plataformas RPG de acción dibujado a mano de Lab Zero, creadores de la aclamada Skullgirls. Ambientada en un enorme mundo de fantasía, Indivisible cuenta la historia de Ajna, una niña valiente con una racha rebelde que se embarca en una búsqueda para salvar todo lo que sabe de la destrucción."*


*<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/ndzu-A4NGLs" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>*
Indivisible nos ofrece un destacable apartado artístico con dibujos y animaciones hechos a mano con mecánicas de combate en tiempo real. Nos veremos envueltos en un mundo de fantasía con seres inspirados en diferentes mitologías y donde la narrativa tendrá mucha importancia.
*Envuélvete de este mundo fantástico con docenas de personajes con los cuales puedes jugar, una experiencia de narración profunda, sencillo de jugar pero difícil de dominar.*
 
En cuanto a los requisitos, son:
**MÍNIMO:**  

+ **SO:** Ubuntu 18.04
+ **Procesador:** Intel Core i3-2100
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** OpenGL 3.3 Core
+ **Almacenamiento:** 6 GB de espacio disponible
+ **Notas adicionales:** 8GB de swap se requieren para jugar con 8GB of RAM


Curiosa nota adicional, requiriendo una cantidad de swap. Si quieres saber más o comprar Indivisible, tienes toda la información en su [página web oficial](https://indivisiblegame.com/es/), o en su [página de Steam](https://store.steampowered.com/search/?snr=1_4_4__12&term=indivisible).


