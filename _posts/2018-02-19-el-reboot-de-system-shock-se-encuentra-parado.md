---
author: leillo1975
category: "Acci\xF3n"
date: 2018-02-19 15:33:56
excerpt: "<p>El desarrollo del juego est\xE1 pasando por dificultades.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6964130a6e5a2ce8c5a7fab6251afd2e.webp
joomla_id: 651
joomla_url: el-reboot-de-system-shock-se-encuentra-parado
layout: post
tags:
- system-shock
- nightdive-studios
- reboot
title: El reboot de System Shock se encuentra parado.
---
El desarrollo del juego está pasando por dificultades.

Cuando va a pasar mas de  un año y medio desde que tuvimos conocimeinto de este proyecto, recibimos la inquietante noticia de que [Nightdive Studios](http://www.nightdivestudios.com/) está atravesando dificultades con el desarrollo de System Shock. Según lo transmitido por su CEO, Stephen Kick en un mensaje, el proyecto en estos momentos está pausado. Conocíamos esta noticia gracias a twitter:



> 
> Update #39: Sometimes You Need To Take a Step Back In Order To Take Two Steps Forward <https://t.co/wu2XhzV99E>
> 
> 
> — Nightdive Studios (@NightdiveStudio) [16 de febrero de 2018](https://twitter.com/NightdiveStudio/status/964573994509742081?ref_src=twsrc%5Etfw)



 Por lo que comenta en la actualización del estado del proyecto en [Kickstarter](https://www.kickstarter.com/projects/1598858095/system-shock/posts/2115044), **el proyecto necesita dar un paso hacía atrás para dar dos hacia adelante**. A pesar de que en su momento recaudaron la nada desdeñable cifra de 1.3 millones de dólares, las razones por la que el proyecto está actualmente en StandBy son varias, entre ellas el **cambio de motor gráfico de Unity a Unreal Engine 4**, el **crecimiento desmedido del proyecto**, pasando de ser un remaster a un juego completamente nuevo, la **contratación de nuevo personal,** los **problemas para obtener financiación extra**... La intención de la compañía es evaluar de nuevo el camino a seguir para poder completar el juego.


Una decisión que realmente nos deja a todos bastante preocupados, a pesar de que recalcan que no es una cancelación sinó una pausa. Como sabeis, System Shock es un juego ciberpunk de **acción con tintes de rol** ambientando en una estación espacial. Lanzadas la primera y segunda parte en la década de los 90, los juegos se convirtieron rapidamente en clásicos y pasaron a ser referentes de otros grandes juegos como **Half Life, Deus Ex o Bioshock**.  Esperemos que estas dificultades por las que están pasando el desarrollo no hagan llegar al proyecto a una via muerta y pronto tengamos buenas noticias que contaros. Os dejamos con el último video publicado por el estudio:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/gt9I-NfH81M" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Conoces la saga System Shock? ¿Crees que se superarán los probelmas en Nightdive Studios? Deja tus impresiones en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

