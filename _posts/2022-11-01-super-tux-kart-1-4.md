---
author: Son Link
category: Carreras
date: 2022-11-01 16:57:14
excerpt: "<p>Despu\xE9s de mucho tiempo tenemos nueva versi\xF3n de este popular juego\
  \ de carreras de karts de c\xF3digo abierto</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperTuxKart/stk-1.4.webp
joomla_id: 1503
joomla_url: super-tux-kart-1-4
layout: post
tags:
- carreras
- open-source
- karts
- supertuxkart
title: SuperTuxKart 1.4
---
Después de mucho tiempo tenemos nueva versión de este popular juego de carreras de karts de código abierto


Después de meses de desarrollo se ha publicado una nueva versión mayor de **Super Tux Kart**, un clon de código abierto del popular **Super Mario Kart**, pero cambiado a los populares personajes de **Nintendo** por **Tux** y otras mascotas de software libre, como las de **GNU**, **KDE** y **Firefox**, ademas de poder descargar otros personajes y pistas creados por la comunidad desde el propio juego.


Ademas cuenta con un modo para jugar al fútbol con los karts, aunque no te encontraras con algo parecido a **Rocket League**.


Un juego ideal para jugar con la familia, y sobre todo, picarse con amigos/as, familiares, etc, ya sea en partidas locales o a través de Internet.


Los cambios que trae esta nueva versión (sin contar todas las pequeñas correcciones y mejoras) son los siguientes:


#### General:


* Modo de prueba de vuelta
* Arreglado el powerup del paracaídas
* Arreglado el giroscopio al conducir en la superficie de conducción de la pared
* Evitar la activación de otras líneas de gol cuando el gol ya se ha marcado.


#### Gráficos:


* Animación de objetos y estrellas
* Optimización de los niveles de detalle
* Implementado correctamente el soporte **HiDPI** en SDL2
* Beta del renderizador **Vulkan**
* Hacer que las partículas del cielo caigan siempre verticalmente


#### Pistas y modelos:


* Actualizado **Konqi** (la mascota de **KDE**)
* Nuevo kart de **Godette** (Mascota de **Godot** fruto de una broma por el April's Fool del 2021)
* Actualizadas **Isla de Batalla** y **Cueva X**
* Arreglado el muro invisible roto en el **Abismo Antediluviano**
* Nuevas texturas en **Shifting Sands**
* Equilibradas las posiciones de salida en todos los campos de fútbol oficiales


####  Red:


* Añadida la búsqueda de pistas a la pantalla de pistas de la red
* Ahora se puede configurar el límite de jugadores en el juego
* Permitir el uso de karts descargados (misma caja de impacto y tipo de kart que en el juego local)


#### Interfaz de usuario:


* Añadir dificultades de repetición de fantasmas en el lado izquierdo


[Aquí tenéis](https://github.com/supertuxkart/stk-code/blob/1.4/CHANGELOG.md#supertuxkart-14-31-october-2022) la lista de cambios con los nombres de los desarrolladores que ha hecho cada cambio.


Y hasta aquí todo. Si buscas gente para jugar, entra a nuestro grupo de **Telegram** o a la sala **A Jugar** en **Matrix** y pregunta ;)

