---
author: P_Vader
category: Entrevistas
date: 2021-09-11 09:10:25
excerpt: "<p>Ante la salida de la versi\xF3n 0.11 de <span class=\"attribution txt-mute\
  \ txt-sub-antialiased txt-ellipsis vertical-align--baseline\">@velorenproject</span>,\
  \ aprovechamos para charlar con uno de sus desarrolladores principales.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Veloren/veloren_portada2_resize.webp
joomla_id: 1355
joomla_url: entrevista-con-forest-anderson-de-veloren
layout: post
tags:
- multijugador
- rpg
- entrevista
- rust
- veloren
title: Entrevista con Forest Anderson de Veloren
---
Ante la salida de la versión 0.11 de @velorenproject, aprovechamos para charlar con uno de sus desarrolladores principales.


Como todos sabéis soy un enamorado de [Veloren](https://veloren.net/). Me parece un proyecto de juego con un recorrido fascinante, muy bien organizado y un futuro prometedor. Ante el lanzamiento de la versión 0.11 hemos charlado con uno de sus desarrolladores principales, Forest Andeson que ademas es el encargado de darle voz al desarrollo del proyecto en el blog ["This week in Veloren"](https://veloren.net/devblogs/). Esta es una traducción de la entrevista que mantuvimos:




---


**JEL: Hola, muchas gracias por dedicarnos tu tiempo para responder a nuestras preguntas, en primer lugar, ¿puedes decirme qué es Veloren?**


**Forest**: ¡Oye, gracias por la oportunidad! Veloren es un juego de rol multijugador de mundo abierto y de código abierto. Tiene una estética similar a Minecraft, con aspecto de voxel. Veloren se inspira originalmente en Cube World, por lo que nos centramos más en la exploración y el combate que en la construcción. ¡Puedes formar equipo con tus amigos y conquistar mazmorras, o subirte a un dirigible para dar un paseo por la comarca! Estamos a punto de lanzar la versión 0.11, que agrega muchos monstruos nuevos, formas de luchar y mucho más.


 


**JEL: ¿Cuándo comenzaste en este proyecto? ¿Quién tuvo la idea original?**


**Forest**: Personalmente me uní al proyecto a principios de 2019, pero el proyecto en sí se inició en mayo de 2018. Zesterer, el fundador del proyecto, sugirió en Reddit que un juego software libre inspirado en Cube World no sería demasiado difícil de hacerlo como proyecto comunitario. Varias personas estaban interesadas en esta perspectiva y comenzó el desarrollo.


Cuando me uní al proyecto, el equipo estaba comenzando a realizar la transición a un nuevo motor desde cero. Esto me dio una gran oportunidad para integrarme con algunas de las partes de creación y pruebas del juego, además de resucitar el blog. Para mí el blog fue una forma de poder hablar con los desarrolladores principales sobre en qué han estado trabajando. Sin embargo para el proyecto ha permitido que muchas personas vean cómo trascurre el desarrollo cada semana.


 


**JEL: ¿En qué manera Cube World o Legend of Zelda inspiraron a Veloren?**


**Forest**: El proyecto comenzó originalmente porque un grupo de personas que les gustaba Cube World quería ver hasta donde podrían llegar con el desarrollo libremente. Nos encantó el estilo artístico, las experiencias multijugador y la capacidad de explorar un mundo vibrante. Entonces, aunque fue un gran punto de partida, comenzamos a encontrar nuestro propio camino diferente de Cube World con bastante rapidez. Nuestro estilo visual combina el realismo con el mundo de los vóxeles, por lo que, aunque todo está hecho de cubos, todavía tenemos cielo, agua y terreno realistas.


 


![Veloren screenshot](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Veloren/screenshot_1625914370109.webp)


**JEL: ¿Cuántas personas hay en tu equipo de desarrollo? ¿Cuáles son sus roles? ¿Cómo os organizáis?**


**Forest**: Durante el desarrollo de Veloren, hemos visto más de 200 colaboradores únicos. Podemos cuantificar esto bastante bien a partir de quién ha contribuido en nuestro repositorio de código, pero también hay muchas personas que han contribuido de otras formas. Desde asistir a reuniones de diseño hasta crear recursos artísticos como sonido y modelos, muchas personas han puesto una parte de sí mismas en Veloren.


Sin embargo, para el equipo central, tenemos unas 15 personas. Tenemos varios grupos de trabajo que se enfocan en diferentes partes del proyecto, como combate, diseño y renderizado. Cada grupo tiene un líder que se coordina más con los demás líderes del equipo. Con esta estructura, tenemos suficiente comunicación para permitirnos movernos con bastante rapidez. Personalmente, soy el líder del metagrupo de trabajo y me concentro en elementos como el blog, nuestra infraestructura en la nube y el alcance comunitario.


Internamente, tratamos de tener la menor cantidad de burocracia posible. Tenemos reuniones semanales que duran alrededor de una hora, pero durante el resto de la semana, ponemos a las personas por encima de la metodología fija. Como no hemos establecido horarios de trabajo, las personas pueden chatear tanto o tan poco como quieran en nuestro Discord.


 


**JEL: Ya sabes que Linux ha sido una promesa para la industria de los videojuegos desde hace algunos años, especialmente desde que Valve se interesó mucho en su desarrollo. ¿Qué opinas de Linux como plataforma de juegos?**


**Forest**: Para Veloren, el desarrollo para Linux es una prioridad de primera clase. La mayoría de los desarrolladores usan de alguna forma \*nix como su entorno de desarrollo principal, y lanzamos el juego a través de varios administradores de paquetes de Linux.


En general para la industria de los juegos, Linux como sistema operativo se ha convertido en casi un estandar para el que lanzar. Wine y Proton permiten a los jugadores jugar más juegos que nunca y Steam está dando grandes pasos con Steam Deck utilizando un sistema operativo basado en Arch.No obstante para los desarrolladores de Rust, el desarrollo para Linux es trivial. De hecho, en algunos casos, es más fácil de compilar que en Windows.


 


![screenshot 1631043276228](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Veloren/screenshot_1631043276228.webp)


**JEL: Su equipo usa Linux para algunas partes del desarrollo de juegos ¿Y alguna herramienta de código abierto, por ejemplo, blender, krita, etc.?**


 **Forest**: Como mencioné anteriormente, la mayoría de nuestros desarrolladores usan Linux. Esto se aplica a todo lo que forme parte de la canalización; desde la codificación del juego hasta las pruebas en Gitlab, los servidores del juego y mucho más. Sin embargo, permitimos que los artistas usen lo que les resulte más cómodo para su trabajo. Por ejemplo, todos nuestros modelos de voxel se crean con MagicaVoxel, que es un software gratuito pero de código cerrado. Cuando se trata de trabajos de audio, también se utilizan varias herramientas. Para las animaciones, hemos discutido el uso de Blender, sin embargo para nuestras necesidades es más fácil escribirlas en el motor.


 


**JEL: ¿Por qué rust y lo consideras un lenguaje que ayuda a desarrollar videojuegos en comparación con otros más consolidados?**


**Forest**: Originalmente, cuando se inició el proyecto en 2018, se eligió Rust como un lenguaje de bajo nivel que se entreveía muy prometedor para el desarrollo de juegos. Ofrecía muchas garantías en torno a la seguridad de la memoria que otros lenguajes de bajo nivel como C ++ no ofrecían. Resulta que esta inversión en Rust valió la pena en muchas ocasiones. La comunidad de desarrollo de juegos en Rust ha crecido significativamente y con ella están surgiendo muchas nuevas crates (bibliotecas de Rust).


Estamos trabajando en estrecha colaboración con las tecnologías gráficas de vanguardia de gfx-rs para nuestra canalización de renderizado. Adoptamos en gran medida el paradigma Entity Component System (ECS), que permite que el multijugador de Veloren se escale muy bien en servidores de varios núcleos.


Hoy por hoy existen varios grandes motores integrados en Rust que permiten a los desarrolladores lanzar juegos en Windows, Mac, Linux y dispositivos móviles. En el futuro, también esperamos ver un impulso hacia las consolas. Aunque hace varios años el espacio de desarrollo de juegos era bastante estéril en Rust, ahora está mucho más animado. Rust ha permitido a nuestro equipo trabajar a velocidades vertiginosas sin casi preocuparte de romper el código de otro.


 


![screenshot 1629679274776](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Veloren/screenshot_1631236823910.webp)


**JEL: La pregunta del millón ¿cuándo podemos esperar una versión 1.0? ¿Tiene una hoja de ruta para ese fin o simplemente os dejais llevar?**


 **Forest**: ¿No nos gustaría saberlo todos? Para nosotros dado que Veloren siempre ha sido un proyecto como hobby, no sentimos la necesidad de tener plazos estrictos sobre cuándo debería lanzarse la versión 1.0. En cambio somos conscientes de los elementos que deberían completarse para que empecemos a pensar en ello:


1. Historia y *quest*:  
 En Veloren puedes crear muchas experiencias con otros mientras juegas en modo multijugador. Sin embargo todavía existe una gran necesidad de que el juego en sí te dé tareas que hacer. Hay muchos sistemas fundamentales para crear un mundo vivo real, pero los jugadores no interactúan con él estrechamente. Hay vínculos entre las ciudades y la historia que se ha generado, pero esto no se transmite al jugador a través de algún tipo de sistema de *quest*. Recientemente se ha trabajado para crear un sistema de "sitios" que permite generar ubicaciones por todo el mundo de forma dinámica. Esto es necesario para un sistema de *quest* que pueda ser integrado con el mundo.
2. Una organización formada para el proyecto:  
Veloren tiene como objetivo convertirse en una organización sin ánimo de lucro. Esto permitirá que Veloren se lance en Steam y sea reconocido como una organización adecuada. También le dará estructura al proyecto. Hemos estado trabajando bien como un equipo que puede cambiar a medida que el proyecto crece, pero al convertirnos en una organización legal se añadirán más garantías, como reuniones periódicas de la junta.
3. Una comunidad de modding fuerte:  
 Aunque estamos creando un gran juego base, queremos que los jugadores puedan crear su propio contenido. Esto puede incluir modelos en el juego, diferentes conjuntos de reglas para jugar, diferentes interfaces de usuario o cualquier cosa que la gente pueda imaginar. Estamos trabajando para crear esto con un sistema de complementos. Los jugadores podrán descargar complementos desde una fuente central y el juego interactuará con ellos.


 También hay muchas otras cosas, sin embargo, si estos grandes problemas se resuelven, nos daría una perspectiva mucho mejor de cómo se vería la versión 1.0.


 


 **JEL: Sé que te esfuerzas en dar voz sobre el desarrollo de Veloren a través del blog "[This week in Veloren](https://veloren.net/devblogs/)", de hecho en casa seguimos ahí todas las novedades. También sé que tenéis un proyecto de financiación en O[pen Collective](https://opencollective.com/veloren). ¿Tenéis un plan de financiación y marketing al respecto? Teniendo en cuenta como el caso de éxito de Blender es un punto de referencia para muchos proyectos en la actualidad.**


**Forest**: Tenemos varias opiniones sobre este tema. En primer lugar, el juego debe ser siempre gratuito para que la gente lo juegue. También estamos muy en contra de la idea de explotar a los jugadores a través de microtransacciones y otros modelos de negocio de naturaleza similar.


Hemos examinado varios medios de ingresos: donaciones, lanzamientos en Steam y patrocinadores de servidores. Hasta ahora, solo hemos implementado donaciones a través de [Open Collective](https://opencollective.com/veloren). La cantidad mensual que ganamos, alrededor de 150 $ dolares americanos, es suficiente para mantener nuestros servidores en funcionamiento. En el futuro nuestro objetivo es convertirnos en una organización sin ánimo de lucro, lo que nos permitirá estudiar un lanzamiento apropiado en Steam.


 


![Veloren screenshot](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Veloren/screenshot_1629679274776.webp)


**JEL: Por último, me gustaría que nos dijeras si hay alguna forma de que los fans puedan colaborar o contribuir con Veloren.**


**Forest**: La mejor manera de involucrarse en el desarrollo es unirse a la comunidad [Discord](https://discord.gg/ecUxc9N) (también tenemos [Matrix](https://matrix.to/#/#veloren-space:fachschaften.org) y puente a IRC). Allí trabajamos para incorporar a personas que quieran ayudar con el proyecto, ya sean desarrolladores, artistas o cualquier otra cosa.


Hay muchos trabajos en los que es necesario trabajar, incluso para personas que no saben programar, ¡hay mucho que se puede hacer! Veloren está creado por una comunidad que quiere ver el juego creado, así que nos encanta cuando los fans pueden ayudar. Intentamos que el desarrollo sea lo más fácil de abordar y lo más abierto posible, ¡así que tal vez nos vemos allí!


 


**JEL: En nombre de JugandoEnLinux ha sido un placer entrevistarte. Felicidades por llevar Veloren a un nivel tan alto de calidad y éxito y le deseamos todo lo mejor para el futuro.**




---


 


Personalmente ha sido muy interesante conocer tantos detalles de este proyecto. ¿Qué os ha parecido a vosotros? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)


 


 

