---
author: leillo1975
category: Editorial
date: 2018-01-15 09:06:18
excerpt: "<p>Grandes t\xEDtulos con soporte que no est\xE1n en Steam.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6577c6b0fbc4ba9ae61ff6583dc67c84.webp
joomla_id: 606
joomla_url: por-que-id-software-por-que
layout: post
tags:
- steam
- vulkan
- valve
- id-software
- zenimax
- tim-besset
- john-carmack
title: "Por qu\xE9, ID Software, por qu\xE9...."
---
Grandes títulos con soporte que no están en Steam.

A veces los astros se conjuran, y aunque ya me he realizado esta pregunta multitud de veces, estos días ha vuelta a mi cabeza de nuevo, aunque no sin una razón. Si recordáis, hace poco publicábamos un artículo donde os animábamos a escuchar una extensa entrevista a **Tim Besset** que realizaba el Blog [Boiling Steam;](index.php/homepage/noticias/item/719-segun-tim-basset-el-port-de-street-fighter-v-no-esta-muerto) también en [GamingOnLinux](https://www.gamingonlinux.com/articles/playing-quake-4-on-linux-in-2018.11017/page=2#comments) se hablaba de Quake 4 y como ejecutarlo en la actualidad, en un artículo al estilo al que había realizado el año pasado [sobre Doom 3](https://www.gamingonlinux.com/articles/playing-doom-3-on-linux-in-2017.10561). Por último, cada cierto tiempo en nuestro grupo de Telegram se suele hablar de estos juegos.


El caso es que la mayor parte de los juegos desarrollados y editados por [ID Software](https://es.wikipedia.org/wiki/Id_Software#Videojuegos) previos a RAGE tenían una versión para GNU/Linux, y todo gracias al trabajo liderado por [Tim Besset](https://en.wikipedia.org/wiki/Timothee_Besset) ([@**TTimo**](https://twitter.com/TTimo)) , que entre otras cosas se encargó de portar todos estos grandes clásicos a nuestro sistema. En su bagaje encontramos títulos de la talla de **Return to Castle Wolfenstein, Wolfenstein: Enemy Territory, Quake III arena, Doom 3, Quake 4, Enemy Territory: Quake Wars o Quake Live**, y más actualmente la cooperación con [Ryan Gordon](https://en.wikipedia.org/wiki/Ryan_C._Gordon) ([@icculus](https://twitter.com/icculus/)) en el port de **Rocket League**. 


![CastleWolfenstein](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/IDSoftware/CastleWolfenstein.webp)


Como la mayoría de vosotros sabreis, estos juegos de la etapa de ID Software podemos encontrarlos en Steam **con soporte unicamente para Windows, algo incomprensible** para muchos usuarios. Hay que resaltar que a pesar de los años muchos de estos juegos, ahora clásicos, en su día revolucionarios; aun se pueden disfrutar enormemente y muchos de ellos aun lucen ahora muy buena factura (Doom 3 BFG o Quake 4). Esta situación provoca enojo en los usuarios del pingüino, ya que si indagamos  o estamos un poco metidos en el mundo de los juegos en GNU-Linux, veremos que el tema ha generado multitud de comentarios, artículos y sobre todo frustración.


Todo esta injusta y risible situación viene dada por la política de [Zenimax Media](https://es.wikipedia.org/wiki/ZeniMax_Media) con respecto a Linux. Según lo [manifestado](http://www.escapistmagazine.com/news/view/121945-John-Carmack-Argues-Against-Native-Linux-Games) por [John Carmack](https://es.wikipedia.org/wiki/John_Carmack) en el año 2013, esta compañía no tenía ninguna política de seguir apoyando los binarios no oficiales, y que su intención hacia linux continuaba con liberar sus motores gráficos como habían hecho hasta ahora. A su juicio, el juego en linux debe ir encaminado hacia la emulación a través de Wine. Estas palabras fueron desalentadoras y decepcionantes para todos aquellos fans que apreciaban el esfuerzo de la compañía por dar la oportunidad a los usuarios de GNU-Linux de disfrutar sus juegos, y por un tiempo John Carmack se convirtió en uno de los blancos de las iras de la comunidad. En la entrevista de Boiling Steam, Tim Besset aclaraba muchas de las claves sobre este abandono a los usuarios, y aseguraba que el personal de ID Software eran mayoritariamente usuarios de Windows, por lo que en parte era injusto el trato a John Carmack.


![doom3bfg](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/IDSoftware/doom3bfg.webp)


Cuesta entender que estando el trabajo hecho, y habiendo soporte por parte de Steam y GOG a la distribución de videojuegos en Linux, no se llegue a lanzar esta interesante parte del catálogo de ID Software. Las razones parecen más cercanas a criterios sumamente económicos y de estrategia comercial por parte de Zenimax. Pero, ¿realmente es tan difícil actualizar el soporte de esos títulos a los requisitos de software actuales de las distribuciones de GNU-Linux? Desde nuestro punto de vista, y con una pequeña inversión de tiempo y dinero, creemos que solo podrían ganar, puede que poco, pero a fin de cuentas ganar.


Recordemos que es posible jugar, por ejemplo, a Doom 3 y Quake 4 gracias a proyectos libres como [RBDOOM 3](index.php/foro/juegos-nativos-linux/14-jugar-de-forma-nativa-a-doom-3-bfg-edition-en-ubuntu) u otros, y quizás estas soluciones se podrían usar para "empaquetar" el juego para su distribución comercial. Muchos usuarios de GNU-Linux/SteamOS estarían interesados en adquirir y disfrutar estos títulos, aunque ya hayan pasado unos cuantos años. Los juegos están desarrollados, y el soporte ya lo estuvo en su día. Simplemente sería un trabajo de  actualización y distribución. Está claro que **la presión de la comunidad no ha impactado lo suficiente en sus desarrolladores y editores**, pero como en otras ocasiones comentamos, quizás un tercero tendría la suficiente fuerza para conseguir que se cambiase de parecer. Este tercero es quien todos estáis pensando, y se llama **Valve Software**, el que hasta ahora ha conseguido que disfrutemos de muchos y variados juegos en nuestro sistema favorito. Es fácil soñar, pero quien sabe, quizás algún día podamos volver a jugar natívamente estos obras de arte tan apetecibles en nuestros equipos, o quien sabe, incluso gracias a Vulkan, a DOOM 2016.


 


¿Has jugado a los clásicos de ID Software? ¿Comprarías estos títulos en la actualidad si tuviesen soporte? Dejanos tu opinión al respecto en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

