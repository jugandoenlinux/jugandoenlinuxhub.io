---
author: Pato
category: "Simulaci\xF3n"
date: 2020-04-09 17:41:51
excerpt: "<p>Phoronix ha hecho una interesante comparativa con una veintena de configuraciones\
  \ gr\xE1ficas</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/xplane11-1.webp
joomla_id: 1204
joomla_url: xplane-11-comienza-a-brillar-en-linux-con-su-implementacion-vulkan
layout: post
tags:
- simulador
- vulkan
- simulacion
- xplane-11
title: "Xplane 11 comienza a brillar en Linux con su implementaci\xF3n Vulkan"
---
Phoronix ha hecho una interesante comparativa con una veintena de configuraciones gráficas


 


Hace algún tiempo que hablamos de **Xplane 11,**uno de los simuladores aéreos más completos, ambiciosos y exigentes del mercado que corre de forma nativa en Linux, y su **intención de implementar la API gráfica Vulkan** para renderizar sus gráficos. Pues bien, tras lanzar una actualización del simulador donde implementan dicha API, en [Phoronix](https://www.phoronix.com/), la web del incansable Michael Larabel han hecho una comparativa del rendimiento de Xplane 11 con su plataforma de testeo Phoronix Test Suite.


En esta comparativa utilizan tanto el renderizado con **OpenGL/RadeonSI** como con **Vulkan + ACO**, tanto en tarjetas AMD como Nvidia y el resultado es bastante interesante.


Cuando la configuración gráfica es baja, con carga en la CPU y bajas resoluciones el desempeño del simulador es similar en ambas opciones, llegando incluso OpenGL a superar a Vulkan en muchos casos, pero la cosa se pone interesante cuando comenzamos a exigir resoluciones 2K y 4K con configuraciones gráficas altas y cargas sobre la GPU donde vemos que Vulkan toma ventaja en todas las pruebas, haciendo que el simulador sea jugable incluso con tarjetas gráficas modestas.


Si quieres ver la comparativa completa y sacar tus conclusiones, puedes visitar la [página de Phoronix en este enlace](https://www.phoronix.com/scan.php?page=article&item=xplane-11-vulkan&num=1).


En otro orden de cosas, el propio **Austin Meyer, CEO de Laminar Research** publicó un vídeo explicando la diferencia y el impacto que ha tenido la implementación de Vulkan en su simulador. Puedes verlo a continuación:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/UfC0ACDWfaQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


¿Qué te parece la evolución de Vulkan con los juegos? ¿Crees que Vulkan le sienta bien a simuladores como Xplane 11?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

