---
author: Pato
category: Aventuras
date: 2018-02-13 16:46:38
excerpt: "<p>Eastshade Studios confirma la versi\xF3n pero limitar\xE1 el soporte\
  \ oficial a SteamOS debido a experiencias previas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ff0158c2594917cd6a9c4e297e8a8d7c.webp
joomla_id: 643
joomla_url: eastshade-una-curiosa-aventura-de-exploracion-tendra-version-linux-steamos
layout: post
tags:
- indie
- aventura
- proximamente
title: "'Eastshade' una curiosa aventura de exploraci\xF3n tendr\xE1 versi\xF3n Linux/SteamOS"
---
Eastshade Studios confirma la versión pero limitará el soporte oficial a SteamOS debido a experiencias previas

Tenemos noticias de un curioso juego de exploración que tendrá soporte en Linux/SteamOS. Se trata de 'Eastshade', un juego de "exploración en un mundo abierto" o lo que otros han dado en llamar "simulador de paseos" que está siendo desarrollado por Eastshade Studios.


*Eres un pintor viajero, explorando la isla de Eastshade. Captura el mundo sobre lienzo usando tu caballete de artista. Habla con los habitantes para aprender sobre sus vidas. Haz amigos y ayuda a los que lo necesiten. Visita ciudades, escala cumbres, resuelve misterios y descubre lugares olvidados.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/FHPWlZYTNNg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Características:


- Compon pinturas en cualquier parte del mundo y ofrécelas a los personajes para obtener elementos, conocer y descubrir secretos.  
- Interactúa con los locales a través de conversaciones dinámicas.  
- Adquiere materiales y esquemas para superar obstáculos.  
- Las acciones y las decisiones de diálogo afectan a las interacciones y resultados futuros a medida que conoces nuevos personajes.


La historia será corta tal y como el estudio confirma. *Se trata de un juego relajante con mecánicas no violentas que podrás terminar de una sentada.*


Este mismo estudio es el responsable de otro "corto" título con "soporte no oficial" en Linux, '[Leavin Lindow](http://store.steampowered.com/app/585840/Leaving_Lyndow/)' del mismo estilo que este que nos ocupa y que aún se puede jugar accediendo a la rama Linux en las opciones de Steam del juego. Y es que el desarrollador terminó "desbordado" por los problemas para las distintas distribuciones en Linux, por lo que optó por retirar el logo de soporte oficial para nuestro sistema favorito.


Tras revisar algunos hilos en sus foros, parece que al fin han dado con la "clave" del soporte oficial para sus juegos: SteamOS. A la pregunta de un usuario sobre la posible versión de Eastshade en Linux, esto es lo que comenta confirmando el soporte:


*"Si! en SteamOS de todas formas. Esta es la distro en la que descargaré y testearé. Tal como lo entiendo, esto significa que debería funcionar bien también en muchas otras distros, pero esta es la única que voy a soportar oficialmente."*


Puedes ver el hilo y mostrar tu apoyo por el soporte [en este enlace](http://steamcommunity.com/app/715560/discussions/0/1700542332338236693/).


'Eastshade' [[web oficial](http://www.eastshade.com/)] no tiene aún una fecha de salida y al contrario que 'Leavin Lindow' probablemente no llegue traducido al español , pero si te van los juegos de exploración tranquilos y con gráficos realmente bellos este juego puede que te interese. Tienes toda la información en su [página de Steam](http://store.steampowered.com/app/715560/Eastshade/).

