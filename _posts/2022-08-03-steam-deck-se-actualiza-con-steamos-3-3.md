---
author: Pato
category: Hardware
date: 2022-08-03 16:08:28
excerpt: "<p>Nuevas opciones y caracter\xEDsticas que estaban siendo probadas en versiones\
  \ beta ahora pasan al canal estable</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/Steam_deck_control.webp
joomla_id: 1482
joomla_url: steam-deck-se-actualiza-con-steamos-3-3
layout: post
tags:
- steamos
- software
- steam-deck
title: Steam Deck se actualiza con SteamOS 3.3
---
Nuevas opciones y características que estaban siendo probadas en versiones beta ahora pasan al canal estable


Hace unas horas Valve ha publicado una [actualización tanto de sistema como del cliente de Steam](https://store.steampowered.com/news/app/1675200/view/3401924854795478414) para su consola Steam Deck con las siguientes novedades:


**General**


* Añadida página de logros superpuesta (mientras que en el juego presione el botón Steam)
* Se agregó la página Guías superpuesta (mientras que en el juego presione el botón Steam)
* Añadida notificación cuando la temperatura de la Steam Deck sale del rango de operación seguro
* Añadida una función de modo nocturno programada, que permite a los jugadores elegir si y cuándo les gustaría que el modo nocturno se active automáticamente
* Añadido un botón para borrar el texto ingresado en la barra de búsqueda
* El interruptor de brillo adaptativo ahora está activo nuevamente
* Corregida la notificación para reclamar recompensas digitales apareciendo sin cesar para algunos clientes
* Corregido el problema con los nombres de juego de longitud media en la superposición del menú principal que no se desplazaba correctamente
* Corregidos algunos problemas al reclamar recompensas digitales de Steam Deck
* Corregido el sonido para notificaciones de progreso de logros
* Corregidos los colores apagados en el cliente Remote Play cuando se juega con hosts específicos
* Corregida la ventana de inicio de sesión de Xbox para Flight Simulator y Halo Infinite no mostrando ciertos caracteres correctamente


**Steam Input**


* Añadidos botones de la Deck faltantes para las opciones habilitar Gyro y Button Chord
* Añadido soporte para iconos de menú virtual agrupados en el juego en la interfaz de usuario de la Deck
* Mejoras de rendimiento diversas


**Teclados**


* Añadido soporte para teclado chino simplificado, chino tradicional, japonés y coreano. Todavía estamos refinando estos teclados, proporcione comentarios en los foros.
* Añadido soporte de entrada IBus IME inicial en el escritorio para teclados chinos, japoneses y coreanos
* El teclado de modo de escritorio fijo a veces no se muestra ni se descarta
* Corregido el teclado en pantalla que aparece en el menú Steam o Menú de Acceso rápido
* Comportamiento actualizado del teclado para mejorar la escritura rápida en el panel táctil y la pantalla táctil. (presionar una tecla mientras sostiene otra tecla ahora confirmará la tecla retenida en lugar de esperar a que se suelte primero)
* Corregidos algunos problemas de estilo táctil con el teclado virtual


**Actualizaciones del sistema**


* Se agregó un nuevo selector de canal de actualización de software Steam Deck: ahora hay tres opciones:
* **Estable**: experiencia recomendada para la mayoría de los usuarios. Esta opción instalará el último cliente estable de Steam y SteamOS
* **Beta**: prueba para nuevas características de Steam. Actualizaciones con frecuencia. Esta opción instalará Steam Client Beta y el último SteamOS estable
* **Preview**: Pruebas para nuevas características de Steam y de nivel de sistema. Actualizaciones con frecuencia. Puedes encontrar problemas. Esta opción instalará Steam Client Beta y SteamOS Beta.
* Solo verás notas de parche para el canal de actualización que ha seleccionado.


 **Rendimiento / Estabilidad**


* Corregidos algunos problemas de rendimiento para los usuarios con muchas capturas de pantalla
* Corregidos varios bloqueos relacionados con la gestión de capturas de pantalla
* Corregidos varios bloqueos relacionados con atajos que no son de Steam
* Corregidos algunos juegos nativos de Linux que no se cerraban cuando se forzaba la salida a través de Steam
* Corregido el cierre incorrecto de Chrome flatpak cuando se cierra desde Steam
* Corregido un error en el que algunas aplicaciones de flatpak (como Edge) no podían cerrarse con éxito
* Corregido un problema de rendimiento con algunos juegos cuando la luz de fondo cambia de intensidad


**Modo escritorio**


* Actualización de Firefox para instalarse como Flatpak, en lugar de desde los repositorios del sistema operativo, para garantizar actualizaciones a tiempo
* Al lanzar por primera vez Firefox desde el escritorio ahora solicitará la instalación a través del Centro de software Discover, que manejará las actualizaciones a medida que se publiquen.
* Conexiones de red actualizadas creadas / editadas en el escritorio de forma predeterminada en todo el sistema, asegurando que estén disponibles en modo de juego
* Añadido el tema VGUI2 Classic Plasma Desktop
* Teclado virtual redimensionado en modo Escritorio a las dimensiones apropiadas
* Añadido soporte para las barras de arcade Qanba Obsidian y Qanba Dragon en modo Escritorio


**Modo Dock**


* Añadida una opción para escalar la interfaz de usuario de Steam Deck para pantallas externas
* Añadida una palanca para la escala automática de la interfaz de usuario de Steam Deck para pantallas externas
* Añadida la capacidad de ajustar la configuración de visualización de imágenes para pantallas externas que tienen problemas de sobreescaneo
* Corregido el panel al desconectarse del Dock poco después de reanudar desde la suspensión
* Corregida la luz de fondo del panel permanentemente encendida mientras esta en Dock


**Audio / Bluetooth**


* Corregida la selección de perfil Bluetooth fija que no se guarda al salir del modo Escritorio
* Corregida la sobrecarga de cancelación de eco de la CPU cuando no se usa el micrófono, mejorando el uso de energía en escenarios inactivos o casi inactivos
* Corregido el audio multicanal en pantallas externas
* Corregida la salida de audio en algunas tarjetas capturadoras
* Corregidos algunos casos de audio corrupto después de reanudar desde suspensión
* Corregida la salida de audio con algunos juegos de 32 bits que usan ALSA


 **Drivers / Firmware**


* Controlador gráfico actualizado con compatibilidad y correcciones de rendimiento
* Controlador inalámbrico actualizado con correcciones para problemas de desconexión WiFi en 5Ghz
* Utilidades de firmware del controlador actualizadas para admitir futuras revisiones de hardware del controlador


 


En otro orden de cosas, hace unos días Valve anunció que su cadena de suministro ha mejorado lo suficiente como para entregar todas las Steam Deck reservadas hasta la fecha dentro de este mismo año, por lo que si tienes reservada una ve echando un ojo a tu página de actualización del estado de tu pedido o a tu correo electrónico por que las fechas se están moviendo.


¿Qué te parecen las novedades de SteamOS 3.3? ¿Te han adelantado tu reserva?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

