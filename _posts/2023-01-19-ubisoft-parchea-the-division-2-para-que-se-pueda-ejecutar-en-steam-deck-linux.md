---
author: Pato
category: "Acci\xF3n"
date: 2023-01-19 22:47:31
excerpt: "<p>La compa\xF1\xEDa gala nos sorprende con un movimiento que puede confirmar\
  \ una tendencia</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/TheDivision2/TheDivision2.webp
joomla_id: 1523
joomla_url: ubisoft-parchea-the-division-2-para-que-se-pueda-ejecutar-en-steam-deck-linux
layout: post
tags:
- accion
- ubisoft
- steam-deck
- tom-clancy-s-the-division-2
title: Ubisoft parchea "The Division 2" para que se pueda ejecutar en Steam Deck /
  Linux
---
La compañía gala nos sorprende con un movimiento que puede confirmar una tendencia


Si eres de los que te mueves en el mundo del videojuego sabrás que **Ubisoft** es una compañía que no destaca precisamente por haber prestado atención al juego en Linux. Son de sobra conocidas las declaraciones de la compañía yéndose a la tienda de Epic o a Stadia durante un tiempo y no ofreciendo sus juegos en Steam, y por supuesto nada de ofrecer soporte a Linux en sus proyectos. Pero los tiempos cambian, y la compañía no está pasando por su mejor momento precisamente con varios proyectos cancelados y otros retrasados, problemas con los trabajadores, etc. La compañía desde luego no es la que fue tan solo hace algunos años, y parece que también atraviesa por una situación delicada a nivel financiero.


En esta tesitura, la compañía ha empezado a dejar de lado exclusividades en su propia tienda o con compañías como con la *Epic Game Store* o *Stadia* (**que acaba de bajar la persiana de forma permanente dicho sea de paso**) para ir volviendo gradualmente a Steam, desde donde probablemente nunca debería haber salido. De este modo en los últimos meses se han estado sucediendo algunos lanzamientos de la empresa en Steam; títulos como Assassin's Creed: Valhalla, Roller Champions, Inmortals Fenyx Rising o el último "**Tom Clancy's The Division 2**" lanzado el pasado día 12 de este mes dan cuenta del regreso de la compañía a la plataforma de Valve. Aunque una cosa es volver, y otra ofrecer soporte en Linux/Steam Deck.


Ya en su momento, preguntados sobre si ofrecerían soporte a la Steam Deck desde la compañía declararon que **ellos estarían donde estuvieran los usuarios** dando a entender que si la ultraportatil de Valve cogía tracción ellos se lo pensarían, pero por lo visto hasta ahora, los últimos lanzamientos no han contado con la atención de la empresa para poderse jugar desde Steam Deck ya que la mayoría de ellos cuentan con sistemas anti-trampas que aunque se pueden activar para poderse ejecutar ya sea en Linux directamente o bajo Proton, la compañía no ha movido un dedo para hacer esto posible. Hasta ahora.


La cuestión es que ayer mismo, y sin previo aviso [Ubisoft lanzó una actualización para su último lanzamiento](https://steamcommunity.com/games/2221490/announcements/detail/3632750190483497137) "**Tom Clancy's The Division 2**" **con el único y exclusivo propósito de hacer que el juego se pueda ejecutar en la Steam Deck y de paso en Linux**. Esto da pie a pensar que la compañía empieza a valorar el soporte de sus juegos en la ultraportatil de Valve, y ya hay quien sueña con que actualicen sus últimos lanzamientos de forma que sean jugables en nuestro sistema favorito.


También es justo mencionar, tal y como comenta [Liam de gamingonlinux.com](https://www.gamingonlinux.com/2023/01/ubisoft-fixed-the-division-2-on-steam-deck-and-linux-desktop/) que aunque el juego ya se puede ejecutar en Linux/Steam Deck, **el juego sufre de los temidos micro-parones cada vez que tiene que compilar los assets** del juego, y que de momento y a la espera de que Valve pueda valorar si lo clasifica como jugable o verificado, y pre-compila los shaders el juego no irá fino en las primeras partidas. Y sobre todo, si lo vas a probar **ejecútalo bajo DirectX11**, y no actives DX12 si no quieres que el juego se quede pillado.


En cualquier caso, y tal y como comentamos en [el artículo]({{ "/posts/hogwarts-legacy-llegara-a-steam-deck-y-linux-en-su-lanzamiento" | absolute_url }}) de Hogwarts Legacy parece que otra compañía de las importantes como es Ubisoft comienza a ver con buenos ojos a los jugadores de Linux/Steam Deck.


*Tom Clancy's The Division 2 es un juego de acción en el que el destino del mundo libre está en juego. Dirige un equipo de agentes de élite en los restos de Washington DC tras una pandemia, restablece el orden e impide su colapso:*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/MtiGg0P0Kyg" title="YouTube video player" width="560"></iframe></div>


 Si quieres echarle un vistazo, [lo tienes ahora mismo en Steam rebajado un 70%](https://store.steampowered.com/app/2221490/Tom_Clancys_The_Division_2/).


Parece que cada vez más compañías empiezan a fijarse en Linux/Steam Deck para sus desarrollos. 


¿Qué piensas del movimiento de Ubisoft? ¿Te gustaría que dieran soporte a algún otro de sus últimos juegos?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

