---
author: Serjor
category: Software
date: 2020-06-26 06:18:29
excerpt: "<p class=\"_eYtD2XCVieq6emjKBH3m\">Tim Sweeney ( <span class=\"TweetAuthor-screenName\
  \ Identity-screenName\" dir=\"ltr\" title=\"@TimSweeneyEpic\" data-scribe=\"element:screen_name\"\
  >@TimSweeneyEpic </span>) confirma que se est\xE1 trabajando en ello.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Wine/wine-easy-anti-cheat.webp
joomla_id: 1236
joomla_url: logran-ejecutar-un-juego-protegido-con-eac-en-wine-a1
layout: post
tags:
- wine
- eac
title: "Logran ejecutar un juego protegido con EAC en Wine (ACTUALIZACI\xD3N)"
---
Tim Sweeney ( @TimSweeneyEpic ) confirma que se está trabajando en ello.


**ACTUALIZADO**: A primera hora de la mañana lanzábamos esta noticia tan impactante y que tanto expectación está levantando entre los Gamers que usamos Linux, y parece que finalmente la pelota ha caido en el tejado correcto y parece que desde Epic (si, EPIC) están dispuestos a ir a recogerla, pues tan solo hace unas horas su CEO, [Tim Sweeney](https://twitter.com/TimSweeneyEpic) publicaba un tweet contestando a los que os dejábamos en la noticia original:



> 
> The team’s working on it, it’s just especially hard on Linux because the incredibly wide variety of configurations and inability to apply traditional digital signature techniques to custom compiled versions of the kernel, etc.
> 
> 
> — Tim Sweeney (@TimSweeneyEpic) [June 26, 2020](https://twitter.com/TimSweeneyEpic/status/1276538519826153473?ref_src=twsrc%5Etfw)



 Según esto, **parece que están trabajando**, algo que siempre se había dicho que estaban haciendo, pero que debido a que no acababa de llegar hacía desconfiar que se lo tomasen muy en serio, especialmente porque es Valve la principal interesada. El caso es que como comenta **parece que les está costando horrores debido a las técnicas de firmas digitales y la cantidad de versiones personalizadas del Kernel,** pero es curioso que la respuesta por parte de Epic llegue justamente cuando la comunidad está a punto de "puentearles". Supongo que no les hará mucha gracia que un equipo de desarrollo externo consiga este hito, además del consabido problema de seguridad y confianza que puede reportarles.   
  
Sea como fuere, según estamos viendo con estas noticias, parece que el **poder ejecutar juegos con EAC en Linux mediante Wine/Proton está cada vez más cerca**, tanto si es por parte de EPIC como si es gracias a nuestra bendita comunidad. 




---


  
**NOTICIA ORIGINAL:**  
  
Hace unas semanas leíamos en [Reddit](https://www.reddit.com/user/Guy1524/comments/hc3o1y/eac_progress_update/) una actualización del estado de EAC en Wine, y de los pasos que habían dado, y de cómo estaban muy cerca de romper la última barrera.


Pues bien, ya lo han conseguido:



> 
> After months of painstaking work in collaboration with [@Guy15241](https://twitter.com/Guy15241?ref_src=twsrc%5Etfw), we have an Easy Anti Cheat protected game running in wine, including their kernel driver! A lot more is ahead to get this cursed wine tree into a stable state, but finally we have something to show. 😎 [pic.twitter.com/ph555TnT6d](https://t.co/ph555TnT6d)
> 
> 
> — David Torok (@0xdt0) [June 25, 2020](https://twitter.com/0xdt0/status/1276286612654227458?ref_src=twsrc%5Etfw)



Según comentan en el propio tweet falta depurar y que funcione de una manera estable, pero es un gran hito que podría abrir la puerta a una inmensa cantidad de juegos que ahora mismo no pueden ser ejecutados en nuestro sistema por este tipo de protecciones anti trampas.


Veremos como responde Epic (ya que compraron en su momento a la empresa desarrolladora de Easy Anti Cheat), pero según Tim Sweeney, en tanto en cuanto esto no lleve a gente haciendo trampas, lo apoyarán:



> 
> We’d be fully supportive of these efforts if confident they wouldn’t lead to the worst-case scenario, which is a significant increase in cheating that we have no ability to detect.
> 
> 
> — Tim Sweeney (@TimSweeneyEpic) [June 20, 2020](https://twitter.com/TimSweeneyEpic/status/1274135828155990023?ref_src=twsrc%5Etfw)



 Así que esperemos que cumpla su palabra.


De todos modos, aún cumpliéndola, esto es solamente un parche sin apoyo de la desarrolladora. Estaría bien que EAC colaborara de cara a no tener que estar jugando al gato y al ratón, porque cualquier actualización de EAC podría hacer que los parches en Wine no sirvieran, y vuelta a empezar.


Y tú, ¿qué opinas de estos avances? Cuéntanoslo en los comentarios o en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux)

