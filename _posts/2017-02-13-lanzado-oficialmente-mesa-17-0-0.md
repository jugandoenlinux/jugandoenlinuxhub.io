---
author: leillo1975
category: Hardware
date: 2017-02-13 22:21:22
excerpt: "<p>Las nueva versi\xF3n de las librerias gr\xE1ficas libres incorpora importantes\
  \ mejoras</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c1a11e28afb03c9d81c096faa0a5ce8e.webp
joomla_id: 218
joomla_url: lanzado-oficialmente-mesa-17-0-0
layout: post
tags:
- opengl
- amd
- intel
- mesa
- nouveau
title: Lanzado oficialmente Mesa 17.0.0
---
Las nueva versión de las librerias gráficas libres incorpora importantes mejoras

En el día de hoy **Mesa ha alcanzado oficialmente la versión 17**, de acuerdo con la nueva nomenclatura, que a partir de ahora se corresponderá con el año de lanzamiento, por lo que hemos pasado directamente de la versión 13 a la 17. Hace bien poco, concretamente a primeros de Noviembre, me estrenaba en esta web con un artículo en el que estabamos celebrando [la llegada de la versión 13](index.php/item/70-mesa-13-lanzado-opengl-4-4-y-4-5-disponible) . Dicho lanzamiento tenía una especial importancia porque se alcanzaba el hito de conseguir la compatibilidad con OpenGL 4.5 en los drivers libres de Nvidia, AMD e Intel, así como la llegada de Vulkan, lo cual permitió a mucha gente poder ejecutar por fin  juegos que anteriormente estaban destinados unicamente a los usuarios de Nvidia con driver propietario.


En Mesa 17 se ha **ampliado y profundizado** ([ver enlace para más detalles](https://lists.freedesktop.org/archives/mesa-dev/2017-February/144115.html)) en ese soporte, ofreciendo numerosas correcciones y optimizaciones, que se hacen especialmente palpables a la hora de poder disfrutar aun más los últimos lanzamientos en videojuegos. Estas librerias son especialmente útiles para los usuarios que tengan tarjetas AMD, ya que verán una compatibilidad e  incremento del rendimiento sustanciales, por lo que la actualización es obligada.


Existe una forma de poder instalar Mesa actualizado en **Ubuntu**. En este momento la versión que hay colgada en el PPA es la RC3, pero supongo que muy pronto está la última. Si quereis instalarlo, abrid como siempre una terminal y ejecutad los siguientes comandos.



> 
> sudo add-apt-repository ppa:paulo-miguel-dias/pkppa -y
> 
> 
> sudo apt update
> 
> 
> sudo apt dist-upgrade
> 
> 
> 


Probablemente existan otras formas de instalar Mesa 17 en otros sistemas, pero no estoy familiarizado con ellas, por lo que os ruego que si las conoceis, dejeis un comentario en este artículo e iré actualizándolo. Por supuesto podeis comentar cuales son vuestras impresiones, opiniones o correciones. No olvideis que también disponemos de nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD) donde os estamos esperando.


 


Podeis ver este artículo en gallego en mi Blog personal (**[OBlogDeLeo.es](http://www.oblogdeleo.es/lanzado-oficialmente-mesa-17-0-0/)**)


 


**FUENTES:** [Phoronix](http://www.phoronix.com/scan.php?page=news_item&px=Mesa-17.0-Released), [Mesa](https://lists.freedesktop.org/archives/mesa-dev/2017-February/144115.html)

