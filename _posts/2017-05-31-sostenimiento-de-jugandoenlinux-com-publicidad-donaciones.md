---
author: Jugando en Linux
category: Editorial
date: 2017-05-31 18:52:38
excerpt: "<p>No podemos subsistir del aire, por lo que os explicamos de forma transparente\
  \ c\xF3mo vamos a intentar financiarnos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/23e58ccd18e32cab182dbd6268a12868.webp
joomla_id: 344
joomla_url: sostenimiento-de-jugandoenlinux-com-publicidad-donaciones
layout: post
title: 'Sostenimiento de Jugandoenlinux.com: publicidad y donaciones'
---
No podemos subsistir del aire, por lo que os explicamos de forma transparente cómo vamos a intentar financiarnos

Tenemos que ser sinceros: somos los primeros a los que no nos gusta la publicidad. Consideramos que es más una molestia que otra cosa, pero con la misma sinceridad tenemos que decir que una web como la de Jugandoenlinux.com no se mantiene del aire. 


Tenemos gastos de hosting, de sistemas, de plugins, de plantillas y complementos que utilizamos para que tu experiencia en nuestra web sea la mejor y más amena posible... y eso sin contar con las innumerables horas que debemos dedicar a programación en la web, actualizaciones, a mantenernos informados y realizar la ingente cantidad de artículos que generamos para poder informaros a vosotros puntualmente.


Además, ya sois unos cuantos los que nos visitáis asíduamente desde casi todas partes, desde Norte América, América Latina, Europa... pasando incluso por países asiáticos donde alguna visita también hay.


Necesitamos alguna fuente de ingresos que nos ayude a llevar toda esta carga y tras estudiarlo detenidamente, la más rápida y sencilla es ofreciendo publicidad. Es por esto que te pedimos, si es que aprecias el trabajo que hacemos en jugandoenlinux.com que **por favor no utilices adblocks en nuestra web**. Nosotros a cambio prometemos que la publicidad que ofreceremos será solo la imprescindible y no será invasiva.


**¿Hay alguna otra forma de ayudarnos y que suponga la eliminación de la publicidad?**


Hemos puesto un botón de **Paypal** en el panel lateral para que puedas donarnos cualquier cantidad que quieras. Toda aportación será bienvenida por pequeña que sea. **Si aportas una donación verás la página web sin ninguna publicidad**, tan solo con acceder con tu usuario y contraseña **durante al menos un mes**.


Por otra parte, **si aportas 5€ o más**, verás la web sin publicidad **durante 6 meses**. Y **si aportas 10€** verás la web sin publicidad **durante un año completo**.


Quede claro que **esto no es un "paywall"**. La información ofrecida será exactamente la misma tanto si aportas una donación como si no. La única diferencia será la publicidad, y nos estarás ayudando a seguir ofreciendo una web con información de calidad sobre videojuegos en Linux/SteamOS.


Desde el equipo de jugandoenlinux.com nos comprometemos a seguir informándote sobre videojuegos en Linux/SteamOS en español, y te damos las gracias por anticipado tanto por no utilizar adblocks, como por tu aportación si nos la haces.


¿Tienes alguna sugerencia o comentario al respecto? Cuéntanoslo en los comentarios, o en los canales habituales de jugandoenlinux.com.

