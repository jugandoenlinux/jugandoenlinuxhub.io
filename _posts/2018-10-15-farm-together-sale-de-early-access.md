---
author: leillo1975
category: Estrategia
date: 2018-10-15 10:32:32
excerpt: <p>El estudio asturiano @milkstone lanza oficialmete su juego</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2448909041eb1b59b4b28c16cb918292.webp
joomla_id: 875
joomla_url: farm-together-sale-de-early-access
layout: post
tags:
- acceso-anticipado
- early-access
- farm-together
- milkstone
title: '"Farm Together" sale de Acceso Anticipado.'
---
El estudio asturiano @milkstone lanza oficialmete su juego

En primer lugar nos gustaría pedir disculpas al estudio por la tardanza de este artículo, ya que el juego ha dejado de estar en Acceso Anticipado (Early Access) hace unos días (el día 11 de este mes). Si recordais, hace ya algún tiempo [hablamos de él](index.php/homepage/generos/simulacion/14-simulacion/764-farm-together-llegara-a-linux-steamos-en-acceso-anticipado-el-proximo-22-de-febrero) en JugandoEnLinux.com,  y durante este periodo han sido constantes las actualizaciones hasta llegar a esta, ya si, versión oficial.


El tiítulo nos permite gestionar nuestra propia granja y presenta unos gráficos desenfadados y coloristas con un magnífico diseño. Según podemos leer en la descripción que encontramos en Steam, en Farm Together encontrareis:



> 
> ***La experiencia granjística definitiva!***
> 
> 
> *¡De la mano los creadores de Avatar Farm llega **Farm Together**, la experiencia granjística definitiva!*  
>   
> *¡Empieza desde cero, con un pequeño huerto, y termina con una impresionante granja que se extiende más alla de donde alcanza la vista!*
> 
> 
> ***Cultiva tu granja***
> 
> 
> *¡Cultiva plantas, planta árboles, cuida a los animales, y mucho más! ¡Gástate el dinero ganado con el sudor de tu frente en nuevos edificios y decoraciones para tu granja! ¡Gana experiencia y desbloquea nuevos elementos y edificios!*  
>   
> *Salta en tu tractor y acelera las tareas, ¡pero ten cuidado o te quedarás sin combustible!*
> 
> 
> ***Relájate***
> 
> 
> *¡Quédate cuanto tiempo necesites! En **Farm Together** el tiempo avanza incluso cuando no estás online, así que puedes estar seguro de que tendrás algo que hacer cuando vuelvas más tarde.*  
>   
> *Gestiona tu granja por ti solo, permite la entrada sólo a tus amigos, ¡o ábrela al público y empezad a cultivar juntos! Con su sencillo sistema de permisos, podrás limitar lo que pueden hacer los desconocidos, para que puedan resultar de ayuda sin riesgos de ser vandalizados.*
> 
> 
> ***Personaliza tu granja y tu aspecto***
> 
> 
> *Tienes multitud de elementos de personalización a tu disposición: Vallas, carreteras, edificios, decoraciones... ¡Muestra tus habilidades de jardinería y decoración a tus vecinos!*  
>   
> *¡Y no te olvides de tu ropa! Personaliza tu avatar y tu tractor a tu gusto, ¡y entra en las granjas de tus vecinos a saludar!*
> 
> 
> 


La verdad es que da gusto ver juegos como este que han sido desarrollados en nuestro pais, y por supuesto en nuestra página siempre tendrán cabida, ya que nos produce un inmenso orgullo poder escribir sobre ellos. Para todos aquellos que no conozcais a [Milkstone](http://www.milkstonestudios.com/), debeis saber que entre otros, desarrollaron en el pasado los divertidisimos [Ziggurat](https://www.humblebundle.com/store/ziggurat?partner=jugandoenlinux), [Little Racers Street](https://store.steampowered.com/app/262690/Little_Racers_STREET/) y [White Noise 2](https://www.humblebundle.com/store/white-noise-2?partner=jugandoenlinux). Desde aquí estamos deseando ver el próximo trabajo de esta compañía. Os dejamos con un video donde podreis ver mejor las virtudes de este juego:


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/lY_JQDTD84M" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


**Podeis comprar Farm Together** en la tienda de [Humble Bundle](https://www.humblebundle.com/store/farm-together?partner=jugandoenlinux) (patrocinado) o en [Steam](https://store.steampowered.com/app/673950/Farm_Together/).


 


¿Qué os parece que trabajos como este salgan de nuestro pais? ¿Os gustan este tipo de juegos? Danos tu opinión en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

