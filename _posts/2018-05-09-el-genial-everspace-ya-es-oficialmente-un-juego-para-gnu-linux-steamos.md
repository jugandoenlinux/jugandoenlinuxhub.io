---
author: leillo1975
category: "Acci\xF3n"
date: 2018-05-09 13:59:57
excerpt: <p>@Rockfishgames acaba de actualizar el juego</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6ffa065634b02c07506d37e072a9ff8b.webp
joomla_id: 729
joomla_url: el-genial-everspace-ya-es-oficialmente-un-juego-para-gnu-linux-steamos
layout: post
tags:
- roguelike
- everspace
- rockfish-games
title: El genial "Everspace" ya es oficialmente un juego para GNU-Linux/SteamOS.
---
@Rockfishgames acaba de actualizar el juego

Gracias a [GamingOnLinux](https://www.gamingonlinux.com/articles/beautiful-space-combat-game-everspace-officially-out-of-beta-for-linux-and-now-on-gog.11740) acabamos de saber que uno de los [mejores juegos](index.php/homepage/editorial/item/713-los-mejores-de-2017) que nos llegaron el año pasado **tiene por fin soporte oficial para nuestro sistema**. El juego, que en su día [analizó genialmente](index.php/homepage/analisis/item/605-analisis-everspace) nuestro colega [Serjor](index.php/homepage/analisis/itemlist/user/196-serjor), podía ser jugado sin problemas en GNU-Linux/SteamOS si disponíamos de una GPU Nvidia, pero no era así el caso si éramos usuarios de soluciones gráficas de AMD. Tal y como [anunciamos](index.php/homepage/generos/accion/item/821-everspace-tendra-su-primera-version-oficial-muy-pronto) hace poco más de un mes, parece que **finalmente han solucionado estos inconvenientes** y el juego luce orgulloso su preciado logo de Steam que certifica que el juego funciona ya en nuestro sistema.


Everspace, que ha llegado a la [versión 1.2.3](https://steamcommunity.com/games/396750/announcements/detail/1643129680836474285),  incluye, aparte de la ya comentada migración a la **versión 4.18 de Unreal Engine**, más de **40 correcciones y retoques**, entre los que destacan las relaccionadas con la realidad virtual, y la opción de apagar el suavizado del controlador que usemos. Si quereis saber de que va el juego os dejamos la descripción oficial:



> 
> *EVERSPACE™ es un shooter de acción espacial para un jugador, que combina elementos roguelike con gráficos de primera calidad y una historia cautivadora. Emprende un desafiante viaje a través de un universo hermosamente creado, lleno de sorpresas y en constante cambio. Tus habilidades, técnicas y talento para la improvisación serán puestos a prueba continuamente, mientras aprendes más sobre tu existencia a través de encuentros con personajes interesantes que irán aportando sus piezas del rompecabezas a la historia. En cada partida encontrarás situaciones emocionantes y completamente nuevas, que garantizan muchas horas de juego en las que vivirás momentos únicos y cargados de significado. Sin embargo, no importa lo hábil que seas como piloto, la muerte es inevitable... pero es solo el comienzo de un viaje mucho más largo.*  
>   
> *Vive frenéticos y emocionantes combates espaciales en los que tendrás que empelar una amplia gama de armas y dispositivos.*  
>   
> *Utiliza los recursos que reuniste o extrajiste para crear equipo o modificaciones con las que podrás conseguir mejoras indispensables o reparar los sistemas de tu nave. Tú eliges.*  
>   
> *Consigue planos de fabricación y ve a la caza de armas y equipo exóticos. Nunca sabrás qué vas a encontrar a continuación.*  
>   
> *Viaja a través de niveles generados procedimentalmente con muchos tesoros ocultos y peligros. Te aguarda un juego de riesgo y recompensas.*  
>   
> *En tus manos está sacar lo mejor de la situación con las herramientas a tu alcance. Utiliza todas las ventajas y usa la creatividad para poner la situación a tu favor, porque cada error que cometas podría ser el último.*
> 
> 
> 


Los requisitos mínimos para poder disfrutar del juego son los siguientes:


* **SO:** Ubuntu 16.04
* **Procesador:** Intel CPU Core i3
* **Memoria:** 4 GB de RAM
* **Gráficos:** NVidia Geforce GTX 480 / AMD Radeon HD 5870
* **Almacenamiento:** 8 GB de espacio disponible
* **Notas adicionales:** No VR/HOTAS support on Linux


También nos gustaría deciros que teneis la posibilidad desde hoy de adquirir este juego en [GOG](https://www.gog.com/game/everspace), así como hasta ahora, en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/396750/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 Os dejamos de nuevo con un par de videos donde tanto Serjor, como un servidor disfrutamos en su día de este título:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/-Qzk8XnR0wY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/G6zj57jcJSM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


¿Teneis ya Everspace? ¿Qué os ha parecido el juego? Déjanos tu opinión en los comentarios o en nuestros grupos de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/VZGNYP).

