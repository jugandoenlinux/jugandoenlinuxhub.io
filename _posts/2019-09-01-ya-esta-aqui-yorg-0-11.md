---
author: leillo1975
category: Carreras
date: 2019-09-01 09:23:51
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >Nueva versi\xF3n de este proyecto libre de @ya2tech</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/dabc434549d80d8d09e61e03ac4a4c32.webp
joomla_id: 1106
joomla_url: ya-esta-aqui-yorg-0-11
layout: post
tags:
- yorg
- ya2
- software-libre
- panda3d
- python
title: "\xA1Ya est\xE1 aqu\xED YORG 0.11!"
---
Nueva versión de este proyecto libre de @ya2tech

Quizás recordeis la última vez que os hablamos de este proyecto. En aquella ocasión os hablábamos sobre [los mejores juegos de conducción Libre](index.php/homepage/generos/carreras/2-carreras/1099-los-mejores-juegos-de-coches-libres), y por supuesto este juego tenía que estar. Cuando se han cumplido 10 meses desde [su última versión](index.php/homepage/generos/carreras/2-carreras/1008-el-juego-libre-yorg-alcanza-la-version-0-10) (0.10) y dos versiones candidatas, finalmente el resultado de montones de horas de desarrollo ya está aquí, y hemos de decir que la lista de mejoras es impresionante, siendo muchas de ellas de especial importancia para la mejora de la jugabilidad.


Entre todos estos cambios, por ejemplo, podemos encontrar el tan demandado **soporte para joypads**, algo que para este tipo de juegos es casi imprescindible. Hasta ahora, el motor gráfico usado para el juego, [Panda3D](https://www.panda3d.org/),  no permitía el uso de estos periféricos, pero desde su última versión, la 1.10, ya se pueden utilizar, por lo que Flavio, su desarrollador enseguida se ha puesto manos a la obra para implementarlo. El soporte no se ha quedado tan solo en los movimientos, sinó que también es posible sentir **efectos de feedback** con el mando adecuado.


También se ha hecho un trabajo excepcional con el **apartado multijugador**, donde se han hecho cambios y adiciones de calado. Por poner ejemplos, se ha añadido el **multijugador local**, que nos permitirá jugar a pantalla partida en el mismo ordenador con un total de hasta 4 jugadores, lo que es perfecto para disfrutar en casa con la familia o amigos. También **se ha remodelado la interfaz del apartado online**, siendo muchísimo más clara ahora. El desarrollador nos aclara que esta característica sigue siendo **experimental**, por lo que es muy posible que siga teniendo fallos, especialmente si se sobrecarga el servidor.


![yorg 0 11 localmulti](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/YORG/yorg_0_11_localmulti.webp)


Es reseñable el trabajo que se ha hecho con las **partículas** en el juego, algo que hasta ahora no teníamos (es su primera implementación), y por lo que ahora podremos ver el humo de los derrapes, destellos, armas... También se ha mejorado el **modelo de conducción**, siendo más sencillo de controlar el coche que en la anterior versión, y siendo también este un aspecto en el que se incidirá en próximas versiones. Encontramos también cambios en la **inteligencia artificial** de los competidores, en algunos **efectos especiales** y también en el **rendimiento del juego**, que ha sido mejorado gracias a la nueva versión de Panda3D. No hay que olvidar tampoco que **el código ha sido portado enteramente a Python 3**. Se han actualizado por supuesto las **traducciones** del juego (Gaélico, Español, Gallego y Francés)


Como veis, Flavio Calva (al que [entrevistamos](index.php/homepage/entrevistas/35-entrevista/587-entrevista-a-flavio-calva-de-ya2-yorg) hace algún tiempo) no ha estado de brazos cruzados todo este tiempo y nos ha regalado una nueva versión cargada de sustanciales mejoras en el juego. Ahora solo queda que os animeis a probarlo, y por que no, a jugar unas partidas online con la gente de nuestra comunidad.  Podeis encontrar YORG 0.11 en su página de [descargas](https://www.ya2.it/pages/yorg.html). Os dejamos con un video que acabamos de grabar repasando las novedades del juego:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/jFhevc5mXM4" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


¿Qué os parece [YORG](https://www.ya2.it/pages/yorg.html#yorg)? ¿Lo habeis jugado ya? Cuéntanoslo en los comentarios o deja tus mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

