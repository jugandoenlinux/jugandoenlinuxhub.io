---
author: Pato
category: Hardware
date: 2019-04-30 17:31:22
excerpt: "<p>Se desvelan los detalles de estas gafas compatibles con Linux que podr\xE1\
  s reservar a partir de ma\xF1ana</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e8254cdf6a60c0aacb685c6fcaf4d937.webp
joomla_id: 1035
joomla_url: las-gafas-de-realidad-virtual-valve-index-aparecen-en-la-seccion-hardware-de-steam
layout: post
tags:
- steam
- steamvr
- valve-index
title: "Las gafas de realidad virtual Valve Index aparecen en la secci\xF3n Hardware\
  \ de Steam"
---
Se desvelan los detalles de estas gafas compatibles con Linux que podrás reservar a partir de mañana

Tras la confirmación de que Valve estaba desarrollando sus gafas de realidad virtual tal y como [informamos en su momento](index.php/homepage/hardware/15-hardware/1131-valve-index-las-gafas-de-realidad-virtual-de-valve-por-fin-se-confirman), ahora llegan todos los detalles ya que Valve ha publicado en su [sección de Hardware](https://store.steampowered.com/valveindex) de Steam las que serán sus propias gafas de realidad virtual: las **Valve Index**


Según se puede ver en su página oficial, las **Valve Index** ofrecerán una óptica mejorada junto a unos paneles LCD ultrarápidos personalizados de **2880×1600 (1440×1600 por ojo) y frecuencias de hasta 144Hz para una visualización panorámica de 130º y auriculares con sonido direccional 3D.**


Además, las Valve Index se ofrecerán junto a las estaciones base "[Index Base Station](https://store.steampowered.com/app/1059570/Valve_Index_Base_Station/)" necesarias para el posicionamiento en 3D que serán compatibles también con las HTC Vive y Vive Pro. También serán imprescindibles los "[Valve Index Controllers](https://store.steampowered.com/app/1059550/Valve_Index_Controllers/)" para un manejo de los juegos dentro del entorno 3D.


En cuanto a los **requisitos necesarios** para poder utilizar las Valve Index, estos son:


**Mínimo:**  

+ **SO:** SteamOS, Linux
+ **Procesador:** Dual Core con Hyper-Threading
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** NVIDIA GeForce GTX 970, AMD RX480
+ **Red:** Conexión de banda ancha a Internet
+ **Notas adicionales:** DisplayPort (Version 1.2) y puerto USB (2.0+)


**Recomendado:**  

+ **Procesador:** Quad Core +
+ **Gráficos:** NVIDIA GeForce GTX 1070 o superior
+ **Notas adicionales:** Puerto USB (3.0+) para el Headset Pass-Through Camera & soporte de puerto USB




Por el momento no se ha anunciado ningún nuevo título de lanzamiento por parte de Valve para apoyar su nuevo hardware.


Todo esto se podrá adquirir en conjunto **en un Kit al "módico" precio de 1.079€** en pre-compra, o por separado si ya tienes el resto de accesorios de las HTC Vive. S**olo las gafas salen por 539€** comenzando la reserva mañana mismo.


Su puesta en venta oficial según pone en su web será el 28 de Junio. Tienes toda la información de las Valve Index en su [página web oficial](https://store.steampowered.com/valveindex).


¿Qué te parecen las nuevas gafas de Valve? ¿Crees que las Valve Index conseguirán abrirse hueco en el mercado?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

