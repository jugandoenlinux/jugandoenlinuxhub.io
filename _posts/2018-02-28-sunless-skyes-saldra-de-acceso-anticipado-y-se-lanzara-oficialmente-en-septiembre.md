---
author: Pato
category: Aventuras
date: 2018-02-28 14:14:35
excerpt: <p>El juego de Failbetter Games nos traslada a los cielos</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0445f454d93c4fcc0efc1c70de029529.webp
joomla_id: 663
joomla_url: sunless-skyes-saldra-de-acceso-anticipado-y-se-lanzara-oficialmente-en-septiembre
layout: post
tags:
- indie
- aventura
- rol
- acceso-anticipado
title: "'Sunless Skyes' saldr\xE1 de acceso anticipado y se lanzar\xE1 oficialmente\
  \ en Septiembre"
---
El juego de Failbetter Games nos traslada a los cielos

'Sunless Skyes' es la secuela del excelente 'Sunless Sea' [[Steam](http://store.steampowered.com/app/304650/SUNLESS_SEA/)] que está disponible también para GNU-Linux/SteamOS y que está siendo desarrollado por Failbetter Games. Se trata de un juego de aventura y exploración en un universo distóptico de ambiente victoriano en el que se sucederán las situaciones de horror y los sobresaltos conforme vayamos avanzando en el juego. Pues bien, acaban de anunciar que el juego saldrá de acceso anticipado y será lanzado oficialmente en nuestro sistema favorito el próximo mes de Septiembre, tal y como podéis ver [en la última actualización](http://steamcommunity.com/games/596970/announcements/detail/1653256439443547479), donde además de esto ofrecen más detalles sobre el gameplay, nuevo armamento, nuevas regiones que explorar, mejoras en el soporte para gamepads y mucho más.


*"Explora un universo lleno de horror celestial y arrasado por la ambición victoriana en este RPG literario para PC, Mac y Linux. Sunless Skies' es un juego de rol en vista cenital con una ambientación victoriana en un Londres en el que tendrás que enfrentarte con tu aeronave "a abominaciones en cielos lejanos, luchar por sobrevivir, hablar a las tormentas, asesinar a un sol y luchar por no perder el juicio".*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/y9iLoHLICUE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


'Sunless Skies' se encuentra actualmente en acceso anticipado tras haber superado con mucho éxito una campaña en Kickstarter, tal y como ya os comentamos [en otro artículo](index.php/homepage/generos/rol/item/327-sunless-skies-esta-en-campana-en-kickstarter-y-logra-financiarse-en-solo-4-horas).


Si quieres echarle un vistazo o saber más puedes visitar [la web oficial](http://www.failbettergames.com/sunless-skies/) del juego, y también lo tienes disponible en su página de Steam, aunque no está traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/596970/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Que te parecen los juegos de Failbetter Games? ¿Jugaste a Sunless Sea? ¿Piensas jugar a este Sunless Skies?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

