---
author: Jugando en Linux
category: Apt-Get Update
date: 2022-04-01 17:29:48
excerpt: ''
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Thefertilecrescent/thefertilecrescentbanner.webp
joomla_id: 1455
joomla_url: nuevos-juegos-con-soporte-nativo-01-04-2022
layout: post
title: Nuevos juegos con soporte nativo 01/04/2022
---

Una semana más, os traemos algunas de las novedades recientes que pensamos que os pueden interesar, y con soporte nativo en Linux:


### Lupa


DESARROLLADOR: LbGamess
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/tZA1AeHoExM" title="YouTube video player" width="720"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1825310/" style="border: 0px;" width="646"></iframe></div>




---

 
### TFC: The Fertile Crescent


 

DESARROLLADOR: Wield Interactive
 
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/nSGXMCTvivE" title="YouTube video player" width="720"></iframe></div>
 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1674820/" style="border: 0px;" width="646"></iframe></div>




---


 


### Void Marauders


DESARROLLADOR: Autarca
 
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/0BRzkyJ76Dg" title="YouTube video player" width="720"></iframe></div>


 
<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1482820/" style="border: 0px;" width="646"></iframe></div>




---

 
### Crystal Project


DESARROLLADOR: Andrew Willman
 
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/Rr3T1Kdhtiw" title="YouTube video player" width="720"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1637730/" style="border: 0px;" width="646"></iframe></div>




---


### Regrowth


DESARROLLADOR: Portgate Studios
 
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/eifN6_R2p18" title="YouTube video player" width="720"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1346210/" style="border: 0px;" width="646"></iframe></div>


 




---


### OFERTAS


Spiritfarer®: Edición Farewell


DESARROLLADOR: Thunder Lotus Games


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/mWcDpodmiKg" title="YouTube video player" width="720"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/972660/319017/" style="border: 0px;" width="646"></iframe></div>




---


### Transport Fever 2


DESARROLLADOR: Urban Games
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/yK2__WGmhPw" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1066780/" style="border: 0px;" width="646"></iframe></div>




---


 


### Two Point Hospital


DESARROLLADOR: Two Point Studios
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/SjrcU1gwkjc" title="YouTube video player" width="720"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/535930/" style="border: 0px;" width="646"></iframe></div>




---


### Slime Rancher


DESARROLLADOR: Monomi Park
 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/jDZUhN8pU9c" title="YouTube video player" width="720"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/433340/" style="border: 0px;" width="646"></iframe></div>

