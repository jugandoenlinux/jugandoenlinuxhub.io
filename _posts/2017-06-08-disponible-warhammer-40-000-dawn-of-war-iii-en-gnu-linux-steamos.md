---
author: leillo1975
category: Estrategia
date: 2017-06-08 11:00:24
excerpt: <p>Al fin tenemos una nueva entrega de esta Saga</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/051a22dde3b372e5c058fbc303756df4.webp
joomla_id: 364
joomla_url: disponible-warhammer-40-000-dawn-of-war-iii-en-gnu-linux-steamos
layout: post
tags:
- feral-interactive
- estrategia
- warhammer
- dawn-of-war
title: 'Disponible Warhammer 40.000: Dawn of War III en GNU-Linux/SteamOS.'
---
Al fin tenemos una nueva entrega de esta Saga

 Tras [múltiples informaciones](index.php/component/search/?searchword=Dawn%20war%20III&ordering=newest&searchphrase=all&limit=20) en los últimos días, finalmente disponemos de un nuevo título ambientado en el **universo Warhammer**. Antes le había tocado a [Total War: Warhammer](index.php/homepage/generos/estrategia/item/109-total-war-warhammer-ya-disponible-en-linux-steamos) y toda la [serie de juegos Dawn of War II](index.php/homepage/generos/estrategia/item/10-warhammer-40000-dawn-of-war-ii-chaos-rising-y-retribution-ya-disponibles-para-linux-steamos), también portados por el estudio **Feral Interactive**, que tantas alegrías nos da. Aquí vemos su anuncio en twitter:


 



> 
> Step into the grimdark far future… Warhammer 40,000: Dawn of War III released for macOS and Linux. Buy it here – <https://t.co/evCccHoEdl> [pic.twitter.com/OLArXRYEmj](https://t.co/OLArXRYEmj)
> 
> 
> — Feral Interactive (@feralgames) [June 8, 2017](https://twitter.com/feralgames/status/872765585918218242)



 


Como todos sabreis se trata de un juego de estrategia en tiempo real con gestión de recursos donde encontraremos las diferentes facciones (marines espaciales, eldars y orcos) y héroes. Esto sería el juego muy a grandes rasgos, pero en un futuro intentaremos desgranaros el juego con un **completo análisis**, donde analizaremos la jugabilidad, gráficos, rendimiento, etc


 


Haciendo una recopilación de todo lo que hemos anunciado, se sabe que dispondrá de **modo multijugador entre Linux y Mac**, aunque se esta intentando desarrollar un parche para poder jugar con usuarios de Windows, **soporte experimental de Vulkan** para Nvidia (381.22), AMD (mesa 17.1) e **Intel** (mesa 17.2). Los requisitos técnicos **oficiales** no son muy elevados pero necesitaremos como mínimo 8GB de RAM para poder jugarlo ( Procesador: 3.4 GHz Intel i3-4130, RAM: 8 GB, Gráficas: 1 GB Nvidia 650Ti). Os dejo aquí el trailer de lanzamiento para que se os haga la boca agua:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/fppWUST7-P0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 Si quereis comprarlo podeis hacerlo directamente en **[la tienda de Feral Interactive](https://store.feralinteractive.com/en/mac-linux-games/dawnofwar3/)** (recomendable) o en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/285190/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Eres fan de la Saga Warhammer? ¿Como te llevas con los juegos de Estrategia en tiempo real? Puedes respondernos en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD) donde os estamos esperando.

