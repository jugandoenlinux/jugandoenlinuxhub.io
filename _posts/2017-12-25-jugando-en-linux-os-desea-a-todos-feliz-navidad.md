---
author: Jugando en Linux
category: Editorial
date: 2017-12-25 09:01:03
excerpt: "<p>Deseamos de coraz\xF3n que os regalen muchos juegos para Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/55f1af74ed6d51549d7d4b31853fd65a.webp
joomla_id: 586
joomla_url: jugando-en-linux-os-desea-a-todos-feliz-navidad
layout: post
title: Jugando en Linux os desea a todos Feliz Navidad
---
Deseamos de corazón que os regalen muchos juegos para Linux

Estamos en estas fechas tan señaladas, y desde Jugando en Linux queremos desearos a todos, los que nos visitáis asíduamente y a los que no, a los que juegan con nosotros cada semana, a todos los que nos siguen en las redes sociales y en los canales de mensajería, y en general a todos los jugadores de habla española que juegan o jugarán en GNU/Linux/SteamOs/nuestro sistema favorito, una muy feliz Navidad (o solsticio de invierno para los que prefieran) y que juguéis y disfrutéis muchísimo con vuestros videojuegos en Linux favoritos.


Durante este último año en Jugando en Linux hemos tratado de traeros las noticias y la información más fresca de todo lo que se ha movido en el entorno de los videojuegos en Linux. Un año en el que hemos visto como cada vez más, nuestro sistema favorito se está abriendo paso en el mundo del videojuego y que va creciendo con paso firme. Esperamos seguir creciendo nosotros también como página web de referencia en español de videojuegos en Linux, y esperamos seguiros informando y jugando con vosotros durante mucho tiempo más.


Como sabéis, hemos evolucionado y remodelado la web para hacerla más acorde y para ofreceros una mayor cantidad y calidad de información. Pero no queremos quedarnos aquí, si no que iremos implementando mejoras y novedades en la medida en la que vayamos pudiendo para crecer y merecer vuestras visitas cada día.


No olvidéis que seguimos necesitando vuestra ayuda. Jugandoenlinux.com necesita mantenimiento, servidores, plugins y plantillas, ingentes horas de trabajo y dedicación y para ello necesitamos vuestra colaboración, ya sea [donándonos alguna moneda](index.php/por-que-tenemos-publicidad), añadiendo la página a vuestros favoritos y a vuestra lista blanca en vuestros adblockers, o enviándonos avisos y [artículos](index.php/contacto/envianos-tu-articulo) para que podamos publicar información interesante.


Desde Jugando en Linux os deseamos Feliz Navidad a todos.


El equipo de [www.jugandoenlinux.com,](http://www.jugandoenlinux.com) Pato, Leo y Serjor.

