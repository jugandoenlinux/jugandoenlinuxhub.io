---
author: Pato
category: Noticia
date: 2020-05-12 21:14:40
excerpt: "<p>Repasamos las \xFAltimas novedades de la plataforma en clave Linux</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/Stadia_2-Games-Jotun-Sundered.webp
joomla_id: 1220
joomla_url: stadia-suma-y-sigue-doom-64-assassin-s-creed-valhalla-chorus-jotun-sundered-y-mas
layout: post
tags:
- jotun
- stadia
- sundered
- assassin-s-creed
- chorus
- doom-64
title: 'Stadia suma y sigue: DOOM 64, Assassin''s Creed Valhalla, CHORUS, Jotun, Sundered,
  y mas...'
---
Repasamos las últimas novedades de la plataforma en clave Linux


 


Es hora de repasar las novedades de la última semana en la plataforma de **Google Stadia**pues entre anuncios y estrenos la plataforma se está poniendo bastante jugosa...


La semana pasada [teníamos noticias](https://community.stadia.com/t5/Stadia-Community-Blog/Explore-mythical-worlds-from-Thunder-Lotus-Games-coming-to/ba-p/21897) de que **Thunder Lotus Games** iba a desembarcar en la plataforma de juego en la nube con dos de sus títulos más emblemáticos, como son **Jotun** y **Sundered**, que si bien es cierto que son títulos que ya estaban disponibles para Linux en otras plataformas, no deja de ser interesante que estos títulos vayan llegando, aunque para muchos de nosotros no nos resulte de mucho interés dado que ya los tenemos hace tiempo.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/3fK27302gJM" width="560"></iframe></div>


 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/8EUHQikyTsU" width="560"></iframe></div>


 


Por otra parte, también [tuvimos noticias](https://community.stadia.com/t5/Stadia-Community-Blog/This-Week-on-Stadia-Turing-Tests-Zombie-Pests-and-SteamWorld-s/ba-p/21635) de la llegada a Stadia Pro de juegos como **The Turing Test, Zombie Army 4: Dead War** y **SteamWorld Heist**, por lo que si eres suscriptor puedes jugarlos todos de forma gratuita.


Seguimos ahora con más videojuegos... y vaya videojuegos! Durante la presentación de la nueva consola de Microsoft hubo unas cuantas presentaciones de videojuegos, y de ellos ya sabemos que al menos dos van a llegar a Stadia. El primero, y sobran las presentaciones, **Assassin's Creed Valhalla**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/E958Mq35YYw" width="560"></iframe></div>


Y el segundo es el no menos espectacular y prometedor **CHORUS**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/zNdc_uoiS3E" width="560"></iframe></div>


Por último, respecto a videojuegos  [tenemos la llegada](https://community.stadia.com/t5/Stadia-Community-Blog/This-Week-on-Stadia-DOOM-64/ba-p/22273) hoy mismo de **DOOM 64**, el juego de acción frenética que tenemos disponible desde ya para adquirir en la tienda de la plataforma:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/mtvHGrPyJw8" width="560"></iframe></div>


En otro orden de cosas, [también nos anunciaron](https://community.stadia.com/t5/Stadia-Community-Blog/This-Week-on-Stadia-Turing-Tests-Zombie-Pests-and-SteamWorld-s/ba-p/21635) que ha llegado el **soporte de mandos inalámbricos de Stadia** bajo navegadores con base Chromium, por lo que ahora deberíamos poder jugar con **los mandos de la propia plataforma** de forma "enchufar y jugar", sin tener que conectarlos por cable al equipo.


Y de momento eso es todo, que tampoco es que sea poco. Como siempre, estaremos atentos a las novedades de Stadia.


¿Que te parecen los juegos que van llegando a Stadia? ¿Esperas la llegada a la plataforma de algún juego en especial?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

