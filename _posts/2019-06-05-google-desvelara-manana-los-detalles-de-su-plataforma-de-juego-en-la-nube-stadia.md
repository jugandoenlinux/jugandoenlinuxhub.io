---
author: Pato
category: Noticia
date: 2019-06-05 17:49:08
excerpt: "<p>El servicio de Google correr\xE1 sobre servidores con Linux y Vulkan\
  \ como API gr\xE1fica</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7825d940a749f0179cc7ea360c8ae6de.webp
joomla_id: 1057
joomla_url: google-desvelara-manana-los-detalles-de-su-plataforma-de-juego-en-la-nube-stadia
layout: post
tags:
- stadia
title: "Google desvelar\xE1 ma\xF1ana los detalles de su plataforma de juego en la\
  \ nube Stadia"
---
El servicio de Google correrá sobre servidores con Linux y Vulkan como API gráfica

Google pretende revolucionar el panorama videojueguil con su próximo servicio de juego en la nube llamado **Stadia.** Este servicio utilizará todo el poder de la infraestructura de servidores de Google, utilizando como base hardware **procesadores y gráficas de AMD** y por la parte de software **sistemas Linux y la API de alto rendimiento Vulkan**.


Por ahora esto es lo que sabemos, a falta de saber detalles tan importantes como el precio del servicio, la experiencia de juego (aunque ya se habla de fibra con 30 megas mínimo para una experiencia fluida a 1080p) y el catálogo, pero Google ha decidido desvelar estos detalles mañana mismo anticipándose al próximo E3. (Por cierto, atentos que tendremos novedades al respecto).


Así lo desveló en un tweet en el que nos emplazan a la retransmisión que tendrá lugar mañana mismo y en el que nos desvelarán estos detalles:



> 
> Some news can't wait for [#E3](https://twitter.com/hashtag/E3?src=hash&ref_src=twsrc%5Etfw).   
>   
> Tune into the first ever [#StadiaConnect](https://twitter.com/hashtag/StadiaConnect?src=hash&ref_src=twsrc%5Etfw) this Thursday 6/6 at 9AM PT for exciting announcements, games, and more → <https://t.co/dKmKakQeQp> [pic.twitter.com/mZRagFGh4k](https://t.co/mZRagFGh4k)
> 
> 
> — Stadia (@GoogleStadia) [June 3, 2019](https://twitter.com/GoogleStadia/status/1135584478389264389?ref_src=twsrc%5Etfw)



 Según el anuncio la retransmisión del evento se llevará a cabo en su [canal de Youtube](https://www.youtube.com/watch?v=k-BbW6zAjL0) mañana a las 6 pm (CEST) de la tarde hora española (CET+1). En Jugando en Linux pensamos ver la retransmisión en directo. ¿Nos acompañas?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

