---
author: leillo1975
category: "Acci\xF3n"
date: 2021-04-09 16:20:01
excerpt: "<p><span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@UtoposGames\
  \ es gratuito por un tiempo limitado. <br /></span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Utopos/Utopos.webp
joomla_id: 1298
joomla_url: utopos-llega-en-acceso-anticipado-con-soporte-nativo-a1
layout: post
tags:
- arcade
- retro
- unity3d
- remake
- atari
- utopos
title: "Utopos llega en Acceso Anticipado con soporte nativo (ACTUALIZACI\xD3N)"
---
@UtoposGames es gratuito por un tiempo limitado.   



**ACTUALIZACIÓN 21-4-21:** Acabamos de ver que el juego **se podrá adquirir temporalmente en Steam de forma totalmente gratuita** para siempre. No sabemos concretamente a que hora termina esta oferta (entre las 18:00 y 19:00 CET del día de hoy), pero yo de vosotros no me lo pensaría dos veces para hacerme con él. También comentaros que según nos ha informado directamente su desarrollador el juego estará terminado más o menos en unos tres meses.


Nos gustaría decir también, que en JugandoEnLinux.com **estamos participando del proceso de testeo de la versión de nativa de Linux**, y podemos aseguraros que el juego es muy divertido y vistoso, por lo que creemos que no os vais a arrepentir de añadirlo a vuestra biblioteca. Podeis encontrarlo en:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1560330/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 




---


**NOTICIA ORIGINAL 9-4-21:** No se si recordais, pero hace unas semanas publicábamos un artículo sobre [URG]({{ "/posts/el-arcade-retro-urg-es-lanzado-en-gnu-linux-a1" | absolute_url }}), un juego de naves y disparos con vista lateral. Pues hace un rato su creador, [Jani Penttinen](https://twitter.com/jani_penttinen?lang=es),  se volvía a poner en contacto conmigo para hablarme sobre que su **nuevo juego**, [Utopos](https://www.utoposgames.com/copy-of-urg), tenía también soporte nativo. Como os comentábamos en el subtítulo, el juego está aun en acceso anticipado, pero por lo que vemos tiene un buen ritmo de [actualizaciones](https://store.steampowered.com/news/app/1560330/view/3123784455261699126) .


El juego también luce una vista lateral en 3D con unos buenos efectos de iluminación, y es un remake del Utopos original de Atari ST. Según la descripción original en el juego podemos encontrar:


*¡Utopos ha vuelto! El Utopos original se lanzó hace casi 30 años, en 1993, pero la magia sigue ahí. Utopos es un juego de lucha de naves espaciales multijugador. Utopos es fácil de aprender y proporciona innumerables horas de divertida acción con tus amigos.*


*Situado en el Cinturón de Asteroides, Utopos es un mundo colonizado habitado por humanos que abandonaron la tierra en busca de una vida mejor. Las peleas de cohetes se han convertido en un deporte muy popular y depende de ti luchar para llegar a la cima. Elige tu nave y tus armas y lucha contra otros jugadores para ver quién es el campeón definitivo de la lucha de cohetes.*


*Aunque Utopos es un juego multijugador, también ofrece bots controlados por la IA que luchan igual que los humanos, así que también puedes entrenar por tu cuenta.*


*Prueba Utopos con un amigo: ¡es muy posible que sea lo más divertido que hayas hecho en años!*


 Como siempre, vale más una imagen, o más bien un video, que mil palabras, y en este video promocional podreis ver mejor de lo que os hablo:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/syigCFxhrYI" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


Si quereis haceros con el juego podeis comprarlo en Steam en **Acceso Anticipado** por **8.19€**:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1560330/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 Podeis dejarnos vuestras impresiones y opiniones sobre Utopos en Linux en los comentarios en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

