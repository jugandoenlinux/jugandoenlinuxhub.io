---
author: Pato
category: Noticia
date: 2019-10-10 11:43:34
excerpt: "<p>Este servicio permitir\xE1 jugar con amigos de forma remota como si estuvieran\
  \ jugando en tu propio PC</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/44ea8054159099b6b90b5528233befe0.webp
joomla_id: 1118
joomla_url: steam-anadira-una-nueva-caracteristica-para-jugar-juegos-con-multijugador-local-llamada-remote-play-together
layout: post
tags:
- steam
- steam-play
- remote-play-together
title: "Steam a\xF1adir\xE1 una nueva caracter\xEDstica para jugar juegos con multijugador\
  \ local llamada Remote Play Together"
---
Este servicio permitirá jugar con amigos de forma remota como si estuvieran jugando en tu propio PC

Steam sigue añadiendo novedades y servicios, y hoy llegan noticias de que un nuevo servicio llamada **Remote Play Together**. Tal y como leemos en [pcgamer.com](https://www.pcgamer.com/steams-remote-play-together-will-introduce-online-support-for-all-local-multiplayer-games/#comment-jump) se ha filtrado esta nueva característica que habría sido anunciada en los foros privados de Steam para desarrolladores (Steamworks) dándoles a conocer la intención de Steam de incluir lo que será una forma de jugar de forma remota a juegos que hasta ahora solo se podían disfrutar en multijugador local.


Remote Play Together será pues un servicio al estilo de otros como [Parsec](https://parsecgaming.com/) pero esta vez integrado en el propio cliente de Steam. Para que os hagáis una idea, hasta ahora si querías jugar a ciertos juegos que solo tienen opción de multijugador local tenías que estar junto a tus amigos delante de tu PC, o usar alguno de los servicios que existen para el caso, pero con Remote Play Together será el propio cliente de Steam el que de la opción de jugar a los juegos con multijugador local de forma remota, conectando a tus amigos a tu partida como si estuvieran sentados jugando a tu lado en tu propia habitación.


La nueva característica ha sido confirmada por Alden Kroll, uno de los programadores de Valve en su cuenta de Twitter:



> 
> Today our team announced another great new platform feature that will be built into Steam: Remote Play Together. This will allow friends to play local co-op games together over the internet as though they were in the same room together. <https://t.co/jEZyGoXEfc>
> 
> 
> — Alden Kroll @ PAX Australia (@aldenkroll) [October 10, 2019](https://twitter.com/aldenkroll/status/1182142185744953345?ref_src=twsrc%5Etfw)




Como es de esperar, la nueva característica debería llegar en la versión beta del cliente de Steam el próximo 21 de Octubre, aunque aún no sabemos si será necesario algún tipo de requisito en cuanto a sistemas o conexión a internet, ya que en teoría utilizará el ancho de banda de nuestra conexión para servir los datos a nuestros amigos, como la imagen de pantalla o los datos de entrada de los mandos.


Seguiremos al tanto para informaros sobre las novedades de Remote Play Together.


¿Que te parece el nuevo servicio de Steam? ¿Piensas jugar con amigos a tus juegos mediante este servicio? ¿A que juegos te gustaría jugar mediante Remote Play Together?


Cuentamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

