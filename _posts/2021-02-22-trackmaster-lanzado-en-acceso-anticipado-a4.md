---
author: leillo1975
category: Carreras
date: 2021-02-22 15:57:36
excerpt: "<p><span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\"><span\
  \ class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@TrackmasterGame </span></span>,\
  \ el juego creado con @GodotEngine de <span class=\"r-18u37iz\">@saitodepaula</span>\
  \ se actualiza. <span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\"\
  ><br /></span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Trackmaster/Trackmaster012.webp
joomla_id: 1280
joomla_url: trackmaster-lanzado-en-acceso-anticipado-a4
layout: post
tags:
- acceso-anticipado
- godot
- trackmaster
- marcos-saito-de-paula
title: "Trackmaster lanzado en Acceso Anticipado (ACTUALIZACI\xD3N 4 - v0.12) "
---
@TrackmasterGame , el juego creado con @GodotEngine de @saitodepaula se actualiza.   



****ACTUALIZADO 09-8-22:**** La verdad es que hace tiempo que tocaba traeros noticias de los avances de este videojuego, que como sabeis fué lanzado hace más o menos un año y medio. Desde el último reporte que fué casi hace un año, **ha habido varias actualizaciones** que principalmente nos han traido **nuevos modos de juego**, como **Master of the Hill** y **Tag** ([0.9](https://store.steampowered.com/news/app/1536740/view/3061878984177093253)),  **Red Rover** ([0.10](https://store.steampowered.com/news/app/1536740/view/4555948886329302352)), o **Kamikaze Race** ([0.11](https://store.steampowered.com/news/app/1536740/view/3091161894486098782)). Pero no solo ha habido nuevos modos de juego, sino que además de correcciones de bugs , se han incluido **nuevas pistas**, la **cámara en primera persona**, y mejoras en los **gráficos** y su rendimiento. Pero vamos a ir ahora con la versión que es noticia, [la 0.12](https://store.steampowered.com/news/app/1536740/view/3372652091910527055), tal y como descubriamos a través de su canal de twitter:  
  




> 
> Update 0.12: Football!<https://t.co/oD8X53TaZl>[#racinggame](https://twitter.com/hashtag/racinggame?src=hash&ref_src=twsrc%5Etfw) [#Gamedev](https://twitter.com/hashtag/Gamedev?src=hash&ref_src=twsrc%5Etfw) [#Godotengine](https://twitter.com/hashtag/Godotengine?src=hash&ref_src=twsrc%5Etfw) [#Godot](https://twitter.com/hashtag/Godot?src=hash&ref_src=twsrc%5Etfw) [#madewithgodot](https://twitter.com/hashtag/madewithgodot?src=hash&ref_src=twsrc%5Etfw) [#Indiedev](https://twitter.com/hashtag/Indiedev?src=hash&ref_src=twsrc%5Etfw) [#PCgaming](https://twitter.com/hashtag/PCgaming?src=hash&ref_src=twsrc%5Etfw) [#indiegaming](https://twitter.com/hashtag/indiegaming?src=hash&ref_src=twsrc%5Etfw) [#gamingonlinux](https://twitter.com/hashtag/gamingonlinux?src=hash&ref_src=twsrc%5Etfw) [#macOS](https://twitter.com/hashtag/macOS?src=hash&ref_src=twsrc%5Etfw) [#macOSGame](https://twitter.com/hashtag/macOSGame?src=hash&ref_src=twsrc%5Etfw) [#macOSGaming](https://twitter.com/hashtag/macOSGaming?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/SnZoic3I3K](https://t.co/SnZoic3I3K)
> 
> 
> — TrackMaster Free-For-All Motorsport (@TrackmasterGame) [August 8, 2022](https://twitter.com/TrackmasterGame/status/1556636089728864257?ref_src=twsrc%5Etfw)



 Como podeis ver ahora tenemos un **nuevo modo de ¡FUTBOL!** . Emulando a los conocidos [Rocket League]({{ "/tags/rocket-league" | absolute_url }}) o [SuperTuxKart]({{ "/tags/supertuxkart" | absolute_url }}), pero con un toque mucho más "bestia", ahora podemos disfrutar a lo grande con **hasta 10 vehículos de todo tipo en el terreno de juego**, y tratar de llevar el balón gigante a la portería contraria sin ningún tipo de reglas. Además de este modo también hay otras caracteríticas reseñables como **cambiar la orientación de la cámara**, algo superútil para el nuevo modo; el **menú de opciones accesible cuando se está en pausa**; o **mejores gráficos en el Red Rover Arena**, entre otras cosas. Os dejamos con un video que acompaña a esta nueva actualización:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/M2TEJt3Cjwc" style="width: 780px; height: 440px;" title="YouTube video player" width="780"></iframe></div>


 




---


****ACTUALIZADO 28-9-21:**** Llevávamos varios meses sin informaros sobre el estado de este título, y ciertamente se produjeron varias actualizaciones de las que no os hablamos (las [0.6](https://store.steampowered.com/news/app/1536740/view/3019080835939510606) y [0.7](https://store.steampowered.com/news/app/1536740/view/2958284143961578326)) donde se añadieron entre otras muchas cosas **nuevos vehículos y pistas**. Según acabamos de ver en la cuenta de twitter del juego, se ha hecho pública la [versión 0.8](https://store.steampowered.com/news/app/1536740/view/4973652682563183216):  
  




> 
> Update 0.8: events! Racing and FreeStyle. Also, lots of additions and improvements!<https://t.co/oD8X53TaZl>[#racinggame](https://twitter.com/hashtag/racinggame?src=hash&ref_src=twsrc%5Etfw) [#Gamedev](https://twitter.com/hashtag/Gamedev?src=hash&ref_src=twsrc%5Etfw) [#Godotengine](https://twitter.com/hashtag/Godotengine?src=hash&ref_src=twsrc%5Etfw) [#Godot](https://twitter.com/hashtag/Godot?src=hash&ref_src=twsrc%5Etfw) [#madewithgodot](https://twitter.com/hashtag/madewithgodot?src=hash&ref_src=twsrc%5Etfw) [#Indiedev](https://twitter.com/hashtag/Indiedev?src=hash&ref_src=twsrc%5Etfw) [#PCgaming](https://twitter.com/hashtag/PCgaming?src=hash&ref_src=twsrc%5Etfw) [#indiegaming](https://twitter.com/hashtag/indiegaming?src=hash&ref_src=twsrc%5Etfw) [#gamingonlinux](https://twitter.com/hashtag/gamingonlinux?src=hash&ref_src=twsrc%5Etfw) [#macOS](https://twitter.com/hashtag/macOS?src=hash&ref_src=twsrc%5Etfw) [#macOSGame](https://twitter.com/hashtag/macOSGame?src=hash&ref_src=twsrc%5Etfw) [#macOSGaming](https://twitter.com/hashtag/macOSGaming?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/PExs011qgn](https://t.co/PExs011qgn)
> 
> 
> — TrackMaster Free-For-All Motorsport (@TrackmasterGame) [September 28, 2021](https://twitter.com/TrackmasterGame/status/1442665158581657602?ref_src=twsrc%5Etfw)



 


Como podeis observar, en esta ocasión la **actualización es importante** y se han incluido **montones de mejoras y añadidos al juego**, tal y como podeis ver en el siguiente listado:


*-**Se han añadido los Eventos**, de dos tipos diferentes, las **Carreras y Freestyle:** En el primero tan solo tendremos que **tratar de llegar a la linea de meta en primer lugar,** siendo esa la única regla. En un futuro habrá más tipos de carreras. En cuanto al Freestyle, tendrás que realizar **hasta 8 tipos de trucos con los vehículos y alcanzar la mejor puntuación**, existiendo bonificaciones especiales si en el mismo evento realizamos 3 trucos diferentes. Al terminar el tiempo (estandar de 2 minutos, pero configurable) obtendremos calificacaciones de bronce, plata y oro.*


![Freestyle](https://cdn.cloudflare.steamstatic.com/steamcommunity/public/images/clans/39725013/0ba79b5a3831a682f094022433d8a3555aadeb56.jpg)


***-Efectos de público en el estadio:** El estadio de Trackmaster ya no está vacío, y **podrás ver al publico agitando banderas y reaccionando a cada truco que realices** con éxito o a cada fallo en las pruebas de FreeStyle, y a cada golpe y choque en las pruebas de carreras. Puedes configurar en opciones el volumen de sonido de este y la cantidad visible, ya que esto último afecta al rendimiento.*


**-Añadido el Minimapa** en los diferentes escenarios, que nos ayudará a prever el trazado de estos y ver la posición de nuestros contrincantes.


***-Opción de pantalla dividida vertical u horizontalmente:** Esto afecta cuando juegan 2 y 3 jugadores*, pudiendo configurar la disposición de estas a nuestro gusto.


![Splitscreen](https://cdn.cloudflare.steamstatic.com/steamcommunity/public/images/clans/39725013/b8cd18d6a359221957070474070765ea364c0cb2.png)  
  
***-FPS de Físicas:*** Una actualización que afecta a la jugabilidad y al rendimiento es la posibilidad de elegir los FPS (fotogramas por segundo) de la física del juego. Dado que TrackMaster es un juego basado en la física, no basta con ejecutar los cálculos de la física 60 veces por segundo (la velocidad a la que el ordenador actualiza los gráficos del juego, es decir, el renderizado de los gráficos). El FPS original de la física del juego es de 240. Pero como esto puede afectar al rendimiento (los FPS generales del juego), puede que quieras cambiar los FPS de la física a 120, 90 o incluso 60. Según mi experiencia, a 90 FPS, la física se comporta casi como a 240, con pequeñas diferencias que no afectan al juego en general. Con 60 FPS, la física se comporta de manera diferente de forma más perceptible.


-*Los vehículos (incluido el jugador) comienzan la carrera en una **posición aleatoria** de la parrilla.*  
*-Corrección:* *Los **sonidos de choque** ahora funcionan cuando el vehículo está en el aire. Anteriormente, sólo funcionaba con el vehículo en tierra.*  
*-**Actualización de la*** ***Demo**, permitiendo jugar una carrera de 3 vueltas y un evento Freestyle de 2 minutos.*  
*-Problemas conocidos:* *al realizar un evento FreeStyle con el GryoMonster, los flips no se calculan correctamente.*


El desarrollador también ha habilitado un [servidor Discord](https://discord.gg/PypEQQNBpA) para que podamos charlar sobre el juego, reportar errores o compartir nuestras fotos y videos. También ha manifestado que con los **cambios introducidos en la estructura del juego**, a partir de ahora le **será mucho más sencillo lanzar nuevas actualizaciones cada menos tiempo**, con **nuevos tipos de eventos**. Como podeis ver, los cambios en el juego son notables, y así lo pudimos comprobar ayer por la noche, justo tras recibir la actualización en Steam, donde pudimos probar algunas de estas nuevas características, y realmente pintan muy bien. Esperemos que pronto tengamos más noticias de este notable título creado con Godot Engine. Mientras tanto os dejamos con el video lanzado para anunciar esta versión 0.8:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/SRlSdnmtFxQ" style="width: 780px; height: 440px;" title="YouTube video player" width="780"></iframe></div>


---


 **ACTUALIZADO 26-5-21:** Hacía tiempo que no os reportábamos el estado de este juego en Acceso Anticipado, pero desde entonces ya se ha actualizado unas cuantas veces, adquiriendo nuevo contenido y más funciones con cada una de las versiones. A lo largo de estos dos últimos meses pudimos ver como **se añadían vehículos como el "Gyro Monster", o la "Explosivan", y pistas como "Intersection"**,  así como nuevas capacidades entre las que destaca el poder usar **Remote Play Together** y los sonidos de motores o choques, entre otras muchas cosas. Hoy venimos aquí para hablaros de la última recien salida del horno, [la 0.5](https://store.steampowered.com/news/app/1536740?emclan=103582791469246421&emgid=3017953668442678740), que tiene la siguiente lista de cambios:


*-Nuevo vehículo: **TUMVE** (Vehículo de Ultra Movilidad TrackMaster)*  
 *-Añadido: **Volumen maestro** en el menú OPCIONES.*  
 *-Añadido: **Ajuste del volumen de la música** en el menú OPCIONES.*  
 *-Añadido: **Tiempo de juego** en las ESTADÍSTICAS (dentro del menú OPCIONES).*  
 *-Añadido: **Distancia recorrida** en las ESTADÍSTICAS (dentro del menú OPCIONES). Sólo cuenta los datos del jugador 1.*  
 *-Añadido: **Ajustes de guardado**. Las configuraciones de gráficos, audio y juego se guardan cada vez que el jugador sale del menú OPCIONES y se cargan cada vez que se inicia el juego. Los ajustes se guardan en un archivo .cfg, que se encuentra en las siguiente ubicación: "~/.config/godot/app_userdata/TrackMaster"*  
*-Añadido: **Estadísticas**. La información como el TIEMPO JUGADO y la DISTANCIA RECORRIDA (sólo para el jugador 1) son visibles ahora en el menú OPCIONES.*  
 *-Añadido: **Guardar estadísticas**. Estas informaciones se guardan en la misma ruta que los AJUSTES.*  
 ***Problemas conocidos:***  
 *Si se selecciona el CAMIÓN CON REMOLQUE en cualquier arena que no sea la ARENA FREESTYLE, el juego se bloquea, porque las otras arenas no tienen un lugar previsto para el CAMIÓN CON REMOLQUE. Este error está en el juego desde la primera versión en realidad, pero me he dado cuenta recientemente.*


Seguiremos informándoos de las novedades que vaya trayéndonos este **juego creado con el motor Open Source Godot Engine**. Ahora os dejamos con un video del nuevo vehiculo, el TUMVE:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/ma3sFVi19zY" style="display: block; margin-left: auto; margin-right: auto; width: 780px; height: 440px;" title="YouTube video player" width="780"></iframe></div>





---


 **ACTUALIZADO 8-3-21:** Hasta ahora solo era posible adquirir este juego en Acceso Anticipado, en la tienda de [Itch.io](https://trackmaster.itch.io/trackmaster-free-for-all-motorsport) , pero desde hoy mismo **ya se puede adquirir en la conocida tienda de Valve** para todos aquellos la que prefirais. Podeis comprar este juego desarrollado con **Godot Engine** al precio de tan solo **6.59€**:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1536740/" style="display: block; margin-left: auto; margin-right: auto; width: 646px; height: 190px;" width="646"></iframe></div>


Además de esto debeis saber que en los últimos días el juego a llegado a la [versión 0.2](https://trackmaster.itch.io/trackmaster-free-for-all-motorsport/devlog/228340/update-02-sound-and-some-improvements), que incluye los siguientes cambios (copio/pego):


*-¡**Sonido**! Ahora hay una bonita **música de fondo** para el menú, y **sonidos de motor** para los vehículos. Todavía no hay sonido para los choques, y los sonidos del motor necesitan muchas mejoras, pero ya es algo.*  
 *-¡Una **introducción** al iniciar el juego!*  
 *-He **bajado el centro de gravedad de MonsterBike y TriMonster** para que sea más divertido jugar con ellos. También he cambiado sus modelos 3D para que se ajusten a esta nueva configuración.*  
 *-El comienzo de un **sistema de daños**: ¡las puertas, los parachoques y el capó ahora se sueltan en las colisiones! Por ahora, sólo el Monster Truck lo tiene.*


---


**NOTICIA ORIGINAL 22-2-21**:


Menudo sorpresón nos ha dado nuestro amigo [@berarma](index.php/component/search/?searchword=berarma&searchphrase=all&Itemid=828), cuando ayer por la noche compartía un ["post" de reddit](https://www.reddit.com/r/linux_gaming/comments/lp49gl/this_is_my_physics_based_racing_game_trackmaster/?utm_medium=android_app&utm_source=share) donde el **desarrollador brasileño** [Marcos Saito de Paula](https://twitter.com/saitodepaula) nos presentaba el juego en el que está trabajando, y que actualmente se puede adquirir en **Acceso Anticipado** en [itch.io](https://trackmaster.itch.io/trackmaster-free-for-all-motorsport) (en [Steam](https://store.steampowered.com/app/1536740/TrackMaster_Free_For_All_Motorsport/) parece  que estaŕá disponible el día 10 del próximo mes de Marzo).


En la descripción que nos da su creador nos define [Trackmaster](https://www.trackmasterthegame.com) como un "**juego sobre carreras, desorden de vehículos y locos tipos de eventos y competiciones, donde hacer cosas inesperadas con vehículos inesperados**". Suena bien, ¿verdad? Podremos mezclar en la misma pista montones de vehículos diferentes y experimentar con ellos, con sus físicas y por supuesto pasar un buen rato.


Como sigue comentando el **único desarrollador** del proyecto, el juego surgió de la **necesidad de crear un juego donde se aunasen diferentes características de los títulos que a él le gustan**, por lo que trata de tomar la física de los clásicos **Destruction Derby**, la libertad de **Interstate'76** y del moderno **BeamNG.Drive**, los locos modos de T**est Drive: Eve of Destruction**, el caos de **Twisted Metal**, los vehículos de **Monster Truck Madness**; y todo ello aderezado con multijugador a pantalla partida. Como podeis con estas influencias solo podremos encontrar **un juego donde la locura y el desenfreno están por todas partes**.


El título pone **mucho énfasis en las físicas**, intentando situarse **a medio camino entre simulación y diversión**: los vehículos se rigen totalmente por la física, de modo que siempre se puede ver un resultado diferente de las colisiones, los saltos y las acrobacias. Los **tamaños y pesos de los vehículos también juegan un papel importante** como en la vida real. Si chocamos un vehículo de grandes dimensiones contra un pequeño utilitario, el segundo saldrá despedido bien lejos, mientras que el primero ni se inmutará; pero poniendo el ojo en la parte arcade del juego, este gran vehículo podrá alcanzar grandes velocidades y además en tiempo record. Es importante comentar que **la física variará según sea la tracción del coche**: RWD (tracción trasera), FWD (tracción delantera) y 4WD (tracción total).


Trackmaster por supuesto, contará en un futuro con el **modo multijugador, tanto online, como a pantalla partida con hasta cuatro jugadores,** pudiendo estos usar tanto el teclado y ratón como mandos de control. Se planean también **diferentes modos de juego, como Eventos**, con **14 tipos** diferentes, **Modo Carrera**, **Torneos** y un modo de **conducción libre**. Habrá **11 tipos diferentes de circuutos y arenas**, y **17 vehículos diferentes**. Los coches podrán ser manejados desde **diferentes tipos de cámara**, incluida la subjetiva, y también será posible **crear tus propios mapas** para poder disfrutar en ellos usando su editor. Si quereis ver la **hoja de ruta** que se seguirá para el desarrollo del juego podeis verla [aquí](https://www.trackmasterthegame.com/planning).


Hablando de los mimbres con los que está hecho el juego, cabe destacar que se ha utilizado como herramienta para crearlo, el **motor Open Source [Godot](https://godotengine.org/)** , que tanto está avanzando y tantas alegrías nos está dando. Como podeis ver en este [enlace](https://godotforums.org/discussion/22177/racing-game-trackmaster-free-for-all-motorsport) el juego cuenta con sección el la web de Godot Engine.


En este momento [podeis comprar el juego en Early Access en Itch.io](https://trackmaster.itch.io/trackmaster-free-for-all-motorsport) a un precio de 5$ (o lo que querais poner a más), y también podeis descargar una **demo** por si quereis probarlo antes. Os dejamos con un video del juego:  
  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/Z7rwenn7gCA" style="display: block; margin-left: auto; margin-right: auto; width: 780px; height: 440px;" width="780"></iframe></div>


¿Que os parece lo visto hasta ahora en Trackmaster? ¿Os gustan este tipo de juegos? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

