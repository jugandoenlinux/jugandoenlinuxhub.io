---
author: Son Link
category: "Acci\xF3n"
date: 2021-09-24 21:22:20
excerpt: "<p>El popular FPS sigue ampli\xE1ndose con m\xE1s modos y caracter\xEDsticas</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/csgo/csgoriptide.webp
joomla_id: 1366
joomla_url: nuevo-contenido-para-counter-stike-global-offensive-operacion-riptide
layout: post
tags:
- accion
- csgo
title: "Nuevo contenido para Counter-Stike Global Offensive: Operaci\xF3n Riptide"
---
El popular FPS sigue ampliándose con más modos y características


VALVe ha anunciado una nueva operación para Counter-Strike: Global Offensive, la **Operación Riptide**.


Esta nueva operación trae nuevos mapas, agentes, pegatinas, entre otras novedades, entre las que están:


* **Colas privadas de matchmaking**: Ahora puedes crear colas privadas para competir, mediante un código para compartir con tus amigos o seleccionando uno de los grupos en los que estés.
* **Ahora puedes seleccionar la duración de la partida**: Corta (16 rondas), larga (30 rondas) o sin preferencias.
* **Ahora puede seleccionar** entre Deathmatch clásico, Deathmatch por equipos (el primer equipo en conseguir 100 víctimas gana) y Deathmatch batalla campal (todos los jugadores son enemigos).
* **5 nuevos mapas**
* **Las partidas de Demolición son ahora más cortas** (10 rondas), no hay cambios de equipo a mitad de la partida, reducido el tiempo de cada ronda y ajustado la progresión de armas.
* **En el modo Carrera de Armamento se ha ajustado la progresión** de armas y recibes una inyección de salud por cada 3 enemigos abatidos.
* **Ahora es posible soltar las granadas del mismo modo que con las armas**.
* **Varios ajustes de jugabilidad, varias armas y mapas**


![csgoriptide1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/csgo/csgoriptide1.webp)


Para poder jugar necesitas tener el estado Prime o [adquirir el pase de temporada](https://store.steampowered.com/app/1766730/CSGO__Operation_Riptide/), aunque solo podrás adquirir las recompensas mediante ese pase, pero no es necesario adquirirlo desde el inicio.


![csgoriptide2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/csgo/csgoriptide2.webp)


Si quieres unirte nuestras partidas del matchmaking privado [unete a nuestro grupo](https://steamcommunity.com/groups/jugandoenlinux). Os esperamos a todos/as, y feliz caza.


Teneís más información [en la web oficial](https://counter-strike.net/riptide) y [y en la página de noticias](https://store.steampowered.com/news/app/730/view/2898619694828496727) de Steam.

