---
author: leillo1975
category: "Simulación"
date: 04-10-2023 18:11:04
excerpt: "El equipo de desarrollo de este juego #OpenSource publica las últimas novedades"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/kurpfalzring_720.webp
joomla_id: 1544
joomla_url: speed-dreams-lanza-versiones-experimentales-con-importantes-mejoras
layout: post
tags:
- open-source
- speed-dreams
- appimage
- experimental
title: "Speed Dreams lanza una versión experimental con importantes mejoras"
---

Realmente no ha pasado mucho tiempo desde el lanzamiento de la última versión oficial de este simulador de carreras de coches libre, poco más de medio año, pero en cambio **las mejoras introducidas en el juego tienen una importancia tremenda**, como veréis más adelante. Ciertamente esto tiene truco, porque digamos que muchas de ellas estaban guardadas en el cajón previamente al lanzamiento oficial de la [versión 2.3.0]({{ "/posts/la-version-2-3-0-de-speed-dreams-lanzada-oficialmente" | absolute_url }}), pero no se introdujeron por necesitar más optimización y correcciones de fallos. El caso es que a los pocos días de lanzarse la versión antes mencionada, comenzaron a caer en cascada todos estos desarrollos.

Todo el mundo que esté al tanto de los simuladores de carreras (simracing), sabe que uno de los aspectos fundamentales de este tipo de juegos es lo que se viene en llamar el **Modelo de Neumáticos** (Tyre Model). Aspectos como la **temperatura, las presiones, el desgaste o las fuerzas que afectan al agarre son de extremada importancia** si se quiere realizar una representación fidedigna de lo que se puede sentir en un coche de carreras real. Hasta ahora  el juego contaba con un "Tyre Model" que no acababa de convencer a sus desarrolladores, y que mostraba determinadas inexactitudes y carencias que se hacían evidentes al compararlo con la realidad o con otros juegos del género. Debido a esto se decidió hacer los cambios necesarios en el código para que esta característica fuese más real, y para ello se decidió tomar como punto de partida el trabajo de **Bernhard Wymann** de [TORCS](https://sourceforge.net/projects/torcs/) (Speed Dreams es un fork de este juego). Además de esto **otros cambios importantes se han hecho durante estos meses son:**

* Inclusión de **5 diferentes compuestos de neumáticos** (1-Blando, 2-Medio, 3-Duro, 4-Mojado y 5-Mojado Extremo). Por ahora solo funcionan en las categorías de MPA11 y MPA12
* Añadidas **3 pistas nuevas** como **Kurpfalzring** (basada en el antiguo Hockenheim), **Jarama** y **Atlanta** (WIP)
* Actualizadas las pistas de **Braga** y **Dijon-Prenois** con muchas correcciones, nuevos objetos, cámaras y mejoras varias.  
    ![Braga](https://www.speed-dreams.net/wp-content/uploads/2023/09/braga.jpg)
* **Quitado el "Recorded Time"** y **añadidas las estaciones**. **Ahora los datos meteorológicos se toman de servidores con información real** mediante una fórmula basada en la estación, la hora y el día.
* El **HUD** para el motor gráfico OpenSceneGraph (OSG) ha sido reescrito completamente y además de mostrar una **imagen mucho más actual y pulida**, tiene **nuevos widget**s, como el del tiempo. **Es posible además personalizar la posición de estos** pulsando Ctrl+8 durante la carrera.
* Muchísimos cambios en **Trackeditor**, incluyendo la habilidad de añadir y editar objetos individuales, limpiar objetos adjacentes, realizar correcciones automáticas interactivas en los segmentos, además de montones de mejoras y correcciones en trackgen y accc.  
![Trackeditor](https://www.speed-dreams.net/wp-content/uploads/2023/09/trackeditor_.jpg)
* **El motor de físicas** (Simu 4.1) **ha sido en parte reescrito para añadir multiplicadores de calor en las fuerzas longitudinales y laterales**. Por ahora solo funciona en la version Test del MPA11, y en la categoría LS-GT1 Test.

Es importante avisar que **el juego solo funciona en esta versión experimental en los niveles de dificultad "Semi-Pro" y "Pro"**, y que están trabajando para que esté disponible en todas. También hay que resaltar que solo las categorías MPA11, MPA12 y Supercars están optimizadas para el nuevo modelo de neumáticos.

Como veis el adjetivo "experimental" no está puesto a la ligera y todo es susceptible de mejoras y correcciones, pero seguro que más de uno está deseando probar las nuevas características a pesar de todo. Para ello **nuestro amigo @SonLink se ha vuelto a "currar" una nueva AppImage** **que contiene todo el contenido oficial del juego más el "Work In Progress" (WIP)**. Podéis descargaros esta AppImage desde estos sitios:

* [AppImageHub](https://www.appimagehub.com/p/1666010/)
* [itch.io](https://speed-dreams.itch.io/speed-dreams/devlog/609465/development-news-oct-2023)
* [mod.db](https://www.moddb.com/games/speed-dreams/downloads/speeddreams-experimental-appimage-20230930-r9158#downloadsform)

Para que tengáis una idea más aproximada de lo que vais a encontrar podéis echarle un ojo a este video de su canal oficial de Youtube:

<div class="resp-iframe">
	<iframe src="https://www.youtube.com/embed/567LZSudjr8?si=jvDImekk9dEZf2VR" width="780" height="440" title="YouTube video player" allowfullscreen="allowfullscreen" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"></iframe>
</div>

Tenéis más información sobre esta actualización en este artículo de su página oficial: [Development News (Oct-2023)](https://www.speed-dreams.net/en/development-news-2023-09/)