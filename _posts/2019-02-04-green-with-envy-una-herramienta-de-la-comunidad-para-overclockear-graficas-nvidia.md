---
author: Serjor
category: Noticia
date: 2019-02-04 21:13:47
excerpt: "<p>Nuevas opciones de overclock para las gr\xE1ficas NVIDIA</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/53eba983c09b2ad2c10bfc8ddb9a07aa.webp
joomla_id: 965
joomla_url: green-with-envy-una-herramienta-de-la-comunidad-para-overclockear-graficas-nvidia
layout: post
tags:
- nvidia
- nvidux
- overclock
- gwe
- green-with-envy
title: "Green With Envy, una herramienta de la comunidad para overclockear gr\xE1\
  ficas NVIDIA"
---
Nuevas opciones de overclock para las gráficas NVIDIA

Nos enteramos gracias a [OMG! Ubuntu!](https://www.omgubuntu.co.uk/2019/02/easily-overclock-nvidia-gpu-on-linux-with-this-new-app) de que tenemos disponible una nueva herramienta para hacer overclock a nuestras gráficas NVIDIA. Si hace unos meses os [hablábamos](index.php/homepage/hardware/15-hardware/1033-nvidux-una-herramienta-grafica-para-overclockear-graficas-nvidia) de NVIDUX, en esta ocasión os traemos [Green With Envy](https://gitlab.com/leinardi/gwe) (verde de envidia), la cuál es una herramienta "diseñada para dar información, controlar los ventiladores y overclockear nuestras gráficas NVIDIA".


La verdad es que la herramienta tiene muy buena pinta, y además, se puede instalar vía flatpak, cosa que simplifica la instalación. Como podemos ver en su página de gitlab, la aplicación tiene una lista de [tareas pendientes](https://gitlab.com/leinardi/gwe#todo) pero parece que la cosa avanza a buen ritmo.


Siempre es de agradecer que aparezcan este tipo de utilidades para exprimir al máximo nuestras gráficas, pero es triste que tenga que ser la comunidad la que haga el trabajo de los demás.


Y tú, ¿ya has probado GWE? Cuéntanos cuantos FPS extra eres capaz de obtener en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

