---
author: Jugando en Linux
category: Editorial
date: 2022-01-01 18:45:44
excerpt: "<p>Hacemos un peque\xF1o resumen de los que fueron los grandes juegos del\
  \ pasado a\xF1o.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen2021/resumen_2021.webp
joomla_id: 1403
joomla_url: los-juegos-de-2021-para-jugandoenlinux
layout: post
tags:
- goty
- resumen
- '2021'
title: Los juegos de 2021 para JugandoEnLinux
---
Hacemos un pequeño resumen de los que fueron los grandes juegos del pasado año.


**Son Link:**
-------------


**[VALHEIM](https://www.humblebundle.com/store/valheim?partner=jugandoenlinux):** Sin duda alguna **Valheim ha sido uno de los grandes éxitos, rompiendo 2 récords** (en numeró de ventas tras su salida y cantidad de jugadores al mismo tiempo), que empezó saliendo en Acceso Anticipado y que aun esta en continuo desarrollo, publicándose este año [su primera gran expansión, Heart & Home]({{ "/posts/hearth-home-la-primera-gran-actualizacion-de-valheim" | absolute_url }}), si bien con algo de retraso, pero con jugosas novedades, añadiendo nuevas recetas, herramientas y material de construcción.   
Su gran atractivo es el de poder explorar un basto mundo, con diversas zonas (los biomas) donde nos esperan nuevos recursos para mejorar nuestro equipo, nuevas herramientas y materiales de construcción, y nuevos enemigos, que nos pondrán las cosas difíciles al principio, y los 5 jefes finales, ademas de, gracias a diversas piedras que iremos encontrando, nos iremos enterando de la historia de Valheim.   
Valheim se puede disfrutar solo o en compañía, si bien como mejor se disfruta es en compañía. Hay muchos servidores, o puedes crear el tuyo y que se unan a el tus amigos y/o familiares, de hecho esta comunidad [tiene un servidor propio](index.php/foro/valheim) al que puedes unirte.  
Si te gustan los juegos de supervivencia, te enganchaste a Legend of Zelda: Brave of the Wild, o Minecraft, sin duda, este juego es para ti**.  
Por cierto PC Gamer ya ha nombrado a [Valheim como juego del año](https://www.pcgamer.com/game-of-the-year-2021-valheim/).**<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/mH9g9o_8ofo" title="YouTube video player" width="780"></iframe></div>


**[THE BATTLE OF WESNOTH](https://www.wesnoth.org/):** Este gran clásico libre (hablo de un juego que empezó su desarrollo en 2003) **recibió este año [una nueva gran actualización]({{ "/posts/la-batalla-por-wesnoth-1-16" | absolute_url }}), la primera tras estar disponible en Steam**, lo cual le ha sentado muy bien ya que trae muchos cambios, como nuevos mapas, mejoras de la Inteligencia Artificial, re-equilibradas varios mapas, campañas y facciones, cambios gráficos, y un largo etcétera. El juego trae varias campañas, algunas cortas, y otras que te llevaran muchas horas, y si añadimos los modos multijugador, y la posibilidad de descargar más mapas y campañas, tienes horas y horas de juego. Altamente recomendado si te gustan los juegos de estrategia por turnos, aunque siendo gratis, puedes probarlo sin compromiso.  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/4Ebww6utt9I" title="YouTube video player" width="780"></iframe></div>


**Serjor**
----------


**[LOOP HERO](https://www.humblebundle.com/store/loop-hero?partner=jugandoenlinux):** En mi caso es muy raro jugar a un juego en el mismo año que ha salido, pero afortunadamente para mí así ha sido (no nos vamos a engañar, gracias a Epic lo pude probar, y me gustó tanto que me lo he comprado de oferta en Steam).  
Es muy difícil describir este juego, **pero como poco os diré que es un roguelike, que sabe picar muy bien para echar una partida más, y que la sencillez en sus mecánicas lo hacen muy, pero muy, adictivo.** Entre sus mecánicas encontramos un deck builder, una pizca de RPG y un acercamiento un tanto peculiar al farmeo en el que además en cada expedición el tablero de juego cambia, y dónde pongamos nuestras cartas darán un resultado u otro en función de qué carta hayamos puesto al lado. No es brillante en ninguna de las mecánicas, pero es una muestra clara de que el resultado es mayor a la suma de las partes, porque consigue la excelencia a base de pequeños destellos de calidad en cada mecánica.  
También puedo deciros que la descripción que he hecho de Loop Hero no le hace nada de justicia, y que lo mejor que podéis hacer es jugarlo (o no, si no queréis quedaros sin tiempo libre)  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/7P58L0AVIEM" title="YouTube video player" width="780"></iframe></div>


[**HOLLOW KNIGHT**](https://www.humblebundle.com/store/hollow-knight?partner=jugandoenlinux): ¿Qué se puede decir de esta maravilla de juego que no se haya dicho ya? Quizás que salió para GNU/Linux en 2017, pero que en mi caso particular este ha sido para mi el año de Hollow Knight **una mezcla de metroidvania con dark souls** en 2D que es bastante más que esos dos géneros juntos, pero que de alguna manera recogen qué te puedes encontrar.  
Con que **su secuela, Silksong, sea la mitad de buena, ya será el GOTY del año en el que salga...**  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/pFAknD_9U7c" title="YouTube video player" width="780"></iframe></div>


**Leillo:**
-----------


[**SUPER WODEN GP:**](https://store.steampowered.com/app/1534180/Super_Woden_GP/) Este juego, como sabeis los que lo conoceis no cuenta con un gran presupuesto, unos gráficos hiperrealistas y un buen puñado de licencias, pero **tiene lo que a algunos le falta, toneladas de diversión al más puro estilo de los arcades de los años 90'**. Con referencias claras a World Rally Championship (el Carlos Sainz, pa entendernos), o al primer Gran Turismo de PSX, en este título podremos conducir toneladas de coches basados en modelos míticos de hace unas décadas, en las más intrincadas pistas, y con un buen puñado de modos. Un sobresaliente en toda regla, tal y como podeis ver en el [Análisis que le dedicamos hace unos meses]({{ "/posts/analisis-super-woden-gp" | absolute_url }}).  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/v-Zwd82IBVQ" title="YouTube video player" width="780"></iframe></div>


**[SPEED DREAMS](https://www.speed-dreams.net/):** De nuevo el simulador de conducción libre por excelencia se vuelve a actualizar, [alcanzando la versión 2.2.3]({{ "/posts/speed-dreams-alcanza-oficialmente-la-version-2-2-3" | absolute_url }}) y con el un buen montón de novedades más que interesantes, como los nuevos monoplazas MP1, basados en la F1 del 2005; el circuito de Sao Paulo, la categoría de coches 1967 GP, o el desgaste y degradación de neumáticos. Recientemente también han lanzado una AppImage experimental (gracias Son Link) donde además podéis encontrar otro buen puñado de novedades como podéis ver en [este artículo de su blog](https://www.speed-dreams.net/en/christmas-developers-news-2021-12/).  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/wwQxyzsAkUM" title="YouTube video player" width="780"></iframe></div>


**P_Vader:**
-------------


**[SPLITGATE](https://store.steampowered.com/app/677620/Splitgate/)**: creo que  ****ha sido una revolución en el 2021**** **para nuestra plataforma, este frenético FPS estilo Halo con el añadido de los portales del mítico Portal**. Empezó con un inesperado éxito desmedido que poco a poco se fue normalizando. Podemos pensar que se trató de un "hype" pero como suele pasar con muchos juegos tienden ha normalizarse pasada la gran novedad y Splitgate sigue teniendo una muy buena base de usuarios. Es **gratis, multiplataforma y sobre todo es realmente original**, algo que se echa mucho de menos en este tipo de juegos.  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="480" src="https://www.youtube.com/embed/AMSN6DxhXuA" title="YouTube video player" width="780"></iframe></div>


[**VELOREN**](https://veloren.net/): Es un proyecto libre que creo que **esta alcanzando una calidad y madurez difícil de encontrar en el mundo del video juego** libre. Como pudimos leer en [la entrevista que le hicimos]({{ "/posts/entrevista-con-forest-anderson-de-veloren" | absolute_url }}), Veloren tiene unas metas bastantes ambiciosas y muy bien pensadas.  
Es un **RPG en un mundo abierto pixelado inspirado en Cube World, Zelda o Minecraft, con la posibilidad de jugarlo en modo multijugador masivo** en sus servidores. Deberías darle una oportunidad a este juegazo, sobre todo si te gusta este genero. **Veloren puede competir de tu a tu con cualquier gran juego comercial.**  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/Vlst_UtpiQg" title="YouTube video player" width="780"></iframe></div>




---


 


¿Que os parece esta pequeña lista de 2021 que compartimos con vosotros desde JuegandoEnLinux? **Cuéntenos tus juegos de 2021 en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).**

