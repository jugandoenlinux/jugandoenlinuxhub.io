---
author: Serjor
category: Carreras
date: 2018-08-02 16:13:39
excerpt: "<p>Aunque por el momento, el modo multiplayer on-line no est\xE1 listo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d7115f2e533cc8bcf91823021f4ba6a2.webp
joomla_id: 814
joomla_url: los-desarrolladores-de-supertuxkart-nos-adelantan-algunas-de-las-novedades-en-las-que-estan-trabajando
layout: post
tags:
- super-tux-kart
- supertuxkart
title: "Los desarrolladores de SuperTuxKart nos adelantan algunas de las novedades\
  \ en las que est\xE1n trabajando"
---
Aunque por el momento, el modo multiplayer on-line no está listo

Gracias a la gente de [OMG! UBUNTU!](https://www.omgubuntu.co.uk/2018/08/supertuxkart-devs-detail-several-upcoming-improvements), llegamos a un post de los desarrolladores de Supertuxkart en el que nos [comentan](http://blog.supertuxkart.net/2018/08/summer-update.html) en qué están trabajando de cara a la próxima release.


Como ellos mismos nos advierten, por ahora no hay nada de juego en red, aunque están trabajando en ello, pero lo que sí podremos encontrar será por ejemplo un multijugador local de hasta 8 jugadores, aunque habrá que ver el tamaño de pantalla necesario para poder jugar 8 jugadores a la vez. Como ellos mismos indican, hará falta un buen hardware para mover los 8 jugadores en un mismo equipo.


También hay pequeñas mejoras, como un feedback al jugador sobre cuándo se va a terminar el escudo, mejoras en el motor gráfico, mejoras en algunos elementos visuales y sonoros, y correcciones de pequeños bugs.


 


La verdad es que se agradecen estas pequeñas notas, ya que nos recuerdan que el juego sigue en constante desarrollo.

