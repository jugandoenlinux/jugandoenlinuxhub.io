---
author: leillo1975
category: "Exploraci\xF3n"
date: 2020-03-27 16:22:44
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >@undermine_game ya tiene fecha de salida oficial<br /></span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Undermine/undermine_w_logo.webp
joomla_id: 1198
joomla_url: undermine-un-roguelike-en-acceso-anticipado-a1
layout: post
tags:
- acceso-anticipado
- roguelike
- undermine
- thorium
- fandom
title: Undermine, un roguelike en Acceso Anticipado (ACTUALIZADO)
---
@undermine_game ya tiene fecha de salida oficial  



**ACTUALIZADO 8-7-20:** StridePr nos ha informado (hace unos días, perdón por el retraso) que finalmente los chicos de [Thorium Entertaiment](https://undermine.game/thorium) lanzarán la **versión final de Undermine el día 6 de Agosto**, poniendo fin así al periodo de Acceso Anticipado. El juego será lanzado en múltiples plataformas entre las que se incluye GNU-Linux consiguiendo una **nada despreciable base de jugadores de más de 120.000 usuarios**. Según el anúncio oficial esto es lo que podremos encontrarnos en la **versión 1.0**, su mayor actualización hasta ahora:


-Lucha final contra el jefe con numerosas fases y escenas de corte  
 -Una secuencia final que ata la historia...  
 -El diálogo de los personajes para los estados posteriores a Golden Core, Seer, y el jefe final  
 -Nuevos artículos  
 -Mejora del rendimiento  
 -Un nuevo sistema de desafíos que permite a los jugadores hacer el juego tan difícil como quieran  
 -Los artefactos añadidos al diario junto con las entradas para cada uno  
 -Las estadísticas de vida se añadieron al diario.  
 -Funcionalidad mejorada del mini-mapa, incluyendo la posibilidad de ver un piso entero en vista ampliada  
 -Un "paquete de cuidados" con capucha que hace que los viajes rápidos sean más valiosos  
 -Revisión visual de la última sala, así como la "Death’s Head" (ahora "Death’s Hand")  
 -Mejora visual de las Cavernas Brillantes  
 -Numerosas correcciones de errores y cambios de equilibrio


Actualizaremos esta noticia con el lanzamiento de la versión final. En este momento [podeis comprarlo con un interesante descuento del 25%](https://store.steampowered.com/app/656350/UnderMine/) con motivo del Summer Sale, costando tan solo **9.36€**. Sabeis que nos gusta acompañar los artículos con videos, y por eso aquí os dejamos con el trailer de lanzamiento:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/58ral7Vy1lM" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL:**  
Vamos a hablaros de un juego que seguramente no es ninguna novedad para vosotros, pues lleva desde el verano pasado en las estanterias digitales de [Steam](https://store.steampowered.com/app/656350/UnderMine/), pero que aquí un servidor, desconocía su existencia hasta que hace un momento recibíamos un email por parte de [StridePr](https://www.stridepr.com/) , que me enviaba información sobre el lanzamiento de la última actiualización del juego apodada "Othermine".


Undermine es un **roguelike de acción y aventuras en 2D**, con una **pizca de RPG** y aspecto **retro**, en el que nos encontraremos un mundo repleto de secretos. Tendemos que **extraer oro** en cada salida que hagamos y **encontrar reliquias para aumentar el poder** de nuestro campesino. También debemos **rescatar diversos personajes** durante nuestra aventura que nos **desbloquearan tiendas** donde comprar objetos especiales. Al **final de cada area** nos encontraremos un **Jefe** que pondrá a prueba a nuestro héroe. En esta **ultima actualización "Otherlike" 0.6.0** , se añade un **nuevo modo de "picaro"**, además los jugadores pueden desbloquear **nuevas mejoras**, entre las que destacan las **Botas de Explorador** que aumentan la velocidad del heroe cuando no está en combate, y un nuevo **Espejo Mágico en la Biblioteca de Arkanos** que aleatoriza el aspecto del campesino. Afrontaremos igualmente **nuevos desafíos** y descubriremos muchos **nuevos encuentros y secretos**.


El juego está desarrollado por [Thorium](https://undermine.game/#thorium), un pequeño **estudio canadiense fundado por ex-empleados de Relic**, y está editado por [Fandom](https://www.fandom.com/). El juego saldrá de Acceso Anticipado a finales de este año. Os dejamos con un video de esta última versión:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/D-rav-N_x8c" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


¿Os gustan este tipo de juegos de aspecto retro y con esta temática? ¿Qué os parece la propuesta de Thorium? Podeis darnos veustra opinión sobre Undermine en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

