---
author: leillo1975
category: Editorial
date: 2017-06-01 05:42:00
excerpt: "<p>Hace unos a\xF1os ni lo hubieramos so\xF1ado, pero a d\xEDa de hoy la\
  \ verdad es que podemos estar contentos de alcanzar una cifra como la actual. Gracias\
  \ a la apuesta serie de Valve en ofrecernos su cliente y a la presi\xF3n ejercida\
  \ a las compa\xF1\xEDas podemos&nbsp; ahora estar jugando en nuestros PC's sin necesidad\
  \ de instalar Windows o usar Wine. En este tweet de la cuenta&nbsp;<a href=\"https://twitter.com/SteamOnLinux\"\
  \ class=\"ProfileHeaderCard-screennameLink u-linkComplex js-nav\"><span class=\"\
  username u-dir\" dir=\"ltr\">@<b class=\"u-linkComplex-target\">SteamOnLinux</b></span></a>&nbsp;\
  \ podemos ver como hemos llegado a esta redonda cifra:</p>\r\n<p>&nbsp;</p>\r\n\
  <blockquote class=\"twitter-tweet\" data-lang=\"es\">\r\n<p dir=\"ltr\" lang=\"\
  en\">There are now 3500 games for Linux on Steam. <a href=\"https://twitter.com/hashtag/SteamGamesForLinux?src=hash\"\
  >#SteamGamesForLinux</a></p>\r\n\u2014 SteamGamesOnLinux (@SteamOnLinux) <a href=\"\
  https://twitter.com/SteamOnLinux/status/870075225504976899\">1 de junio de 2017</a></blockquote>\r\
  \n<script src=\"//platform.twitter.com/widgets.js\" async=\"\" type=\"text/javascript\"\
  \ charset=\"utf-8\"></script>\r\n<p>&nbsp;</p>\r\n<p>Obviamente otros sistemas disponen\
  \ de m\xE1s cat\xE1logo, y por supuesto de juegos de m\xE1s factura; pero debemos\
  \ estar muy satisfechos de poder <a href=\"http://store.steampowered.com/search/?category1=998&amp;os=linux\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\">disponer en nuestra biblioteca</a>\
  \ de tal cantidad de t\xEDtulos.&nbsp; Obviamente, la gran mayor\xEDa de ellos son\
  \ juegos indies y de bajo presupuesto, con algunas-muchas honrosas excepciones de\
  \ juegos AAA tales como el reciente <a href=\"index.php/homepage/generos/estrategia/item/456-total-war-shogun-2-y-fall-of-the-samurai-disponibles-para-linux-steamos\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\">TW: Shogun 2</a>, <a href=\"index.php/top-linux/item/203-tomb-raider-2013\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\">Tomb Raider</a>, <a href=\"index.php/homepage/analisis/item/362-analisis-dirt-rally\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\">Dirt Rally</a> o <a href=\"index.php/homepage/analisis/item/352-analisis-sid-meier-s-civilization-vi\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\">Civilization VI</a>. Esperemos que\
  \ en un futuro vengan m\xE1s y mejores. Nosotros como siempre estaremos aqu\xED\
  \ para contartelo.</p>\r\n<p>&nbsp;</p>\r\n<p>\xBFQue te parece el cat\xE1logo al\
  \ que podemos acceder en Steam? \xBFVes un futuro prometedor a nuestra plataforma?\
  \ Cuentanoslo en los comentarios o usa nuestros canales de <span style=\"color:\
  \ #000080;\"><a href=\"https://telegram.me/jugandoenlinux\" target=\"_blank\" rel=\"\
  noopener noreferrer\" style=\"color: #000080;\">Telegram</a></span> y <span style=\"\
  color: #000080;\"><a href=\"https://discord.gg/ftcmBjD\" target=\"_blank\" rel=\"\
  noopener noreferrer\" style=\"color: #000080;\">Discord</a></span>.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/08b282bed88832c9197a25b1ea22b623.webp
joomla_id: 341
joomla_url: steam-llega-a-los-3500-juegos-en-gnu-linux-steamos
layout: post
tags:
- steamos
- steam
- valve
- linux
- aaa
title: Steam llega a los 3500 juegos en GNU-Linux/SteamOS
---
Hace unos años ni lo hubieramos soñado, pero a día de hoy la verdad es que podemos estar contentos de alcanzar una cifra como la actual. Gracias a la apuesta serie de Valve en ofrecernos su cliente y a la presión ejercida a las compañías podemos  ahora estar jugando en nuestros PC's sin necesidad de instalar Windows o usar Wine. En este tweet de la cuenta [@**SteamOnLinux**](https://twitter.com/SteamOnLinux)  podemos ver como hemos llegado a esta redonda cifra:


 



> 
> There are now 3500 games for Linux on Steam. [#SteamGamesForLinux](https://twitter.com/hashtag/SteamGamesForLinux?src=hash)
> 
> 
> — SteamGamesOnLinux (@SteamOnLinux) [1 de junio de 2017](https://twitter.com/SteamOnLinux/status/870075225504976899)



 


Obviamente otros sistemas disponen de más catálogo, y por supuesto de juegos de más factura; pero debemos estar muy satisfechos de poder [disponer en nuestra biblioteca](http://store.steampowered.com/search/?category1=998&os=linux) de tal cantidad de títulos.  Obviamente, la gran mayoría de ellos son juegos indies y de bajo presupuesto, con algunas-muchas honrosas excepciones de juegos AAA tales como el reciente [TW: Shogun 2](index.php/homepage/generos/estrategia/item/456-total-war-shogun-2-y-fall-of-the-samurai-disponibles-para-linux-steamos), [Tomb Raider](index.php/top-linux/item/203-tomb-raider-2013), [Dirt Rally](index.php/homepage/analisis/item/362-analisis-dirt-rally) o [Civilization VI](index.php/homepage/analisis/item/352-analisis-sid-meier-s-civilization-vi). Esperemos que en un futuro vengan más y mejores. Nosotros como siempre estaremos aquí para contartelo.


 


¿Que te parece el catálogo al que podemos acceder en Steam? ¿Ves un futuro prometedor a nuestra plataforma? Cuentanoslo en los comentarios o usa nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

