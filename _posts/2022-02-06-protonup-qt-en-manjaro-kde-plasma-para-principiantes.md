---
author: P_Vader
category: "V\xEDdeos JEL"
date: 2022-02-06 13:25:29
excerpt: "<p><span class=\"style-scope yt-formatted-string\" dir=\"auto\">Peque\xF1\
  o tutorial de como instalar versiones personalizadas de Proton f\xE1cilmente con\
  \ ProntonUp-Qt, en Manjaro con KDE Plasma para usuarios principiantes.</span></p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Videotutoriales/protonup-qt.webp
joomla_id: 1424
joomla_url: protonup-qt-en-manjaro-kde-plasma-para-principiantes
layout: post
tags:
- steam
- proton
- tutorial
- videotutorial
- manjaro
- kde
- plasma
- protonup-qt
- proton-ge
title: ProtonUp-Qt en Manjaro KDE Plasma para principiantes
---
Pequeño tutorial de como instalar versiones personalizadas de Proton fácilmente con ProntonUp-Qt, en Manjaro con KDE Plasma para usuarios principiantes.



 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="480" src="https://www.youtube.com/embed/aGTMozMbpPE" title="YouTube video player" width="770"></iframe></div>


 


 

