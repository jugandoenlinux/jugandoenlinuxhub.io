---
author: Serjor
category: "Acci\xF3n"
date: 2017-11-17 16:58:28
excerpt: "<p>M\xE1s contenido para un juego que no deja de actualizarse</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bbfb4faa18ee2951b02b656fb34be1d7.webp
joomla_id: 545
joomla_url: vuelven-los-objetivos-especiales-a-hitman
layout: post
tags:
- accion
- feral-interactive
- hitman
- io-interactive
title: Vuelven los Objetivos Especiales a Hitman
---
Más contenido para un juego que no deja de actualizarse

Leemos en el [blog de Feral](https://www.feralinteractive.com/es/news/832/) que la versión para GNU/Linux y Mac de Hitman reactiva los Objetivos Escurridizos. Estos objetivos son misiones que suceden en paralelo a la historia principal y que nos dan algunos bonus como experiencia.


Cada misión se activa durante un periodo de tiempo concreto y no se vuelve a repetir, salvo que como en esta ocasión, las reactiven. Eso sí, tal y como advierten los chicos de Feral, si ya los tenéis completados (con éxito o fracaso) no podréis volver a jugarlos.


La verdad es que el que después de que IO Interactive, la desarrolladora del juego, se desvinculara de Square Enix, la editora, había algún temor con lo que pasaría tanto con el juego, como en particular con la versión para GNU/Linux, pero nos alegra ver que parece que todo sigue como debería, y continúan apostando por nuestro sistema.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/JdAVK62d5y4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Así que si os gustó Hitman y tenéis estos objetivos en vuestra lista de pendientes, aprovechar a que están de vuelta. Y si no tenéis el juego podéis haceros con él en la propia tienda de [Feral](https://store.feralinteractive.com/es/mac-linux-games/hitmangoty/) o en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/236870/" style="display: block; margin-left: auto; margin-right: auto; border-style: none;" width="646"></iframe></div>


Y tú, ¿tienes pendiente acabar con alguno de estos objetivos? Cuéntanos qué te parece este Hitman en los comentarios en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

