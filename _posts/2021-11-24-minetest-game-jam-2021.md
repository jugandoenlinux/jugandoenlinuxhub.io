---
author: P_Vader
category: "Exploraci\xF3n"
date: 2021-11-24 23:34:12
excerpt: "<p>El proyecto presenta esta ingeniosa competici\xF3n para crear juegos\
  \ con Minetest.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Minetest/minetest_game_jam_2021.webp
joomla_id: 1390
joomla_url: minetest-game-jam-2021
layout: post
tags:
- concurso
- minetest
- jam
title: Minetest Game Jam 2021
---
El proyecto presenta esta ingeniosa competición para crear juegos con Minetest.


Por si no lo conoces aún Minetest podría decirse que parece un clon software libre de Minecraft, pero aunque es similar en su aspecto realmente Minetest es muy diferente en su objetivo. Minetest es una base simple, un juego tipo voxeles de contrucción y creación mediante bloques en un mundo abierto. **Pero su fuerte es que es posible ampliarlo con [cientos de mods](https://content.minetest.net/packages/?type=mod), incluso modificar la mecánica con los "[juegos](https://content.minetest.net/packages/?type=game)", que son un conjunto de modificaciones y añadidos en forma de paquete y que lo pueden convertir en experiencias de juego muy diferentes y divertidas como juegos de laberintos, carreras de coches, captura la bandera, un señor de los anillos, etc.** Esa es el plus que tiene Minetest, su versatilidad y potencia a la hora de crear como base.


Para darle un impulso el proyecto **acaba de anunciar una JAM o competición para crear estos juegos de Minetest, como explican**:



> 
> Tu objetivo es crear una experiencia de juego pequeña, completa y autónoma.
> 
> 
> Algunos ejemplos del tamaño de juego que buscamos:
> 
> 
> * <https://content.minetest.net/packages/Hume2/boxworld3d/>
> * <https://content.minetest.net/packages/1248/labyrinthus/>
> 
> 
> El objetivo es hacer un juego pequeño, no un sandbox inmerso o un juego de rol. Elija una idea sencilla y manejable y divide tu tiempo sabiamente.
> 
> 
> 


¿Necesitas ideas? Algunos ejemplos que dan:


* Juego de carreras
* Simulador de Agricultura
* Veo, veo (o ¿dónde está Wally?)
* Simulación de negocios (tipo tycoon)
* Defensa de torres
* Juegos de mesa.


En su día os enseñamos como jugamos a Minetest en familia usando el pack de juego Mineclone2:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/_-UKopX3qNM" title="YouTube video player" width="780"></iframe></div>


La competición empieza **el 1 de diciembre** y finaliza **el 21 de diciembre**. La valoración será a continuación hasta el 30 de diciembre. Finalmente los resultados se darán a conocer **el 31 de diciembre** (o el 1 de enero).


La forma de **participar es muy sencilla, sube un juego a [la base de datos de contenido](https://content.minetest.net/) antes de la fecha límite con la etiqueta *"Jam / Game 2021"***. El **premio será de 200$** a repartir entre los 3 primeros. Tienes mas información y detalles en el [anuncio oficial de su foro](https://forum.minetest.net/viewtopic.php?t=27512).


Para la creación de mods y juegos, [Minetest dispone de una API](https://dev.minetest.net/Modding_Intro) haciendo uso de [LUA](https://www.lua.org/manual/5.1/), un lenguaje de programación relativamente sencillo de aprender e ideal para entornos educativos.


¿Te atreves a participar? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)


 

