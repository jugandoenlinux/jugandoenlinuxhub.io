---
author: Pato
category: Apt-Get Update
date: 2018-07-31 17:57:14
excerpt: "<p>Repasamos las novedades de los \xFAltimos d\xEDas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b018fd5ec8f1b90a1c8015900c2c2630.webp
joomla_id: 810
joomla_url: apt-get-update-steamos-steam-companyofheroes2-arma3-warlocks-2-star-renegades
layout: post
tags:
- aptget
title: 'Apt-Get Update SteamOS & Steam & Company Of Heroes 2 & ArmA 3 & Warlocks 2:
  God Slayers & Star Renegades'
---
Repasamos las novedades de los últimos días

Después de unos días en los que no nos ha sido posible actualizar noticias todo lo que quisiéramos, (tenemos una vida, trabajo, vacaciones... guiño-guiño) volvemos a la carga con un repaso a lo que ha dado de sí estos últimos días para ponernos al día, valga la redundancia. ¡Comenzamos!


* SteamOS dió señales de vida en lo que parece que será la nueva versión del sistema operativo de Valve, que vendrá con cambio de versión a SteamOS 3 y con base de Debian 9 stretch. El nombre en clave será Clockwerk, tal y como puede verse en el [repositorio de Steam](http://repo.steampowered.com/steamos/dists/). Tienes más info en [gamingonlinux.com](https://www.gamingonlinux.com/articles/looks-like-steamos-30-is-on-the-way-codenamed-clockwerk.12208)
* Seguimos con SteamOS, y es que además de la nueva versión Valve también ha lanzado una actualización para la versión actual, numerada como SteamOS 2.154 y que trae aparte de los paquetes y actualizaciones de seguridad de rigor, los preparativos para el cambio de kernel y drivers gráficos que se avecinan en el sistema. Tienes toda la información y cambios en el anuncio oficial en los [foros de Steam](https://steamcommunity.com/groups/steamuniverse/discussions/1/1709564547929669543/).
* Terminamos con el triplete de Valve con la actualización del cliente Steam, con la novedad del nuevo chat con el que pretenden competir en funcionalidad con otras plataformas de comunicaciones para jugadores. Ya está disponible, y las novedades puedes verlas [en este enlace](https://steamcommunity.com/updates/chatupdate).
* Company of Heroes 2, el juego cuyo port corrió a cargo de **Feral Interactive** se ha actualizado para dar **soporte oficial a las gráficas AMD**. Puedes ver toda la información en el [post de actualización del juego](https://community.companyofheroes.com/discussion/67/coh-2-changelog/p5), o en [Gamingonlinux.com](https://www.gamingonlinux.com/articles/company-of-heroes-2-is-now-officially-supported-on-amd-gpus-on-linux.12212). También puedes conseguir el juego en [Feral Store](https://store.feralinteractive.com/en/mac-linux-games/companyofheroes2/), [Humble Bundle](https://www.humblebundle.com/store/company-of-heroes-2?partner=jugandoenlinux) o en [Steam](https://store.steampowered.com/app/231430/Company_of_Heroes_2/).
<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/yGlQhMhKE_o" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>
* El port de ArmA 3 en su versión Linux ha sido actualizado a la versión 1.82. Esto actualiza nuevamente el juego para poder jugar junto a los jugadores de Windows. Tienes toda la información en el [post de actualización](https://dev.arma3.com/post/spotrep-00079) y en [gamingonlinux.com](https://www.gamingonlinux.com/articles/the-linux-beta-of-arma-3-has-been-updated-to-182.12215). El juego está disponible en [Steam](https://store.steampowered.com/app/107410/Arma_3/).
<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/M1YBZUxMX8g" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>
* Warlocks 2: God Slayers traerá a Linux su acción para hasta 4 jugadores el próximo mes de Octubre. Tienes toda la información en el artículo de [gamingonlinux.com](https://www.gamingonlinux.com/articles/warlocks-2-god-slayers-brings-the-action-to-linux-this-october.12229)
<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/yFKc4X7x-gc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>
* Star Renegades es un RPG de acción táctica que llegará a Linux a principios del año que viene. Tienes más información en el artículo de [linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/star-renegades-tactical-rogue-lite-rpg-linux-68571/)
<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/yaBeEKxFA3s" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Esto es todo por ahora, pero seguro que nos hemos dejado muchas cosas en el tintero. Cuéntamelas en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

