---
author: Pato
category: Arcade
date: 2019-05-31 14:57:08
excerpt: <p>El juego de <span class="css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo
  r-qvutc0" dir="auto">@zapakitul</span> sigue con la iniciativa Linux 1st</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/46eff594cc95ecffb759c53475ade30a.webp
joomla_id: 1055
joomla_url: ya-puedes-descargar-la-demo-de-space-mercs-desde-la-bearded-giant-store-o-itch-io
layout: post
tags:
- accion
- indie
- demo
- arcade
title: Ya puedes descargar la demo de Space Mercs desde la Bearded Giant Store o Itch.io
---
El juego de @zapakitul sigue con la iniciativa Linux 1st

Bearded Giant Games, suma y sigue. Como ya hemos dicho en otros artículos, es un "one man" estudio que debido a su predilección sobre nuestro sistema favorito primero desarrolla las versiones de sus juegos para Linux y posteriormente para los demás, tal y como ya ha hecho con juegos como [Farm Life](index.php/homepage/generos/puzzles/10-puzzles/1151-bearded-giant-games-lanza-un-nuevo-juego-bajo-su-iniciativa-linux-first-farm-life) del que ya os hemos hablado.


Siendo así, ahora tenemos la posibilidad de probar **la demo de Space Mercs**, su último desarrollo que ya está disponible para descarga:


 



> 
> Download Space Mercs - Demo - for Linux now from [@store_giant](https://twitter.com/store_giant?ref_src=twsrc%5Etfw) or [@itchio](https://twitter.com/itchio?ref_src=twsrc%5Etfw)! <https://t.co/bSNWIBHmGD><https://t.co/xFezZDjgjH>[#linuxgaming](https://twitter.com/hashtag/linuxgaming?src=hash&ref_src=twsrc%5Etfw) [#indiegaming](https://twitter.com/hashtag/indiegaming?src=hash&ref_src=twsrc%5Etfw) [#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [#gaming](https://twitter.com/hashtag/gaming?src=hash&ref_src=twsrc%5Etfw) [#games](https://twitter.com/hashtag/games?src=hash&ref_src=twsrc%5Etfw) [#linux](https://twitter.com/hashtag/linux?src=hash&ref_src=twsrc%5Etfw) [#demo](https://twitter.com/hashtag/demo?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/jErTXwAoJQ](https://t.co/jErTXwAoJQ)
> 
> 
> — Bacioiu Ciprian (@zapakitul) [May 31, 2019](https://twitter.com/zapakitul/status/1134432564964409345?ref_src=twsrc%5Etfw)



*¡Space Mercs es un juego de combate espacial arcade extremo en el que la cantidad de proyectiles y láseres en pantalla solo es superado por la cantidad de estrellas en el universo! ¿Podrás completar todas las misiones como mercenario y convertirte en el mejor piloto de la Galaxia?*


![Spacemercs2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Beardedgiant/Spacemercs2.webp)


El juego es de estilo arcade espacial sin muchas complicaciones, con acción y láseres a raudales y montones de naves que derribar. Además soporta teclado y ratón o mando, e incluso se pueden asignar botones y teclas desde el menú del juego.


**La versión demo incluye tres misiones de la campaña y un modo batalla rápida** en el que tendremos que enfrentarnos a mas de 100 naves. Para descargarla tan solo tenemos que visitar la tienda [Bearded Giant Store](https://beardedgiant.games/product/space-mercs-demo/) o [Itch.io](https://zapakitul.itch.io/space-mercs-demo) y agregarlo al carrito de compra. No hace falta pagar nada por la demo, por supuesto. **El juego completo estará disponible para su compra a finales de** **junio** y el desarrollador [nos ha confirmado](https://twitter.com/zapakitul/status/1134485066384584707) que el juego también llegará a Steam para el mes de Julio.


Por si os lo estais preguntando, la versión demo para Windows está en desarrollo y llegará seguramente en los próximos días.


¿Que te parece Space Mercs? ¿Probarás la demo?


Cuéntame tus impresiones en los comentarios o en el canal de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux).

