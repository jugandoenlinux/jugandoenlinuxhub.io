---
author: Pato
category: Hardware
date: 2021-11-10 20:43:43
excerpt: "<p>Valve ha tenido que retrasar la entrega de las reservas por la actual\
  \ crisis de componentes</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/steamdeckdevkit1.webp
joomla_id: 1386
joomla_url: el-lanzamiento-de-steam-deck-se-retrasa-hasta-febrero-de-2022
layout: post
tags:
- steam
- valve
- steam-deck
title: El lanzamiento de Steam Deck se retrasa hasta febrero de 2022
---
Valve ha tenido que retrasar la entrega de las reservas por la actual crisis de componentes


Corren malos tiempos para los ensambladores de hardware, y es que hoy Valve ha tenido que notificar a los usuarios que tienen reservada una **Steam Deck** que el envío del PC-consola portable se retrasa hasta el mes de Febrero de 2022.


La causa es ni más ni menos que la actual crisis de los componentes que tiene a media industria en vilo y que no consigue abastecer suficientes chips para poder satisfacer la demanda:



> 
> 
> 
> |  |
> | --- |
> | Sentimos comunicarte que el lanzamiento de Steam Deck se retrasará dos meses. Hicimos todo lo posible por sortear los problemas generalizados en la cadena de suministros, pero, a causa de la escasez de materiales, nuestras fábricas no están recibiendo los componentes a tiempo para cumplir las fechas de lanzamiento previstas inicialmente.
> Según nuestros nuevos cálculos, los envíos de Steam Deck comenzarán en febrero de 2022, al igual que la cola de reservas. No perderás tu puesto, pero las fechas se retrasarán en función a estos cambios. Las fechas de reserva previstas se actualizarán poco después de este anuncio.
> Una vez más, sentimos no poder cumplir las fechas de envío originales. Seguiremos esforzándonos por mejorar las fechas de reserva en función al nuevo cronograma y te mantendremos informado de cualquier novedad. |
> 
> 
> 


Si eres de los que tienen una Steam Deck reservada y quieres consultar la nueva disponibilidad prevista para los pedidos y las preguntas más frecuentes, puedes visitar la [página de la tienda de Steam Deck](https://store.steampowered.com/steamdeck).


En otro orden de cosas, Valve va a realizar una serie de conferencias para todos los interesados en profundizar en todo lo relacionado con la Steam Deck, como el hardware, el desarrollo para la máquina sin tener un dev kit, la APU de AMD etc. Tendrá lugar el próximo día 12 a partir de las 19:00h (Hora peninsular española) [en este enlace](https://steamcommunity.com/steamworksvirtualconference/steamdeck).


En cuanto al retraso de la Steam Deck, ¿piensas que puede perjudicar las ventas, o al contrario puede ser una oportunidad para tener mas tiempo y lanzar un producto más redondo y con juegos más pulidos?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

