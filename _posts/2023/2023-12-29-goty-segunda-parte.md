---
author: Odin
category: Editorial
date: 30-12-2023 14:00:00
excerpt: "Os presentamos la segunda parte de nuestros elegidos para jugar en el 2023."
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/2023/2023-12-29-goty-segunda-parte/gotyremnantsoftheprecursors.jpg
layout: post
tags:
  - GOTY
  - JuegoDelAño
  - JDADCASJEL
title: El JDADCASJEL (El Juego Del Año, De Cualquier Año, Según Jugando En Linux, Segunda Parte)
---


Volvemos a la carga con la segunda parte de nuestra peculiar selección de los Goty del 2023.

# Odin

Para este año tengo dos recomendaciones, un pequeño JRPG indie, y un juego libre que hará las delicias de los aficionados al género de la estrategia.

## Cassette Beasts

Creo que a nadie le va a extrañar si escojo Cassette Beasts como mi Goty del 2023 ya que a todo el mundo que he podido le he dado la turra con este juego. En mi defensa, tengo que decir que una vez que se le da la oportunidad a este juego indie, cuesta no cogerle aprecio.

Al fin y al cabo tiene multiples virtudes. Nativo para **Linux** y verificado para la **Steam Deck**, hecho en **Godot**, que actualmente es el game engine libre más popular y uno de los más potentes, y perfectamente traducido al **español**.

Pero más alla de la parte técnica y de la traducción, que es muy importante pero no hace un juego divertido, tenemos un magnífico videojuego del género **Monster Catcher**. En él, vamos a ir coleccionando criaturas, que usaremos durante las batallas, y que le van a dar una gran profundidad táctica y variedad a las peleas. A la par de este sistema de lucha, tenemos una historia y unos personajes que hacen que quieras avanzar en el juego, con una mezcla de reflexiones interesantes y de algún toque de humor irónico, se irá desgranando los misterios del mundo en el que aparece el protagonista.


<div class="resp-iframe">
<iframe width="754" height="424" src="https://www.youtube.com/embed/_OLST_Fw5Ms" title="Cassette Beasts | PC release Trailer | Available on Consoles May 25th!" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

En definitiva, si os gustan los juegos **JRPG a turnos** creo que este pequeño videojuego es muy recomendable.

<div class="steam-iframe">
  <iframe src="https://store.steampowered.com/widget/1321440/" frameborder="0" width="646" height="190"></iframe>
</div>

## Remnants of the Precursors

Si habéis jugado al clásico [Master of Orion](https://es.wikipedia.org/wiki/Master_of_Orion) la explicación más sencilla del **Remnants of the Precursors** es que es una modernización del clásico hecho por fanáticos de la saga.

Si no habéis jugado a ninguno de la saga, os podemos decir que que es un juego de estrategia del género 4X, en el que tenéis  que convertir vuestra civilización en la dominante de la galaxia, pudiéndolo conseguir por medios diplomáticos, o por medios bélicos.

![captura de pantalla del menú principal de Remnants of the Precursors](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/2023/2023-12-29-goty-segunda-parte/remnantsoftheprecursors.jpg)

En definitiva, uno de esos videojuegos que empiezas a jugarlo, y con el tema de "un solo turno más" terminas enganchado hasta las tantas. Y como además es gratis, caer en la tentación es muy facil.

Lo podeis instalar facilmente desde [Flathub](https://flathub.org/apps/com.remnantsoftheprecursors.ROTP) gracias a Flatpak en cualquier distribución.

 
---
# P_Vader
En mi caso este año voy a recomendar dos juegos y voy a explicar el descarte de otro.

## [Brotato](https://store.steampowered.com/app/1942280/Brotato/): El GOTY GOTY de 2023
Un juego en el que manejas una patata con 6 armas para protegerse de unos alienígenas, parece muy chorra ¿no? **Detrás de esa apariencia tan informal hay un súper adictivo juego.**

Llegó a su versión final a mediados de este año 2023 (verdadero GOTY 2023!) aunque su éxito vino desde mucho antes mientras estaba en acceso anticipado. 

Si te gustan juegos casuales, tipo roguelite de disparos shot'em up, intenso, con cientos de combinaciones de armas, potenciadores, cartas, atributos, etc. **cómpralo**, ademas suele estar a un precio muy muy asequible. Perfecto para la Steam Deck, de lo mas jugado en la consola según los datos de Steam. Partidas rápidas de no mas de 30 minutos que a lo tonto me tiene consumidas mas de 30 horas.
<div class="resp-iframe">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/32N0SL0V0cs?si=_GW_GYPYgO51hPLP" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>




## [Project Zomboid](https://store.steampowered.com/app/108600/Project_Zomboid/): El GOTY de la década.

De la década porque lleva en acceso anticipado desde hace 10 años, pero eso da igual. En este caso regalé este juego y gracias al préstamo familiar de Steam lo pude jugar de prestado y casi me transformé en otro zombie pegado al ordenador. Durante un mes estuve infectado a este tremendo juego. Lo tuve que aparcar para recuperar mi vida pre-apocalíptica.

Es un clásico de la supervivencia y tan extremadamente real en muchas situaciones que ni el mismísimo [Rick Grimes](https://es.wikipedia.org/wiki/Rick_Grimes) aguantaría una semana. **Lo que tiene de difícil lo tiene de adictivo. Las estrategias de supervivencia son infinitas. Añádele ademas una comunidad de mods brutal** donde hace un juego infinito de posibilidades. Tambien está verificado para la Steam Deck.

Así empiezas la partida, no hay mas:

![Captura de pantalla del inicio de Project Zomboid](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/2023/2023-12-29-goty-segunda-parte/project_zomboid.webp)

<div class="resp-iframe">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/nPbsDmzZ3Oc?si=nfylChCtWt70OELt" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>




## [Counter-Strike 2](https://store.steampowered.com/app/730/CounterStrike_2/): El NO GOTY
A mediados de este año Valve nos sorprendió con Counter Strike 2. Comenzá muy fuerte, pero poco después **empezó a tener problemas con una cantidad exagerada de tramposos que campan a sus anchas**, llegando actualmente a rangos top mundiales junto a jugadores profesionales sin ningún problema. Un desastre que ha llevado a la perdida de mas del 30% de usuarios en poco tiempo y una pésima imagen del juego insignia de Valve.

**Iba a ser mi GOTY de 2023, pero se ha roto.** Con lo que bien que se me da jugarlo...  
<div class="resp-iframe">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/Wvr7fSrcjIE?si=oG-ar2oDCrxllPfx" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>


---
¿Qué os ha parecido esta nueva selección de juegos? Cuéntanoslo en nuestros canales habituales de [Telegram](https://t.me/jugandoenlinux), [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) o [Mastodon](https://mastodon.social/@jugandoenlinux)