---
author: Serjor
category: Editorial
date: 2023-12-28 7:00:00
excerpt: Otra vez más, el infierno se ha debido de congelar
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/2023/2023-12-28-microsoft_wine/ms-heart-linux-770px.jpg
layout: post
tags:
  - Microsoft
  - Noticias
title: Microsoft comenzará a aportar código al proyecto Wine
---

**Actualización:** Sí, teníais razón, el artículo era una inocentada por el 28 de diciembre. Esperamos haberos sacado una sonrisa al menos 😉

De piedra nos hemos quedado al leer esta noticia:
**Microsoft anuncia que comenzará a aportar parches y código al proyecto Wine, que es el corazón de proton.**

Según su post, deben llevar varios años debatiendo internamente si colaborar o no con Wine. Las conversaciones debieron comenzar en 2016, tras hacerse miembro [platinum de la Linux Foundation](https://www.genbeta.com/actualidad/microsoft-se-une-a-la-linux-foundation-en-calidad-de-miembro-platino).

Desde entonces han ido liberando parte de .Net en [su cuenta de github](https://github.com/microsoft), y promocionan el software libre en su propio [blog](https://cloudblogs.microsoft.com/opensource/)

De hecho, tengo que admitir, que estoy redactando este artículo con VSCode, que es de código abierto, y desarrollado por Microsot.

El caso es que son conscientes de que Windows en el modelo actual de negocio no les sale rentable, y como parte de su estrategia ["Desktop as a Service"](https://azure.microsoft.com/en-us/blog/what-is-desktop-as-a-service-daas-and-how-can-it-help-your-organization/) que anunciaron el año pasado, poder ejecutar Windows en plataformas virtualizadas pasa por aligerar la carga del servidor lo máximo posible, para ello su plan es usar su distribución Linux interna, [CBL-Mariner](https://github.com/microsoft/CBL-Mariner) y ejecutar sobre ella las aplicacione de escritorio. Para esto el camino más eficiente pasa por evitar virtualizaciones de cualquier tipo, y hacer llamadas al propio Kernel con la menor cantidad de capas posible, y es aquí donde Wine se erige como la solución perfecta. Lleva años de desarrollo, es compatible con la gran mayoría de la API Win32, y encaja en su estrategia.

Esto evidentemente, como gamers nos beneficia, ya que aquellos programas y juegos que hoy no funcionan correctamente, tarde o temprano funcionarán igual o mejor que en Windows. Eso sí, hay que andar con cautela, porque el lema ["Embrace, Extend, and Extinguish"](https://en.wikipedia.org/wiki/Embrace%2C_extend%2C_and_extinguish) no lo inventaron por nada.

En fin, como dicen los anglosajones, What a time to be alive!

Y tú, ¿qué opinas de este cambio de rumbo, o confirmación del mismo, de Microsoft? Cuéntanoslo en nuestro canal se [Telegram](https://t.me/jugandoenlinux)
