---
author: leillo1975
category: "Acci\xF3n"
date: 2017-07-17 09:46:00
excerpt: "<p>Podr\xEDamos verlo pronto gracias a DOSBOX</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/49d777cdd477e76dea7b279017b034de.webp
joomla_id: 405
joomla_url: parece-que-star-wars-dark-forces-llegara-a-gnu-linux-en-steam
layout: post
tags:
- gog
- steamdb
- star-wars
- dark-forces
- dosbox
- rebel-assault
title: "Parece que STAR WARS: Dark Forces llegar\xE1 a GNU/Linux en Steam (ACTUALIZADO)"
---
Podríamos verlo pronto gracias a DOSBOX

**ACTUALIZACIÓN:**  Informa de nuevo [GamingOnLinux](https://www.gamingonlinux.com/articles/looks-like-star-wars-rebel-assault-i-ii-will-get-their-dosbox-versions-for-linux-on-steam.10000) que según [SteamDB](https://steamdb.info/app/456540/history/?changeid=3192462) también se están portando para Steam **STAR WARS: Rebel Assault I y II**, juegos que ya están también disponibles en [GOG.com](https://www.gog.com/game/star_wars_rebel_assault_1_2). La forma de hacerlo es también mediante DOSBOX. La verdad es que se agradece este pequeño esfuerzo en traer estos grandes clásicos de la saga a nuestros PC's . Seguiremos informando de posibles novedades.




---


 


Gracias a la información proporcionada por [GamingOnLinux](https://www.gamingonlinux.com/articles/looks-like-the-dosbox-wrapped-linux-version-of-star-wars-dark-forces-might-make-it-to-steam.9971) nos acabamos de enterar que parece que están desarrollando un port para Steam del clásico de Lucasarts de 1995. Por lo que podemos apreciar en [SteamDB](https://steamdb.info/app/32400/history/?changeid=3167772), fuente inagotable de información, existe un ejecutable en testeo para nuestro sistema de tan afamado juego.


 


Hasta ahora solo podíamos disfrutarlo en nuestros PC's gracias a [GOG.com](https://www.gog.com/game/star_wars_dark_forces), ya que existe soporte mediante el emulador **DOSBOX**, pero gracias a esta nueva entrada en SteamDB parece que también tendremos la posibilidad de ejecutarlo en un futuro con el cliente de [Steam](http://store.steampowered.com/app/32400/STAR_WARS__Dark_Forces/).


 


Para quien no lo conozca, STAR WARS: Dark Forces es un juego de acción 3D ambientado en el universo de la conocida saga, donde tomaremos el rol de **Kyle Katarn**, un Stormtrooper imperial que se unirá a la Alianza Rebelde y combatirá desde dentro el Imperio.  En el recorreremos reconocibles escenarios y lucharemos contra variados enemigos. Podeis ver un gameplay en el siguiente video de Youtube:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/AW8yk_eIHJU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


¿Qué te parece que este juego llegue a Steam? ¿Lo has jugado ya? Deja tus impresiones en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

