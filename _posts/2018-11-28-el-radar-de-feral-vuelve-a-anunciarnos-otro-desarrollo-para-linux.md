---
author: Pato
category: Noticia
date: 2018-11-28 16:33:30
excerpt: <p>@feralgames ... suma y sigue</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/01be3d395dd399cb7a1780083126f30b.webp
joomla_id: 913
joomla_url: el-radar-de-feral-vuelve-a-anunciarnos-otro-desarrollo-para-linux
layout: post
tags:
- proximamente
- feral-interactive
- radar
title: El radar de Feral vuelve a anunciarnos otro desarrollo para Linux
---
@feralgames ... suma y sigue

No contentos con habernos traído hace unos días el nuevo **Total War: WARHAMMER II** y tener en la parrilla de salida otros títulos del calibre de "**Life is Strange 2**" o "**Shadow of the Tomb Raider**" (sin olvidar el **Three Kingdoms**) Feral Interactive nos acaba de anunciar un nuevo desarrollo para nuestro sistema favorito mediante su ya peculiar radar:



> 
> \*CLINK\* \*CLANK\* \*BZZZZZRRRRRRRT\* … \*ding\*  
>   
> Wait… is that ANOTHER macOS and Linux clue on the Feral Radar? What is that thing playing at?!<https://t.co/huXFd95Dab> [pic.twitter.com/4rcVU3FIRq](https://t.co/4rcVU3FIRq)
> 
> 
> — Feral Interactive (@feralgames) [November 28, 2018](https://twitter.com/feralgames/status/1067791540704157696?ref_src=twsrc%5Etfw)


  





Según se puede ver, la imagen parece ser una especie de tablero con luces y la leyenda "**Flushing Point**":


![teaserferal1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Feral/teaserferal1.webp)


 


¿Tablero? ¿Luces?... ¿o no?. ¿Alguna idea de qué juego puede ser?


Estamos deseosos de saber lo que se os ocurre. Podéis contárnoslo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [discord](https://discord.gg/ftcmBjD). 

