---
author: Pato
category: Estrategia
date: 2020-02-25 18:40:36
excerpt: "<p>El battle chess de Valve introduce gran cantidad de cambios y contenido</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DotaUnderlords/underlords.webp
joomla_id: 1179
joomla_url: dota-underlords-sale-de-acceso-anticipado-y-da-comienzo-a-su-primera-temporada
layout: post
tags:
- rts
- valve
- dota-underlords
title: DOTA Underlords sale de acceso anticipado y da comienzo a su primera temporada
---
El battle chess de Valve introduce gran cantidad de cambios y contenido


Tal y como anunciaron hace [casi un mes]({{ "/posts/anunciada-la-primera-temporada-de-dota-underlords" | absolute_url }}), Valve ha lanzado hoy la primera temporada de su battle chess **DOTA Underlords** en la que introducen cuantiosas novedades como la campaña "**City Crawl**", reorganización de objetos, un nuevo héroe, una nueva alianza, más de 100 recompensas y mucho más.


En esta nueva campaña tendremos que luchar para recuperar White Spire barrio por barrio mediante peleas callejeras completando los desafíos que se nos pondrán por delante para desbloquear nuevos atuendos y posters. También tendremos una tabla de clasificación para saber como vamos ganando a nuestros amigos y poder liderar la clasificación.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/m9MqIzmWG24" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


*La primera temporada viene con un pase de batalla completo que ofrece más de 100 recompensas. Juega partidos y completa desafíos para subir de nivel tu Pase de batalla y gana recompensas como nuevos tableros, accesorios de mapas, pósters deseados y más.*


Recordamos que DOTA Underlords es un juego "Free to play" con lo que puedes jugar y acceder a muchas recompensas de forma gratuita, aunque **para tener acceso a todo el contenido de la Campaña será necesario comprar el pase de batalla por 4.99€**, eso sí, Valve avisa que es solo para el contenido. No supondrá ningún tipo de ventaja en el juego. 


Si quieres saber más puedes visitar el [anuncio de la temporada](https://steamcommunity.com/ogg/1046930/announcements/detail/1702861358078368783), o si quieres jugar a DOTA Underlords puedes visitar su [página de Steam](https://store.steampowered.com/app/1046930/Dota_Underlords/) donde está disponible de forma gratuita, o también puedes visitar su [web oficial](http://underlords.com/) donde también hay enlaces para jugar al juego en teléfonos móviles y no perder tu tiempo de juego ni tus progresos.


¿Qué te parecen las novedades introducidas en este DOTA Underlords? ¿Te gustan los Battle Chess?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

