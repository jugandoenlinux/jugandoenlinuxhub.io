---
author: P_Vader
category: "V\xEDdeos JEL"
date: 2021-09-13 16:01:35
excerpt: "<p>Aqu\xED tienes la partida de Veloren que echamos con @OdinTdh y dos pros\
  \ del juego invitados.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Veloren/veloren_gameplay.webp
joomla_id: 1357
joomla_url: video-partida-de-veloren-0-11
layout: post
tags:
- multijugador
- rpg
- streaming
- youtube
- videos
- veloren
title: "V\xCDDEO: Partida de Veloren 0.11"
---
Aquí tienes la partida de Veloren que echamos con @OdinTdh y dos pros del juego invitados.


Si quieres ver la partida de Veloren que echamos con @OdinTdh y dos pros del juego invitados, aquí tenéis el vídeo con sus mejores momentos por capítulos:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/Vlst_UtpiQg" title="YouTube video player" width="780"></iframe></div>


 Y no te pierdas las valoraciones que hicimos al finalizar el video de este proyecto y el software libre.


 


 

