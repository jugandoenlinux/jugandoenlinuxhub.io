---
author: leillo1975
category: Carreras
date: 2017-03-23 14:12:47
excerpt: "<p>Podr\xE1s jugar sin comprar el juego hasta el lunes de la semana que\
  \ viene</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/92c3523de61d06eebdc515f2babb64b9.webp
joomla_id: 261
joomla_url: disfruta-gratis-del-gran-motorsport-manager-hasta-el-dia-27-de-marzo
layout: post
tags:
- gratis
- motorsport-manager
- playsport-games
title: "Disfruta gratis del gran Motorsport Manager hasta el d\xEDa 27 de Marzo"
---
Podrás jugar sin comprar el juego hasta el lunes de la semana que viene

 Con bastante retraso, pero aun a tiempo para que lo disfruteis este fin de semana (4 días), debo informaros que [PlaySport Games](http://www.playsportgames.com/) y SEGA han permitido descargar su gran éxito de forma gratuita durante esta semana en Steam. Aquí teneis el tweet que lo nos recordaba a los despistados la disponibilidad de la descarga:


 



> 
> Given our free week a try yet? If so, how are you getting on? have you managed to pick up any points? [#newtoMM](https://twitter.com/hashtag/newtoMM?src=hash)  
>  <https://t.co/CwNdZK6Ero>
> 
> 
> — Motorsport Manager (@PlayMotorsport) [22 de marzo de 2017](https://twitter.com/PlayMotorsport/status/844602894548713473)



 


Además también hay que destacar que el juego estará también con una **rebaja del 50% (17.49€)**, por lo que es una ocasión excepcional de probar el software y después comprarlo a buen precio. Para todos aquellos que no conozcais de que va, deciros que [Motorsport Manager](http://www.motorsportmanager.com/) es un título de gestión, donde tendremos que dirigir nuestra propia escudería de coches, y donde podremos contratar pilotos, mecánicos, ampliar nuestras instalaciones, mejorar piezas, gestionar sponsors, decidir la estratégia y los ajustes mecánicos en las carreras, etc. Un título altamente recomendable y de gran calidad para los amantes de los "managers" y el automovilismo. También es importante reseñar que el juego dispone de una expansión con las [GT Series](http://store.steampowered.com/app/559430/) y Workshop con un montón de modificaciones, con lo que la rejugabilidad es infinita. Personalmente puedo decir que es un juego que te mantiene pegado a la silla horas y horas y tremendamente adictivo. 


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/ZPzu6GmIQg0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Puedes descargar y comprar Motorsport Manager en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/415200/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 ¿Descargarás Motorsport Manager? ¿Ya lo tienes? ¿Qué te parece el juego? Puedes responder a estas preguntas o escribir lo que quieras en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

