---
author: Pato
category: Arcade
date: 2018-05-23 17:30:17
excerpt: "<p><strong>@SEGA_Europe lo ha anunciado! Ya tenemos disponible toda la colecci\xF3\
  n de 'Sega Mega Drive &amp; Genesis Classics' en nuestro sistema!!!</strong> </p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/10a451d868feb5fd854c1535dddc148e.webp
joomla_id: 746
joomla_url: aparecen-referencias-para-linux-steamos-de-sega-mega-drive-genesis-classics-en-steamdb-actualizacion
layout: post
tags:
- arcade
- sega
title: "Aparecen referencias para Linux/SteamOS de 'Sega Mega Drive & Genesis Classics'\
  \ en SteamDB (Actualizaci\xF3n!)"
---
**@SEGA_Europe lo ha anunciado! Ya tenemos disponible toda la colección de 'Sega Mega Drive & Genesis Classics' en nuestro sistema!!!** 

**(Actualización 29/05/2018):**


¡Los rumores eran ciertos! Sega ha confirmado lo que era casi un secreto a voces:



> 
> SEGA [#GenesisClassics](https://twitter.com/hashtag/GenesisClassics?src=hash&ref_src=twsrc%5Etfw) is OUT NOW! We're celebrating the release with this awesome musical mash by the talented [@EclecticMethod](https://twitter.com/EclecticMethod?ref_src=twsrc%5Etfw). <https://t.co/iKTfkYWdcZ> [pic.twitter.com/Z5FpiCSOeK](https://t.co/Z5FpiCSOeK)
> 
> 
> — SEGA (@SEGA) [29 de mayo de 2018](https://twitter.com/SEGA/status/1001463176737837056?ref_src=twsrc%5Etfw)



Sega ha publicado una actualización de su paquete de juegos **'Sega Mega Drive & Genesis Classics'** dando soporte a Linux/SteamOS y de paso actualizando la lanzadera para añadir soporte para realidad virtual (RV), multijugador online, rankings, modos desafío, nuevas opciones gráficas, avance rápido y rebobinado, modo espejo etc...


Además, **han "retocado" los precios tanto del paquete completo como de los juegos individuales** rebajándolos para la ocasión. ¡Completa tu colección al mejor precio!


Todos los detalles los tienes en el anuncio de su página de Steam [en este enlace](https://steamcommunity.com/games/34270/announcements/detail/1674656328580811757).


Si quieres echar un vistazo a la lista de juegos de la colección o comprarla directamente, tienes disponible **'Sega Mega Drive & Genesis Classics'** en su página de Steam, [en este enlace](https://store.steampowered.com/app/34270/SEGA_Mega_Drive_and_Genesis_Classics/).


¿Eres de los nostálgicos que juegan a los juegos de Sega? ¿Que te parece que haya llegado **'Sega Mega Drive & Genesis Classics'** a nuestro sistema favorito?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 




---


**(Artículo original):**


Buenas noticias para los amantes de lo retro y los clásicos de consolas antiguas. Leemos en [gamingonlinux.com](https://www.gamingonlinux.com/articles/theres-hints-of-the-sega-mega-drive-and-genesis-classics-pack-coming-to-linux.11826) que han aparecido referencias del recopilatorio de clásicos de Sega para sus consolas Mega Drive y Genesis que ya se encuentra disponible para sistemas Windows desde hace tiempo.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/nAzzoU0fIxU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


'Sega Mega Drive & Genesis Classics' es un recopilatorio de los grandes títulos de esas máquinas actualmente disponibles en PC "empaquetados" en un entorno virtual "fidedigno" con soporte para controladores y gamepads, función de guardar partidas, modos cooperativos locales en los juegos que lo soporten y Steam Workshop.


 Aún no está confirmado el soporte para nuestro sistema favorito, pero según las referencias, en [SteamDB](https://steamdb.info/app/34270/history/?changeid=4470022) han aparecido contenedores para Linux, y algunos poseedores del recopilatorio, entre ellos nuestro compañero Leillo han verificado que el botón de descarga se ha activado en Steam para Linux, aunque no se descarga aún nada.


![Leosega](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Segamegadriveclassics/Leosega.webp)


![Leosega2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Segamegadriveclassics/Leosega2.webp)


Estaremos atentos a la confirmación de Sega al respecto.


¿Qué os parece el que puedan venir los clásicos de Mega Drive a Linux/SteamOS? ¿Eres de los que disfrutas con los juegos de hace 25 o 30 años?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

