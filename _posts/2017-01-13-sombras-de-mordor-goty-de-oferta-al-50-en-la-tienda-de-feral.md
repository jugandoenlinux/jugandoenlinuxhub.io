---
author: Pato
category: "Acci\xF3n"
date: 2017-01-13 17:06:00
excerpt: <p>Si no tienes el juego, esta es una buena oportunidad</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/463052dad9377fe2445d3b1bfb5f62a3.webp
joomla_id: 176
joomla_url: sombras-de-mordor-goty-de-oferta-al-50-en-la-tienda-de-feral
layout: post
tags:
- accion
- oferta
- feral-interactive
- exploracion
title: '''Sombras de Mordor GOTY'' de oferta al 50% en la tienda de Feral'
---
Si no tienes el juego, esta es una buena oportunidad

Aprovechando la época de rebajas los chicos de Feral Interactive han vuelto a poner de oferta el juego 'La Tierra Media: Sombras de Mordor' en la edición juego del año con un suculento descuento del 50%, lo que lo deja en unos imperdonables 9,99€.



> 
> Enter the ghostly world of the Feral Store to reveal a wonder invisible to mortal eyes: 50% off SoM for Mac & Linux. <https://t.co/mMxMjnecFM> [pic.twitter.com/YG2kP13jvZ](https://t.co/YG2kP13jvZ)
> 
> 
> — Feral Interactive (@feralgames) [13 de enero de 2017](https://twitter.com/feralgames/status/819879166661128193)



Si no lo tienes ya, es una buena oportunidad de comprar este grandísimo juego a un muy buen precio y de paso apoyarás diréctamente a los responsables del port a Linux.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/Zkj9MUbszxo" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


La oferta estará disponible hasta el día 20 de este mes. Puedes ver toda la información en la página de la tienda del juego en la web de Feral:


 <https://store.feralinteractive.com/es/mac-linux-games/shadowofmordor/>

