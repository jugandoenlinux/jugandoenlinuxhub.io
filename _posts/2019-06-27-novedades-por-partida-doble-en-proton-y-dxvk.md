---
author: leillo1975
category: Software
date: 2019-06-27 07:38:27
excerpt: "<p>Steam Play se renueva con la versi\xF3n&nbsp;4.2-9 de Proton, y DXVK\
  \ llega a la 1.2.3</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d9fc17f32b375e9eeed63a6c2d3200a4.webp
joomla_id: 1069
joomla_url: novedades-por-partida-doble-en-proton-y-dxvk
layout: post
tags:
- valve
- steam-play
- dxvk
- proton
title: Novedades por partida doble en Proton y DXVK (ACTUALIZADO)
---
Steam Play se renueva con la versión 4.2-9 de Proton, y DXVK llega a la 1.2.3

**ACTUALIZADO 28-6-19:** 


Tan solo un día después de haber lanzado la 4.2-8, llega una revsión de esta ([4.2-9](https://github.com/ValveSoftware/Proton/releases/tag/proton-4.2-9)) que simplemente corrige algunos problemas menores tal y como podemos ver aquí:


*-Correcciones para la funcionalidad multijugador en Mordhau, SOULCALIBUR VI, y otros con problemas de 4.2-8.*


 




---


**NOTICIA ORIGINAL (27-6-19):** 


Afinando un poquito en cada nuevo lanzamiento, cada cierto tiempo se actualizan estos dos **proyectos paralelos** que nos hacen llenarnos de "orgullo y satisfacción". Empezamos con **Steam Play/Proton**, que en el día de ayer apretaba un poco más la tuerca con algunas [novedades](https://github.com/ValveSoftware/Proton/wiki/Changelog) interesantes de esta herramienta que bien seguro aumentarán la compatibilidad y calidad de los juegos con los que lo usemos. Los cambios realizados con esta **versión 4.2-8** son los siguientes:


*-Correcciones para juegos que usan **navegadores web** utilizando el cliente Steam. Por ejemplo, **Football Manager 2019**.*  
 *-Soluciona un problema con **Bloodstained: Ritual of the Night** y otros títulos colapsando con un diálogo de error al salir.*  
 *-**Actualiza wine-mono a 4.9.0**, que incluye **soporte para winforms**. Esto puede ayudar a algunos lanzadores de juegos.*  
 *-**Gestión de ventanas y correcciones cuando usamos Alt-Tab**.*  
 *-Corrección para los controladores que pierden el **Force Feedback** cuando se retiran y se vuelven a añadir.*


Por su parte, **Philip Rebohle** (@[d](https://github.com/doitsujin)[oitsujin](https://github.com/doitsujin)), no se ha estado quieto ultimamente y ha continuado mejorando su **plugin DXVK** llevándolo a la versión 1.2.3, donde realiza **arreglos y mejoras** notables en este bendito complemento de Wine/Proton que nos permite ejecutar aplicaciones desarrolladas con DX10 y DX11 usando para ello la potencia de Vulkan. En la lista de cambios podemos ver las siguientes [novedades](https://github.com/doitsujin/dxvk/releases/tag/v1.2.3):  



*-Corregido error que causaba que algunos juegos de **Unreal Engine 4** mostraran mensajes de error al salir (PR #1104)*  
 *-Se ha corregido la regresión que rompía la carga de texturas en los **World of Warships** y potencialmente en otros juegos (#1096).*  
 *-Más **optimizaciones menores de la sobrecarga de la CPU***  
 *-Se han implementado correctamente las consultas desagregadas de marcas de tiempo para que la **cuantificación del tiempo en la GPU sea más robusta**.*  
 *-Mejora del comportamiento de **asignación de memoria cuando está bajo presión**. En algunas situaciones, esto **puede mejorar el rendimiento de las GPUs Nvidia de gama baja**.*  
 *-Mejora del comportamiento de **asignación de búfer de escalonamiento** para limitar la cantidad de memoria que se reserva permanentemente para la carga de recursos.*


Debeis sabe que **aunque Proton use DXVK, los cambios de este último por ahora aun no están implementados en al herramienta de Valve**, por lo que el primero continuará usando la versión 1.2.1 de DXVK, que fué introducida en la la 4.2-6 de Proton. Es bien seguro que probablemente en poco tiempo la veamos incluida, por lo que continuaremos atentos a estos dos proyectos para contaros todas sus novedades.


¿Habeis probado ya los cambios? ¿Habeis detectado alguna mejoría con vuestros juegos? Cuentanoslo en los comentarios, en nuestra [comunidad de Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

