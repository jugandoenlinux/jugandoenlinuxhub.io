---
author: leillo1975
category: "Simulaci\xF3n"
date: 2018-11-06 08:40:23
excerpt: <p>@SCSsoftware nos sorprende con esta DLC gratuita</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d223ee4fdebdcd40ae2cd65f88141ed1.webp
joomla_id: 891
joomla_url: volvo-vnl-la-nueva-marca-de-camiones-para-american-truck-simulator
layout: post
tags:
- dlc
- american-truck-simulator
- scs-software
- volvo-vnl
title: Volvo VNL, la nueva marca de camiones para American Truck Simulator.
---
@SCSsoftware nos sorprende con esta DLC gratuita

Una de las pocas quejas que los usuarios de American Truck Simulator tienen sobre el juego es la **falta de camiones donde elegir.** Al contrario que su hermano mayor, Euro Truck Simulator, hasta ahora solo permitía comprar camiones de dos marcas, **Kenworth** y **Peterbilt**, con un máximo de 4 camiones diferentes donde elegir.


En el día de ayer, los desarrolladores de la checa [SCS Software](index.php/homepage/analisis/tag/SCS%20Software), [lanzaron su última DLC para ATS](http://blog.scssoft.com/2018/11/volvo-vnl-coming-to-american-truck.html), y lo mejor de todo es que ha sido de forma **gratuita**. Esta DLC incluye un **quinto camión** en el catálogo del juego, el **Volvo VNL**, perteneciente a la anterior generación de camiones de la marca en Estados Unidos. Obviamente **han sido habilitados por todo el mapa disponible sus concesionarios** correspondientes. Con esta cabeza tractora aumentan aun más las posibilidades dentro del juego, y por supuesto viene acompañada de un buen número de personalizaciones posibles.


SCS también ha anunciado que **están trabajando para incluir el último modelo de la marca**, y lo más interesante, nos comentan que ya han "logrado el éxito en varios frentes" por lo que en un futuro, esperemos que no muy lejano, v**eremos más marcas disponibles en el simulador**. Nosotros en JugandoEnLinux os ofreceremos como siempre toda la información referente a este tema y todo lo relaccionado con SCS software como hemos hecho hasta ahora. Os dejamos con el directo que ayer mismo grabamos para celebrarlo y que así pudierais ver de primera mano este nuevo camión, el Volvo VNL, por las carreteras de [Oregón](index.php/homepage/analisis/20-analisis/998-analisis-american-truck-simulator-dlc-oregon):


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/w8iMxf0q8tI" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Habeis probado ya esta nueva DLC? ¿Qué os parece el nuevo modelo de camión? Dejanos tu opinión en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

