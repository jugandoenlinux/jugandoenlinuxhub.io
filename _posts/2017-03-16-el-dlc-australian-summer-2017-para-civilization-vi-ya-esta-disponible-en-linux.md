---
author: Serjor
category: Estrategia
date: 2017-03-16 20:35:39
excerpt: "<p>Modos de juego, mejoras y ajustes para la ocasi\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/983fb56032c1ac1b726360eae9dd3d18.webp
joomla_id: 254
joomla_url: el-dlc-australian-summer-2017-para-civilization-vi-ya-esta-disponible-en-linux
layout: post
tags:
- dlc
- civilization-vi
- civilization
title: "El DLC 'Australian Summer 2017' para Civilization VI ya est\xE1 disponible\
  \ en Linux"
---
Modos de juego, mejoras y ajustes para la ocasión

Parece que después de los lanzamientos de navidad y los parches de rigor, en esta primera mitad de año los estudios se están poniendo las pilas en lanzar nuevo contenido para nuestros juegos favoritos, y en esta ocasión le toca el turno a Civilization VI, cuyo análisis podréis encontrar [aquí](index.php/homepage/analisis/item/352-analisis-sid-meier-s-civilization-vi).



> 
> G'Day, Civ 6 Mac and Linux players! The 'Australian Summer' update is now available on Steam :D <https://t.co/URn6Iq91e3> [pic.twitter.com/avG4CH46Mi](https://t.co/avG4CH46Mi)
> 
> 
> — Aspyr Media (@AspyrMedia) [14 de marzo de 2017](https://twitter.com/AspyrMedia/status/841686628036935681)



Cómo anuncian en su tweet, los chicos de Aspyr han sacado por un lado una [actualización gratuíta](https://blog.aspyr.com/2017/03/14/sid-meiers-civilization-vi-australian-summer-2017-update-live-steam-mac-linux/), la cuál incluye numerosos cambios, entre ellos:


* Soporte para SteamWorkshop
* Partidas multijugador por equipos
* Mejora en la inteligencia artificial
* Correcciones de errores
* Y una larga lista de cambios que podéis encontrar [aquí](https://drive.google.com/file/d/0B2WdW_Sub4U6LUEybTZZR1hCbHM/view)


Y junto con estos cambios llega el DLC 'Australian Summer 2017', aunque este ya es de pago, en el que Australia se añade como civilización con unidades y habilidades exclusivas.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/11HVt8f0X-I" width="560"></iframe></div>


 


Os dejamos con el enlace a la página de Steam para haceros con el DLC de la región Australiana:


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/512034/" width="646"></iframe></div>


 


Y tú, ¿te harás con este nuevo DLC? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

