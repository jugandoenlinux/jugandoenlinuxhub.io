---
author: Serjor
category: "Acci\xF3n"
date: 2017-10-10 21:46:19
excerpt: <p>Nuevos contratos a lo largo del mes con un nuevo contenido a final del
  mismo</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d6f811fd0c668b06401e12f22b91eabb.webp
joomla_id: 480
joomla_url: octubre-sera-un-mes-interesante-para-los-seguidores-de-hitman
layout: post
tags:
- hitman
title: "Octubre ser\xE1 un mes interesante para los seguidores de Hitman"
---
Nuevos contratos a lo largo del mes con un nuevo contenido a final del mismo

La gente de IO Interactive nos [anuncia](https://www.ioi.dk/hitman-october-content-schedule/) el nuevo contenido del mes de octubre para Hitman.


Y este que este mes que comienza pinta muy bien para el Agente 47, ya que nos prometen nuevos contratos y contenido extra muy potente:


![hitman october](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisHitman/contenido_octubre_2017/hitman_october.webp)


Para empezar el día 10 de octubre tendremos:


* 10 nuevos contratos hechos por la comunidad
* Pack de retos del espantapájaros que si completamos nos proporcionará un ítem para poder usar en cualquier escenario


A mediados de mes llega más contenido:


* 10 nuevos contratos elegidos por uno de los jugadores


Y de cara al 24 de octubre nos traerán un nuevo contenido que se intuye hará las delicias de los fans. No obstante, y como ya aclaran, a pesar de este nuevo contenido tan prometedor, no se trata de la temporada 2 del juego, la cuál está aún por llegar.


![hitman october content](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisHitman/contenido_octubre_2017/hitman_october_content.webp)


 


Si os interesa, lo tenéis disponible en steam:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/236870/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Y tú, ¿desplegarás tus habilidades asesinas con este nuevo contenido? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

