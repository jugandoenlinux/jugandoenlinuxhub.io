---
author: lordgault
category: "Simulaci\xF3n"
date: 2018-11-10 18:17:10
excerpt: "<p>SCS sigue a\xF1adiendo contenido a los veh\xEDculos de su gran simulador</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6111134b69b9dcb9c953bb227317f7fc.webp
joomla_id: 896
joomla_url: llega-special-transport-para-american-truck-simulator
layout: post
tags:
- simulador
- american-truck-simulator
- ats
- simulacion
title: Llega Special Transport para American Truck Simulator
---
SCS sigue añadiendo contenido a los vehículos de su gran simulador

Ya tenemos el nuevo [DLC Special Transport de American Truck](https://store.steampowered.com/app/962750/American_Truck_Simulator__Special_Transport/) Simulator disponible para nuestro sistema operativo favorito, y para el resto, claro. Al igual que en su homologo Euro Truck Simulator 2, el nuevo DLC nos permite realizar trabajos con mercancías especiales las cuales pondrán a prueba nuestra destreza la volante.


Este DLC también ha recibido muy buenas críticas, es de suponer entre otros, que su reducido precio tiene buena culpa de ello y es que puede ser tuyo por tan solo 4,99 €. En los viajes en los que tendrás que transportar helicópteros, barcos, casas, estructuras de almacenes, etc, no esterás solo. Un pequeño grupo de ayudantes harán de tu trayecto un trabajo algo más cómodo.


Este DLC te dará:


 


* 11 cargas enormes (hasta 170.000 libras (85 toneladas), 21 pies (6,5 m) de ancho, 17 pies (5+ m) de alto y 52+ pies (16 m) de largo).
* Nuevos remolques especiales de transporte.
* Rutas de trabajo únicas para el Transporte Especial (la mayoría en el mapa base, algunos a través de Nuevo México, más para que Oregon venga más tarde como una actualización gratuita).
* Una combinación de más de 90 trabajos de transporte especial.
* Acompañamiento personalizado de IA y vehículos de la policía.
* Nuevos modelos, personajes y animaciones de personas que asisten el transporte.
* Peatones curiosos, periodistas o compañeros de transporte a lo largo de las rutas.
* Logros especiales en el sector del transporte.


 Podéis ver el trailer del DLC a continuación:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/KuQp-sgAdbA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 

