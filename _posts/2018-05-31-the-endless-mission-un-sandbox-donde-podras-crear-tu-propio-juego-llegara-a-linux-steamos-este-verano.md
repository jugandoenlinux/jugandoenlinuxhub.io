---
author: Pato
category: Sandbox
date: 2018-05-31 15:30:26
excerpt: <p>Modela tu juego a tu gusto mediante mods y herramientas</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2fe3e76a66d4105790a62f68d8e5622a.webp
joomla_id: 751
joomla_url: the-endless-mission-un-sandbox-donde-podras-crear-tu-propio-juego-llegara-a-linux-steamos-este-verano
layout: post
tags:
- indie
- proximamente
- sandbox
title: "'The Endless Mission' un sandbox donde podr\xE1s crear tu propio juego llegar\xE1\
  \ a Linux/SteamOS este verano"
---
Modela tu juego a tu gusto mediante mods y herramientas

Curiosa propuesta la que nos propone este 'The Endless Mission'. Se trata de un "sandbox" en toda regla en el que se nos invita a emprender un viaje donde fuerzas misteriosas nos llevarán tras las cortinas de distintos géneros de juegos. En este "Sandbox de creación" podremos combinar, mezclar y modificar estos géneros para desbloquear poderosas herramientas basadas en Unity para crear tus propias experiencias y compartirlas con la comunidad.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/rvLmnpf0p-4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Ante todo, llama la atención el hecho de que puedas "personalizar" el juego a tu gusto, añadiendo o quitando elementos para "crear" una experiencia única, y posteriormente poder compartirla con la comunidad.


En cuanto a las carácterísticas, destacan:


Herramientas integradas para modificación y creación basadas en el motor de Unity  
Historia y narrativa creada por Sleep Deprivation Labs  
Modificación en tiempo real de los recursos del juego que afectarán en la jugabilidad  
Calendario de lanzamiento de DLCs Rolling sin costo adicional


Si tienes curiosidad sobre cómo será y todos los detalles sobre 'The Endless Missión', puedes visitar [su página web](https://theendlessmission.com/) o su [página de Steam](https://store.steampowered.com/app/827880/The_Endless_Mission/) donde estará disponible este próximo verano, sin fecha concreta.


Estaremos atentos a las noticias al respecto de su lanzamiento.

