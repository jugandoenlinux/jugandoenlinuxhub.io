---
author: Serjor
category: "Exploraci\xF3n"
date: 2018-10-23 18:46:25
excerpt: <p>Buscan enamorar a antiguos y nuevos jugadores</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/67df04ded316a5b3c8f749febe0cedd2.webp
joomla_id: 878
joomla_url: the-away-team-se-prepara-a-viajar-por-el-espacio-con-una-gran-actualizacion
layout: post
tags:
- actualizacion
- the-away-team
title: "The Away Team se prepara a viajar por el espacio con una gran actualizaci\xF3\
  n"
---
Buscan enamorar a antiguos y nuevos jugadores

Nos contactaba [Cheeseness](https://mastodon.social/@Cheeseness) a través de [nuestra cuenta de mastodon](https://mastodon.social/@jugandoenlinux) para avisarnos de que ya está disponible una gran actualización del juego The Away Team:


<div class="resp-iframe"><iframe class="mastodon-embed" src="https://mastodon.social/@Cheeseness/100945518851730905/embed" style="max-width: 100%; border: 0;" width="400"></iframe></div>




Y es que después de unos cuantos meses de desarrollo tenemos disponible una actualización que en sus propias palabras es necesaria y pretenden darle un nuevo empujón al juego. Podéis leer sobre la actualización [aquí](https://steamcommunity.com/games/426290/announcements/detail/1685928465899074268)


El juego es una mezcla de FTL y aventura conversacional donde tendremos que leer, pero leer mucho, ya que según anuncian hay más de 120000 palabras, lo malo, que están en perfecto inglés, así que si no dominamos el idioma, va a ser un poco duro.


La lista de cambios de esta nueva versión:


* Más historia
* Mejoras en la exploración de los sectores, con nuevas mecánicas de control de la nave, misiones aleatorias y campos de asteroides
* Mejoras estéticas en los gráficos
* Nueva forma de dar experiencia a la tripulación
* Un personaje nuevo con su propio pasado para poder investigarlo
* Más de 50 elementos “memento” para poder recolectar (ni idea de qué es, la verdad)
* Nuevos logros de Steam
* Editor de personaje con integración en Steam Workshop
* Mejorado el sistema de renderizado de sprites
* Texto más claro y opción a usar fuentes que ayuden a la gente con dislexia


La propia desarrolladora reconoce que es un juego muy nicho, pero si te gustan este tipo de juegos, no dudes en darle una oportunidad, y para ello puedes hacerte con él en steam y en [itch.io](https://underflow-studios.itch.io/the-away-team).


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/426290/" style="border: 0px;" width="646"></iframe></div>


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/t12HtZWVxxQ" width="560"></iframe></div>


[Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

