---
author: Pato
category: Realidad Virtual
date: 2017-05-26 08:38:42
excerpt: <p>El entorno de realidad virtual de Valve sigue avanzando y ofrece soporte
  de forma experimental</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6faf95a407946923d692f4ddda792716.webp
joomla_id: 342
joomla_url: steamvr-beta-ya-ofrece-soporte-para-steamvr-home-en-linux-steamos
layout: post
tags:
- steamos
- steamvr
title: SteamVR Beta ya ofrece soporte para SteamVR Home en Linux/SteamOS
---
El entorno de realidad virtual de Valve sigue avanzando y ofrece soporte de forma experimental

Después de que SteamVR fuera lanzado para Linux el año pasado, Valve no ha parado de trabajar de cara a traer la realidad virtual a Linux/SteamOS.


Así, en su última actualización en la rama beta de SteamVR ya ofrece soporte experimental para el entorno de lanzamiento de aplicaciones VR en Linux/SteamOS. Así lo anunció el propio Pierre Loup, encargado del proyecto:



> 
> SteamVR Home now with Linux support!<https://t.co/0U5sq1GJnS>
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [May 25, 2017](https://twitter.com/Plagman2/status/867885692101251074)



El anuncio oficial se puede ver [en este enlace](https://steamcommunity.com/games/250820/announcements/detail/1256914311376778619).


SteamVR Home es un entorno virtual para tener una experiencia rica e interactiva a la hora de manejar el lanzamiento y la gestión de las aplicaciones dentro de SteamVR. Puedes ver toda la información en [su página de información de SteamVR Home](https://steamcommunity.com/games/250820/announcements/detail/1256913672017157095). También puedes visitar la web oficial del proyecto [en este enlace](https://steamcommunity.com/steamvr).


¿Estás interesado en la realidad virtual en Linux/SteamOS?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

