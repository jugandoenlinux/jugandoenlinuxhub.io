---
author: leillo1975
category: Carreras
date: 2018-07-23 10:32:53
excerpt: "<p>Thorsten Folkers nos desvelan material e informaci\xF3n sobre su juego</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/681a23d9bda3cc6608e358bb4db90e3a.webp
joomla_id: 809
joomla_url: los-desarrolladores-de-drag-nos-muestran-nuevos-avances
layout: post
tags:
- early-access
- drag
- thorsten-folkers
title: Los desarrolladores de DRAG nos muestran nuevos avances.
---
Thorsten Folkers nos desvelan material e información sobre su juego

Hacía tiempo que no teníamos noticias sobre los avances de DRAG. Si recordais hace unos meses [os hablamos](index.php/homepage/generos/carreras/item/735-pronto-veremos-drag-un-juego-de-carreras-en-acceso-anticipado) de este proyecto de los hermanos [Thorsten Folkers](https://tfolkers.artstation.com/), un juego de carreras con un acabado gráfico sorprendente, y más teniendo en cuenta que está hecho solo por dos personas. Hace unos días actualizaban el estado del desarrollo informando de las partes completadas y detallándonos los siguientes pasos que darían en la creación de su videojuego.


En cuanto a lo primero, las **partes completadas**, han termindo lo siguente en las últimas semanas :



> 
> *-Terminado el modo online de carreras*
> 
> 
> *-Se ha pulido el modo de un jugador*
> 
> 
> *-Se ha testeado y corregido errores*
> 
> 
> *-Añadidos los tiempos de sector y los tiempos de vuelta*
> 
> 
> *-Incluido un modo de carrera por tiempos*
> 
> 
> *-Graduación de color mejorada*
> 
> 
> 


 


En cuanto al **trabajo que están realizando actualmente** en el juego :



> 
> *-Realizando pruebas de compatibilidad de Hardware*
> 
> 
> *-Creando visuales para las pantallas de carga*
> 
> 
> *-Acumulación dinámica de lodo en los autos*
> 
> 
> *-Trabajando en un segundo tipo de coche.*
> *-Comenzando con la consutrucción de una nueva pista de carreras con un nuevo entorno.*
> 
> 
>  
> 
> 
> 


Como veis no han perdido el tiempo, y todos estamos deseando poder probarlo en cuanto salga en modo de acceso anticipado. Como siempre, en JugandoEnLinux.com os mantendremos informados de todas la novedades que genere este interesante proyecto. Os dejamos con un video que han realizado sobre su trabajo con los menues del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/kzBwATD1mxc" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


