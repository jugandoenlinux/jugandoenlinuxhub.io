---
author: Serjor
category: Rol
date: 2017-03-14 21:00:43
excerpt: "<p>Incluye tambi\xE9n mejoras y nuevas opciones de configuraci\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4effff5398b13b476604d4d8450bedfd.webp
joomla_id: 248
joomla_url: nuevos-modos-de-juego-para-the-dwarves
layout: post
tags:
- rol
- dwarves
- actualizacion
title: Nuevos modos de juego para 'The Dwarves'
---
Incluye también mejoras y nuevas opciones de configuración

Los poseedores de 'The Dwarves', el título de la compañía THQ que nos permite controlar grupos de enanos, junto con otros tipos de personajes, están de enhorabuena, ya que el juego acaba de recibir una actualización que seguramente será del agrado de sus usuarios.


Este parche incluye un nuevo modo de juego, llamado "Challenges" que incluye tres modalidades diferentes:


* Modo Horda, donde nos irán llegando oleadas de enemigos y en cada oleada irá aumentando la dificultad del enfrentamiento
* Time Challenge, en el cuál deberemos acabar con la mayor cantidad de enemigos lo más rápidamente posible
* Chase (persecución), modo en el que perseguiremos un orco y al que deberemos robarle su saca


 


![Los nuevos modos de juego](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDwarves/dwarves_actualizacion_challenges.webp)


 


A parte, este parche incluye también algunas correcciones de errores, un nuevo modo de dificultad "muy fácil", más opciones para los ajustes gráficos, la capacidad de reasignar teclas, mejoras en la cámara y el idioma turco. Podéis encontrar la lista de cambios [aquí](http://www.kingart-games.com/blog/1534).


Por desgracia la rebaja que [mencionábamos ayer](index.php/homepage/generos/rol/item/368-ahorra-un-40-al-comprar-the-dwarves) ya no está disponible, sino hubiera sido una buena oportunidad para hacerse con el juego, cuyo análisis podéis leer [aquí](index.php/homepage/analisis/item/247-analisis-the-dwarves).


 


Referencias: [The Linux Game Consortium](https://linuxgameconsortium.com/linux-gaming-news/the-dwarves-major-update-adds-three-different-game-modes-49816/)


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/403970/79083/" width="646"></iframe></div>


 

