---
author: Pato
category: Aventuras
date: 2017-06-13 16:24:02
excerpt: <p>Fue una de las sorpresas en el PC Gaming Show</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3946cc5a2ed843c2c9fca0b4efcd28ba.webp
joomla_id: 373
joomla_url: tunic-el-juego-de-finji-tiene-posibilidades-de-llegar-a-linux
layout: post
tags:
- accion
- indie
- aventura
title: TUNIC, el juego de Finji tiene posibilidades de llegar a Linux
---
Fue una de las sorpresas en el PC Gaming Show

Si estuviste atento al PC Gaming Show que estuvimos retransmitiendo en nuestro canal de [Twitch](https://www.twitch.tv/jugandoenlinux)" hubo un juego que no dejó indiferente a nadie. Entre tanto triple A y tanto músculo gráfico **TUNIC** [[web oficial](http://www.tunicgame.com/)] se hizo hueco con un escueto trailer que dejó a más de uno preguntando qué era aquello, incluyéndome a mi.


Según su escueta descripción, se trata de *una aventura de acción sobre un pequeño zorro en un vasto mundo. Explora el desierto, descubre ruinas espeluznantes y lucha contra terribles y antiguas criaturas.*


Pero lo que de verdad llama la atención es... mejor que lo veas por ti mismo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/0f9OoELUhKY" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 No ha faltado el que lo ha comparado con todo un Zelda. Indagando un poco más, se sabe que a TUNIC en realidad se le conocía hasta ahora como "**Secret Legend**" y que detrás del proyecto está FINJI, compañía responsable de títulos como '[Night in the Woods](http://store.steampowered.com/app/481510/Night_in_the_Woods/)', '[FEIST](http://store.steampowered.com/app/327060/FEIST/)' u '[Overland](http://store.steampowered.com/app/355680/Overland/)' que están o estarán (en el caso de Overland) todos ellos disponibles para Linux. La pregunta obvia es: ¿llegará TUNIC a Linux también?


Dado que ni en el anuncio ni en la web oficial, ni siquiera en su [página de Steam](http://store.steampowered.com/app/553420/TUNIC/) mencionan su salida en Linux un usuario en Twitter les ha hecho esta misma pregunta:



> 
> we’ve been happily sim-shipping Linux on [@OverlandGame](https://twitter.com/OverlandGame) for over a year now, so bear with us <3
> 
> 
> — Finji @E3 (@FinjiCo) [12 de junio de 2017](https://twitter.com/FinjiCo/status/874333509174906883)



 "*Hemos estado lanzando felizmente Linux en @OverlandGame desde hace un año, por tanto quedaros con nosotros"*


De acuerdo que no es una confirmación en toda regla, pero desde luego con los "antecedentes" de lanzamientos para nuestro sistema favorito no es descabellado pensar que este TUNIC llegará a Linux próximamente.


¿Qué te parece TUNIC? ¿Te gustaría que llegase a Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

