---
author: leillo1975
category: Software
date: 2017-01-07 15:53:04
excerpt: "<p>En las \xFAltimas 48 horas los desarrolladores del cliente de Steam han\
  \ lanzado una nueva beta de esta aplicaci\xF3n. Esta tiene muchas particularidades\
  \ que afectan muy directamente a la experiencia de uso de dicho programa con los\
  \ usuarios de GNU/Linux. Muchas de estas son viejas demandas y otras son novedades\
  \ propiamente dichas. A continuaci\xF3n paso a detallarlas:</p>\r\n<p>- Steam ahora\
  \ deber\xEDa funcionar sin problemas con los controladores abiertos en las distribuciones\
  \ de Linux m\xE1s modernas. Debido a que cada vez m\xE1s usuarios est\xE1n usando\
  \ los controladores abiertos en sus equipos, sobretodo en los que tienen gr\xE1\
  ficas AMD, la gente de de Valve se ha puesto manos a la obra para que estos funcionen\
  \ directamente , sin pasos previos, con el cliente. En el anuncio tambi\xE9n indican\
  \ que si se experimentan problemas se puede usar \"STEAM<em>RUNTIME</em>PREFER<em>HOST</em>LIBRARIES=0\"\
  \ para que todo sea como en un principio. Tambi\xE9n han decidido meterle mano a\
  \ la librer\xEDa \"libxcb\" para solucionar los cuelgues relaccionados con DRI3\
  \ que se producian de vez en cuando.</p>\r\n<p>- La aplicaci\xF3n se cierra ahora\
  \ correctamente la bandeja del sistema, tal y como lo hace en Windows o Mac. Esta\
  \ caracter\xEDstica era una vieja demanda de los usuarios desde hace bastante tiempo\
  \ y extra\xF1aba bastante, la verdad, que hasta ahora no se solucionase.</p>\r\n\
  <p>- El cliente ahora detecta la inactividad del usuario como en el resto de plataformas\
  \ y lo indica en el chat a todos nuestros amigos, indicando que estamos ausentes\
  \ cuando llevamos tiempo sin usar nuestro equipo.</p>\r\n<p>- En el hardware AMD,\
  \ se ha modificado la interfaz de Steam durante la partida cuando se usa el controlador\
  \ Vulkan. Tambi\xE9n se corrigen problemas con el teclado y el cursor en estas circunstancias.</p>\r\
  \n<p>- Se han seguido mejorando las funcionalidades de los controladores, sus mapeos,\
  \ funcionamiento, etc; no solo en el Steam Controller, sin\xF3 tambi\xE9n en mandos\
  \ de XBOX One/360 y PS4.</p>\r\n<p><br data-mce-bogus=\"1\" />Por supuesto que hay\
  \ m\xE1s correcciones y funciones nuevas, pero estas creo que son, a mi humilde\
  \ juicio, las m\xE1s importantes. Si quereis consultar la lista completa de cambios\
  \ <strong><span style=\"color: #000080;\"><a href=\"http://steamcommunity.com/groups/SteamClientBeta/announcements/detail/586991182161672256\"\
  \ target=\"_blank\" style=\"color: #000080;\">pulsad aqu\xED</a></span></strong>.</p>\r\
  \n<p>&nbsp;</p>\r\n<p><strong>Fuentes:</strong> <a href=\"http://steamcommunity.com/groups/SteamClientBeta#announcements/detail/586991182161672256\"\
  \ target=\"_blank\">Steam Community,</a> <a href=\"https://www.genbeta.com/linux/steam-se-hace-por-fin-compatible-por-defecto-con-drivers-graficos-open-source-en-gnu-linux\"\
  \ target=\"_blank\">Genbeta.com</a>, <a href=\"http://phoronix.com/scan.php?page=news_item&amp;px=Steam-Client-Beta-20170105\"\
  \ target=\"_blank\">Phoronix.com</a></p>\r\n<p>&nbsp;</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d65085d854a39feae81a41eb458281c5.webp
joomla_id: 169
joomla_url: valve-actualiza-la-beta-de-steam-con-importantes-mejoras
layout: post
tags:
- steam
- drivers
- vulkan
- valve
title: Valve actualiza la beta de Steam con importantes mejoras
---
En las últimas 48 horas los desarrolladores del cliente de Steam han lanzado una nueva beta de esta aplicación. Esta tiene muchas particularidades que afectan muy directamente a la experiencia de uso de dicho programa con los usuarios de GNU/Linux. Muchas de estas son viejas demandas y otras son novedades propiamente dichas. A continuación paso a detallarlas:


- Steam ahora debería funcionar sin problemas con los controladores abiertos en las distribuciones de Linux más modernas. Debido a que cada vez más usuarios están usando los controladores abiertos en sus equipos, sobretodo en los que tienen gráficas AMD, la gente de de Valve se ha puesto manos a la obra para que estos funcionen directamente , sin pasos previos, con el cliente. En el anuncio también indican que si se experimentan problemas se puede usar "STEAM*RUNTIME*PREFER*HOST*LIBRARIES=0" para que todo sea como en un principio. También han decidido meterle mano a la librería "libxcb" para solucionar los cuelgues relaccionados con DRI3 que se producian de vez en cuando.


- La aplicación se cierra ahora correctamente la bandeja del sistema, tal y como lo hace en Windows o Mac. Esta característica era una vieja demanda de los usuarios desde hace bastante tiempo y extrañaba bastante, la verdad, que hasta ahora no se solucionase.


- El cliente ahora detecta la inactividad del usuario como en el resto de plataformas y lo indica en el chat a todos nuestros amigos, indicando que estamos ausentes cuando llevamos tiempo sin usar nuestro equipo.


- En el hardware AMD, se ha modificado la interfaz de Steam durante la partida cuando se usa el controlador Vulkan. También se corrigen problemas con el teclado y el cursor en estas circunstancias.


- Se han seguido mejorando las funcionalidades de los controladores, sus mapeos, funcionamiento, etc; no solo en el Steam Controller, sinó también en mandos de XBOX One/360 y PS4.


  
Por supuesto que hay más correcciones y funciones nuevas, pero estas creo que son, a mi humilde juicio, las más importantes. Si quereis consultar la lista completa de cambios **[pulsad aquí](http://steamcommunity.com/groups/SteamClientBeta/announcements/detail/586991182161672256)**.


 


**Fuentes:** [Steam Community,](http://steamcommunity.com/groups/SteamClientBeta#announcements/detail/586991182161672256) [Genbeta.com](https://www.genbeta.com/linux/steam-se-hace-por-fin-compatible-por-defecto-con-drivers-graficos-open-source-en-gnu-linux), [Phoronix.com](http://phoronix.com/scan.php?page=news_item&px=Steam-Client-Beta-20170105)


 

