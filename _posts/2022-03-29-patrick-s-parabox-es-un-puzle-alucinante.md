---
author: P_Vader
category: Puzzles
date: 2022-03-29 17:09:52
excerpt: "<p>Por fin llega este original juego donde todo es una paradoja recursiva\
  \ y en bucle, casi infinito.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/PatricksParabox/patricks_parabox_logo.webp
joomla_id: 1453
joomla_url: patrick-s-parabox-es-un-puzle-alucinante
layout: post
tags:
- puzzles
title: Patrick's Parabox es un puzle alucinante
---
Por fin llega este original juego donde todo es una paradoja recursiva y en bucle, casi infinito.


Otro buen juego nativo para linux descubierto gracias al Next Fest donde probé su demo hace bastante tiempo y que desde entonces seguía de cerca, se trata de **Patrick's Parabox desarrollado por [Patrick Traynor](https://twitter.com/clockworkpat). El juego en si es bastante original, con puzles donde hay que hacerse con una mecánica un tanto peculiar**: los rompecabezas están en cajas, que a menudo están dentro de las mismas cajas y de nuevo en las mismas cajas, de manera recursiva, en un bucle, hasta el infinito...


¡¿Complejo?! No, no, no, **quizás es complejo de describirlo, pero una vez que lo pruebas y aprendes la mecánica es extremadamente adictivo darle unas pocas vueltas a la cabeza para ver como resolver estos increíbles puzles.**


Mejor mira el video para entender que quiero decir y no puedo:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/Rl_ohYPXNMo" title="YouTube video player" width="720"></iframe></div>


En total son **350 puzles con una curva de dificultad muy equilibrada y bastante intuitivo** para ir aprendiendo como resolverlos ayudándote de los bucles de cajas.


 ![](https://www.patricksparabox.com/press/images/patricksparabox_gif1.gif)


Su música relajante acompaña agradablemente al juego sin molestar o distraer, una buena obra de [Priscilla Snow](https://www.ghoulnoise.com):
<div class="resp-iframe"><iframe seamless="" src="https://bandcamp.com/EmbeddedPlayer/album=1568813989/size=small/bgcol=ffffff/linkcol=0687f5/transparent=true/" style="border: 0; width: 75%; height: 42px;"><a href="https://ghoulnoise.bandcamp.com/album/patricks-parabox-original-soundtrack">Patrick's Parabox (Original Soundtrack) by Ghoulnoise</a></iframe></div>


Otro punto cuidado y personalmente muy valorado es que **se han preocupado por cuidar aspectos de accesibilidad para diferentes circunstancias o dificultades** ¡Bravo! Podéis leer los detalles del informe: <https://www.taminggaming.com/accessibility/Patricks+Parabox>


Desde aquí os recomiendo que lo probéis y si estas interesado en este juego puedes encontrarlo en Steam o Itch:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1260520/" style="border: 0px none;" width="646"></iframe></div>


<div class="resp-iframe"><iframe class="itch" height="167" src="https://itch.io/embed/638497?dark=true" width="552"><a href="https://patricktraynor.itch.io/patricks-parabox">Patrick's Parabox by Patrick Traynor</a></iframe></div>


 Y recuerda que **tiene una buena demo para probarlo.** ¿Te ha gustado? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

