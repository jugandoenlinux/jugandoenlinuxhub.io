---
author: Odin
category: Editorial
date: 2022-12-28 15:30:44
excerpt: "<p>En este especial, os damos a conocer los juegos favoritos del 2022 de\
  \ Jugando en Linux.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/NuestrosJuegos2022/spidermanremastered.webp
joomla_id: 1516
joomla_url: nuestros-juegos-del-2022-primera-parte
layout: post
tags:
- steam
- goty
- steam-deck
title: Nuestros Juegos del 2022 (primera parte)
---
En este especial, os damos a conocer los juegos favoritos del 2022 de Jugando en Linux.


Quedan pocos días para terminar este año y desde **Jugando en Linux** hemos querido preguntar a nuestros colaboradores cuáles son los videojuegos que más han disfrutado y cuáles son los que más esperan para el próximo 2023. Al final tenían tanto que contar que os hemos dividido este artículo en 2 partes. Hoy os publicamos la **primera parte** y en **breve tendréis la segunda**. Y ahora, sin más dilación, os presentamos lo que nos han escrito parte del equipo de Jugando en Linux:


**Odin**
--------


Lo cierto es que este **2022** ha sido un magnífico año para **Linux**, y en gran medida ha sido gracias a la Steam Deck que ha dado un nuevo impulso a Linux como plataforma de videojuegos y que en cierta manera ha conseguido popularizarlo tanto en canales de youtube como en medios generalistas. Otra de las ventajas que ha tenido su lanzamiento es que Valve ha conseguido colaborar con editoras y desarrolladores para conseguir que los lanzamientos de PC más esperados puedan jugarse en la **Steam Deck** con calificación de **verificados**. De este trabajo de Valve con las editoras quiero destacar el trabajo que ha realizado con Sony y es por eso que de los videojuegos que he jugado en el 2022 voy a elegir estos dos juegos: **God of War y Marvel's Spider-Man Remastered**.


![Spiderman Remastered](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/NuestrosJuegos2022/spidermanremastered.webp)


No os voy a contar de que van, ya que son tan populares que a estas alturas ya lo sabréis todo acerca de estos dos juegos, sin embargo, lo que si os puedo decir es que la experiencia en la Steam Deck ha sido increíble. Es realmente sorprendente como van de bien de rendimiento, especialmente God of War, y la calidad gráfica es asombrosa. Es una pasada poder disfrutar de **las versiones completas de PC** en una **portátil**.


Con respecto al juego que más espero para Linux en el 2023 es [Kingdoms of the Dump](https://kingdomsofthedump.com/). Es un juego de rol indie con un argumento muy peculiar, ya que el protagonista es un caballero con forma de **papelera** que deberá luchar a través de los múltiples reinos del **vertedero** contra el **Ejército Tóxico**. Desde un punto de vista técnico, el juego está hecho en [Godot Engine](https://godotengine.org/) y tendrá versión nativa.


![Kingdoms Of the Dump](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/NuestrosJuegos2022/kingdomsdump.webp)


**Serjor**
----------


Si 2022 no ha sido uno de los mejores años del mundo de los videojuegos en cuestión de lanzamientos, pocos más habrá que le hagan sombra. Yo, por mi parte, no lo descubriré hasta dentro de varios años, porque como gamer-boomer, las obligaciones me impiden jugar tanto como me gustaría. En todo caso, no puedo quejarme, ya que este año he jugado a una verdadera joya, un juego que considero imprescindible, y que no puedo dejar de recomendar. Eso sí, por tu bien, para que disfrutes de la experiencia de la mejor manera posible, y para que tú también tengas esa experiencia tan especial, no puedo hablar mucho más de él. [Inscryption](https://www.inscryption.com/ "https://www.inscryption.com/"), un juego que parece que es de cartas, pero que no lo es... No sé cómo explicarlo sin fastidiarlo, así que por favor, simplemente jugadlo. Suele estar de oferta, y vale cada céntimo que cuesta, que podéis adquirir en [Steam](https://store.steampowered.com/app/1092790/ "https://store.steampowered.com/app/1092790/")


 
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/70QOKsr27Po" title="YouTube video player" width="560"></iframe></div>


En segundo lugar, estaría un juego al que le tenía un cariño especial, y que por motivos tan viejuners como que perdí la partida, y por aquel entonces no había guardado en la nube, así que lo he vuelto a retomar, a ver si puedo terminarlo de una vez. **Mass Effect Legendary Edition**, que me quedé a medias del 3 en su día, y por hacer la partida completa, este año he jugado al 1 y al 2, y si bien, han pulido algunos de los defectos de los originales, son juegos de su época. Si os gustaron, os gustarán, si no los jugasteis en su día, probablemente os guste. Personalmente, creo que ha envejecido muy bien, y aunque gráficamente tienen sabor añejo, sobre todo el primero, el juego engancha. Disponible en Steam [aquí](https://store.steampowered.com/app/1328670/Mass_Effect_Legendary_Edition/ "https://store.steampowered.com/app/1328670/Mass_Effect_Legendary_Edition/")


 
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/n8i53TtQ6IQ" title="YouTube video player" width="560"></iframe></div>


De cara al año que viene, con un poco de suerte me gustaría poder completar Hades, Miles Morales y Stray (aunque este último únicamente porque tiene gatos, porque los comentarios me han hecho perder un poco la ilusión en él).


 ¿Qué te parecen los videojuegos que han escogido nuestros colaboradores? Cuéntanos cuáles son los videojuegos a los que más caña has dado durante este 2022 en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

