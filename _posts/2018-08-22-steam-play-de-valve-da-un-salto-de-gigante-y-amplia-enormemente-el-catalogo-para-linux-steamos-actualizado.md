---
author: leillo1975
category: Software
date: 2018-08-22 07:23:25
excerpt: <p>Integra "Proton", fork de Wine con DXVK para ofrecer compatibilidad con
  los juegos de Windows en Linux</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/755a09762452d6eb5c314d532540d319.webp
joomla_id: 838
joomla_url: steam-play-de-valve-da-un-salto-de-gigante-y-amplia-enormemente-el-catalogo-para-linux-steamos-actualizado
layout: post
tags:
- wine
- valve
- steam-play
- dxvk
- proton
title: "Steam Play de Valve da un salto de gigante y ampl\xEDa enormemente el cat\xE1\
  logo para Linux/SteamOS (actualizado)"
---
Integra "Proton", fork de Wine con DXVK para ofrecer compatibilidad con los juegos de Windows en Linux

**Actualización 29/08:**


Esta vez eso del "Valve time" parece que ha ido al revés de lo esperado, y el cliente estable ha sido [actualizado](https://store.steampowered.com/news/43351/) hoy mismo con la nueva funcionalidad, aunque por ahora los juegos anunciados siguen sin tener el logo de GNU/Linux - SteamOS


**Actualización 24/08:**


Ayer se actualizó el cliente de Steam de nuevo pues parece ser que lanzaron una versión de Proton con los marcadores de debug activados. Ahora debería estar resuelto, por lo que Pierre Loup aconseja probar de nuevo los juegos en Steam Play para notar la mejoría.



> 
> The Proton build we released yesterday accidentally contained a debug build of DXVK, which impacted performance. This is fixed now! If you ran benchmarks, might want to do it again. This user went from 34 -> 56 FPS in Dark Souls 3 after fixing it locally: <https://t.co/nKY7AnEJSH>
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [August 23, 2018](https://twitter.com/Plagman2/status/1032450256087044096?ref_src=twsrc%5Etfw)


  





 Por otra parte, para tranquilidad de los que teman que las compras de juegos para jugarlos en Steam Play cuenten como ventas en Windows, **Valve ha aclarado** directamente a [gamingonlinux.com](https://www.gamingonlinux.com/articles/valve-officially-confirm-a-new-version-of-steam-play-which-includes-a-modified-version-of-wine.12400) que **el algoritmo de Steam está dispuesto de forma que si los juegos que compramos los jugamos con Steam Play contarán como ventas Linux**. ¡Bravo por Valve!




---


**Noticia original:**


Hoy es uno de esos días con los que probablemente la mayoría de nosotros llevamos soñando mucho tiempo. [Valve software](http://www.valvesoftware.com/), la compañía que está detras de Steam acaba de anunciar la nueva versión de "**Steam Play**", y a pesar de que corrían [rumores muy fundados](index.php/homepage/noticias/item/953-valve-podria-estar-desarrollando-un-sistema-para-ejecutar-juegos-para-otras-plataformas-desde-steam) de lo que estaba haciendo nos ha sorprendido a todos una vez más y para bien. La noticia básicamente es que se ha incluido en el cliente de Steam a **Proton**, un fork de Wine que tiene el plugin DXVK incluido, además de otras herramientas, **facilitando la instalación y ejecución de software para Windows en GNU-Linux/SteamOS**.


No os voy a mentir, como la mayoría de vosotros en JugandoEnLinux.com aun estamos en shock y procesar y sintetizar toda la información nueva que tenemos es bastante difícil en este momento, por lo que creemos que lo mejor es que leais la [extensa información](https://steamcommunity.com/games/221410/announcements/detail/1696055855739350561) que ha facilitado [Pierre Loup Griffais](https://twitter.com/plagman2?lang=es) de Valve sobre esta nuevisima característica de su cliente para Linux (copio y pego):



> 
> *Presentando la nueva versión de Steam Play*
> --------------------------------------------
> 
> 
> *21 agosto - [Pierre-Loup](https://steamcommunity.com/profiles/76561198088081180)*
> 




> 
> *En 2010, anunciamos Steam Play, una funcionalidad que permite a los usuarios de Steam acceder a los juegos de Steam para Windows, Mac y Linux con una sola compra. Desde entonces, más de 3000 juegos de los que se han añadido a Steam han incluido soporte para Linux, y más títulos se siguen añadiendo cada día. Hemos continuado buscando formas para hacer que los usuarios de Linux puedan acceder a más títulos.*  
>   
> *Por eso, hace dos años empezamos a trabajar en mejorar la calidad y el rendimiento de soluciones para la compatibilidad con Windows de los juegos de Steam. Mucho de nuestro trabajo se ha centrado en el soporte de [Wine](https://steamcommunity.com/linkfilter/?url=https://www.winehq.org/)[www.winehq.org] y en otros proyectos de compatibilidad existentes. También hemos estado integrando esas herramientas en el cliente de Steam para proporcionar la misma experiencia de simplemente conectar y usar que ofrecen los juegos normales de Linux.*  
>   
> *Nuestro objetivo en esta tarea es que los usuarios de Linux en Steam dispongan de acceso fácil al amplio catálogo existente. Creemos que eso les permitirá a los futuros desarrolladores usar su trabajo en otras plataformas para adaptarlo a Linux. Esto deja abierta la opción de enfocarse más en las áreas que generan una diferencia significativa en la experiencia de todos los usuarios, como el soporte de [Vulkan](https://steamcommunity.com/linkfilter/?url=https://www.khronos.org/vulkan/)[www.khronos.org].*  
>   
> *Como resultado de ese trabajo, ¡hoy lanzamos la beta de una nueva versión mejorada de Steam Play para todos los usuarios de Linux! Se incluye una distribución modificada de Wine, que se llama Proton, para proporcionare compatibilidad con los títulos de juegos para Windows. He aquí algunas de las mejoras que se incluyen en el lanzamiento:*  
> 
> * *Los juegos para Windows sin versión disponible para Linux ahora se pueden instalar y ejecutar directamente desde el cliente de Steam para Linux, que tiene ahora soporte nativo para Steamworks y OpenVR.*
> * *Las implementaciones de DirectX 11 y 12 ahora se basan en Vulkan, lo cual genera una mejora en la compatibilidad de los juegos y una reducción en el impacto del rendimiento.*
> * *Se ha mejorado el soporte para pantalla completa: los juegos de pantalla completa se ajustarán de tamaño de forma imperceptible hasta encajar en el monitor deseado, sin interferir con la resolución nativa del monitor, ni requerir el uso de un escritorio virtual.*
> * *Mejorado el soporte para mandos de juegos: los juegos reconocerán automáticamente todos los mandos compatibles con Steam. Espera encontrar compatibilidad con mandos que no requieren configuración adicional, más allá de la que proporcione la versión original del juego.*
> * *Se ha mejorado mucho el rendimiento para juegos con multiproceso, comparados con la versión normal de Wine.*
> 
> 
>   
> *![](https://steamcdn-a.akamaihd.net/steamcommunity/public/images/clans/4178173/b7d725ea1a57c7f4b7992efaea0c10f3450a0ec1.png)*  
>   
> *Esto está asociado con un esfuerzo reiterado de testeo que incluye el catálogo completo de Steam, un esfuerzo que busca identificar lo que funciona bien en este entorno de compatibilidad y lo que no funciona bien para poder resolver los problemas correspondientes. La lista inicial de juegos compatibles que estamos habilitando con este lanzamiento beta inicial incluye:*  
> 
> * *[Beat Saber](https://store.steampowered.com/app/620980)*
> * *[Bejeweled 2 Deluxe](https://store.steampowered.com/app/3300)*
> * *[Doki Doki Literature Club!](https://store.steampowered.com/app/698780)*
> * *[DOOM](https://store.steampowered.com/app/379720)*
> * *[DOOM II: Hell on Earth](https://store.steampowered.com/app/2300)*
> * *[DOOM VFR](https://store.steampowered.com/app/650000)*
> * *[Fallout Shelter](https://store.steampowered.com/app/588430)*
> * *[FATE](https://store.steampowered.com/app/246840)*
> * *[FINAL FANTASY VI](https://store.steampowered.com/app/382900)*
> * *[Geometry Dash](https://store.steampowered.com/app/322170)*
> * *[Google Earth VR](https://store.steampowered.com/app/348250)*
> * *[Into The Breach](https://store.steampowered.com/app/590380)*
> * *[Magic: The Gathering - Duels of the Planeswalkers 2012](https://store.steampowered.com/app/49470)*
> * *[Magic: The Gathering - Duels of the Planeswalkers 2013](https://store.steampowered.com/app/97330)*
> * *[Mount & Blade](https://store.steampowered.com/app/22100)*
> * *[Mount & Blade: With Fire & Sword](https://store.steampowered.com/app/48720)*
> * *[NieR: Automata](https://store.steampowered.com/app/524220)*
> * *[PAYDAY: The Heist](https://store.steampowered.com/app/24240)*
> * *[QUAKE](https://store.steampowered.com/app/2310)*
> * *[S.T.A.L.K.E.R.: Shadow of Chernobyl](https://store.steampowered.com/app/4500)*
> * *[Star Wars: Battlefront 2](https://store.steampowered.com/app/6060)*
> * *[Tekken 7](https://store.steampowered.com/app/389730)*
> * *[The Last Remnant](https://store.steampowered.com/app/23310)*
> * *[Tropico 4](https://store.steampowered.com/app/57690)*
> * *[Ultimate Doom](https://store.steampowered.com/app/2280)*
> * *[Warhammer® 40,000: Dawn of War® - Dark Crusade](https://store.steampowered.com/app/4580)*
> * *[Warhammer® 40,000: Dawn of War® - Soulstorm](https://store.steampowered.com/app/9450)*
> 
> 
> *Habilitaremos más títulos en un futuro cercano a medida que avanzan los esfuerzos de desarrollo y testeo; mientras tanto, los usuarios entusiastas podrán intentar jugar juegos no incluidos en la lista si usan un interruptor de anulación en el cliente de Steam. En adelante, los usuarios podrán votar para que sus juegos favoritos se consideren para su uso en Steam Play utilizando la [lista de la plataforma](https://steamcommunity.com/games/221410/announcements/detail/1475356649450732547).*  
>   
> *Los juegos listados en Steam Play no se ofrecerán para su compra, ni se marcarán como compatibles en Linux en la tienda durante el periodo beta inicial.*  
>   
> *Proton, la herramienta que usa Steam Play para proporcionar compatibilidad con Windows, incluye una versión personalizada de Wine, así como las bibliotecas adicionales desarrolladas junto con Wine. Es una herramienta de código abierto y está disponible [ahora mismo en GitHub](https://steamcommunity.com/linkfilter/?url=https://github.com/ValveSoftware/Proton/)[github.com]!*  
>   
> *Si estás familiarizado con la creación de proyectos en código abierto, puedes incluso generar tus propias compilaciones locales de Proton; el cliente de Steam proporciona soporte para usarlas para ejecutar juegos en lugar de la versión incluida. ¡Únete a la discusión de detección de problemas y comparte tus parches y el resultado de tu testeo con el resto de la comunidad!*  
>   
> *![](https://steamcdn-a.akamaihd.net/steamcommunity/public/images/clans/4178173/e8f273314d536714c2d33edada3229f5967a2266.png)*  
>   
> ***P: ¿Qué debo hacer para comenzar?***  
>   
> *No mucho, esto es lo que debes tener en mente:*  
> 
> * *Participa en la [Beta para el cliente de Steam](https://support.steampowered.com/kb_article.php?ref=7021-EIAH-8669) de Linux.*
> * *Cerciórate de que los controladores estén actualizados; si no estás usando SteamOS, sigue nuestras, [instrucciones rápidas para otras distribuciones](https://steamcommunity.com/linkfilter/?url=https://github.com/ValveSoftware/Proton/blob/proton_3.7/PREREQS.md)[github.com].*
> * *¡Terminaste! Instala y juega tus juegos.*
> 
> 
> ***P: ¿Qué es Proton exactamente? ¿En qué se diferencia de la versión normal de Wine? ¿Quién trabajo en Proton?***  
>   
> *Proton es una herramienta que se puede distribuir basada en una versión modificada de Wine. Las mejoras que se incluyen con respecto a Wine han sido diseñados y pagados por Valve, en un esfuerzo de desarrollo compartido con CodeWeavers. He aquí unos ejemplos de aquello en lo que hemos estado trabajando juntos desde 2016:*  
> 
> * *[vkd3d](https://steamcommunity.com/linkfilter/?url=https://source.winehq.org/git/vkd3d.git/)[source.winehq.org], la implementación de Direct3D 12 basada en Vulkan*
> * *Los puentes de API nativos para OpenVR y Steamworks*
> * *Múltiples arreglos de funcionalidad y rendimiento de wined3d para Direct3D 9 y Direct3D 11*
> * *La reorganización completa del soporte para mandos y para pantalla completa*
> * *El set de parches "[esync](https://steamcommunity.com/linkfilter/?url=https://github.com/zfigura/wine/blob/esync/README.esync)[github.com]" para mejoras del rendimiento en subprocesos*
> 
> 
> *Las modificaciones en Wine se remiten al proyecto global de Wine si son compatibles con los objetivos y requisitos generales, esto hace que los usuarios de Wine se hayan estado beneficiando de parte de este trabajo desde hace más de un año. El resto está disponible en nuestro repositorio de código fuente para Proton y sus módulos.*  
>   
> *Además, hemos estado apoyando el desarrollo de [DXVK](https://steamcommunity.com/linkfilter/?url=https://github.com/doitsujin/dxvk)[github.com], la implementación de Direct3D 11 basada en Vulkan; estos son los aspectos que incluye este apoyo:*  
> 
> * *Uso del desarrollador DXVK en nuestro grupo de gráficos de código abierto desde febrero de 2018*
> * *Proporcionar soporte directo de nuestro grupo de gráficos de código abierto para resolver problemas de controladores Mesa que afecten a DXVK, y proporcionar también implementaciones de prototipos para las funciones nuevas de Vulkan que mejoren la funcionalidad de DXVK*
> * *Trabajar con nuestros socios en Khronos, NVIDIA, Intel y AMD para coordinar el soporte de controladores y funcionalidad para Vulkan*
> 
> 
> ***P: ¿Cómo va a ser el rendimiento?***  
>   
> *Se espera que haya diferencias en el rendimiento con juegos para los que se precisa traducir la API de gráficos, pero no hay razones fundamentales para que un título de Vulkan se ejecute más lentamente.*  
>   
> ***P: ¿Hay juegos que nunca van a funcionar con Proton?***  
>   
> *Es probable que determinados juegos que usen un DRM complejo o sistemas antitrampas sean difíciles o imposibles de hacer compatibles.*  
>   
> ***P: ¿Cuándo se van a marcar como compatibles con el nuevo Steam Play más títulos de juegos?***  
>   
> *Añadiremos más juegos a medida que avanza el testeo, no hay un ritmo predeterminado. Se añadirán nuevos juegos al sistema sin que sea preciso actualizar el cliente de Steam.*  
>   
> ***P: ¿Puedo probar un juego con Proton incluso si no está marcado como compatible?***  
>   
> *Sí, ve a las opciones de Steam Play en tu cliente de Steam y podrás habilitarlo para todos los juegos.*  
>   
> ***P: ¿Hay planes para soporte en macOS?***  
>   
> *Si bien Wine y Proton funcionan en macOS, en este momento no hay planes para proporcionar soporte a la nueva funcionalidad de Steam Play en macOS.*   
>   
> ***P: Soy un desarrollador, si mi juego ya es compatible con Linux, ¿qué tipo de cambios debo esperar?***  
>   
> *Probablemente ningún cambio, si ya adaptaste tu código o si usas un motor compatible con Linux, sigue haciéndolo, no es necesario hacer nada más.*  
>   
> ***P: Soy un desarrollador, no tenía planes para versiones Linux, ¿cuál es la mejor forma de sacar ventaja del nuevo Steam Play?***  
>   
> *Recomendamos que te enfoques en dar soporte nativo a Vulkan para que puedas ofrecer el mejor rendimiento en todas las plataformas o al menos que lo ofrezcas como una opción si es posible. También es buena idea evitar middleware invasivo de DRM de terceras partes, ya que a veces impide que las funciones de compatibilidad funcionen correctamente.*  
>   
> ***P: Soy un desarrollador, mi juego está en la lista de Steam Play; ¿tengo que dar soporte para una plataforma adicional?***  
>   
> *No, si un juego está en la lista después de nuestro testeo, hemos comprobado que la experiencia es idéntica, tal vez con la excepción de un impacto moderado en el rendimiento. Aquellos usuarios que jueguen en Steam Play y noten problemas específicos de Linux, se les debe referir al equipo de Soporte de Steam. Hay que tener en cuenta que los usuarios probablemente ya estaban jugando tus juegos con Wine; algo que ahora será más visible para ti.*  
>   
> *----*  
>   
> *Si tienes alguna pregunta que no hayamos mencionado antes, visita el foro de la [comunidad de Steam](https://steamcommunity.com/app/221410/discussions/8/) y empieza una conversación.*  
>  *- Pierre-Loup*
>  
> 


Como podéis ver la nota no tiene desperdicio, y en ella no han escatimado en incluir información a raudales, por lo que después de leerla pocas preguntas quedan. Lo que si es cierto, es que esta nueva situación va a traer obviamente una serie de implicaciones en cuanto a lo que se refiere al mundo gamer en nuestra plataforma pero esto lo trataremos en una editorial que estamos preparando, y donde daremos las valoraciones que creemos que merece este nuevo movimiento de Valve.
 
De momento, si quieres valorar o comentar sobre este tema, puedes hacerlo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).
 
