---
author: Laegnur
category: "Exploraci\xF3n"
date: 2021-10-17 14:58:23
excerpt: "<p>Os contamos todas las novedades acontecidas en <a href=\"https://twitter.com/Minecraft\"\
  \ target=\"_blank\" rel=\"noopener\">@Minecraft</a></p>\r\n"
image: https://i.imgur.com/BulaZK6.jpg
joomla_id: 1376
joomla_url: minecraft-live-2021
layout: post
tags:
- actualizacion
- minecraft
title: Minecraft Live 2021
---
Os contamos todas las novedades acontecidas en [@Minecraft](https://twitter.com/Minecraft)


¡Salve jugadores! Soy Laegnur, una vez mas os traigo las novedades sobre Minecraft, que son muchas.


Mientras aun estamos esperando a que salga la segunda parte de la actualización Caves & Clifs, Minecraft 1.18, este fin de semana pasado, concretamente el Sábado 16 de Octubre, tubo lugar el evento **Minecraft Live**, donde entre otras cosas que comentaremos a continuación, se nos presento la siguiente actualización, **Minecraft 1.19, The Wild Update**.


Como podemos ver en la imagen de presentación, serán varias las novedades, que paso a detallaros:


* Se realizaran cambios a los biomas **Bosques de abedules**, para que pasear por ellos sea una experiencia mas agradable
* Se añade un nuevo tipo de árbol con su correspondiente bloque de madera y los objetos creados a partir de la misma; el **Mangle**.
* Y con los mangles, se añade un nuevo bioma, el **Manglar**.
* Y con este nuevo bioma, se añade un nuevo bloque, el **Lodo**, que se creara a partir del bloque de Tierra, añadiéndole agua, y que si usamos las propiedades del bloque de Espeleotema introducido en  [Minecraft 1.17](index.php?option=com_content&view=article&id=1313&catid=15), goteara el agua del bloque de Lodo secándolo en un bloque de Arcilla, con lo que obtendremos una forma de convertir toda nuestra tierra en bloques mas útiles.
* Los bloques de Lodo ademas se podrán convertir en **Ladrillos de Lodo**, lo que nos da nueva opción a la hora de construir estructuras.
* Se añaden dos nuevas criaturas, las **Ranas**, largamente pedidas por los fans del juego, y las **Luciérnagas**.
* Y hablando de largamente pedido, por fin se añaden los **Barcos con cofres**! Lo que nos permitirá explorar el mundo y recolectar una mayor cantidad de recursos.


Pero no todo van a ser actualizaciones en la superficie. Bajo el suelo, en las profundidades, también veremos alguna actualización nueva, a mayores de las que aun están por venir en la próxima actualización.


Y es que si en la 1.18 se nos van a introducir dos nuevos biomas, las Cuevas kársticas y las Cuevas frondosas, en la 1.19 se nos añadirá un nuevo bioma en las profundidades, las **Ciudades profundas oscuras (Deep dark cities)**, que como el nombre indica, serán ruinas de piedra en las profundidades del mundo, con un ambiente oscuro y misterioso.


Y dentro de estas ciudades podremos encontrar ademas de los bloques de piedra, una nueva familia de bloques, que transmiten señales Red Stone de unos a otros.


* **Sensor de Sculk**: Es un bloque, que si bien ya se introdujo en la 1.17, no habíamos mencionado en el articulo anterior, porque no se introduciría hasta la 1.18 y su única funcionalidad parecía ser el crear sensores de proximidad de Red Stone y ninguna utilidad natural en el mundo. Pero ahora, se va a posponer hasta esta nueva actualización, y servirá como detector de proximidad del sistema de alarma natural de las ciudades oscuras ya que detectara ruidos a su al rededor y transmitirá una señal de Red Stone ante ellos.
* **Catalizador Sculk**: Este sera el bloque central de esta nueva familia de bloques, y a partir del cual "nacerán" los demás. Cada vez que se mate un monstruo dentro del área de un catalizador, este convertirá la experiencia obtenida del monstruo en nuevos bloques de sculk.
* **Flujo de Sculk**: El bloque de suelo por donde se transmitirá la señal de Red Stone de un bloque sculk a otro.
* **Chillón Sculk**: Si el sensor de Sculk es el detector de movimiento, el chillón es la alarma en si. Este bloque emitirá un grito de alerta en cuanto reciba una señal de Red Stone. Ademas, provocara el estado Oscuridad en el jugador, lo que nos dejara completamente a oscuras en un entorno ya de por si tenebroso.
* **Guardián**: Un nuevo y terrible monstruo acecha en las ciudades oscuras, escondido bajo el suelo de flujo sculk, saldrá a cazar al jugador si este hace saltar repetidas veces la alarma sculk. Pero ademas, sera capaz de detectar al jugador mediante el olfato. Una experiencia aterradora.


Y aunque todas estas novedades os parezcan mucho, esto solo fue una pequeña parte del evento Minecraft Live.


El acto central del evento, fue que la comunidad votase por la nueva criatura útil para los jugadores, que va a ser añadida.


![](https://i.imgur.com/gw1HOCU.webp)


Como en anteriores ocasiones, ademas de los animales que van a ser añadidos, en esta actualización las ranas y las luciérnagas, Mojang añade una criatura que aportara algo a los jugadores, y por esto, deja en ellos la decisión de cual sera.


Se nos presentaron tres posibles criaturas, y mediante votaciones en la [cuenta oficial de Minecraft en Twitter](https://twitter.com/Minecraft "Cuenta oficial de Minecraft en Twitter"), la comunidad elige la que sera añadida.


En esta ocasión se nos presentaban estas tres criaturas


* **Glare**: Una criatura parecida a un pequeño arbusto, que habitara las cavernas y que se enfadara y emitirá pequeños gruñidos cuando detecte zonas mal iluminadas, alertando al jugador de posible aparición de monstruos.
* **Golem de Cobre**: Una pequeña criatura fabricada a partir de cobre, que de forma aleatoria pulsara botones, útil a la hora de automatizar circuitos de Red Stone.
* **Allay**: Y por ultimo, la criatura ganadora, y que sera introducida en la actualización 1.19. El Allay, una pequeña criatura voladora, similar a los ya existentes Vex, solo que esta no atacara al jugador, sino que si se le entrega un bloque de cualquier material, el soló recogerá todos los bloques sueltos que encuentre de ese mismo material, y los llevara al bloque musical mas cercano, lo que ahorrara al jugador tiempo a la hora de minar grandes cantidades de bloques, permitiendo liberar material del inventario y dejando que el Allay se ocupe de recogerlo.


Ademas de todas estas novedades sobre la actualización 1.19, se dejo caer que a partir de la siguiente versión, la 1.18, los sistemas de semillas de los mapas de las ediciones Java y Bedrock se van a unificar, por lo que a partir de la misma semilla se obtendrá el mismo mapa en las dos versiones del juego.


Y aunque aun no se sabe la fecha oficial, se confirma que la próxima actualización, la 1.18, estará en uno o dos meses, con lo que antes de fin de año, deberíamos tenerla aquí.


Si os interesa ver el evento completo, podéis ver la repetición en la pagina oficial de [Minecraft Live](https://www.minecraft.net/en-us/live "Minecraft Live").
