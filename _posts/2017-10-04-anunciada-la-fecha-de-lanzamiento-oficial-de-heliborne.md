---
author: Pato
category: "Acci\xF3n"
date: 2017-10-04 16:30:42
excerpt: "<p>Se trata de un juego de acci\xF3n a\xE9rea con helic\xF3pteros y ya est\xE1\
  \ disponible</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/91650abe72b74891ce17dffa3726f716.webp
joomla_id: 474
joomla_url: anunciada-la-fecha-de-lanzamiento-oficial-de-heliborne
layout: post
tags:
- accion
- indie
- multijugador
title: Anunciada la fecha de lanzamiento oficial de 'Heliborne' (actualizado)
---
Se trata de un juego de acción aérea con helicópteros y ya está disponible

**ACTUALIZACIÓN-**El juego ya ha sido lanzado oficialmente.


Seguimos en época de lanzamientos. Hoy llegan noticias de que el juego de acción aérea con helicópteros 'Heliborne' ¡llegará a nuestro sistema favorito el próximo 12 de Octubre día de lanzamiento oficial!.


El juego está aún en periodo de acceso anticipado para pulir diversos aspectos, con lo que para su salida no deberían haber problemas graves.


'Heliborne' [[web oficial](https://helibornegame.com/)] es un juego de acción aérea "en tercera persona" donde tendrás que ponerte a los mandos de los mejores helicópteros del mundo -desde los clásicos de los 50 hasta los modernos helicópteros de combate armados hasta los dientes. Juega misiones con tus amigos y compite contra otros jugadores de todo el mundo en varios modos multijugador. Formarás parte de una campo de batalla dinámico donde darás soporte a las tropas de tierra y ayudarás a crear líneas de abastecimiento.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/XlSMeCNKhgQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Heliborne no está traducido al español, pero si no es problema para ti y te va la temática, lo tienes disponible ya en acceso anticipado en su página de Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/433530/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Qué te parece Heliborne? ¿Surcarás los cielos a lomos de estas máquinas con aspas?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

