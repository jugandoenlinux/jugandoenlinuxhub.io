---
author: Pato
category: "Acci\xF3n"
date: 2019-04-04 17:33:08
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  >@AspyrMedia confirma las texturas HD para ambos juegos y la restauraci\xF3n del\
  \ \"crossplay\" con los jugadores de Windows</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a0e2fbcdd218693394d187e767c5271e.webp
joomla_id: 1014
joomla_url: la-actualizacion-hd-de-borderlands-para-borderlands-2-y-the-pre-sequel-esta-de-camino-a-linux
layout: post
tags:
- accion
- rol
- aspyr-media
- fps
- cooperativo
title: "La actualizaci\xF3n HD de Gearbox para Borderlands 2 y The Pre-Sequel est\xE1\
  \ de camino a Linux"
---
@AspyrMedia confirma las texturas HD para ambos juegos y la restauración del "crossplay" con los jugadores de Windows

Buenas noticias nos llegan desde [gamingonlinux.com](https://www.gamingonlinux.com/articles/aspyr-media-confirm-the-free-ultra-hd-dlc-for-borderlands-2-and-the-pre-sequel-is-coming-to-linux.13896) donde podemos leer que Aspyr ha anunciado que las texturas HD están en camino a las versiones Linux de **Borderlands 2 y Borderlands The Pre-Sequel**, Característica que [acaba de llegar](https://steamcommunity.com/games/49520/announcements/detail/1777141576197274241) a las versiones Windows de ambos juegos.


En un escueto tweet nos invitan a estar atentos a la página de novedades de Aspyr:



> 
> The Borderlands HD update is coming for Mac and Linux to Borderlands 2 and The Pre-Sequel. Stay tuned to this article for the latest updates: <https://t.co/9yBwZuSEas>
> 
> 
> — Aspyr Media (@AspyrMedia) [4 de abril de 2019](https://twitter.com/AspyrMedia/status/1113839005609668608?ref_src=twsrc%5Etfw)


  





Las texturas HD mejoran y adaptan los juegos a las resoluciones mas altas que se manejan hoy día haciendo que estos luzcan mucho mejor visualmente.


Por otra parte, en la [página de actualizaciones](https://support.aspyr.com/hc/en-us/articles/360004860091-Borderlands-2-Mac-Known-Issues) podemos leer que **las versiones de Linux y Mac han perdido la posibilidad de jugar junto a otros jugadores con sistemas Windows**, pudiendo eso sí seguir jugando entre ambos sistemas **hasta que se resuelva este problema que esperan tener solventado en una próxima fecha aún por determinar**.


Seguiremos atentos a las novedades por parte de Aspyr para informaros sobre **Borderlands 2 y Borderlands The Pre-Sequel** y esperemos poder disfrutar tanto de las texturas HD como el crossplay entre sistemas muy pronto.


 

