---
author: Pato
category: "Acci\xF3n"
date: 2018-05-30 18:01:02
excerpt: "<address>Running With Scissors&nbsp;@RWSbleeter por fin nos trae el remake\
  \ de su cl\xE1sico de acci\xF3n</address>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1b15decb97a7e442d17245363ff90a02.webp
joomla_id: 750
joomla_url: postal-redux-se-actualiza-para-corregir-errores-y-anade-soporte-para-linux-steamos
layout: post
tags:
- accion
- indie
- vista-isometrica
- violento
title: "'Postal Redux' se actualiza para corregir errores y a\xF1ade soporte para\
  \ Linux/SteamOS"
---
Running With Scissors @RWSbleeter por fin nos trae el remake de su clásico de acciónBuenas noticias para los amantes de la acción old school. [Hace ya un año](index.php/homepage/generos/accion/item/459-postal-redux-llegara-a-linux-steamos-proximamente) que supimos que 'Postal redux' estaba en camino para nuestro sistema favorito y ayer mismo recibíamos la notícia vía foros de Steam de que el juego de Running With Scissors acaba de recibir una nueva actualización para corregir errores y de paso recibe soporte para Linux/SteamOS.


"*Postal Redux es un remake en alta definición del primer tipo infame de Postal en el mundo. Prepárate para experimentar su viaje psicológico con gráficos de alta resolución, música, sonido y diálogos remasterizados, y jugabilidad modernizada. Pistoleros locos por tu sangre te esperan en cada esquina. La única opción está clara: acaba con ellos antes de que ellos acaben contigo. Lucha con un basto arsenal al tiempo que te abres camino a través de una ciudad golpeada por la violencia."*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/GQdSpV6atyI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 **Características principales:**


* Cooperativo online hasta 4 jugadores
* Online deathmatch con hasta 8 jugadores.
* Nuevo modo Rampage.
* 17 niveles de campaña clásica
* Incluye los añadidos del juego original
* 10 armas de destrucción.
* Multijugador Online crossplatform


Además de esto, el estudio ha aprovechado la actualización para lanzar la banda sonora del juego como DLC gratuito, así como resolver algunos problemas de los que adolecía el juego y que ahora deberían estar resueltos.


Si te gusta la acción cooperativa o versus en vista isométrica, tienes este 'Postal Redux', eso sí, no está traducido al español. Puedes encontrarlo en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/401680/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

