---
author: Son Link
category: "Acci\xF3n"
date: 2021-10-19 15:45:11
excerpt: "<p>@ValveSoftware aplica un nuevo parche a su obra maestra.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HalfLife/portada.webp
joomla_id: 1379
joomla_url: half-life-2-se-actualiza-a-vulkan-dxkv
layout: post
title: Half-Life 2 se actualiza a Vulkan (DXVK)
---
@ValveSoftware aplica un nuevo parche a su obra maestra.


Han pasado 17 años desde que **Half-Life 2** reciba una actualización (sin contar el port a nuestro sistema), pero **Valve** acaba de anunciar el lanzamiento de una nueva, tanto para este juego como los dos Episodios. Dicho parche añade soporte para **Vulkan** a través de **DXVK Native**, un port de la herramienta para Wine que traduce las instrucciones **OpenGL a Vulkan**, pero que se ejecuta de manera nativa, de cara a la salida de las **Steam Deck.** Aparte se han incluido otros cambios, como es la corrección de algunos fallos en algunas escenas, mejoras en la interfaz de usuario debido al escalado para las mencionadas maquinas, soporte para monitores ultra-anchos y el angulo de visión ahora puede configurarse hasta un máximo de 110º en lugar de 90º.


Todo esta esta en fase beta y podemos probarlo siguiendo estos sencillos pasos:


Desde la **Biblioteca** de Steam pulsamos sobre el juego con el botón derecho de nuestro ratón → **Propiedades** → **Beta** y seleccionamos **beta – Beta Updates**. Tras esto se actualizara el juego.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HalfLife/activar_beta.webp)


Sin cerrar la ventana nos vamos a la pestaña **General** y en **Parámetros de lanzamiento** escribimos **-vulkan**.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HalfLife/activar_vulkan.webp)


Tened en cuanta que es una beta y puede fallar (en mi caso en la primera beta la cámara no giraba y tampoco había sonido al cargar una partida, incluso sin activar Vulkan)


Es de suponer que estos cambios lleguen a otros juegos que usen el motor **Source**, como Counter-Strike: Global Offensive, Portal, Portal 2 y Team Fortress 2 una vez que salga la versión estable de esta actualización.


##### Actualización 25/10/2021


Tras actualizarse la beta decidí probar el rendimiento de mi ordenador, el cual tiene la siguiente configuración:


* CPU: AMD Ryzen 2600 (Zen+)
* GPU: AMD RX 590 8GB
* Distro: Arch Linux
* Escritorio: LXQt
* Versión del Kernel: 5.14.14
* Versión de MESA: 21.2.4
* Driver usado: MESA y vulkan-radeon (drivers libres)


La carga de la GPU en Vulkan es menor, no sobrepasando la carga del 30% la mayoría de las veces, mientras que en OpenGL llega a superar los 50%. La carga de la CPU es algo menor, pero la diferencia, al menos hasta donde he probado, no es muy grande. Por otro lado he apreciado que en varias ocasiones la VRAM baja de los 1000MHz a 400Mhz, y por ultimo que el Vulkan los gráficos están mas suavizados.  
  
Os dejo con una captura de mi configuración y varias capturas:


Configuración del juego:


![Configuracion](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HalfLife/configuracion.webp)


Primera captura OpenGL


![estable opengl 1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HalfLife/estable_opengl_1.webp)


Primera captura con Vulkan


![beta vulkan 1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HalfLife/beta_vulkan_1.webp)


Segunda captura OpenGL


![estable opengl 2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HalfLife/estable_opengl_2.webp)


Segunda captura con Vulkan


![beta vulkan 2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HalfLife/beta_vulkan_2.webp)

