---
author: Pato
category: Noticia
date: 2020-02-20 19:04:20
excerpt: "<p>Y repasamos tambi\xE9n las \xFAltimos t\xEDtulos que llegar\xE1n a la\
  \ plataforma</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/Stadia_logo.webp
joomla_id: 1178
joomla_url: que-pasa-si-nos-damos-de-baja-de-stadia-pro-google-nos-resuelve-las-dudas
layout: post
title: "\xBFQu\xE9 pasa si nos damos de baja de Stadia Pro? Google nos resuelve las\
  \ dudas"
---
Y repasamos también las últimos títulos que llegarán a la plataforma


Como pasa el tiempo. Hace ya tres meses que **Stadia** llegó a los primeros aventureros que se adentraron en la era del juego en streaming mediante la adquisición de Stadia Pro, y como es natural, ahora tras el periodo en que se acaba la oferta de los tres meses gratis para un amigo, la pregunta natural es: ¿Y ahora que pasa si no quiero seguir pagando el servicio?


Pues en un [comunicado a través de su blog](https://community.stadia.com/t5/Stadia-Community-Blog/What-happens-after-my-3-free-months-of-Stadia-Pro-are-up/ba-p/15574), el equipo de Stadia nos resuelve las dudas, que como es natural podamos tener. A continuación os traemos la traducción:


P:**¿Qué sucede con mi cuenta de Stadia si cancelo mi suscripción a Stadia Pro o cuando caducan mis 3 meses de Stadia Pro?**


R: Si decides cancelar tu suscripción a Stadia Pro, tu cuenta se revertirá a la versión gratuita de Stadia cuando llegue a la fecha de renovación de tu suscripción. Sin embargo, tu suscripción a Stadia Pro se renueva automáticamente, por lo que **tu suscripción no caducará a menos que la canceles activamente**.


P: **¿Qué pasará con los juegos gratuitos de Stadia Pro que haya reclamado (por ejemplo, Destiny 2: The Collection)?**


R: Perderás el acceso a sus juegos Stadia Pro reclamados, pero **conservarás el acceso a los juegos que hayas comprado** en la tienda Stadia.


P: **¿Perderé el acceso a la tienda Stadia si ya no soy un suscriptor de Stadia Pro?**


R: Seguirás teniendo la opción de comprar juegos en la tienda Stadia y jugarlos.


P: **¿Qué pasará con el progreso que he logrado en los juegos Stadia Pro si cancelo mi suscripción?**


R: Si decides volver a suscribirte a Stadia Pro, recuperarás el acceso a tus juegos previamente reclamados, así como también retendrás tu progreso para esos juegos. Además, si decides comprar uno de tus juegos Stadia Pro previamente reclamados a la carta en la tienda Stadia, **recuperarás el acceso a todos tus datos y progreso guardados.** ¡Tu progreso guardado está a salvo!


P: **¿Perderé algo más si cancelo mi suscripción a Stadia Pro?**


R: Perderás la capacidad de jugar juegos en 4K con HDR y también sonido envolvente 5.1.


P: **¿Qué pasará con mi amigo que no compró una Founders / Premiere Edition ni recibió un Buddy Pass? ¿Cuándo podrá jugar en Stadia?**


R: **En los próximos meses, cualquier persona en nuestros 14 países de lanzamiento** que no haya comprado una Edición Premiere o recibido un pase de amigo **podrá acceder a Stadia de forma gratuita**.


Esperamos que las respuestas sirvan para aclararos un poco las cosas respecto a Stadia, al igual que ya estamos esperando la fecha en la que podremos acceder al servicio de forma gratuita. 


Recordamos que cuando esté disponible el servicio de forma gratuita, tendremos la oportunidad de comprar los juegos [en la tienda Stadia](https://store.google.com/es/product/stadia_games) y jugarlos **sin tener que pagar ninguna suscripción**, eso sí **con la limitación de la resolución a 1080p y sonido stereo**, además de no poder beneficiarnos de los juegos gratis que sí estarán disponibles para los suscriptores de Stadia Pro.


En otro orden de cosas, acabamos de [recibir noticias](https://community.stadia.com/t5/Stadia-Community-Blog/Dig-into-new-adventures-with-SteamWorld-coming-soon-to-Stadia/ba-p/15637) de que **la saga Steamworld** está en camino y llegará a Stadia en próximas fechas, reuniendo los siguientes títulos:


**SteamWorld Dig, SteamWorld Dig 2, SteamWorld Heist y SteamWorld Quest: Hand of Gilgamech**.


En este caso, SteamWorld Dig 2 y SteamWorld Quest: Hand of Gilgamech estarán disponibles para jugar de forma gratuita para los suscriptores de Stadia Pro en cuanto lleguen a la plataforma. Los otros dos títulos estarán disponibles para su compra a través de la tienda para todos.


Si quieres saber más puedes visitar el [comunicado del equipo Stadia](https://community.stadia.com/t5/Stadia-Community-Blog/Dig-into-new-adventures-with-SteamWorld-coming-soon-to-Stadia/ba-p/15637).


¿Qué te parecen las novedades que comienzan a llegar a Stadia? ¿Tienes suscripción o te vas a suscribir a Stadia Pro, o esperarás a que el servicio esté disponible de forma gratuita?


Cuéntamelo en los comentarios, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

