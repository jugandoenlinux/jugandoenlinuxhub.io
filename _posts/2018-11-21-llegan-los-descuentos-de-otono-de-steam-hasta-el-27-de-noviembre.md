---
author: Pato
category: Ofertas
date: 2018-11-21 18:15:43
excerpt: "<p>Adem\xE1s, vota a los nominados a los \"Premios Steam 2018\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c5f6baee01bcaad1c58a783afd54c7cc.webp
joomla_id: 907
joomla_url: llegan-los-descuentos-de-otono-de-steam-hasta-el-27-de-noviembre
layout: post
tags:
- steam
- rebajas
- ofertas
- otono
title: "Llegan los \"descuentos de Oto\xF1o\" de Steam, hasta el 27 de Noviembre"
---
Además, vota a los nominados a los "Premios Steam 2018"

Ya estamos de rebajas!, y eso que aún no ha llegado Diciembre, pero en las fechas que estamos Steam no quiere perder la oportunidad de "rascarnos un poco mas" el bolsillo.


Así que, ¡**desde hoy y hasta el próximo día 27 de Noviembre tendremos suculentas ofertas ¡que no podremos rechazar!**


Tienes todas las ofertas en la [tienda de Steam](https://store.steampowered.com/).


Además, como viene siendo habitual desde hace unos años, se abre la campaña de votaciones a los juegos nominados para los "**Premios Steam 2018**", que se celebrarán a principios del año que viene.


Tienes toda la información en: <https://store.steampowered.com/SteamAwardNominations>


¿Qué te parecen las ofertas? ¿Qué juegos desearías comprar estas rebajas? ¿Votarás a algún juego para los premios Steam 2018?


Cuentamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

