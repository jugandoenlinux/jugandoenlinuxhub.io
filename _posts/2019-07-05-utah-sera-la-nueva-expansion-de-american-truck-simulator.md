---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-07-05 14:50:40
excerpt: <p>@SCSSoftware nos confirma la inminente fecha de salida</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2f7bb693f9e8b1bbf875f0818996978d.webp
joomla_id: 1073
joomla_url: utah-sera-la-nueva-expansion-de-american-truck-simulator
layout: post
tags:
- dlc
- american-truck-simulator
- ats
- scs-software
- expansion
title: "\"Utah\" ser\xE1 la nueva expansi\xF3n de American Truck Simulator (ACTUALIZACI\xD3\
  N)"
---
@SCSSoftware nos confirma la inminente fecha de salida

**ACTUALIZACIÓN 4-11-19:**


El estudio checo anunció ayer mismo que su próximo mapa de expansión para American Truck Simulator, la DLC Utah, saldrá en tan solo unos días, concretamente el día **7 de Noviembre**, por lo que en nada todos aquellos que disfrutamos de conducir camiones por los EEUU pronto podremos incarle el diente a un nuevo territorio. El anuncio nos llegaba, como es habítual a través de Twitter, tal y como podeis ver aquí mismo


 



> 
> A drive through Utah isn't all desert and dust ??   
>   
> Are you ready to deliver to the Beehive State in American Truck Simulator on November 7th? ?   
>   
> Make sure to add Utah to your Steam Wishlist by clicking on the video below ? [pic.twitter.com/tLjk1v1Qxl](https://t.co/tLjk1v1Qxl)
> 
> 
> — SCS Software (@SCSsoftware) [November 3, 2019](https://twitter.com/SCSsoftware/status/1190988891194560514?ref_src=twsrc%5Etfw)


  





Por supuesto, en cuanto tengamos conocimiento de su salida oficial, os informaremos de ello, y si nos es posible realizaremos un Stream especial para celebrarlo el mismo día o en fechas posteriores.


 




---


**NOTICIA ORIGINAL:**  
  
Se venía venir... aunque ciertamente no era muy difícil de prever, y es que hace unas semanas podíamos ver un [críptico video](https://youtu.be/im-4V8xu_Ok), y poco después [las imágenes que publicaban en su blog](https://blog.scssoft.com/2019/06/ats-screenshot-teasing.html) que se lo dejaban bastante claro a los que conocen este estado adyacente a Nevada y Arizona (primeras DLC's gratuitas de ATS). Y es que como podemos ver ultimamente la compañía checa está que no para, y más cuando aún estamos recuperándonos de su reciente lanzamiento, [Washington](index.php/homepage/generos/simulacion/14-simulacion/1183-ha-llegado-washington-la-nueva-expansion-de-american-truck-simulator) (del cual aún os debemos un completo análisis), pues van y anuncian la llegada de este DLC, cuya fecha de salida solo sabemos que **saldrá a finales de 2019**. Como suele ser habitual, a través de twitter recibíamos la noticia:



> 
> We are excited to announce that Utah will be our next map expansion coming to American Truck Simulator later this year ??  
>   
> Watch the video trailer & see more photos at our blog ?<https://t.co/LVSLJ0KFQZ>  
>   
> Add Utah to your Steam Wishlist: <https://t.co/zCeIUcn9m6> ? [pic.twitter.com/Vrh76Q35nI](https://t.co/Vrh76Q35nI)
> 
> 
> — SCS Software (@SCSsoftware) [4 de julio de 2019](https://twitter.com/SCSsoftware/status/1146794787732959235?ref_src=twsrc%5Etfw)


  





![ATS UTAH 00](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Utah/ATS_UTAH_00.webp)


 Teniendo en cuenta lo publicado en su [blog](https://blog.scssoft.com/2019/07/american-truck-simulator-utah.html), El **estado de las Colmenas** (o de la industria) nos ofrecerá unos **paisajes más áridos y desérticos** que las dos ultimas entregas, situadas al noroeste del EEUU, aunque sus montañas también nos permitirán descubrir otro tipo de climas. Por ahora aun no tenemos más que contaros sobre esta expansión, pero ante cualquier actualización de la que tengamos noticia os iremos actualizando para mantener bien al tanto a todos los seguidores de los juegos de esta compañía, que sabemos que son muchos en nuestra comunidad. Os dejamos con el video que han lanzado con motivo de este anuncio:  
  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/_snJZh9mzNc" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Y vosotros, ¿os lo esperabais? ¿Qué os parece que sea Utah la próxima expansión de American Truck Simulator? Cuéntanoslo en los comentarios o deja tus mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

