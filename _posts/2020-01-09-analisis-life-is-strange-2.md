---
author: yodefuensa
category: "An\xE1lisis"
date: 2020-01-09 09:53:41
excerpt: "<p>Nuestro colaborador Yodefuensa nos analiza otro de los grandes ports\
  \ de Feral</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/LifeIsStrange/LiS2_full_season_keyart_landscape_copy.webp
joomla_id: 1147
joomla_url: analisis-life-is-strange-2
layout: post
title: "An\xE1lisis: Life is Strange 2"
---
Nuestro colaborador Yodefuensa nos analiza otro de los grandes ports de Feral


Teníamos muchas ganas de probar el nuevo **Life is Strange 2**, y como mencionamos salió publicado para Linux el pasado 19 de diciembre. Juego creado por **DontNod**, distribuido por **Squarenix** y portado por **Feral Interactive**. Está conformado de 5 episodios y un prólogo. Aunque este último no ha llegado a Linux quizá se deba a ser un prólogo gratuito. Este pequeño episodio no es necesario para entender la trama.


Antes de comenzar, agradecer a Feral Interactive la clave que nos han facilitado para realizar este análisis.


El relato se basa en la relación de Sean y Daniel Díaz, un adolescente de 16 años y un niño de 9 que tras un accidente, tienen que huir de la justicia. Por si esto fuera poco el menor de los hermanos desarrolla poderes especiales. De un día para otro emprenderán un viaje hasta Puerto Lobos, México. Lo que nos hará visitar diferente localidades a lo largo del juego. Durante el camino conoceremos muchos personajes que nos ayudaran o nos harán ver el lado menos amable del ser humano.<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Fshknn1o49M" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


**Life is Strange 2** es posiblemente el juego más político que ha pasado entre mis manos. En el primer episodio se tocan temas como el racismo hacia los latinos, referencias a Trump o negligencias policiales. Como tema central podríamos hablar sobre la familia. Se hace incapié hasta que punto son los lazos de sangre relevantes, o es lo que construimos nosotros en las acciones que tomamos con quienes nos rodean. Cada episodio tiene su pequeña dosis de temas que te harán sentirte incómodo pero son temas que están presentes en nuestra sociedad. Y se les da la importancia y la seriedad que se merecen.


Hablando de sus personajes podemos decir que **todos, absolutamente todos tiene su historia, su pequeño drama**. Los personajes secundarios de este juego están muy bien logrados. Pero lo principal de este viaje es nuestro hermano pequeño. Ahora que solo nos tiene a nosotros, ¿Qué enseñanza le estamos dando? ¿Qué camino debemos marcarle? ¿Vale la pena robar en caso de necesidad? ¿Debería usar sus poderes? ¿Deberíamos poner a la familia por encima del resto sin que nada nos importe más? Al fin y al cabo Daniel es un niño de 9 años y el juego se esfuerza en demostrarnos que estamos ante un niño, un niño que debe ir creciendo a marchas forzadas y nosotros deberemos marcarle una moral a lo largo del juego, una moral que estará sometida a constantes cambios. Dontnod intenta que en situaciones similares veamos las cosas con diferentes criterios, momentos en los que pedíamos a Daniel que actúe de una manera porqué ahora debería comportarse diferente.


En el primer juego la protagonista era quien tenía los poderes. Esto a nivel jugable tenía varías implicaciones y es que cuando aún no entendíamos de que iba la historia, podíamos retroceder en el tiempo, jugar con las respuestas y las diferentes acciones entre los personajes hasta que finalmente fuera la trama la que nos atrapara. En Life is strange 2 al ser nuestro quien desarrolla los poderes de telequinesis, imprime un ritmo más lento y una mayor responsabilidad a la hora de interactuar entre los diferentes personajes. Pese a que me ha parecido un juego más pausado, no ha sido para nada aburrido, Life is strange 2 necesita de una construcción de personajes y esto solo lo logras con mimo y sin prisas.


Life is strange 2 nos cuenta una historía que merece ser contada, donde se ve que han puesto toda la carne en el asador. Sin embargo **podemos decir que se han descuidado otros aspectos.** En septiembre de 2018 llego el primer episodio, y el último el 3 de diciembre de 2019, (podríamos hablar de que no ha habido demasiado retraso en el lanzamiento para Linux). Pero si entre sus diferentes capítulos. Durante todo el juego visitaremos diferentes lugares, lo cual ha supuesto un retraso en el desarrollo. Además se ha restado en jugabilidad, es un mundo más amplio pero con muchos menos puzzles y minijuegos que en anteriores entregas.


Como ya hemos dicho el juego se divide en 5 capítulos de una duración de entre 14-16 horas, con 7 finales diferentes a descubrir y coleccionables que le dan algo de rejugabilidad. Solo he sentido curiosidad por el final del episodio 3 y el final juego por lo que hay múltiples opciones pero **en general no invita demasiado a volver a pasarnos el juego salvo por estos momentos.**


El apartado gráfico está bien logrado, obviamente no estamos ante un referente pero las cosas que se plantea hacer las hace bien. No hablamos de unos gráficos punteros pero si de unos escenarios llenos de vida y de objetos, muchos objetos que consiguen dotar de realismo a los escenarios. La contra partida la pongo en las expresiones faciales. Es fácilmente reconocible ver si los personajes se enfadan, se sorprenden pero el movimiento de boca queda un poco pobre, y no acompaña con el doblaje.


En el apartado sonoro volvemos a encontrarnos con una increíble banda sonora cuyos temas han sido compuestos por Jonathan Morali, además de contar con canciones de grupos como Phoenix, The Streets o Sufjan Stevens entre otros que le aportan muchos toques épicos a la aventura. Los efectos están bien creados, mientras que las voces nos llegan en inglés con un magnífico trabajo, fácilmente de lo mejor del juego **el doblaje que vemos en esta entrega**. Los textos nos llegan en perfecto castellano.


El rendimiento del port es sublime, sabíamos que viniendo por parte de Feral no podría defraudarnos. Los requisitos mínimos estaban un poco altos sobre el papel y así ha sido. El juego ha sido probado en un i7-6700k y una rx 570 y ha funcionado en Alto a resolución 1080p por encima de los 60fps la mayoría del tiempo. Con el ordenador menos potente que he podido probar hablaríamos de una Nvidia MX150. Con la cual hemos podido disfrutar del juego en una resolución 1080p en calidad baja rondado los 30fps, a veces un poco por debajo. Pero no es un juego que exija funcionar a 60fps para una mayor inmersión. Como era de esperar funciona sin problemas en AMD como en Nvidia.


Os adjunto dos capturas con el juego en mínimo y en máximo. Apreciamos que en el caso de modelado de personajes no hay mucha diferencia. Pero refiriéndonos a los paisajes se han desactivado los efectos de la iluminación y se ha recortado en vegetación.


![Alto paisaje](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisLifeisStrange2/Alto_paisaje.webp)


\*subtítulos totalmente irrelevantes para la trama


![bajo paisaje](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisLifeisStrange2/bajo_paisaje.webp)


![HQ](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisLifeisStrange2/HQ.webp)


![LHQ](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisLifeisStrange2/LHQ.webp)


En conclusión life is strange 2 es un digno sucesor, un juego entretenido que a veces se nos puede hacer largo y que cuando llega el momento de que termine no queremos que lo haga. En general es un juego de ritmo desigual pero que deja un buen sabor de boca. En el aspecto jugable es donde más hemos perdido pero no es algo que nos vaya a echar para atrás. Es sin duda un título más que recomendado tanto si te gustan este tipo de aventuras como si no, pues el mensaje que nos quiere hacer llegar merece la pena que sea transmitido.


Si quieres adentrarte en el mundo de Life is Strange 2, puedes conseguir la [temporada completa en Humble Bundle](https://www.humblebundle.com/store/life-is-strange-2-complete-season?partner=jugandoenlinux) (enlace de afiliado), en la [tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/lifeisstrange2/), responsables del port (máximo beneficio para ellos) o en su [página de Steam](https://store.steampowered.com/app/532210/Life_is_Strange_2/).

