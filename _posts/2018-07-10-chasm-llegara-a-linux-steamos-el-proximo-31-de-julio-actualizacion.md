---
author: Pato
category: Aventuras
date: 2018-07-10 10:10:21
excerpt: <p>@ChasmGame ya ha publicado el juego con soporte en Linux de salida</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1d11e5ebeed3c42540f69555bbe34fb8.webp
joomla_id: 800
joomla_url: chasm-llegara-a-linux-steamos-el-proximo-31-de-julio-actualizacion
layout: post
tags:
- accion
- indie
- aventura
- rol
- metroidvania
title: "'Chasm' llegar\xE1 a Linux/SteamOS el pr\xF3ximo 31 de Julio - Actualizaci\xF3\
  n -"
---
@ChasmGame ya ha publicado el juego con soporte en Linux de salida

**(Actualización 31/07/2018):**


Chasm ya ha sido publicado con soporte en Linux desde hoy mismo, día de lanzamiento. Puedes adquirirlo en su [página de Steam](https://store.steampowered.com/app/312200/Chasm/) con un 10% de descuento por su lanzamiento hasta el día 6 de Agosto.




---


¡Grandes noticias para los que estamos esperando un buen juego de corte "metroidvania"! 'Chasm' llega para "saciar" nuestra sed de juego durante este caluroso verano que nos espera. Tras un exitoso Kickstarter y mas de cuatro años de desarrollo, por fin llega este desarrollo de "Bit Kid Inc.". Influenciado claramente en títulos ya clásicos como Castlevania "SOTN" o Axiom Verge, **'Chasm'** [[web oficial](http://www.chasmgame.com/)] nos llevará explorar y luchar contra incontables enemigos.


*Bienvenido a Chasm, un juego de acción y aventuras en el que interpretas a un nuevo recluta que realiza su primera misión para el Reino de Guildean. Emocionado por demostrar tu valía como caballero, rastreas los extraños rumores de que se ha cerrado una mina vital para el Reino. Pero lo que descubres en la ciudad minera es peor de lo que imaginabas: los habitantes del pueblo han desaparecido, secuestrados por criaturas sobrenaturales que emergen de las profundidades.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/9BL162RQXKg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Características:


* Explora seis áreas generadas proceduralmente.
* Disfruta con un desafiante juego retro con auténtico "pixel Art" (resolución nativa 384x216)
* Lucha contra jefes finales masivos y descubre nuevas habilidades para acceder a áreas inaccesibles previamente.
* Personaliza a tu personaje equipándolo con armas, escudos y hechizos.
* Compatibilidad completa con mandos


En cuanto a la versión para nuestro sistema favorito, tal y como recogen en [gamingonlinux.com](https://www.gamingonlinux.com/articles/long-awaited-adventure-platformer-chasm-to-launch-with-same-day-linux-support-on-july-31st.12103), en los comentarios del [anuncio oficial en Steam](https://steamcommunity.com/games/312200/announcements/detail/1678037020104093612) responden a un usuario que la versión de Linux también estará disponible de salida.


'Chasm' no está disponible en español, pero si eso no es problema y el juego es tan bueno como promete, lo tendrás disponible en su [página de Steam](https://store.steampowered.com/app/312200/Chasm/) en próximo 31 de este mes de Julio.


¿Que te parece Chasm? ¿Te gustan los juegos de tipo "metroidvania"?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

