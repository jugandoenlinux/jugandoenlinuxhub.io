---
author: leillo1975
category: "Acci\xF3n"
date: 2019-03-05 12:06:38
excerpt: <p class="ProfileHeaderCard-screenname u-inlineBlock u-dir" dir="ltr"><span
  class="username u-dir" dir="ltr">@MinetestProject</span> viene cargado de mejoras</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ce93b7c97ea8831859bbe17c36b83648.webp
joomla_id: 984
joomla_url: minetest-llega-oficialmente-a-la-version-5-0
layout: post
tags:
- open-source
- minetest
- clon
- minecraft
title: "Minetest llega oficialmente a la versi\xF3n 5.0."
---
@MinetestProject viene cargado de mejoras

Hacía tiempo que había expectación por este nueva "release",ya que al menos en [nuestro grupo de Telegram](https://t.me/jugandoenlinux) estaban deseando que finalmente llegase. Para quien no conozca [Minetest](https://www.minetest.net/), se trata de un **clon del archiconocido Minecraft**, aunque su descripción oficial lo trata como "Un motor de juegos de voxeles de código abierto". Con el podrás jugar online con tus amigos en **multitud de modos  diferentes** (supervivencia, creativo o lucha), utilizar múltiples **mods** o generar mapas aleatorios. Por supuesto también te permitirá crear tus propios juegos y mods.


Con respecto a lo que vamos a encontrar en la versión 5, existe una enorme [lista de cambios](https://dev.minetest.net/Changelog), pero quizás lo más destacado será:


-Agregado repositorio de contenido en línea (juegos, mods, mods, modpacks, paquetes de texturas)  
 -Añadidos mapas de los Cárpatos  
 -Salto automático  
 -Android: Controles reescritos. Añadido un joystick y modificados los botones en pantalla  
 -Los Mods y juegos pueden ser traducidos  
 -Renombrado 'subjuego' a 'juego'.  
 -Modding: Más tipos de dibujo de nodos: nodeboxes desconectados (más formas de crear bloques conectados), plantlike_rooted (para plantas submarinas)  
 -Modding: Caja de colisión de jugador personalizada  
 -Modding: Una gran cantidad de nuevas características del generador de mapas


Puedes descargar Minetest para Linux en su [página de descargas](https://www.minetest.net/downloads/) (también disponible en otras plataformas). Os dejamos con este video donde podeis ver muchas de las novedades que antes os comentamos:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/S8sXbYzWPP4" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Eres usuario de Minetest y te apetece hablar sobre el tema? Dejanos tus impresiones sobre el juego en los comentarios o charla con otros jugadores en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

