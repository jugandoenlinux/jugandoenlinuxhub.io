---
author: leillo1975
category: Plataformas
date: 2019-01-11 15:58:59
excerpt: <p>Su creador es @ainumortis, un miembro de nuestra comunidad</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0f597c772f890f86635f670eec728e88.webp
joomla_id: 947
joomla_url: disponible-la-version-1-0-de-run-run-froggerboy
layout: post
tags:
- comunidad
- froggerboy
- ainumortis
title: "Disponible la versi\xF3n 1.0 de \"Run Run Froggerboy\"."
---
Su creador es @ainumortis, un miembro de nuestra comunidad

Lo mejor de JugandoEnLinux.com sois vosotros, que leeis, comentais y compartis nuestros artículos, nos seguis en las redes sociales, veis nuestros videos ,charlais en nuestros grupos de mensajería... no, no es peloteo, es la verdad, y sería muy injusto y bastante extraño que no apoyáramos los proyectos de sus integrantes. Si hacemos memoria recordareis que **[@SonLink](https://twitter.com/sonlink?lang=es)** ha creado flatpaks de [Warzone 2100](https://flathub.org/apps/details/net.wz2100.wz2100) y [Speed Dreams](index.php/homepage/generos/simulacion/14-simulacion/994-speed-dreams-disponible-en-flatpak); y **@OdinTdh** nos regalaba una fantástica utilidad para configurar nuestros volantes Logitech ([PyLinuxWheel](index.php/homepage/generos/software/12-software/971-pylinuxwheel-una-pequena-gran-utilidad-para-configurar-los-grados-de-nuestros-volantes-logitech)).


En el día de hoy, tras varias betas previas, **[@Ainumortis](https://twitter.com/ainumortis)** (creador también de [MisJuegosEnLinux](https://misjuegosenlinux.blogspot.com/)) nos ha presentado oficialmente su primer juego, que según el mismo es un experimento para aprender y en un futuro desarrollar algo más complejo. "Run Run Froggerboy" es aparentemente un sencillo pero adictivo juego de estilo retro que recuerda mucho la mecánica de Flappy Bird, donde con tan solo una tecla intentaremos superarnos y conseguir los mayores puntos posibles. [Podeis descargarlo desde itch.io](https://ainumortis.itch.io/froggerboy) donde podreis apoyar a su desarrollador con la cantidad que querais o descargarlo libremente. El juego es multiplataforma y puede ser jugado actualmente en Linux, Windows y Android.


![Froggerboy gameplay](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Froggerboy/Froggerboy_gameplay.webp)


 


¿Cuantos puntos habeis hecho? ¿Quereis dejarle algún mensaje o sugerencia a @Ainumortis? Podeis hacerlo en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

