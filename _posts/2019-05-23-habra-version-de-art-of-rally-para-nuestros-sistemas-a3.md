---
author: leillo1975
category: Carreras
date: 2019-05-23 17:38:44
excerpt: "<p>El juego de @funselektor tiene ya fecha de salida</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ArtOfRally/ArtOfRally02.webp
joomla_id: 1047
joomla_url: habra-version-de-art-of-rally-para-nuestros-sistemas-a3
layout: post
tags:
- unity3d
- art-of-rally
- funselektor-labs
title: "Habr\xE1 versi\xF3n de \"Art of Rally\" para nuestros sistemas (ACTUALIZACI\xD3\
  N 3)"
---
El juego de @funselektor tiene ya fecha de salida


**ACTUALIZADO 2-09-20:** 


Acabamos de conocer gracias a un video del [canal de Youtube de @funselektor](https://www.youtube.com/channel/UCrNHgOBz_cGcH4YY9AQsAdA) que el juego finalmente verá la luz en pocos días, concretamente el próximo **23 de Septiembre** (miércoles) . Como veis en justo tres semanas tendremos la oportunidad de disfrutar nativamente de este atractivo juego de Rallys con esa **estética tan especial y sus físicas tan trabajadas**. Podeis ver el video del anuncio a continuación:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="420" src="https://www.youtube.com/embed/BDqW6dz84mU" style="display: block; margin-left: auto; margin-right: auto;" width="750"></iframe></div>


 También hace un par de días pudimos ver en este mismo canal un **video donde podemos ver los coches que estarán disponibles en el "Grupo 2"**, que seguro que a más de uno le recordarán a los fantásticos coches de Rally de los años 80:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="420" src="https://www.youtube.com/embed/8y2mgefhrDA" style="display: block; margin-left: auto; margin-right: auto;" width="750"></iframe></div>


 Como siempre os decimos, permaneceremos atentos a la fecha de salida de este juego para ofreceros la información de primera mano, así como un estreno en nuestros canales de video ([Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux)) el día de su lanzamiento.


 




---


**ACTUALIZADO 12-6-20:** 


Gracias a [GOL](https://www.gamingonlinux.com/2020/06/art-of-rally-looks-terrific-in-the-latest-trailer-and-it-will-be-on-gog-too) nos enteramos que este título acaba de estrenar un **nuevo trailer** donde **podemos ver una buena parte de su contenido y potencial,** y de verdad que luce de maravilla. Podeis verlo justo aquí:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/iFQiukq82xs" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Como veis, este video no deja indiferente a nadie. Además de este trailer, debeis saber que el juego **también será lanzado en GOG**, a parte de [Steam](https://store.steampowered.com/app/550320/art_of_rally/), [GameJolt](https://gamejolt.com/games/artofrally/478982) e [Itch.io](https://funselektor.itch.io/art-of-rally). No menos importante es mencionar que parece que tendremos una **nueva demo** la semana que viene, **durante la Steam Game Festival** (del 16 al 22 de Junio), haciendo una **retransmisión especial el día 17**. Nosotros permaneceremos más que atentos a cualquier nueva información que nos llegue.




---


**ACTUALIZADO 27-3-20:**


Tras unos meses sin noticias por fin tenemos algo para ir abriendo el apetito, y para ello nada mejor que una demo del juego. Hace un rato nos llegó el aviso por parte de su desarrollador de que podeis descargarla y jugarla. La demo incluye lo siguiente:


- Dos icónicos coches de rally, uno del Grupo 2, uno del Grupo B  
- Una etapa mixta de grava y asfalto de Finlandia, con saltos, crestas y esquinas. Es un curso difícil de dominar  
- Cinco condiciones diferentes para correr: mañana, tarde, atardecer, niebla, lluvia, noche...  
- Modo fotográfico, modo de repetición y modo fotográfico en repeticiones, para que puedas revivir y capturar tus mejores momentos


El contenido de esta demo es un trabajo en curso y es susceptible de que haya cambios antes de su lanzamiento, previsto a final de año. .... y ahora a disfrutar.


Podeis encontrar esta demo en:


**Game Jolt:** [https://gamejolt.com/games/artofrally/478982](https://funselektor.us9.list-manage.com/track/click?u=62f70a437c25f11cb57c7a0bf&id=3b7c0cf7ab&e=688643c9ae)  
 **Indie DB:** [https://www.indiedb.com/games/art-of-rally](https://funselektor.us9.list-manage.com/track/click?u=62f70a437c25f11cb57c7a0bf&id=0cd4d99b52&e=688643c9ae)  
 **itch.io:** [https://funselektor.itch.io/art-of-rally](https://funselektor.us9.list-manage.com/track/click?u=62f70a437c25f11cb57c7a0bf&id=dc21948e47&e=688643c9ae)


Os dejamos con un pequeño video que hemos grabado para que veais el juego en acción:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/82SFy3P6ozs" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**ACTUALIZADO 5-11-19:** 


Bastante tiempo llevábamos sin saber nada de este atractivo proyecto, pero tras meses de silencio informativo, finalmente nos han llegado [imágenes](https://imgur.com/gallery/4PUO3ay) y un video ambientados en Finlandia. La verdad es que la estética del juego es fenomenal, y las mecánicas y físicas vistas no han hecho más que hacernos desear que el juego llegue a nuestros ordenadores lo antes posible. Os dejamos con el video para que entendais mejor a lo que nos referimos:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/G8_lTWoFVjo" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Seguiremos atentos a cualquier información que nos llegue de este atractivo proyecto.





---


**NOTICIA ORIGINAL:**


Ojipláticos nos hemos quedado al ver como luce este "[Art of Rally](http://artofrally.com/)". Hace un momento leiamos la noticia en [GamingOnLinux](https://www.gamingonlinux.com/articles/art-of-rally-is-bringing-a-stylized-racing-experience-to-linux-later-this-year.14193) y **trás ver el Trailer del anuncio ya estábamos enamorados del juego** y deseando que salga a la venta, aunque por lo que se ve **tendremos que esperar al final del año** para poder "catarlo". El tweet donde su creador mencionaba que su desarrollo tendría finalmente soporte es el siguiente:  
  




> 
> It is planned for 64-bit systems only, as long as the newer versions of Unity (2018+, 2019+) still support it!
> 
> 
> — Dune (@funselektor) [21 de mayo de 2019](https://twitter.com/funselektor/status/1130870949740367872?ref_src=twsrc%5Etfw)


  





Como podeis ver en el video de abajo el juego, **desarrollado con Unity 3D**, tiene una estética indie muy original y sus mecánicas me recuerdan al "[World Rally Championship](https://youtu.be/Z8VpFlRTrPY)", aquel juego español ([Gaelco](https://es.wikipedia.org/wiki/Gaelco_Multimedia)) que nos hizo dejar la mitad de nuestras raquiticas pagas en los recreativos.  [Funselektor Labs](http://funselektor.com/) ha desarrollado con anterioridad el juego "[Absolute Drift Zen Edition](http://absolutedrift.com/)", un juego donde podemos ver mucho de lo mostrado en este "Art of Rally". Las físicas prometen ser buenas, teniendo en cuenta que Dune Casu, su creador, ha condiucido coches de Rally desde los 90 y tomó clases en la escuela Dirtfish (si, la de [DIRT4](index.php/homepage/analisis/20-analisis/1143-analisis-dirt-4)). Según podemos ver en Steam, en "Art of Rally" nos encontraremos con las siguientes características:



> 
> *"Hacer algo peligroso con estilo es arte..."*
> ----------------------------------------------
> 
> 
>   
> ***art of rally** es una estilizada experiencia de rally de los creadores de [Absolute Drift](https://store.steampowered.com/app/320140/Absolute_Drift/)*  
>   
> *La vista cenital te permite centrarte en la pista viendo las curvas que te esperan más adelante sin necesidad de anotaciones.*  
>   
> 
> 
> 
> * *Progresa a través de la era dorada del rally en el modo carrera*
> * *Más de 30 coches icónicos de rally, desde los 60, 70, 80, Grupo B y Grupo S*
> * *Un manejo del coche completamente revisado proveniente del sistema de físicas de Absolute Drift*
> * *50 pistas de rally localizadas en Finlandia, Cerdeña, Noruega, Japón y Alemania*
> * *Repara los danos del coche entre etapas*
> * *Retos diarios y semanales con tabla de clasificaciones*
> * *Banda sonora original por Tatreal*
> 
> 
> *Nota del desarrollador:*
> -------------------------
> 
> 
> *El sistema de manejo ha sido revisado siguiendo el que ya había en Absolute Drift y es mucho mas predecible y menos exigente. Los ajustes de asistencia de conducción permiten una experiencia de juego accesible para novatos y desafiante para conductores expertos.*
> 
> 
> 


Si os gusta el juego podeis ponerlo en vuetra lista de deseos de Steam para que quede reflejado vuestro interés. Os dejamos con su fantástico Trailer del anuncio:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/zw_KeaVWgro" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Y a ti, ¿qué te parece este "Art of Rally"? Impresiona, ¿verdad? Dejanos tu opinión en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

