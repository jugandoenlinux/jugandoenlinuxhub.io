---
author: Odin
category: Software
date: 2022-11-03 09:36:25
excerpt: "<p>Descubre con nosotros este estupendo motor que os permitir\xE1 crear\
  \ vuestros propios juegos de rol.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Solarus/jel-solarus-engine-logotype.webp
joomla_id: 1504
joomla_url: solarus
layout: post
tags:
- rol
- desarrollo
- software-libre
- arpg
- herramientas-libres
- solarus
title: Solarus, un motor libre para crear ARPG en Linux.
---
Descubre con nosotros este estupendo motor que os permitirá crear vuestros propios juegos de rol.


Además de videojuegos, en [Jugando en Linux](https://jugandoenlinux.com/) nos gusta también informaros de los motores y herramientas que usan los desarrolladores para crear los juegos que tanto nos encantan. Es por esta razón que hoy os damos a conocer [Solarus](https://www.solarus-games.org/).


Solarus es un estupendo motor libre para crear videojuegos del género **Action RPG**. Si nos os suena este género, seguro que si que conocéis la fantástica saga **Zelda de Nintendo** con lo que os podéis hacer una idea del estilo de videojuegos que se pueden programar. Tanto es así que Solarus ha sido utilizado con éxito por los fans de la saga para hacer sus propias versiones inspirados en los Zelda clásicos. No obstante, como bien indican en la documentación, no es simplemente una herramienta para hacer **clones de Zelda**, es un **potente motor 2d** que incluye editores que nos facilitarán programar cualquier videojuego en 2d de estilo ARPG para varios sistemas operativos, incluidos como no, nuestro sistema favorito **Linux**.


![zelda fangame](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Solarus/zelda_fangame.webp)


A continuación os pasamos algunas de las **características** que tiene este magnífico motor para que le echéis un vistazo:


* Programado en **C++** haciendo uso de las bibliotecas **SDL y OpenGL**.
* Soporta **shaders** mediante **GLSL**.
* Usa **Lua** como lenguaje de **scripting**.
* **Solarus Quest Editor**, un IDE gráfico para crear mapas, sprites, diálogos y scripts en Lua y GLSL.
* **Solarus Launcher**, permite gestionar y ejecutar videojuegos hechos en Solarus.
* Multiplataforma: **Linux** (incluido **Raspberry Pi**), **BSD**, macOS, Windows y proximamente Android.
* Publicado mediante software libre (licencia **GPL**).
* Paquetes de **recursos** con **sprites y scripts** **libres** para ayudaros a crear vuestros propios videojuegos.


![captura de pantalla del editor Solarus Quest Editor](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Solarus/jel-screenshot-quest-editor-01.webp)


Si no os atrevéis a meteros en el mundillo del desarrollo de videojuegos, Solarus aun así os tiene mucho que ofrecer. Mediante el **Solarus Launcher** podéis ejecutar de forma nativa en Linux los videojuegos hechos por la comunidad. Yo por mi parte os voy a destacar este pequeño juego gratuito llamado [YarnTown](https://www.solarus-games.org/en/games/yarntown), en el que según su autor **Max Mraz**, es un **homenaje en 2D** del popular **Bloodborne**.


![videojuego yarntown](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Solarus/jel-yarntown.webp)


¿Qué te parece [Solarus](https://www.solarus-games.org/)? Cuéntanos acerca de los videojuegos que vayas a programar con este excelente motor en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

