---
author: Pato
category: Noticia
date: 2020-06-24 10:13:04
excerpt: "<p>Cuantiosas novedades llegar\xE1n a Stadia en pr\xF3ximas fechas</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/ThisWeekonStadia-June23.webp
joomla_id: 1235
joomla_url: novedades-stadia-crayta-monster-boy-steamworld-dig-wave-break-metro
layout: post
tags:
- stadia
- metro-2033-redux
- metro-last-light-redux
- crayta
- monster-boy
- steamworld
- the-elder-scrolls-online
title: 'Novedades Stadia: Crayta, Monster Boy, SteamWorld Dig, Wave Break, Metro... '
---
Cuantiosas novedades llegarán a Stadia en próximas fechas


Vamos con las novedades semanales de Stadia que podremos disfrutar en nuestro sistema favorito en próximas fechas.


Cuatro nuevos juegos llegarán de forma gratuita a los suscriptores de **Stadia Pro** este próximo 1 de Julio. Si eres de los que gustan de "crear" tus propios juegos, llega a Stadia en exclusiva **Crayta**, un juego en el que podrás dar rienda suelta a tu creatividad y luego compartir tus creaciones con otros jugadores de Stadia mediante la nueva característica "*State Share Beta*" con la que podrás generar un enlace mediante el cual los demás podrán entrar de forma instantánea a tu juego y colaborar/jugar contigo.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/ZTo8TiOoABk" width="560"></iframe></div>


 


El siguiente juego no es otro que **Monster Boy and the Cursed Kingdom**, un juego plataformas en entorno 2D con variado contenido:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/mbYE6QfTLv8" width="560"></iframe></div>


 


**West of Loathing** es un juego de rol divertido y refrescante que te hará reir... de los creadores de Kingdom of Loathing:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/1lU0aFZr1R8" width="560"></iframe></div>


 


Por otra parte, ya está disponible en Stadia Wave Break, un juego en el que tendremos que "surfear" sobre diferentes pistas y superficies sobre distintos "aparatos":


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/9FoXJmPqV2M" width="560"></iframe></div>


 


 Por último, [Metro 2033 Redux](https://stadia.com/link/otLakML9gpCt7WQA8)  y [Metro: Last Light Redux](https://stadia.com/link/FQCjUC6WpkvoREtx7) ya están disponibles en Stadia y mañana día 25 de Junio tendremos disponible **Borderlands: Bounty of Blood.**


Además, recordar que **los suscriptores** de Stadia Pro tienen ofertas especiales y descuentos en juegos, por ejemplo esta semana tenemos:


NOVEDAD! Football Manager 2020 por 25 €  
NOVEDAD! Metro Exodus por 16 €  
NOVEDAD! Metro Exodus - Gold Edition por 26 €  
NOVEDAD! Metro Exodus - The Two Colonels por 4.79 €  
NOVEDAD! Metro Exodus - Sam’s Story por 14.39 €  
Assassin’s Creed Odyssey – Stadia Ultimate Edition por 39.60 €  
Assassin’s Creed Odyssey – Stadia Season Pass por 20 €  
Borderlands 3 por 30 €  
Borderlands 3 Deluxe Edition por 40 €  
Borderlands 3 Super Deluxe Edition por 50 €  
Borderlands 3 Season Pass por 39.99 €  
Just Dance 2020 por 20 €  
Red Dead Redemption 2: Ultimate Edition por 59.99 €  
The Crew 2 – Deluxe Edition por 18 €  
The Crew 2 – Season Pass por 20 €  
The Division 2: Warlords of New York Edition por 30 €  
The Division 2: Warlords of New York Expansion por 22.49 €  
Tomb Raider: Definitive Edition por 10 €  
Tom Clancy’s Ghost Recon Breakpoint – Ultimate Edition por 39.60 €  
Tom Clancy’s Ghost Recon Breakpoint: Year 1 Pass por 20 €  
Trials Rising – Digital Gold Edition por 16 €  
Trials Rising – Expansion Pass por 10 €


Recordarte que si eres suscriptor de Stadia Pro aún tienes **The Elder Scrolls Online** gratis para reclamarlo. 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/A15Qu7UqhYk" width="560"></iframe></div>


Puedes ver el anuncio en el [blog oficial de Stadia](https://community.stadia.com/t5/Stadia-Community-Blog/This-Week-on-Stadia-Free-games-headed-to-Pro-and-much-more/ba-p/25345).

