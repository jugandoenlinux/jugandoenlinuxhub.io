---
author: Serjor
category: Software
date: 2017-09-04 21:31:35
excerpt: "<p>Aunque los propios desarrolladores recomiendan esperar a la versi\xF3\
  n 17.2.1</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e2514efc12d4fe082fdc20ab2f336dc3.webp
joomla_id: 452
joomla_url: mesa-17-2-liberado
layout: post
tags:
- mesa
title: Mesa 17.2 liberado
---
Aunque los propios desarrolladores recomiendan esperar a la versión 17.2.1

Vía [gamingonlinux.com](https://www.gamingonlinux.com/articles/mesa-172-officially-released.10283) nos enteramos de que la versión 17.2 de Mesa ha sido liberada. Y es que los amantes del software libre y los videojuegos (u otras aplicaciones de uso intensivo de las GPU) no podrían estar más contentos, ya que poco a poco esta librería va demostrando que en el mundo del software libre, aunque sea con pasos pequeños se recorren caminos enormes.


Además vía [phoronix.com](https://phoronix.com/scan.php?page=news_item&px=OpenGL-4.6-Easy-Extensions) podemos leer que para llegar a la compatibilidad con la versión 4.6 de OpenGL (podéis leer [aquí](index.php/homepage/generos/software/item/546-liberada-la-especificacion-4-6-de-opengl) sobre su anuncio) quedan apenas un par de extensiones, así que probablemente tengamos los drivers al día en breve.


Lejos quedan aquellos días dónde las versiones de Mesa iban muy por detrás de los drivers propietarios, y para los usuarios de AMD actualmente usar los drivers libres es una opción más que válida.


Esperemos que sigan a este nivel y que en cada iteración podamos contaros los avances que han realizado. Si queréis, tenéis la lista de cambios completa [aquí](https://www.mesa3d.org/relnotes/17.2.0.html) pero lo más destacable sería la lista de juegos que se [suman](https://cgit.freedesktop.org/mesa/mesa/tree/src/mesa/drivers/dri/common/drirc?id=mesa-17.2.0) al multihilo, ganando así en rendimiento.


Y tú, ¿cómo ves la evolución de los drivers libres? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

