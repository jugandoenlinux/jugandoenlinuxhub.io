---
author: Pato
category: Rol
date: 2018-04-04 10:02:15
excerpt: <p>El nuevo juego de Richard Garriott no parece haber comenzado su andadura
  con muy buen pie</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8f9f14c4f7dd7ce0bc8e0048b00b1155.webp
joomla_id: 704
joomla_url: shroud-of-the-avatar-llego-a-linux-steamos-pero-no-sin-problemas
layout: post
tags:
- rol
title: "'Shroud of the Avatar: Forsaken Virtues' lleg\xF3 a Linux/SteamOS, pero no\
  \ sin problemas"
---
El nuevo juego de Richard Garriott no parece haber comenzado su andadura con muy buen pie

Un juego del que se nos pasó el lanzamiento fue este '**Shroud of the Avatar: Forsaken Virtues**'. Se trata del último juego que ha lanzado Richard Garriott, (Lord British para los amigos) responsable de sagas de rol emblemáticas como **Ultima**, **Lineage** o **Tabula Rasa**.



> 
> Shroud of the Avatar: Forsaken Virtues es un juego de rol de fantasía multijugador y el sucesor espiritual de los juegos Ultima y Ultima Online de Richard Garriott.  
>   
> Shroud of the Avatar trata de poner a prueba el carácter de uno mismo en un camino lleno de peligros y decisiones difíciles. Las acciones de los jugadores tienen consecuencias significativas dentro del profundo y persistente mundo de Nueva Bretaña, ya sea defendiendo o rompiendo los Principios de Verdad, Amor y Valor.
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/UWAsO_Nqi3U" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 Shroud of the Avatar' trata de recuperar los clásicos y sesudos RPGs de antaño tratando de adaptarlos a un entorno mas moderno, sin embargo a pesar de haber levantado con éxito una campaña en [kickstarter](https://www.kickstarter.com/projects/portalarium/shroud-of-the-avatar-forsaken-virtues-0?ref=discovery&term=shroud%20of%20the%20avatar) no parece que las críticas de la comunidad en Steam le estén siendo favorables. Y puede que no les falte razón, ya que al parecer el juego peca de poca optimización, pobre contenido y no está exento de fallos. En la última actualización, en la parte de bugs conocidos se puede ver un listado de problemas por resolver, incluyendo un par de ellos que afectan a la versión de Linux:


* La versión de Linux utiliza una versión anterior de Unity hasta que problemas de rendimiento conocidos sean resueltos.
* No se puede acceder al mapa del juego (tecla M) en Linux


Puedes ver la lista completa de bugs y los cambios introducidos en el parche en el [anuncio](http://steamcommunity.com/gid/[g:1:7181173]/announcements/detail/1658888566598172624) de este mes.


Por otra parte, los requisitos para mover el juego son:


**Mínimos**


* Procesador y Sistema de 64 bits
* **OS:** Ubuntu 10.10 o superior, aunque la mayoría de distribuciones están soportadas
* **Procesador:** Quad Core 2.4 GHz o superior
* **Memoria:** 8 GB RAM
* **Graficos:**  NVIDIA 960 / AMD 560
* **Almacenamiento:** 22 GB de espacio en disco


**Recommendado:**  

+ Procesador y sistema de 64 bits
+ **OS:** Ubuntu 10.10 o superior aunque la mayoría de distribuciones están soportadas
+ **Procesador:** Quad Core 2.8GHz o superior
+ **Memoria:** 12 GB RAM
+ **Graficos:** NVIDIA GTX 1070 / ATI Radeon HD 5870
+ **Almacenamiento:** 22 GB de espacio en disco
+ **Notas Adicionales:** Rendimiento significativamente mejorado con un disco SSD


Si aún sabiendo que tiene ciertos problemas y que no está traducido al español, quieres darle un vistazo a la última obra de "Lord British" tienes 'Shroud of the Avatar' disponible en su [página oficial](https://www.shroudoftheavatar.com/) donde puedes probar una demo gratis, o en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/326160/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>
 ¿Qué te parece este 'Shroud of the Avatar'? ¿Te gustan los juegos de Richard Garriott?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 


