---
author: Pato
category: Editorial
date: 2017-07-07 15:00:00
excerpt: "<p>Ha aparecido una curiosa petici\xF3n en Change.org sobre este juego que\
  \ debeis conocer.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/271073fa434dfbedecc5cddef10cff3e.webp
joomla_id: 399
joomla_url: the-witcher-3-posiblemente-no-llegara-nunca-a-linux-por-las-criticas-a-the-witcher-2
layout: post
tags:
- accion
- rol
- cdprojekt
title: "The Witcher 3 posiblemente no llegar\xE1 nunca a Linux por las cr\xEDticas\
  \ a The Witcher 2 (ACTUALIZADO)"
---
Ha aparecido una curiosa petición en Change.org sobre este juego que debeis conocer.

**ACTUALIZACIÓN 14-3-18**:Ya ha pasado bastante tiempo desde que se publicó este artículo, pero el tema del brujo sigue candente, tanto que alguien ha tenido la original idea de hacer una **petición en Change.org para que CD Project Red permita que sean los jugadores que usan linux los que sufraguen los costes de portar el juego a nuestro sistema**. Un usuario (**ja-moli**) nos ha dejado un [mensaje en nuestro foro](index.php/foro/juegos-nativos-linux/100-se-acaba-de-abrir-una-peticion-de-change-org-para-pedirle-a-cdprojekt) donde nos explica en que consiste exactamente y nos facilita el enlace a [la petición](https://www.change.org/p/cd-projekt-red-to-cd-projekt-red-please-open-a-kickstarter-to-bring-the-witcher-3-game-to-linux-6e863ea4-52ea-4ac8-8e6d-af99fbaec7e6). Desde JugandoEnLinux no judgamos la idoneidad de esta iniciativa, pues cada uno puede tener una opinión diferente, pero como dijimos antes al menos es una propuesta cuando menos, original y diferente.


Es bueno que recordeis que [como os dijimos hace algún tiempo en un artículo](index.php/homepage/noticias/item/689-cambios-en-steam-para-mejorar-la-informacion-de-los-juegos-deseados-por-los-usuarios) que escribió nuestro colega [Serjor](index.php/homepage/noticias/itemlist/user/196-serjor), una buena forma de hacer saber a los desarrolladores el interés por un juego es **meterlo en nuestra lista de deseados,** pero antes poniendo en las opciones de plataforma usada (al fondo de la página que abre [este enlace](https://store.steampowered.com/account/preferences/)) **solo SteamOS y Linux**, quitando la opción de Windows y de MacOS.


 




---


**NOTICIA ORIGINAL:** El caso de The Witcher 3 es uno de los casos de "promesas incumplidas" que hemos tenido recientemente, comillas incluidas pues CDProjekt nunca confirmó oficialmente que el juego estuviese siendo portado a nuestros sistemas. Fue Valve la que "metió la pata" anunciando su próximo lanzamiento en SteamOS durante la promoción de su sistema y de las Steam Machines, hace ya más de dos años. ¿Qué pasó para que después de aquel anuncio solo hayamos recibido silencio?


![Thw Witcher 3](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Thewitcher/Thw-Witcher-3.webp)


Pongámonos en contexto: CDProjekt es la compañía que está detrás de la tienda online [gog.com](https://www.gog.com/) que tan apreciada es para todo el que gusta de comprar sus juegos sin DRM incluyendo multitud de juegos que están disponibles en Linux. Además de esto, son los responsables de la saga de juegos de rol "The Witcher" aclamada por crítica y público como una de las mejores sagas de rol y acción de los últimos tiempos hasta tal punto que son famosas las conversaciones sobre las prácticas y políticas de distribución de la empresa favorables siempre a los usuarios.


Cuando la compañía polaca decidió traer The Witcher 2 a Linux [[steam](http://store.steampowered.com/app/20920/The_Witcher_2_Assassins_of_Kings_Enhanced_Edition/)] [[gog](https://www.gog.com/game/the_witcher_2)] confiaron el port a [Virtual Programming](https://www.vpltd.com/), por aquel entonces una compañía que aún estaba iniciando su actividad como editora de títulos para Linux y que utilizó su wrapper eON para traer el juego a nuestros sistemas. El caso es que en el momento del lanzamiento, el juego prácticamente era injugable en casi todas las distribuciones, con lo que las malas críticas hacia el juego y sobre todo hacia CDProjekt fueron el detonante de una verdadera marea de comentarios negativos que terminaron por afectar económicamente a la compañía y al juego, puesto que la cotización de aquella cayó y el juego, al menos en un principio no fue rentable. Pocas semanas después, VP lanzó varios parches que solucionaron los problemas y a día de hoy es un juego completamente funcional y con un rendimiento bastante decente, y si te gusta el género es un título muy recomendable. Pero el daño ya estaba hecho.


Virtual Programming por su parte también recibió su parte de críticas sobre todo a su wrapper eON, que durante bastante tiempo estuvo en boca de los más críticos convirtiendo cualquier desarrollo con ese "middleware" como algo menos que "pobre". El resultado fue que VP tardó mas de un año en volver a traer un juego a Linux y aunque nos han traído juegos bastante decentes como [Bioshock Infinite](http://store.steampowered.com/app/8870/), la saga [Saints Row](http://store.steampowered.com/app/9480/), [MicroMachines](http://www.deliver2.com/index.php?route=product/product&product_id=343) o el port de [ArmA3](http://www.deliver2.com/index.php?route=product/product&product_id=331) entre otros, nada se ha vuelto a saber de su relación con CDProjekt.


Tras el anuncio de Valve parecía que volvían a dar una oportunidad a Linux, pero a tenor de un comentario de un [exdesarrollador de Virtual Programming en Reddit](https://www.reddit.com/r/linux_gaming/comments/6kl1sg/any_word_on_the_witcher_3/djq73f7/) que trabajó en aquel título parece que CDProjekt quedó "tocada" y no parece haber olvidado el trato que se le dió por parte de "algunos de aquellos" usuarios de Linux.


No es de recibo que una compañía lance un producto roto o inacabado, pero tampoco podemos obviar que a veces las personas no medimos bien el alcance de nuestras acciones, y hemos perdido la paciencia en un momento en que se ha vuelto habitual el lanzar títulos con fallos y errores que luego se solventan con parches y más parches.


De todos modos, el resto es historia. El hecho de que una compañía con el calado e historial de CDProjekt nos dé de lado es algo que duele, y más sabiendo que vienen nuevos desarrollos como Cyberpunk 2077 que promete ser un nuevo exponente dentro de los videojuegos de rol futurista. Habrá que hacer una buena campaña y ver si la compañía vuelve a mirar con buenos ojos a Linux y su comunidad, y nos traen sus próximos desarrollos. Ya veremos.


Tras toda esta parrafada, os dejo el impresionante vídeo promocional de Cyberpunk 2077. ¿No os gustaría que llegase a Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/P99qJGrPNLs" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Fuentes:


[gamingonlinux.com](https://www.gamingonlinux.com/articles/the-witcher-3-didnt-come-to-linux-likely-as-a-result-of-the-user-backlash-from-the-witcher-2.9940)


[Reddit](https://www.reddit.com/r/linux_gaming/comments/6kl1sg/any_word_on_the_witcher_3/djq73f7/)

