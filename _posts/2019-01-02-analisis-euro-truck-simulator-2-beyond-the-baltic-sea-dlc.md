---
author: leillo1975
category: "An\xE1lisis"
date: 2019-01-02 15:10:41
excerpt: "<p>Profundizamos en la nueva expansi\xF3n de @SCSsoftware</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3750cec8aaaa9dff79700a4982c35862.webp
joomla_id: 945
joomla_url: analisis-euro-truck-simulator-2-beyond-the-baltic-sea-dlc
layout: post
tags:
- ets2
- dlc
- analisis
- scs-software
- euro-truck-simulator-2
- beyond-the-baltic-sea
title: "An\xE1lisis: Euro Truck Simulator 2 - Beyond The Baltic Sea DLC."
---
Profundizamos en la nueva expansión de @SCSsoftware

Fieles a la costumbres, un año más nuestra compañía checa favorita, [SCS Software](index.php/buscar?searchword=SCS&ordering=newest&searchphrase=all&areas[0]=k2),  nos vuelve a servir nuestra ración anual de nuevas carreteras para Euro Truck Simulator 2. Cuando aun no habíamos acabado de disfrutar la [DLC Oregon](index.php/homepage/analisis/20-analisis/998-analisis-american-truck-simulator-dlc-oregon) de su "hermano americano" (nombre con el que nos solemos referir a American Truck Simulator), nos dejan este apetitoso segundo plato en la mesa. Nosotros, en JugandoEnLinux.com, aun a riesgo de empacharnos lo hemos devorado a gusto.


Y es que a pesar de la **decepción por ver un año más que nuestra querída península Ibérica sigue sin su merecida expansión**, el visitar este nuevo conjunto de paises que rodean al mar báltico enseguida nos ha hecho olvidar tan ansiado territorio. Como es habitual, la desarrolladora fué preparando el terreno con la [actualización 1.33](https://blog.scssoft.com/2018/11/euro-truck-simulator-2-update-133.html), que incluía importantes cambios como las mejoras en las suspensiones, las gotas de lluvia más realistas, las traducciones revisadas o los transportes de alta capacidad.


![balt blog map small](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/BeyondTheBalticSea/balt_blog_map_small.webp)


*El área representada en esta expansión es enorme e incluye 5 paises*


Tan pronto como fué lanzada la expansión, el pasado [29 de Noviembre](index.php/homepage/generos/simulacion/14-simulacion/1037-ya-esta-disponible-beyond-the-baltic-sea-la-nueva-dlc-de-euro-truck-simulator-2), nos pusimos manos a la obra y comenzamos a recorrer poco a poco estos singulares territorios. Tal y como podeis ver en los videos de nuestro [canal de Youtube](https://www.youtube.com/c/jugandoenlinuxcom) ([video1](https://youtu.be/3Z41LbBjYto), [video2](https://youtu.be/JIMCznKDOHM), [video3](https://youtu.be/vJDVnmefo-8) y [video4](https://youtu.be/28TGNv6E64A)), nunca le dedicamos a una DLC semejante cobertura, pero es que la ocasión lo merecía por 3 razones fundamentales. La primera es que **el área representada es enorme**, la segunda era **visitar todos los paises y principales ciudades** que conforman "Beyond The Baltic Sea" (BTBS a partir de ahora), y la tercera es... que bueno, nos apetecía mucho.


Para comenzar a jugar esta DLC, lo primero que hicimos fué realizar un viaje desde Bialystok (Polonia) a Tallin (Estonia). Como primer viaje nos serviría para ver como se enlazaba la expansión [Going East](https://www.humblebundle.com/store/euro-truck-simulator-2-going-east?partner=jugandoenlinux) con la que aquí analizamos, y de paso atravesar de sur a norte 3 de los 5 paises que conforman BTBS. He de decir que en un primer momento, como podeis ver en el [primer video](https://youtu.be/3Z41LbBjYto), no encontré nada especialmente reseñable, lo que provocaría que si no tuviesemos el rigor que tenemos en JugandoEnLinux.com y tomasemos unicamente este viaje como referencia para realizar este análisis, las conclusiones serían bien diferentes a las que detallaremos a continuación, además de totalmente injustas. Extrapolando un poco, en nuestra web consideramos que **el software que analizamos debe ser ampliamente jugado para que las críticas sean lo más veraces posibles**, aunque esto signifique que nuestras reviews tarden en salir algo más de lo que nos gustaría.


![BTBS Frontera](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/BeyondTheBalticSea/BTBS_Frontera.webp)


*Cuando crucemos a Rusia tendremos que parar en la aduana*


Trás jugar bastantes horas más en los territorios que abarca BTBS, hay que decir de forma categórica que la impresión que tuve con el primer viaje no es para nada definitoria de esta expansión. Lo que simplemente pasó es que la ruta tomada no era tan atractiva como otras posibles. Debo aclarar que mi primera impresión sobre esta fué tomando como referencia [Italia](index.php/homepage/analisis/20-analisis/688-analisis-euro-truck-simulator-2-italia-dlc), pais por el que siento especial devoción y me trae gratos recuerdos, además de ser una cultura y paisajes muy parecidos a los nuestros.  En posteriores viajes descubrí para mi tranquilidad y alegría que **esta expansión está como mínimo al mismo nivel que las anteriores**, siendo rica en detalles y novedades.


Lo primero que nos llama la atención es la cantidad de paises que la conforman, teniendo el conductor la **posibilidad de visitar Lituania, Letonia, Estonia, el oeste de Rusia y el sur de Finlandia**. Por supuesto cada uno de estos paises con sus particularidades. A lo largo de los **13.000 Km de carreteras** que recorreremos mientras "trabajamos" también encontraremos montones de edificios y lugares singulares.  Es muy definitorio encontrar **llamativos y originales carteles con el nombre de las ciudades** en sus entradas. Encontraremos también **multitud de monumentos conmemorativos de la guerra** por todas partes, algo que nos obligará a parar el camión en más de una ocasión para observarlos.


![BTBS Tallin](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/BeyondTheBalticSea/BTBS_Tallin.webp)


*Encontraremos llamativos carteles, tanto antiguos como modernos, en las entradas de las ciudades*


Especial mención es el **tratamiento que han dado a las regiones de Rusia** que podemos recorrer en la DLC. Se nota que han puesto empeño en diferenciarlas del resto, aunque probablemente esto se deba a que los lugares reales sean así. Lo primero que nos llamará la atención es que **al cruzar su frontera tendremos que detener nuestro camión y entregar nuestros papeles para poder acceder al pais**. En los puestos fronterizos veremos a la policía en una posición elevada sobre unas plataformas para vigilar nuestro camión. Como sabeis Rusia no está en la Unión Europea ni es miembro del tratado de [Schengen](https://es.wikipedia.org/wiki/Acuerdo_de_Schengen)


Inmediatamente después  comenzaremos a ver los **carteles en Cirílico** por todas partes. Resulta curioso ver por ejemplo el [Óblast de Kaliningrado](https://es.wikipedia.org/wiki/%C3%93blast_de_Kaliningrado), un bello, y en su historia disputado territorio, separado por el resto de la Federación Rusa y "encerrado" por  Polonia  al sur, y por Lituania al este y al norte. Destaca, por ejemplo, su catedral y el puente de la reina Luisa sobre el Niemen en Sovetsk, en la frontera con Lituania, y el cual podeis ver en la foto:


![BTBS Puente Reina](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/BeyondTheBalticSea/BTBS_Puente_Reina.webp)


También merece ser resaltado el **fantástico trabajo realizado en otra hermosa ciudad como es [San Petesburgo](https://es.wikipedia.org/wiki/San_Petersburgo)**, también conocida como la Venecia del Báltico. La segunda ciudad más poblada de Rusia, con 5 millones de habitantes, y urbe que llegó a ser capital del imperio durante la época de los Zares, no podía sinó tener un tratamiento especial por parte de SCS, destacando en gran medida su **gigantesca circunvalación** (142Km) con carteles y farolas curvos, y esbeltos puentes que cruzan la bahía del Nevá. La ciudad **combina monumentos y edificios históricos con austeros bloques residenciales de la época soviética**, así como también edificios modernos como estadios, aeropuertos, etc.


![BTBS Circunvalacion](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/BeyondTheBalticSea/BTBS_Circunvalacion.webp)


*El trabajo realizado con la circunvalación de San Petesburgo es algo digno de ver*


Ya más a nivel general, los desarrolladores han creado **multitud de recursos nuevos especiales para BTBS**. Podemos encontrar por ejemplo, casas unifamiliares con la **arquitectura típica de los diferentes paises**, o edificios sobrios y espartanos como los antes mencionados en San Petesburgo. En nuestro camino veremos a menudo en los márgenes de la carretera bastantes **iglesias ortodoxas de estilo tradicional que brillan por su variedad y belleza**, así como también templos más modernos en las ciudades con un diseño mucho más contemporaneo y singular.


A nivel de vehículos del tráfico, veremos bastante a menudo **coches, todoterrenos, furgonetas y camiones propios** que nos recuerdan las latitudes por las que estamos conduciendo. También encontraremos **tranvías y trenes conservados de épocas pasadas** que llaman inmediatamente nuestra atención. Por supuesto, el diseño de las señales de tráfico, semáforos, e indicadores son iguales a los de cada pais visitado. He de decir que existe al menos un puente levadizo en juego, aunque no tuve la oportunidad de cruzarlo, o al menos no me dí cuenta. Como sabeis, este recurso también apareció hace bien poco en la DLC Oregon de American Truck Simulator y parece que ha llegado para quedarse, lo cual es de agradecer. Por supuesto el juego incluye como es habitual **empresas y remolques propios de los paises bálticos**, incluyendo los transportes de alta capacidad de dos remolques mencionados al principio de este análisis. Los bosques y vegetación también se corresponden con la zona y el clima de esos lugares, lo cual ayuda aun más a la inmmersión en el juego.


![BTBS Iglesia](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/BeyondTheBalticSea/BTBS_Iglesia.webp)


*Encontraremos una buena cantidad de iglesias en nuestro camino*


Hay un detalle que no ha pasado desapercibido, y es que en BTBS **nunca se llega a poner de todo el sol**, como se corresponde con los paises cercanos al circulo polar ártico, por lo que al contrario que en otros territorios, podremos comtemplar el paisaje incluso de noche, lo cual es un detalle de realismo a agradecer. Aunque hay un pero, y es que durante el camino no vermos nunca la nieve, algo habitual en estos paises, especialmente en los más nórdicos.


Como sabreis, esto último no es un problema de esta expansión, ya que implicaría hacer cambios de calado en el juego base, para poder incluir no solo la precipitación de este fenómeno natural, sinó su acumulación en las carreteras y sus bordes, y también la existencia de las estaciones, algo muy demandado por los fans de ETS2. Cada estación tendría que tener su propia climatología, cambiaría el paisaje, la vegetación, la luz, las transiciones y numero de horas de día y noche.... y como os podeis imaginar esto sería un trabajo titánico que implicaría un gran número de recursos humanos y tiempo. Por lo que es perfectamente entendible que sigamos circulando en verano como hasta ahora.


![BTBS Tanvía](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/BeyondTheBalticSea/BTBS_Tanvía.webp)


*"Beyond The Baltic Sea dispone" de montones de vehículos propios de la expansión*


Aunque suene repetitivo decirlo, "Beyond The Baltic Sea DLC" se ha convertido por derecho en una **expansión obligada** para todos los fans de Euro Truck Simulator 2, pues es bien seguro que la disfrutarán realizando trabajos mientras contemplan este entorno enorme y sin igual que hará que el jugador se "zambulla" completamente en las carreteras del noreste de Europa . Proporciona, sin salir de sus límites, **horas y horas de juego** mientras descubrimos cada uno de sus lugares, y todo por un precio completamente ajustado que no pasa de los 18€. Un gran trabajo de SCS Software que una vez más les pondrá un poco más dificil superarse a si mismos. Os dejamos con este fantástico video hecho por un fan donde podreis ver una gran parte de los elementos aquí descritos:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/yo5nvAJWB54" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Podeis comprar "Beyond The Baltic Sea DLC" en la **[tienda de Humble Bundle](https://www.humblebundle.com/store/euro-truck-simulator-2-beyond-the-baltic-sea?partner=jugandoenlinux)** (Patrocinado) o en [Steam](https://store.steampowered.com/app/925580/Euro_Truck_Simulator_2__Beyond_the_Baltic_Sea/). Recordad que es necesario disponer de [Euro Truck Simulator 2](https://www.humblebundle.com/store/euro-truck-simulator-2?partner=jugandoenlinux) para poder jugar esta expansión.


 


¿Habeis conducido ya por las carreteras bálticas? ¿Qué os ha parecido esta expansión? Déjanos tus impresiones sobre "Beyond the Baltic Sea" en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

