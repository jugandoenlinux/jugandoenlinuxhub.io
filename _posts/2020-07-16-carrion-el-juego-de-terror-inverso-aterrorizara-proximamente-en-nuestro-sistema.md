---
author: yodefuensa
category: Terror
date: 2020-07-16 07:36:48
excerpt: "<p>Devolver Digital nos traer\xE1 su pr\xF3xima joya en la que \"el terror\
  \ no tiene forma\"</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Carrion/carrion2.webp
joomla_id: 1241
joomla_url: carrion-el-juego-de-terror-inverso-aterrorizara-proximamente-en-nuestro-sistema
layout: post
tags:
- terror
- devolver-digital
title: "Carrion, el juego de \"terror inverso\" aterrorizar\xE1 pr\xF3ximamente en\
  \ nuestro sistema"
---
Devolver Digital nos traerá su próxima joya en la que "el terror no tiene forma"


 


En la pasada la conferencia de Devolver Digital pudimos ver que Carrion llegaría a nuestro sistema. La espera será breve pues el lanzamiento es el próximo 23 de julio en [steam](https://store.steampowered.com/app/953490/CARRION/)


Como bien han querido describirlo sus creadores. "CARRION es un juego de terror inverso en el que asumes el papel de una criatura amorfa de origen desconocido. Acecha y consume a los que te encarcelaron para difundir el miedo y el pánico por toda la instalación. Crece y evoluciona a medida que derribas esta prisión y adquieres habilidades cada vez más devastadoras en el camino hacia la retribución."


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/EbXBqae_iJI" width="560"></iframe></div>


Podemos ver el apartado gráfico es un precioso pixelart 2D con efectos de iluminación por lo que los requisitos son bastante comedidos.


Mínimo:


+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 18.04+
+ **Procesador:** 2 core processor
+ **Memoria:** 1024 MB de RAM
+ **Gráficos:** compatible with OpenGL 3.0
+ **Almacenamiento:** 500 MB de espacio disponible


Además, tenemos disponible un pequeño adelanto del juego que podemos descargar y disfrutar desde su [página de Steam](https://store.steampowered.com/app/953490/CARRION/).


¿Qué te parece la propuesta de Carrion? ¿Te gustan los juegos de terror 2D?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

