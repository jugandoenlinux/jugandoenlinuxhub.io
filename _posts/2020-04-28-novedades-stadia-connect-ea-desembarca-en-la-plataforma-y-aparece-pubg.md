---
author: Pato
category: Noticia
date: 2020-04-28 17:17:46
excerpt: "<p>El evento online de la plataforma nos trae suculentas novedades: Star\
  \ Wars, FIFA, PUBG...</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/stadiaconnect042020.webp
joomla_id: 1215
joomla_url: novedades-stadia-connect-ea-desembarca-en-la-plataforma-y-aparece-pubg
layout: post
title: 'Novedades Stadia Connect: EA desembarca en la plataforma y aparece PUBG'
---
El evento online de la plataforma nos trae suculentas novedades: Star Wars, FIFA, PUBG...


La espectación que estaba levantando Stadia con el evento online de hoy en parte estaba justificado. El hecho de las circunstancias que estamos viviendo hace que cada Stadia Connect, que así se llaman los eventos sea más llamativo si cabe dado el impacto que puede tener.


Y para muestra, Stadia ha comenzado recordando los grandes juegos que ya podemos disfrutar en la plataforma o que están por llegar en próximas fechas, a saber:


**Darksiders Genesis**


**Orcs Must Die 3**


**Mortal Kombat 11**


**Lara Croft and the Temple of Osiris**


**Kona**


Y a continuación nos cuentan que **Electronic Arts** (si, EA, si) desembarcará en Stadia con varios juegos, comenzando por:


**Star Wars: Jedi fallen Order**


**Madden NFL**


**FIFA**


Además, también han anunciado otros juegos que llegarán como son **Octopath Traveller** (ya disponible), **Rock of Ages 3** (Junio), **Zombie Army 4** (1 de Mayo), **Crayta** (Stadia Pro, Verano), **Wave Break** (First on Stadia, Verano), **Embr** (acceso anticipado, Verano) y **Get Packed** (First on Stadia, ya disponible).


Por último, y no menos importante, **Player Unknown Battlegrounds** desembarca en Stadia **gratis para los suscriptores a Stadia Pro,** con lo que ahora mismo todos los que accedan a la plataforma tendrán acceso al juego gratis durante lo que dure la oferta de Stadia Pro.


Puedes ver el evento de hoy de Stadia Connect en el siguiente vídeo:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/zPITKqTguxI" width="560"></iframe></div>


Recordaros que Stadia es un servicio de juego en Streaming desde los servidores de Google Stadia con sistema base Debian y API Vulkan, y que podemos disfrutar desde nuestros sistemas Linux de escritorio con un navegador con base Chromium.


¿Qué te parecen los juegos que vienen a Stadia? ¿Jugarás a los juegos de EA en la plataforma? ¿Y a PUBG?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

