---
author: Pato
category: Rol
date: 2017-01-30 21:09:55
excerpt: "<p>Disponible en nuestro sistema el d\xEDa de lanzamiento.&nbsp;</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/274936c4b649c88ffad7944bfc7a744a.webp
joomla_id: 202
joomla_url: disgaea-2-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- accion
- rol
- steam
- estrategia
- jrpg
title: "'Disgaea 2' ya est\xE1 disponible en Linux/SteamOS"
---
Disponible en nuestro sistema el día de lanzamiento. 

![](http://cdn.akamai.steamstatic.com/steam/apps/495280/header.jpg)


Disgaea 2 merece una mención aparte por lo inusual del lanzamiento en Linux/SteamOS, ya que no es habitual que recibamos títulos de este tipo en nuestro sistema favorito. El género de "Rol estratégico" de calidad desde luego no es algo que abunde mucho en Linux, y mucho menos si hablamos del japonés, por lo que el desarrollo de "Nippon Ichi" tiene doble mérito.


El caso es que ahora nos llega este juego que además es la "Definitive Edition" que trae todos los extras aparecidos en consolas. Disgaea 2 nos lleva a un mundo de fantasía donde un señor oscuro ha maldecido a la tierra y los demonios campan a su aire. Solo un joven ha resistido a la maldición y junto con algunos compañeros lucharán.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/8GeAAyr-oNI" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 'Disgaea 2' cuenta con unas 200 clases de personajes para formar tu grupo, un nuevo sistema de "Corte oscura" donde los buenos son castigados y los malos adorados, utiliza el nuevo ataque "Stack" para añadir un nivel extra de estrategia y mucho mas. 


Si te van este tipo de juegos no lo dejes escapar. Quien sabe... si consigue un buen nivel de ventas ¡puede que nos traigan el resto de la serie también!. Ciertamente no es habitual recibir JRPGs de esta calidad y renombre en nuestro sistema.


'Disgaea 2' no está disponible en español, pero si eso no es problema para ti puedes conseguirlo en su página de Steam, además con un 10% de descuento por su lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/495280/" width="646"></iframe></div>

