---
author: Pato
category: Rol
date: 2018-03-08 21:28:01
excerpt: <p>El juego ha sido publicado hoy mismo para otros sistemas</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/59107b985636086705aabc0c717a42ae.webp
joomla_id: 672
joomla_url: earthlock-llega-a-linux-steamos-manana-en-fase-beta
layout: post
tags:
- indie
- aventura
- rol
- estrategia
title: "'Earthlock' llega a Linux/SteamOS ma\xF1ana en fase beta"
---
El juego ha sido publicado hoy mismo para otros sistemas

Otro juego de rol nos llegará mañana. Se trata de 'Earthlock', un título que mezcla el rol, la aventura y la estrategia por turnos para un jugador con la jugabilidad de los títulos de los 90. En un [escueto post](http://steamcommunity.com/app/761030/discussions/0/1698293068433719305/) en los foros de Steam el estudio acaba de anunciar que tras el lanzamiento del juego hoy mismo, mañana lanzarán una beta para testear la versión de nuestro sistema favorito.


*"Earthlock es un RPG de aventuras inspirado en los clásicos juegos de rol en 3D de los 90, con un enfoque fresco en los combates por turnos y progresión de personajes.*


*Entra en un mundo encantado por su desgraciado pasado y únete a los héroes en su camino para rescatar al tío de Amon, un culto ancestral y desvela el secreto de Earthlock"*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/j1rB5pKSxfk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Características:


* Explora el mundo mágico de Umbra.
* Batallas por turnos ágiles y áltamente estratégicas.
* Cosecha materiales mágicos para crear artículos útiles y beneficios en tu isla de origen.
* Utiliza tu tabla de talentos: combina un 'árbol de habilidades' clásico y equipamiento para personalizar la progresión de tu personaje
* Alterna entre 6 personajes jugables.


En cuanto a los requisitos para Linux, aun no han sido publicados, y el juego está en proceso de ser traducido al español. Si te interesa la propuesta de 'Hearthlock', puedes ver todos los detalles en su página de Steam donde estará disponible en fase beta mañana mismo:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/761030/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

