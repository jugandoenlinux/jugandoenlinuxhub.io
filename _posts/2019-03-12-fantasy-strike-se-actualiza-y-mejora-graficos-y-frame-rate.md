---
author: Pato
category: Arcade
date: 2019-03-12 18:52:08
excerpt: "<p>El juego de <span class=\"username u-dir\" dir=\"ltr\">@SirlinGames</span>\
  \ a\xFAn en acceso anticipado renueva la iluminaci\xF3n y mejora el refresco multiplic\xE1\
  ndolo por 2,5</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6cc1906906242cb93fd96a345b4d3e52.webp
joomla_id: 993
joomla_url: fantasy-strike-se-actualiza-y-mejora-graficos-y-frame-rate
layout: post
tags:
- accion
- indie
- arcade
- lucha
title: "Fantasy Strike se actualiza y mejora gr\xE1ficos y frame rate"
---
El juego de @SirlinGames aún en acceso anticipado renueva la iluminación y mejora el refresco multiplicándolo por 2,5

**Fantasy Strike** es uno de los pocos exponentes de lucha (por no decir el único, con permiso de **Skullgirls**) que tenemos **nativo** en Linux/SteamOS, con una calidad bastante decente. El juego aún sigue en acceso anticipado desde hace ya mas de un año, pero sigue recibiendo actualizaciones y contenido. Ahora nos hacemos eco de la última actualización que nos trae un renovado apartado gráfico debido al cambio del motor de iluminación lo que influye notablemente en el aspecto y el rendimiento del juego.


*"Hemos hecho mejoras masivas en el rendimiento. Para hacer que el juego se ejecute más rápido, tuvimos que hacer que ...  los gráficos se vean mejor. El juego ejecuta aproximadamente el 250% de los fps que solía hacer!, sí, realmente. Alrededor de 2.5x más rápido. Es una locura y esperamos que se lo digas a todos.  
  
Ahora solo hay dos configuraciones de calidad de gráficos (estándar y alta) en lugar de cuatro configuraciones, y la nueva configuración más baja se ve mejor que la configuración más alta anterior ... mientras se ejecuta más rápido que la configuración más baj**a"*


Algunas imágenes para comparar:


![1](https://steamcdn-a.akamaihd.net/steamcommunity/public/images/clans/29867465/95ee1e7ef68ad639f5aa86f54dfbcdff329cb1dc.gif)


![2](https://steamcdn-a.akamaihd.net/steamcommunity/public/images/clans/29867465/b3d19e26a2a1f94cee9e625fbeaabd0f1d92c47f.gif)


![3](https://steamcdn-a.akamaihd.net/steamcommunity/public/images/clans/29867465/19c5ae66d119275b8d5e1522b63f1756666f4dd4.gif)


Desde luego hay diferencia. Además de esto, se ha añadido un nuevo modo de juego, se han llevado a cabo multitud de ajustes en los efectos de los golpes, se han añadido nuevas mejoras y ajustes y se han añadido nuevos sonidos. La lista de cambios es muy larga, por lo que si quieres verla completa puedes visitar e[l anuncio en el foro de Steam](https://steamcommunity.com/games/390560/announcements/detail/1789523941897735695).


Si te gusta la lucha y quieres probar un estilo nuevo tienes disponible Fantasy Strike en acceso anticipado en su [página de Steam.](https://store.steampowered.com/app/390560?snr=2_9_100003__apphubheader)


¿Que te parece Fantasy Strike? ¿Te gustan los juegos de lucha?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux)

