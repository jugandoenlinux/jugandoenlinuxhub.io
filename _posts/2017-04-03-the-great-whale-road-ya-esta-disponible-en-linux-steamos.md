---
author: Pato
category: Rol
date: 2017-04-03 19:55:25
excerpt: <p>El juego obra de Sunburned Games mezcla RPG, aventura y combate por turnos</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5b98a51d844cf083418c7193dcee292b.webp
joomla_id: 277
joomla_url: the-great-whale-road-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- indie
- aventura
- rol
- estrategia
title: "'The Great Whale Road' ya est\xE1 disponible en Linux/SteamOS"
---
El juego obra de Sunburned Games mezcla RPG, aventura y combate por turnos

Desde hace un tiempo se ha puesto de moda el mezclar varios géneros para dar forma a juegos innovadores y que supongan nuevas experiencias. En este aspecto, el juego del estudio español Sunburned Games 'The Great Whale Road' nos propone un juego que mezcla rol y acción por turnos con una historia absorvente que nos llevará a vivir aventuras en un mundo antiguo en la alta edad media.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/WoxCrR2xWFE" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Sinopsis de Steam:



> 
> Ambientado en la alta edad media, el juego revive el mundo antiguo con su arte dibujada digitalmente, sus coloridos personajes, su atmosférica banda sonora y su folclore minuciosamente investigado.
> 
> 
> Lucha, comercia y descubre la historia de tu poblado y sus héroes.
> 
> 
> El juego se divide en dos estaciones. En invierno administrarás tus recursos y harás planes para el resto del año. Durante el verano navegarás por la ruta de la ballena y visitarás otros poblados y ciudades. Tú y tu tropa tendréis que completar una misión cada año, asegurándoos de que tendréis alimento suficiente para dar de comer a todos durante el invierno. Las localizaciones dibujadas a mano están conectadas con las pantallas de navegación. El combate por turnos y cuadrícula se aseguran de que el Valhalla esté a un grito de guerra de distancia.
> 
> 
> 


Características:


Vive la historia de los daneses de Úlfarrsted, un pequeño poblado en las islas del Mar del Norte, cerca de la costa donde las tierras danesas y sajonas hacen frontera.   
Una campaña que te llevará desde el norte de Dinamarca hasta el sur de Inglaterra.  
Misiones de personajes.  
Progesión de personajes.  
Armas y armaduras para equipar a tus personajes.  
Cuatro clases de guerrero y sus respectivos gritos de guerra.  
Combates por mar y tierra.  
Un sistema de eventos que cubre todas las áreas de la vida tradicional en la edad media, desde la agricultura a las contiendas entre familias.  
Sistema de comercio dinámico.  
Mejoras para tu barco y tu poblado.


Si quieres darle una oportunidad, tienes disponible 'The Whale Road' en español en su página de Steam, con un descuento del 10% hasta el 6 de Abril:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/464830/" width="646"></iframe></div>


 ¿Piensas jugar a 'The Whale Road'? ¿Si lo estás jugando, nos cuentas que te parece?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

