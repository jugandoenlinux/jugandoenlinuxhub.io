---
author: Pato
category: Hardware
date: 2022-02-07 21:41:48
excerpt: "<p>Detalles como drivers espec\xEDficos para el Kernel, opciones gr\xE1\
  ficas, soporte para FSR y las primeras previews...</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/overview-shared-library-spanish.webp
joomla_id: 1425
joomla_url: salen-a-la-luz-nuevos-detalles-de-steam-deck-y-los-primeros-avances-sobre-la-maquina
layout: post
tags:
- primeras-impresiones
- steam-deck
- amd-fsr
- previews
- avance
title: "Salen a la luz nuevos detalles de Steam Deck, y los primeros avances sobre\
  \ la m\xE1quina"
---
Detalles como drivers específicos para el Kernel, opciones gráficas, soporte para FSR y las primeras previews...


Van a ser unas semanas muy movidas en cuanto a Steam Deck. De momento, vamos a repasar lo que ha estado saliendo a la luz en los últimos días que no es poco.


#### Un driver específico para Steam Deck


Lo primero que vamos a mencionar, [gracias a Phoronix.com](https://www.phoronix.com/scan.php?page=news_item&px=Steam-Deck-Platform-Driver) es **la aparición de un driver** para el kernel Linux para soportar el dispositivo "VLV0100" por parte de Valve, con la misión de ofrecer **funcionalidades como el control del ventilador de CPU o dispositivo, acceso a registros DDIC, mediciones de temperatura de la batería, configuraciones relacionadas con la pantalla y notificaciones de eventos USB tipo C**. Se espera que llegue al kernel en la próxima iteración, la 5.18 para el mes de Marzo por lo que es muy probable que la Steam Deck salga de fábrica con un kernel parcheado para ofrecer todas estas funcionalidades, dejando para más adelante la actualización mediante una futura actualización del sistema.


#### Opciones gráficas y FSR


Por otro lado, gracias a [boilingsteam.com](https://boilingsteam.com/steam-deck-gpu-settings-fully-customizable/) sabemos que han salido a la luz vídeos donde se pueden observar **ciertas opciones de configuración gráfica** para la Steam Deck, con cinco tipos de configuración:


* Rendimiento de interfaz
* Rendimiento GPU
* Limitador de FPS
* Configuración de TDP
* Activación FSR


La primera opción probablemente tenga que ver con la configuración de la capa gráfica de los menús, la segunda que afectará al rendimiento gráfico según el artículo tendrá opciones como "Auto", "manual", "bajo" y "alto", con lo que dependiendo de la opción que elijamos tendremos más o menos rendimiento visual, con un impacto directo en el consumo de la batería.


La tercera opción debería ofrecer un limitador de "framerate" para jugar con una fluidez aceptable ya sea a 30 fps o a 60 fps. La cuarta opción sería para configurar el rendimiento bruto que entrega la APU semipersonalizada, con un impacto directo en el consumo de batería (esto podría dar una especie de "Boost" o extra para juegos exigentes) y por último y la opción más interesante es la **activación de FSR**, que hay que recordar que **es la tecnología de reescalado de AMD**. Esta tecnología permite a la gráfica renderizar los juegos a una resolución inferior a la nativa de pantalla y luego reescalarla para representarla a resolución nativa casi sin pérdida de calidad. Esta opción será la que posiblemente permita conectar la Deck a una televisión mediante un dock USB-C y poder jugar de forma decente a resoluciones 1080p, o **ejecutar juegos muy exigentes gráficamente a framerates aceptables en la propia Deck**. Además, a diferencia del DLSS de Nvidia **esta tecnología no depende de que el juego la tenga implementada** por lo que en teoría se puede activar independientemente del juego que estemos jugando, y deberíamos notar cierta mejora en el rendimiento del juego. Eso sí, la mejora dependerá del título en cuestión.


Aquí tenéis el vídeo que público Boilingsteam, eso sí, tendréis que activar subtítulos por que está en chino:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/dxeQL-tFOhY" title="YouTube video player" width="560"></iframe></div>


####  Primeras previews


Hoy parece que se ha abierto la veda, y han comenzado a circular los primeros vídeos en youtube de algunos grandes influencers que han tenido la suerte de tener una Steam Deck en sus manos y han comenzado a mostrar **todo tipo de detalles sobre rendimiento, temperaturas, sistema, juegos, etc...** pero más que contártelo, mejor que veas dos de los más influyentes. (Spoiler: Steam Deck supera con creces cualquier otra máquina de su estilo tanto en rendimiento/precio/prestaciones que pueda haber en el mercado). Activa los subtítulos si no entiendes el inglés:


**Gamers Nexus**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/NeQH__XVa64" title="YouTube video player" width="560"></iframe></div>


Y quizás el influencer más importante en su categoría, **Linus Tech Tips**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/HjZ4POvk14c" title="YouTube video player" width="560"></iframe></div>


Estaremos atentos a las novedades que seguro se irán sucediendo en los próximos días y semanas.


Cuéntame tus impresiones en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

