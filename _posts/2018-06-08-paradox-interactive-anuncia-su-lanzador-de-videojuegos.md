---
author: Serjor
category: Software
date: 2018-06-08 16:48:47
excerpt: "<p>La versi\xF3n para GNU/Linux llegar\xE1 pronto</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0b2c0f4f7276d5cab9ebf370bc3a4b57.webp
joomla_id: 764
joomla_url: paradox-interactive-anuncia-su-lanzador-de-videojuegos
layout: post
tags:
- paradox
- paradox-interactive
title: Paradox Interactive anuncia su lanzador de videojuegos
---
La versión para GNU/Linux llegará pronto

Vía [reddit](https://www.reddit.com/r/linux_gaming/comments/8pjzs6/paradox_launcher_coming_soon_to_linux/) nos enteramos de que Paradox Interactive, quién recientemente ha adquirido a la empresa desarrolladora de Battletech, ha anunciado su nuevo [lanzador de juegos](https://play.paradoxplaza.com/), y tal y cómo podemos leer en su misma página, la versión de GNU/Linux llegará pronto.


La verdad es que son grandes noticias, ya que la gente de Paradox ya ha mostrado varias veces sus [dudas](https://twitter.com/ShamsJorjani/status/963175186953588736) respecto al futuro de GNU/Linux como plataforma de videojuegos, así que podemos pensar que aún con [reservas](https://www.gamingonlinux.com/articles/paradox-havent-decided-if-their-new-game-imperator-rome-will-be-on-linux-update-it-will.11822), siguen apostando por ella.


Y tú, ¿qué piensas sobre que cada compañía tenga su propio launcher? Cuéntanoslo en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

