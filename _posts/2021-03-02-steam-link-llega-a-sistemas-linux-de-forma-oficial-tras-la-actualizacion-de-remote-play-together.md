---
author: Pato
category: Software
date: 2021-03-02 22:39:51
excerpt: "<p>Hasta ahora, la aplicaci\xF3n solo estaba disponible en Linux para la\
  \ Raspberry en arquitectura ARM 32bits</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/steamlink/steamlink.webp
joomla_id: 1284
joomla_url: steam-link-llega-a-sistemas-linux-de-forma-oficial-tras-la-actualizacion-de-remote-play-together
layout: post
tags:
- valve
- steam-link
- noticias
title: "Steam Link llega a sistemas Linux de forma oficial tras la actualizaci\xF3\
  n de Remote Play Together"
---
Hasta ahora, la aplicación solo estaba disponible en Linux para la Raspberry en arquitectura ARM 32bits


Tenemos novedades interesantes respecto al soporte que brinda Valve sobre Linux. Hace tan solo unos días que Valve anunció la actualización de "[Remote Play Together - invite anyone](https://steamcommunity.com/groups/homestream/discussions/0/3111395750231979202/)" en fase beta para todo aquel que quiera jugar con amigos de forma remota como si fuera local sin necesidad de tener cuenta, y ahora nos anuncian la llegada de la aplicación **Steam Link** para Linux. 


*Un momento... ¿pero Steam Link no estaba ya disponible en Linux?*


Pues si, pero no. Vamos a ponernos en contexto:


**Remote Play Together** es la forma que nos brinda Valve desde Steam para poder jugar con otras personas de forma remota utilizando nuestro ordenador y nuestra conexión a internet para "compartir" nuestra sesión de juego como si estuviésemos delante de la misma pantalla. De este modo podemos jugar a los juegos que no tienen multijugador en línea, si no solo multijugador local sin necesidad de emplear ninguna otra tecnología. Esta característica es muy útil en los tiempos que corren, en los que nos es muy difícil o imposible juntarnos en el salón de casa con los amig@s o familia debido a la pandemia.


Para utilizar esta característica que está disponible desde hace ya un tiempo era necesario tener Steam en versión beta instalado en el equipo que hace de "servidor" del juego, y por parte del jugador remoto era necesario tener un ordenador con otra cuenta de Steam.


Con la actualización de la semana pasada, Valve eliminó la necesidad de tener una cuenta de Steam para poder jugar en remoto pues **con tan solo compartir un código** con otras personas e introducirlo **en cualquiera de los sistemas soportados  por la aplicación Steam Link** podemos jugar juntos sin más. De este modo las opciones de Remote Play Together se amplían al poder "conectar" a una sesión de juego remota desde cualquier dispositivo con soporte para la aplicación Steam Link, que está disponible para los sistemas  iOS, Android, Windows y... Linux pero solo arquitectura ARM 32bits para Raspberry Pi. **Hasta ahora**.


Mas de una voz se alzó la semana pasada al ver que en todos los sistemas era posible jugar mediante la app Steam Link sin necesidad de una cuenta, salvo en sistemas GNU/Linux x86-64. Valve reaccionó rápido afirmando que estaban trabajando en la aplicación para Linux, y ahora con la colaboración de sus socios de Collabora nos anuncian que **ya tenemos disponible la app en formato Flatpak,**lo que supone la primera vez que Valve distribuye una aplicación en este formato de manera oficial. Hurra!


Esto además, abre las posibilidades de utilizar equipos que tengamos por casa con pocos recursos, como portátiles antiguos, barebones o PCs de pequeño formato para instalar un sistema Linux ligero y mediante Steam Link utilizarlo como "cliente" remoto y poder jugar en cualquier televisión y lugar de casa sin tener que estar sentado delante del ordenador de juego. ¡Las posibilidades solo están limitadas por tu imaginación!.


### Como instalar la app Steam Link en GNU/Linux


Como hemos dicho, Steam Link está disponible en fase beta en formato Flatpak, por lo que necesitaremos un sistema con soporte para dicho formato. Si tienes un sistema con soporte para Flatpak puedes saltarte el primer paso. Si no, sigue leyendo.


**Paso 1: Instalar el soporte Flatpak**


Lo primero, tendrás que visitar la [página de soporte de Flatpak](https://flatpak.org/setup/) y seleccionar tu distribución Linux. Una vez hecho te mostrarán la forma de instalar el soporte Flatpak. A modo de ejemplo, para Ubuntu 20.04 los pasos serían los siguientes:


El soporte Flatpak para Ubuntu está disponible diréctamente desde los repositorios desde hace algunas versiones, por lo que con solo ejecutar el siguiente comando en una terminal:


 `$ sudo apt install flatpak`


A continuación instalaremos el plugin de Gnome Software para no tener que estar instalando las aplicaciones Flatpak mediante la línea de comandos, si no desde la propia aplicación del sistema:



```
 $ sudo apt install gnome-software-plugin-flatpak
```

Una vez hecho esto ya tenemos instalado el soporte Flatpak, pero si queremos sacarle partido a este formato, lo ideal es tener acceso al repositorio de aplicaciones Flathub donde además de Steam Link tendremos disponible una gran cantidad de programas y software en este formato. Para ello ejecutaremos:



 `$ flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo`  
  



**Paso 2: Instalar la App Steam Link**


Para esto, si hemos instalado el soporte Flathub en nuestro sistema tan solo tendremos que buscarla en la tienda de aplicaciones, o ir diréctamente al enlace de la aplicación en Flathub:


<https://flathub.org/apps/details/com.valvesoftware.SteamLink>


Y clicar en "instalar". Si todo ha ido bien se instalará la aplicación y ya podrás jugar a tus juegos o a los juegos de tus [amig@s](mailto:amig@s) de forma remota.


### Cómo utilizar Remote Play Together


Como hemos dicho, ahora puedes invitar a otra persona (u otras, tantas como soporte tu línea de internet) a jugar contigo en tus juegos con soporte multijugador local y Remote Play. Para ello tienes que optar al cliente beta de Steam en tu ordenador. Una vez actualizado, encontrarás un botón COPIAR ENLACE en tu lista de amigos cada vez que juegues a uno de los miles de juegos compatibles con Steam Remote Play Together.


Para acceder rápidamente a sus propios juegos con soporte para Remote Play Together, haz clic en el icono de opciones de filtrado avanzadas en tu Biblioteca, luego selecciona Remote Play Together en Funciones. Incluso puedes crear una colección dinámica y así siempre tendrás tus juegos compatibles a mano.


Cuando estés listo para jugar, inicia tu juego, luego selecciona un enlace de invitación de tu lista de amigos en la superposición de Steam. Envía el enlace a tu amigo en Windows, iOS, Android, Raspberry Pi o Linux y podrán hacer clic en tu invitación para unirse a la partida. Facil, no?


Para saber más, los enlaces de los anuncios son:


<https://steamcommunity.com/groups/homestream/discussions/0/3111395750231979202/>


<https://steamcommunity.com/app/353380/discussions/10/3106892760562833187/>


