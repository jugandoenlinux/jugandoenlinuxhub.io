---
author: leillo1975
category: Carreras
date: 2019-02-28 15:00:19
excerpt: "<p>10 divertidos proyectos \"sobre ruedas\" en el Open Source.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2da7e6c037342f54c10e77c0d3e4a775.webp
joomla_id: 977
joomla_url: los-mejores-juegos-de-coches-libres
layout: post
tags:
- open-source
- yorg
- speed-dreams
- dust-racing-2d
- supertuxkart
- vdrift
- torcs
- trigger-rally
- stunt-rally
- rigs-of-rods
title: "Los mejores juegos de conducci\xF3n libres."
---
10 divertidos proyectos "sobre ruedas" en el Open Source.


Supongo que muchos de vosotros conoceréis mi gran afición por los juegos de conducción y cuanto disfruto de ellos. Si os soy sincero es una afición relativamente reciente, pues hace años me gustaban pero no por encima de otros géneros como la estrategia o los shooters. Todo vino a raiz de Euro Truck Simulator 2, que particularmente me gustó mucho, pero que con mando se me hacía muy difícil, y me llevó a comprar de segunda mano mi primer volante decente, un Logitech Driving Force GT. El poder usar este periférico me descubrió una nueva dimensión en los juegos de conducción, haciendo que fuese adquiriendo y probando cada vez más títulos.


Y si, todos sabemos que DIRT Rally, F1 2017 o GRID Autosport son una pasada de juegos, pero hay vida más allá de ellos en nuestro sistema, pues existen proyectos libres muy veteranos en el "mercado" que merecen una consideración y por supuesto también tienen su sitio aquí, en JugandoEnLinux.com. Encontramos juegos desde la rama más desenfadada y arcade, a los simuladores más serios, **juegos que tienen un gran esfuerzo detrás que merece ser reconocido**, en ocasiones discontinuados, y en otras todavía en desarrollo. Nosotros os los vamos a recordar aquí para que tengan el sitio que merecen:


### **SuperTuxKart**


Que levante la mano quien no conozca este título y no lo haya jugado una vez. Tomando como referencia la jugabilidad de **Mario Kart**, pero con las mascotas de los proyectos libres, este juego de carreras de Karts discurre en unos coloristas y cuidados escenarios que harán las delicias tanto de los más pequeños de la casa como de adultos. SuperTuxKart cuenta con una buena cantidad de modos de conducción e incluso con historia. Con un desarrollo continuado desde hace unos 15 años, actualmente el proyecto **está a punto de estrenar oficialmente el multiplayer**, aunque desde hace meses se puede jugar en sus [versiones de prueba](http://blog.supertuxkart.net/2019/01/supertuxkart-networking-010-beta-release.html) con muy buenos resultados. Podéis descargar este juego Multiplataforma (Linux, Windows, Android y Mac) su última versión oficial desde su [página web](https://supertuxkart.net). Recientemente lo hemos disfrutado en "La partida del viernes" tal y como puedes ver aquí:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/IHAr0kpU-NI" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


 


### **TORCS**


Nos ponemos serios y vamos ahora con uno de los simuladores pioneros del software libre. Con un [desarrollo](https://en.wikipedia.org/wiki/TORCS#Development) con más de 20 años, y con varios nombres a sus espaldas (RCS, ORCS y finalmente TORCS), en este juego podremos disputar carreras únicas, campeonatos, resistencia... con una amplia variedad de coches y circuitos en un entorno completamente 3D. El juego puede ser controlado con una amplia variedad de periféricos como gamepads, volantes y por supuesto teclado y ratón. Por norma general en casi todas las distribuciones podremos instalar facilmente sus paquetes (p.ej, en Ubuntu con "sudo apt install torcs"), pero también existe un [Flatpak](https://flathub.org/apps/details/net.sourceforge.torcs) y por supuesto su código en la [página del proyecto](https://sourceforge.net/projects/torcs/) si queremos compilarlo. Teneis más información en su [página web](http://torcs.sourceforge.net/). Aquí os dejamos un video donde repasamos algunas de sus características:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/7U2rTv9voms" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


 


### **Speed Dreams**


Vamos ahora a hablar sobre un juego que es un fork de TORCS, y que nació como la necesidad de incluir muchas más funciones a este último. En el año 2008, **un grupo de desarrolladores, cansados del lento ritmo de desarrollo de TORCS**, decidió tomar su código y crear **TORCS-NG** (Torcs Next Generation). En 2010 ya como [Speed Dreams](https://www.speed-dreams.net/), nombre votado por la comunidad, lanzan la primera versión pública, la 1.4, y dos años después tras muchos cambios llegaría la versión 2.0, que convertiría a Speed Dreams en un juego completamente diferente y mucho más avanzado que su "padre". Los menús serían completamente rediseñados, añadiendo muchísimas más opciones y haciéndolos mucho más intuitivos; el juego adquirió el tiempo dinámico, mejoras en los reflejos, el modo carrera, un nuevo modo de simulación y dual threading. Con el tiempo se fueron implementando muchas más opciones, como el modo multijugador local, el **Force Feedback**, y por supuesto más y mejores coches y pistas, entre otras cosas; convirtiéndolo en un juego muchísimo más completo que su predecesor.


Su desarrollo aunque no es excesivamente activo, es constante y recientemente han presentado la versión 2.2.2, de la que hablamos en un [artículo de nuestra web](index.php/homepage/generos/simulacion/14-simulacion/994-speed-dreams-disponible-en-flatpak). Hace algún tiempo publicamos una [entrevista con dos de sus creadores](index.php/homepage/entrevistas/35-entrevista/784-entrevista-speed-dreams-un-simulador-de-coches-libre) en nuestra web. **Si queréis compilarlo e instalarlo** podéis seguir un [Tutorial de nuestro Foro](index.php/foro/tutoriales/108-como-compilar-la-ultima-version-de-speed-dreams-gracias-gnuxero26). También podeis instalarlo en formato [Flatpak](https://flathub.org/apps/details/org.speed_dreams.SpeedDreams), creada por nuestro amigo @SonLink, aunque en este caso se trata de la versión 2.2.2RC2. Podéis ver las virtudes de Speed Dreams en el siguiente video:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/vYo5r4drlH8" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


 


### **Dakar 2011 Game**


Lo reconozco, sabía de este proyecto hace mucho tiempo, pero nunca lo había probado hasta ahora, quizás el buscar alternativas al comercial Dakar 18 que finalmente no ha visto versión para nuestro sistema haya influido en darle un espacio en esta compilación. El caso es que **este sería el segundo de los 3 juegos que lanzó este mismo desarrollador**, ([2010](http://dakar2010.sourceforge.net/), [2011](http://dakar2011.sourceforge.net/) y [2012](http://dakar2012.sourceforge.net/)). He escogido este y no el del año 2012 básicamente porque este último solo se puede compilar para Windows. El juego, construido en 3D con el motor libre [IrrLicht](http://irrlicht.sourceforge.net/), permite recorrer **14 etapas** y **competir contra 140 oponentes**. Las etapas están construidas basándose en las reales pero obviamente en miniatura. El sistema de navegación consiste en indicaciones que vemos en el hud cada cierto tiempo, checkpoints y un mapa para guiarnos. En el podrás conducir múltiples **coches basados en vehículos reales** (Mitsubishi Racing Lancer, Hummer H3, Nissan Navara, Volkswagen Touareg, BMW X3 CC, Jeep...) y **terrenos en hierba, fango, desierto y asfalto**. Los vehículos sufrirán daños que afectarán su rendimiento y el tener que recuperar nuestro coche tras un accidente nos penalizará en tiempo.


Para jugar a Dakar 2011 tendremos que [descargar sus archivos en SourceForge](https://sourceforge.net/projects/dakar2011/files/final/) y ejecutar el archivo "**Dakar2011.sh**". Si al ejecutar este archivo os da un error, sabed que es un problema de rutas a dos librerías, y que siguiendo el [tutorial](index.php/foro/tutoriales/143-como-ejecutar-dakar-2011) que hemos dejado en el foro podreis solucionarlo (gracias @Bernat). He aquí un video que hemos grabado recientemente para que lo veais en acción:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/-1sze8ISfsQ" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


 


### **VDrift**


Al igual que Speed Dreams, hablar de [VDrift](https://vdrift.net/) es hablar con palabras mayores. Nos encontramos ante otro veterano juego, basado en el motor de físicas [Vamos](http://vamos.sourceforge.net/), y con unos **acabados realmente destacables**. Con casi 15 años a sus espaldas, el juego permite a los jugadores conducir montones de coches a través de **detallados escenarios basados en circuitos reales**. Las físicas del juego son bastante realistas como le corresponde a un simulador, y por supuesto podremos usarlo con múltiples dispositivos como teclados, mandos y volantes, incluyendo el soporte experimental de **Force Feedback**.  El proyecto que luce en ciertos aspectos como un juego comercial, está parado últimamente, con pequeñas correcciones en el código como podemos ver en la [página de su proyecto](https://github.com/VDrift/vdrift/). La última versión que podemos conseguir es una [Nightly Build](https://sourceforge.net/projects/vdrift/files/vdrift/nightly%20builds/) de Noviembre de 2015. Según hemos sabido de su creador principal, **Joe Venzon**, **sería ideal que alguien retome el proyecto o haga un fork para crear algo completamente nuevo**. Esperemos que esto se produzca y que este proyecto tan destacable continúe su andadura y no caiga en el olvido. Si queréis **descargar y compilar Vdrift** podéis seguir el [tema que hemos preparado en la sección de Tutoriales](index.php/foro/tutoriales/141-compilar-e-instalar-vdrift-en-ubuntu) de nuestro Foro. Aquí está el video que hemos grabado hace unos días en nuestra web:  
  



<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/PUsgZ8YZkYA" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


 


### **Stunt Rally**


Hablando de Forks y de Vdrift, aquí tenemos un divertido proyecto que se basa en el anterior simulador para ofrecernos una experiencia mucho más árcade y desenfadada de los Rallies. Con un **aspecto más que destacable**, al igual que Vdrift, en [Stunt Rally](http://stuntrally.tuxfamily.org/) **podremos hacer desde los tramos más realistas en escenarios comunes a esta disciplina, como realizar saltos imposibles , circular por tubos y toboganes en mapas imposibles de auténtica fantasía**. Al igual que las pistas, **los vehículos que podremos utilizar son de lo más variado** encontrando desde el típico coche de rallys a superdeportivos, motos, el coche de batman o incluso naves espaciales. Podremos disputar carreras simples, campeonatos, desafíos, e incluso carreras online contra amigos o a pantalla partida. Durante las carreras podremos usar múltiples vistas, y recibiremos información en pantalla de las curvas que nos encontraremos y competiremos contra un coches fantasma predefinidos o de nuestro mejor tiempo. El juego, que utiliza el motor libre de gráficos [OGRE](https://www.ogre3d.org/), puede ser [descargado de la página de su proyecto en Sourceforge](https://sourceforge.net/projects/stuntrally/files/2.6/), donde encontraremos sus binarios sin necesidad de compilarlo. También es posible descargar un Flatpak desde [Flathub.org](https://flathub.org/apps/details/org.tuxfamily.StuntRally). Al igual que los anteriores juego también tenemos un video para mostraros:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/NKCn0cdhliI" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


 


### **Trigger Rally**


Continuamos con los Rallys, y en este caso vamos con otro proyecto con un enfoque un poco más serio que Stunt Rally. En [Trigger Rally](http://trigger-rally.sourceforge.net/) nos encontramos con un **juego de carreras para un jugador** donde tendremos la oportunidad de realizar etapas a través de diferentes y variados escenarios de arena, grava, nieve o asfalto, siguiendo un **sistema de checkpoints**. Recibiremos **completa información en pantalla** del tipo de curvas o accidentes del terreno e incluso a través de la **voz del copiloto**. También podremos usar diferentes vistas para permitirnos adaptarnos a la que nos haga sentir más cómodo. Aunque ofrece efectos como lluvia, nieve, niebla o diferentes condiciones de luz, **gráficamente el juego es limitado** y se agradecería un poco más de detalle tanto en coches como en circuitos y efectos, pero hay un **aspecto muy destacable**, y es que **las físicas de conducción son bastante buenas**, teniendo que controlar la técnica del contravolanteo para trazar las curvas, corregir constantemente la trazada o aplicar correctamente los pedales de freno y acelerador para obtener más agarre. Es posible usar mandos o volantes para jugar, pero el juego viene configurado para usar el teclado y no ofrece posibilidad de configurarlos en el menú, algo que estaría muy bien; aunque se puede hacer fácilmente editando un archivo de configuración que encontraremos en nuestro carpeta personal de forma oculta. **El proyecto, que continua en desarrollo constante**, puede ser descargado de su [página de Sourceforge](https://sourceforge.net/projects/trigger-rally/files/), aunque para no tener que compilarlo, **os será mucho más cómodo instalarlo desde los repositorios de vuestra distribución** (p.ej. en Ubuntu "sudo apt-get install trigger-rally"). Hace bien poco lo hemos jugado como podéis ver en este video:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/_P-l8_fJq9I" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


### **YORG**


Con una trayectoria relativamente corta, este proyecto libre ha logrado en poco tiempo hacerse un sitio, a nuestro juicio, entre los juegos libres más destacables, especialmente si tenemos en cuenta que **en tan solo dos años ha incluido multitud de nuevas e interesantes características en cada uno de sus lanzamientos**. [YORG](https://www.ya2.it/pages/yorg.html#yorg), acrónimo de "***Y***org's an ***O***pen ***R***acing ***G***ame" es un juego que está **programado en Python** y usa el motor gráfico libre [Panda3D](https://www.panda3d.org/). Cuando estamos ansiosos por recibir la esperada nueva versión 0.11, a día de hoy en YORG podemos encontrar un **divertido árcade de coches al estilo MicroMachines** donde podremos escoger entre 8 diferentes conductores, 7 variados y coloridos circuitos, y 8 coches con diversas características. Podremos usar dos tipos de vista durante las carreras, una cenital y otra trasera, en **a lo largo de la pista encontraremos diversos Powerups y Armas** que podremos activar para superar a nuestros enemigos. Los coches contarán con un sistema de daños que afectará a su velocidad, y estos podrán ser reparados pasando por boxes. También cuenta con un **apartado multijugador** que nos permitirá disputar carreras contra otros jugadores a través de internet.  En este momento no es posible usar mandos por un problema con Panda3D, pero se espera que en la próxima versión podamos hacerlo.


Como sabéis en varias ocasiones [hemos cubierto los lanzamientos de YORG](index.php/buscar?searchword=yorg&ordering=newest&searchphrase=all), e incluso hemos publicado una [interesantisima entrevista con Flavio Calva](index.php/homepage/entrevistas/35-entrevista/587-entrevista-a-flavio-calva-de-ya2-yorg), su creador. Podéis encontrar el [proyecto en Github](https://github.com/cflavio/yorg) y descargar la última versión de YORG desde su [página web](https://www.ya2.it/pages/yorg.html#yorg). Os dejamos con un video donde repasamos las novedades de su última versión:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/ZFHxDvEus68" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


### 


### **Rigs of Rods**


En este caso no estamos exactamente ante un juego propiamente dicho, sinó ante una especie de **simulador de físicas**, con capacidad para emular las reacciones de un vehículo ante determinados terrenos, superficies o incluso las colisiones contra objetos. En el año 2005 nació [Rigs of Rods](https://www.rigsofrods.org/) con la intención de ser un simulador de camiones todo-terreno, pero ahora **podemos encontrar vehículos de todo tipo**, hasta barcos, aviones y trenes. Tiene la particularidad de que **los vehículos están diseñados con nodos independientes pero interconectados**, por poner un ejemplo, las suspensiones actúan de forma diferente en cada rueda y esto afecta al agarre general del vehículo. En el juego no encontraremos ningún objetivo que cumplir, simplemente empezaremos con un personaje en un escenario al que podremos montar en un vehículo y circular libremente por el escenario elegido, pudiendo probar el comportamiento de este en determinadas superficies o como se deforma cuando choca contra otros elementos. El juego proporciona **diferentes vistas** y existe la posibilidad de poder utilizarlo en **multijugador** . Podéis encontrar la ultima versión del juego en la página de su proyecto, pero tendréis que compilar. Actualmente es posible instalarlo fácilmente con un [paquete Snap](https://snapcraft.io/rigs-of-rods). También podéis encontrar múltiples recursos (pistas y vehículos) para añadir al juego en [esta web](https://forum.rigsofrods.org/resources/). Aquí podéis ver su trailer oficial:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/bRbQ4OaljWs" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>




---


### **Dust Racing 2D**


Me había dejado atrás este pequeñín pero no menos importante, aunque nunca es tarde si la dicha es buena. El caso es que hace unos meses [os hablamos de él en nuestra web](index.php/homepage/generos/carreras/2-carreras/857-conoceis-dust-racing-2d) en uno de nuestros artículos. Para quien aun no lo conozca, se trata de un juego 2D, tal y como reza su nombre, con **vista cenital** en el que podremos competir en **diversos modos**, como Carrera, Contrarreloj o Duelo, en un amplio número de pistas. [Dust Racing 2D](https://github.com/juzzlin/DustRacing2D) a pesar de su **aparente simplicidad**, dispone de unas **físicas convincentes** (colisiones, derrapes...). Será necesario entrar en boxes para cambiar las gomas, y habrá que cumplir la normativa para no ser sancionado. Los efectos como el humo y el polvo están muy bien hechos, y a nivel general, teniendo en cuenta sus características, nos muestra unos **gráficos e interfaz sencillos pero bien presentados**. El tema del sonido también llama la atención pues los ruidos de motor, choques, derrapes están muy bien implementados. El juego escrito en **C++**, y haciendo uso de **QT** y **OpenGL** puede ser instalado fácilmente desde la mayoría de las distribuciones pues por norma general suele estar en los repositorios (p.ej.  en Ubuntu y derivadas: "*sudo apt install dustracing2d*")


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/3hrIzzItggQ" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


 


Como podéis ver podemos disfrutar de experiencias muy diferentes en el mundo de las cuatro ruedas sin salirnos del Software Libre. Proyectos muy interesantes que merecen toda nuestra atención por el trabajo desinteresado que tienen detrás y por que también, por supuesto, son muy divertidos. Esperamos que os haya gustado nuestra compilación y que nos digáis vuestra opinión sobre ellos en los comentarios, en nuestra ["Zona Racing" de Matrix](https://matrix.to/#/#zona_racing-jugandoenlinux:matrix.org) o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux)

