---
author: Serjor
category: Puzzles
date: 2020-03-05 22:23:47
excerpt: "<p>Una interesante propuesta dispuesta a quitarnos nuestras pocas horas\
  \ libres</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/mindustry/mindustry.webp
joomla_id: 1182
joomla_url: mindustry-un-juego-open-source-que-une-gestion-de-recursos-y-el-genero-tower-defense
layout: post
tags:
- open-source
- codigo-abierto
- mindustry
title: "Mindustry, un juego open source que une gesti\xF3n de recursos y el g\xE9\
  nero tower defense"
---
Una interesante propuesta dispuesta a quitarnos nuestras pocas horas libres


Escuchando el [capítulo 29](https://www.linux4everyone.com/29-schyken-nuggets-of-wisdom) de Linux4Everyone, he descubierto un juego open source que la verdad, no tenía ni idea de su existencia.


El juego en cuestión es [Mindustry](https://mindustrygame.github.io/), un juego un tanto curioso de explicar. Aparentemente es una mezcla de Factorio (no lo tengo en mi biblioteca y lo pude probar un poco por encima en una ocasión) y de un tower defense, que además, tiene soporte para partidas multijugador (¿partida con la comunidad uno de estos viernes quizás?).


Hemos estado probándolo un poco, y sinceramente, está muy muy bien, así que os hemos hecho un pequeño gameplay del tutorial:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/ZEQ5RZFGV-k" width="560"></iframe></div>


Si os interesa el juego podéis colaborar económicamente con él comprándolo en Steam, o podéis descargarlo directamente desde [itch.io](https://anuke.itch.io/mindustry) y aportar la cantidad que consideréis adecuada.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1127400/" style="border: 0px;" width="646"></iframe></div>


Y tú, ¿conocías este Mindustry? Cuéntanoslo en los comentarios, o en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux)

