---
author: yodefuensa
category: "An\xE1lisis"
date: 2017-03-09 12:35:00
excerpt: "<p>Si bien no es un t\xEDtulo nuevo, ahora que DIRT Rally esta de actualidad,\
  \ \xBFpor qu\xE9 no vamos a hacer un repaso a su \u201Chermano\u201D DIRT Showdown?.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6ddb2450462828abf9aabc88d6bfb7fe.webp
joomla_id: 238
joomla_url: analisis-dirt-showdown
layout: post
tags:
- analisis
- dirt-showdown
- virtual-progamming
- codemasters
title: "An\xE1lisis: Dirt Showdown"
---
Si bien no es un título nuevo, ahora que DIRT Rally esta de actualidad, ¿por qué no vamos a hacer un repaso a su “hermano” DIRT Showdown?.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/7JHxfFFBuOI" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


En 2012 Salió este título para pc y tres años más tarde aterriza en Linux de la mano de [Virtualprograming](https://www.vpltd.com/), compañía con la experiencia de haber portado anteriormente otros títulos triple A. Nos encontramos ante un título que si bien en Windows se ejecuta mejor, podemos decir que su desempeño está bastante a la par. En tarjetas Nvidia podemos ver apenas 5-10fps de diferencia, mientras que en las de AMD al no llevarse tan bien con OpenGL vemos como su rendimiento esta un poco por debajo de lo esperado. Por lo tanto no podemos decir que sea un mal port, las diferencias estarán en el driver de nuestra tarjeta y lo bien que esté implementado el OpenGL 4.1 necesario para este juego.

![image1](http://images.akamai.steamusercontent.com/ugc/85969528710200093/546FFE20250B23042E4B781606DA94193D598E40/)


En las modalidades de juego nos encontramos dos modos principales, **tour showdown** y **joy ride**, en esta última deberemos realizar una sucesión de trucos que nos pedirán en determinadas partes del escenario, y un modo libre donde probar nuestra habilidad para hacer piruetas.

![image2](http://images.akamai.steamusercontent.com/ugc/85971273893123186/AFBB5CB7104027F59EDB2F75A234ED98B15B3E5E/)


*joyride*

La modalidad **Tour Showdown** podemos considerarla el modo campaña, donde tendremos un total de 52 pruebas divididas en 4 temporadas. Aquí es donde descubrimos que estamos ante un título diferente dentro de la saga Dirt, pues abandonamos las clásicas etapas de rally para abrazar un estilo destruction derby. En general tenemos 3 tipos de modalidades: Carrera, Estilo y Destrucción.


Dentro de cada una de estas modalidades encontramos diferentes pruebas, y diferentes estilos de coches. En cada una de ellas elegiremos un tipo de coche más enfocado al tipo de prueba pudiendo mejorarlo en tres aspectos, rendimiento, durabilidad y manejo, algo simple comparado con los ajustes de coche en dirt3 que de por si ya eran básicos.


En la que podríamos denominar modalidad carrera tendremos:


* **Race Off**, será la “clásica” carrera donde todo vale, y debemos golpear a los diferentes contrincantes pues esto nos dará nitro.
* **8 ball** al igual que race off donde la única diferencia es que el circuito contará con cruces donde podremos chocar con nuestros rivales.
* **Eliminación** Carrera con temporizador, cada vez que el temporizador llegue a cero, el conductor en el último lugar se elimina.
* **Dominación** El circuito está dividido en cuatro sectores y los jugadores se otorgan puntos para el establecimiento de los tiempos más rápidos del sector. El ganador es el piloto que termina con más puntos.


![raceOff](http://images.akamai.steamusercontent.com/ugc/85971273893122852/FDFB9BC7A2D2556132E33A470D61EE91BFD8D340/)


Al ser un juego más callejero o sucio veremos como la IA intenta hacernos derrapar en las curvas para que perdamos, toda una carta blanca a dar rienda suelta a nuestro lado más bestia.Siguiendo con las pruebas más enfocadas a hacer saltar la chapa de nuestro coche vemos:


* **Rampage** Un todos contra todos.
* **knock out** Una variación de Rampage, donde la arena cuenta con una estructura en forma de tabla elevada. Los puntos se conceden para chocar contra otros coches, y por empujar a un rival de fuera de la plataforma.
* **Hard target**  Tendremos que sobrevivir el máximo de tiempo posible a los envites del resto de coches.


Por último a mi parecer el modo más exigente donde prima el control del coche y donde los amantes de la competición más disfrutarán.


* **Trick Rush:**  modo Gymkhana donde los jugadores tienen un tiempo limitado para completar tantos trucos como sea posible.
* **Head 2 Head** Si ya habíamos visto esta modalidad de carrera anteriormente esta vez tenemos el añadido de que realizaremos trucos mientras nos batimos por ser el más rápido.
* **Smash Hunter** tendremos que atravesar ladrillos con la mayor rapidez posible.

Dirt Shodown es un juego que está pensado para hacerse fuerte en el modo multijugador en que se añaden nuevos tipos de juego que me voy a ahorrar comentar porque a día de hoy el online esta muerto, con lo que podemos olvidarnos de encontrar partidas abiertas en internet. Por suerte Dirt Showdown rescata el modo de juego pantalla divida, al más puro estilo consolero que a día de hoy parece estar desapareciendo, por lo que se convierte en un titulo bastante divertido para jugar en compañía.


![image4](http://images.akamai.steamusercontent.com/ugc/85969528704634574/83BDDFEAAF04AED3B72ECCAE082EC4B2739C324C/)


*Los coches dejaran de lado las marcas por algunos de fabricación casera*


Desde el aspecto de jugabilidad podemos ver que nos encontramos frente a una compañía con experiencia, porque pese a ser un juego mucho más arcade necesitaremos de cierta habilidad para ir avanzando y superando varias pruebas, es decir pese a no ser un modo realista hay cierta complejidad de fondo. Puede parecernos que jugamos igual después de una hora que después de 20, pero la diferencia a la hora de afrontar las curvas establece una diferencia palpable, esto parece obvio, pero no todos los juegos arcade pueden presumir de tener un sistema de conducción con este trasfondo. Pese a ello recalco que es un arcade, por lo que los errores no penalizan de la misma forma que en anteriores entregas, pudiendo ir últimos en la última vuelta para acabar cruzando la bandera de cuadros primero.


Con Dirt Showdown nos encontramos ante un título bastante bueno pero que no llega al nivel que nos tiene acostumbrados Codemasters, esto se debe a que el título pierde competitividad para ser menos exigente, abusando de escenarios reciclados del Dirt 3, para colmo el juego pierde la vista de interior que si pudimos disfrutar en su predecesor.


En resumen estamos ante un juego que esta a la sombra de otros grandes. Un juego que pierde su mejor baza que es el online. Un juego al que no le falta variedad de pruebas pero puede llegar a ser repetitivo en algunos momentos.


Dirt Showdown cuenta con un integración total en steam, esto incluye tanto el sistema de logros, multijugador, soporte para mandos y steam cloud para guardar nuestras partidas online. Mención especial en esto último porque estás no serán compatibles entre Linux y Windows, para cada SO tendremos que crear una campaña diferente.

Si quieres comprar **DIRT Showdown** queremos advertirte de que ya no es posible hacerlo desde la tienda de Steam, pero está disponible en la tienda que normalmente vende los juegos de Virtual Programming, **[deliver2.com](http://www.deliver2.com/index.php?route=product/product&product_id=106)**.


Este artículo ha sido escrito por un invitado (**yodefuensa**). Si quieres enviarnos tus artículos puedes hacerlo desde [este enlace](index.php/contacto/envianos-tu-articulo).

