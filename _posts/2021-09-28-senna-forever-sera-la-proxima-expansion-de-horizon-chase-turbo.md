---
author: leillo1975
category: Carreras
date: 2021-09-28 14:30:20
excerpt: "<p>&nbsp;<span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\"\
  >@AquirisGS presenta oficialmente esta DLC y anuncia su pr\xF3xima salida.</span></p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HorizonChaseTurbo/SennaForever/Hero_Senna_FOREVER.webp
joomla_id: 1369
joomla_url: senna-forever-sera-la-proxima-expansion-de-horizon-chase-turbo
layout: post
tags:
- dlc
- horizon-chase-turbo
- aquiris
- senna-forever
title: "\"SENNA FOREVER\" ser\xE1 la pr\xF3xima expansi\xF3n de Horizon Chase Turbo"
---
 @AquirisGS presenta oficialmente esta DLC y anuncia su próxima salida.


 Tres añazos han pasado desde que [analizamos en nuestra web]({{ "/posts/analisis-horizon-chase-turbo" | absolute_url }}) este **arcade de carreras de coches** que tanto ha dado que hablar, y que tan merecido éxito ha tenido. También si recordais , por estas fechas, pero hace dos años también os hablamos de la última DLC, [Summer Vibes]({{ "/posts/summer-vibes-sera-la-proxima-dlc-de-horizon-chase-turbo" | absolute_url }}). Ahora volvemos a vosotros para anunciaros que **el próximo 20 de Octubre**,  [Aquiris](https://www.aquiris.com.br/en/), pondrá a la venta lo que seguro que es un producto muy especial tanto para ellos como brasileños, como para todos los aficionados al motor, y podeis ver justo aquí debajo en el siguiente tweet:



> 
> Some emotions are forever. 🏎️🌧️  
>   
> This is a homage to the legendary driver Ayrton Senna. An expansion inspired by the greatest moments of his career.   
>   
> 🏁[#SennaForever](https://twitter.com/hashtag/SennaForever?src=hash&ref_src=twsrc%5Etfw) launches on October 20th. [pic.twitter.com/wLeBvOOMy2](https://t.co/wLeBvOOMy2)
> 
> 
> — Horizon Chase 🏁 #SennaForever (@HorizonChase) [September 27, 2021](https://twitter.com/HorizonChase/status/1442459012914507779?ref_src=twsrc%5Etfw)



 La compañía brasileña rinde así su peculiar homenaje a este astro del automovilismo, que **para muchos ha sido el mejor piloto de todos los tiempos**. Presenta un **conjunto completamente nuevo de vehículos, pistas y características inspiradas en la carrera de Senna**. Según la descripción oficial, en esta expansión encontraremos el siguiente contenido:


***-Modo Carrera para un jugador**: sigue los pasos del legendario piloto brasileño a través de cinco nostálgicos capítulos.*


***-Nueva vista en primera persona** (específica de Senna Forever): siente el calor del momento desde el interior de la cabina.*


***-Nuevas mecánicas de estrategias de carrera** (específicas de Senna Forever): toma decisiones difíciles y elige la configuración de tu vehículo teniendo en cuenta cada pista y las condiciones climáticas.*


***-130 Records de Senna en el modo Carrera**: persíguelas para lograr los resultados históricos del piloto.*


***-Modo Campeonato con 18 equipos diferentes para elegir y más de 30 bólidos para desbloquear**: Domina el circuito mundial de carreras a través de sus tres categorías diferentes.*


***-Juega solo o con hasta 4 jugadores en modo local**: Siente la experiencia de enfrentarte a carreras únicas en cada Campeonato con la imprevisibilidad de las condiciones meteorológicas, las pistas y los rivales generados de forma aleatoria (modo multijugador local no disponible en la versión para dispositivos móviles).*


 ![screenshot 01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HorizonChaseTurbo/SennaForever/screenshot_01.webp)


 


 La expansión tendrá un precio más que atractivo, pues **solo costará 5.99€,** y gracias a la colaboración y el apoyo de Senna Brands, **parte de las ganancias obtenidas con la licencia de Senna Forever se dedicará a contribuir a los programas educativos del Ayrton Senna's Institute.** Como podeis ver no le faltan alicientes para que todos los fans del piloto estén contentos. Nosotros desde JugandoEnLinux os seguiremos informando sobre ella, cuando se produzca el lanzamiento oficial, y **os ofreceremos en exclusiva contenido multimedia** en nuestro canal de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) proximamente. Antes de cerrar este artículo nos gustaría, una vez más, darle las gracias a Jesús Fabre por facilitarnos la información y materiales utilizados para la elaboración de esta noticia. Os dejamos con el trailer del anuncio, que como veis apela a la nostalgia de muchos de nosotros:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/OOMwmwbarP4" title="YouTube video player" width="780"></iframe></div>


 


¿Habeis jugado ya a Horizon Chase Turbo? ¿Qué os parece esta DLC homenajeando al gran Airton Senna? Si quieres decir que te parece esta expansión o tienes cualquier pregunta sobre ella, hazlo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux) o nuestras salas de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org).

