---
author: leillo1975
category: Plataformas
date: 2022-03-31 14:14:46
excerpt: "<p>@BULLWAREsocial y SpoonBox Studio, los creadores de @<span class=\"highlight\"\
  >Hack</span><span class=\"highlight\">And</span><span class=\"highlight\">Slime,\
  \ la han lanzado hace unos d\xEDas.</span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HackAndSlime/HackandSlime03.webp
joomla_id: 1454
joomla_url: hack-and-slime-ya-tiene-demo-publica-en-steam
layout: post
tags:
- steam
- alpha
- itch-io
- hack-and-slime
- spoonbox
- bullware-soft
- construct
title: "\"Hack and Slime\" ya tiene demo p\xFAblica en Steam"
---
@BULLWAREsocial y SpoonBox Studio, los creadores de @HackAndSlime, la han lanzado hace unos días.


 Si recordais, hace un par de meses [os comenzamos a hablar]({{ "/posts/el-videojuego-espanol-hack-and-slime-tendra-version-linux-nativa-a2" | absolute_url }}) de este **videojuego español con aspecto retro**. Desarrollado con el motor de videojuegos  [Construct](https://www.construct.net/en) por las compañías [Bullware Soft](https://bullwaresoft.com/) y [SpoonBox](https://www.spoonboxstudio.com), este título de **Acción 2D** mezcla diferentes géneros como las **plataformas**, el **rol**, o incluso el **Metroidvania**. Hasta ahora podíais disfrutar de su demo en formato Pre-Alpha desde [Itch.io](https://bullwaresoft.itch.io/hackandslime), pero desde hace unos días se puede encontrar en "la madre" de todas las tiendas digitales, Steam, y pasando a ser **Alpha** "a secas".


En este versión "[Alpha](https://store.steampowered.com/news/app/1773700/view/3128317225043928468)" de la demo vas a encontrar las siguientes características:


*- 6 niveles completamente jugables.  
- 1 Quest principal completa.  
- Sistema de progresión con experiencia y niveles de personajes.  
- Puntos de atributo personalizables y sistema de estadísticas.  
- Árbol de habilidades basado en puntos por nivel.  
- Habilidades activas, pasivas y de apoyo.  
- 10 tipos de enemigos con comportamientos diferentes.  
- Generación aleatorio de Cofres, Templos, pozos.  
- Sistema de Bufos y Debufos con más de 12 modificadores.  
- Loot de oro de enemigos y otros elementos.  
- Objetos destructibles en los niveles con posibilidades aleatorias.  
- Sistema de warp portals y town portals.  
- Primer Boss "The Slime King" completamente disponible para ser derrotado.  
- Humble Village ahora tiene: Herrería, Taberna, Santuario, Instructor y Elevador, con sus personajes y funciones activadas.  
- Un nuevo personaje (npc) aún no revaldo.*


Como podeis ver teneis una fantástica oportunidad de disfrutar de un adelanto de este juego, ahora desde el cliente de Steam, y por supuesto con **soporte nativo**. Nosotros ya pudimos saborear sus mieles hace algún tiempo, tal y como podeis ver en el siguiente video (el contenido actual de la demo ya ha variado desde entonces):


 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/AQ7QySR7oKU" title="YouTube video player" width="780"></iframe></div>


 Podeis encontrar la demo Alpha de Hack and Slime en [Steam](https://store.steampowered.com/app/1773700/Hack_and_Slime/),  o si lo preferís en [Itch.io](https://bullwaresoft.itch.io/hackandslime).

