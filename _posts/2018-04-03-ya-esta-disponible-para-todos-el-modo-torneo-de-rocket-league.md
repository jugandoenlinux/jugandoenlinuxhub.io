---
author: Serjor
category: Carreras
date: 2018-04-03 20:59:23
excerpt: <p>Se avecina el famoso torneo de Rocket League de jugandoenlinux.com</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/90c8a860751b252e58c26f97e05856c4.webp
joomla_id: 701
joomla_url: ya-esta-disponible-para-todos-el-modo-torneo-de-rocket-league
layout: post
tags:
- torneo
- rocket-league
- psyonix
title: "Ya est\xE1 disponible para todos el modo torneo de Rocket League"
---
Se avecina el famoso torneo de Rocket League de jugandoenlinux.com

Vía actualización de steam nos enteramos de que los torneos de Rocket League ¡¡ya están [aquí](http://store.steampowered.com/news/externalpost/steam_community_announcements/2383968623915969054)!!


Y es que la versión 1.43 nos trae, aparte de correcciones y mejoras, un modo torneo, en el que organizar todo el torneo de principio a fin. La verdad es que leyendo las notas de la versión, el modo torneo está bastante cuidado:


* Mínimo 8 equipos y máximo 128
* Cualquier judador puede crear un torneo, y además definir si es público o privado
* Los torneos permiten juego cruzado entre plataformas
* Se puede definir los niveles máximo y mínimo de los jugadores


Al margen del modo torneo también han implementado baneos. No sé cómo de tóxica es la gente en Rocket League, pero poder votar para banear a alguien es algo triste pero necesario. También han mejorado el tema de la detección y notificación de los problemas de conexión (que no los problemas de conexión en sí), así que ahora tendremos más info cuando se de algún problema.


Y ahora sí que no tenemos excusa. Llevamos tiempo intentando montar una liga Rocket League en el foro, pero no ha habido manera, así que este gestor de torneos nos viene que ni pintado.


Si te interesa participar en un torneo de Rocket League con la gente de [jugandoenlinux.com](https://jugandoenlinux.com), pásate por este [hilo](index.php/foro/foro-general/28-liga-rocket-league) y apúntate. Si todo va bien el tema de los torneos será algo recurrente, así que no importa cuando veas este artículo, aunque pienses que ya ha pasado tiempo, anímate, quizás nos ayudes a retomar el tema ;-)


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/4R7SzPOKKCM" width="560"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/252950/30948/" style="border: 0px;" width="646"></iframe></div>


Y tú, ¿te vas a apuntar al torneo? Pásate por el hilo del foro para dejar tu nombre, y también pásate por los canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD) para que nos organicemos.

