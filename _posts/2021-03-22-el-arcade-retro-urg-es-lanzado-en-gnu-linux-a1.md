---
author: leillo1975
category: Arcade
date: 2021-03-22 09:22:29
excerpt: "<p><span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@UtoposGames</span>\
  \ corrige un error en la demo.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/URG/urg-logo.webp
joomla_id: 1290
joomla_url: el-arcade-retro-urg-es-lanzado-en-gnu-linux-a1
layout: post
tags:
- arcade
- retro
- unity3d
- remake
- atari
- urg
- utopos-rocketship-game
title: El arcade retro URG es lanzado en GNU-Linux (ACTUALIZADO)
---
@UtoposGames corrige un error en la demo.


**ACTUALIZADO 23-3-21:**   
Se ha corregido el problema con el ejecutable de la demo en Linux, por lo que ya podeis probar sin problemas el juego y juzgar por vosotros mismos.





---


**NOTICIA ORIGINAL 22-3-21:**  
 Hace unos días, dándome mi "garbeo" diario por ese mundo aparte que es reddit, encontré una [publicación de un desarrollador](https://www.reddit.com/r/linux_gaming/comments/m7uj9h/should_i_release_my_game_urg_for_linux/) el cual **preguntaba a los jugadores de GNU-Linux si debería otorgar soporte a su juego**. Por supuesto la respuesta por parte de la comunidad no podía ser mayoritariamente otra diferente a que lo publicase de forma nativa en nuestros sistemas operativos, y muy especialmente teniendo en cuenta que previamente lo había publicado en la consola [ATARI VCS]({{ "/tags/atari" | absolute_url }}), siendo **el juego más vendido de esa plataforma** (para quien no lo sepa, esta consola funciona con una distribución Linux personalizada basada en Debian).


Raudos y veloces, en la compañía [Utopos Games](https://www.utoposgames.com/) se pusieron manos a la obra y en tiempo record han lanzado el juego con soporte nativo para nuestro sistema, **lanzando incluso una demo para quien quiera probarlo previamente**. Para quien no conozca esta compañía afincada en Las Vegas, hay que decir que su fundador y CEO, el finlandés **Jani Penttinen**, lleva la friolera de 27 años en el mundillo del videojuego, habiendo trabajado previamente en EA, Westwood Studios, Housemarque, Remedy Entertainment, e incluso fundado una compañía de juegos para móviles en China.


![](https://cdn.cloudflare.steamstatic.com/steam/apps/1195140/ss_50b5aba167f87673b872fb4673a22c59dd387fca.1920x1080.jpg)


URG ([Utopos Rocketship Game](https://www.utoposgames.com/urg)) es un remake de otro juego, [Utopos](https://youtu.be/PbfX7F2YU28), lanzado para **Atari ST** hace casi 30 años por el mismo desarrollador, y su vuelta al ruedo después de un **proceso de desarrollo de 3 años**. El juego es un **arcade de disparos y desplazamiento lateral al estilo retro** con unos potentes **gráficos en 3D** y una buena dosis de acción y grandes explosiones. Permite el juego simultaneo a **pantalla compartida de hasta 4 jugadores** y es compatible con [Remote Play Together](index.php/component/k2/1-blog-principal/noticia/1240-steam-anadira-una-nueva-caracteristica-para-jugar-juegos-con-multijugador-local-llamada-remote-play-together) de Steam. Pilotarás una nave espacial en un campo de asteroides, realizando misiones para recoger objetos y resolver puzzles, mientras disparas a los enemigos y haces explotar asteroides. El juego está dividido en **varios mundos temáticos**, cada uno con diferentes niveles. A medida que el juego avanza, se desbloquean nuevos mundos y cada uno de ellos añade nuevos elementos de juego.


Si quieres premiar el esfuerzo de este desarrollador por traer su trabajo a nuestro sistema y disfrutar de un divertido juego de disparos como los de antes, puedes comprar URG actualmente con un interesantísimo **descuento del 25%** en Steam al **fantástico precio de 12,59€:**


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1195140/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Os dejamos con un video hecho exclusivamente para el lanzamiento en Linux del URG:

  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/5q7tfJaTe1Y" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


 Podeis dejarnos vuestras impresiones y opiniones sobre el lanzamiento de URG en Linux en los comentarios en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

