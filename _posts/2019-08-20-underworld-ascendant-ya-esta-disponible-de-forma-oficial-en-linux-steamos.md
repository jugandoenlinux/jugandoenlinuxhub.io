---
author: Pato
category: Rol
date: 2019-08-20 20:31:11
excerpt: <p>El esperado juego de @Underworld_Asc llega por fin tras varios retrasos</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f564c1422f75d8af61b6191b15b3db80.webp
joomla_id: 1100
joomla_url: underworld-ascendant-ya-esta-disponible-de-forma-oficial-en-linux-steamos
layout: post
tags:
- accion
- indie
- rol
title: "Underworld Ascendant ya est\xE1 disponible de forma oficial en Linux/SteamOS"
---
El esperado juego de @Underworld_Asc llega por fin tras varios retrasos

Tras varios retrasos por el camino, hoy nos enteramos gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/underworld-ascendants-linux-port-has-now-been-released.14832) que el port del esperado **Underworld Ascendant**, de los creadores de la excelente saga Ultima Underworld ya está disponible para disfrutarlo en nuestro sistema favorito.


Underworld Ascendant es un "dungeon crawler" donde las decisiones del jugador son cruciales en las batallas que tendremos que afrontar en el "abismo Stygian".


*"Underworld Ascendant es un RPG de acción diseñado para maximizar las elecciones del jugador.*


*Adéntrate en terrenos desconocidos. Da rienda suelta a tu potencial creativo. Aprovecha el entorno para desequilibrar la balanza a tu favor. Planifica de antemano el plan de batalla ideal o lánzate a la aventura e improvisa sobre la marcha.*


*Cada decisión acarrea grandes oportunidades... pero también graves consecuencias. Lo que hagas a tu paso tendrá repercusiones en las vidas de los demás, que no olvidarán tus acciones. Siempre y cuando sobrevivas..."*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/-sX3wsh02fs" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


De este modo el estudio cumple la promesa desde Kickstarter de traer el juego a Linux/SteamOS de forma oficial. En el momento de escribir este artículo no hay disponible en su página de características información sobre los requisitos recomendados para mover el juego.


Si quieres saber mas sobre Underworld Ascendant puedes visitar [su página web](https://www.underworldascendant.com/) o comprarlo en [Humble Bundle](https://www.humblebundle.com/store/underworld-ascendant?=jugandoenlinux) (enlace patrocinado) o [en Steam](https://store.steampowered.com/app/692840/Underworld_Ascendant/?snr=1_7_15__13).

