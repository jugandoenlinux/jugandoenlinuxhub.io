---
author: leillo1975
category: Deportes
date: 2018-08-06 16:25:15
excerpt: <p class="ProfileHeaderCard-screenname u-inlineBlock u-dir" dir="ltr"><a
  class="ProfileHeaderCard-screennameLink u-linkComplex js-nav" href="https://twitter.com/milesSI"><span
  class="username u-dir" dir="ltr">@milesSI</span></a><span class="username u-dir"
  dir="ltr"> director </span>de <a class="ProfileHeaderCard-screennameLink u-linkComplex
  js-nav" href="https://twitter.com/SI_games"><span class="username u-dir" dir="ltr">@SI_games</span></a><span
  class="username u-dir" dir="ltr"> (</span><a class="ProfileHeaderCard-screennameLink
  u-linkComplex js-nav" href="https://twitter.com/SEGA_Europe"><span class="username
  u-dir" dir="ltr">@SEGA_Europe</span></a><span class="username u-dir" dir="ltr">)
  lo ha anunciado en twitter.<b class="u-linkComplex-target"><br /></b></span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c86ae9bf9d69e790910600aa8bced4fe.webp
joomla_id: 820
joomla_url: malas-noticias-football-manager-2019-no-tendra-version-para-gnu-linux-steamos
layout: post
tags:
- manager
- football-manager
- sega
- sports-interactive
title: "Malas noticias: Football Manager 2019 no tendr\xE1 versi\xF3n para GNU-Linux/SteamOS"
---
[@milesSI](https://twitter.com/milesSI) director de [@SI_games](https://twitter.com/SI_games) ([@SEGA_Europe](https://twitter.com/SEGA_Europe)) lo ha anunciado en twitter.

Esta mañana, gracias de nuevo [GamingOnLinux](https://www.gamingonlinux.com/articles/football-manager-2019-announced-and-sadly-its-not-coming-to-linux.12287), nos llegaba esta **funesta información** por parte del máximo responsable de [Sports Interactive Games](http://www.sigames.com/), [Miles Jacobson](https://twitter.com/milesSI), de que el más conocido mánager de Fútbol de los últimos años solo tendría soporte para Windows y Mac. En relación a la pregunta de por que no estaba incluido Linux en las plataformas soportadas la respuesta ha sido la siguiente:



> 
> cost/benefit analysis. It unfortunately wasn't selling enough on Linux to cover the QA costs, let alone the dev cost. :(
> 
> 
> — Miles Jacobson (@milesSI) [6 de agosto de 2018](https://twitter.com/milesSI/status/1026406026780856320?ref_src=twsrc%5Etfw)



 Lo que traducido significa que analizando el coste/beneficio, las ventas de Linux no eran suficientes para afrontar los gastos de los costes de calidad, y mucho menos los de desarrollo. Buscando un poco más de luz en el asunto y tratando de buscar un mínimo resquicio de esperanza o posibilidad Miles Jacobson fué muy claro contestando a nuestro mensaje:



> 
> it is absolutely final.
> 
> 
> — Miles Jacobson (@milesSI) [August 6, 2018](https://twitter.com/milesSI/status/1026459930432335875?ref_src=twsrc%5Etfw)



La noticia  es un aunténtico jarro de agua fria no solo para todos los amantes de los juegos de gestión y/o futbol, sinó también para todos aquellos que tenemos como plataforma de juegos a GNU-Linux. Ver como compañías que año a año renovaban el compromiso de portar sus juegos a nuestro sistema, y ahora declinan el soporte a nuestra plataforma es muy desalentador, **especialmente cuando se trata de superventas como esta franquicia**, lo cual aún es más extraño. Aunque cierto es también que muy poca gente está en esto por amor al arte, y si a pesar de tratarse de un título tan conocido y aclamado, no logra cubrir gastos, **es completamente legítimo** que nos dejen de lado.


Esperemos que esta situación no se siga produciendo en un futuro y que más compañías no sigan el mismo rumbo que Sports Interactive Games.... pero bueno, para eso ya sabeis que **solo se puede hacer una cosa, y esa es seguir usando nuestro sistema operativo favorito y apoyar el esfuerzo que en muchos casos realizan los desarrolladores comprando sus juegos**. También recordaros que si quereis adquirir la versión 2018 de Football Manager, que si que tiene soporte, podeis hacerlo en la [Humble Store](https://www.humblebundle.com/store/football-manager-2018?partner=jugandoenlinux) (enlace patrocinado)


 


Ahora queremos conocer vuestra opinión sobre Football Manager y todo este tema, por lo que podeis dejar vuestros mensajes en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

