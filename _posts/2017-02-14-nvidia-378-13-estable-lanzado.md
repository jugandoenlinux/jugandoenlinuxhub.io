---
author: leillo1975
category: Hardware
date: 2017-02-14 19:42:19
excerpt: "<p>En el d\xEDa de hoy, Nvidia ha lanzado una nueva versi\xF3n de driver\
  \ para Linux, concretamente la versi\xF3n 378.13. Tras pasar por un periodo de beta\
  \ p\xFAblica, pasa a ser estable la rama 378, que tiene&nbsp;,a parte de las t\xED\
  picas correcciones y ajustes, como principales caracter\xEDsticas las siguientes:</p>\r\
  \n<p>&nbsp;</p>\r\n<p>-Soporte para m\xE1s tarjetas profesionales Quadro</p>\r\n\
  <p>-Se a\xF1ade soporte para Xorg-server 1.19 (ABI 23)</p>\r\n<p>-Se a\xF1ade soporte\
  \ para la extensi\xF3n&nbsp; \"ARB_parallel_shader_compile\" para permitir compilaci\xF3\
  n multihilo de los shaders GLSL.</p>\r\n<p>-A\xF1adido soporte en los ajustes de\
  \ nvidia (\"nvidia-settings\") para ver los monitores Prime configurados.</p>\r\n\
  <p>-Hay actualizaciones en el driver de Nvidia EGL.</p>\r\n<p>-Y la que me parece\
  \ m\xE1s importante, quedan activadas las&nbsp; optimizaciones de procesos OpenGL\
  \ por defecto en el driver y desactivadas bajo Xinerama.</p>\r\n<p>&nbsp;</p>\r\n\
  <p>Si quereis ver con m\xE1s detalle todos los cambios podeis consultar la lista\
  \ en la<span style=\"color: #000080;\"> <a href=\"http://www.nvidia.com/download/driverResults.aspx/115031/en-us\"\
  \ target=\"_blank\" style=\"color: #000080;\">web de Nvidia</a></span>. Tambi\xE9\
  n podeis descargar el driver desde la p\xE1gina de nvidia, o esperar un dia o dos\
  \ a que lo cuelguen en el <span style=\"color: #000080;\"><a href=\"https://launchpad.net/~graphics-drivers/+archive/ubuntu/ppa\"\
  \ target=\"_blank\" style=\"color: #000080;\">repositorio oficial de Ubuntu (Graphics\
  \ Drivers Team)</a></span></p>\r\n<p>&nbsp;</p>\r\n<p>FUENTES: <a href=\"http://www.phoronix.com/scan.php?page=news_item&amp;px=NVIDIA-378.09-Beta-Linux\"\
  \ target=\"_blank\">Phoronix</a>, <a href=\"http://www.nvidia.com/download/driverResults.aspx/115031/en-us\"\
  \ target=\"_blank\">Nvidia</a></p>\r\n<p>&nbsp;</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/23f6a067599ae98276b159b7685c0abf.webp
joomla_id: 219
joomla_url: nvidia-378-13-estable-lanzado
layout: post
tags:
- nvidia
- '37813'
- driver
title: Nvidia lanza los drivers 378.13 estables
---
En el día de hoy, Nvidia ha lanzado una nueva versión de driver para Linux, concretamente la versión 378.13. Tras pasar por un periodo de beta pública, pasa a ser estable la rama 378, que tiene ,a parte de las típicas correcciones y ajustes, como principales características las siguientes:


 


-Soporte para más tarjetas profesionales Quadro


-Se añade soporte para Xorg-server 1.19 (ABI 23)


-Se añade soporte para la extensión  "ARB_parallel_shader_compile" para permitir compilación multihilo de los shaders GLSL.


-Añadido soporte en los ajustes de nvidia ("nvidia-settings") para ver los monitores Prime configurados.


-Hay actualizaciones en el driver de Nvidia EGL.


-Y la que me parece más importante, quedan activadas las  optimizaciones de procesos OpenGL por defecto en el driver y desactivadas bajo Xinerama.


 


Si quereis ver con más detalle todos los cambios podeis consultar la lista en la [web de Nvidia](http://www.nvidia.com/download/driverResults.aspx/115031/en-us). También podeis descargar el driver desde la página de nvidia, o esperar un dia o dos a que lo cuelguen en el [repositorio oficial de Ubuntu (Graphics Drivers Team)](https://launchpad.net/~graphics-drivers/+archive/ubuntu/ppa)


 


FUENTES: [Phoronix](http://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-378.09-Beta-Linux), [Nvidia](http://www.nvidia.com/download/driverResults.aspx/115031/en-us)


 

