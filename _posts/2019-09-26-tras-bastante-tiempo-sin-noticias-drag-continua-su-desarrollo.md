---
author: leillo1975
category: Carreras
date: 2019-09-26 11:07:50
excerpt: <p>Sus creadores, Thorsten Folkers, siguen mejorando el juego.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/621ee1cee67b7f5dc5016ca5762406c5.webp
joomla_id: 1114
joomla_url: tras-bastante-tiempo-sin-noticias-drag-continua-su-desarrollo
layout: post
tags:
- drag
- thorsten-folkers
title: Tras bastante tiempo sin noticias, DRAG continua su desarrollo.
---
Sus creadores, Thorsten Folkers, siguen mejorando el juego.

Si recordais, hace ya bastante tiempo que [os hablamos por primera vez de este proyecto](index.php/homepage/generos/carreras/2-carreras/735-pronto-veremos-drag-un-juego-de-carreras-en-acceso-anticipado). Haciendo memoria sabreis que se trata de un juego de carreras desarrollado por dos hermanos alemanes que han **creado su propio motor gráfico bajo Linux**, programando en C++ y con una interfaz Python para scripting. En el podremos jugar en modo de un jugador, así como un multijugador tanto local (pantalla partida) como el tan necesario hoy en día Online.


Aunque hace más de un año que en teoría tendría que salir en Steam con Acceso Anticipado, el proyecto, aunque lentamente, parece que avanza con buenos cimientos, a tenor de lo mostrado en su cuenta de [Polycount](https://polycount.com/discussion/183339/ong-offroad-racer-indie-game-dev-log/p3). Haciendo un poco de resumen desde [la última vez que tuvimos un reporte de su trabajo](index.php/homepage/generos/carreras/2-carreras/1058-el-desarrollo-de-drag-continua-con-importantes-novedades), en esta ocasión nos han reportado que:


*-Ahora disponen de un **sistema completamente nuevo para pintar el follaje del suelo y los escombros**. El anterior sistema era adecuado pora objetos y árboles, pero no para la hierba o el suelo. El nuevo sistema funciona definiendo en primer lugar hasta cuatro mezclas diferentes de mallas. Una podría contener varias piedras para hacer un escombro interesante y otra tiene hierba pequeña y así sucesivamente.*


*-Por fin disponen de un **nuevo tonemapper** que no sólo maneja correctamente la luma sino también el croma. Esto puede verse cuando una fuente de luz de color altamente saturada se vuelve muy brillante. En ese caso el color se vuelve blanco debido a la sobre saturación de los tres conos sensibles al color en sus ojos. Los tres conos RGB se solapan y son al menos algo sensibles en cada posición del espectro de color. Fué necesario rehacer los LUTs para el nuevo tonemapping. Las sombras eran demasiado calientes, así que se ajustó el color ambiental y el tragaluz para lograr una iluminación general mucho mejor.*


*![Accion](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DRAG/Accion.webp)*


*-**Se ha añadido una nueva pista al juego**, tanto en el modo de un jugador, como en el multijugador, y se trata del bioma Antártico / Offshore es un entorno modular de plataformas encima de un paisaje nevado, donde el trabajo que se ha realizado en él permite ver la pista  a gran distancia.*


*![NuevaPista](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DRAG/NuevaPista.webp)*


*- A raiz de la creación de esta última pista, los desarrolladores se encontraron con un **problema con la actual forma de implementar las sombras**, debido a que esta pista tiene más de 2 Km de longitud sin obstaculos, lo que cargaba demasiado el sistema. Ahora se renderizan dinámicamente a la distancia de la vista, de forma que lo que no vemos no se genera y por lo tanto no impacta en el rendimiento.*


*-Se utiliza una técnica inspirada en Forza Horizon 4 que utiliza **dos versiones diferentes de las texturas del terreno**, usando texturas cercanas mucho más detalladas y lejanas más ligeras, de forma que se puede mejorar el aspecto de los objetos cercanos dándole una apariencia más orgánica y natural.*


*-**Garaje**: Creada una primera iteración de la escena de selección / personalización de coches. La interfaz de usuario sigue siendo un "arte" de programador:*


*-**Vulkan**: Se está trabajando en un backend vulkan para el renderizador. Ya se puede empezar el juego y renderizar completamente el menú y el vídeo en vulkan. El juego en sí sólo rinde parcialmente y todavía queda mucho trabajo por hacer. Esto no es crítico, pero se pretende estar al día en esto de todos modos.*


*-**Árboles**: al comprobar las visualizaciones del buffer se han dado cuenta de que podían mejorar aún más los árboles aumentando la profundidad con la oclusión interna del árbol. Se ajustaron los valores de albedo a niveles realistas y se añadió información de cavidades a los canales alfa de los vértices en todos los árboles.*


*-Mejoras físicas*


*-Pantalla dividida local habilitada de nuevo*


*-Se han creado **paredes invisibles animadas** para delimitar las pistas:*


*![Paredes](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DRAG/Paredes.webp)*


*-**Acumulación de suciedad en los vehículos:** Se ha pintado a mano máscaras de suciedad para los coches en Substance Painter y se ha fabricado un material de suciedad "tileable". Dependiendo de la superficie del suelo, el barro se acumula en el coche con el tiempo. Una característica especial que se espera que se pueda apreciar es la abrasión del barro de la superficie de la banda de rodadura del neumático al volver a conducir sobre el asfalto. Las paredes laterales quedarán embarradas como se ve en la segunda parte de la animación de abajo. Todo esto se logra con una sola máscara y algunos scripts inteligentes:*


*![barro](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DRAG/barro.webp)*


En cuanto a la fecha de salida no se ha aclarado nada. Unicamente se ha comunicado que el motivo del restraso está causado por el cambio de planes. Esperemos que a partir de ahora todo vaya bien, y al menos tengamos noticias más frecuentes de este interesantísimo proyecto.


Apasionante este DRAG, ¿verdad? ¿Qué te parecen estas últimas actualizaciones? ¿Te parece una propuesta interesante esta de los [Thorsten Folkers](https://tfolkers.artstation.com/)? Puedes dejar vuestras impresiones sobre este  juego en los comentarios de la noticia,  o vuestros mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

