---
author: Pato
category: Arcade
date: 2017-11-08 17:38:05
excerpt: <p>Lucha y mamporros arcade en entornos 2D&nbsp;</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/24fae0cf4e190078d5b9896e00870cd9.webp
joomla_id: 522
joomla_url: slashers-the-power-battle-ya-esta-disponible-en-linux-steamos-en-acceso-anticipado
layout: post
tags:
- accion
- indie
- arcade
- 2d
- lucha
title: "'Slashers: The Power Battle' ya est\xE1 disponible en Linux/SteamOS en acceso\
  \ anticipado"
---
Lucha y mamporros arcade en entornos 2D 

No es facil encontrar nuevos títulos de lucha arcade para nuestro sistema favorito. Tras el fiasco de supone el no saber nada sobre Street Fighter (Capcom... ¿hay alguien ahi?...) nos llega este juego de lucha arcade en 2D.


'Slashers: The Power Battle' [[web oficial](http://www.stun-games.com/)] es un juego de acción arcade con claras influencias de otros títulos clasicos del género como Street Fighter 3, Last Blade, Samurai Showdown o Guilty Gear. 


Podrás elegir entre 11 personajes (ampliables mediante DLC) cada uno con su estilo propio de juego y habilidades. Podrás personalizar a tu personaje, entrenar en un modo tutorial, descubrir su historia en el modo Arcade o luchar contra otros jugadores ya sea en local en el modo versus, o de todo el mundo en el modo "Net Play".


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/0DROg9vVbVM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


'Slashers: The power Battle' está aún en acceso anticipado por lo que puede presentar bugs o problemas varios. Si aun así quieres jugarlo e incluso ayudar a mejorar dando "feedback" a los desarrolladores, lo tienes disponible en su página de Steam con un 15% de descuento. Eso sí, no está traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/361100/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


¿Qué te parece este 'Slashers: The Power Battle'? ¿Piensas machacar los botones de tu gamepad?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

