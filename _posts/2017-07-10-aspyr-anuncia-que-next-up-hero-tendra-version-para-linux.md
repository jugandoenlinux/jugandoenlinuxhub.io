---
author: Serjor
category: "Acci\xF3n"
date: 2017-07-10 18:46:00
excerpt: "<p>Aspyr acaba de anunciar que estar\xE1 disponible en nuestros sistemas\
  \ esta primavera</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b5b56b2ae93d3dc958cf0c21c9383b18.webp
joomla_id: 403
joomla_url: aspyr-anuncia-que-next-up-hero-tendra-version-para-linux
layout: post
tags:
- aspyr-media
- early-access
- next-up-hero
title: "Aspyr anuncia que Next Up Hero tendr\xE1 versi\xF3n para Linux (actualizado)"
---
Aspyr acaba de anunciar que estará disponible en nuestros sistemas esta primavera

**(Actualización)**


Ya dijimos que Next Up Hero sería exclusivo de Windows durante su etapa de acceso anticipado, y que estaba previsto su lanzamiento en algún momento de 2017. Pues bien, con un poco de retraso y [tras añadir](http://steamcommunity.com/games/667810/announcements/detail/1647625036737058427) a un nuevo héroe y algunos cambios en el apartado cooperativo para hacerlo más equilibrado y atractivo, Aspyr Media ha anunciado que **Next Up Hero estará disponible para Linux esta próxima primavera** mediante un tweet:



> 
> If you got [@NextUpHero](https://twitter.com/NextUpHero?ref_src=twsrc%5Etfw) from the quiz, no worries! Its Windows only just for Early Access. It's coming to Linux and consoles in the Spring :)
> 
> 
> — Aspyr Media (@AspyrMedia) [February 28, 2018](https://twitter.com/AspyrMedia/status/968847140733816832?ref_src=twsrc%5Etfw)



 Esperemos que la espera merezca la pena y podamos echarle el guante más pronto que tarde. A continuación el trailer:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/v6Fqs-wPPw0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Podeis ver más detalles del juego en el enlace al final del artículo.




---


**(Artículo original)**


Leemos en [gamingonlinux](https://www.gamingonlinux.com/articles/next-up-hero-announced-by-aspyr-media-and-digital-continue-will-support-linux.9957) que el próximo juego de [Digital Continue](http://digitalcontinue.com/), [Next Up Hero,](http://nextuphero.com/) será distribuido por Aspyr Media, la cuál es conocida por portar juegos a Linux y Mac, será lanzado en nuestro sistema preferido.


El juego parece una mezcla de Bastion con Dark Souls, ya que las mecánicas jugables recuerdan mucho al primero, pero según la sinopsis del propio juego, nos prometen que vamos a morir, y mucho. Estas muertes no obstante servirán de ayuda para el próximo héroe que entre en la partida, ya que al morir dejaremos un 'echo' que servirá para invocar a un Antiguo, el cuál nos ayudará en la batalla.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/z2F-nBYWWTE" width="560"></iframe></div>


 


La verdad es que la premisa es interesante, y parece el típico juego de corte casual con el que matar el rato, pero que al final acabas horas enganchado.


Su lanzamiento oficial será a lo largo de este 2017, y por ahora está disponible en Early Access para Windows. Esperemos que la versión para Linux esté disponible pronto, y es de agradecer que esta vez sí Aspyr se haya acordado de nosotros.


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/667810/" width="646"></iframe></div>


 


¿Y a ti qué te parece este juego? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

