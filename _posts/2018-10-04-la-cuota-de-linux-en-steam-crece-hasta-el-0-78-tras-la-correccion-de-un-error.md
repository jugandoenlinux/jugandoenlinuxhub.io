---
author: Pato
category: Noticia
date: 2018-10-04 09:46:54
excerpt: "<p>Linux encadena tres meses de subida cont\xEDnua</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7ad7f11d190c995ba540f7eeb4073ef6.webp
joomla_id: 867
joomla_url: la-cuota-de-linux-en-steam-crece-hasta-el-0-78-tras-la-correccion-de-un-error
layout: post
tags:
- steam
- linux
title: "La cuota de Linux en Steam crece hasta el 0,78% tras la correcci\xF3n de un\
  \ error"
---
Linux encadena tres meses de subida contínua

Cuando hablamos de cuota de usuarios en Steam siempre cogemos los datos un poco con pinzas pues como todo porcentaje y datos basados en encuestas, nunca se puede saber hasta que punto son fiables. Sin embargo lo que si merece la pena es fijarse sobre todo en las "tendencias", y en ese sentido nos hacemos eco de un [tweet de Phoronix](https://twitter.com/phoronix/status/1047663777603342337) en el que explica que debido a un error en la medición, el porcentaje de Steam ha sido corregido al alza.



> 
> Just fixed a small rounding error in the HW survey that was affecting reported Linux usage. Distributions with less than a handful of users weren't getting factored into the total percentage. Numbers for September have just been updated: <https://t.co/kqwtgyzTvk>
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [4 de octubre de 2018](https://twitter.com/Plagman2/status/1047655420020453378?ref_src=twsrc%5Etfw)


  





Las estadísticas de Steam se publican todos los meses desde hace unos años, y ya ha sufrido varias correcciones debido a errores en la medición, como cuando se contabilizaban de más los usuarios de los cibercafés. En un primer momento la encuesta de este mes en concreto, la cuota que Steam daba para Linux era de 0,71%, pero ahora gracias a la corrección del error que contabilizaba mal las distribuciones Linux minoritarias, ese porcentaje **ha subido hasta el 0,78%**.


Mas allá del porcentaje, lo que sí podemos afirmar es que **Linux ha encadenado ya 3 meses consecutivos de subida** en la cuota, y ya nos acercamos al objetivo del 1%. Hay que tener en cuenta que estamos en un momento en que la aparición de proton a mediados del mes de Agosto ha supuesto un antes y un después en cuanto al juego en Linux se refiere, y es posible que estemos ante la consecuencia de los movimientos de Valve en su apuesta por el ecosistema Linux/SteamOS, Proton y Vulkan.


Habrá que ver si se consolida la tendencia y podemos hablar de un crecimiento real de usuarios en Linux, lo cual siempre sería una buena noticia ya que alcanzar una cuota significativa es importante de cara a que los estudios tengan en cuenta a nuestro sistema favorito a la hora de portar sus desarrollos.


Puedes ver la estadística de Steam en este enlace:


<https://store.steampowered.com/hwsurvey/?platform=combined>


¿Qué piensas sobre el crecimiento de la cuota de Linux en Steam? ¿Llegaremos a superar el 1% a corto o medio plazo?


Cuentamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

