---
author: Serjor
category: "Hero Shooter"
date: 24-08-2024 17:00:00
excerpt: "Primeras impresiones de Deadlock, el nuevo Hero Shooter de Valve"
layout: post
tags:
- MOBA
- Deadlock
- Dota2
- Hero Shooter
title: "Deadlock, el juego secreto de Valve menos secreto de todos"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DeadLock_Valve/deadlock.webp
---

La primera regla de Deadlock es que no se habla de Deadlock. Bueno ahora ya sí, que ya nos dejan, así que ¿qué es Deadlock?

## El juego
Vamos a hacer un juego, ¿cómo definiríamos un MOBA? ¿Qué hace que un MOBA sea un MOBA?

Si nos vamos a los ejemplos clásicos, podríamos listar una serie de características comunes a todos ellos:

- Variedad de Héroes, Leyendas o personajes a escoger ✅
- Cada personaje tiene un rol diferente en la partida ✅
- Cada personaje tiene varias habilidades ✅
- Estas habilidades se pueden mejorar según el personaje gana experiencia ✅
- Al menos dos equipos que se enfrentan entre ellos ✅
- Un número, normalmente entre 5 y 6, de jugadores por equipo ✅
- Una base desde la que partir junto con el resto del equipo ✅
- Una tienda en la base en la que comprar objetos y mejoras para el personaje que llevamos ✅
- Varios carriles o líneas por las que avanzar hacia la base del enemigo ✅
- Minions que avanzan por dichas líneas hacia la base enemiga, y que golpean cualquier enemigo que tengan a la vista ✅
- Torres en mitad de las líneas que protegen y retrasan el avance hacia la base enemiga ✅
- La partida la gana el primero en destruir la base enemiga ✅
- La habilidad mecánica del jugador es clave para el gunplay ❌

¿Gunplay? ¿Armas? ¿Tiros? Eso no es lo normal en un MOBA

No, no lo es, y esa es la gracia de este Deadlock.

## Las luces

Deadlock es un Hero Shooter 6vs6, con tintes de MOBA, y la verdad, es sorprendente que este género híbrido no tenga más representación. Quizás la discontinuación Paragon, el MOBA de Epic, frenó en su momento que hubiera más títulos que mezclaran ambos géneros.

Y es que jugando a Deadlock no hemos podido evitar tener la sensación de estar jugando a su hermano mayor Dota 2. Se siente como Dota, se entiende como Dota, pero no se juega como Dota.

![Si tiene hasta jungla](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DeadLock_Valve/jungla.webp "Si tiene hasta jungla")

Cuando nos unimos una partida tenemos que elegir un héroe con el que jugar. La selección de personaje es clave, porque cada uno tiene un set de habilidades diferente. Las habilidades pueden hacer daño físico o daño mágico, y este daño puede ser canalizado en línea recta o en área, también hay habilidades de sanación, o habilidades que por ejemplo sirven para desposicionar (por ejemplo hay un personaje que tiene un gancho, y se puede usar para atrapar a otro personaje, ya sea para sacar a un aliado de una lluvia de balas, o para arrastrarlo al centro de su equipo y llenarle el cuerpo de plomo).

Al comenzar una partida, partimos de la base que tenemos que defender, donde podemos recargar vida si volvemos a ella, y donde encontramos una tienda donde comprar objetos. 

![Visitando la tienda](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DeadLock_Valve/deadlock_shop.webp "En la tienda")

Y mientras avanzamos hacia la base rival, los minions van avanzando en oleadas hacia la base contraria. A diferencia de otros MOBA clásicos, aquí hay healers, que los reconoceremos porque llevan una bandera con una cruz, que nos pueden curar hasta que se termina su carga de curación (la cruz es de color verde, y se va decolorando según cura). Y sí, también tiene minions torreta según van cayendo las torres.

Como Hero Shooter, una cosa muy buena que hace Deadlock es llevarte lo más rápidamente posible a la acción. Las líneas por las que avanzamos tienen railes suspendidos en el aire a los que podemos subirnos y nos llevan rápidamente a la acción, pudiendo, si tenemos la habilidad cargada, aumentar la velocidad por la que nos desplazamos por dicho raíl. Y este es uno de los grandes aciertos del juego, ya que Valve se ha evitado tener que implementar el teletransporte, que a veces es un problema en estos juegos, y de esta manera resuelven el tener que llegar de la base a la acción en caso de que muramos, o tengamos que ir a la tienda si ya no hay tiendas en mitad de la senda (tenemos una tienda junto a cada "torre", pero si nos destruyen la torre, la tienda desaparece).

![Camino de la batalla](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DeadLock_Valve/railes.webp "Camino de la batalla")

*Ya, todo esto es muy bonito, es un MOBA, y seguro que ir ganando experiencia para subir de nivel y todo eso es muy importante, muy bien, pero esto va de pegar tiros, ¿qué tal en este apartado?*

Estamos hablando de Valve:
- Half Life 1, 2 y 3 (eh, Valve, Alyx no cuenta como 3, ¡ya os vale!)
- Counter Strike, en todas sus versiones
- Team Fortress 2
- Portal 1 y 2

Creo que no hay mucho más que decir, Valve sabe hacer juegos de disparos, y pocas compañías les pueden explicar nada sobre este género. Se aleja mucho de CS, no va de este palo, es un juego de acción frenética y divertida, y la jugabilidad es una mezcla de TF2 y algo de Portal, con combates alocados y mucha verticalidad

Y volvemos un poco al comienzo del artículo, ¿cómo definimos un MOBA? Con este Deadlock hay que cambiar un poco la definición clásica. Tiene los mimbres para sentarse a la mesa de su hermano mayor Dota2, y el haber integrado dentro de un Hero Shooter mecánicas de un MOBA es un acierto en toda regla, lo hace muy muy divertido.

## Las sombras

Ahora, ¿qué será de este juego? No podemos evitar pensar en Artifact o Underlords, que tenían muy buena pinta, eran divertidos y entretenidos, pero Valve decidió no seguir apostando por ellos, y nos preguntamos ¿cuánto margen dará Valve a Deadlock antes de echar la persiana?

Hay que tener en cuenta que el juego no ha sido lanzado oficalmente a fecha de publicación de este artículo, está aún en una fase temprana de desarrollo y solamente se puede acceder por invitación, con lo que no sabemos si será un free to play, o habrá que pagar por él, o cómo será el sistema de monetización. Todo hace pensar que será f2p, pero juegos como Helldivers 2 y Concord, que apuestan por un pago inicial, puede que marquen una tendencia en la monetización de este tipo de juegos.

Y otra sombra que se alarga sobre el juego es que después de polémicas y algún baneo, si estamos hablando del juego es porque ahora Valve lo permite, han aflojado las restricciones, y ya se puede hablar de él, ya se pueden compartir imágenes, hacer streaming o subir vídeos a youtube, pero las incógnitas respecto al futuro del juego siguen ahí, oficialmente, el juego no ha salido aún, y no hay comentarios respecto a cuándo saldrá, o ni tan siquiera, si saldrá.

Sombras sobre un un juego con muchas luces, al que sí, le queda camino, y cosas que depurar, pero que el playtesting actual le debería dar datos suficientes para mejorar y evolucionar antes de su lanzamiento oficial.

La lista de personajes que elegir es limitada, la interfaz no está completamente traducida al castellano (aunque una gran parte sí), el tutorial se queda un poco escaso, no queda claro cómo comunicarse con el resto del equipo en caso de no tener voicecomms, faltan mapas y modos de juego (se nos antojaría muy extraño que en un Hero Shooter solamente hubiera un mapa como en Dota), el arte gráfico parecen assets descartados de TF2 en algunos momentos, pero lo dicho, está en fase temprana de desarrollo, nada preocupante _hoy_.

## ¿Y qué tal va en GNU/Linux?

Proton. Da un poco de pena, pero es lógico. Eso sí, en la Steam Deck no va, lo hemos intentado 🤷
Arranca, puedes entrar en el juego, pero al querer entrar en una partida, el juego ya dice que el matchmaking no está implementado para la deck.

En todo caso, si nos olvidamos de que está funcionando con protón, el juego rinde muy bien. En este sentido Valve también suele exprimir muy bien el hardware.

## El ¿veredicto?

Ninguno, estas son las primeras impresiones de un juego que está en desarrollo y evolución constante, al que le queda trabajo, y que no sabemos qué planes tiene Valve en mente para él.

Solamente sabemos que si tienes acceso al juego lo aproveches el tiempo que dure, porque si te gustan los juegos de disparos en tercera persona y el juego competitivo Player Vs Player, de seguro este juego te va a gustar, tiene todos los ingredientes para ello y si Valve lo cuida un poco, puede coronarse como un juego muy interesante, incluso para la escena competitiva, que realmente es a dónde Valve está apuntando.

Y tú, ¿también tienes acceso a Deadlock? Pásate por nuestros canales de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux) para contarnos qué te parece, y no dejes de seguirnos en [Mastodon](https://mastodon.social/@jugandoenlinux/) para estar al día de las novedades