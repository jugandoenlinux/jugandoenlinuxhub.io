---
author: Serjor
category: "Apt-Get Update"
date: 24-11-2024 20:00:00
excerpt: "Resumen semanal de las noticias publicadas durante la semana 47 en Mastodon."
layout: post
tags:
- noticias
- resumen
title: "Resumen de la semana 47 de 2024 y recomendación de un juego, que nos os podéis perder"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Cocoon/Cocoon.webp
---


Esta semana ha sido incluso un poquito más floja que la anterior, así que esta semana, junto con el recopilatorio habitual de toots de nuestra cuenta de [Mastodon](https://mastodon.social/@jugandoenlinux), al final os dejaré una recomendación personal de un juego que funciona perfecto en GNU/Linux, con soporte en Steam Deck, y que es una joyita que merece mucho la pena. Pero antes, vayamos con lo que ha pasado esta semana:

## [Continúa el desarrollo de Valheim](https://mastodon.social/@jugandoenlinux/113516380525895662)

Por un lado, ayer se publicó un nuevo parche que soluciona varios problemas al usar un mando de juego, el teclado virtual, entre otros. Puedes ver la lista de cambios en [este enlace](https://store.steampowered.com/news/app/892970/view/6562402086097518856?l=spanish)

Y, por otro lado, han publicado la primera imagen de Norte profundo, la que será la próxima gran actualización y que, además, ya será la versión 1.0 del juego, aunque tocará esperar para ello. Puedes ver más detalles en [este enlace](https://store.steampowered.com/news/app/892970/view/6562402086097518822?l=spanish)



## [Me estoy empezando a poner nervioso...](https://mastodon.social/@jugandoenlinux/113517175248048961)

Llevamos mucho tiempo pensando que Valve tiene algo nuevo en desarrollo.
Hace bastante los ingenieros de Valve comentaron que un steam controller 2 🎮 sería un proyecto posible y parece que todo apunta a que se viene algo.

The Verge se hace eco de una filtración donde se habla de nuevos mandos. Uno sería el steam controller 2 y otro podría tener relación con unas VR‼

[theverge.com/games/2024/11/19/24300757/valve-steam-controller-2-roy-deckard-leak](https://www.theverge.com/games/2024/11/19/24300757/valve-steam-controller-2-roy-deckard-leak)

Como siempre es una filtración con poco o mucho acierto 🤷🏽‍


## [Mesa se pone las pilas con dispositivos ARM](https://mastodon.social/@jugandoenlinux/113525816238984540)

Tenemos nueva versión de Mesa!! Con la 24.3, tanto la Raspberry Pi 4 y 5 soportan la última versión de Vulkan.

Además, hay mejoras en el soporte de Vulkan y OpenGL en Apple Silicon. Y es que cada vez es mejor el soporte de Linux para el Apple M1.

El listado completo de la nueva versión lo podéis ver en: [docs.mesa3d.org/relnotes/24.3.0.html](https://docs.mesa3d.org/relnotes/24.3.0.html)


## COCCON, una master class del diseño de videojuegos

No sé si conocéis COCOON, que está hecho por uno de los diseñadores principales de Limbo e Inside (Inside no sé, tengo que probarlo, pero Limbo es otra joya), Jeppe Carlsen.
TL;DR: Compradlo, os durará una tarde, y seréis felices. Lo tenéis en Steam [aquí](https://store.steampowered.com/app/1497440/COCOON/)

Es un juego de puzzles, donde todo el juego se resuelve usando un solo botón, además del movimiento del personaje. Los puzzles requieren pensar en varias dimensiones, porque una particularidad del juego es que casi siempre vas con un mundo encima. Sí, con un mundo. Tienes unas zonas donde puedes pasar del mundo en el que estás, al mundo que llevas en la espalda (sí, no tiene sentido, jugadlo, y lo entenderéis), con lo que muchas veces la resolución de un puzzle implica lo que tienes en el escenario actual, y el mundo que llevas a tus espaldas. Además, funciona como un Mario, es decir, cuando va añadir una mecánica nueva, te enseña sin que te des cuenta, con lo que cuando la tienes que poner en práctica, sobretodo con "jefes finales" (no mueres nunca en el juego, simplemente no te dejan avanzar), no sabes cómo, pero ya sabes lo que hay que hacer para superar ese puzzle.

Y poco más, me hacía ilusión recomendaros esta maravilla jugable.

Por lo demás, como siempre, podeis comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).

