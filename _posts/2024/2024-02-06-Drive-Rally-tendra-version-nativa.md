---
author: leillo1975
category: "Carreras"
date: 06-02-2024 12:00:00
excerpt: "Ya puedes probar novedosa y colorida propuesta de @PixelPerfectDud en el SteamNextFest"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DriveRally/DRIVE_Rally_Linux.jpg
layout: post
tags:
  - carreras
  - Drive Rally
  - Unity3D
title: "#Drive Rally tendrá versión nativa"
---

Hacía bastante tiempo que le teniamos echado el ojo a este l**lamativo juego de rallies**, pero es gracias a nuestro colega **@SonLink** que ayer nos recordó que este ya se podía jugar gracias a la **demo publicada en el Steam Next Fest**.

Con una **estética rompedora basada en la animación y el cell shading**, este original título está siendo desarrollado desde Polonia por el **pequeño estudio indie** [Pixel Perfect Dude](https://www.pixelperfectdude.com). Para su creación han usado el versatil **Unity3D**, consiguiendo un impactante y fluido resultado que hará las delicias de los aficionados a los juegos de conducción y/o la estética retro con montones de referencias a los años 90.

![](https://cdn.cloudflare.steamstatic.com/steam/apps/2494780/extras/About_Steam_Legendary_Cars.gif?t=1707212151)

Por lo que hemos podido probar en la **demo nativa** los gráficos se mueven con soltura, siendo la conducción bastante arcade, pero no sin ello dejando de lado lo que se supone que tienen que ser las físicas de un juego basado en los rallies. Aun así **exige una cierta curva de aprendizaje** que nos exigirá concentrarnos en el tramo que tenemos delante, **todo ello siempre amenizado con un apasionado y particular copiloto** que nos cantará las curvas y nos echará la bronca cuando sea necesario. 

![](https://cdn.cloudflare.steamstatic.com/steam/apps/2494780/extras/About_Steam_Passionate_Co-Drivers.gif?t=1707212151)

El escenario que podemos jugar en esta demo es un **rally ubicado en Alemania** (Holzberg), con una mezcla de etapas de tierra y asfalto, y rodeado tanto de la frondosa naturaleza que se espera de estas latitudes, como la imagen idílica de los pueblos germanos.

Algunas de las cosas que encontraremos en esta demo son:

● **Modo Carrera Rápida:** Elige entre 3 emocionantes circuitos de rally en nuestro impresionante mapa de Holzberg

● **Modo Carrera:** únete al equipo Benzin Motors, rellena tu diario de carreras y asóciate con Hans, el copiloto de Benzin.

● **Elige tu coche:** prueba a conducir 3 coches de diferentes clases de velocidad. Celestia, el robusto Doggo RS y el poderoso Das Sandsturm XR.

● **Diversión en modo foto:** Captura y atesora esos impresionantes momentos de carreras con nuestro ¡Modo Foto!

![](https://cdn.cloudflare.steamstatic.com/steam/apps/2494780/extras/About_Steam_Design.gif?t=1707212151)

En la **versión final**, que **se espera a lo largo de este año**, además de lo antes mostrado, encontraremos más escenarios, como es de esperar, como el arído Dry Crumbs o helado Revontuli. También** multitud de coches que podremos personalizar** en aspecto y caracteríticas, para que se adapten tanto a nuestra forma de conducir como a nuestros gustos personales. También podremos escoger entre diversos copilotos, cada uno con su personalidad y estética noventera. Habrá también **numerosos huevos de pascua** que iremos descubriendo a medida que vayamos avanzando en el juego. 

![](https://cdn.cloudflare.steamstatic.com/steam/apps/2494780/extras/About_Steam_Retro_Revival.gif?t=1707212151)

Podeis disfrutar de la demo disponible durante toda esta semana, hasta el proximo lunes día 12 en su [página de Steam](https://store.steampowered.com/app/2494780/DRIVE_Rally/), donde también podreis añadirlo a vuestra lista de deseados: 

<div class="steam-iframe">
<iframe src="https://store.steampowered.com/widget/2494780/" frameborder="0" width="646" height="190"></iframe>
</div>

Os dejamos como siempre con un video promocional de la demo:

<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/4J7UnJNRLcw?si=hDIVPJ6MSfylLT7q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

