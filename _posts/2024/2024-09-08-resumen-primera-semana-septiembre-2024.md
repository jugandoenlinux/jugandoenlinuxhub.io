---
author: Odin
category: "Apt-Get Update"
date: 08-09-2024 01:10:00
excerpt: "Resumen semanal de las noticias publicadas durante la primera semana de Septiembre en Mastodon."
layout: post
tags:
- noticias
- resumen
title: "Resumen de noticias: Proton For Old Vulkan, Slipstream, Ludusavi y mucho más."
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20240908/sliptStream.webp
---

A continuación os mostramos el resumen de la semana de las noticias que hemos publicado en nuestra cuenta de [Mastodon](https://mastodon.social/@jugandoenlinux).

## [Proton For Old Vulkan](https://mastodon.social/@jugandoenlinux/113096941360875684)
Nueva versión de Proton-For-Old-Vulkan,  el proyecto que permite ejecutar Proton en tarjetas gráficas antiguas que no tienen soporte para Vulkan 1.3

Te comentamos las  novedades de la última versión:

* Pasa a usar como base  Proton-GE-9-13
* WineD3D: uso de OpenGL 4.6 o en su defecto la versión más reciente de OpenGL que soporte el driver de la GPU
* DXVK 1.10.3

La release podéis descargarla de [Github](https://github.com/pythonlover02/Proton-For-Old-Vulkan/releases/tag/Sarek9-13)

## [Slipstream publica su última gran actualización](https://mastodon.social/@jugandoenlinux/113095561527434356)

Gracias a leillo1975 nos enteramos de que Slipstream ha recibido su última gran actualización. Y es que ha sido una década de desarrollo para este fantástico videojuego al estilo OutRun.

![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20240908/sliptStream.webp)

La novedad más destacada es el soporte para Steam Workshop, con lo que vamos a poder jugar los circuitos y coches creados por la comunidad.

Podéis ver la noticia al completo en [Steam](https://store.steampowered.com/app/732810/Slipstream/).

## [Nueva versión de Ludusavi](https://mastodon.social/@jugandoenlinux/113091700565602140)

Ludusavi es una útil herramienta software libre para realizar copias de seguridad de  partidas y configuraciones de tus videojuegos.
Capaz de escanear juegos instalados en Steam, Heroic o Lutris por ejemplo.

Hace poco se actualizó añadiendo la posibilidad de ignorar algunos juegos y hacer copia a tu medida.

Funciona muy bien tanto en Linux como en la Steam Deck  

Se recomienda instalar como flatpak:
[Flathub](https://flathub.org/apps/com.github.mtkennerly.ludusavi)

## [RetroidPocket 5 soportará oficialmente Linux](https://mastodon.social/@jugandoenlinux/113083889946584893)

La nueva Retroid Pocket 5 y la Retroid Pocket Mini soportarán oficialmente Linux. En concreto Batocera y armbian.

![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20240908/retroidPocket5.webp)

Estas videoconsolas, con procesadores ARM, están orientadas al uso de emuladores y de videojuegos retro, así que no es de extrañar el uso de una distribución especializada en eso como la genial Batocera.

Desde Jugando En Linux nos alegramos de ver Linux en cada vez más videoconsolas portátiles.

## [OpenGoal](https://mastodon.social/@jugandoenlinux/113072563851841053)

Muchos conocerán Naughty Dog, uno de los estudios más importantes de Sony, por juegos como los Uncharted y The Last of Us. Sin embargo antes de sacar estos AAA se dedicaban a hacer plataformas de grandísima calidad como por ejemplo la saga Jak and Daxter que fue publicada en PS2.

Si quieres jugarlos te recomendamos OpenGoal. Es un proyecto software libre que permite ejecutarlos natívamente en Linux sin necesidad de emularlos.

[OpenGoal](https://opengoal.dev/)

## [Mejora tu funda de Steam Deck gracias a la impresión 3d](https://mastodon.social/@jugandoenlinux/113068435372764525)

Si tienes una Steam Deck y puedes imprimir piezas 3D aquí va una recomendación que puede ser muy útil.

Se trata de una pieza para insertar el cargador y las tarjetas de memoria ordenadamente en el hueco de la funda oficial:
[ThingVerse](https://www.thingiverse.com/thing:5399610/remixes),
[Printables](https://www.printables.com/es/model/207028-steam-deck-carrying-case-insert-for-eu-plugs-europ)

En este caso es para el adaptador europeo, pero hay para otras regiones.



