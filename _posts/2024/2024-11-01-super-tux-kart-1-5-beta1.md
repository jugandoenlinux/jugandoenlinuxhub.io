---
author: Son Link
category: Carreras
date: 2024-11-01 08:00:00
excerpt: "Después de mucho tiempo tenemos la primera beta de la próxima versión de este popular juego de karts de código abierto"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperTuxKart/1.5/portada-beta1.webp
layout: post
tags:
  - carreras
  - open-source
  - karts
  - supertuxkart
title: SuperTuxKart 1.5 Beta 1
---

Después de mucho tiempo tenemos nueva versión de este popular juego de carreras de karts de código abierto, similar a la popular saga **Mario Kart**

Los cambios que trae esta nueva versión (sin contar todas las pequeñas correcciones y mejoras) son los siguientes:

#### General:

- Añadido un modo de **Benchmark**. Se puede acceder a al en pocos clics, lo que permite probar el rendimiento del juego con diferentes ajustes y/o sistemas. También se ha añadido un parámetro en la línea de comandos para ejecutarlo
- Métricas de rendimiento robustas que reflejan mejor el impacto de la variación de frametimes
- Ahora en la pantalla **Conectado** se muestran noticias del blog
- Ahora la progresión de los niveles de audio es geométrica y se aumentan los pasos por defecto, permitiendo establecer niveles de audio más bajos y una mejor precisión para los niveles de audio bajos (especialmente útil para los usuarios de auriculares)
- Varios ajustes, correcciones de errores y mejoras en la calidad del código

#### Gráficos:

- Se ha mejorado la precisión del limitador de fotogramas
- Se han añadido más opciones de velocidad máxima de fotogramas al limitador de velocidad de fotogramas
- Añadidos algunos efectos gráficos para los controladores de vídeo antiguos
- Varias mejoras en los cálculos automáticos de las distancias de nivel de detalle (LoD)
- Se han habilitado nuevos ajustes superiores de LoD y sombras
- Se han integrado los ajustes LoD (Geometry Detail) en los preajustes gráficos
- Ahora se prefiere mostrar un modelo LoD de menor calidad en lugar de cambiar a uno de mayor calidad cuando está demasiado cerca
- Varias correcciones de errores y mejoras

#### Pistas y modelos:

- Ahora el pájaro de rescate (Thunderbird) coloca el kart hacia el balón en el modo fútbol
- Nueva música para Las Dunas Arena/Las Dunas Soccer
- Actualizada la textura de la cara de Godette
- Arreglar el skybox de Northern Resort
- Varios cortes/correcciones

#### Red:

- Mejorada la lógica de votación cuando no se alcanza la mayoría
- Ahora se evita que los espectadores eliminen los complementos seleccionables
- Ahora los índices de bots empiecen desde uno

#### Interfaz de usuario:

- Ahora la ventana de juego se redimensiona en todas las pantallas de la interfaz de usuario
- Ahora se usan valores separados de «tema base» y «variante de skin» para la configuración del skin
- Añadidas algunas variantes de skin para los temas base Clásico y Dibujos animados
- Añadida una nueva pestaña de Pantalla en los Ajustes
- Añadida información de ayuda para el modo Lap Trial
- Ahora se pueden valorar los complementos usando el teclado o el mando
- Ahora se pueden marcar tus karts/pistas/arenas favoritas
- Ahora se pueden buscar karts y arenas
- Ahora se pueden agrupar los karts por clases
- Varias mejoras en el diseño de la interfaz de usuario
- Se han generado texturas de mayor resolución para fuentes escalables
- La puntuación ahora de muestra con color en el centro del velocímetro en las batallas
- Varias mejoras

**Botón para iniciar el test de rendimiento:**

![Botón para iniciar el test de rendimiento](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperTuxKart/1.5/benchmark.webp)

**Resultado del test de rendimiento:**

![Resultado del test de rendimiento](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperTuxKart/1.5/resultado_benchmark.webp)

Como veis son muchas las mejoras añadidas, de hecho [aquí tenéis](https://github.com/supertuxkart/stk-code/blob/master/CHANGELOG.md#supertuxkart-15-beta1) la lista de cambios completa con los nombres de los desarrolladores que han hecho cada cambio.

Podéis descargar el juegos desde la página de lanzamiento en [GitHub](https://github.com/supertuxkart/stk-code/releases/tag/1.5-beta1)

Y hasta aquí todo. Si buscas gente para jugar, entra a nuestro grupo de **Telegram** o a la sala **A Jugar** en **Matrix** y pregunta.

Nos vemos en la pista.
