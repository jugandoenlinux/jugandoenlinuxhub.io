---
author: Serjor
category: "Apt-Get Update"
date: 08-12-2024 19:00:00
excerpt: "Resumen semanal de las toots publicados en mastodon durante la semana 49"
layout: post
tags:
- noticias
- resumen
title: "Juegos gratis, nuevas versiones de Lutis y más en el resumen semanal"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Lutris/lutris.webp
---


La semana de Consti e Inma han dado para mucho, os lo contamos en el resumen semanal de nuestra cuenta en [Mastodon](https://mastodon.social/@jugandoenlinux)

## [Des pa cito... (se veía venir)](https://mastodon.social/@jugandoenlinux/113584232985596016)

Aunque Lutris ha sufrido un desarrollo lento, ya que su autor tuvo que abandonar su dedicación exclusiva, como os contamos en su día [aquí](https://mastodon.social/@jugandoenlinux/111767087995868490), nos alegra anunciar que llega una nueva versión 0.5.18 con cosas interesantes:

- Nuevo tema oscuro
- Soporte para DirectX 8
- Nuevos perfiles
- Ahora en GoG e Itch se puede seleccionar los instalables para Linux y Windows del mismo juego

Hay muchos más cambios y arreglos:
[Ver cambios detallados](https://github.com/lutris/lutris/releases/tag/v0.5.18)



## [Hablando de verse cosas...](https://mastodon.social/@jugandoenlinux/113584831186350231)

Se ha publicado una nueva versión del estándar abierto de computación gráfica Vulkan. La 1.4 integra en el core de la especificación extensiones que antes eran opcionales.

En lo que a nosotros concierne, hay que destacar que los drivers de código abierto de Mesa, usados en Linux, han pasado los test que verifican que cumplen el estándar 1.4.

No hay duda de que los drivers de Mesa son hoy en día una referencia.

[https://www.khronos.org/news/press/khronos-streamlines-development-and-deployment-of-gpu-accelerated-applications-with-vulkan-1.4](https://www.khronos.org/news/press/khronos-streamlines-development-and-deployment-of-gpu-accelerated-applications-with-vulkan-1.4)



## [Pura envidia](https://mastodon.social/@jugandoenlinux/113588729067185418)

Ayer os comentábamos que había salido Vulkan 1.4 y que los drivers de Mesa para Linux ya lo implementan.

Hoy os ponemos un enlace del blog de uno de los desarrolladores de NVK, el driver libre de Vulkan para tarjetas gráficas Nvidia.

[Enlace al blog de Collabora](https://www.collabora.com/news-and-blog/news-and-events/nvk-now-supports-vulkan-14.html)

Y es que ahora que tienen el estándar al completo se van a enfocar en el rendimiento y en aquellas extensiones que van a mejorar la ejecución de juegos dx12 con proton en gráficas Nvidia.



## [Gracias, os queremos](https://mastodon.social/@jugandoenlinux/113593497943332550)

Este año ha sido un gran año para nosotros. Hemos doblado y superado el total de seguidores, también hemos hecho cambios en nuestra web, etc.

Gracias a todas las personas que este año nos habéis empezado a seguir y que nos animan a seguir creciendo. Miles de gracias de parte del equipo que formamos Jugando en Linux.

![Estadísticas de jugandoenlinux en mastodon](https://files.mastodon.social/media_attachments/files/113/593/386/773/167/914/original/8b531e00e8497c8c.jpg)


## [SteamOS a toda máquina 😅](https://mastodon.social/@jugandoenlinux/113599374508095774)

SteamOS, el sistema operativo de Valve para la Steam Deck, puede que le haya llegado la hora de desembarcar en más dispositivos.

Y es que se ha descubierto que en la guía de uso de marcas de Valve ahora aparecen logotipos de "SteamOS compatible" y "Powered by SteamOS", lo que sugiere que la Steam Deck va a dejar de ser el único dispositivo hardware soportado con SteamOS de forma oficial.

[Guía de uso de marcas de Valve](https://cdn.cloudflare.steamstatic.com/steamcommunity/public/images/steamworks_docs/english/steam_brandGuidelines.pdf)



## [Corred insensatos!](https://mastodon.social/@jugandoenlinux/113611800261434889)

En Steam está gratis el juego de plataformas Pankapu hasta el 18 de diciembre:

[store.steampowered.com/app/418670/Pankapu/](https://store.steampowered.com/app/418670/Pankapu/)

Nativo para Linux 🐧 y verificado para Steam Deck ✅


Nada mal, la verdad. Animaros a comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).
