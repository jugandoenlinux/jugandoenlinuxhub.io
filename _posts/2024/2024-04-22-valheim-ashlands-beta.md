---
author: Son Link
category: Simulación
date: 22-04-2024 14:50:00
excerpt: "Ashlands: el nuevo bioma que hará que sientas que estás en el mismísimo infierno"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/ashlands/portada.webp
layout: post
tags:
- simulacion
- tycoon
- estrategia
- codigo-abierto
title: "Valheim 0.218.9 – Ashlands (Beta Pública)"
---
Después de meses de trabajo, pronto tendremos una "nueva" zona para explorar en Valheim: Ashlands. Ashlands (Tierra de cenizas) son los biomas de fuego situados en la parte sur del mundo, donde antes solo había eso, fuego y surtling.

Para visitar este nuevo bioma antes tenemos que acabar con La Reina en las Tierras Brumosas y hacernos con el objeto que nos ayudara en la tarea, tal y como pasa con los objetos que obtenemos tras derrotar al resto de jefes.

En el tráiler podemos ver a varios de los nuevos enemigos, armas, equipamiento, etc. (ojo, contiene spoilers):

<div class="resp-iframe">
<iframe width="1366" height="480" src="https://www.youtube.com/embed/XSVbXgBJIuI" title="Valheim: Ashlands Gameplay Trailer" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

### Nuevo contenido

* +30 nuevas armas
* Nuevas bombas y municiones
* 3 nuevos sets de armadura
* 2 capas nuevas
* +10 criaturas nuevas
* +70 nuevos objetos de construcción
* 5 nuevas actualizaciones de la estación de artesanía
* +30 nuevos objetos de artesanía
* +15 nuevos objetos de pociones y comidas
* Nuevas localizaciones
* Nueva música
* Nuevos eventos
* Nuevas mecánicas

### Correcciones y mejoras:

* Arreglados los materiales para las maltas
* Textos normalizados en todo el juego

Esto son los cambios resumidos. Si quieres más, puedes pulsar en **Show spoiler** en [su web](https://www.valheimgame.com/es/news/patch-0-218-9-ashlands-public-test). También verás en él las instrucciones para poder probar la versión de test abierto antes de su publicación

Podéis adquirir el juego en [Steam](https://store.steampowered.com/app/892970/Valheim/)

Nos vemos en las Ashland, hijos de Odin