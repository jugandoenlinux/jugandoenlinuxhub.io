---
author: leillo1975
category: "Carreras"
date: 05-11-2024 16:00:00
excerpt: "El juego de Pixel Wrappers finalmente tiene version Linux"
layout: post
tags:
- carreras
- Classic Sport Driving
- Pixel Wrappers
- Indie
- Unity3D
- nativo
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ClassicSportDriving/ClassicSportDrivingTux.webp
title: "Lanzado Classic Sport Driving con soporte nativo"

---
Hace mucho tiempo que le seguía la pista a este juego, incluso durante su periodo de desarrollo. Si lo conocéis sabréis que el juego fue lanzado hace más de un año para Windows, y puede que muchos de vosotros incluso ya lo hayáis jugado usando Proton, pero había algo que estaba pendiente, y es que como uno de sus desarrolladores, Sylvain, me comentó hace ya años, **el juego tendría una versión nativa para Linux y Steam Deck**.

El caso es que, como en otras ocasiones, desde **el equipo de JugandoEnLinux se pusieron manos a la obra para que esto fuera realidad y durante meses realizaron tareas de testeo en diferentes builds**. En un determinado momento apareció un problema grave al que no fué posible encontrar una solución inmediata y no permitió lanzar simultáneamente las versiones de Windows y Linux, aplazando la segunda hasta encontrar una solución.

![](https://classicsportdriving.net/press/res/pics/C9EA00E2-0AED-4E2D-B20B-70D6BCCB3185.png)

Pero finalmente, hace unas semanas, Sylvain volvió a ponerse en contacto conmigo comentándome que el problema había sido solucionado y que el juego ya se podía jugar en Linux sin problemas, por lo que **estaría disponible para todos los usuarios de nuestro sistema a partir del 4 de Noviembre** ... y aquí estoy, dando la noticia que hace tanto tiempo quería dar, y que tanto ha costado el llegar a ella, tal y como podeis ver en este tweet:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/ClassicSportDriving?src=hash&amp;ref_src=twsrc%5Etfw">#ClassicSportDriving</a> is now available on <a href="https://twitter.com/hashtag/Linux?src=hash&amp;ref_src=twsrc%5Etfw">#Linux</a>!🐧🥳<br><br>And the game is on sale at €/$ 3.99 during the whole week to celebrate (Linux+Windows)!<br>🌴|🚘|🚘|🌵<br>Join the competition now, and make some noise for <a href="https://twitter.com/hashtag/LinuxGaming?src=hash&amp;ref_src=twsrc%5Etfw">#LinuxGaming</a>!🐧💪<a href="https://twitter.com/hashtag/gamedev?src=hash&amp;ref_src=twsrc%5Etfw">#gamedev</a> <a href="https://twitter.com/hashtag/Gameplay?src=hash&amp;ref_src=twsrc%5Etfw">#Gameplay</a> <a href="https://twitter.com/hashtag/indiegame?src=hash&amp;ref_src=twsrc%5Etfw">#indiegame</a> <a href="https://twitter.com/hashtag/racing?src=hash&amp;ref_src=twsrc%5Etfw">#racing</a> <a href="https://twitter.com/hashtag/RETROGAMING?src=hash&amp;ref_src=twsrc%5Etfw">#RETROGAMING</a> <a href="https://t.co/vTUaNwsnNV">pic.twitter.com/vTUaNwsnNV</a></p>&mdash; Classic Sport Driving🌴|🚘|🚘|🌵 (@cSportDriving) <a href="https://twitter.com/cSportDriving/status/1853497441917558930?ref_src=twsrc%5Etfw">November 4, 2024</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

[Classic Sport Driving](https://classicsportdriving.net/) es un **juego arcade de carreras de coches al más puro estilo retro de los juegos de los 80 y 90** (Lotus Turbo Challenge, Lamborghini American Challenge, Outrun ...), pero con unos gráficos que, aunque sencillos, son bastante definidos y con una **estética muy marcada**. La **sensación de velocidad está muy conseguida** y tiene una **jugabilidad endiablada**.

En el juego conduciremos nuestro coche en trayectos desde el punto A al punto B pasando por diversos checkpoints, recogiendo por el camino boosts que nos darán un buen empujón para conseguir llegar a la meta en tiempo. **La mecánica del juego es sencilla, lo cual no quiere decir que sea un juego fácil**, especialmente en las primeras partidas, donde tendremos que ir aprendiendo a anticiparnos a las curvas y a los otros vehículos que obstaculizarán nuestro recorrido. Hay **dos niveles de dificultad, arcade y pro**, donde el primero será más accesible, y el segundo para los más avezados y hábiles conductores.

Durante las diferentes fases del juego nos encontraremos **escenarios con variados biomas** (desiertos, montañas, acantilados, bosques...) **donde podremos conducir a diferentes horas del día** (día, noche, amanecer, anochecer), y/o **con diferentes condiciones meteorológicas** (sol, lluvia, nieve, niebla...), lo cual puede incrementar la dificultad, además de hacer mucho más vistoso el juego.

![](https://classicsportdriving.net/press/res/pics/56bd1480-1bb3-443e-86af-7c18850e976c.jpg)

El juego cuenta con un **modo campaña para un jugador** donde correremos en pistas predefinidas, y donde iremos adquiriendo estrellas según lo rápido en completar la fase seamos. Con estas estrellas iremos desbloqueando nuevas fases. Pero donde radica toda la "chicha" del juego es en las **pistas infinitas** que nos ofrece, ya que tiene un **generador procedural** que introduciendo una semilla, en este caso un nombre, nos creará un escenario único con su trazado y condiciones meteorológicas únicos que podremos compartir con nuestros amigos fácilmente (basta compartir el nombre de la pista). Todas estas pistas tendrán una **tabla de clasificaciones online** donde podremos ver los mejores tiempos del resto de jugadores del mundo.

Esto además permite que los desarrolladores del juego creen **pruebas semanales y mensuales** donde los usuarios del juego podrán competir dejando su mejor tiempo a la vez que ven en pantalla los diferentes **coches fantasma** de los demás jugadores. Tanto las pistas infinitas, como los retos semanales y mensuales hacen que **la rejugabilidad de este juego sea total**. El juego además cuenta desde hace unos días con un **modo foto** donde podréis sacar a relucir vuestra creatividad y buen gusto tomando instantaneas durante las partidas y compartiéndolas en sus salas de [discord](https://discord.gg/TAUrZgDA).

![](https://classicsportdriving.net/press/res/pics/107A9684-7442-43FD-A8DC-5B89011285D8.png)

Como veis, un juego más que recomendable para todos aquellos que gustéis de los árcades de conducción de antaño, y que además tiene un precio espectacular, teniendo además esta semana un descuento del 20% que lo deja por debajo de los 4€. Podéis comprar este juego en Steam, no tenéis excusa:

<div class="steam-iframe">
<iframe src="https://store.steampowered.com/widget/1267580/" frameborder="0" width="646" height="190"></iframe>
</div>

Os dejamos con el trailer del juego:

<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/GIEVpwlPHwM?si=T2UImyWi3a8rnj8I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>
