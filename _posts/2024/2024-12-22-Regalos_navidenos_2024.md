---
author: Serjor
category: "Apt-Get Update"
date: 22-12-2024 19:00:00
excerpt: "Resumen semanal de las toots publicados en mastodon durante la semana 51"
layout: post
tags:
- noticias
- resumen
title: "Regalos navideños, zombis, juegos para compartir y más"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20241224/tux-navideno.webp
---

Pues... No vamos a mentir, esta semana más bien poco. Hala, os adelantamos, mucho más que la semana que viene, porque es lo que tienen épocas como navidades y verano, que también las desarrolladoras cierran unos días para descansar, con lo que hay poco movimiento.

Con todo, [la semana pasada](https://jugandoenlinux.com/posts/GOTY_2024_y_mas/) os emplazamos a [una encuesta](https://mastodon.social/@jugandoenlinux/113642188265096054) que teníamos en marcha sobre lo que está preparando Valve. Ya sabemos (un secreto a voces), que en el CES 2025 habrá una Lenovo Legion con SteamOS, pero en vuestra opinión, además de esto, también se viene una Steam Machine 2.0 Veremos a ver si tenemos videntes entre los miembros de la comunidad.

Y ahora sí, el resumen semanal:

## [Zombis zumbaos](https://mastodon.social/@jugandoenlinux/113675882927317972)

¿Impaciente por probar la nueva versión 42 de Project Zomboid? Acaban de hacer pública la versión previa a su lanzamiento y la puedes probar: [Aquí](https://store.steampowered.com/news/app/108600/view/658185520169354701)

Trae una barbaridad de novedades: edificios, sótanos, iluminación, animales, granjas, nuevos menús,...

Siempre ha tenido un gran soporte para Linux 🐧 y además se preocuparon de adaptarlo a la Steam Deck 🎮 para verificarlo, por increíble que parezca. 😍 

Pronto estará en GOG también.



## [¿Serán las últimas ofertas del año? Que vaya ritmo de rebajas llevamos...](https://mastodon.social/@jugandoenlinux/113684093993734946)

El invierno se acerca, y como cada año vuelven las ofertas en Steam, además de las votaciones para los Premios Steam en varias categorías.

Tenéis hasta el 2 de Enero a las 19:00, hora española, para aprovechar las ofertas: [store.steampowered.com/sale/special_deals?l=spanish](https://store.steampowered.com/sale/special_deals?l=spanish)

Y para votar, tenéis hasta el 31 de Diciembre hasta las 19:00 hora española. Podéis realizar las votaciones en [aquí](https://store.steampowered.com/steamawards/2024?snr=1_4_661__steamawards-banner)

Aprovechad para pedirle algún juego a Papa Noel.



## [Un multi en casita, siempre apetece](https://mastodon.social/@jugandoenlinux/113696339674866388)

Ahora, con la llegada de las Navidades, ¿qué mejor que echar unas partidas en familia a juegos multijugador?

Aquí va una recomendación, fácilmente instalable gracias a Flatpak y Flathub:

[Ring Racers](https://flathub.org/apps/org.kartkrew.RingRacers)



## [Papa Noel, el Olentzero, Santa Claus y los Reyes Magos necesitan tu ayuda](https://mastodon.social/@jugandoenlinux/113697988305441924)

En estas fechas festivas...

¿Qué videojuegos has regalado o piensas regalar para Linux o Steam Deck? ❄ ☃ 🎁 
Comenta en el hilo del toot y deja tus regalos videojueguiles.


Animaros a comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).
