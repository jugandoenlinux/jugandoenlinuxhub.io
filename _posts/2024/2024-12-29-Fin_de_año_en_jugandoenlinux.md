---
author: Serjor
category: "Apt-Get Update"
date: 29-12-2024 19:00:00
excerpt: "Resumen semanal de las toots publicados en mastodon durante la semana 52"
layout: post
tags:
- noticias
- resumen
title: "El último resumen del año!! A disfrutar de nochevieja"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/new-lg4ff/new-lg4ff-1.webp
---

Como a este 2024 solamente le quedan tres días, os traemos los últimos tres toots de la semana (tampoco había más) y del año.

## [Qué apropiado, una actualización como una vaca](https://mastodon.social/@jugandoenlinux/113703406294241913)

¡Publicada la nueva versión 2.9.0 de ScummVM!

Gracias a este programa software libre se puede ejecutar alrededor de 325 aventuras gráficas, incluyendo juegos como Monkey Island, Broken Sword, e incluso algunas nuevas como Thimbleweed Park.

Podéis ver las notas de publicación de la versión en el siguiente enlace:

[scummvm.org/news/20241222/](https://www.scummvm.org/news/20241222/)



## [Como un embarazo, y mirad qué guapo a salido!!](https://mastodon.social/@jugandoenlinux/113721268996053760)

Tras nueve meses de desarrollo llega Veloren 0.17:

- Nuevo sistema de recetas para fabricar.
- Nuevos enemigos: Hydra, Karkatha, Cactids, Goblins y Legooms.
- Nuevas estructuras: Terracotta, Minas Dwarven y Castillo Vampiro. También granjas, más muelles para aeronaves y circuitos de planeo.
- Mazmorras rehechas completamente: Cultist, Myrmidon y Sahagin.

Habrá una fiesta 🎉 de presentación el 28 de diciembre a las 19:00 (CET)

[veloren.net/blog/release-0-17/](https://veloren.net/blog/release-0-17/)



## [Ha llegado el momento de ponerse al volante de verdad](https://mastodon.social/@jugandoenlinux/113725651888311339)

Se ha publicado una nueva versión de new-lg4ff, la 0.4.1, un driver para volantes de la marca Logitech para Linux:

* Rebasado el código a Linux 6.3
* Se ha corregido la compilación en Linux 6.12
* Se han corregido algunos problemas en los efectos condicionales
* Se ha fijado la duración y retardo en actualizaciones de efectos
* No se crean entradas sysfs relacionadas para volantes sin FFB

[https://github.com/berarma/new-lg4ff/releases/tag/v0.4.1](https://github.com/berarma/new-lg4ff/releases/tag/v0.4.1)



Y salvo que haya alguna notica destacable, este será el último artículo del año, desde luego, el último resumen del año, seguro.
Desde [Jugando En Linux](https://jugandoenlinux.com) os deseamos un feliz año 2025. Que lo mejor del 2024 sea lo peor del 2025 🎉

Animaros a comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).
