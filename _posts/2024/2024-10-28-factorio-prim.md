---
author: P_Vader
category: "Apt-Get Update"
date: 28-10-2024 21:30:00
excerpt: "Resumen semanal de las noticias publicadas en Mastodon en la última semana"
layout: post
tags:
- noticias
- resumen
title: "Muchos seguidores, Factorio, PRIM y más"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20241028/2048-seguidores.webp
---

El resumen semanal que ha sido muy especial para todos nosotros porque...

## [Llegamos a 2048 seguidores en mastodon](https://mastodon.social/@jugandoenlinux/113378496629832877)

Muchas gracias, que nos sigáis por mucho más tiempo...

y a por los 4096 🥳

---

## [Dosbox 0.82.0](https://mastodon.social/@jugandoenlinux/113379519160700687)

Recién salida del horno, la versión 0.82.0 de dosbox staging viene con grandes mejoras tanto en la compatibilidad, incluyendo emulación de las instrucciones MMX de Pentium, como en el rendimiento de la emulación de 3dfx Voodo.

Os recomendamos, para tener una visión global de todo lo que trae la 0.82.0, la release notes que han publicado en su [web](https://www.dosbox-staging.org/releases/release-notes/0.82.0/)

---

## [Beat Invaders 3.0](https://mastodon.social/@jugandoenlinux/113369635242419509)

Si te flipan los matamarcianos 👾 (shot'em up) deberías de echarle un ojo a Beat Invaders.

Es un homenaje al mítico Space Invaders, hiper vitaminado, con muchísimas variaciones, jefes y posibilidades para mejorar tus naves.

Como buen arcade tiene su tabla de máximas puntuaciones a nivel mundial.

Acaba de lanzar su versión 3.0 ampliando el juego mas aun si cabe en [steam](https://store.steampowered.com/app/1863080/Beat_Invaders/)

Nativo para #Linux 🐧 y verificado para #SteamDeck ✅ 

---

## [SteamOS 3.6.19](https://mastodon.social/@jugandoenlinux/113363569427645518)

Valve acaba de lanzar una nueva versión estable de SteamOS para la steam deck, la versión 3.6.19, que tiene un listado enorme de correcciones y mejoras, el cuál podéis consultar [aquí](https://steamcommunity.com/games/1675200/announcements/detail/4676514574283544995)

---

## [Se estrena PRIM](https://mastodon.social/@jugandoenlinux/113361577651148731)

Se publicó PRIM, una aventura gráfica muy entretenida y simpática con un estilo de dibujo pintado a mano impresionante.

En su día probamos la demo en diferentes festivales y nos gustó mucho. La demo aún puedes probarla.
Ya disponible en [steam](https://store.steampowered.com/app/1510470/PRIM/)

Está nativo para Linux 🐧 e idioma español.

---

## [Factorio estrena expansión](https://mastodon.social/@jugandoenlinux/113351432775351100)

Los aficionados a Factorio están de enhorabuena porque recientemente han publicado la esperada expansión "Space Age".

Los que todavía no tienen pensado adquirirla también tienen buenas noticias porque al juego base llega la actualización gratuita 2.0

Os recomendamos leer las novedades directamente de su entrada en el [blog](https://www.factorio.com/blog/post/factorio-space-age-release)

Podéis adquirir la expansión en [Steam](https://store.steampowered.com/app/645390/Factorio_Space_Age/)

O en [GOG](https://www.gog.com/en/game/factorio_space_age)

---

## [Nueva versión de Unvanquished](https://mastodon.social/@jugandoenlinux/113350017075170601)

Con una extensa entrada de su blog, los desarrolladores del FPS multijugador gratuito Unvanquished nos anunciaban la publicación de la versión 0.55

Se ha hecho un gran trabajo de optimización para que independientemente de la antigüedad de vuestro ordenador lo podáis ejecutar.

Os animamos a probarlo!!

Lo podéis instalar fácilmente desde su web o incluso con Flatpak.

[Blog Unvanquished](https://unvanquished.net/unvanquished-0-55-awesomeness-has-arrived/)

[Flathub](https://flathub.org/apps/net.unvanquished.Unvanquished)

---

Y hasta aquí el resumen de noticias. Como siempre podéis comentar estas noticias en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), [Telegram](https://t.me/jugandoenlinux) o en nuestro [Mastodon](https://mastodon.social/@jugandoenlinux)