---
author: Odin
category: "Apt-Get Update"
date: 07-10-2024 10:41:00
excerpt: "Resumen semanal de las noticias publicadas en Mastodon en la pasada semana"
layout: post
tags:
- noticias
- resumen
title: "Sea of Stars, The Last Plague: Blight, Civilization VII, y otras noticias."
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20241007/seaofstars.webp
---


Aquí os traemos un resumen con las noticias de la semana pasada que hemos ido publicando en nuestro [Mastodon](https://mastodon.social/@jugandoenlinux/).

## [GemRB permite completar Icewind Dale 2.](https://mastodon.social/@jugandoenlinux/113260178327590379)

GemRB, la reimplementación libre del Infinity Engine, por primera vez es capaz de ejecutar Icewind Dale 2.

Gracias a este motor podemos jugar nativamente los RPG clásicos de BioWare.

Estos motores libres son de una gran importancia para la preservación de los videojuegos y nos alegramos que sigan mejorando con cada versión.

[GemRB](https://gemrb.org/2024/10/03/icewind-dale-2-is-now-completable.html)

## [The Last Plague: Blight a la venta en acceso anticipado](https://mastodon.social/@jugandoenlinux/113254584235777882)

Hace mucho tiempo en algún festival de demos, probamos el juego The Last Plague: Blight y pese a estar en una fase temprana, nos enganchó.

![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20241007/thelastplague.webp)

Se trata de un juego de supervivencia en un mundo abierto donde tienes que enfrentarte a la dura naturaleza y a una extraña enfermedad que lo ha desolado todo.

Ya está a la venta en acceso anticipado, nativo para Linux y dispone de demo por si te apetece probarlo en [Steam](https://store.steampowered.com/app/1564600/The_Last_Plague_Blight/)

## [Civilization VII tendrá versión para Linux](https://mastodon.social/@jugandoenlinux/113250926743771952)

Sid Meir's Civilization VII tendrá soporte nativo para Linux / SteamOs según han anunciado Firaxis Games. 

El juego está previsto que se publique a principios del años que viene en [Steam](https://store.steampowered.com/app/1295660/Sid_Meiers_Civilization_VII/)

Ya son unos cuantos juegos de este estudio que dan soporte a Linux y se agradece.

## [Primera versión oficial de UMU launcher](https://mastodon.social/@jugandoenlinux/113250451587894210)

[GloriousEggroll](https://mastodon.social/@gloriouseggroll@fosstodon.org), conocido por ser el creador de Proton GE y de la distribución Nobara, ha publicado la primera versión oficial de UMU launcher.

Hay que recordar que UMU Launcher nos va a permitir ejecutar juegos con Proton que no son de Steam pero con la misma configuración de Proton y Steam Runtime que se usaría de ser la versión del juego de Steam.

[Github UMU laucher](https://github.com/Open-Wine-Components/umu-launcher/releasest)

## [Preserve](https://mastodon.social/@jugandoenlinux/113248567433020686)

Aprovechando de nuevo el festival de rol por turnos de steam hemos sabido de Preserve, un juego de turnos, puzles y construcción relajado donde el objetivo es la conservación de la naturaleza en diferentes ecosistemas:

[Preserve](https://store.steampowered.com/app/2109270/Preserve/)

![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20241007/preserve.webp)

El juego tiene buenas reseñas y está nativo para Linux.

## [GOG regala Whispering Willows](https://mastodon.social/@jugandoenlinux/113245227507700806)

Whispering Willows es una aventura con puzles que lo están regalando en GOG por tiempo limitado: 

[Whispering Willows](https://www.gog.com/en/game/whispering_willows)

Tiene muy buena pinta y está nativo para Linux.

## [Luchando en Pico-8 con Backstreet Warriors](https://mastodon.social/@jugandoenlinux/113242200231246338)

Ayer supimos de este pequeña joya llamada Backstreet Warriors, un Beat'em up hecho con Pico-8 y donde nuestro objetivo es acabar con los 99 enemigos que trataran de acabar con nosotros, además de sacar la mayor puntuación posible, incluso se puede jugar a dobles

Esta disponible de forma nativa para Linux, incluyendo versión para las Raspberry Pi, y es totalmente gratis, aunque si te gusta, siempre puedes dar algo de dinero a su creador:

[Backstreet Warriors](https://sebagamesdev.itch.io/backstreet-warriors)

## [Modo juego con Gamescope](https://mastodon.social/@jugandoenlinux/113238016668872065)

Si quieres jugar con gamescope (como en la SteamDeck) en tu Linux de escritorio, no es demasiado complicado creando un par de scripts manualmente para iniciar sesión en este modo **juego**.

En este caso la guía que hemos usado se centra en Arch con #kde y Wayland, aunque es posible hacerlo en otras distribuciones, incluso con Xorg, sin mucho problema:
[Guía Gamescope](https://github.com/shahnawazshahin/steam-using-gamescope-guide)

Si necesitas mas información sobre gamescope la puedes consultar en el [Github de Valve](https://github.com/ValveSoftware/gamescope)

## [Juegazos indies JRPG](https://mastodon.social/@jugandoenlinux/113231045887284823)
Si te interesan los jrpg de corte indie te recomendamos los dos siguientes juegos que actualmente están rebajados al 30% con motivo del Festival del Rol por Turnos de Steam:

* [Sea of Stars](https://store.steampowered.com/app/1244090/Sea_of_Stars/)

* [Chained Echoes](https://store.steampowered.com/app/1229240/Chained_Echoes/)

Ambos juegos están verificados y el segundo incluso tiene versión nativa.

![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20241007/seaofstarsworld.webp)

---
Y hasta aquí el resumen de noticias. Como siempre podeis comentar estas noticias en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), [Telegram](https://t.me/jugandoenlinux) o en [Mastodon](https://mastodon.social/@jugandoenlinux)