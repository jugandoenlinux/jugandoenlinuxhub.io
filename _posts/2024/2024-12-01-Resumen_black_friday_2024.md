---
author: Serjor
category: "Apt-Get Update"
date: 01-12-2024 19:00:00
excerpt: "Resumen semanal de las toots publicados en mastodon durante la semana 48"
layout: post
tags:
- noticias
- resumen
title: "BlackFriday y jueguicos en el resumen semanal"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/OpenMOHAA/openmohaa.webp
---

Terminamos semana, comenzamos mes y celebramos un viernes negro con muchos descuentos:


## [Open source of Honor](https://mastodon.social/@jugandoenlinux/113548348818308978)

La saga Medal of Honor es una de las veteranas de la industria. El primer juego salió en la PlayStation y posteriormente tuvo varias continuaciones.

Un grupo de desarrolladores de código abierto ha creado un motor que permite ejecutar el juego Medal of Honor: Allied Assault, incluyendo varias expansiones.

Como siempre hemos apoyado la preservación de los videojuegos, nos alegra conocer y compartir con vosotros iniciativas como esta.

[openmohaa.org](https://www.openmohaa.org/)



## [El Cities: Skylines bueno](https://mastodon.social/@jugandoenlinux/113549642848828439)

Of Life and Land es un juego de estrategia en el que creas tu propio asentamiento y en el que la relación de tus aldeanos y el ecosistema tiene una gran importancia en su desarrollo.

Nativo para Linux, hecho en [Godot Engine](https://godotengine.org/), lo cierto es que tiene muy buena pinta. Su propio autor [@Kerzoven](https://mastodon.social/@Kerzoven) tiene cuenta en Mastodon, por lo que podéis seguir los avances del desarrollo del videojuego en esta misma red social.

[Enlace a Steam](https://store.steampowered.com/app/1733110/Of_Life_and_Land/)



## [Venga que así llegamos a fin de mes](https://mastodon.social/@jugandoenlinux/113554092498309206)

Nuevas rebajas en GOG con motivo del Black Friday!!

Como hace poco hablamos de openmohaa, os recomendamos pillar el Medal of Honor Allied Assault:

- [Medal of Honor Allied Assault War Chest](https://www.gog.com/en/game/medal_of_honor_allied_assault_war_chest)
- [Promoción de Black Friday 2024 en GOG](https://www.gog.com/en/promo/2024_black_friday)



## [Por lo menos aquí no hay que hacer cola en la puerta del centro comercial](https://mastodon.social/@jugandoenlinux/113556578738754151)

Más rebajas!!

Ahora le toca el turno a Steam, con sus rebajas de Otoño.

Aquí va una recomendación:

* **Ace Combat 7** rebajado al 90%
  - [Ver en Steam](https://store.steampowered.com/app/502500/ACE_COMBAT_7_SKIES_UNKNOWN/)



## [Que sí, que se viene... O no 👀](https://mastodon.social/@jugandoenlinux/113561001368663679)

Y otra semana de filtraciones, en esta ocasión han aparecido unos supuestos renders de los nuevos mandos de Valve:

[bsky.app/profile/xpaw.me/post/3lbv5dzg5fc2o](https://bsky.app/profile/xpaw.me/post/3lbv5dzg5fc2o)

Como siempre, mucha cautela porque también podrían ser diseños desechados, pero es cierto que llevamos mucho tiempo viendo pistas de que algo diferente tiene entre manos Valve.



## [¿Y qué hay mejor que algo rebajado? Efectivamente](https://mastodon.social/@jugandoenlinux/113570965284371037)

En la tienda GOG están regalando el videojuego Oaken:  
[https://www.gog.com/en/game/oaken](https://www.gog.com/en/game/oaken)

Este es nativo para Linux 🐧

Y recuerda que GOG, como muchas otras tiendas, están de rebajas:  
[https://mastodon.social/@jugandoenlinux/113554092498309206](https://mastodon.social/@jugandoenlinux/113554092498309206)


Por lo demás, como siempre, podeis comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).
