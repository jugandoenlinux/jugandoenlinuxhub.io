---
author: P_Vader
category: "Apt-Get Update"
date: 29-09-2024 16:30:00
excerpt: "Resumen semanal de las noticias publicadas en Mastodon en la última semana"
layout: post
tags:
- noticias
- resumen
title: "City Game Studio, Tiny Glade, Stunt Rally y mucho más."
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20240924/tiny_glade.webp
---

Otro recopilatorio de todo lo acontecido en nuestra cuenta de Mastodon a lo largo de esta última semana:

## [City Game Studio](https://mastodon.social/@jugandoenlinux/113214123575356933)

Después de 7 años de desarrollo, City Game Studio ve la luz, un juego nativo para linux, desarrollado con [@godotengine](https://mastodon.gamedev.place/@godotengine) [@krita](https://mastodon.art/@Krita) [@Blender](https://mastodon.social/@Blender) [@inkscape](https://mastodon.art/@inkscape) y mucho talento y esfuerzo, sin duda.

En CGS seremos responsables de hacer crecer un estudio de videojuegos indies, al más puro estilo Game Dev Tycoon.

Si te interesa, puedes encontrarlo en [Steam](https://store.steampowered.com/app/726840) [GOG](https://www.gog.com/game/city_game_studio_a_tycoon_about_game_dev/) 
o [itch](https://binogure.itch.io/city-game-studio/)

<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/45Jkyo__4Pk?si=wAFHQZqAakbO2WrN" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

## [Se publicó Tiny Glade.](https://mastodon.social/@jugandoenlinux/113191473543447264)

Un simulador de construcción de castillos que ha sido programado con el motor de videojuegos de código abierto bevyengine y que tiene versión nativa para Linux.

Actualmente tiene un pequeño descuento en su precio con motivo de su lanzamiento. Otro punto a su favor es que las reseñas, ahora mismo en Steam, son de categoría "Extremadamente positivas".

[Steam](https://store.steampowered.com/app/2198150/Tiny_Glade/)

---

## [Nuevo Stunt Rally 3.2](https://mastodon.social/@jugandoenlinux/113219956980640821)

Un veterano de los juegos software libre 3d de carreras, ha publicado la versión 3.2.

Esta nueva versión incluye novedades como un nuevo modo de juego llamado Collection, mejoras en la vegetación, en el renderizado de agua, cambios en la interfaz gráfica y en el editor.

[Los cambios](https://github.com/stuntrally/stuntrally3/releases/tag/3.2)

---

## [Valve y Arch colaboran](https://mastodon.social/@jugandoenlinux/113215069141376454)

Nos levantamos con la grata noticia de que Valve y Arch Linux van a colaborar directamente entre ellas, anuncio que ha sido publicado en la lista de correo de [Arch](https://lists.archlinux.org/archives/list/arch-dev-public@lists.archlinux.org/thread/RIZSKIBDSLY4S5J2E2STNP5DH4XZGJMR/)

Valve va ayudar en 2 áreas críticas: la construcción de una infraestructura de servicios y un enclave de firma segura, siendo hasta la fecha gestionado por voluntarios a tiempo parcial.

Además hemos de recordar que SteamOS 3 se basa en Arch

[Fuente](https://lemmy.ml/post/20782733)

---

## [Nuevos drivers gráficos en Debian](https://mastodon.social/@jugandoenlinux/113210346758062456)

Debian 12 posiblemente sea una de las distribuciones más sólidas y a prueba de fallos. Eso la hace ideal como distribución de trabajo.

Sin embargo para gaming hay que reconocer que "stable" precisamente no tiene los paquetes más modernos y eso puede impactar a la hora de jugar.

Recientemente han publicado Mesa 24.2 en backports, que junto con el kernel 6.10, permiten seguir en stable pero con unas versiones mucho más actualizadas.

---

## [Valve trabaja en dar soporte ARM](https://mastodon.social/@jugandoenlinux/113187697656922747)

Recientemente se ha filtrado, gracias a SteamDB, que Valve estaría trabajando en dar soporte a la arquitectura ARM.

Para ello usaría FEX, que permite ejecutar binarios compilados en x86 y x86-64 en máquinas con procesadores ARM.

Quizás esto lleve a que en un futuro haya una Steam Deck version ARM o que puedas jugar con Steam de forma oficial en tu Raspberry Pi. 

---

## [EmuDeck 2.3](https://mastodon.social/@jugandoenlinux/113208604689470075)

Salió publicada la versión 2.3 de EmuDeck. Entre las novedades se encuentra una completa revisión de la interfaz gráfica, nuevos emuladores de Nintendo 3DS (Lime3DS, PabloMK7 Citra fork) y la posibilidad de actualizar emuladores desde gamemode con EmuDecky.

Hay que tener en cuenta que para actualizar a esta nueva versión es necesario hacer un Quick o un Custom reset de EmuDeck.

En [Reddit](https://www.reddit.com/r/EmuDeck/comments/1fpyab2/emudeck_23_is_out_citric_acid_edition/)

---

## [MangOverlay para configurar Mangohud](https://mastodon.social/@jugandoenlinux/113205125017136046)

Siempre es bueno tener alternativas, por lo que nos encanta saber de nuevas aplicaciones software libre aunque su funcionalidad ya esté cubierta por otra.

MangOverlay es un programa, que al igual que Goverlay, permite configurar Mangohud.

Lo puedes descargar e instalar desde su [GitHub](https://github.com/loissascha/MangOverlay)

---

## [GOG de rebajas](https://mastodon.social/@jugandoenlinux/113197981603742456)

Siempre intentamos avisar cuando hay rebajas de Steam, pero lo cierto es que otras tiendas también las hacen, así que no podíamos dejar pasar la ocasión para comentaros que han empezado las rebajas de [GOG de otoño](https://www.gog.com/en/promo/autumn_sale_2024)

Y ya que estamos os ponemos una recomendación
[Flatout 2 a 1.39 €](https://www.gog.com/en/game/flatout_2)

---

## [Nueva versión de Proton GE 9-14](https://mastodon.social/@jugandoenlinux/113193361060095949)

Como ya sabéis Proton GE suele incluir arreglos que facilitan ejecutar videojuegos que en la versión de Protón normal necesitan configuraciones manuales. Este es el caso del último juego de God of War que para ejecutarse en PC hay que habilitar una variable en la lista de parámetros de lanzamiento. Gracias a esta versión nos ahorra tener que hacer esa configuración manual.

[Changelog](https://github.com/GloriousEggroll/proton-ge-custom/releases/tag/GE-Proton9-14)

---

Y hasta aquí el resumen de noticias. Como siempre podeis comentar estas noticias en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), [Telegram](https://t.me/jugandoenlinux) o en [Mastodon](https://mastodon.social/@jugandoenlinux)
