---
author: P_Vader
category: "Apt-Get Update"
date: 14-10-2024 16:00:00
excerpt: "Resumen semanal de las noticias publicadas en Mastodon en la última semana"
layout: post
tags:
- noticias
- resumen
title: "Luanti, Pictopix, Warhammer, Analisis,..."
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20241014/luanti.webp
---

Una semana mas os mostramos el resumen de toda la semana con noticias que hemos publicado en nuestra cuenta de [Mastodon](https://mastodon.social/@jugandoenlinux)

## [Minetest cambia de nombre](https://mastodon.social/@jugandoenlinux/113302305303832563)

Después de años de deliberación Minetest ha decidido cambiar de nombre y dejar atrás la minería ⛏️ y el testeo, para evitar la erronea comparación con Minecraft, del que ya poco se parece como hemos explicado muchas veces.

El nuevo nombre elegido será Luanti, que viene de la mezcla de la palabra finlandesa "luonti" (creación) y del lenguaje de programación Lua que se usa para sus mods y juegos.

[El anuncio oficial](https://blog.minetest.net/2024/10/13/Introducing-Our-New-Name/)

Nos gusta Luanti ❤️ 

---

## [Pictopix](https://mastodon.social/@jugandoenlinux/113295832608199349)

Pictopix es un juego de puzles tipo nonograma 🔢 con unas reseñas extremadamente positivas y nativo para Linux 🐧 

A través de su cuenta en Mastodon [@pictopix](https://mastodon.gamedev.place/@pictopix) ha anunciado que el juego va a ser verificado para Steam Deck 🎮 

Además está de [oferta](https://store.steampowered.com/app/568320/Pictopix/)


---

## [Warhammer jugable](https://mastodon.social/@jugandoenlinux/113272637517417352)

Los aficionados a Warhammer 40K en Linux están de enhorabuena ya que Space Marine 2 vuelve a funcionar.

Concretamente en las notas del hotfix que han publicado hoy pone "Fixed launch issue on Linux and Steam Deck".

Esperemos que sigan dando soporte y de esa manera su calificación para Steam Deck pueda pasar del actual "No compatible" a al menos "jugable".

[Steam](https://store.steampowered.com/app/2183900/Warhammer_40000_Space_Marine_2/)

---

## [Análisis de IstharVega](https://mastodon.social/@IstharVega)

Esta semana en Mastodon impulsamos un par de toots de [IstharVega](https://mastodon.social/@IstharVega) donde hace un muy buen análisis de algunos juegos nativos para Linux y/o Steam Deck:

[Jugando a The Beginner’s Guide – Una Pequeña Reflexión](https://mastodon.social/@IstharVega/113283831302047960)

> The Beginners Guide es el JUEGO, es una obra que debe ser jugada por todos, para aprender a analizar, dejar de sacar conclusiones y juicios precipitados.


[Jugando a: 3 Minutes to Midnight](https://mastodon.social/@IstharVega/113276650809501988)

>En un pueblo en medio del desierto, una estruendosa explosión sacude a toda la población en medio de la noche...

Puedes seguir a [IstharVega](https://mastodon.social/@IstharVega) en Mastodon

---

## [Songs of Conquest](https://mastodon.social/@jugandoenlinux/113283956785699467)

Si los juegos clásicos de la saga Heroes of Might Magic están entre tus preferidos, probablemente te interese Songs of Conquest ya que su jugabilidad está claramente inspirada en dicha saga.

Verificado para Steam Deck, con unos buenos gráficos pixel art, y con una valoración de reseñas muy positivas, Songs of Conquest está actualmente rebajado en [Steam al 50%](https://store.steampowered.com/app/867210/Songs_of_Conquest/)

---

## [Nuevo lanzamiento de Open3D Engine](https://mastodon.social/@jugandoenlinux/113282305267586174)

El potente motor de video juegos Open3D Engine "O3DE" acaba de lanzar nueva versión 24.09. Entre las muchas novedades destaca el gran aumento de rendimiento y optimización en muchas de sus áreas de uso.

Este motor #SoftwareLibre está especializado en la simulación de grandes espacio abiertos, ideal para videojuegos tipo "mundo abierto".  
Si siempre comparamos Godot con Unity, para O3DE su homologo sería Unreal.

Su [Web](https://o3de.org/) y sus [cambios](https://docs.o3de.org/docs/release-notes/2409-release-notes/)

---

## [Vulkan en Nvidia](https://mastodon.social/@jugandoenlinux/113288872648077425)

Si quieres saber en que estado se encuentra el driver libre de Vulkan para Nvidia y te gustan los detalles técnicos te recomendamos la siguiente [presentación](https://indico.freedesktop.org/event/6/contributions/295/attachments/228/308/2024-10-10%20XDC%202024%20-%20Nouveau%20NVK%20Update.pdf)

Hecho por uno de sus desarrolladores, hace un repaso, entre otras cosas, de las funcionalidades que se han implementado para poder ejecutar DXVK y Zink en NVK.

---

## [Nuevo driver Razer ](https://mastodon.social/@jugandoenlinux/113267169705032865)


Se publicó una nueva versión del driver libre para dispositivos [Razer](https://fosstodon.org/@openrazer), añadiendo soporte a 12 dispositivos de dicha marca, aumentando la lista a 201, entre teclados, ratones y otros dispositivos.

También se han corregido varios problemas y eliminada la dependencia del paquete de Python notify2, la cual está obsoleta.

Tenéis la lista de dispositivos y resto de cambios en la página de [descargas](https://github.com/openrazer/openrazer/releases)			
	
---

## [Franquicia Kingdom](https://mastodon.social/@jugandoenlinux/113289619599797971)

La franquicia Kingdom 👑 está de oferta este fin de semana en steam y los puedes probar gratis!! 

[Franquicia 2024](https://store.steampowered.com/sale/KingdomFranchise2024)

Estos juegos son una mezcla entre construcción, defensa de torres y supervivencia. Tienen un estilo artístico con cámara lateral muy vistoso. Y enganchan mucho. 

Están nativos para Linux 🐧 y verificados para Steam Deck ✅ 

---

## [Oferton Stardew Valley](https://mastodon.social/@jugandoenlinux/113267781118800754)

Si aun no tienes Stardew Valley esta semana lo puede encontrar al [50% en Steam](https://store.steampowered.com/app/413150/Stardew_Valley/). Muy pocas veces en su historia ha llegado a un precio tan bajo.

Es uno de los mas jugados en Steam Deck ✅ y nativo para Linux 🐧 

---

Y hasta aquí el resumen de noticias. Como siempre podeis comentar estas noticias en nuestros canales de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), [Telegram](https://t.me/jugandoenlinux) o  [Mastodon](https://mastodon.social/@jugandoenlinux)