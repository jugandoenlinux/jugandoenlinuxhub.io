---
author: P_Vader
category: "Apt-Get Update"
date: 03-08-2024 12:30:00
excerpt: "Resumen semanal de las últimas noticias publicadas en la primera semana de agosto"
layout: post
tags:
- noticias
- resumen
title: "The Garden Path, World of Goo 2, Nazi Zombies, Pixelorama, Linux al 4,45% y más"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20240803/the_garden_path.webp
---

Continuamos otra semana con un resumen del mundo de los videojuegos en Linux . A continuación te mostramos noticias destacadas que hemos publicado recientemente en nuestras redes sociales.

---
## The Garden Path

The Garden Path es un juego que en los diferentes festivales de Steam probamos su demo y nos llamó mucho la atención. Es un simulador de vida, relajante, "cozy", con un arte muy cuidado y original. Podrás gestionar tu jardin pescando, cultivando, comerciando o haciando amigos. También se puede jugar cooperativo localmente.

Puedes adquirirlo ya en [Steam](https://store.steampowered.com/app/1638500/The_Garden_Path/) o [Itch](https://carrotcakestudio.itch.io/thegardenpath)

Está nativo para Linux y hecho en Godot.

<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/yha0038eC1o?si=LwY9QVkfT06UVDYn" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

[toot](https://mastodon.social/@jugandoenlinux/112876203321255012)

---
## World of Goo 2

16 años después, 2DBoy, un indie conocido, ha puesto a la venta World of Goo 2, secuela directa de uno de los primeros indiegame que dieron soporte nativo a linux, y en esta ocasión, también con soporte nativo!!

Si os interesa, podéis haceros con él directamente en su [tienda](https://worldofgoo2.com/#getitnow), y sin DRM por $29\.99

<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/q3XVl53Ajsk?si=6LwyG-s4yzsXDEPW" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

[toot](https://mastodon.social/@jugandoenlinux/112894394551476761)

---
## FNA con DirectX11

Cuando Microsoft abandonó el framework XNA, dejando de lado a una gran cantidad de desarrolladores indies, la comunidad de software libre recogió el testigo creando Monogame y posteriormente FNA.

Ambos frameworks gozan de una salud excelente y en concreto FNA ha publicado una nueva versión, la 24\.08, que permite usar Direct3d 11 directamente en Linux usando DXVK native.

[FNA 24\.08](https://github.com/FNA-XNA/FNA/releases)

[toot](https://mastodon.social/@jugandoenlinux/112894203611447176)

---
## Steam en AppImage

Si tenemos Steam en flatpak y en snap, no es de extrañar que a alguien se le ocurriera empaquetar Steam en un Appimage.

Te contamos lo que aporta:  
\- Incluye la última versión de los drivers de Mesa  
\- Es capaz de descargar automáticamente los drivers de Nvidia.  
\- Glibc con el parche aplicado para que funcione EAC  
\- Bibliotecas de 32 bits para una compatibilidad completa de Steam.  
\-Basado en el contenedor Conty de Arch. 

[Steam\-AppImage](https://github.com/ivan-hc/Steam-appimage)

[toot](https://mastodon.social/@jugandoenlinux/112891499038216494)

---
## Nazi Zombies

Nazi Zombies: Portable es un demake de Call of Duty: Zombies usando motores de [\#Quake](https://mastodon.social/tags/Quake) y está para varios sistemas operativos, consolas y a través del navegador web.

En él tendremos que, bien solos, o en cooperativo online, ir acabando con las oleadas de zombis nazis, y así además hacernos con dinero para comprar otras armas, incluso tiene logros.

[Descargar](https://github.com/nzp-team/nzportable) y [Jugar online](https://nzp.gay/)

[toot](https://mastodon.social/@jugandoenlinux/112887722258644673)

---
## Cloaks and Capes gratuito

Cloaks and Capes, un roguelite RPG, pasa a ser gratuito en Steam, por lo que si quieres probar este juego indie nativo para Linux lo tienes más fácil que nunca.

Cloaks and Capes en [Steam](https://store.steampowered.com/app/1637950/Cloaks_and_Capes/)

[toot](https://mastodon.social/@jugandoenlinux/112885664794450016)

---
## Linux al 4,45%

En el Lemmy de Linux gaming nos hemos enterado que el escritorio en Linux ha llegado a uno de sus máximos históricos con un 4,45% de uso, según la web de análisis [Statcounter](https://gs.statcounter.com/os-market-share/desktop/worldwide)

[https://lemmy.world/post/18162400](https://lemmy.world/post/18162400)

Piensa que hace poco mas de 10 años casi no llegábamos al 1%.

[toot](https://mastodon.social/@jugandoenlinux/112885158775236074)

---
## CoolerControl

CoolerControl es un programa que te permite gestionar los dispositivos de refrigeración de tu ordenador, tanto ventiladores como refrigeración líquida.

Recientemente ha publicado una nueva versión, la 1\.4\.0, que añade soporte a tarjetas gráficas AMD con arquitectura RDNA 3\.

Si queréis ver todas sus novedades podéis ver el listado al completo en [Gitlab](https://gitlab.com/coolercontrol/coolercontrol/-/releases)


[toot](https://mastodon.social/@jugandoenlinux/112874376230919740)

---
## Pixelorama 1.0

Nos gusta mucho los videojuegos 🎮 pero también nos gusta crear videojuegos, sobre todo con herramientas libres como Pixelorama ❤️ de [@OramaInteractive](https://mastodon.gamedev.place/@OramaInteractive) 

Ya está aquí la nueva versión 1\.0 cargada con un sin fin de novedades y sobre todo con su estreno en [Steam](https://store.steampowered.com/app/2779170/Pixelorama/) para ayudar en su financiación:  


También puedes darles soporte en [Itch](https://orama-interactive.itch.io/pixelorama)

En Linux ademas lo puedes encontrar en flatpak y snap.


[toot](https://mastodon.social/@jugandoenlinux/112870874280650324)

---
## Nuevo Goverlay

Publicada nueva versión de Goverlay. La 1\.2 trae varias novedades como la vuelta de los quick layouts, ahora llamados presets, nuevas opciones para los FPS incluyendo el AVG 1% y 0,1%, nuevo modo compacto para el HUD, etc.

Todas las novedades de la release la podéis ver en su [GitHub](https://github.com/benjamimgois/goverlay/releases/tag/1.2)

[toot](https://mastodon.social/@jugandoenlinux/112870422338416598)

---
## Vanilla OS 2

Hoy, tras un año de desarrollo, la gente de [@vanillaos](https://fosstodon.org/@vanillaos) ha publicado Vanilla OS 2\. 

Es una distribución del tipo Fedora Silverblue pero basada en Debian y trae una gran cantidad de características como la utilidad Apx que permite instalar aplicaciones de cualquier distribución o su capacidad para ejecutar aplicaciones Android mediante Waydroid.

Podeis leer mas en su [blog](https://vanillaos.org/blog/article/2024-07-28/vanilla-os-2-orchid---stable-release)

[toot](https://mastodon.social/users/jugandoenlinux/statuses/112865529426867856)

---
## Asahi Linux

La gente de Asahi Linux, el proyecto que permite ejecutar Linux en los Apple Silicon Macs, no para de trabajar. Recientemente han estado desarrollando un driver de Vulkan para Mesa que soporta la última versión del estándar gráfico en los M1 y M2 de Apple.

Para los más interesados en los detalles técnicos, hay que decir que este driver lo han realizado a partir de NVK y que el código lo podéis ver en el [siguiente commit.](https://gitlab.freedesktop.org/mesa/mesa/-/commit/5bc828481630147575348b66677edaade9e891e6)

[toot](https://mastodon.social/@jugandoenlinux/112857823146918275)

---
## The Verge le gusta SteamOS

En The Verge les gusta la nueva consola de Asus, pero definitivamente no les gusta Windows. Explican como tuvo que soportar mas de 45 minutos de actualizaciones y anuncios de Microsoft antes de poder usarla por primera vez. Al final concluyen:  
"...I hope Asus will seriously consider a SteamOS version, too. I hope to test it with Bazzite, an unofficial SteamOS clone, later this year."

Podeís leer el [artículo completo](https://www.theverge.com/24204770/asus-rog-ally-x-review-handheld-gaming-pc) si os interesa.

[toot](https://mastodon.social/users/jugandoenlinux/statuses/112852356302615705)

---

Y hasta aquí el resumen de noticias. Como siempre podeis comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).