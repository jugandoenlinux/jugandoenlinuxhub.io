---
author: Serjor
category: "Apt-Get Update"
date: 10-11-2024 20:15:00
excerpt: "Resumen semanal de las noticias publicadas durante la semana 45 en Mastodon."
layout: post
tags:
- noticias
- resumen
title: "Lúces, cámara y resumen semanal"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20241110/steam_game_recording.webp
---

No habrá sido la semana más intensa de la historia de Mastodon, pero eso no quiere decir que no haya habido noticias interesantes.

## [Classic Sport Driving](https://mastodon.social/@jugandoenlinux/113431577375730346)

Por fin, Classic Sport Driving tiene versión nativa para Linux después de mucho tiempo de trabajo para que fuera posible.

Si te gustan los juegos de carreras tipo árcade, como OutRun, este juego es para ti. Y también está disponible para Steam Deck.

[Artículo en la Jugando en Linux](https://jugandoenlinux.com/posts/ClassicSportDriving/)



## [Grabación de partidas en Steam](https://mastodon.social/@jugandoenlinux/113437193414263095)

Llega la grabación de partidas a Steam 🎬

Esta nueva función está disponible para Linux 🐧 y Steam Deck 🎮

Entre otras cosas:
- Se puede crear ajustes para diferentes juegos.
- Asignación de teclas rápidas para capturar.
- Exportación avanzada, clips, mp4 o compartir en otros dispositivos y medios.
- Un nuevo gestor para ver tus grabaciones y capturas, con etiquetas y datos para cada juego.

Nuestro compañero [son_link](https://mastodon.social/@son_link) probó esta nueva funcionalidad cuando salió la beta abierta.

Podéis ver los enlaces a los vídeos en este toot [aquí](https://mastodon.social/@jugandoenlinux/112699321075664679)

[Página de Steam](https://store.steampowered.com/gamerecording)

## [La jam de Luanti](https://mastodon.social/@jugandoenlinux/113454207555743735)

El proyecto del videojuego Luanti ⛏️ (anteriormente llamado Minetest) tiene en marcha una Jam para crear juegos dentro de Luanti. Para programar sobre Luanti se usa el lenguaje LUA. Si estás interesado, aquí tienes más información:

[JAM](https://jam.luanti.org/)

Cada año salen cosas muy buenas de la Jam ❤️

Como siempre decimos, Luanti es una base para jugar diferentes juegos y con muchos mods:

[Content](https://content.luanti.org/)



## [Nueva versión de LibreQuake](https://mastodon.social/@jugandoenlinux/113454775139359212)

Ha sido publicada la versión 0.08-beta de **LibreQuake**, que incluye:

* Se han añadido, corregido y mejorado varios mapas, incluyendo mapas multijugador
* Añadidos varios efectos de sonido
* Corregidos varios efectos
* Añadida una guía de como montar una partida de Deathmatch
* Otras correcciones y mejoras

En la página de descargas tenéis el listado completo de cambios

[https://github.com/lavenderdotpet/LibreQuake/releases/tag/v0.08-beta](https://github.com/lavenderdotpet/LibreQuake/releases/tag/v0.08-beta)



## [Proton GE 9-19](https://mastodon.social/@jugandoenlinux/113460370577672012)

Recientemente se ha publicado Proton GE 9-19, que incluye arreglos para *Silent Hill 3*, *Horizon Zero Dawn Remaster* y *Monster Hunter Wilds*.

Además, como suele ser habitual, viene con las últimas actualizaciones de DXVK y vkd3d.

Podéis descargar esta versión con la estupenda aplicación ProtonUp-Qt.

[ProtonUp-Qt](https://davidotek.github.io/protonup-qt/)



## [Actualización de Luanti](https://mastodon.social/@jugandoenlinux/113460502536977086)

Si ayer hablabamos de la Jam de Luanti ⛏️ (anteriormente llamado Minetest), hoy toca su actualización a la versión 5.10.

Hay mucho trabajo sustituyendo el nombre de Minetest por Luanti, pero ten en cuenta que hay carpetas y archivos de configuración que mantienen el nombre de Minetest por ahora.

Entre otras cosas, se ha subido la versión de OpenGL ES y se ha rediseñado la pestaña para descargar contenido en el menú principal:

[Changelog 5.9.1 → 5.10.0](https://dev.minetest.net/Changelog#5.9.1_→_5.10.0)

---

Y hasta aquí el resumen de noticias. Como siempre podeis comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).
