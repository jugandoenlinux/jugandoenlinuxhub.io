---
author: Serjor
category: "Apt-Get Update"
date: 01-09-2024 20:30:00
excerpt: "Resumen semanal de las noticias publicadas en la última semana en Mastodon"
layout: post
tags:
- noticias
- resumen
title: "Jueguitos nativos y actualizaciones de aplicaciones para la última semana de agosto"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20240901/canvasofkings.webp
---

Será agosto, pero el movimento en la esfera del videojuego linuxero no ha parado

## The Abandoned Planet

The Abandoned Planet es una aventura gráfica en primera persona del género de ciencia ficcion que nos ha llamado la atención. Gráficamente luce un estupendo pixel art, tiene versión nativa para Linux y está traducida al español.

Es por esas razones que te recomendamos que le eches un vistazo en [Steam](https://store.steampowered.com/app/2014470/The_Abandoned_Planet/)

[toot](https://mastodon.social/@jugandoenlinux/113061239206975357)

---

## Canvas of Kings (un editor de mapas, no un juego) en Steam

Canvas of Kings es una herramienta interactiva para la creación de mapas, especialmente para juegos de rol de mesa, aunque puede ser para otras cosas, por ejemplo, mapas para tus videojuegos. Tiene muchos elementos, puedes añadirlos siguiendo una ruta o dentro de una zona marcada, rotarlos, redimensionarlos, etc., por lo que puede ser una herramienta muy útil.

Está hecho con Godot tiene una demo para probarlo y está nativo para Linux en [Steam](https://store.steampowered.com/app/2498570/Canvas_of_Kings/>)

[toot](https://mastodon.social/@jugandoenlinux/113056985079438434)

---

## Bazzite, actualización de la que sea la probablemente la mejor distro para hacerse una Steam Machine

Bazzite, la distribución gaming construida sobre Fedora Silverblue, publicó hace unos pocos días la versión 3.7.

Hay bastantes novedades en la nueva versión. Destacamos las siguientes:

* Instalador con usuario por defecto para permitir instalar Bazzite sin tener que usar teclado. 
* Actualizaciones entre 2-5 veces más rapidas gracias a la reducción del tamaño de las descargas
* Nuevo planificador con mejoras en el rendimiento

[Lista de cambios](https://universal-blue.discourse.group/t/bazzite-3-7-0-update-released/3726)

[toot](https://mastodon.social/@jugandoenlinux/113056033051245298)

---

## Emudeck

Emudeck, el popular proyecto que permite configurar de forma sencilla emuladores en Steam Deck, se pasa al hardware y va a sacar dos consolas retro.

Para ello han abierto una campaña de crowdfunding en [Indiegogo](https://www.indiegogo.com/projects/emudeck-machines-retro-emulation-console-pc#/)

La opción más barata se basa en un Intel N97 y la más cara en un AMD Ryzen 8600g. A nivel de sistema operativo llevará una de nuestras distribuciones Linux favoritas que es Bazzite.

[toot](https://mastodon.social/@jugandoenlinux/113050317454217313)

---

## Warside, un homenaje a los juegos por turnos nativo y hecho con Godot

Ayer supimos del lanzamiento de este nuevo juego nativo para #Linux y compatible con Steam Deck, y que tiene una pinta increíble:

Warside, o como rendir homenaje a los juegos de estrategia por turnos | Jugando En Linux
<https://jugandoenlinux.com/posts/warside/>

[toot](https://mastodon.social/@jugandoenlinux/113044359722246911)

---

## ProtonUp-Qt

ProtonUp-Qt es una herramienta software libre muy potente para administrar las distintas versiones de Wine y Proton en diferentes aplicaciones como Steam, Lutris o Heroic.

Recientemente se actualizó añadiendo entre otras cosas soporte para el lanzador WineZGUI:
<https://github.com/DavidoTek/ProtonUp-Qt/releases/tag/v2.10.2>

Es muy fácil de instalar tanto como Flatpak o AppImage:
<https://davidotek.github.io/protonup-qt/>

[toot](https://mastodon.social/@jugandoenlinux/113040477945497557)

---

## Cemu

Aunque fue eclipsada por la Switch, la Wii U fue la primera videoconsola de Nintendo en reproducir videojuegos en alta resolución y además tuvo entre su catálogo algunas joyas indiscutibles.

En Linux puedes ejecutar sus juegos con Cemu. Recientemente han publicado la versión 2.1 con añadidos muy interesantes como el soporte Wayland y el empaquetado mediante Flatpak AppImage.

[Changelog v2.1](https://github.com/cemu-project/Cemu/releases/tag/v2.1)

[Flatpak](https://flathub.org/en-GB/apps/info.cemu.Cemu)

[toot](https://mastodon.social/@jugandoenlinux/113038590414727411)

---

## SteamOS vs Windows en una Asus ROG Ally

En Tom's Guide han cogido una Asus ROG Ally X y le han instalado SteamOS con Linux para comparar con Windows 11.

Los números son brutales, SteamOS arrasa con muchos mas FPS y un 20% mas de duración de batería frente al sistema de Microsoft.

A nosotros no nos ha sorprendido. 😎

<https://www.tomsguide.com/gaming/handheld-gaming/i-turned-the-asus-rog-ally-x-into-a-steam-deck-it-proves-windows-11-kills-power-and-battery-life>

Gracias a nuestro compi Jaime Rodriguez por el aviso.

[toot](https://mastodon.social/@jugandoenlinux/113035427417883369)

---

## Core Keeper llega oficialmente a la versión 1.0

Como avisamos ya está oficialmente la versión final del juego 1.0 Core Keeper en [Steam](https://store.steampowered.com/news/app/1621690/view/4335363098084315163?l=spanish)

[toot](https://mastodon.social/@jugandoenlinux/113034462186310251)

---

## La beta de Steam Deck se actuliza con una pequeña mejora de calidad de vida para ajustar la resolución

Por desgracia no todos los juegos están verificados para la Steam Deck, es por eso que algunos pueden venir con una resolución que no se ajuste adecuadamente a la pantalla de la Deck.

Es por eso que Valve nos está dando múltiples opciones en el cliente de Steam. Recientemente, en el canal beta, han incluido la funcionalidad de ajustar la resolución máxima a la que se permite usar los videojuegos de forma global.

[Stream News](https://store.steampowered.com/news/app/1675200?emclan=103582791470414830&emgid=4273439871240674776)

[toot](https://mastodon.social/@jugandoenlinux/113033400639364083)

---

Y hasta aquí el resumen de noticias. Como siempre podeis comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).
