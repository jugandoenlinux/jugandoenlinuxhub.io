---
author: Son Link
category: "Apt-Get Update"
date: 21-10-2024 16:00:00
excerpt: "Resumen semanal de las noticias publicadas en Mastodon en la última semana"
layout: post
tags:
- noticias
- resumen
title: "Prism, GameImage, Steam,..."
image: https://files.mastodon.social/cache/preview_cards/images/062/895/648/original/0aa4a48ec192181c.jpg
---

Ya tenemos para todos vosotros nuestro resumen semanal con las noticias más destacadas del mundo de los videojuegos en Linux. Como siempre podéis ver las noticias originales en nuestro [Mastodon](https://mastodon.social/@jugandoenlinux)

## [Prism Launcher 9.0](https://mastodon.social/@jugandoenlinux/113344404011622298)

Prism Launcher es un lanzador para Minecraft ⛏️ que permite gestionar fácilmente diferentes cuentas, instancias o mods.

Ayer mismo lanzó su versión 9.0 con muchas mejoras y arreglos.  
[Prism Launcher Release 9.0](https://prismlauncher.org/news/release-9.0/)

Esta aplicación tan útil es software libre y nativa para Linux 🐧

Tienes muchas maneras de instalarlo:  
[Descargas](https://prismlauncher.org/download/)

Y puedes seguirles en [@PrismLauncher](https://floss.social/@PrismLauncher) 

---

## [GameImage](https://mastodon.social/@jugandoenlinux/113338693659810932)

Hoy nos hemos enterado de la aplicación GameImage. 

Esta herramienta permite empaquetar en un solo fichero videojuegos y roms junto con las dependencias necesarias para ejecutarse. 

¿Te ha pasado que un juego de GOG te funcionaba en una versión de Ubuntu y ahora no te va en tu nueva distribución? GameImage quiere ser la solución a estos problemas.

Te recomendamos que le eches un vistazo a su GitHub:

[GitHub](https://github.com/ruanformigoni/gameimage)

---

## [Steam Runtime](https://mastodon.social/@jugandoenlinux/113333030251476915)

Una de las ventajas de Linux es la gran variedad de distribuciones que tenemos. 

Sin embargo está variedad puede dificultar que funcione en la mayoría de distribuciones. 

Para resolver este problema Valve desarrolló el Steam Runtime. Pues bien tenemos que deciros que se vienen cambios. En concreto se usará por defecto en los juegos nativos el runtime 1.0 (scout). Además los programadores podrán escoger la versión de Steam Runtime que quieren para sus juegos.

---

## [Multijugador en Buckshot Roulette](https://mastodon.social/@jugandoenlinux/113328312313900217)

El 31 de octubre 🗓️ llega por fin el modo multijugador a BUCKSHOT ROULETTE 🔫 

[Buckshot Roulette — Multiplayer Release Date Announcement](https://store.steampowered.com/news/app/2835570/view/4508758024123322722)

Un juego pequeño, adictivo, original y barato que en su día explotó en steam.

Recordaros que es nativo para Linux 🐧 y se puede jugar sin problema en la Steam Deck 🎮 
<div class="resp-iframe">
<iframe width="1203" height="677" src="https://www.youtube.com/embed/TwUxC41_4Y0" title="Warside - 2024 Trailer" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

Que ganas de probarlo!! 😍 

---

## [EVERGLORY](https://mastodon.social/@jugandoenlinux/113327355446889398)

EVERGLORY, un juego indie de estrategia en tiempo real con versión nativa para Linux, ha publicado recientemente una actualización de su demo en Steam. 

Nos ha llamado la atención de que a pesar de que es un juego comercial, el desarrollador tiene publicado el código fuente del motor con licencia GPL.

Si queréis probarlo lo podéis descargar desde:

[Steam](https://store.steampowered.com/app/1309720/EVERGLORY/)

[Itch](https://spb.itch.io/everglory)

[Código fuente](https://github.com/eduard-permyakov/permafrost-engine)

---

## [Ogre3d 3](https://mastodon.social/@jugandoenlinux/113322939460541698)

Uno de los motores de renderizado en tiempo real más veterano, dentro del software libre, es Ogre3d.

Sin embargo no se han quedado parados en su desarrollo, y ayer anunciaban la versión Ogre Next 3.0.0, y a la par ya adelantaban las novedades de la 4.0.0, en la que llevan más de un año trabajando.

Si quieres saber los detalles de estas nuevas versiones te recomendamos leerte la siguiente entrada de su blog

[Noticia del lanzamiento](https://www.ogre3d.org/2024/10/16/ogre-next-3-0-0-eris-released)

---

## [Neva](https://mastodon.social/@jugandoenlinux/113312890671601226)

Hoy ha visto a la luz Neva del estudio barcelonés Nomada, famoso por ser los creadores de Gris.

<div class="iframe-steam">
	<iframe src="https://store.steampowered.com/widget/2420660/" frameborder="0" width="646" height="190"></iframe>
</div>

El juego está verificado para Steam Deck ✅ y según los comentarios bien optimizado para la consola.

---

## [Valheim: The Bog Witch](https://mastodon.social/@jugandoenlinux/113312580782956818)

Ya es posible apuntarse a la beta pública de **Valheim: The Bog Witch**.

Esta nueva actualización trae una nueva comerciante, la Bruja del Pantano, a la que podemos comprar diversos materiales. Además, incluye:

* Nueva mecánica de banquetes  
* 16 nuevos materiales de artesanía  
* 8 nuevas pociones  
* 3 nuevos artículos de ropa  
* 3 nuevas habilidades  
* 4 nuevas piezas de construcción  
* 2 nuevas herramientas

<div class="resp-iframe">
<iframe width="1203" height="677" src="https://www.youtube.com/embed/lQiLPwV_8sY" title="Warside - 2024 Trailer" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

[+ info:](https://steamcommunity.com/games/892970/announcements/detail/7514910232818862415?snr=2___)

---

## [Steam Next Fest](https://mastodon.social/@jugandoenlinux/113310189039318701)

Hace unos días mpezó una nueva edición de #Steam Next Fest, un festival donde podemos probar demos de juegos que se lanzarán en un futuro y algunos actuales, ver directos de sus creadores, así como ofertas.

[Página de Next Fest](https://store.steampowered.com/sale/nextfest)

El staff de Jugando en #Linux ya estamos bajando varias demos para probar. Y tu, ¿que demos de juegos para Linux vas a probar?. Podéis dejar vuestras sugerencias contestando en el toot 😉
