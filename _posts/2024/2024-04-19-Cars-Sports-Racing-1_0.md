---
author: leillo1975
category: "Carreras"
date: 19-04-2024 12:00:00
excerpt: "Primera versión oficial de este Fork de Speed Dreams"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/CarsSportsRacing/CSR-1_0.jpg
layout: post
tags:
  - carreras
  - Cars Sports Racing
  - Open Source
  - Speed Dreams
  - SimRacing
  - fork
title: "Lanzada la versión 1.0 de Cars Sports Racing"
---

Hace unos días, el equipo detrás de **Speed Dreams** realizaba un [anuncio](https://www.speed-dreams.net/en/major-changes-in-the-speed-dreams-project/) donde entre otras cosas bastante importantes, anunciaba la marcha de dos de sus integrantes, entre ellos **Xavier Bertaux**, su principal desarrollador, principalmente por razones personales, y también por diferencias de parecer con respecto el camino que debía seguir el proyecto.

Esta decisión, tomada ya hace medio año, propició que meses después comenzase a gestarse la idea de un nuevo juego que siguiese el rumbo deseado por Xavier, **[Cars Sports Racing](https://sourceforge.net/projects/cars-sports-racing/) (C.S.R.)**, usando para ello como base el código de Speed Dreams. Como sabeis, Speed Dreams está basado a su vez en [TORCS](https://sourceforge.net/projects/torcs/), por lo que puede decir que **C.S.R. es un fork del fork**.

![](https://a.fsdn.com/con/app/proj/cars-sports-racing/screenshots/sd-20240410110303-cf9177d0.png/)

La **principal diferencia con Speed Dreams** radica principalmente en que el jugador, a medidia que avanza en el modo carrera, va **desbloqueando poco a poco nuevos retos** que debe superar, por lo que **no tendrá acceso a todo el contenido del juego desde el principio** como en Speed Dreams . También se incide bastante en la **resistencia y consistencia** del jugador, pues normalmente las pruebas son carreras de bastante duración, por lo que **está orientado hacía el jugador hardcore más acostumbrado al simracing**. 

A **nivel técnico** se diferencia principalmente con Speed Dreams en que no hace uso del motor gráfico OpenSceneGraph, si no que **usa una versión propia de PLib** (el viejo motor de S.D. y TORCS), **al que irá añadiendo nuevas características**. Esto se debe a que PLib permite la multextura, y en OpenSceneGraph esto no está implementado.

![](https://a.fsdn.com/con/app/proj/cars-sports-racing/screenshots/sd-20240410110342-e136bd56.png)

Algunas de las **novedades principales** que incluye esta **versión 1.0** son:  
- **Graficos**: primeros trabajos en destellos de lente, efectos de agua, fundido a blanco cuando el sol está en frente, y desvanecimiento a negro en los túneles.
- **Físicas**: Se pasa de la versión 4 del motor de fisicas SIMU, a la versión 5, que entre otras cosas incluye activación o desactivación de ciertas funciones según la dificultad, degradación y temperatura de neumáticos en todos los coches, o daños por temperatura del motor.
- **Robots**: personalización del setup de los robots para las diferentes pistas, eliminación de Dandroid y añadido de un nuevo tipo de bot derivado de USR pero más eficiente: Axiom.
- **Pistas**: Actualizadas algunas pistas con nuevos fondos, texturas y eliminación de referencias a SD. Comienzo en el trabajo de separar en archivos 3D (terreno/carretera/grava/arena/edificio) para efectos de shaders diferentes para cada archivo.

Para mas detalles os recomendamos que consulteis la [lista completa de cambios](https://sourceforge.net/p/cars-sports-racing/code/HEAD/tree/trunk/CHANGES.txt). Podeis descargaros la versión RPM o el código fuente de la [versión 1.0](https://sourceforge.net/projects/cars-sports-racing/files/1.0.0/), desde la página del proyecto de Cars Sports Racing en Sourceforge.

![](https://a.fsdn.com/con/app/proj/cars-sports-racing/screenshots/sd-20240410110645-09068762.png/)

Desde JugandoEnLinux.com le deseamos a este nuevo proyecto toda la suerte del mundo, y que su desarrollo repercuta tanto al mismo proyecto como en su proyecto "padre" Speed Dreams.

Podeis comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).
