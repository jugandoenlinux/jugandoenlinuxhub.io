---
author: P_Vader
category: "Apt-Get Update"
date: 26-08-2024 16:45:00
excerpt: "Resumen semanal de las noticias publicadas en la última semana en Mastodon"
layout: post
tags:
- noticias
- resumen
title: "Mucho Deadlock, Beyond All Reason y más"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20240826/bar.webp
---

Esta semana hablamos mucho sobre Deadlock y también de otras muchas cosas interesantes en nuestro [mastodon](https://mastodon.social/@jugandoenlinux).

## Mucho mucho DeadLock...

Estuvimos varios días probándolo con antelación y oficialmente lo [anunciamos](https://mastodon.social/@jugandoenlinux/113016073591838080) nada mas aparecer en [steam](https://store.steampowered.com/app/1422450/Deadlock/)

Mas tarde os dimos las primeras impresiones del juego en este [artículo](https://jugandoenlinux.com/posts/deadlock/)

Y por último jugamos un rato en directo en nuestro canal de [Twitch](https://twitch.tv/jugandoenlinux)

Si te lo perdiste aquí puedes verlo:
<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/ZTB2ahM9UKg?si=yQtL8EfvWFMwmwsd" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

## Beyond All Reason

Beyond All Reason es probablemente uno de los mejores videojuegos software libre de estrategia en tiempo real. Recientemente han publicado un trailer en el que se puede ver su estado actual.

<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/8K_fSWfOC1w?si=iLS4ED289IcjBg8K" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

Lo podéis instalar fácilmente en cualquier distribución mediante AppImage o desde [Flathub](https://flathub.org/apps/info.beyondallreason.bar) con Flatpak.


[toot](https://mastodon.social/@jugandoenlinux/112993786476643261)

---

## Jugando en otros SO Libres

Aunque nos llamamos jugandoenlinux nos encanta la gran diversidad de sistemas operativos de código abierto que existen.

Si juegas en algún otro sistema operativo libre que no sea Linux, nos gustaría que compartieses tu experiencia contestando a nuestro [toot](https://mastodon.social/@jugandoenlinux/113027790603120284).

Y es que recientemente nos hemos enterado de [Mizutamari](https://codeberg.org/Alexander88207/Mizutamari), una alternativa a Lutris para FreeBSD que permite usar Steam en este sistema operativo.


[toot](https://mastodon.social/@jugandoenlinux/113027790603120284)

---

## Core Keeper cerca del 1.0

Core Keeper está a punto de lanzar su versión final 1.0 y para celebrarlo estuvo gratis para [jugarlo](https://store.steampowered.com/app/1621690/Core_Keeper/).

Este juego dicen que es una mezcla entre Stardew Valley, Minecraft y Terraria 😱 

Está nativo para Linux 🐧 y además verificado para Steam Deck ✅ 


[toot](https://mastodon.social/@jugandoenlinux/113010555112732243)

---

## Limo para mods

Una de las ventajas de los videojuegos en PC es sin lugar a dudas los mods. Amplían la experiencia de nuestros videojuegos e incluso algunos han desembocado en títulos totalmente nuevos.

Nos hemos enterado recientemente de la aplicación Limo. Es un gestor de mods programado en Qt que tiene soporte para videojuegos como Skyrim y Fallout 4 entre muchos otros.

Lo podéis instalar fácilmente con [Flatpak](https://flathub.org/apps/io.github.limo_app.limo) en vuestra distro favorita.


[toot](https://mastodon.social/@jugandoenlinux/113008075343983684)

---

## Batocera 40

Batocera es una distribución de Linux orientada al retrogaming. Como interfaz utiliza EmulationStation, soporta una gran cantidad de emuladores, y se puede instalar en un rango amplio de hardware ya sea en una Raspberry Pi o en una Steam Deck.

Este mes han publicado la versión 40 que incluye una gran cantidad de mejoras, arreglos y mayor compatibilidad con periféricos como volantes y pistolas.

El listado de cambios lo podéis ver en su [changelog](https://batocera.org/changelog)

[toot](https://mastodon.social/@jugandoenlinux/113006148332200129)

---

## DXVK en RISC-V

Una de las mejores cosas del software libre es la capacidad que tiene en reutilizarse en proyectos que ni siquiera se pensaban originalmente.

Y es que cuando Valve contrató al desarrollador de DXVK no creo que pensase que terminaría por utilizarse con Wine y Box64 para ejecutar la versión de GOG de The Witcher 3 en una máquina RISC-V.

<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/5UMUEM0gd34?si=tsf0MrqUlIGj1TbW" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

RISC-V es una arquitectura libre y cualquiera puede fabricar hardware RISC-V sin tener que pagar licencias.

[toot](https://mastodon.social/@jugandoenlinux/113000308498276759)

---

## Valve y VAC 3.0

Valve empieza a actualizar su software anti trampas VAC a la versión 3.0 en [Counter Strike](https://store.steampowered.com/news/app/730/view/6500469346429581891?l=spanish).

Y ojo con esto porque también [han eliminado](https://store.steampowered.com/news/app/730/view/6500469346429600836?l=spanish) los atajos de teclado con pulsaciones automatizadas, los llamados "binds".

Cambios importantes y esperemos que acaben con la plaga de tramposos en sus juegos.

[toot](https://mastodon.social/@jugandoenlinux/112994047824698993)

---

## Descent 3:

Hace 4 meses se liberó el código fuente del motor de [Descent 3](https://jugandoenlinux.com/posts/descent3-1.5/). Hace un par de días, por fin vemos el resultado de tanto trabajo: el lanzamiento de la versión 1.5, con muchas novedades y cambios, normal teniendo en cuanta que el juego se publicó originalmente en 1999 (y en 2000 también para #Linux), eso sí, necesitas los assets del juego original

[toot](https://mastodon.social/@jugandoenlinux/112987131905872119)

---

Y hasta aquí el resumen de noticias. Como siempre podeis comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).
