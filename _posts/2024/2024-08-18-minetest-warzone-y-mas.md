---
author: P_Vader
category: "Apt-Get Update"
date: 18-08-2024 15:45:00
excerpt: "Resumen semanal de las últimas noticias publicadas en la útlima semana"
layout: post
tags:
- noticias
- resumen
title: "Minetest, Warzone, Two Point Museum y más"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20240818/minetest_5_9.webp
---

Llega otra semana con el resumen del mundo de los videojuegos en Linux. Te mostramos noticias destacadas que hemos publicado recientemente en nuestras redes sociales.

---
## Minetest 5.9

Nueva actualización de [@Minetest](https://fosstodon.org/@Minetest) ⛏️, uno de los mas famosos juegos libres, que por apariencia se le compara con Minecraft, pero realmente es mucho mas que este. Minetest es la base donde crear juegos y modificaciones sin limite.

Después de medio año llega la versión 5.9 donde destaca la inclusión de los "rayos de dios" ☀️, un efecto de iluminación bastante resultón. 😎 

En Linux es fácil instarlo en su [web](https://www.minetest.net/downloads/)
o [Flatpak](https://flathub.org/apps/net.minetest.Minetest)


[toot](https://mastodon.social/@jugandoenlinux/112948214662034324)

---
## Nuevo Warzone

Se ha lanzado una nueva revisión de Warzone 2100, la 4.5.2, que incluye entre otros:

* Numerosas correcciones de cuelgues, problemas de limpieza y fugas de memoria.

* Las unidades fijadas para mantener la posición no disparan.

* Corregidos los estados de investigación pendientes al cargar partidas.

* Mejorada la mezcla de efectos cuando la niebla está activada.

* Corregida la compatibilidad UPnP con algunos routers.

[Más info](https://wz2100.net/news/version-4-5-2/)

[toot](https://mastodon.social/@jugandoenlinux/112956791420508349)

---
## Two Point Museum

Two Point Museum es el nuevo videojuego creado por el equipo de desarrollo Two Point Studios. Este estudio es conocido originalmente por crear Two Point Hospital un juego inspirado en el clásico Theme Hospital.

Los juegos de la franquicia Two Point tienen versión para Linux y este nuevo, que saldrá próximamente en [Steam](https://store.steampowered.com/app/2185060/Two_Point_Museum/), también la tendrá. 


[toot](https://mastodon.social/@jugandoenlinux/112965786004280693)

---
## Godot 4.3

Se ha publicado la versión 4.3 del motor de videojuegos Godot.

Más de 3500 commits realizados por más de 500 contribuidores demuestran el gran éxito que está teniendo el proyecto [Godot](https://godotengine.org/) 

Mejoras en los recursos de audio, de rendimiento en la tasa de frames,en las físicas 2d, en el editor shaders, nuevo nodo TileMapLayer, soporte nativo para fbx, etc.

[Rafa Laguna](https://mastodon.gamedev.place/@rafalagoon) hizo un vídeo explicando algunas de las novedades más interesantes de esta versión.

<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/SSAXLPs18bU?si=gGs4vKXvdio8fn5d" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

Si te interesa síguelo en [@todogodot](https://mastodon.gamedev.place/@todogodot)


[toot](https://mastodon.social/@jugandoenlinux/112968519546757737)

---
## Proton para viejas gráficas.
Proton se ha convertido en una herramienta imprescindible para los aficionados de los videojuegos en Linux. En la búsqueda de mejoras de rendimiento se actualiza haciendo uso de nuevas tecnologías como es el caso de Vulkan 1.3

Sin embargo no todos tienen un hardware a la última por lo que un usuario ha hecho un [fork de Proton](https://github.com/pythonlover02/Proton-For-Old-Vulkan) que incluye versiones anteriores de DXVK que son compatible con Vulkan 1.2 y por tanto con GPU más antiguas.

[toot](https://mastodon.social/@jugandoenlinux/112971631314773248)

---
## SteamOS tendrá soporte para Asus Rog

Vía The Verge nos enteramos de que Valve dará soporte en SteamOS 3 a la portátil de Asus, Rog Ally, sorprendiendo a propios y a extraños. Para más detalles, la [noticia original](https://www.theverge.com/2024/8/13/24219469/valve-steamos-asus-rog-ally-steady-progress-dual-boot) (en inglés).

[toot](https://mastodon.social/@jugandoenlinux/112961628968495672)

---
## FEX EMU para Arm

La mayoría de videojuegos para PC están compilados para la arquitectura x86 y x86-64 puesto que es la que usan AMD e Intel.

FEX EMU es una herramienta que te permite ejecutar binarios con arquitectura x86 y x86-64 en dispositivos ARM como por ejemplo la Raspberry Pi.

Recientemente han sacado una [nueva versión](https://github.com/FEX-Emu/FEX/releases/tag/FEX-2408), además tenéis el [listado de videojuegos](https://wiki.fex-emu.com/index.php/Category:Game_Type_Application) que se pueden ejecutar.

---
## Nueva ChimeraOS 46-2


Si bien Steam Deck no ha sido el primer ordenador de videojuegos portátil, si que podemos decir que ha sido el que los ha popularizado y eso ha hecho que marcas como Lenovo o Asus saquen los suyos.

Desgraciadamente, estos últimos vienen con Windows y no con Linux.

Si has adquirido la nueva Asus Rog Ally X y quieres ponerle Linux tenemos buenas noticias. Recientemente se publicó [ChimeraOS 46-2](https://github.com/ChimeraOS/chimeraos/wiki/Release-Notes#chimeraos-46-2-2024-08-10) que incluye soporte completo para este hardware.

[toot](https://mastodon.social/@jugandoenlinux/112949868583707090)

---

Y hasta aquí el resumen de noticias. Como siempre podeis comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).