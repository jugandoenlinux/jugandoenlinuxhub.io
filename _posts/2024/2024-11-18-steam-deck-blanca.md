---
author: P_Vader
category: "Apt-Get Update"
date: 18-11-2024 19:25:00
excerpt: "Resumen semanal de las noticias publicadas en Mastodon en la última semana"
layout: post
tags:
- noticias
- resumen
title: "Resumen semanal edición limitada"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20241118/steamdeck_blanca.webp
---


Una semana muy tranquila aunque siempre hay alguna noticia interesante que publicamos en nuestra cuenta de [Mastodon](https://mastodon.social/@jugandoenlinux)


				
## [Steam Deck blanqueada](https://mastodon.social/@jugandoenlinux/113470363435997572)

Valve anunció una edición limitada de la Steam Deck en blanco.
Saldrá a la venta en todo el mundo el 19 de noviembre a las 0:00 a un precio de 719€ 

La verdad que luce muy bien, aunque con la cantidad de formas que hay de "tunear" la consola, no sabemos si es algo excesivo la "exclusividad".

En su [web](https://store.steampowered.com/steamdeck) puede encontrar mas información.


---

## [Half-Life 2 de aniversario](https://mastodon.social/@jugandoenlinux/113492430448368872)

Por su 20 aniversario Valve regala estos días, y para siempre, Half-Life 2, incluyendo además los 2 Episodios y Lost Coasts, un pequeño mapa en una isla a la que llegue Freeman y debe de averiguar y parar los planes de La Alianza. Además incluye un montón de novedades a nivel gráfico, de jugabilidad y de correcciones que podréis ver en la noticia oficial

[Noticia en Steam](https://store.steampowered.com/news/app/220/view/6633333780228604001?l=spanish)

Si un no tienes este gran juego aún estás a tiempo de hacerte con él.

---

## [Gran herramienta para tu GPU](https://mastodon.social/@jugandoenlinux/113486162744284220)

LACT es una aplicación que te permite controlar la configuración de tu GPU ya sea para hacer overclocking, controlar sus ventiladores o monitorizar sus valores de temperatura y de consumo energético.

Con la nueva versión, la 0.6.0, ahora da soporte para tarjetas gráficas Nvidia y lo hace incluso en Wayland gracias a la biblioteca NVML.

Podéis ver el Changelog de la versión en su [GitHub](https://github.com/ilya-zlobintsev/LACT/releases)

---

## [Yawnoc](https://mastodon.social/@jugandoenlinux/113482271196812763)

Yawnoc es un pequeño roguelite creado por dafluffypotato, un programador que usa Python y Pygame para desarrollar sus videojuegos y que ha tenido el acierto de incluir el código fuente del juego en la versión de Steam. 

[Yawnoc](https://store.steampowered.com/app/2824730/Yawnoc/)

---

## [Nuevo Proton experimental](https://mastodon.social/@jugandoenlinux/113477646248673037)

Tenemos nueva versión de Proton experimental, que entre los múltiples cambios que contiene, llega el soporte para DLSS 3 Frame Generation.

Por otro lado ahora son jugables: APB Reloaded, Total War: SHOGUN 2, Welcome to Dustown, Warhammer 40k: Space Marine 2 (requiere deshabilitar Steam Overlay), Sniper Elite: Nazi Zombie Army 2 (solo para AMD GPUs), Test Drive Unlimited Solar Crown, Hard Chip Demo, Conqueror's Blade y ScarQuest.

[Changelog](https://github.com/ValveSoftware/Proton/wiki/Changelog)

---

Y hasta aquí una semana mas el resumen de noticias. Como siempre podéis comentar estas noticias en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), [Telegram](https://t.me/jugandoenlinux) o en nuestro [Mastodon](https://mastodon.social/@jugandoenlinux)
