---
author: Son Link
category: Editorial
date: 2024-12-30 09:00:00
excerpt: Como cada año, hacemos un resumen de lo que ha significado el año para nosotros
image: 
layout: post
tags:
  - resumen
  - opinion
title: Resumen del año 2024 de JeL
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/resumen-2024.webp
---
Otro año está a punto de acabar, y como otros años, el equipo de Jugando en Linux hacemos un resumen del año que acaba, así como que juegos han sido para cada uno el juego del año.

Aunque este año tampoco ha sido el “Año de Linux en el escritorio”, sí que lo ha sido en consolas, tanto en las consolas emuladoras (como la RS36S), como en las tipo Steam Deck (que no dejan de ser ordenadores más pequeños). Además, este año también ha estado plagado de novedades, nuevas versiones de juegos cargadas de muchas novedades, y un largo etcétera.

Antes de dar paso al resumen y a los juegos favoritos de este año del staff, os deseamos un feliz año nuevo, y que el año que viene sea mejor que este 2024.

Atentamente: el staff de Jugando en Linux.

## P_Vader

Este año me compre 3 juegos para las vacaciones y han sido lo que mas me ha entretenido. Me compré Dave the Diver (entre los mejores juegos de 2023), Bleak Sword DX y Balatro (mejor juego indie del 2024) que aunque no me llama la atención los juegos de cartas, mi compi Serjor me lo recomendó y menuda recomendación. Balatro es mucho mas que un juego de cartas y un pierde horas peligroso.

Respecto a juegos libres este año han sido muy escasos en mi caso. Siempre le hecho un ojo a Veloren por su calidad y el aprecio personal que le tengo al proyecto. También este año nos hicieron llegar un proyeccto curioso de videojuego, hecho en Godot y que es GPL3. Se llama Delta-Park y además estará disponible en Steam. Os invito a que lo probéis.

* Juego no Libre: **Balatro** 
* Juego libre: [Delta-Park](https://store.steampowered.com/app/2632680/Parque_del_Delta/)


## Serjor

Pues no sé yo cómo valorar este año. Lo voy a terminar jugando a Cult of the Lamb, y lo comencé con balatro, así que creo que no me puedo quejar, porque vaya juegardos.

Y junto a estos, Elden Ring, GTA V, Deadlock, Dota2, X-COM 2, Halo: TMCC... Ni tan mal

* Juego no libre favorito de este año: GTA V

Ahora, como linuxero y fan del software libre, este año no he jugado a ningún juego libre. La falta de tiempo y una biblioteca inmensa en Steam hacen que no mire más allá, y bucee dentro de los juegos que ya tengo, así que no puedo opinar en este frente.

Me anoto como propósito del 2025 darle a un juego de código abierto, que son muy buenos.

## Son Link

Para mi este ha sido un buen año, con muchos juegos nuevos, tanto nativos como a través de Proton, lanzamiento de versiones mayores de varios juegos libres, como Battle for Wesnoth, Freedoom y Warzone 2100, o las primeras betas de la próxima versión de OpenTTD, sin olvidarme de proyectos aún en desarrollo como LibreQuake. Como punto negativo, es que este año tampoco se ha lanzado la Alpha 27 de 0 A.D., si bien el proyecto sigue activo.

Creo que el 2025 va a ser otro gran año para jugar en Linux, con más juegos nuevos, la evolución de muchos proyectos libres, mejoras en Mesa y a nivel de kernel, etc.

* Juego libre favorito de este año: LibreQuake
* Juego no libre favorito de este año: Deadlock