---
author: Odin
category: "Apt-Get Update"
date: 23-09-2024 10:56:00
excerpt: "Resumen semanal de las noticias publicadas de la tercera semana de Septiembre en Mastodon."
layout: post
tags:
- noticias
- resumen
title: "God of War Ragnarök en Linux, Día Internacional del software libre, JunkStore, y otras noticias."
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20240922/godofwarragnarok.webp
---
Empezamos este lunes con una recopilación de las noticias que hemos ido publicando en nuestro [Mastodon](https://mastodon.social/@jugandoenlinux/) durante la semana pasada.

## [Día Internacional del software libre](https://mastodon.social/@jugandoenlinux/113177367567611470)
Hoy es el día internacional del Software Libre, y por ello hoy queremos mencionar a varios proyectos libres vinculados al gaming que tienen cuenta en Mastodon:

* [play0ad](https://mastodon.social/@play0ad)
* [liblast](https://mastodon.social/@liblast@mastodon.gamedev.place)
* [veloren](https://mastodon.social/@veloren@floss.social)
* [freeciv](https://mastodon.social/@freeciv@fosstodon.org)
* [openttd](https://mastodon.social/@openttd@fosstodon.org)
* [wesnoth](https://mastodon.social/@wesnoth@fosstodon.org)
* [FreeRCT](https://mastodon.social/@FreeRCT@floss.social)
* [Speed dreams official](https://mastodon.social/@speed_dreams_official)
* [supertux](https://mastodon.social/@supertux@floss.social)
* [simutrans](https://mastodon.social/@simutrans@fosstodon.org)
* [Minestest](https://mastodon.social/@Minetest@fosstodon.org)
* [lutris](https://mastodon.social/@lutris@fosstodon.org)
* [heroiclauncher](https://mastodon.social/@heroiclauncher)
* [scummvm](https://mastodon.social/@scummvm@manitu.social)
* [godotengine](https://mastodon.social/@godotengine@mastodon.gamedev.place)
* [openrazer](https://mastodon.social/@openrazer@fosstodon.org)

## [Bazzite en Asus Rog Ally X](https://mastodon.social/@jugandoenlinux/113175429388849531)
Los desarrolladores de Bazzite nos cuentan, en un interesante artículo, como consiguieron dar soporte para la Asus Rog Ally X.

Y es que Bazzite se está convirtiendo en la distribución de referencia para ejecutar Linux en estas videoconsolas/odenadores portátiles.

El artículo completo lo tenéis en el siguiente [enlace](https://universal-blue.discourse.group/t/achieving-day-0-rog-ally-x-support-in-bazzite/4165). 

## [Nueva actualización de Minetest](https://mastodon.social/@jugandoenlinux/113174350103076886)
Uno de nuestros juegos libres favoritos es [Minetest](https://www.minetest.net/) y hace pocos días tuvo una pequeña actualización.

Se han corregido muchos casos y destaca arreglos en el renderizado. Si tenías algún problema con las novedades gráficas que introdujeron, quizás se haya solucionado.
[Changelog](https://dev.minetest.net/Changelog#5.9.0_.E2.86.92_5.9.1)

Recuerda que Minetest es fácil de instalar en muchas distros, incluido como flatpak:
[Descargas](https://www.minetest.net/downloads/)

Y FELIZ DÍA DEL SOFTWARE LIBRE ❤️ ! 

## [Heroic Launcher se actualiza con nuevos bugfixes](https://mastodon.social/@jugandoenlinux/113171922741162038)
Una pequeña actualización de heroiclauncher para resolver algunos problemas con las diferentes tiendas:
[HeroicGamesLauncher v2.15.2](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher/releases/tag/v2.15.2)

- La librería de Amazon a veces no se sincronizaba.
- El salvado en la nube de GOG no se actualizaba si lo eliminabas desde la web.
- Algun juego de Epic tenía problemas.

Recuerda que este lanzador recomienda instalarse como flatpak o appimage desde su web:
https://heroicgameslauncher.com/downloads

## [Nueva versión con mejoras de Proton](https://mastodon.social/@jugandoenlinux/113170712451890792)
Tenemos nueva versión de Proton. En concreto la 9.0-3 que permite ejecutar de forma jugable nuevos juegos como Avatar: Frontiers of Pandora, Flatout 3, Sleeping Dogs: Definitive Edition, etc.

También arregla regresiones que impedían ejecutar videojuegos como Grid Autosport y Grid 2 entre otros.

Si queréis ver la lista completa de cambios la podéis ver en [GitHub](https://github.com/ValveSoftware/Proton/releases/tag/proton-9.0-3)

## [Apúntate al PlayTest de Dawnfalk](https://mastodon.social/@jugandoenlinux/113169295152565625)
Dawnfolk (anteriormente llamado Lueur) fue una grata sorpresa  cuando estuvimos probando su demo.

Ahora su desarrollador [darennkeller](https://mastodon.social/@darennkeller@mastodon.gamedev.place) invita a todo el mundo a un nuevo playtest para conocer tu opinión. Destaca que se han añadido muchas novedades y cambios para hacer mas atractivo el juego:
<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/PWESwfPPy3s?si=KYCeuc96qBrupGWS" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

[Steam](https://store.steampowered.com/app/2308630/Dawnfolk/)

Lo tienes nativo en Linux y funciona muy bien en Steam Deck. ¿Te apuntas?

## [God of War Ragnarök en Linux y Steam Deck](https://mastodon.social/@jugandoenlinux/113166640730960760)
Ya ha salido publicado God of War Ragnarök en Steam, la segunda parte de una de las sagas más exitosas de Sony.

Si te estás preguntando si funciona en Linux, te alegrará saber que ya hay videos del juego funcionando en Steam Deck, pero por ahora sí lo quieres hacer funcionar en tu PC, tendrás que añadir la opción SteamDeck=1 %command% para poder ejecutarlo.

* [Steam](https://store.steampowered.com/app/2322010/God_of_War_Ragnarok/)
* [Proton issue God of War Ragnarök](https://github.com/ValveSoftware/Proton/issues/8107)

## [Boomstock Festival](https://mastodon.social/@jugandoenlinux/113165335502737110)
¿Te gustan los juegos de disparos de la vieja escuela, los #Doom, Duque Nukem, Worms, etc.?

Pues ha empezado en Steam el festival Boomstock, donde encontraras muchos juegos en oferta, incluso varias demos y juegos gratis, varios de ellos nativos y verificados para Steam Deck 

[Steam Boomstock 2024](https://store.steampowered.com/sale/Boomstock2024)

## [Protege tu Steam Deck gracias a la impresión 3d](https://mastodon.social/@jugandoenlinux/113159484147894613)
¿Tienes una Steam Deck y puedes imprimir piezas 3D? Aquí os dejamos otro impresionante proyecto y extremadamente útil para mantener a "salvo"  tu consola.

En este caso se trata de una pieza que se ajusta en el interior de la funda manteniendo la consola bien sujeta dentro y evitar caídas en caso de apertura accidental: 
[Printables](https://www.printables.com/model/254680-steam-deck-lifesaver-drop-protection?lang=es) 

¿Alguna vez has tenido un susto con la funda?

## [Sácale el máximo partido a tu Steam Deck con la JunkStore](https://mastodon.social/@jugandoenlinux/113152803487914468)

Si tienes una Steam Deck, estamos seguros de que la estarás aprovechando al máximo, sin embargo puede que no todos tus juegos los tengas en Steam si o en GOG o en *esa tienda que regalan juegos y que su dueño no está muy a favor de Linux y por eso no la promocionamos*.

Para esos juegos tienes Heroic, Lutris, Bottles..., pero no dejan de ser aplicaciones de escritorio. Si quieres algo integrado en Steam game mode te recomendamos [JunkStore](https://github.com/ebenbruyns/junkstore)

JunkStore se distribuye como plugin de Decky Loader, y esta siendo desarrollado con licencia BSD, por lo que es software libre. Hay que tener en cuenta que la capacidad de instalar juegos de GOG está solo disponible para los usuarios que apoyan el proyecto subscribiéndose a su [Patreon](https://www.patreon.com/junkstore) o haciendo un pago único en su store de 6 euros en [Ko-fi](https://ko-fi.com/junkstore) o en el mismo shop de Patreon.

Te animamos a que lo pruebes, y si te gusta apóyales de alguna de las maneras que recomiendan en su [web](https://www.junkstore.xyz/support/)

Y hasta aquí el resumen de noticias. Como siempre podeis comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).