---
author: P_Vader
category: "Tutorial"
date: 19-08-2024 16:00:00
excerpt: "Analisis de velocidades de espacio externo en la Steam Deck"
layout: post
tags:
- tutorial
- steam-deck
- hardware
title: "El mejor y peor almacenamiento para la Steam Deck"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AlmacenamientoParaLaSteamDeck/steamdeck_microsd.webp
---

Os traemos una extensa comparativa que hizo en su día [Taranasus](#el-video) en su canal de youtube con las diferentes **velocidades que se obtienen en la Steam Deck cargando el juego Horizon Zero Dawn** según el almacenamiento externo que uses.
Puedes ver diferencias entre PC, portatil, steamdeck y en algún caso con una PlayStation.

---
Claramente usar un NVME de manera externa usando un cable adecuado te da la mayor velocidad, cercana a la unidad interna de la consola portatil.

| Plataforma | Unidad | Modelo | Interfaz | Partición | SO | Tiempo |
| --- | --- | --- | --- | --- | --- | --- |
| Steam Deck | NVMe | Kingston OM3PDP3256B-A01 | Interno | EXT4 | SteamOS | 00:23 |
| Steam Deck | NVMe | Samsung 980 | USB C | EXT4 | SteamOS | 00:25 |
| PS5 | NVMe |??? | Interno | PFS | PS5 | 00:26 |
| Laptop | NVMe | SAMSUNG MZFLV256HCHP | Interno | NTFS | Windows 11 | 00:34 |

---
Usar MicroSD es lo mas comodo. Las microsd de categoria V30 es el máxima rendimiento que se puede obtener en el caso de la Deck.

| Plataforma | Unidad | Modelo | Interfaz | Partición | SO | Tiempo |
| --- | --- | --- | --- | --- | --- | --- |
| Steam Deck | MicroSD V30 | Samsung MB-MD256KA/APC | Lector microsd | EXT4 | SteamOS | 00:51 |
| Steam Deck | MicroSD V60 | ProGrade MICROSDXC UHS-II | Lector microsd | EXT4 | SteamOS | 00:53 |
| PS4 | HDD | HGST Travelstar 5K1000 | Interno | PFS | PS4 | 01:03 |
| Laptop | MicroSD V30 | Samsung MB-MD256KA/APC | Lector microsd | EXT4 | Pop OS | 01:04 |
| Steam Deck | MicroSD V10 | QUMOX QX-MCSDXC-I-128G | Lector microsd | EXT4 | SteamOS | 01:05 |
| PC | HDD | Western D. WDBBEP0010BBK | USB 3.0 | NTFS | Windows 11 | 01:25 |
| Laptop | HDD | Western D. WDBBEP0010BBK | USB 3.0 | NTFS | Windows 11 | 01:29 |
| Laptop | MicroSD V30 | Samsung MB-MD256KA/APC | Lector microsd | NTFS | Windows 11 | 01:36 |
| Steam Deck | HDD | Western D. WDBBEP0010BBK | USB-C Apple | EXT4 | SteamOS | 02:03 |
| Laptop | HDD | Western D. WDBBEP0010BBK | USB-C Apple | EXT4 | Pop OS | 02:04 |
| Laptop | HDD | Western D. WDBBEP0010BBK | USB 3.0 | EXT4 | Pop OS | 02:06 |
| Laptop | HDD | Western D. WDBBEP0010BBK | USB 3.0 | EXT4 | Manjaro | 02:14 |

---
El uso de USB 2.0 o los discos duros mecanicos es no es opción valida.

| Plataforma | Unidad | Modelo | Interfaz | Partición | SO | Tiempo |
| --- | --- | --- | --- | --- | --- | --- |
| Steam Deck | NVMe | Samsung 980 | USB C / USB 2.0 | EXT4 | SteamOS | 01:40 |
| PC | HDD | Western D. WDBBEP0010BBK | Cable USB 2.0 | NTFS | Windows 11 | 02:33 |
| Steam Deck | HDD | Western D. WDBBEP0010BBK | Cable USB-C / 2.0 | EXT4 | SteamOS | 02:55 |
| Laptop | HDD | Western D. WDBBEP0010BBK | Cable USB-C / 2.0 | EXT4 | Pop OS | 03:09 |

---
### El video:
<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/8RjUz4HAo8s?si=1VrMXHRY2o6yB8jg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>


