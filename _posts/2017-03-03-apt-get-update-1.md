---
author: Pato
category: Apt-Get Update
date: 2017-03-03 19:35:25
excerpt: "<p>Llegamos una semana m\xE1s... al fin de semana y como siempre hay que\
  \ hacer update</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/58b053c805beeea9e04dde1724076741.webp
joomla_id: 237
joomla_url: apt-get-update-1
layout: post
title: Apt-Get Update Heroes of Arca & Graveyard Keeper & Stellaris Utopia & System
  Shock Reboot...
---
Llegamos una semana más... al fin de semana y como siempre hay que hacer update

Vamos una semana más con todo lo que no hemos publicado y que por diferentes motivos no pudimos publicar... 3, 2, 1, GO! (VROOOOOOMMM...)


Esta semana comenzamos con un saludo a muylinux [[www.muylinux.com](http://www.muylinux.com/)], la página de referencia de muchos de los que nos movemos en esto del sistema operativo del pingüino y que la semana pasada nos saludaron a su vez en su habitual "[Linux Play](http://www.muylinux.com/2017/02/25/linux-play-63)". ¡Gracias compañeros!.


Al grano. Pues a finales de la semana pasada en muylinux nos enseñaron [en un (grán) artículo](http://www.muylinux.com/2017/02/24/integrar-nvidia-pascal-ubuntu) cómo podemos integrar una gráfica Nvidia Pascal en Ubuntu Unity para no sufrir con el "tearing", "popping" y otras hierbas, cosa que para los jugones que monten las últimas gráficas de Nvidia les vendrá que ni pintado. Sin embargo, la cosa les quedó un pelín "densa" para el común de los mortales, (vamos, que había que meterse en harína con la terminal) y hoy publican otro artículo donde lo dejan todo clarito y sin apenas tener que tocar una línea de código. Puedes ver este procedimiento simplificado [en este enlace](http://www.muylinux.com/2017/03/03/ubuntu-unity-nvidia-pascal-tearing). Rápido, fácil y para toda la familia... ¡Simplemente genial!


Desde [www.gamingonlinux.com](http://www.gamingonlinux.com) :


- Heroes of Arca, un juego de rol y estratégia por turnos visualmente muy llamativo llega a Linux, y Liam le da un repaso. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/heroes-of-arca-a-turn-based-strategy-and-rpg-released-for-linux-some-thoughts.9193).


- Kronos, el consorcio detrás de Vulkan anuncia el desarrollo de OpenXR, el nuevo estandar que nos traerá realidad virtual y aumentada. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/khronos-announce-openxr-their-new-standard-for-virtual-reality-and-augmented-reality.9198).


- Valve está desarrollando una nueva interfaz para su cliente de Steam. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/valve-are-working-on-a-new-design-for-the-steam-client.9215).


- Munch VR es un juego para RV que ha lanzado una versión experimental para Linux. Ojo, puede dar errores. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/munch-vr-has-released-an-experimental-linux-version-munch-your-way-up-the-food-chain.9228).


- Graveyard Keeper es un juego donde tendrás que gestionar... un cementerio. Puedes [verlo en este enlace](https://www.gamingonlinux.com/articles/tinybuild-announce-graveyard-keeper-an-inaccurate-medieval-cemetery-management-sim.9230).


Desde [www.linuxgameconsortium.com](http://www.linuxgameconsortium.com):


- Stellaris: Utopia es la nueva expansión que llegará el 6 de abril. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/stellaris-utopia-first-expansion-release-date-announced-48612/).


- Total War: Warhammer nos trae nuevos contenidos con Isabella Von Carstein y Bretonnia. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/total-war-warhammer-gaming-gets-isabella-von-carstein-knights-bretonnia-48740/).


- Cities: Skylines nos trae nueva expansión con "Mass Transit". Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/cities-skylines-expansion-mass-transit-announced-48674/).


- Europa Universalis se expande con nuevo contenido con "Mandate of Heaven". Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/mandate-of-heaven-expansion-announced-europa-universalis-4-48807/).


- System Shock Reboot cambia su motor a Unreal Engine 4 y parece que va por buen camino para llegar a Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/system-shock-reboot-first-person-development-switches-unreal-engine-4-video-48832/).


Vamos con los vídeos de la semana:


Stellaris Utopia:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/YmvGlBFaFEU" width="640"></iframe></div>


 TW: Warhammer Bretonnia


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/60WQUG3XW3M" width="640"></iframe></div>


 System Shock Reboot


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/IPcFU6kXeZ4" width="640"></iframe></div>


 Graveyard Keeper


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/GmZS6XFBJfQ" width="640"></iframe></div>


 Esto es todo por este Apt-Get Update. ¿Qué te ha parecido? Seguro que me he dejado muchas cosas en el tintero.


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

