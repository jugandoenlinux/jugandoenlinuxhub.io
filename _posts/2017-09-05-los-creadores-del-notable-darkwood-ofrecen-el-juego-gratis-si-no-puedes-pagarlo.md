---
author: Pato
category: Terror
date: 2017-09-05 15:04:39
excerpt: "<p>Acid Wizard Studio publica una versi\xF3n estable en thePirateBay</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6d1e5c49b492af74c789e86df6f68797.webp
joomla_id: 454
joomla_url: los-creadores-del-notable-darkwood-ofrecen-el-juego-gratis-si-no-puedes-pagarlo
layout: post
tags:
- accion
- indie
- terror
title: Los creadores del notable 'Darkwood' ofrecen el juego gratis si no puedes pagarlo
---
Acid Wizard Studio publica una versión estable en thePirateBay

Hace menos de un mes que [informábamos del lanzamiento oficial](index.php/homepage/generos/terror/item/557-darkwood-por-fin-sale-de-acceso-anticipado-y-llega-oficialmente-a-linux-hoy-dia-de-lanzamiento) de 'Darkwood' [[web oficial](http://www.darkwoodgame.com/)] y hace solo unos días, el estudio detrás del juego "Acid Wizard Studio" formado por solo tres personas (y un perro) con miedo a jugar juegos de terror, publicaron [un extenso post](http://imgur.com/a/xVhDz) donde contaban cómo hace cuatro años dejaron sus trabajos y sin tener experiencia en desarrollo han llegado a crear y publicar uno.


El desarrollo del juego como ellos cuentan tuvo muchos altibajos, con momentos en los que la comunidad criticaba el desarrollo por que parecía que estaba abandonado, o las dificultades de los desarrolladores para cumplir los plazos fijados por ellos mismos al no calcular correctamente la complejidad del desarrollo. Pero lo realmente llamativo de la historia es que tras el éxito inicial del lanzamiento del juego, donde afirman haber vendido más de la mitad de copias que en la campaña de financiación y de las críticas del juego que fueron bastante positivas, han decidido publicar una copia "estable" del juego en thepiratebay. ¿Por que?


La respuesta está en su visión respecto a las devoluciones y la reventa de claves. Afirman comprender el funcionamiento de las devoluciones de Steam, las cuales permiten probar el juego durante un par de horas y si no te convence puedes pedir la devolución del dinero, lo que sirve para probarlo para ver si te gusta o no. Pero lo que les movió a "regalar" su juego fue un usuario que les afirmó devolver el juego por que no quería que sus padres se encontrasen sin dinero a final de mes.


Esto les causó una gran impresión, lo que les llevó a afirmar que *si no puedes pagar el juego por cualquier motivo y quieres jugarlo, hemos puesto un torrent de una copia segura de Darkwood (1.0 hotfix 3) estable en Thepiratebay completamente libre de DRM. No hay trampa ni cartón, ningun sombrero pirata en los personajes ni nada de eso. Solo tenemos una petición: Si te gusta el juego y quieres que continuemos haciendo juegos, considera comprarlo en el futuro, puede que en unas rebajas, en Steam, GOG o Humble Store. Pero por favor, no lo compres a través de ninguna página de reventa de claves. Haciendo eso, estás alimentando el cancer que está devorando esta industria.*


Sin duda el tema de la reventa de claves es un punto caliente respecto a su forma de actuar. Por nuestra parte tenemos pendiente el publicar un artículo sobre este tema pero lo cierto es que cuando tantos estudios y desarrolladores están criticando este tipo de páginas es por algo, y pronto lo expondremos en jugandoenlinux.com.


'Darkwood' es un juego de terror en vista cenital con una ambientación muy conseguida y donde tendremos que sobrevivir en un mundo cambiante donde las sombras esconden horrores:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/3S3tmWfFACQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Si quieres ver el enlace al torrent, lo tienes al final del[post original del estudio](http://imgur.com/a/xVhDz). Si quieres comprarlo [lo tienes en GOG](https://www.gog.com/game/darkwood), y si quieres probar el juego en Steam lo tienes en su página:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/274520/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Qué te parece el movimiento del estudio respecto a poner su juego en thepiratebay? ¿Piensas descargarlo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 


Fuente: [Steam](http://steamcommunity.com/gid/103582791435461441/announcements/detail/1448325533553949067), [Reddit](https://www.reddit.com/r/pcgaming/comments/6w0909/darkwood_creators_upload_the_full_game_to_the/)


 

