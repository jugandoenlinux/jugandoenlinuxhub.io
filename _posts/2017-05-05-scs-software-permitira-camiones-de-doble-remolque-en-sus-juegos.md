---
author: leillo1975
category: "Simulaci\xF3n"
date: 2017-05-05 14:28:36
excerpt: "<p>Euro Truck Simulator 2 y American Truck Simulator recibir\xE1n esta actualizaci\xF3\
  n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/963c54073e784a324883122381877c85.webp
joomla_id: 308
joomla_url: scs-software-permitira-camiones-de-doble-remolque-en-sus-juegos
layout: post
tags:
- ets2
- american-truck-simulator
- ats
- scs-software
- euro-truck-simulator-2
- remolques-dobles
title: "SCS Software permitir\xE1 camiones de doble remolque en sus juegos"
---
Euro Truck Simulator 2 y American Truck Simulator recibirán esta actualización

Acabo de leer ahora mismo una noticia que me ha alegrado el día. SCS Software acaba de anunciar con una [nueva entrada en su blog](http://blog.scssoft.com/2017/05/doubles.html) que tras mucho tiempo de desarrollo está preparando para una próxima actualización la **inclusión de la los camiones con doble remolque** en sus juegos [Euro Truck Simulator 2](http://store.steampowered.com/app/227300/Euro_Truck_Simulator_2/) y [American Truck Simulator](http://store.steampowered.com/app/270880/American_Truck_Simulator/). Se tratará de una **actualización gratuita** y será lanzada a **principios de este verano** si todo transcurre normalmente.


 



> 
> Double trailers are coming to ETS2 and ATS in the next free update! <https://t.co/yRHrS9uVH2> [pic.twitter.com/KfLpolEsBj](https://t.co/KfLpolEsBj)
> 
> 
> — SCS Software (@SCSsoftware) [May 5, 2017](https://twitter.com/SCSsoftware/status/860493702074118146)



 


SCS Software ha admitido que la inclusión de este tipo de camiones era una demanda de los usuarios desde hace bastante tiempo. También ha aclarado que esta actualización llega tras muchas reuniones internas, muchas fustraciones y problemas para hacer que las físicas en el juego funcionasen. SCS Software es una compañía con un gran respeto por sus usarios y continuamente estan dotando de mejoras y contenido sus títulos. Sin duda es una gran noticia que todo fan de estos juegos debería estar celebrando, como es mi caso, por supuesto. **BRAVO como siempre SCS Software.**


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATSDoble.webp)


 


¿Qué os parece este anuncio? ¿Estais deseando probar los nuevos camiones dobles? Deja tus impresiones en los comentarios o en nuestros caneles de [Telegram](https://telegram.me/jugandoenlinux) o [Discord.](https://discord.gg/ftcmBjD)

