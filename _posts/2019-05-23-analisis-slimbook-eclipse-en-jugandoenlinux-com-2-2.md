---
author: Pato
category: "An\xE1lisis"
date: 2019-05-23 16:53:03
excerpt: "<p>Concluimos el an\xE1lisis del Slimbook Eclipse testeando videojuegos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b998737c1fadb4532aff61629ed0f96a.webp
joomla_id: 1034
joomla_url: analisis-slimbook-eclipse-en-jugandoenlinux-com-2-2
layout: post
tags:
- hardware
- analisis
- slimbook
title: "An\xE1lisis: Slimbook Eclipse en Jugandoenlinux.com -2/2-"
---
Concluimos el análisis del Slimbook Eclipse testeando videojuegos

Tras la "presentación" del **Slimbook Eclipse** que hicimos en la [primera parte](index.php/homepage/analisis/20-analisis/1155-analisis-slimbook-eclipse-en-jugandoenlinux-com-1-2) de este análisis, es hora de meternos en faena con los videojuegos, que al fin y al cabo de esto es de lo que se trata. Antes de nada, volver a agradecer a Slimbook el habernos prestado este Slimbook Eclipse para realizar las pruebas.


Lo primero que hacemos es actualizar los drivers gráficos de la gráfica Nvidia [según el PPA](index.php/foro/drivers-graficas-nvidia/19-instalar-el-ultimo-driver-propietario-de-nvidia-en-ubuntu-y-derivadas). En el momento del análisis los drivers actualizados son los 418.


![Driversppa](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/slimbook/Driversppa.webp)


Una vez actualizados, comenzamos con las pruebas:


Empezamos por **Tomb Raider**, un clásico ya en cuanto a las pruebas en Linux que fue portado por Feral Interactive. El test se hizo con la configuración gráfica mas alta que permitía el juego dando los resultados siguientes:


 


![Tombradr](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Slimbook/Tombradr.webp)


Como podéis ver, la GTX1060 es perfectamente capaz de mover el juego sin problemas a un nivel de FPS más que aceptable. Durante la sesión de juego no hubo ningún problema de ejecución yendo fluido en todo momento. A continuación tenéis el vídeo de la prueba donde podéis ver el contador de FPS en la parte superior izquierda. Tener en cuenta que hay diferencias de rendimiento debido a que estábamos grabando con OBS a la vez que lo ejecutábamos:


Tomb Raider:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/fNGrJmWKIrY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Continuamos con otro peso pesado, el **F1 2017**. En este caso, el juego de Codemasters emplea la API Vulkan gracias a Feral Interactive. La prueba arrojó el siguiente resultado:


![F12017](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Slimbook/F12018.webp)


El F1 2017 es un juego exigente, pero aún así el Slimbook Eclipse es capaz de mantener los FPS mínimos por encima de los 30 y una média de 56 FPS durante la partida. A continuación tenéis el vídeo con la prueba realizada donde también tenéis el contador de FPS en tiempo real. Recordar que hay cierta diferencia con los resultados mostrados debido a que estábamos grabando la sesión con OBS lo que siempre tiene un impacto de entre un 10% - 15% según casos.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Tzuhutp5UK4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Seguimos las pruebas, esta vez con **DiRT Rally**. Un juego que también es capaz de exprimir procesador y gráfica mientras corremos por caminos terreros a toda velocidad. En este caso los resultados fueron los siguientes:


![Dirtrally](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Slimbook/Dirtrally.webp)


En Dirt Rally los FPS mínimos quedaron justo al límite de los 30, pero aún así el juego ofreció una experiencia de juego fluida arrojando medias de más de 37 FPS durante toda la sesión a un nivel de calidad muy alto. A continuación tenéis el vídeo de la prueba con el contador de FPS en vivo. Recordar que hay diferencia de rendimiento e incluso puede verse algún pequeño salto debido a que estábamos grabando con OBS y en los tramos más exigentes es posible que los FPS bajasen más que en la prueba real:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/LShGW1yG214" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 Pasamos ya a juegos que aunque no sean tan exigentes a nivel de potencia sí que son interesantes de ver moviéndose en un equipo de estas características.


**Distance** es un juego de carreras futurista donde los coches son capaces de hacer piruetas, saltos e incluso salir volando. Los gráficos son muy llamativos y la experiencia de juego con el Slimbook Eclipse fue totalmente satisfactoria:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/_nnuh9IH3M4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Seguimos con **Classic Racers**, un juego que hace poco nos descubrió nuestro compañero Leillo y con el que el Slimbook Eclipse no tuvo ningún tipo de problema:


 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/-lMt0FiPO_c" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Classic Racers no tiene un modo benchmark pero dada la potencia del Slimbook Eclipse tampoco hace falta, tal y como puede verse en el contador de FPS y con el OBS grabando.


Que pase el siguiente. **Horizon Chase Turbo** es un juego de carreras arcade del estilo de las antiguas recreativas donde la diversión está asegurada siempre que se mueva fluido:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/hwhw2c-hSBg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 Por último, una sesión de carreras de vainas futuristas con **BallisticNG**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/YiegbfJB0eg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 **Conclusión:**


El **Slimbook Eclipse es un portátil** que cumple las expectativas en cuanto a juegos. Tiene potencia a raudales y es perfectamente capaz de mover los juegos actuales a niveles altos o muy altos, e incluso ultra para juegos menos punteros. Es cierto que no monta la serie gráfica más alta, ni el chipset de última hornada, pero es capaz de mantener el tipo además suministrando la suficiente potencia para jugar de forma satisfactoria manteniendo una adecuada experiencia de juego incluso con los juegos más exigentes siempre que se les ajusten los parámetros adecuados. Además, hay que tener en cuenta el **ajustado precio** que Slimbook ha logrado para este portátil y que lo hace ideal si vas buscando un equipo con garantía, precio y un soporte a la altura.


No podemos más desde Jugando en Linux que recomendar el Slimbook Eclipse si es que estás buscando un portátil para jugar a un precio adecuado. Hay opciones portátiles con características superiores, pero en relación calidad/potencia/precio el Slimbook Eclipse pensamos que merece la pena.


Tienes disponible el **Slimbook Eclipse** en su [página web oficial de Slimbook](https://slimbook.es/eclipse-linux-profesional-workstation-and-gaming-laptop), además durante unos días podrás conseguirlo **con 50€ de descuento** cortesía de la casa. Si estás pensándotelo, ¡esta es tu oportunidad!


