---
author: Pato
category: Terror
date: 2017-01-10 22:48:31
excerpt: "<p>As\xED lo ha anunciado Aspyr Media que publicar\xE1 el juego de Bloober\
  \ Team</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/16db9b88d9515580a78d5965b066ac2d.webp
joomla_id: 171
joomla_url: observer-un-nuevo-juego-de-terror-de-los-creadores-de-layers-of-fear-llegara-a-linux-proximamente
layout: post
tags:
- proximamente
- primera-persona
- terror
title: "'Observer' un nuevo juego de terror de los creadores de Layers of Fear llegar\xE1\
  \ a Linux pr\xF3ximamente"
---
Así lo ha anunciado Aspyr Media que publicará el juego de Bloober Team

"¿Qué harías si tus temores fuesen hackeados?"


Esta es la inquietante pregunta que nos hace Aspyr Media en el tweet que anuncia el próximo lanzamiento de 'Observer' [[Web oficial](http://observer.gamepedia.com/Observer_Wiki)], un juego de tipo "sobrevive al horror" desarrollado por Bloober Team los creadores de Layers of Fear [[página de Steam](http://store.steampowered.com/app/391720/)], también disponible en Linux.



> 
> What would you do if your fears were hacked? Announcing Observer, a new psychological horror game from [@BlooberTeam](https://twitter.com/BlooberTeam) <https://t.co/FhV5xolUBE> [pic.twitter.com/7qEXORbV2V](https://t.co/7qEXORbV2V)
> 
> 
> — Aspyr Media (@AspyrMedia) [10 de enero de 2017](https://twitter.com/AspyrMedia/status/818936795295584256)



 Dado que el anuncio no dejaba claro en qué sistemas aparecería el juego, Liam de GOL les ha preguntado al respecto y Aspyr lo ha confirmado:



> 
> [@gamingonlinux](https://twitter.com/gamingonlinux) [@BlooberTeam](https://twitter.com/BlooberTeam) Yes it WILL be on Linux! We'll be sharing more on the game in the coming weeks - stay tuned!
> 
> 
> — Aspyr Media (@AspyrMedia) [10 de enero de 2017](https://twitter.com/AspyrMedia/status/818940205449248768)



 El juego utiliza el motor Unreal 4 y se lanzará en una fecha aún por determinar de este mismo año 2017. Estaremos atentos a lo que Aspyr Media anuncie al respecto, pero desde luego la pinta que tiene es bastante prometedora. Aquí tienes un par de vídeos para que os hagais una idea:


Teaser Trailer:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/Si2BVRfJhNQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Gameplay:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/o2WhEoOABD8" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


¿Que te parece este Observer? ¿Te gustan este tipo de juegos?


Cuéntamelo en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

