---
author: leillo1975
category: Ofertas
date: 2018-08-30 17:48:40
excerpt: <p>Corred Insensatos!!!!</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c5c3ef3b8b263faba0c86cf911308716.webp
joomla_id: 845
joomla_url: rebajas-de-verano-de-humble-bundle
layout: post
tags:
- humble-bundle
- witcher
- sega
- ofertas
- humble-store
- 2k-games
- paradox-interactive
title: "\xA1Rebajas de verano de Humble Bundle!"
---
Corred Insensatos!!!!

Si estais detrás de algunos juegos desde hace tiempo esta es vuestra oportunidad por que la conocida [tienda de Humble Bundle](https://www.humblebundle.com/store?partner=jugandoenlinux) acaba de lanzar sus rebajas de verano con unas ofertas más que destacadas. Existen tanto ofertas individuales como rebajas de juegos de determinadas editoras. Entre las que más destacan podeis encontrar:



> 
> -[Hollow Knight](https://www.humblebundle.com/store/hollow-knight?partner=jugandoenlinux) a 9.89€ (-34%)
> 
> 
> -[The Witcher 2: Assasins of Kings Enhanced Edition](https://www.humblebundle.com/store/the-witcher-2-assassins-of-kings-enhanced-edition?partner=jugandoenlinux) a 2.99€ (-85%)
> 
> 
> -[Rocket League](https://www.humblebundle.com/store/rocket-league?partner=jugandoenlinux)  a 11.99€ (-40%)
> 
> 
> -[Darkest Dungeon](https://www.humblebundle.com/store/darkest-dungeon?partner=jugandoenlinux) a 6.89€ (-70%)
> 
> 
> -[Planetary Annihilation: Titans](https://www.humblebundle.com/store/planetary-annihilation-titans?partner=jugandoenlinux)  a 7.39€ (-80%)
> 
> 
> -[Everspace](https://www.humblebundle.com/store/everspace?partner=jugandoenlinux) a 9.23€ (-67%)
> 
> 
> -[Road Redemption](https://www.humblebundle.com/store/road-redemption?partner=jugandoenlinux) a 11.99€ (-40%)
> 
> 
> -[Super Meat Boy](https://www.humblebundle.com/store/super-meat-boy?partner=jugandoenlinux) a 3.67€ (-73%)
> 
> 
> -[Yooka-Laylee](https://www.humblebundle.com/store/yooka-laylee?partner=jugandoenlinux) a 13.59€ (-66%)
> 
> 
> -[American Truck Simulator](https://www.humblebundle.com/store/american-truck-simulator?partner=jugandoenlinux) a 4.99€ (-75%)
> 
> 
> -[Juegos de 2K Games](https://www.humblebundle.com/store/promo/2k-summer-sale?partner=jugandoenlinux) (Bioshock Infinite, Borderlands 2, Civilization VI, Xcom 2, Spec Ops: The Line....y muchos más)
> 
> 
> -[Juegos de Paradox Interactive](https://www.humblebundle.com/store/promo/paradox-interactive-summer-sale?partner=jugandoenlinux) (Cities Skylines, Surviving Mars, Pillars of Eternity, Stellaris, Europa Universalis...y muchos más)
> 
> 
> -[Juegos de SEGA](https://www.humblebundle.com/store/promo/sega-summer-sale?partner=jugandoenlinux) (Total War Shogun 2, Alien Isolation, Company of Heroes 2, Motorsport Manager, Tatal War Warhammer.... y muchos más)
> 
> 
> 


Por supuesto podreis encontrar muchos más de los que hay aquí, incluso juegos que puede que querais jugar con **Steam Play/Proton** (como [The Witcher III](https://www.humblebundle.com/store/the-witcher-3-wild-hunt-game-of-the-year-edition?partner=jugandoenlinux) o la saga [Bioshock](https://www.humblebundle.com/store/bioshock-the-collection?partner=jugandoenlinux)) pero aseguraos antes de que funcionan en [esta lista](https://docs.google.com/spreadsheets/d/1DcZZQ4HL_Ol969UbXJmFG8TzOHNnHoj8Q1f8DIFe8-8/htmlview#). Podeis dejar también las ofertas que más os llamen la atención en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).


 


 


 


 

