---
author: Pato
category: Noticia
date: 2020-08-18 16:54:50
excerpt: "<p>Ahora es posible a trav\xE9s de cualquier navegador con base Chromium,\
  \ sin m\xE1s</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Geforcenow/Geforcenow.webp
joomla_id: 1248
joomla_url: ya-es-posible-jugar-en-streaming-mediante-el-servicio-geforce-now-en-linux-actualizacion
layout: post
tags:
- league-of-legends
- thecrew-2
- geforce-now
- assetto-corsa
- fortnite
title: Ya es posible jugar en Streaming mediante el servicio GeForce NOW en Linux
  - Actualizado -
---
Ahora es posible a través de cualquier navegador con base Chromium, sin más


**Actualización 16/09/2020**


Volvemos sobre **GeForce NOW** pues gracias a [gamingonlinux.com](https://www.gamingonlinux.com/2020/09/nvidia-geforce-now-on-linux-can-run-without-user-agent-spoofing-in-a-browser) sabemos que ahora ya no hace falta cambiar el user agent de nuestro navegador basado en Chromium. Ahora el servicio al parecer funciona sin más en nuestro navegador, eso sí **Nvidia no ha hecho ninguna referencia a si ahora ofrece su servicio de manera oficial a través del navegador**, aunque tal y como parece puede ser cuestión de tiempo que la compañía lo haga público para así no depender de una aplicación dedicada al servicio, y así poder ofrecer GeForce NOW en todas las plataformas con navegador independientemente de si hay aplicación o no.


¡Mucho más fácil para todos!... Seguiremos atentos por si Nvidia ofreciera información oficial respecto al soporte a través de navegador.




---


**Artículo original**


Hay que asumirlo. Nos guste más o menos los servicios de juego vía *streaming* han venido para quedarse, y la prueba es que toda compañía que pretenda ser alguien dentro del mundillo del videojuego está ahora mismo o invirtiendo o desarrollando para este tipo de plataformas. Llámese Google, Microsoft, Sony o Perico de los Palotes, si te quedas atrás en esta carrera puede que ya llegues tarde.


Para los que jugamos desde un sistema **Linux**, la única opción que teníamos a la hora de jugar vía streaming era el servicio de **Stadia** mediante navegadores con base Chromium desde el que podemos jugar a los videojuegos que van saliendo para dicha plataforma (pagando por ellos, obviamente), pero ahora, **[gracias a gamingonlinux.com y al incombustible Liam](https://www.gamingonlinux.com/2020/08/nvidia-geforce-now-adds-chromebook-support-so-you-can-run-it-on-linux-too)** hemos sabido que **ya es posible jugar a nuestros videojuegos** mediante el servicio de [GeForce NOW](https://play.geforcenow.com/), gracias a la llegada del servicio a los Chromebooks mediante su navegador.


Un momento Pato. ¿Has dicho Chromebooks?. Si, gracias a que el servicio de [Nvidia acaba de lanzar la opción](https://blogs.nvidia.com/blog/2020/08/18/geforce-now-open-a-chromebook/) de poder hacer *streaming* del servicio GeForce NOW **mediante el navegador Chrome para ChromeOS**, **haciendo un sencillo cambio del *user agent*** de nuestro navegador con base Chromium tendremos la posibilidad de ejecutar los juegos mediante GeForce NOW sin mayores problemas. Y funciona, ¡vaya si funciona!


En jugandoenlinux nos hemos puesto manos a la obra y hemos hecho la prueba. Tras instalar Chromium (o derivadas) desde nuestro repo o appstore preferida, hemos instalado un [user agent changer](https://chrome.google.com/webstore/detail/user-agent-switcher-for-c/djflhoibgkdhkhhcedjiklpkjnoahfmg?hl=en-US&gl=US). Después hay que añadir un user agent del sistema ChromeOS, añadiendo la línea:


`Mozilla/5.0 (X11; CrOS x86_64 13099.85.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.110 Safari/537.36`


hemos probado a entrar en el servicio, cosa que hemos hecho sin mayores problemas y después hemos podido ejecutar algunos juegos con una calidad bastante buena. Si bien es cierto que el servicio lo hemos probado con la opción de **suscripción gratuita**, lo único que te limita esta modalidad es el tiempo de juego a 1 hora y que para ciertos juegos tendrás que esperar unos minutos para poder entrar en una suerte de "lista de espera".


![GFNLOL](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Geforcenow/GFNLOL.webp)


*Esperando a jugar al LOL desde Ubuntu 20.04 a través de Geforce NOW... tras lo cual se ejecutó sin problemas*


Por lo demás, al igual que con Stadia los juegos se ejecutan en los servidores de Nvidia y se retransmiten vía tu conexión de internet hasta tu sistema, con la particularidad de que con Geforce NOW puedes vincular tu cuenta de Steam y así poder ejecutar toda tu biblioteca de juegos a través de este servicio. Eso sí, te recomendamos que cuentes con una buena conexión de fibra a internet por que el servicio es muy exigente en cuanto a ancho de banda. Por otra parte, si no quieres colas de espera o quieres jugar más de 1 hora por sesión, las opciones de [suscripción al servicio](https://www.nvidia.com/es-es/geforce-now/memberships/) son:


**Fundadores**, que te da la opción de tiempo de juego extendido, acceso prioritario (sin esperas) y modo RTX activado (opciones gráficas de tarjetas Gforce RTX) **por 5,49€ al mes**, o una **oferta especial de 27,45€ para 6 meses** de servicio que ofrece lo mismo que la oferta Fundadores, pero con algunos addons exclusivos para el juego *Hyper Scape.*


En cuanto a nuestras pruebas, Tanto **Leillo** como un servidor **hemos estado jugando a juegos como el** **League of Legends** (que pronto [implementará su polémico sistema anti-trampas]({{ "/posts/league-of-legends-anadira-un-anti-trampas-a-nivel-de-kernel-windows-y-tendra-los-dias-contados-en-linux" | absolute_url }}) que impedirá ejecutarlo mediante Wine), el **Assetto Corsa Competizione**, o el **The Crew,** con la salvedad de que en estos últimos no detecta el volante, cosa que también pasa ejecutando el servicio desde sistemas Windows. Y ¿que hay del Fortnite?... También:![GFNFortnite](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Geforcenow/GFNFortnite.webp)


*Fortnite ejecutándose desde nuestro sistema Ubuntu 20.04 mediante el servicio Geforce NOW*


 Hemos de advertir no obstante, que estamos jugando haciendo la "pequeña trampa" del user agent, por lo que es posible que Nvidia haga algún cambio que impida en algún momento que el servicio deje de ejecutarse en nuestro sistema por lo que **si jugáis mediante este método sabed que no está soportado de manera oficial y jugáis bajo vuestra cuenta y riesgo**, aunque dado lo "simple" del mecanismo de verificación (el user agent no es que sea la panacea en cuanto a complejidad) probablemente sea cuestión de tiempo que el soporte a nuestro sistema mediante navegador se haga oficial por parte de Nvidia.


Largo tiempo hemos estado esperando a que pudiésemos jugar a los últimos videojuegos desde nuestro sistema favorito. Con la llegada del juego mediante servicios de streaming esta posibilidad es más real que nunca. Si aun no os lo creeis os dejamos con un video donde podreis ver como podemos jugar a unos cuantos juegos como tales Fortnite, LoL, WRC7 o The Crew:  
  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="420" src="https://www.youtube.com/embed/N8HfezQg4cU" style="display: block; margin-left: auto; margin-right: auto;" width="750"></iframe></div>


¿Qué te parece la posibilidad de poder ejecutar tus juegos mediante el servicio de Nvidia Geforce NOW? ¿Piensas jugar algún juego mediante este servicio?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux en [Telegram](https://twitter.com/JugandoenLinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

