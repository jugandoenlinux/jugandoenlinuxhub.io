---
author: Pato
category: Aventuras
date: 2018-01-11 18:50:52
excerpt: <p>El juego es obra de Eteru Studio afincado en las Islas Canarias</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bf26253d7b8f171dddb155f84ce1d562.webp
joomla_id: 602
joomla_url: hostil-un-interesante-juego-corto-de-tipo-point-and-click-ya-disponible-en-linux-steamos
layout: post
tags:
- indie
- aventura
- point-and-clic
title: '''Hostil'' un interesante juego corto de tipo "point and click" ya disponible
  en Linux/SteamOS'
---
El juego es obra de Eteru Studio afincado en las Islas Canarias

Siempre es interesante hacerse eco de los lanzamientos de estudios españoles que lanzan títulos para nuestro sistema favorito. En este caso hablamos de 'Hostil', un juego de aventuras tipo "point and clic" desarrollado por [Eteru Studio](http://www.eterustudio.com/) afincado en las Islas Canarias y en el que tendremos de "buscar nuestra propia aventura".


*"Hostil es un cuento sobre un niño perdido en un planeta desconocido. Un juego narrativo donde podrás explorar el escenario para desvelar la historia, mientras haces frente a peligros mortales."*


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/CGsQUXqCBRY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


El juego en sí es bastante corto. Según los desarrolladores lo podremos terminar en aproximadamente una hora, aunque como verás por su precio tampoco está mal:


Sus características principales son:


* 1 hora de juego.
* 12 bellos escenarios dibujados a mano.
* 350 párrafos de misteriosa historia.
* Puzzles narrativos, visuales y musicales.
* Banda sonora original.
* Adecuado para todas las edades.


 Si quieres darle un vistazo y te va la temática, pueder verlo en su página de Steam, como es de suponer en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/726200/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

