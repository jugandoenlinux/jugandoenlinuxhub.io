---
author: leillo1975
category: Rol
date: 2017-11-21 20:55:16
excerpt: "<p>La compa\xF1\xEDa Canadiense&nbsp;@BeamdogInc finalmente ha relanzado\
  \ este cl\xE1sico en Steam</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/dbf38d4f97abb8ef843a32ffd8c16913.webp
joomla_id: 547
joomla_url: neverwinter-nights-enhanced-llegara-a-gnu-linux-gracias-a-beamdog
layout: post
tags:
- rpg
- neverwinter-nights
- beamdog
- dungueons-dragons
- bioware
title: "Neverwinter Nights Enhanced Edition llegar\xE1 a GNU-Linux gracias a Beamdog\
  \ (ACTUALIZADO)"
---
La compañía Canadiense @BeamdogInc finalmente ha relanzado este clásico en Steam

**ACTUALIZACIÓN 29-3-18**: Os pedimos disculpas por no informaros debidamente de la [salida oficial en Steam](http://blog.beamdog.com/2018/03/neverwinter-nights-enhanced-edition_27.html) de Neverwinter Nights: Enhanced Edition. Finalmente y según lo esperado el juego ya está disponible para todo el mundo. Aquí teneis el tweet del anuncio:



> 
> Neverwinter Nights: Enhanced Edition is now live on Steam! <https://t.co/6vlr4PA49W>[#NeverwinterNights](https://twitter.com/hashtag/NeverwinterNights?src=hash&ref_src=twsrc%5Etfw) [#NWNEE](https://twitter.com/hashtag/NWNEE?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/XGWVLRYTpJ](https://t.co/XGWVLRYTpJ)
> 
> 
> — Beamdog (@BeamdogInc) [27 de marzo de 2018](https://twitter.com/BeamdogInc/status/978694701413949440?ref_src=twsrc%5Etfw)



 Desde aquí queremos destacar el formidable trabajo que está realizando esta compañía [restaurando y mejorando estos magníficos clásicos del Rol](http://store.steampowered.com/search/?developer=Beamdog). Si quereis comprar Neverwinter Nights: Enhanced Edition, podeis hacerlo en [su tienda](https://www.beamdog.com/products/neverwinter-nights-digital-deluxe) (**recomendado**) o en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/704450/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 




---


**ACTUALIZACIÓN 9-3-18**: Ya tenemos la fecha de salida de Neverwinter Nights: Enhanced Edition en Steam, y queda realmente poco: el 27 de este mismo mes podremos disfrutarlo. El anuncio lo realizaba la compañía a traves de este tweet:



> 
> JUST ANNOUNCED!   
>   
> Neverwinter Nights: Enhanced Edition is coming to [#Steam](https://twitter.com/hashtag/Steam?src=hash&ref_src=twsrc%5Etfw) March 27![#DnD](https://twitter.com/hashtag/DnD?src=hash&ref_src=twsrc%5Etfw) [#NeverwinterNights](https://twitter.com/hashtag/NeverwinterNights?src=hash&ref_src=twsrc%5Etfw) [#Launch](https://twitter.com/hashtag/Launch?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/IMGdzge6qm](https://t.co/IMGdzge6qm)
> 
> 
> — Beamdog (@BeamdogInc) [9 de marzo de 2018](https://twitter.com/BeamdogInc/status/972175238321520640?ref_src=twsrc%5Etfw)



Si os apetece echar un ojo al juego, recordad que hace unos dias nuestro colega [Pato](index.php/homepage/generos/rol/itemlist/user/192-pato) nos retransmitió el Unboxing Anticipado del juego, del que disponemos de una copia en exclusiva gracias a Beamdog. Aquí os lo dejo el Link:


["Unboxing" de Neverwinter Nights Enhanced Edition en exclusiva](https://youtu.be/GHszQv9rrOY)


 




---


**NOTICIA ORIGINAL**: A principios de siglo (buff, como suena eso), allá por el 2002, la conocida compañía [Bioware](http://www.bioware.com) lanzó al mercado uno de sus juegos más aclamados. Después del éxitazo de Baldur's Gate, su secuela, los Icewind Dale o MDK2, la compañía entró de lleno en los 3D's y dotó de la potencia de su motor Aurora a este fantástico juego. 15 años más tarde, [Beamdog](https://www.beamdog.com/) nos trae de nuevo Neverwinter Nights tal y como lo hicieron antes con los juegos antes mencionados, así como como con Planescape Torment, pero como siempre con mutitud de características nuevas y pulidas que mejoran la experiencia en nuestros PC's actuales.


 


A última hora de esta misma tarde la propia desarrolladora nos confirmaba mediante un tweet lo que todos esperábamos, y que hasta la fecha han cumplido. El tweet en cuestión es este:


 



> 
> Sure will be! More news at noon PST! <https://t.co/MTxjfuCw5K>
> 
> 
> — Beamdog (@BeamdogInc) [November 21, 2017](https://twitter.com/BeamdogInc/status/933032437738639360?ref_src=twsrc%5Etfw)



 


El juego, que en su día también fué lanzado para Linux (teneis instrucciones de [como jugarlo en el foro](index.php/foro/tutoriales/41-instalar-neverwinter-nights-diamond-edition-gog-en-ubuntu)),  incluirá aparte de la campaña original lo siguiente:


 



> 
> -Sus **dos expansiones oficiales**, Shadows of Undrentide y Hordes of the Underdark
> 
> 
> -T**res módulos premium** que dotan al juego de 40 horas más de nuevas historias.
> 
> 
> -**Cliente de Maestro de las Mazmorras**, para dirigir  tus propias partidas.
> 
> 
> -Por supuesto multitud de **mejoras en el aspecto gráfico** como soporte para HD y 4K, **opciones gráficas avanzadas** (pixel shaders y efectos de postprocesamiento entre otras)
> 
> 
> -**Compatibilidad** con elementos tales como partidas salvadas del juego original, mods, módulos...
> 
> 
> 


Teneis más información en [su blog](http://blog.beamdog.com/2017/11/neverwinter-nights-enhanced-edition.html) y la lista completa de cambios y mejoras [aquí](https://www.beamdog.com/files/nwnee_release_notes.pdf). Si os da tiempo en este momento están retransmitiendo un gameplay del juego en su canal de Twitch: Este es el video que han retransmitido en twitch:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="378" src="https://player.twitch.tv/?autoplay=false&amp;video=v203213828" width="620"></iframe></div>[Watch Beamdog Plays Planescape: Torment: Enhanced Edition from Beamdog on www.twitch.tv](https://www.twitch.tv/videos/203213828?tt_content=text_link&tt_medium=vod_embed)


El **juego podrá correr en cualquier equipo** pues solo pide 1GB de memoria, OpenGL 3.0 y 1Ghz de velocidad de procesador. En este momento podeis pre-comprar Neverwinter Nights Enhanced Edition en la **[página web de Beamdog](https://www.beamdog.com/products/neverwinter-nights-enhanced-edition)**, y dentro de poco en Steam


 


¿Habeis jugado ya a Neverwinter Nights? ¿Qué os parecen las reediciones de BeamDog? Deja tus opioniones en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

