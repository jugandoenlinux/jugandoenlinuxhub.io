---
author: Pato
category: Software
date: 2021-04-23 19:09:43
excerpt: "<p>La herramienta de Steam Play empieza a soportar extensiones de trazado\
  \ de rayos entre otras cosas</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DXVK-valve-Proton.webp
joomla_id: 1301
joomla_url: la-rama-de-proton-experimental-se-actualiza-con-novedades-muy-importantes
layout: post
title: La rama de Proton experimental se actualiza con novedades muy importantes
---
La herramienta de Steam Play empieza a soportar extensiones de trazado de rayos entre otras cosas


Por norma general solemos hablar de las características de Proton que llegan a la rama estable, sin embargo no nos resistimos a publicar novedades de la rama experimental cuando, como es el caso traen novedades interesantes.


Según podemos ver en el reporte de [Proton experimental](https://github.com/ValveSoftware/Proton/wiki/Changelog), se han añadido todas las mejoras hasta la [versión 6.3-2](https://github.com/ValveSoftware/Proton/wiki/Changelog#63-2) , y se ha introducido la versión 2.3 de vkd3d-proton. ¿Y por que es importante?


Pues por que en esta versión **se ha comenzado a implementar el soporte para las extensiones de trazado de rayos sobre DXR 1.0** a través de VK_KHR_raytracing. 


Esto significa que se podrán ejecutar los juegos que soporten trazado de rayos en DirectX a través de Vulkan.


En estos momentos, **solo funciona sobre las tarjetas de Nvidia** puesto que son las que ya implementan trazado de rayos a traves de su controlador sobre Vulkan, aunque esperan que las gráficas de **AMD RDNA2 ofrezcan soporte en un futuro próximo** cuando se implemente el soporte de trazado de rayos en los drivers para tarjetas Radeon.


Si quieres probar su funcionamiento, tendrás que elegir la rama experimental de Proton en las opciones de Steam Play del juego, y luego en las opciones de lanzamiento escribir la siguiente línea:


``VKD3D_CONFIG=dxr %command%``


Por otra parte, se han introducido otros cambios y mejoras como:


- Mejoras de rendimiento en torno a la entrada, el sistema de ventanas y la asignación de memoria.  
- Agregado soporte para la representación de ventanas secundarias de Vulkan.  
- Método de distribución de archivos actualizado para ahorrar espacio en disco.  
- **Forza Horizon 4** se puede reproducir (requiere una GPU AMD con los controladores mesa-git más recientes (confirmación buena conocida de mesa-git: 899dd8e60a5228c4506400d621ef6b5abfe5e32c)).  
**- Mount & Blade II: Bannerlord** es jugable.  
**- Origin Overlay es funcional,** lo que permite jugar a It Takes Two con amigos.  
- Mejor soporteen  **Anno 1404 - History Edition**.  
Se corrigió el error "No comprado" de **Red Dead Redemption 2**.  
Se corrigió el bloqueo de **Age of Empires II: Definitive Edition** al iniciar.


Parece que Valve sigue apretando el acelerador con Proton para cualquiera que sean los planes que tienen respecto a nuestro sistema favorito, y haciendo que cada vez más, su implementación se haga imprescindible para todo aquel que quiera jugar videojuegos "no nativos", que como ya hemos dicho en otras ocasiones tiene su lado bueno, y su lado malo.


En cualquier caso, se trata de una herramienta más que tenemos disponible para usar siempre que queramos, y eso siempre está bien.


Toda la información la tienes [en este enlace](https://github.com/ValveSoftware/Proton/wiki/Changelog). 

