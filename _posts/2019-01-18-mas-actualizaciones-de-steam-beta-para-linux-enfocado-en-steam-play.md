---
author: Pato
category: Software
date: 2019-01-18 09:11:32
excerpt: "<p>M\xE1s novedades para el cliente de Valve</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/760af276ee6f9b46d3f3a54c804b7c7d.webp
joomla_id: 954
joomla_url: mas-actualizaciones-de-steam-beta-para-linux-enfocado-en-steam-play
layout: post
tags:
- steam
- beta
- steam-play
title: "Mas actualizaci\xF3nes de Steam beta para Linux enfocado en Steam Play (ACTUALIZADO)"
---
Más novedades para el cliente de Valve

**ACTUALIZADO 19-1-19:** Una vez más el cliente se vuelve a actualizar con [novedades](https://steamcommunity.com/groups/SteamClientBeta#announcements/detail/1703951108833155107) más que interesantes, entre las que destaca la **posibilidad de ejecutar con Proton juegos que no son de Steam**, Esta es la lista completa de novedades que encontraremos en esta nueva versión:


**General**


Se ha corregido la configuración de Steam Play que se mostraba incorrectamente en las propiedades del juego en Windows y MacOS.


**Steam Input**


Corregidas varias regresiones en el Configurador de Steam Input introducido en la actualización del miércoles.


**Linux**


Se ha solucionado un problema con las selecciones de la herramienta Steam Play no válidas si el usuario había optado previamente por habilitar Steam Play para todos los títulos. (Nota: esto causará un restablecimiento único de todas las selecciones globales y per-app de Steam Play al estado predeterminado).  
 Añadida la posibilidad de forzar herramientas de compatibilidad de Steam Play para atajos que no sean de Steam Game.


Con respecto a la, a nuestro juicio, más importante, he de decir personalmente que lo he probado con [Live for Speed](https://youtu.be/aABAClAPTjM), que había instalado con Lutris y funciona perfectamente. Para ello he tenido que buscar el ejecutable del juego en vez de su acceso directo.




---


**NOTICIA ORIGINAL:** Si hace tan solo unas horas anunciábamos una nueva versión beta del cliente de Steam con jugosas novedades para Linux, nuevamente tenemos otro comunicado de la rama beta de Steam con novedades, esta vez relacionadas con Steam Play.


En concreto, la nueva actualización trae mejoras para el chat en modo Big Picture y el overlay, soporte para un par de pads HORI (Nintendo) pero lo sustancial viene en los cambios para Linux:


     · Se agrega la capacidad de habilitar el uso de Steam Play en las propiedades por título, incluso para juegos nativos  
     · Se corrige el desplazamiento offset incorrecto en la superposición del juego  
     · La configuración global de Steam Play se ha vuelto a rehacer, permite anular solo la versión de Proton utilizada por los juegos no compatibles  
     · Se corrige un error en el que la configuración global de habilitación de Steam Play no pedía un reinicio del cliente Steam


Puedes ver el anuncio original en [este enlace](https://steamcommunity.com/groups/SteamClientBeta#announcements/detail/1703951108827819236).


¿Qué te parecen las novedades que trae Steam para Linux? ¿Piensas que puede afectar la forma en que Steam está implementando Proton en los ports de videojuegos para Linux?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux)

