---
author: Pato
category: Aventuras
date: 2018-04-21 18:13:03
excerpt: "<p>El juego llegar\xE1 este verano sin fecha concreta confirmada. Ya est\xE1\
  \ disponible en pre-compra</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fd6c1bb5b0a1bed64c5dda3726185da3.webp
joomla_id: 715
joomla_url: state-of-mind-lo-nuevo-de-daedalic-entertainment-tendra-version-linux
layout: post
tags:
- accion
- indie
- aventura
- proximamente
title: "'State of Mind' lo nuevo de Daedalic Entertainment tendr\xE1 versi\xF3n Linux"
---
El juego llegará este verano sin fecha concreta confirmada. Ya está disponible en pre-compra

Gracias al último comunicado de Daedalic Entertainment nos enteramos que su último desarrollo llamado 'State of Mind' llegará este próximo verano, aunque sin una fecha concreta. Daedalic es uno de los estudios que últimamente nos traen sus títulos a Linux/SteamOS, como **la saga Deponia**, **Silence**, **Fire** o **Los Pilares de la Tierra**.


En cuanto a State of Mind, es un juego en forma de thriller futurista que se adentra en el transhumanismo. El juego explora temas de separación, descoyuntura y reunificación en un mundo dividido entre una realidad material distópica y un futuro virtual utópico.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/_O3bT5I1kaM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


**Características:**


* Experimenta una visión alarmantemente realista del futuro cercano
* Sumérgete en un enmarañado thriller de ciencia ficción que entremezcla la realidad distópica con la utopía digital
* Descubre una conspiración global en una sociedad donde el digitalismo, la vigilancia y el transhumanismo están por todas partes
* Explora un mundo que cuenta con un estilo visual único y variado que combina entornos realistas con personajes minimalistas
* Asume el papel del periodista Richard Nolan y de otros cinco personajes jugables adicionales
* Emplea tu destreza, capacidad de deducción e investigación para reconstruir el pasado de Richard


En cuanto al lanzamiento en Linux/SteamOS, lo dejan bien claro en el comunicado, indicando que el lanzamiento será para el verano de este 2018 para Pc (incluyendo Mac y Linux). Podéis verlo [en este enlace](https://press.daedalic.com/state-of-mind-daedalic-kicks-off-preorder#).


Por otra parte, State of Mind ya está disponible para pre-compra en las tiendas de [GOG](https://www.gog.com/game/state_of_mind) y [Steam](http://store.steampowered.com/app/437630/State_of_Mind/) con un 10% de descuento sobre el precio final, y como curiosidad ya cuenta con el icono de soporte para Linux/SteamOS pero no hay aún requisitos para nuestro sistema favorito.


¿Qué te parece lo nuevo de Daedalic? ¿Te gustaría jugar a este 'State of Mind?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

