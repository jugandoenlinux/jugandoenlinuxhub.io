---
author: leillo1975
category: Estrategia
date: 2020-09-10 14:10:05
excerpt: "<p>El juego de estrategia ha llegado al estado de Alpha p\xFAblica.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/BAR/BAR_01.webp
joomla_id: 1254
joomla_url: beyond-all-reason-bar-un-rts-libre
layout: post
tags:
- open-source
- codigo-abierto
- appimage
- beyond-all-reason
- bar
- balanced-annihilation
- supreme-commander
- total-annihilation
- springrts
title: Beyond All Reason (BAR), un RTS libre y gratuito
---
El juego de estrategia ha llegado al estado de Alpha pública.


 Reddit, como sabeis es una fuente inagotable de noticias. En muchas ocasiones la información es redundate, intrascendental o confusa, pero si rebuscas un poco siempre encuentras algo interesante. Es así como hemos conocido este juego, "escarbando" en el subreddit [r/linux_gaming](https://www.reddit.com/r/linux_gaming/), donde tras un rápido vistazo apareció un [escueto post](https://www.reddit.com/r/linux_gaming/comments/ip23yb/new_total_annihilationlike_rts_now_in_playable/) donde encontramos el enlace a un atractivo **juego de estrategia en tiempo real (RTS)** del que desconocíamos totalmente su existencia.


**Beyond All Reason,** que así se llama el juego, o **BAR** en forma abreviada, utiliza el conocidisimo motor [SpringRTS](https://springrts.com/), utilizado en multitud de juegos de estrategia, y trata de recrear la jugabilidad de títulos clásicos como Total Annihilation o Supreme Comander. Si juntamos el motor que usa con los juegos a los que se parece, podríamos decir que se trata de un fork de [Zero-K](https://zero-k.info/), pero realmente no es así. Ciertamente **ambos juegos comparten una misma raiz**, pues ambos vienen de "[Balanced Annihilation](https://springrts.com/wiki/Balanced_Annihilation)" y tienen puntos en común, pero ninguno de los dos deriva del otro. BAR sería realmente el acrónimo de **Balanced Annihilation Remake** o Remastered. Aclarado esto vamos a describir lo que podreis encontrar en este juego.


**Escala y Realismo:** Podrás comandar grandes cantidades de unidades como si fueran una, usando su propia inteligencia artificial para moverse por el campo de batalla, simulándose estos en tiempo real y con físicas propias proporcionadas por el motor SpringRTS.


**Importancia del Terreno:** el diseño del mapa donde tendrán lugar las batallas influirá decisivamente en el desarrollo de estas, siendo más apropiadas determinadas unidades para ciertos terrenos, pudiendo tomar ventaja frente al enemigo sabiendo usarlas. Podrás usar bots, vehículos, aviones, barcos, aerodeslizadores, unidades anfibias y todoterreno para vencer a tus enemigos. Además el paisaje es deformable,lo cual puede influir en el devenir de la partida.


**Recursos y Estrategias:** El juego no va  limitar tus estrategias con la recolección de recursos, permitiéndote atacar a tus enemigos desde principio de la partida, aunque si prefieres crecer y crecer y aplastarlos con todo tipo de unidades, también podrás hacerlo. La única limitación del juego será la cantidad de unidades que tu equipo pueda manejar.


**Hasta 392 tipos de unidades diferentes:** Es posible distinguir todas y cada una de las unidades del campo de batalla gracias a su modelado, textura y estilo general, iconos estratégicos y comportamiento. Además, cada unidad tiene su propio y único papel, lo que significa que hay montones de formas diferentes de combatir a tu enemigo.


En BAR **podremos disputar batallas tanto nosotros solos contra la IA**, como la posibilidad de disputar **partidas contra otros jugadores**. Para ello tendremos que registrarnos en el juego. Por ahora el título **aun no cuenta con un modo campaña**, ni tampoco misiones o escenarios, y en modo de un jugador **solo es posible disputar la típica escaramuza** contra la computadora. Tanto para este modo como en multijugador, podremos descargar multitud de mapas diferentes de los servidores. A nivel técnico el juego se mueve con mucha soltura y cuenta con unos gráficos 3D coloridos y con un buen detalle. Si queremos buscar referencias de otros juegos a los que se parece, a parte de los antes mencionados, podemos añadir Planetary Annihilation, tanto en el tipo de unidades como en las estructuras que podemos construir.


Hay algo muy importante que estamos dejando atras, y es que tal y como os adelantábamos en el título de este artículo, al igual que el motor en que se basa, se trata de un **juego de Código Abierto**, del que podemos disponer de su código fuente en la [página de su proyecto](https://github.com/beyond-all-reason/Beyond-All-Reason), por lo que somos libres de leerlo y modificarlo a nuestro antojo. BAR se encuentra actualmente en estado **Alpha+**, lo que significa que puede haber bugs y errores, pero es jugable para el 90% de las configuraciones. Podemos descargarlo en formato **AppImage** si seguimos los pasos que nos indican en [este enlace](https://www.beyondallreason.info/download).


 Interesante el juego, ¿verdad? Pues si quereis acompañar toda esta información con imágenes del juego, lo mejor es que veais este gameplay y judgueis por vosotros mismos:


 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="420" src="https://www.youtube.com/embed/kLTEcgn9C80" style="display: block; margin-left: auto; margin-right: auto;" width="750"></iframe></div>


¿Qué os parece BAR? ¿Qué opinais de que sea un juego libre? ¿Os gustan los juegos de estrategia? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

