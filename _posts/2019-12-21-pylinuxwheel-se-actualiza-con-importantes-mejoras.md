---
author: leillo1975
category: Software
date: 2019-12-21 21:51:42
excerpt: "<p>Nuestro querido @OdinTdh nos trae la versi\xF3n 0.6.1 de su software</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/PyLinuxWheel/PyLinuxWheelLogoTitulo.webp
joomla_id: 1144
joomla_url: pylinuxwheel-se-actualiza-con-importantes-mejoras
layout: post
tags:
- logitech
- volantes
- pylinuxwheel
- odintdh
title: PyLinuxWheel se actualiza con importantes mejoras (ACTUALIZADO)
---
Nuestro querido @OdinTdh nos trae la versión 0.6.1 de su software


**ACTUALIZACIÓN 1-6-20**: [@OdinTdh](https://gitlab.com/OdinTdh) nos ha comunicado que recientemente ha actualizado su utilidad a la versión [0.6.1](https://gitlab.com/OdinTdh/pyLinuxWheel/-/releases/0.6.1) para corregir un error en la descarga de las reglas [udev](https://es.wikipedia.org/wiki/Udev), por lo que en la versión anterior esta funcionalidad fallaría. A si mismo se ha añadido la traducción al idioma turco por parte de [@fahri.uzun](https://gitlab.com/fahri.uzun). Por supuesto también ha actualizado la AppImage para que todos podais disfrutar de esta última versión de una forma cómoda y sencilla. Podeis encontrar este paquete con estas correcciones en su página de [itch.io](https://odintdh.itch.io/pylinuxwheel).


 




---


**NOTICIA ORIGINAL:**


 Todos aquellos usuarios que disfrutamos como enanos con nuestros amados **volantes Logitech** en Linux, ultimamente no hacemos más que alegrarnos con la cantidad de soporte que estamos obteniendo ([PyLinuxWheel](index.php/buscar?searchword=Pylinuxwheel&ordering=newest&searchphrase=all), [Oversteer](index.php/buscar?searchword=oversteer&ordering=newest&searchphrase=all&limit=20), [New-Lg4Ff](index.php/buscar?searchword=new-lg4ff&ordering=newest&searchphrase=all&limit=20)...), y lo que es mejor, gracias al trabajo de miembros de nuestra comunidad. Atrás quedaron los tiempos en los que si queríamos cambiar simplemente el rango de nuestros volantes teníamos que acudir a la consola y teclear interminables comandos, o hacer uso de socorridos scripts. **Que yo sepa** PyLinuxWheel fué la primera herramienta de este tipo que nos facilitaba las cosas con una aplicación gráfica.


Con el tiempo ha ido tomando forma y adquiriendo más y más características útiles e interesantes que nos permite tener un soporte cercano al que tienen los usuarios de Windows, y hoy mismo se ha actualizado a la versión 0.6.0 que incluye las siguientes nuevas características:


*-Se añadió una **nueva pestaña de perfil**. Ahora pyLinuxWheel puede crear, aplicar, guardar, importar y exportar perfiles de su configuración de volante*  
 *-Se ha **solucionado el problema de Fontconfig** en el paquete de aplicaciones.*  
 *-**Corregida la detección de pedales** para las ruedas MOMO en la pestaña de prueba.*  
 *-Añadido un **nuevo paquete deb** creado usando como referencia el paquete pyLinuxWheel hecho por desarrolladores de MX Linux. Gracias a SwampRabbit por empaquetar pyLinuxWheel para MX Linux.*  
 *-Creado un **nuevo Appimage***  
 *-Añadido un conjunto de **perfiles predefinidos** para diferentes juegos creados por leillo1975 (vaya, ese tio me suena...). Puedes descargarlo de [itch.io](https://odintdh.itch.io/pylinuxwheel)*


![PyLinuxWheelProfile](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/PyLinuxWheel/PyLinuxWheelProfile.webp)


Podeis descargar PyLinuxWheel de la [página de su proyecto](https://gitlab.com/OdinTdh/pyLinuxWheel/-/releases), o en formato .AppImage o .deb desde su sección en [Itch.io](https://odintdh.itch.io/pylinuxwheel) Como veis **@OdínTdh** no ha estado de brazos cruzados en estas últimas semanas, por lo que ya estais tardando en descargarlo y poner a punto vuestros volantes.

