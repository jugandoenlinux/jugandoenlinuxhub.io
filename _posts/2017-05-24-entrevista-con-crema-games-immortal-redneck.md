---
author: leillo1975
category: Entrevistas
date: 2017-05-24 09:23:00
excerpt: "<div dir=\"ltr\">El estudio madrile\xF1o contesta a nuestras preguntas</div>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/edc1d0314df26f4954651131e82a7939.webp
joomla_id: 335
joomla_url: entrevista-con-crema-games-immortal-redneck
layout: post
tags:
- entrevista
- crema-games
- immortal-redneck
title: Entrevista con Crema Games (Immortal Redneck)
---
El estudio madrileño contesta a nuestras preguntasHace unos días publicábamos un extenso [análisis de Immortal Redneck](index.php/homepage/analisis/item/435-analisis-immortal-redneck), la opera prima en PC del estudio madrileño [Crema Games](index.php/component/search/?searchword=immortal&searchphrase=all&Itemid=101). En él veiamos unas maneras y un buen hacer que apuntan muy alto. Desde aquí nos gustaría agradecerles toda la colaboración prestada a JugandoEnLinux.com por facilitarnos las cosas para la elaboración de dicho análisis. También le propusimos una entrevista con nuestra batería habitual de preguntas a la que accedieron gustosamente, y aquí están sus respuestas:


 


**JugandoEnLinux (JeL)** - ¿Podríais hacer un pequeño resumen sobre vuestra compañía? (Juegos, número de personas, años de existencia, historia...) Nos gustaría que nos hablaseis también de otros juegos que hayais desarrollado.


**Crema Games (CG)** - *Crema se formó en 2009 cuando comenzamos a desarrollar para plataformas móviles. Instant Buttons fue nuestra primera aplicación, que hasta la fecha ha sobrepasado las 20 millones de descargas. La empresa se fundó en 2012 y supuso el paso de desarrollar aplicaciones a desarrollar videojuegos para móvil. Nuestras dos grandes producciones, Oh My Goat y Ridiculous Triathlon, fueron ambas destacadas por Apple en el AppStore y fueron bien recibidas tanto por la prensa como por los usuarios. Oh My Goat además consiguió un gran éxito en China donde fue destacado en varias tiendas consiguiendo más de medio millón de descargas.*


*Actualmente Crema está enfocada en el desarrollo para PC y Consolas, siendo Immortal Redneck el debut para estas plataformas.*


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisImRedneck/Cremalogo.webp)


 


**JeL** -¿Que distribución de Linux usais para desarrollar y/o probar el juego?


**CG** - *El juego se desarrolla activamente en Windows, solo usamos Linux para testear o para probar cosas específicas de esa plataforma. Usamos Ubuntu 16.04.2 LTS para ello.*


**JeL** - ¿Que motor y herramientas de desarrollo usais y si hay alguna en especial para la versión de Linux?


**CG** - Usamos Unity y una de las cosas buenas que ofrece es precisamente la abstracción en cuanto a plataformas. Normalmente todo lo que desarrollas para una plataforma funciona en el resto de plataformas soportadas sin necesidad de muchos cambios mayores.


**JeL** - ¿Cómo es el proceso de desarrollo? ¿Es más fácil o difícil de lo esperado el desarrollo para Linux? ¿Sois vosotros también usuarios de GNU/Linux?


**CG** - *El proceso ha sido bastante fácil gracias a Unity como comentaba. El desarrollo en Linux ha sido prácticamente nulo y básicamente lo que hemos hecho ha sido probar que todo funcionaba correctamente y arreglar las pequeñas cosas que han salido. Nosotros no somos usuarios de Linux pero igualmente queríamos dar la posibilidad a todos los usuarios de jugar a Immortal Redneck.*


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisImRedneck/02.webp)


 


**JeL** - ¿Que es lo que os ha llevado a decidir desarrollar también para Linux?


**CG** - *Por un lado la sencillez en el proceso de desarrollar para Linux y por otro lado tampoco queríamos privar a la base de usuarios de otros sistemas. Siempre es mejor que el juego llegue al máximo público posible.*


**JeL** - ¿Ha merecido la pena el esfuerzo en dar soporte a Linux?


**CG** - *Bueno, por ahora las ventas de Linux representan un porcentaje muy muy bajo de las ventas del total (menos del 1% de las ventas) así que no es que se haya rentado mucho. Como comentaba al ser sencillo el proceso tampoco le hemos tenido que dedicar muchísimo tiempo así que no supone un problema grande.*


**JeL** - ¿Cómo estais viendo el panorama del juego en Linux?


**CG** - *En nuestro caso concreto muy escaso, si bien es cierto que hay otros juegos que parecen tener una userbase mayor en Linux así que igual todavía no hemos llegado al público general de Linux.*


**JeL** - ¿Seguireis apostando por esta plataforma?


**CG** - *Mientras sea sencillo seguir desarrollando la versión de Linux si, pero si para futuros juegos se nos complica el port seguramente no. Al menos mientras se mantenga este nivel de ventas que hemos visto con Immortal Redneck.*


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisImRedneck/Hero1080.webp)


 


**JeL** - ¿Qué tipo de juegos tomais como referencia o inspiración para vuestros desarrollos? ¿Cuales son vuestros juegos favoritos?


**CG** - *Para Immortal Redneck nos hemos fijado sobretodo en Rogue Legacy y en Serious Sam, básicamente es una combinación de ambos juegos con bastantes añadidos para hacerlo un poco más único.*


**JeL** - Si quereis, podeis decir lo que querais sobre vuestro juego y vuestra compañía, o dar un mensaje a nuestros lectores


**CG** - *Poco más, muchas gracias por la entrevista y espero que vuestros lectores puedan disfrutar del juego.*


 


Y esto es todo. Agradecemos a Crema Games todo el tiempo que nos han dedicado y todos sus esfuerzos por traernos us juego a nuestros sistemas. Esperamos que en un futuro nos traigan juegos tan buenos o mejores como este Immortal Redneck y que la comunidad Linuxera apoye a esta compañía con un incremento de las ventas. Nosotros por nuestra parte queremos recomendaros una vez más este magnífico juego que podeis adquirir en la [tienda de Steam](http://store.steampowered.com/app/595140/Immortal_Redneck/).


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/RzahU23jExA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 
Si quieres apoyar a Crema Games y disfrutar de un buen juego en nuestro sistema puedes hacerte con Immortal Redneck en Steam:
 
<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/595140/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


  
¿Qué os ha parecido la entrevista? ¿Añadiriais alguna pregunta más? Puedes contestar a estas preguntas en los comentarios, así como usar nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).
 