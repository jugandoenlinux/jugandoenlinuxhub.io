---
author: Pato
category: Estrategia
date: 2017-11-14 19:19:35
excerpt: "<p>Estrategia en tiempo real combinada con \"rogue-lite\" a trav\xE9s de\
  \ la galaxia</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ca0d6da997c7f811cdca8d4563c6a22c.webp
joomla_id: 535
joomla_url: distant-star-revenant-fleet-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- espacio
- rts
- estrategia
title: "'Distant Star: Revenant Fleet' ya est\xE1 disponible en Linux/SteamOS"
---
Estrategia en tiempo real combinada con "rogue-lite" a través de la galaxia

Nos llegan noticias desde los [foros de Steam](http://steamcommunity.com/app/335830/discussions/0/619573787387526004/?tscn=1510675874) de que 'Distant Star: Revenant Fleet' ya está disponible en nuestro sistema favorito.


En concreto, el soporte para Linux/SteamOS llegó hace unos días tras una actualización. En 'Distant Star: Revenant Fleet' [[web oficial](http://www.blazinggriffin.com/games/distant-star-revenant-fleet/)] tendremos que hacernos con una pequeña flota de hasta 5 naves seleccionando entre 8 clases distintas y recorrer una galaxia generada aleatóriamente para derrotar a la P*lataforma Erebus.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/G8PdLY6dAS4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Tendremos a nuestra disposición diversos items y mejoras para las naves, y cada trayecto tendrá sus propias características por lo que cada vez que juegues será una experiencia distinta. todo ello aderezado con un apartado gráfico cuidado y una buena banda sonora.


Si quieres saber más de este 'Distant Star: Revenant Fleet', lo tienes disponible en su página de Steam, eso sí, no está traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/335830/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


¿Qué te parece 'Distant Star: Revenant Fleet'? ¿Cruzarás la galaxia para luchar?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

