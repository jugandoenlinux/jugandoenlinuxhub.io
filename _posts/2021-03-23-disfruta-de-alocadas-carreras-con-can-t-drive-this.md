---
author: leillo1975
category: Carreras
date: 2021-03-23 15:34:01
excerpt: "<p>La compa\xF1\xEDa alemana <span class=\"css-901oao css-16my406 r-poiln3\
  \ r-bcqeeo r-qvutc0\">@pixel_maniacs</span> nos trae esta original propuesta.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/CantDriveThis/CantDriveThisTux.webp
joomla_id: 1291
joomla_url: disfruta-de-alocadas-carreras-con-can-t-drive-this
layout: post
tags:
- competitivo
- cooperativo
- can-t-drive-this
- pixel-maniacs
title: Disfruta de alocadas carreras con Can't Drive This.
---
La compañía alemana @pixel_maniacs nos trae esta original propuesta.


 En primer lugar tengo que decir que hace días que tendría que haber escrito sobre este juego (despiste total, "sorry"), pues desde el viernes, que fué la fecha oficial de salida del juego, tengo conocimiento de su existencia gracias a un [mensaje](https://matrix.to/#/!VjCGmWghqbSHAxMshG:matrix.org/$1616174410144646WhYHG:matrix.org?via=matrix.org&via=t2bot.io) de **@Berarma** en nuestras [salas de Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org). Además segun tengo entendido está levantando bastante revuelo por sus criticas positivas y la cantidad de Streamers que están jugando con el en directo.


Quien no conozca este juego debe saber que está desarrollado por la compañía alemana [Pixel Maniacs](https://pixel-maniacs.com/) y llevaba **4 años y medio en Acceso Anticipado**, concretamente desde **Septiembre de 2016** (ya ha llovido, ehh?) Sus creadores son también responsables  de [ChromaGun](https://www.humblebundle.com/store/chromagun?partner=jugandoenlinux) (**también con soporte nativo**). En el juego tendremos que **conducir un Monster Track por una pista que irá apareciendo delante de nosotros**, pero con la dificultad de no poder bajar de determinada velocidad a riesgo de explotar (como en Speed con Keanu Reeves y Sandra Bullock). 


![CDT_gif](https://cdn.akamai.steamstatic.com/steam/apps/466980/extras/slalom.gif)


Con esta sencilla premisa es posible jugar en el **modo de un solo jugador**, pero donde el juego toma más relevancia es en el **modo cooperativo-competitivo** (si, como suena, las dos cosas a la vez), que podremos disputar tanto en **modo local** (**o Remote Play Together**) conectando varios mandos a nuestro equipo, y hasta **4 jugadores a pantalla partida**; como a través de **internet**. **Mientras un jugador conduce el vehículo, el otro podrá ir creando la carretera** delante nuestra, por lo que las risas están aseguradas.


Esté título pone además especial énfasis en la **personalización tanto de coches como de pistas**, incluyendo sendos editores que permitirán hacer únicas nuestras creaciones. Además funciona perfectamente en máquinas modestas, pues solo necesitamos un equipo con una configuración mínima de:


* **Procesador:** 2.5 GHz Dual core
* **Memoria:** 4 GB de RAM
* **Gráficos:** NVIDIA GeForce 760, AMD Radeon R7 270X, or better
* **Red:** Conexión de banda ancha a Internet
* **Almacenamiento:** 1 GB de espacio disponible


Podeis comprar [Can't Drive This](https://pixel-maniacs.com/cantdrivethis) tanto en la **[Humble Store](https://www.humblebundle.com/store/cant-drive-this?partner=jugandoenlinux)** (**Patrocinado**), como en [Steam](https://store.steampowered.com/app/466980/Cant_Drive_This/). Os dejamos con el trailer oficial:
  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/4INPDBcIN84" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


¿Verdad que el juego tiene una pinta estupenda? ¿Qué os parece el original modo multijugador de "Can't Drive This"?  Podeis dejarnos vuestras impresiones y opiniones sobre el lanzamiento de este juego en los comentarios en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

