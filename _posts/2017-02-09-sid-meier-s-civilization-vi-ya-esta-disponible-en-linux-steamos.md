---
author: Pato
category: Estrategia
date: 2017-02-09 18:40:25
excerpt: "<p>Aspyr Media lanza el esperado juego tal y como estaba previsto. \xA1\
  Ven a conquistar el Mundo!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d6e0c71ca3722c71ddd73a242718257f.webp
joomla_id: 212
joomla_url: sid-meier-s-civilization-vi-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- steamos
- steam
- estrategia
- aspyr-media
title: "'Sid Meier's Civilization VI' ya est\xE1 disponible en Linux/SteamOS"
---
Aspyr Media lanza el esperado juego tal y como estaba previsto. ¡Ven a conquistar el Mundo!

![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/CivVI.webp)


Tal y como estaba anunciado 'Sid Meier's Civilization VI' ya ha llegado a Linux/SteamOS gracias a [Aspyr Media](https://www.aspyr.com/games/sid-meier-s-civilization-vi). El esperado juego de estrategia por fin está disponible para nuestro sistema favorito y parece que lo ha hecho por la puerta grande. Ganador de varios premios al mejor juego de estrategia en el pasado E3, este Civilization VI es un juego que hará las delicias de los amantes de la estrategia por turnos.


Construye tu Imperio y haz que prevalezca en el tiempo. 


*"Conquista el mundo entero estableciendo y liderando tu propia civilización desde la Edad de Piedra hasta la Era de la Información. Libra guerras, utiliza la diplomacia, fomenta el progreso de tu cultura y enfréntate cara a cara a los líderes más importantes de la historia para crear la civilización más grande jamás conocida.*


*Civilization VI ofrece nuevas maneras de interactuar con tu mundo: ahora las ciudades se expanden físicamente en el mapa, la investigación activa de tecnología y cultura desbloquea nuevos potenciales y los líderes actúan en función de sus rasgos históricos mientras intentas conseguir la victoria de una de las cinco maneras posibles."*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/5KdE0p2joJw" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


En Sid Meier's Civilization encontrarás nuevas opciones y características, como "Imperios expansivos" que abarcan varias casillas, "investigación activa" con la que podrás explorar y avanzar mas rápidamente, "Diplomacia dinámica" donde las relaciones con las civilizaciones irán cambiando y si al principio predominarán los enfrentamientos al final primarán las relaciones diplomáticas y mucho más.


Por el momento, el apartado multijugador para juego cruzado solo está disponible para Linux y Mac, pero tal y como comentan en el [anuncio](http://steamcommunity.com/games/289070/announcements/detail/572357653496580374) hecho público, el multijugador cruzado con Windows estará disponible en próximas fechas.


Os recordamos los requisitos mínimos y para jugar:


Sistema: Ubuntu 16.04 / SteamOS  
 Procesador: Intel Core i3 530 o AMD A8-3870  
 CPU: 2.93 GHz  
 Memoria: 6 GB  
 Disco duro: 15 GB  
 Gráfica (NVIDIA): GeForce 650 con 1GB de memoria


Las gráficas de Intel y AMD no están oficialmente soportadas pero tal y como ya han probado en webs como [www.gamingonlinux.com](https://www.gamingonlinux.com/articles/civilization-vi-released-for-linux-video-and-port-report.9075) el juego funciona bastante decente en gráficas de esas marcas que cumplan los requisitos de memoria, aunque parece que cuando el juego va avanzando y va cargando mas cosas el rendimiento baja un poco por lo que es recomendable cuanta más máquina mejor. Además en GOL también podeis ver un vídeo del juego corriendo en Linux y algunos test de rendimiento que Liam ha hecho, probando que el juego se mueve bastante decentemente con gráficas Nvidia. 


'Sid Meier's Civilization VI está disponible completamente en español, y debido al lanzamiento en nuestro sistema ahora lo puedes adquirir con la mejor oferta que han hecho hasta la fecha, con un descuento de entre un 20% y un 30% según el pack que cojas. Lo tienes en:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/289070/123215/" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/289070/123216/" width="646"></iframe></div>


¿Que te parece este 'Sid Meier's Civilization'? ¿Estabas esperándolo? ¿Piensas jugarlo?


Cuéntamelo en los comentarios, en el foro o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

