---
author: Pato
category: Software
date: 2021-06-21 17:43:47
excerpt: "<p>Nuevas extensiones y soporte para DLSS sobre gr\xE1ficas RTX</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Vulkan/Vulkan_DLSS.webp
joomla_id: 1318
joomla_url: llegan-novedades-importantes-a-vulkan-y-los-drivers-de-nvidia-para-linux
layout: post
tags:
- realidad-virtual
- vulkan
- nvidia
- ray-tracing
- dlss
title: Llegan novedades importantes a Vulkan y los drivers de Nvidia para Linux
---
Nuevas extensiones y soporte para DLSS sobre gráficas RTX


 


Vía [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Vulkan-1.2.182-Released) nos llegan noticias de que la nueva versión de Vulkan llega cargada de novedades respecto a Linux. Y es que entre las extensiones hay un poco de todo, como **VK_EXT_acquire_drm_display** o **VK_EXT_physical_device_drm**para gestionar la interfaz DRM de cara a Wayland y VR.


También tenemos **VK_EXT_multi_draw**desarrollada por Valve, Nvidia, AMD o Intel entre otros, o **VK_NV_ray_tracing_motion_blur**desarrollada por Nvidia para gestionar el apartado ray-tracing tan de moda últimamente. Toda la información sobre estas y otras expensiones [en Vulkan.org](https://vulkan.org/).


En otro orden de cosas, **Nvidia en conjunto con Valve** ha estado trabajando en el soporte para la tecnología de reescalado **DLSS sobre la API Vulkan en los juegos ejecutados bajo Proton,** soporte que llegaráen los próximos drivers que se lanzarán mañana. Esto habilitará los núcleos Tensor de las gráficas con soporte para acelerar los FPS en juegos como *DOOM Eternal, No Man's Sky o Wolfenstein: Young Blood*. Según el anuncio, el soporte DLSS para los juegos bajo DirectX llegará también a nuestros sistemas bajo Proton a partir de otoño.


También anuncian la llegada del soporte DLSS a un buen número de motores gráficos. Toda la información la tienes en el [post del anuncio de Nvidia](https://www.nvidia.com/en-us/geforce/news/june-2021-rtx-dlss-game-update/).


¿Qué te parece la evolución de Vulkan? ¿Piensas que la llegada de tecnologías como DLSS pueda ayudar a expandir el juego en nuestros sistemas?


Cuéntamelo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

