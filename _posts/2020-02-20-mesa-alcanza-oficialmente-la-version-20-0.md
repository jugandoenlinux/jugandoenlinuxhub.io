---
author: leillo1975
category: Software
date: 2020-02-20 08:50:15
excerpt: "<p>AMD e Intel reciben una gran mejora con este lanzamiento</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/MesaGlxgears.webp
joomla_id: 1177
joomla_url: mesa-alcanza-oficialmente-la-version-20-0
layout: post
tags:
- vulkan
- opengl
- amd
- intel
- mesa
- open-source
title: "MESA alcanza oficialmente la versi\xF3n 20.0"
---
AMD e Intel reciben una gran mejora con este lanzamiento


 Tras un año de trabajo intenso, **los desarrolladores de [Mesa](https://es.wikipedia.org/wiki/Mesa_3D) han liberado oficialmente la versión número 20 de su biblioteca gráfica**, y como es de esperar los [avances introducidos](https://lists.freedesktop.org/archives/mesa-dev/2020-February/224132.html) son muchos y de gran importancia, especialmente para los **usuarios de AMD e Intel**, que ven como aumenta la compatibilidad y rendimiento de sus controladores gráficos un año más.


Entre **los cambios más importantes** que podemos encontrar en esta nueva versión están los siguientes:  
- **Soporte de OpenGL 4.6 para RadeonSI** , ahora que la NIR y SPIR-V están activadas. Se ha eliminado el soporte TGSI al ser sustituido por ellas.


- El controlador **Intel Gallium3D** es el predeterminado para usuarios con **gràficos Broadwel**l (8ª generación) o superiores, sustiyendo al ya obsoleto i965. Este último driver se mantiene para graficos Haswell (4ª generación) y anteriores.


- El **compilador de sombreados [ACO](index.php/component/k2/12-software/1193-valve-anuncia-aco-un-nuevo-compilador-de-sombreado-para-mesa) para RADV**, desarrollado por Valve, soporta las **tarjetas gráficas GCN 1.0/1.1**, aumentando si cabe aun más la vida útil de estas longevas tarjetas, las de la **serie HD 7000**. También se ha trabajado en manejo de sombreadores geométricos y el soporte de Navi/GFX10 Wave32. Cabe recordar que a pesar de que este sombreador funciona muy bien y es muy recomendable para los que jugamos, no es el que viene aun por defecto.


- **RADV** ha vuelto a habilitar el soporte de **sombreado de la geometría NGG**.


- **RadeonSI** ha añadido una **caché de sombreado en vivo**.


- Se han añadido **nuevas extensiones tanto de OpenGL como de Vulkan** (GL_EXT_direct_state_access, VK_AMD_device_coherent_memory, VK_AMD_mixed_attachment_samples, ...)


- Los **procesadores "Jasper Lake"** (Atom, Celeron y Pentium),  adquieren **soporte Vulkan y OpenGL** con el driver de Intel


- Tanto el driver **Intel ANV**, como **RADV** son **compatibles con Vulkan 1.2**, adquiriendo el primero algunas extensiones más.


- Montones de **optimizaciones en Vulkan para Intel**


- Se ha aumentado el **rendimiento en el enlazador de NIR**


- El **controlador V3D de Raspberry Pi** ahora tiene soporte de **sombreador de geometría OpenGL ES 3.2**


Hay muchos más cambios en esta versión 20.0, pero además de alargar innecesariamente este artículo,tienen explicaciones excesivamente técnicas que la mayoría no entenderíamos. Creo que no es necesario decir que **si sois usuarios de AMD como Intel, la actualización a esta versión es altamente recomendable**, y bien seguro que notareis diferencias con respecto a la anterior versión. Suponemos que poco a poco las diferentes distribuciones irán actualizandose a medida que vayan lanzado nuevas versiones. También es probable que en poco tiempo veamos introducidos estos cambios en los diferentes repositorios existentes, como por ejemplo el de [Padoka](index.php/foro/drivers-para-graficas-amd-intel/7-ppa-drivers-mesa-no-oficial-de-paulo-dias) para Ubuntu.


Podeis darnos veustra opinión sobre MESA en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

