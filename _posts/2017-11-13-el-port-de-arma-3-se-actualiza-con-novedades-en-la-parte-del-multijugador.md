---
author: Serjor
category: "Simulaci\xF3n"
date: 2017-11-13 20:06:26
excerpt: <p>El desarrollo del port sigue avanzando con la firmeza del paso militar</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a3193da9f744508278b55342b9cabd86.webp
joomla_id: 532
joomla_url: el-port-de-arma-3-se-actualiza-con-novedades-en-la-parte-del-multijugador
layout: post
tags:
- virtual-progamming
- bohemia-interactive
- arma
- arma-iii
- simulacion
- militar
title: El port de Arma 3 se actualiza con novedades en la parte del multijugador (ACTUALIZADO)
---
El desarrollo del port sigue avanzando con la firmeza del paso militar

**ACTUALIZACIÓN 6-3-18**: Acabo de arrancar Steam y estoy recibiendo una actualización de **7.5 GB**. Buscando información ([aquí](http://steamcommunity.com/app/107410/discussions/0/1696040635928282828/) y [aquí](https://forums.bohemia.net/forums/topic/188937-experimental-ports-release-announcements/?tab=comments#comment-3274516)) veo que se trata la sincronización con la [versión 1.8](https://dev.arma3.com/post/spotrep-00076) de Windows, por lo que de nuevo podremos volver a jugar en red con ellos. Además habilita la posibilidad de poder adquirir pack de misiones [Tac-Ops](https://arma3.com/dlc/tacops).


 




---


**NOTICIA ORIGINAL:** Leemos en [gamingonlinux.com](https://www.gamingonlinux.com/articles/vps-arma-3-176-beta-now-out-compatible-with-windows-for-now.10709) una noticia que hará las delicias de los fans de los simuladores de combate más realistas, y es que por primera vez la beta para GNU/Linux está a la par de la versión para Windows, por lo que en las partidas multijugador podremos jugar contra ellos.


Según vemos desde Bohemia Interactive no aseguran que las próximas versiones de los clientes de ambos sistemas sigan siendo compatibles, pero es una buena noticia que sigan manteniendo el desarrollo vivo, y sobretodo que la gente de Virtual Programming sean capaces de tener el port lo más al día posible, porque además de tener compatibilidad con la versión de Windows, esta beta también soporta el DLC Laws of War.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/M1YBZUxMX8g" style="display: block; margin-left: auto; margin-right: auto; border-style: none;" width="560"></iframe></div>


Si queréis podéis haceros con el juego y acceder a su beta para GNU/Linux.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/107410/31539/" style="display: block; margin-left: auto; margin-right: auto; border-style: none;" width="646"></iframe></div>


Y tú, ¿demostrarás a los windowseros quién tiene un sistema superior? Cuéntanos qué te parece esta noticia en los comentarios o en nuestro canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

