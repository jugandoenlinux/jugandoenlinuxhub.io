---
author: Pato
category: Ofertas
date: 2017-04-13 21:16:36
excerpt: "<p>Adem\xE1s \xA1puedes ganar el juego de Feral que quieras concursando\
  \ en twitter! Sigue leyendo para saber como</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/34649abc993982b197aa9a6210af69ed.webp
joomla_id: 290
joomla_url: feral-interactive-lanza-sus-ofertas-de-pascua-y-rebaja-su-catalogo-un-50
layout: post
tags:
- feral-interactive
- ofertas
title: "Feral Interactive lanza sus Ofertas de Pascua y rebaja su cat\xE1logo un 50%"
---
Además ¡puedes ganar el juego de Feral que quieras concursando en twitter! Sigue leyendo para saber como

Como lo leeis, Feral Interactive ha lanzado sus "rebajas de Pascua" en su tienda rebajando todo su catálogo para Linux un 50%...


Puedes verlo en el siguiente enlace:


<https://store.feralinteractive.com/es/offers/#offer_323>


Esta puede ser una buena oportunidad de hacerte con esos juegos que has estado esperando rebajados. Además, según han anunciado en su cuenta de twitter conmemorando estas rebajas, si eres bueno con las fotos puedes ganar el juego de Feral de tu elección:



> 
> To mark our Linux 50% off sale, you could win any Linux game you want. Photograph a penguin (e.g. a toy or keyring) in a strange place… [pic.twitter.com/9oKN14wfnv](https://t.co/9oKN14wfnv)
> 
> 
> — Feral Interactive (@feralgames) [13 de abril de 2017](https://twitter.com/feralgames/status/852555607978672128)



 



> 
> ...and tweet it to us with the hashtag [#tuxsnap](https://twitter.com/hashtag/tuxsnap?src=hash). Stranger the better. Only one entry per person. Sale and competition end Friday 21st April. [pic.twitter.com/YobTq0Mv4W](https://t.co/YobTq0Mv4W)
> 
> 
> — Feral Interactive (@feralgames) [13 de abril de 2017](https://twitter.com/feralgames/status/852555806348365825)



 Tal y como lo pone, fotografía un pingüino (puede ser de peluche u otros) en un lugar insólito, cuanto más mejor y twitéalo en su cuenta con el hastag #tuxsnap y podrás ganar un título de tu elección. Solo una entrada por persona, y el concurso termina el 21 de abril.


¡Mucha suerte!

