---
author: leillo1975
category: Rol
date: 2018-07-02 14:10:07
excerpt: <p>El juego es lanzado con soporte para Linux.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5d7e022bfc88ea606725533249e32b08.webp
joomla_id: 789
joomla_url: el-clasico-drakkhen-disponible-en-gog-com
layout: post
tags:
- dosbox
- drakkhen
- infogrames
- classics-digital
- clasico
title: "El cl\xE1sico \"Drakkhen\" disponible en GOG.com ."
---
El juego es lanzado con soporte para Linux.

Aun recuerdo hace unos cuantos años, quizás demasiados, que uno de los juegos que más tiempo me hizo "perder" con mi viejo PC fué "[Drakkhen](https://en.wikipedia.org/wiki/Drakkhen)", un juego de Rol con unos gráficos bastante resultones que en su día editó a principios de los 90 la vieja Infogrames (ahora ATARI). El viejo diskette de 3.5" DD echaba humo cuando ejecutaba en MS-DOS este clásico.... seguramente estará en una caja transparente en el trastero de mis  padres junto a mi viejo [Amstrad PC2086](http://www.cpcwiki.eu/index.php/Amstrad_PC) de 8Mhz. Todos estos recuerdos afloraron de nuevo en mi cabeza gracias a un artículo que hace unos días era publicado en una de nuestras webs de referencia, [GamingOnLinux](https://www.gamingonlinux.com/articles/gog-adds-the-classic-rpg-drakkhen-to-their-store.12031).


Una vez más, gracias a las virtudes de DOSBOX podemos disfrutar de nuevo en nuestros PC's de estas pequeñas-grandes joyas en nuestros ultrarrápidos PC's de hoy en día. Reeditado hace unos cuantos meses por [Classics Digital](http://classicsdigital.com/hello-world/), ha llegado hace unos pocos días con soporte a una de nuestras tiendas favoritas, GOG.com. Esperemos que pronto podemos disfrutarlo también en Steam en nuestras máquinas, junto con el resto del catálogo clásico de esta compañía, que en algunos casos ya ha dado soporte a juegos como "Daemonsgate", "Alien Rampage" o "King's Table - The Legend of Ragnarok".


Drakkhen, que en su día fué lanzado, además del PC, en multitud de plataformas como SNES, Amiga, Atari ST... empleaba un pseudo3D para representar un extenso mundo por el que podías moverte libremente (lo que te llevaba en innumerables ocasiones a la muerte). En el juego teníamos que comandar en tiempo real a un grupo de cuatro héroes y luchar contra multitud de variados enemigos. La descripción oficial del juego es la siguiente (tradución online):



> 
> *"A través de las neblinas arremolinadas de una antigua isla, el llamado del gran dragón se escapa en la noche. "ANHAK DRAKKHEN AGHNAHIR HURTHD!" El sonido se extiende para apoderarse de tu alma, atrayéndote más profundamente hacia una inmensa y primordial tierra de potente magia y un intrincado peligro.*  
> *Este es un mundo controlado por el poder de los dragones. Con la muerte de este gran dragón, el mundo y toda su magia desaparecerían.*  
> *Has sido elegido para guiar a tu banda de cuatro valientes aventureros en un viaje traicionero. Su búsqueda es recuperar las joyas místicas de ocho príncipes dragón, resucitar el gran dragón y restaurar el reino primitivo, la fuente de toda la magia en el universo.*  
> *Ten cuidado! Tus enemigos son muchos y taimados. Más de 150 monstruos distintos esperan a que termine tu búsqueda y tu vida. Las criaturas malvadas no esperarán tu movimiento, sino que te atacarán a su discreción.*  
> *Armado sólo con tu ingenio, armamento y 200 hechizos mágicos, atraviesas una inmensa y variada tierra en fantásticos viajes tridimensionales. A través de una serie aparentemente interminable de castillos malditos y peligrosas mazmorras.*  
> *Tu destino te espera. ¡Responde a la llamada de Drakkhen!*
> 
> 
> *-Personajes personalizados que se adaptan a tu propio estilo de aventura: ¡ya seas explorador, sacerdote, mago o guerrero!*  
>  *-Más de 150 monstruos, ogros y dragones únicos*  
>  *-Más de 200 hechizos mágicos en pantalla"*
> 
> 
> 


Os dejamos con un gameplay del juego para que si no lo conoceis os hagais una idea:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/YUZNabsBv3M" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Puedes comprar Drakkhen con soporte para Linux en [GOG.com](https://www.gog.com/game/drakkhen).

