---
author: leillo1975
category: Estrategia
date: 2018-11-09 19:01:00
excerpt: <p class="ProfileHeaderCard-screenname u-inlineBlock u-dir" dir="ltr"><span
  class="username u-dir" dir="ltr">@Snapshot_Games "argumenta" dificultades en el
  desarrollo.</span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/534ce826239b10b60fac93e160603051.webp
joomla_id: 894
joomla_url: malas-noticias-phoenix-point-ha-sido-cancelado-para-nuestro-sistema
layout: post
tags:
- cancelacion
- unity3d
- phoenix-point
- snapshot-games
title: 'Malas noticias: Phoenix Point ha sido cancelado para nuestro sistema.'
---
@Snapshot_Games "argumenta" dificultades en el desarrollo.

Aiiii que palo más grande..... Empezar el fin de semana con anuncios como este es desagradable, pero por desgracia es la actualidad. Justo cuando acababamos de recibir un correo electrónico anunciandonos la [Backer Build 3](https://phoenixpoint.info/blog/2018/11/7/backer-build-three?utm_source=Phoenix+Point&utm_campaign=709afee33a-BACKER_BUILD_3_NB&utm_medium=email&utm_term=0_0f91b4b9df-709afee33a-178016385&mc_cid=709afee33a&mc_eid=1f7eb10941), y se nos empezaba a hacer la boca agua.... ZAS!, en toda la boca!, también nos enteramos que **han cancelado la versión para Linux/SteamOS**. Como sabeis [llevamos tiempo cubriendo el desarrollo de este juego](index.php/homepage/generos/estrategia/tag/Phoenix%20Point) de [Julian Gollop](https://es.wikipedia.org/wiki/Julian_Gollop), creador original de la saga XCOM, desde su campaña hasta las sucesivas actualizaciones que se han ido sucediendo. Para que no haya equívocos ni malos entendidos nos vamos a limitar a traducir el [comunicado de Snapshot Games](https://phoenixpoint.info/linux/):



> 
> ***Anuncio de Linux***
> 
> 
> *Hemos tomado la difícil decisión de retirar el soporte para Linux en este momento por las siguientes razones:*
> 
> 
> *Linux requiere una gran cantidad de programación gráfica especializada, ya que utiliza OpenGL en lugar de DirectX en Windows.*
> 
> 
> *Como el soporte de controladores para Linux no es tan completo como para Windows y Mac, tendríamos que hacer una serie de adaptaciones a los sombreadores gráficos para que funcionen correctamente.*
> 
> 
> *Para poder lanzar en Linux tenemos que construir y probar muchas distribuciones diferentes, cada una con su propio conjunto de complicaciones y adaptaciones requeridas. En este momento no tenemos los recursos del estudio para crear y probar adecuadamente todos ellos.*
> 
> 
> *También tenemos otros problemas, como las complicaciones con los dispositivos de entrada, especialmente los controladores de juegos, junto con algunos errores de unidad específicos de Linux, como no poder renderizar correctamente el reproductor de vídeo.*
> 
> 
> *Nos gustaría seguir apoyando a Linux y reevaluaremos la posibilidad de una versión de Linux en el futuro.*
> 
> 
> *Si ha hecho un pedido por adelantado de Phoenix Point para Linux, por favor envíe un correo electrónico a [contact@snapshotgames.com](mailto:contact@snapshotgames.com) para que podamos ayudarle.*
> 
> 
> 


Bueno, ¿Qué os parece? Sinceramente las razones que "argumentan" (y lo entrecomillo a proposito) son a mi juicio, y esto es una opinión personal, al menos discutibles. Creo que la verdadera razón es la de siempre, el dinero. Supongo que lo que les pasa es lo que a muchas compañías que se llenan la boca para recaudar fondos y luego cuando consiguen lo que quieren, toman decisiones como esta. Entendemos que **dar soporte a Linux es un gasto extra que puede no ser rentable teniendo en cuenta la base de jugadores**, pero es curioso, ¿como pretenden tener mejor base de jugadores si no dan soporte? Al final es lo de siempre la pescadilla que se muerde la cola...


Con respecto a las razones que ellos argumentan lo primero que tenemos que decir es que **hasta ahora han lanzado dos backer builds con soporte** que aquí probamos ([video 1](https://youtu.be/eXw8FVT6ZR0), [video 2](https://youtu.be/ncZSYNpY3C4)). También que **usan un motor gráfico que hasta ahora ha permitido lanzar al mercado montones de juegos en nuesrtros sistema, el Unity 3D**. Sobre el tema de los controladores graficos, no tengo los conocimientos necesarios para poder discutirlo, pero creo que en eso hemos avanzado enormemente los últimos años. Iguamente pienso con respecto a los dispositivos de control. Si hasta ahora han podido, ¿por qué ahora no?


También hay que decir que **según comentan en twitter no es una decisión categórica**. Sinceramente a mi si me lo parece, pero vamos a confiar en que cambien de opinión y al menos podamos jugar aunque sea más tarde que el resto de los mortales:



> 
> We too are sad about it. It isn't that we don't want to support Linux, we always wanted and intended too. It was a difficult decision, and we hope we can deliver a Linux build in the future.
> 
> 
> — Phoenix Point (@Phoenix_Point) [9 de noviembre de 2018](https://twitter.com/Phoenix_Point/status/1060961595696791552?ref_src=twsrc%5Etfw)



 Que, ¿cómo os habeis quedado? ¿Qué opinión os merece todo esto? Al menos siempre nos quedarán los fantásticos ports de Feral de los originales, [XCOM](https://www.humblebundle.com/store/xcom-enemy-unknown?partner=jugandoenlinux) y [XCOM 2](https://www.humblebundle.com/store/xcom-2?partner=jugandoenlinux) para disfrutar en nuestro sistema. Puedes dejarnos tus impresiones sobre el tema en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

