---
author: Pato
category: MOBA
date: 2019-03-07 12:23:30
excerpt: <p>El exitoso juego de Valve sigue evolucionando</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/65829020f6ea67f48aa759d3dd32c68a.webp
joomla_id: 988
joomla_url: el-dios-de-la-guerra-marte-llega-a-dota-2
layout: post
tags:
- moba
- dota-2
title: El dios de la guerra 'Marte' llega a Dota 2
---
El exitoso juego de Valve sigue evolucionando

Si algún juego nativo tenemos que destaca como "MOBA", este es Dota 2 o mejor conocido como "**Defense of the Ancient 2**". Se trata de un juego gratuito de tipo "**Multiplayer Online Battle Arena**" en el que nos enfrentamos con nuestros héroes a otros jugadores en trepidantes partidas de acción estratégica. Si bien no hablamos del videojuego mas jugado en estos momentos, cosa que ahora es cosa de los "Battle Royale" si que estamos hablando de uno de los mas jugados actualmente en PC, a la altura de otros juegos como el LoL o PUBG manteniéndose en el ranking de los primeros desde hace ya años.


Hace un par de días Valve anunció que ya está disponible una nueva deidad en el juego, tal y como recogen en su blog de novedades. En este caso se trata de '**Marte**', el dios de la guerra que llega para liderar una nueva era:


*Marte, dios de la guerra y uno de los primeros del cielo, ahora se han unido a la batalla de los antiguos.*


*Abandonando las formas insensibles de su pasado y abrazando una nueva identidad sin carga por los legados del panteón arcaico de su padre, Marte ha entregado una nueva lanza. Prometiendo derribar los remanentes del viejo mundo salvaje para construir un nuevo imperio, Marte está finalmente listo para asumir el verdadero mando de liderazgo sobre dioses y hombres, a una posición que hasta ahora había sido realizada durante eones por los caprichos de Zeus.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/3450S8MfemE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Si quieres saber mas sobre 'Marte' puedes visitar la [página del nuevo héroe](http://www.dota2.com/mars) en la web de Dota 2, o jugar ya a Dota 2  que por cierto está traducido al español en su [página web en Steam](https://store.steampowered.com/app/570/Dota_2/). ¡Recuerda que puedes jugarlo gratis!


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/SmnqsdeHFT0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>

