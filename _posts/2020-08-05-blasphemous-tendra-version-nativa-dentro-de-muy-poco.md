---
author: leillo1975
category: "Acci\xF3n"
date: 2020-08-05 14:22:22
excerpt: "<p>Con motivo de la salida de la DLC gratuita \"Stir of Dawn\", <span class=\"\
  css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\">@TheGameKitchen</span>\
  \ pone fecha del port de Linux.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Blasphemous/BlasphemousStirOfDawnTux.webp
joomla_id: 1244
joomla_url: blasphemous-tendra-version-nativa-dentro-de-muy-poco
layout: post
tags:
- dlc
- espana
- blasphemous
- the-game-kitchen
- stir-of-dawn
title: "Blasphemous tendr\xE1 versi\xF3n nativa dentro de muy poco."
---
Con motivo de la salida de la DLC gratuita "Stir of Dawn", @TheGameKitchen pone fecha del port de Linux.


 Ya han pasado unos cuantos meses desde que [os anunciamos]({{ "/posts/blasphemous-llegara-a-linux-junto-a-su-primer-dlc" | absolute_url }}) que la versión nativa de [Blasphemous](https://store.steampowered.com/app/774361/Blasphemous/) llegaría conjuntamente con la salida de la DLC gratuita "[Stir of Dawn](https://store.steampowered.com/newshub/app/774361/view/2719563557145696173)". Finalmente esta expansión acaba de llegar, pero en cambio los usuarios de Linux (y también los de Mac) seguimos esperando por el soporte de esta. La verdad es que en ese sentido muchos de los que llevamos meses esperando poder jugar, esta noticia podría decepcionarnos una vez más, pues el juego cumplirá un año dentro en el mercado en poco más de un mes. Pero finalmente parece que nuestras plegarias han tenido su fruto y recientemente han publicado una **actualización sobre el tema** en su página de [Kickstarter](https://www.kickstarter.com/projects/828401966/blasphemous-dark-and-brutal-2d-non-linear-platform/posts/2827460) (gracias **@josegp26** por el aviso):


(Traducción del Inglés) *"The Stir of Dawn" está oficialmente disponible en Mac y Linux*


*Varios problemas nos han impedido entregar el soporte oficial de Mac y Linux, hasta ahora. Nos complace anunciar que finalmente hemos resuelto todo, y Blasphemous (+The Stir of Dawn) ya está llegando a Mac & Linux el 3 de septiembre.*


Realmente , si esto es cierto, ya queda muy poco para echarle el guante a este juego del **estudio Sevillano** [The Game Kitchen](https://thegamekitchen.com/), pero una pregunta nos asalta trás leer esto. **Si todo está ya resuelto, ¿por qué esperar al 3 de septiembre para poder disfrutarlo en nuestros equipos?** Queremos pensar que le queda algún fleco que pulir y quieren lanzar un port con tanta calidad como su juego. Mientras tanto podeis deleitaros con el trailer de la nueva DLC gratuita:  
  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/M_kGG-SD8oA" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Desde JugandoEnLinux.com os adelantamos que en cuanto salga esta versión para GNU/Linux de esta joya del software lúdico español, os informaremos puntualmente para que no pase inadvertido, y por supuesto haremos un stream o grabación especial "estrenando" el juego en nuestro sistema, por lo que permaneced atentos... Puedes dejarnos tus impresiones sobre Blasphemous y The Game Kitchen en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 