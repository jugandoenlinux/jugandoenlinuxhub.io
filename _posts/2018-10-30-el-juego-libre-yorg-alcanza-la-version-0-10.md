---
author: leillo1975
category: Carreras
date: 2018-10-30 14:26:15
excerpt: "<p>@ya2tech acaba de publicar esta nueva actualizaci\xF3n cargada de novedades</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4556f9757414c95d6b9db044b0c9421c.webp
joomla_id: 886
joomla_url: el-juego-libre-yorg-alcanza-la-version-0-10
layout: post
tags:
- carreras
- open-source
- yorg
- ya2
- libre
title: "El juego libre \"YORG\" alcanza la versi\xF3n 0.10."
---
@ya2tech acaba de publicar esta nueva actualización cargada de novedades

Si recordais, hace bien poco [os hablábamos de Speed Dreams](index.php/homepage/generos/simulacion/14-simulacion/994-speed-dreams-disponible-en-flatpak), un juego libre de carreras de coches (simulación), que gracias a **@SonLink** dispone de un **paquete Flatpak** para facilitar su instalación. También de carreras de coches y libre, aunque con una **vertiente mucho más arcade**, tenemos desde ayer la [versión 0.10 de YORG](https://www.ya2.it/articles/yorg-010-has-been-released.html) (***Y***org's an ***O***pen ***R***acing ***G***ame). A través de correo electrónico de uno de sus creadores, Flavio Calva, [al que entrevistamos hace algún tiempo](index.php/homepage/entrevistas/35-entrevista/587-entrevista-a-flavio-calva-de-ya2-yorg); y mediante un tweet, se nos informaba de el lanzamiento de esta nueva "build":



> 
> Yorg 0.10 has been released! <https://t.co/wGgGxDPiDj> [#indiegaming](https://twitter.com/hashtag/indiegaming?src=hash&ref_src=twsrc%5Etfw) [#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/tEfNMvV0Xz](https://t.co/tEfNMvV0Xz)
> 
> 
> — Ya2 technologies (@ya2tech) [October 29, 2018](https://twitter.com/ya2tech/status/1056999772526202882?ref_src=twsrc%5Etfw)



**Esta versión incluye numerosos e importantes cambios**, entre los que ellos destacan (traducción online):


### ***Un nuevo tema, una nueva canción***


*En primer lugar, hemos lanzado un nuevo tema (¡con su canción original!): Sinus Aestuum (¡está en la Luna!). Esta pista tiene varios aspectos interesantes, como caminos alternativos, hoyos,... He aquí una instantanea de una carrera en la nueva pista:*


*[![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/YORG/yorg_0_10_a.webp)](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/YORG/YORG/yorg_0_10_a.jpg)*


*Así que ahora puedes conducir en 7 pistas!*


### ***Nueva cámara***


*Hemos recibido una petición de funciones en el foro de Panda3D por parte de los usuarios sazearte y Uther, que nos han pedido una cámara nueva. Ahora puedes seleccionar una cámara desde atrás, si lo prefieres. Aquí hay una captura de pantalla con la nueva cámara:*


*[![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/YORG//yorg_0_10_b.webp)](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/YORG//yorg_0_10_b.jpg)*


### ***Interfaz gráfica de usuario actualizada***


*Además, como puedes ver en la captura de pantalla anterior, hemos reorganizado las cosas en la interfaz gráfica, siguiendo las sugerencias de Leandro: ¡gracias!*


### ***Multijugador***


*Un aspecto muy importante es que hemos cambiado nuestra anterior implementación multijugador para mejorar la experiencia de los jugadores.*


*Anteriormente, los usuarios necesitaban crear cuentas XMPP externamente. Ahora, puedes crear tus cuentas desde dentro del juego, con un proceso fácil y rápido.*  
*Además, ahora un jugador que quiere alojar un partido no necesita configurar su máquina para aceptar tráfico externo (es decir, ya no necesita abrir un puerto en el router).*


*Por lo tanto, ahora la experiencia multijugador debe ser lo más simple posible.*


### ***Modelo de conducción***


*Hemos recibido algunos comentarios y hemos refinado nuestro modelo de conducción teniéndolos en cuenta*


*[![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/YORG//yorg_screen.webp)](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/YORG//yorg_screen.jpg)*


### ***Traducciones actualizadas***


*Hemos recibido varias contribuciones. Específicamente, hemos actualizado estas traducciones: Gaélico escocés, español, gallego, francés e italiano! Muchas gracias a GunChleoc, Leandro y Xin!*


### ***Trailer***


*Así que, aquí está nuestro nuevo trailer de lanzamiento!*


*<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/1-ePMtYUMv0" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>*


Como podeis ver, la cantidad de cambios y añadidos es significateiva, por lo que os recomendamos a todos que le deis una oportunidad a este juego libre y por supuesto, gratuito, y si os animais, incluso probar su multijugador. [Podeis descargar YORG desde su página](https://www.ya2.it/pages/download.html).


Acabamos de grabar un video probando esta nueva versión donde podeis ver todas las novedades. Esperamos que os guste... y perdonad por mi voz:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/ZFHxDvEus68" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>

