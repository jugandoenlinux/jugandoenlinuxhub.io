---
author: leillo1975
category: Carreras
date: 2018-01-19 14:14:09
excerpt: "<p>Nuevas noticias sober su pr\xF3ximo lanzamiento</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4c704b823894e663c696fb5ee0a8793f.webp
joomla_id: 613
joomla_url: pronto-veremos-drag-un-juego-de-carreras-en-acceso-anticipado
layout: post
tags:
- carreras
- simulacion
- drag
- thorsten-folkers
title: "Pronto veremos DRAG, un juego de carreras en Acceso Anticipado (ACTUALIZACI\xD3\
  N 2)"
---
Nuevas noticias sober su próximo lanzamiento

**ACTUALIZACION 24-5-18**: En una reciente comunicación con los desarrolladores del juego, [Thorsten Folkers](https://www.artstation.com/tfolkers) nos ha comunicado que el desarrollo del juego continua a buen ritmo y sin problemas, aunque reconocen que se han equivocando poniendo una estimación de cuando podría lanzarse el juego. Comentan que actualmente están trabajando en los menues y que quieren mantener un alto estandar de calidad en todo su trabajo. Para ellos es más importante tener un buen material en el lanzamiento que sacar su juego demasiado rápido.


![http://polycount.com/discussion/183339/ong-offroad-racer-indie-game-dev-log](https://us.v-cdn.net/5021068/uploads/editor/1o/3psyinc6sfci.gif)


Seguiremos pendientes de nuevas noticias sobre este espectacular desarrollo para dároslas en cuanto se produzcan.

---


**ACTUALIZACIÓN 27-3-18**: Tras dos meses sin saber nada nuevo del proyecto, finalmente tenemos algunas noticias sobre él, y estas son muy buenas. Los desarrolladores nos comunican que recientemente **han completado los menues y el modo de un solo jugador**, estando muy cerca ya de poderlo lanzar en acceso anticipado. Aun así le quedan bastantes cosas que terminar antes de publicar el juego en Steam, como un HUD para el multijugador, un video del juego para la página de la tienda, o algunos circuitos más (tienen listos 3).  La verdad es que este juego promete, y estaremos atentos a cualquier actualización sobre este proyecto:


![DRAG](https://us.v-cdn.net/5021068/uploads/editor/1o/3psyinc6sfci.gif)


---


**NOTICIA ORIGINAL**: De todos es sabido que nuestros colegas de [GamingOnLinux.com](https://www.gamingonlinux.com/articles/realistic-racing-game-drag-coming-to-linux-soon-built-on-linux-and-it-looks-astonishing.11069) son una fuente inagotable de noticias, y por supuesto una gran inspiración. Gracias a ellos nos hemos enterado de la existencia de este proyecto, que así de buenas a primeras impresiona. Tras buscar información diversa por la red hemos decidido ponernos en contacto con ellos y han sido muy amables (y rápidos) en facilitarnos suculenta información sobre este juego tan prometedor. Podríamos procesar toda la información ofrecida y dárosla masticada, pero las explicaciónes de [Thorsten Folkers](https://tfolkers.artstation.com/), que así se hacen llamar estos dos hermanos alemanes,  son tan claras que creemos que lo mejor es simplemente traducir su mensaje de respuesta. Y esto es lo que nos han contestado:


A la pregunta sobre las **herramientas y sistemas** que han usado para el juego nos han contestado:



> *"Nosotros hemos construido **nuestro propio motor bajo Linux** en los últimos años. Está escrito en C++11 y tiene una interfaz Python para scripting. **Soporta juego online completamente**, incluyendo la unión tardía a la partida y el modo espectador. Tenemos un servidor de emparejamiento (Matchmaking) operativo en Linux con servidores de juegos agrupados. El modo multiplayer soporta tanto juego online, como local, **incluso con pantalla partida*** (es posible jugar dos personas en el mismo ordenador y a la vez jugar online con otros jugadores) *. Funciona tanto en Windows como en Linux usando OpenGL 4.5 como API de renderización, pero está contemplado y planeado el soporte de Vulkan y Direct3D."* 

Hablando un poco sobre ellos mismos y su compañía su respuesta ha sido la siguiente:

> 
> *"Nosotros fundamos una compañía en el sur de Alemania llamada "Orontes Games", pero a partir de ahora **somos solo dos persona trabajando en este juego**. Mi hermano es el desarrollador del motor gráfico y el programador del juego. Yo soy el diseñador/artista que se encarga de crear los vehículos, el entorno y el arte de nivel. Así que somos dos verdaderos desarrolladores indies ;-)*
> 
> 
> 


> 
> 
> 
> *Somos ambos unos "petrolheads" y disfrutamos de las carreras competitivas. Construir un juego de carreras que ya disfrutamos mucho jugando es una gran experiencia."*
> 
> 
> 
> 
> 


En cuanto a las características del juego han sido bastante técnicos y por lo que se deduce el juego **irá más encaminado hacia la simulación que el arcade**:

> 
> *"Hemos creado una **física de 4 suspensiones** adecuada que simula el automóvil altamente realista. De esta manera tu puedes sobrevirar y subvirar como en un coche de rally.  El coche tiene tracción total en las cuatro ruedas con una distribución de potencia de 20-80 de adelante a hacia atrás, y un tiene un equilibrio de peso de 50/50.* 
> 
> 
> *Los jugadores experimentan una transferencia de peso bajo frenado o aceleración que puede causar un sobreviraje de despegue. Si vas a acelerar y pisar los frenos en la "mid-corner" (?!?) , experimentarás subviraje, como en un automóvil real. Como piloto de carreras puedes usar todos estos efectos en el coche para tu ventaja. Configuramos el automóvil para que tenga un equilibrio perfecto en términos de exceso de subviraje, pero como es un auto de carreras, **no tendrá asistentes electrónicos** como el Control de Tracción o el ASR. Es una vuelta  a la época dorada de los rallyes del Grupo B, se trataba solo de ti y de la máquina. "Si todo parece estar bajo control, tu no estás llendo lo suficientemente rápido" ---Mario Andretti*
> 
> 
> 

> 
> 
> 
> *Si quereis ver información adicional e imágenes en este artículo:*   
> *[https://80.lv/articles/building-a-racing-game-with-your-own-engine/"](https://80.lv/articles/building-a-racing-game-with-your-own-engine/)*
> 
> 
> ![DRAG Susp](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DRAG/DRAG-Susp.webp)
> 
> 

 
DRAG llegará pronto a Steam en Acceso Anticipado , sobre los meses de Marzo o Abril, por lo que en JugandoEnLinux.com estarmos ojo avizor en este proyecto para traerte todas las noticias que este produzca e informaros de su disponibilidad. ¿Qué os parece DRAG? ¿Qué opinais de que esté siendo desarrollado enteramente en Linux? Contestanos en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).
