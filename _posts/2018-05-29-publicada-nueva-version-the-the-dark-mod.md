---
author: leillo1975
category: "Acci\xF3n"
date: 2018-05-29 14:38:25
excerpt: <p>Conoce las novedades de este juego Open Source</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6384a1a5c65741ab6f8625372f3d4127.webp
joomla_id: 749
joomla_url: publicada-nueva-version-the-the-dark-mod
layout: post
tags:
- mod
- idtech-4
- doom-3
- the-dark-mod
title: "Publicada nueva versi\xF3n the \"The Dark Mod\"."
---
Conoce las novedades de este juego Open Source

Gracias de nuevo a [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=TDM-2.06-idTech4-2018-Progress) nos enteramos de que esta conocida y completa modificación de Doom 3 ha llegado a su versión 2.06. Para los que no lo conozcais se trata de un juego de acción sigilosa al estilo Thief construida con IDTech 4, motor gráfico liberado por ID Software en el año 2011. El juego incluye notables [actualizaciones](http://www.thedarkmod.com/posts/the-dark-mod-2-06-now-released/) que podemos resumir en las siguientes:



> 
> **-Mejoras visuales:** se han implementado "Soft Shadows" que permiten borrar los bordes perfilados de las sombras (experimental).
> 
> 
> **-Mejor Sonido:** El audio EFX permite a los poseedores de tarjetas Creative el activar el sonido EAX en el menú. Esto permite utilizar efectos envolventes en el juego en algunos mapas del juego.
> 
> 
> **-Mejores menues:** estos se han rediseñado para adaptarlos a las pantallas panorámicas, dotándolos también de  mejores resoluciones y nuevas opciones como transparencias en el HUD.
> 
> 
> -**Nuevos Elementos:** tales como módulos arquitectónicos y prefabricados que permitiran a los creadores realizar el trabajo más rápido.
> 
> 
> **-Mejores introducciones de misión:** Permite el uso de FFMPEG, además de ROQ, lo que permitirá que los mapeadores lo tengan más fácil a la hora de crear introducciones para sus niveles.
> 
> 
> **-Mejor jugabilidad:** se corrigen diversos errores, entre los que están la recuperación de vida cuando comemos o que los guardias puedan dormir y despertarse al estar sentados.
> 
> 
> **-Mejor Rendimiento:** Se incluye el uso de soporte multinúcleo, lo que permitirá más velocidad en el juego, aunque se trata de una opción experimental que puede causar errores y cierres inesperados.
> 
> 
> 


Como sabeis, de IDTech 3 hay un montón de juegos (aka ioQuake, tales como Urban Terror, Open Arena, Red Eclipse... ), pero de IdTech 4 es uno de los pocos desarrollos realizados , aunque no por ello deja de tener una calidad excelente. En JugandoEnLinux.com os recomendamos que le deis una oportunidad a este juego Open Source, pues no os va a defraudar.  El juego, por supuesto, puede ser descargado gratuitamente de su [página](http://www.thedarkmod.com/download-the-mod/) . Os dejamos con su trailer:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/brJqHnXmpgE" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Conocíais "The Dark Mod"? ¿Lo habeis jugado ya? Déjanos tu opinión en los comentarios o en nuestro grupos de [Telegram](https://t.me/jugandoenlinux) o [Discord](https://discord.gg/fgQubVY).

