---
author: Serjor
category: Estrategia
date: 2017-10-17 21:03:32
excerpt: "<p>Un RTS espacial para los amantes de gal\xE1ctica</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ab9083c4344a26f28a8b1378ce88e8e8.webp
joomla_id: 493
joomla_url: battlevoid-sector-siege-llegara-a-gnu-linux-el-proximo-25-de-octubre
layout: post
tags:
- rts
- estrategia
- battlevoid-sector-siege
title: "Battlevoid: Sector Siege llegar\xE1 a GNU/Linux el pr\xF3ximo 25 de octubre"
---
Un RTS espacial para los amantes de galáctica

El próximo 25 de octubre los chicos de Bugbyte pondrán a la venta en Steam su Battlevoid: Sector Siege, el cuál llegará a GNU/Linux, Mac y Windows el mismo día, cosa que agradecemos enormemente.


Este Battlevoid: Sector Siege es un RTS 2D que nos obligará a gestionar recursos, establecer una buena táctica y ser buenos estrategas.


Como podemos leer en su [página de steam](http://store.steampowered.com/app/716630/Battlevoid_Sector_Siege/?utm_source=SteamDB&utm_medium=SteamDB&utm_campaign=SteamDB%20App%20Page)


**Características principales:**


* Juego multiplataforma
* Explora mapas nuevos generados por procedimientos en cada partida
* Personalización detallada de cada unidad que creas
* Aborda naves enemigas y crea copias de estas
* Generación aleatoria de eventos
* Gráficos y efectos pixelados fabulosos


La verdad es que tiene una pinta bastante curiosa, y a mi me ha recordado mucho a los combates de Battlestar Galactica y a Gratuitous Space Battles.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/neU5X6rUJsk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/716630/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Y tú, ¿Quieres emular al mismísimo Adama? Cuéntanos si te interesa este juego en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

