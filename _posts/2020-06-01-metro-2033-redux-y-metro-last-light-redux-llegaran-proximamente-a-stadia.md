---
author: Pato
category: "Acci\xF3n"
date: 2020-06-01 10:18:16
excerpt: "<p>Siguen llegando t\xEDtulos a la plataforma de streaming de Google.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/Stadia_2-Games-MetroRedux.webp
joomla_id: 1227
joomla_url: metro-2033-redux-y-metro-last-light-redux-llegaran-proximamente-a-stadia
layout: post
tags:
- stadia
- metro-2033-redux
- metro-last-light-redux
title: "Metro 2033 Redux y Metro Last Light Redux llegar\xE1n pr\xF3ximamente a Stadia"
---
Siguen llegando títulos a la plataforma de streaming de Google.


Siguen las incorporaciones a la lista de títulos de Stadia. Tal y como estamos viendo desde hace algún tiempo, algunos de los títulos que se espera que vayan llegando a la plataforma de streaming son títulos que ya disponen o van (o iban) a disponer de versiones nativas para Linux en otras plataformas, un movimiento de algún modo "natural" ya que Stadia corre bajo servidores Linux. 


Es el caso de los dos títulos que han anunciado para próximas fechas la llegada a **Stadia**:


**Metro 2033 Redux**:


*Metro 2033 Redux es la versión definitiva del clásico de culto "Metro 2033", rediseñado con la mejor y más reciente versión del motor 4A.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/dwLcizrqDWg" width="560"></iframe></div>


 


**Metro Last Light Redux:**


*Metro: Last Light Redux es la versión definitiva del juego elogiado por la crítica "Metro: Last Light", rediseñado con la mejor y más reciente versión del motor 4A*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/LSZhNXK1oKs" width="560"></iframe></div>


Los dos títulos estarán disponibles en la plataforma en próximas fechas, aún sin concretar.


Puedes ver el anuncio en el blog de Stadia.


¿Que te parece la llegada de títulos con versión nativa Linux a Stadia? ¿Piensas que es una estrategia acertada?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 


 


 

