---
author: Serjor
category: Terror
date: 2017-04-20 17:33:00
excerpt: "<p>Por fin los ping\xFCinos podr\xE1n jugar juntos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/45ff2190802f9793d44160c4e551925c.webp
joomla_id: 295
joomla_url: dying-light-se-actualiza-para-solucionar-los-problemas-del-modo-cooperativo-entre-jugadores-de-gnu-linux
layout: post
tags:
- actualizacion
- dying-light
title: Dying Light se actualiza para solucionar los problemas del modo cooperativo
  entre jugadores de GNU/Linux
---
Por fin los pingüinos podrán jugar juntos

TechLand ha lanzado una actualización que principalmente se centra en la versión para Linux. Por un lado, y de manera genérica, este parche soluciona un problema relacionado con el seguimiento con los ojos cuando un usuario tenía configurado en su equipo un valor de DPI personalizado en resoluciones no nativas.


Pero por otro lado, las correcciones que más nos interesan son que ya está solucionado el problema del juego en modo cooperativo entre usuarios de la versión para Linux, y que ahora cada vez que hablemos por el micrófono, el juego no creerá siempre que estamos chillando y los zombies no vendrán a por nosotros tan facilmente. Si queréis podéis ver el listado [aquí](https://steamcommunity.com/gid/103582791434300000/announcements/detail/1294065920977529834), aunque no es muy extenso.


 


Curiosamente nunca me había dado cuenta de ambos problemas, pero es de agradecer que aunque sea después de casi dos años desde su lanzamiento en Linux, hayan solucionado estos problemas.


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/239140/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 


Y tú, ¿ya has elegido con qué otro miembro de JEL vas a disfrutar matando zombies? Aprovecha y organiza unas partidas con la comunidad ya sea en los comentarios o a través de los grupos de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).


Fuente: [gamingonlinux.com](https://www.gamingonlinux.com/articles/techland-claim-to-have-finally-fixed-linux-co-op-in-dying-light.9539)

