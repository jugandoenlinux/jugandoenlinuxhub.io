---
author: Pato
category: "Acci\xF3n"
date: 2018-03-13 10:10:48
excerpt: <p>Supervivencia en vista cenital dentro de una nave espacial</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5b74e738223d177a4030153c35f64896.webp
joomla_id: 675
joomla_url: xenosis-alien-infection-llegara-a-linux-steamos-dentro-de-un-ano
layout: post
tags:
- accion
- indie
- proximamente
- rol
- violento
title: "'Xenosis: Alien Infection' llegar\xE1 a Linux/SteamOS dentro de un a\xF1o"
---
Supervivencia en vista cenital dentro de una nave espacial

Imagina que eres un "chatarrero espacial" en busca de un tesoro, o algo a lo que echarle el diente para poder trapichear. Imagina que descubres por el azar del destino una nave espacial, la "Carpathian" que supuestamente fue destruida en una misión de terraformación de un planeta en el espacio profundo hace 50 años. ¡Menudo golpe de suerte! ¿Verdad?


¡Los componentes de esa nave deben valer una fortuna en el mercado negro!. Así que te dispones a entrar en la nave y "tomar prestadas" algunas cosas. En principio la nave parece vacía, no hay signos de vida, y no hay atmósfera respirable. Después de 50 años perdida en el espacio no esperarás encontrar un coctel de bienvenida en la nave, ¿verdad?. Sin embargo, durante la exploración aparecen signos de una toxina no identificada...


De repente, la nave cobra vida y las puertas se bloquean. Y comienzas a recibir una transmisión de alguien en lo profundo de la Carpathian que te alerta de que no estás solo...


Con esta premisa 'Xenosis: Alien Infection' [[web oficial](http://www.xenosisgame.com/)] nos propone una aventura espacial de exploración, acción y supervivencia en vista cenital de estilo retro enfocada en una historia para un jugador, donde las luces 3D, las sombras dinámicas y el sonido envolvente en 3D te sumergirán en una experiencia inmersiva única.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/-P0u8g0j4Rg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


En 'Xenosis: Alien Infection' tendremos que explorar, recolectar elementos y tratar de sobrevivir mejorando el armamento y esquivando aliens en intensos combates. Buscaremos oxígeno, dosificaremos las provisiones y trataremos de escapar de la Carpathian mientras tratamos de desvelar la historia que nos ha llevado hasta allí.


En cuanto a los requisitos, teniendo en cuenta que aún queda un año de desarrollo me parece que aún es algo prematuro hablar de ellos, pero en Steam tienen listados los siguientes:


**Mínimo:**  

+ **SO:** Ubuntu 12.04 o superior (o similar Linux Distro)
+ **Procesador:** x86 Intel i3 / AMD Equivalente
+ **Gráficos:** Nvidia o ATI GPU (DX9 SM3) 1GB


**Recomendado:**  

+ **SO:** Ubuntu 16.10
+ **Procesador:** x86 Intel i5 o superior / AMD Equivalente
+ **Gráficos:** Nvidia GTX 7xx o superior / ATI equivalente (DX11 SM3) 2GB


¿Que te parece la propuesta de 'Xenosis: Alien Infection'?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).



