---
author: Pato
category: Hardware
date: 2017-11-23 17:15:58
excerpt: <p>Comienzan a darse pasos para dar el soporte adecuado al dispositivo</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8f704c6e91e045c72378c71d940a59ce.webp
joomla_id: 551
joomla_url: el-kernel-linux-4-15-tratara-al-htc-vive-como-un-dispositivo-aparte-del-escritorio
layout: post
tags:
- steamvr
- htc-vive
title: "El Kernel Linux 4.15 tratar\xE1 al HTC Vive como un dispositivo aparte del\
  \ escritorio"
---
Comienzan a darse pasos para dar el soporte adecuado al dispositivo

Leemos en Phoronix que se están llevando a cabo diferentes peticiones para incluir el soporte en el Kernel Linux 4.15 para el casco de realidad virtual HTC Vive.


Hasta ahora, cuando se conectaba un HTC Vive el kernel Linux lo trataba como un dispositivo de escritorio, cosa que no es y por tanto no es capaz de aprovechar bien sus características. Para solventar esto, uno de los mantenedores ha hecho una petición para incluir en el DRM del Kernel 4.15 un conector apropiado para dispositivos que no sean de escritorio, aparte de otras características en los drívers para soportar mejor a estos dispositivos.


Esperamos ver estas mejoras cuando se lance dicho Kernel, por que esto supone un buen empujón en la adopción de este dispositivo como Casco VR en Linux.


Fuente: [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Linux-4.15-Non-Desktop-Quirk)

