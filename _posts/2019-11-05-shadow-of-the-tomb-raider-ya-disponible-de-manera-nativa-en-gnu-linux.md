---
author: Serjor
category: Aventuras
date: 2019-11-05 21:44:10
excerpt: "<p>Lara Croft contin\xFAa su aventura en las tierras del ping\xFCino</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/40b9b55f0670c8418c21bc05f68a28a0.webp
joomla_id: 1127
joomla_url: shadow-of-the-tomb-raider-ya-disponible-de-manera-nativa-en-gnu-linux
layout: post
tags:
- feral-interactive
- linux
- tomb-raider
- linux-gaming
title: Shadow of The Tomb Raider ya disponible de manera nativa en GNU/Linux
---
Lara Croft continúa su aventura en las tierras del pingüino

Ya os lo [avisábamos](index.php/homepage/generos/accion/5-accion/1242-shadow-of-the-tomb-raider-desembarcara-pronto-en-linux-steamos), y es que hoy 5 de noviembre se ponía a la venta la "Definitive Edition" del juego Shadow of The Tomb Raider, fecha que Feral Interactive ha aprovechado para poner a la venta la versión nativa para GNU/Linux y MacOS.


Y tal como nos prometieron, ya tenemos disponible esta nueva aventura de Lara Croft que sigue la historia del último reboot, y continúa donde lo dejó el anterior Rise of The Tomb Raider, en una historia, que promete ser más oscura.


Lamentablemente no hemos podido contar con una versión del juego para hacer ningún tipo de contenido propio y comentaros de primera mano nuestras primeras impresiones, así que os dejamos con el trailer del juego.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/bugahtf9Sdg" width="560"></iframe></div>


Podéis haceros con este juego directamente desde la [tienda](https://store.feralinteractive.com/en/mac-linux-games/shadowofthetombraider/) de Feral Interactive, que es la mejor manera de apoyar a esta compañía y seguir fomentando los ports nativos. También desde este enlace a [Humble Bundle](https://www.humblebundle.com/store/shadow-of-the-tomb-raider-definitive-edition?partner=jugandoenlinux), con el que nos ayudaríais económicamente a nosotros, o desde Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/750920/" style="border: 0px;" width="646"></iframe></div>


Y tú, ¿piensas hacerte con esta nueva aventura de Lara Croft? Cuéntanoslo en los comantarios o en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux)

