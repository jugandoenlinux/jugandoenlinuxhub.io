---
author: Serjor
category: Software
date: 2019-03-23 09:45:28
excerpt: "<p>Aunque se trata de una actualizaci\xF3n menor, merece la pena echarle\
  \ un vistazo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1b61b449206bee159b838ef0eb1537ef.webp
joomla_id: 1005
joomla_url: la-version-0-5-1-de-lutris-ya-esta-disponible
layout: post
tags:
- lutris
title: "La versi\xF3n 0.5.1 de lutris ya est\xE1 disponible"
---
Aunque se trata de una actualización menor, merece la pena echarle un vistazo

Hace unas horas la gente de Lutris anunciaban que la versión [0.5.1 está ya disponible](https://github.com/lutris/lutris/releases/tag/v0.5.1):




> 
> Lutris 0.5.1 is out! <https://t.co/KmvEsrHlWS>  
>   
> Updates should be rolling out soon on all distros, if you are on Ubuntu, we have a new PPA you can use! <https://t.co/7sI0T6fca1>  
>   
> Don't forget to show your support if you like this release! <https://t.co/MWjL8J0nN2>
> 
> 
> — Lutris Gaming (@LutrisGaming) [23 de marzo de 2019](https://twitter.com/LutrisGaming/status/1109254539029315584?ref_src=twsrc%5Etfw)



Al ser una actualización menor el listado de cambios no trae grandes novedades, pero sí les ha dado tiempo a añadir una característica bastante interesante y es que si ejecutamos lutris con el parámetro `--submit-issue`, lutris se abrirá con un diálogo para indicar cuál ha sido el problema y generará un fichero que podremos adjuntar en la incidencia que abramos. Es cierto que es una solución un tanto reactiva, y que no sucede justo cuando tenemos el problema, pero es un primer paso a mejorar la recolección de datos que proporcionamos al equipo de Lutris y de esta manera hacerles algo más sencillo el proceso de depuración, y dicen que la integración con su API vendrá más adelante.


A parte de esto, el listado de cambios:


* Se descarga de manera automática la versión por defecto de wine de Lutris si esta no está disponible
* Ya no se generan duplicados cuando se importan juegos de servicios de terceros (pj GOG)
* Añadido soporte de imágenes de CD-ROM para modelos de Amiga que no fueran CD32/CDTV
* Se ha eliminado la opción de búsqueda en la web de la barra lateral y se ha integrado con la búsqueda principal
* Se muestra un mensaje de aviso si el driver de NVIDI es demasiado viejo
* Corregido el problema de no poder instalar los juegos de GOG si Lutris no estaba conectado a GOG
* Si se usa Proton se elminina winecfg
* Uso por defecto de la gráfica integrada en sistemas compatibles
* Mejoras varias y correcciones de errores


Lo dicho, no son los cambios más llamativos del mundo, pero siempre está bien tener estos pequeños avances.


Y como anécdota, durante la GDC de este año Red Hat ha montado un stand para promocionar la filosofía Open Source, y en las máquinas que ha usado para demostrar las posibilidades, ha usado Lutris como gestor de videojuegos:




> 
> Did you know that [@RedHat](https://twitter.com/RedHat?ref_src=twsrc%5Etfw) used Lutris to run games on their [#GDC19](https://twitter.com/hashtag/GDC19?src=hash&ref_src=twsrc%5Etfw) machines? It's really wonderful to see them embracing and promoting Linux gaming ❤️ [pic.twitter.com/V8P4Im83w4](https://t.co/V8P4Im83w4)
> 
> 
> — Lutris Gaming (@LutrisGaming) [22 de marzo de 2019](https://twitter.com/LutrisGaming/status/1109181326849163265?ref_src=twsrc%5Etfw)



Y tú, ¿usas Lutris como biblioteca de videojuegos? Cuéntanoslo en los comentarios y en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

