---
author: Serjor
category: Estrategia
date: 2018-03-23 17:36:54
excerpt: <p>Aventuras y estrategia por turnos en formato pixel-art</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b9ad772005653afce4d4bd46c2efe842.webp
joomla_id: 692
joomla_url: pathway-llegara-a-gnu-linux-a-lo-largo-del-2018
layout: post
tags:
- estrategia
- pathway
- estrategia-por-turnos
title: "Pathway llegar\xE1 a GNU/Linux a lo largo del 2018"
---
Aventuras y estrategia por turnos en formato pixel-art

Vía nuestro canal de Telegram nos pegan un tirón de orejas por no haber hablado antes de este juego:







Según el [enlace a elotrolado.net](https://www.elotrolado.net/noticia_trailer-y-detalles-de-pathway-aventura-con-combates-tacticos-por-turnos-y-cuidada-estetica-retro_35657), Chucklefish se encargará de distribuir [Pahtway](http://www.pathway-game.com/), un juego de Robotality, con una clara inspiración en Indiana Jones, tanto en trama como ambientación, ya que el juego discurre durante 1936, y tendremos que ir tras los Nazis para rescatar a nuestro amigo Morten.


El juego se basa en la estrategia por turnos, con una estética pixel art, donde las campañas se generan de manera procedural.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/JW7HkW8866M" width="560"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/546430/" style="border: 0px;" width="646"><p>Y a ti, ¿qué te parece este Pahtway? Cuéntanoslo en los comentarios, o en nuestros canales de <a href="https://telegram.me/jugandoenlinux">Telegram</a> o <a href="https://discord.gg/ftcmBjD">Discord</a></p></iframe></div>

