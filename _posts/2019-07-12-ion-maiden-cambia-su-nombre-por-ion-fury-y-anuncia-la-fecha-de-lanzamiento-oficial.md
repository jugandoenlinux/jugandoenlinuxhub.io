---
author: leillo1975
category: "Acci\xF3n"
date: 2019-07-12 09:04:21
excerpt: <p>@voidpnt rebautiza su juego para evitar problemas legales.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9f1687b36272682d88ef7d3481444c51.webp
joomla_id: 1075
joomla_url: ion-maiden-cambia-su-nombre-por-ion-fury-y-anuncia-la-fecha-de-lanzamiento-oficial
layout: post
tags:
- accion
- indie
- acceso-anticipado
- retro
- gore
- voidpoint
title: '"Ion Maiden" cambia su nombre por "Ion Fury" y anuncia la fecha de lanzamiento
  oficial.'
---
@voidpnt rebautiza su juego para evitar problemas legales.

Si estais al tanto de la actualidad en el mundo de los videojuegos (y del Heavy Metal) sabreis que recientemente un equipo legal que representa a la conocida banda británica [Iron Maiden](https://ironmaiden.com/), [demandó](https://www.reddit.com/r/ionmaiden/comments/bunlbl/heavy_metal_band_iron_maiden_brings_trademark/) a la gente que está detrás del juego "Ion Maiden", del que [ya habamos aquí en varias ocasiones](index.php/homepage/generos/accion/5-accion/786-3d-realms-vuelve-ion-maiden-llega-a-gnu-linux-steamos-en-acceso-anticipado), por utilizar un nombre parecido, que el logo del juego recuerda al de la banda, y que la calavera que sale en él podría estar basada en su mascota "Eddie", entre otras muchas cosas . A nivel personal, como fiel seguidor de la banda creo que todos estos argumentos están cogidos por los pelos y hay que tener mucha imaginación para ver los parecidos, pero por si acaso la compañía [Voidpoint](https://twitter.com/voidpnt), finalmente ha decidido cambiar el nombre, quedando finalmente [Ion Fury](http://www.ionfury.com/), que la verdad tampoco suena nada mal. Este anuncio nos llegaba a través del correo electrónico directamente gracias a [StridePR](https://www.stridepr.com/) (gracias), y también tenía su eco en las redes sociales:



> 
> ION MAIDEN is now ION FURY!  
> ARRIVES ON PC, AUGUST 15!  
>   
> NEW TRAILER, NEW BIG BOX, NEW SCREENSHOTS, NEW GIFS, NEW KEY ART! GET IT HERE: <https://t.co/7RewD9J37j><https://t.co/7OU3tsOmNB>
> 
> 
> — Ion Fury (@Bombshell_Game) [11 de julio de 2019](https://twitter.com/Bombshell_Game/status/1149310248613744642?ref_src=twsrc%5Etfw)


  





El caso es que aprovechando el anuncio del cambio de nombre, como podeis ver también anuncian que **el juego finalmente saldrá del Acceso Anticipado y verá la luz oficialmente el 15 de Agosto**. Este juego construido sobre el conocido **motor Build** que dió forma en los añorados 90'  a joyas como Duke Nukem o el Shadow Warrior clásico, conjuga la jugabilidad y gráficos de aquella época, con elementos más modernos como físicas, resoluciones, auto-salvados o headshots, además de por supuesto adoptar los estándares de los controles actuales al manejo del juego.


Ion Fury además se podrá comprar, a parte de la edición digital, en una **caja especial con un montón de extras** que encantarán a los fans del juego. La **BIG BOX**, que es así como se llama, incluirá una unidad USB en forma de Bowling Bomb cargada con una versión libre de DRM del juego y la banda sonora, un póster, una tarjeta de claves basada en las que Shelly encuentra en el juego, un making-of booklet, un juego de pegatinas de Bowling Bomb y, por supuesto, Ion Fury, todo ello por 59,99€.


Recordad que podeis ahora mismo podeis comprar el juego por **19.99€**, pero **a partir del 18 de Julio este pasará a costar 24.99€**, por lo que si estais pensando en comprarlo ahora es el momento. Podeis encontrarlo en [Steam](https://store.steampowered.com/app/562860/Ion_Fury/) y [GOG](https://www.gog.com/game/ion_fury). Nosotros os seguiremos informando sobre el juego, y por supuesto os recordaremos el lanzamiento oficial cuando se produzca. Os dejamos con el Trailer que acompaña al anuncio:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/zjnhRz_GP3c" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Habeis jugado ya en Early Access a Ion Fury (o Ion Maiden)? ¿Qué os parecen este tipo de juegos retro que rememoran tiempos pasados? Cuéntanoslo en los comentarios o deja tus mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

