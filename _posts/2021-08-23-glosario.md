---
author: Jugando en Linux
category: Info+Tuto
date: 2021-08-23 15:49:02
excerpt: "<p>{jcomments off}</p>\r\n<p style=\"text-align: center;\"><a href=\"#A\"\
  >A</a> <a href=\"#B\">B</a> <a href=\"#C\">C</a> <a href=\"#D\">D</a> <a href=\"\
  #E\">E</a> <a href=\"#F\">F</a> <a href=\"#G\">G</a> <a href=\"#H\">H</a> <a href=\"\
  #I\">I</a> <a href=\"#J\">J</a> <a href=\"#K\">K</a> <a href=\"#L\">L</a> <a href=\"\
  #M\">M</a> <a href=\"#N\">N</a> <a href=\"#O\">O</a> <a href=\"#P\">P</a> <a href=\"\
  #Q\">Q</a> <a href=\"#R\">R</a> <a href=\"#S\">S</a> <a href=\"#T\">T</a> <a href=\"\
  #U\">U</a> <a href=\"#V\">V</a> <a href=\"#W\">W</a> <a href=\"#X\">X</a> <a href=\"\
  #Y\">Y</a> <a href=\"#Z\">Z</a></p>\r\n<hr />\r\n<p><strong><a id=\"A\">A</a></strong></p>\r\
  \n<p><strong><a id=\"B\">B</a></strong></p>\r\n<p><strong><a id=\"C\">C</a></strong></p>\r\
  \n<p><strong><a id=\"D\">D</a></strong></p>\r\n<dl><dt><strong>Drivers:</strong></dt><dd>definici\xF3\
  n</dd><dd></dd><dt><strong>DXVK:</strong></dt><dd>Definici\xF3n</dd></dl>\r\n<p><strong><a\
  \ id=\"E\">E</a></strong></p>\r\n<p><strong><a id=\"F\">F</a></strong></p>\r\n<p><strong><a\
  \ id=\"G\">G</a></strong></p>\r\n<dl><dt>\r\n<p><strong>GNU/LINUX:&nbsp;</strong></p>\r\
  \n</dt><dd>Sistema operativo formado por la uni\xF3n del conjunto de las herramientas\
  \ b\xE1sicas del proyecto GNU, y el n\xFAcleo LINUX derivado de UNIX creado por\
  \ Linus Torvalds. Para m\xE1s informaci\xF3n, visita su p\xE1gina en Wikipedia:\
  \ <a href=\"https://es.wikipedia.org/wiki/GNU/Linux\" target=\"_blank\" rel=\"noopener\"\
  >https://es.wikipedia.org/wiki/GNU/Linux</a></dd></dl>\r\n<p><strong><a id=\"H\"\
  >H</a></strong></p>\r\n<dl><dt><strong>Heroic Games Launcher:</strong></dt><dd>Definici\xF3\
  n</dd></dl>\r\n<p><strong><a id=\"I\">I</a></strong></p>\r\n<p><strong><a id=\"\
  J\">J</a></strong></p>\r\n<p><strong><a id=\"K\">K</a></strong></p>\r\n<p><strong><a\
  \ id=\"L\">L</a></strong></p>\r\n<dl><dt><strong>Lutris:</strong></dt><dd>Definici\xF3\
  n</dd></dl>\r\n<p><strong><a id=\"M\">M</a></strong></p>\r\n<dl><dt><strong>Mesa:</strong></dt><dd>Definici\xF3\
  n</dd></dl>\r\n<p><strong><a id=\"N\">N</a></strong></p>\r\n<p><strong><a id=\"\
  O\">O</a></strong></p>\r\n<dl><dt><strong>OpenGL:</strong></dt><dd>Definici\xF3\
  n</dd></dl>\r\n<p><strong><a id=\"P\">P</a></strong></p>\r\n<dl><dt><strong>Proton:</strong></dt><dd>Definici\xF3\
  n</dd></dl>\r\n<p><strong><a id=\"Q\">Q</a></strong></p>\r\n<p><strong><a id=\"\
  R\">R</a></strong></p>\r\n<p><strong><a id=\"S\">S</a></strong></p>\r\n<dl><dt><strong>Steam:</strong></dt><dd>Plataforma\
  \ de distribuci\xF3n de software y videojuegos creada por Valve Corp. Ofrece soporte\
  \ para sistemas GNU/Linux desde el a\xF1o 2013. Para m\xE1s informaci\xF3n, puedes\
  \ visitar su p\xE1gina en Wikipedia:&nbsp;<a href=\"https://es.wikipedia.org/wiki/Steam\"\
  \ target=\"_blank\" rel=\"noopener\">https://es.wikipedia.org/wiki/Steam</a></dd></dl>\r\
  \n<p><strong>Steam Deck:</strong></p>\r\n<dl><dd>Definici\xF3n</dd></dl>\r\n<p><strong>Steam\
  \ Play:</strong></p>\r\n<dl><dd>Definici\xF3n</dd><dt></dt></dl>\r\n<p><strong><a\
  \ id=\"T\">T</a></strong></p>\r\n<p><strong><a id=\"U\">U</a></strong></p>\r\n<p><strong><a\
  \ id=\"V\">V</a></strong></p>\r\n<dl><dt><strong>VK3D:</strong></dt><dd>Definici\xF3\
  n</dd></dl><dl><dt><strong>Vulkan:</strong></dt><dd>Definici\xF3n</dd></dl>\r\n\
  <p><strong><a id=\"W\">W</a></strong></p>\r\n<dl><dt><strong>WINE:</strong></dt><dd>Definici\xF3\
  n</dd></dl>\r\n<p><strong><a id=\"X\">X</a></strong></p>\r\n<p><strong><a id=\"\
  Y\">Y</a></strong></p>\r\n<p><strong><a id=\"Z\">Z</a></strong></p>"
joomla_id: 1338
joomla_url: glosario
layout: post
title: "Glosario de t\xE9rminos"
---
{jcomments off}


[A](#A) [B](#B) [C](#C) [D](#D) [E](#E) [F](#F) [G](#G) [H](#H) [I](#I) [J](#J) [K](#K) [L](#L) [M](#M) [N](#N) [O](#O) [P](#P) [Q](#Q) [R](#R) [S](#S) [T](#T) [U](#U) [V](#V) [W](#W) [X](#X) [Y](#Y) [Z](#Z)




---


**A**


**B**


**C**


**D**


**Drivers:**definición**DXVK:**Definición
**E**


**F**


**G**



**GNU/LINUX:**


Sistema operativo formado por la unión del conjunto de las herramientas básicas del proyecto GNU, y el núcleo LINUX derivado de UNIX creado por Linus Torvalds. Para más información, visita su página en Wikipedia: <https://es.wikipedia.org/wiki/GNU/Linux>
**H**


**Heroic Games Launcher:**Definición
**I**


**J**


**K**


**L**


**Lutris:**Definición
**M**


**Mesa:**Definición
**N**


**O**


**OpenGL:**Definición
**P**


**Proton:**Definición
**Q**


**R**


**S**


**Steam:**Plataforma de distribución de software y videojuegos creada por Valve Corp. Ofrece soporte para sistemas GNU/Linux desde el año 2013. Para más información, puedes visitar su página en Wikipedia: <https://es.wikipedia.org/wiki/Steam>
**Steam Deck:**


Definición
**Steam Play:**


Definición
**T**


**U**


**V**


**VK3D:**Definición**Vulkan:**Definición
**W**


**WINE:**Definición
**X**


**Y**


**Z**

