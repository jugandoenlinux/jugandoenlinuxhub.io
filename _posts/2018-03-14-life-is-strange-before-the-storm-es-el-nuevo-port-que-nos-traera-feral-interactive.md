---
author: Pato
category: Aventuras
date: 2018-03-14 11:12:52
excerpt: "<p>Por fin desvelamos el \xFAltimo OFNI en el radar</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ddbd51c6174319adf1ea9a6d30392812.webp
joomla_id: 678
joomla_url: life-is-strange-before-the-storm-es-el-nuevo-port-que-nos-traera-feral-interactive
layout: post
tags:
- aventura
- buena-trama
- episodios
title: "'Life is Strange: Before the Storm' es el nuevo port que nos traer\xE1 Feral\
  \ Interactive"
---
Por fin desvelamos el último OFNI en el radar

¡Hurra! Por fin tenemos la confirmación de que el juego que Feral Interactive está portando es un "nuevo" episodio de la sa 'Life is Strange'.


Según acaban de anunciar en su cuenta de twitter, 'Life is Strange: Before the Storm' llegará a nuestro sistema favorito en algún momento de esta primavera:



> 
> This spring, Life is Strange: Before the Storm comes to macOS and Linux. The Deluxe Edition will include all three episodes along with the bonus episode, “Farewell”. Hit the minisite to learn more about Arcadia Bay and its strong cast of characters – <https://t.co/G8iTTo0UkO> [pic.twitter.com/a5ldS1d6yT](https://t.co/a5ldS1d6yT)
> 
> 
> — Feral Interactive (@feralgames) [March 14, 2018](https://twitter.com/feralgames/status/973877425351352320?ref_src=twsrc%5Etfw)



 


*Before the Storm* es una historia de aventuras independiente que transcurre antes de *Life Is Strange*, el primer juego de la serie galardonada por BAFTA.


Juegas siendo Chloe Price, una rebelde de espíritu libre que forja una amistad inverosímil con Rachel Amber, una estudiante modélica. Cuando Rachel descubre un secreto familiar que amenaza con destruir su mundo, las dos amigas tendrán que enfrentarse juntas a sus demonios y encontrar la manera de sobreponerse a ellos.


*Life is Strange: Before the Storm - Edición Deluxe* para macOS y Linux incluirá los tres episodios completos junto con el episodio adicional “Adiós”.


Sobre los requisitos mínimos, aún no han sido publicados, por lo que estaremos atentos a cualquier información al respecto e informaremos llegado el caso.


Si quieres saber más puedes visitar [la miniweb del juego](http://www.feralinteractive.com/es/games/lisbeforethestorm/) que ya está disponible.


¿Qué te parece lo nuevo que nos trae Feral Interactive? ¿Has jugado ya a algún episodio de Life is Strange?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

