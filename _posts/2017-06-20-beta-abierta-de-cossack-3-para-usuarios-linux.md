---
author: Serjor
category: Estrategia
date: 2017-06-20 17:26:11
excerpt: "<p>Despu\xE9s de la beta cerrada, hoy han liberado una beta abierta para\
  \ los poseedores del juego.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/84bd8bd55711fc1be7650ea3945868c3.webp
joomla_id: 382
joomla_url: beta-abierta-de-cossack-3-para-usuarios-linux
layout: post
tags:
- rts
- cossacks-3
title: Beta abierta de Cossack 3 para usuarios Linux
---
Después de la beta cerrada, hoy han liberado una beta abierta para los poseedores del juego.

Nos enteramos gracias a [GOL](https://www.gamingonlinux.com/articles/cossacks-3-finally-in-open-beta-for-all-linux-users.9866) de que hoy Cossacks 3 ha salido de su beta cerrada para estar disponible en beta abierta a todos los usuarios de Linux.


Según [comentan](https://steamcommunity.com/games/333420/announcements/detail/2790391902845801813) los desarrolladores, en el estado actual del juego prefieren anunciar la beta pública ya que aunque es una versión cercana a la final, temen que haya algún fallo que arruine la experiencia y prefieren avisar y testear algo más que arruinar la experiencia de los jugadores.


Junto con la beta pública anuncian que los usuarios de Linux recibirán el pack de pre-compra, el cuál incluye una mini-campaña adicional, dos unidades exclusivas y una insignia para lucir en el modo multijugador, que además es multiplataforma, con lo que se podrá jugar entre sistemas diferentes desde el primer día.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/cqTYxwJ-rhk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/333420/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Y tú, ¿te apuntarás a la guerra con este Cossacks 3? Dínoslo en los comentarios, o en nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).


 

