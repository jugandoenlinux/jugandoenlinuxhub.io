---
author: Serjor
category: Software
date: 2017-07-26 04:00:00
excerpt: "<p>El soporte por ahora es para PC y para Android vendr\xE1 en versiones\
  \ posteriores</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cdbf366d9f51982d2973fefc5c0ec9b1.webp
joomla_id: 419
joomla_url: lanzada-el-cryengine-5-4-preview-con-soporte-para-vulkan-en-modo-beta
layout: post
tags:
- vulkan
- cryengine
title: Lanzada el CryEngine 5.4 (preview) con soporte para Vulkan en modo beta
---
El soporte por ahora es para PC y para Android vendrá en versiones posteriores

Leemos en [gamefromscratch](http://www.gamefromscratch.com/post/2017/07/25/CryTek-Release-CryEngine-54-Preview.aspx) que Crytek ha liberado la preview de lo que será la versión 5.4 de su motor CryEngine.


Esta nueva versión trae entre otras novedades el soporte para Vulkan, que en esta primera revisión sólo da soporte para PC aunque el soporte para Android no está lejos y lo liberarán en próximas versiones.


Es interesante ver cómo los grandes motores van incluyendo el soporte para esta ya no tan nueva API, y aunque el CryEngine ya no sea tan popular como antes, esperemos que los diferentes equipos desarrolladores que estuvieran trabajando con él se animen a probar a exportar sus juegos con soporte para Vulkan.


Os recordamos que el motor es de código abierto, aunque leyendo su licencia no me ha quedado claro que sea de código libre, y que compila para Linux, a pesar de que su editor no esté disponible en nuestra plataforma preferida. No obstante prometen que cuando liberen la versión 5.4 definitiva liberarán también el código fuente de su editor.


Si queréis ver las notas de la versión podéis encontrarlas [aquí](http://docs.cryengine.com/display/SDKDOC1/CRYENGINE+5.4.0+Preview).

