---
author: Son Link
category: Estrategia
date: 2021-10-25 21:38:16
excerpt: "<p>Nueva versi\xF3n de este gran cl\xE1sico que es <a href=\"https://twitter.com/wesnoth\"\
  \ target=\"_blank\" rel=\"noopener\"><span class=\"css-901oao css-16my406 r-poiln3\
  \ r-bcqeeo r-qvutc0\">@Wesnoth</span></a></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/wesnoth_1.16/wesnoth.webp
joomla_id: 1382
joomla_url: la-batalla-por-wesnoth-1-16
layout: post
tags:
- estrategia-por-turnos
- battle-for-wesnoth
- codigo-abierto
title: La Batalla por Wesnoth 1.16
---
Nueva versión de este gran clásico que es [@Wesnoth](https://twitter.com/wesnoth)


Ya esta aquí la nueva versión estable de este gran clásico (hablamos de un juego que empezó su andadura por el **2003**), la cual trae una larga lista de cambios, las cuales resumiré porque si no este articulo sería muy largo.


Una de las principales novedades es que el juego se actualizara automáticamente desde una versión anterior. Nada más arrancar la nueva versión nos saldrá un dialogo para exportar los datos, seleccionando la más actual que hayamos usado, ademas de poder eliminar cualquier complemento que tengamos instalado y que funcione con esta versión.


![migracion datos](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/wesnoth_1.16/migracion_datos.webp)


Otra novedad esta en la descarga de los complementos. La primera de ellas, y que es una gran noticia para aquellos con conexiones lentas, es que ahora solo se bajaran los archivos actualizados y no todo el paquete, reduciendo así tanto el tamaño como el tiempo de las descargas, algo que seguramente se notara en los más pesados, como campañas o packs de mapas. La otra es que ahora se podrá seleccionar que versión instalar cada vez que los desarrolladores suban actualizaciones, por ejemplo, para instalar una anterior si la más reciente falla.


La **IA también he recibido una gran actualización** y ahora podrá manejar mejor a los curanderos, envenenamiento, retirada, entre otros comportamientos.


En cuanto a las campañas, **Descenso a la oscuridad**, **Libertad** y **El cetro de fuego** han recibido muchas revisiones, simplificación del juego, añadidos elementos roleros, reescrita parte de la historia y de los diálogos, nuevos gráficos y equilibrar las distintas facciones. Para **Bajo los soles ardientes** se han renovado los mapas, ademas de que la facción **Quenoth**, que es la que controlaremos a lo largo de ello, ha sido reequilibrada para estar más ajustada al resto de facciones. Por otro lado, con Halloween a la vuelta de la esquina nos recomienda jugar esos días a las campañas **Descenso a la oscuridad** y **Los secretos de los ancestros**.


![en partida](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/wesnoth_1.16/en_partida.webp)


A lo largo del desarrollo se ha ido reajustando la IA para mantener la dificultad prevista y que no haya picos de dificultad inusuales, sobre todo en **La invasión del Este**. Las campañas de nivel novato han visto reducida su dificultad para hacerlas más accesibles a los jugadores más nuevos.  
 


Para la campaña **El Martillo de Thursagan** se han revisado varios escenarios y eliminados otros y así modernizar la historia, especialmente para enfocarse en la historia enana y la interacción entre clanes. Por ultimo **Los secretos de los ancestros** también ha recibido una actualización menor, con nuevas variedades de zombis para crear y reclutar.


Ademas de estos y otros cambios en las campañas se han añadido unos nuevos menús para las campañas con unos impresionantes fondos dibujados por **Emilien Rotival** (*LordBob*), **Stefan** y **Kathrin Polikeit** (*Kitty*).


![portada](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/wesnoth_1.16/seleccion_campana.webp)


El modo multijugador tambien trae muchas novedades. Para empezar ha habido dos adiciones importantes: **La isla de la niebla** y **Conquista mundial**. El primero es un nuevo escenario cooperativo de supervivencia en la misma linea que **Pronóstico oscuro**. Conquista mundial, que anteriormente era el complemento **World Conquest II**, es una campaña multijugador con elementos RPG donde hasta tres jugadores pueden cooperar para sobrevivir y mejorar sus ejércitos para derrotar a los ejércitos que se les opongan.


Otro complemento que pasa a formar parte del juego es **Planifica promoción de unidad**. Cuando este complemento esta activado permitirá a los jugadores elegir previamente a que unidades evolucionan sus tropas cuando suben de nivel durante el turno de otro. Aquellas unidades para las que no se haya planificado simplemente subirán de nivel y no se escogerá una evolución al azar.


También se han realizado varios ajustes, sobre todo en las facciones de la era predeterminada, especialmente en el coste en oro y puntos de vida de las unidades clave, por lo que ahora ciertos enfrentamientos y sus unidades deberán de estar más equilibradas y permitir una mayor diversidad en el juego.


![portada](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/wesnoth_1.16/portada.webp)


También se han implementado mejoras en la gestión de las partidas para los anfitriones. La primera es que ahora las exclusiones de usuarios se hace a través del nombre y no de su dirección IP, los jugadores que están en la lista de excluidos automáticamente no podrán unirse a la partida y los que vayan a entrar como espectadores también tendrán que introducir la contraseña.


Estos son algunos de los cambios, pero hay muchos más, los cuales puedes ver en [la noticia oficial](https://www.wesnoth.org/start/1.16/) así como el listado completo [en el foro](https://forums.wesnoth.org/viewtopic.php?t=55012).


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/4Ebww6utt9I" title="YouTube video player" width="780"></iframe></div>


Que lo disfrutéis, y ya sabéis, **si queréis organiza alguna partida, pasaros por nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).**


 

