---
author: leillo1975
category: Carreras
date: 2023-06-28 13:26:24
excerpt: "<p>As\xED lo acaba de anunciar su creador, @ansdor en sus redes sociales.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SleepStream/SlipStream_BlueHour.webp
joomla_id: 1538
joomla_url: la-dlc-gratuita-de-slipstream-blue-hour-llegara-pronto
layout: post
tags:
- retro
- racing
- slipstream
- ansdor
title: "La DLC gratuita de Slipstream, Blue Hour, ya est\xE1 disponible! (ACTUALIZACI\xD3\
  N)"
---
Así lo acaba de anunciar su creador, @ansdor en sus redes sociales.


 Tal y como os habíamos comentado hace más o menos un mes, esta expansión gratuita finalmente ya se puede disfrutar por parte de todos los poseedores de este genial juego, tal y como acabamos de ver en este tweet de su desarrollador [Ansdor](https://www.ansdor.com/):



> 
> Blue Hour, a free DLC expansion for Slipstream, is now available! <https://t.co/zQUJbRtAzU> [pic.twitter.com/diu8SEYMlZ](https://t.co/diu8SEYMlZ)
> 
> 
> — sandro (@ansdor) [June 28, 2023](https://twitter.com/ansdor/status/1674040445167443969?ref_src=twsrc%5Etfw)



  
  
Para todos aquellos que no dispongáis del juego, estáis ante una magnifica oportunidad de llevaros una auténtica joya retro, cargada de contenido y mejoras, que no ha parado de llegar a lo largo de estos cinco años desde que se lanzó oficialmente el juego. Os recordamos que podeis comprar Slipstream en [Steam](https://store.steampowered.com/app/732810/Slipstream/), [Itch.io](https://ansdor.itch.io/slipstream) y [GOG.com](https://www.gog.com/en/game/slipstream) .


 




---


**NOTICIA ORIGINAL 23-5-23:**  
Acabamos de leer la noticia en la cuenta de twitter de este desarrollador brasileño, [Ansdor](https://www.ansdor.com/),  que con motivo del 5º aniversario del estreno de su juego, **lanzará este mismo verano, lo más probable a finales de Junio, una expansión gratuita** para su fantástico juego Slipstream, con lo que amplía aun más la jugabilidad de este juego de carreras de coches al más puro estilo de los arcades de los 80. Podeis ver el tweet aquí:  
  




> 
> The blue hour is a brief period of the day, between sunset and nightfall, when the sky takes on a distinctive deep blue shade.  
>   
> Blue Hour, a FREE DLC expansion, is coming to Slipstream this summer. [#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/qDozWvzSUT](https://t.co/qDozWvzSUT)
> 
> 
> — sandro (@ansdor) [May 23, 2023](https://twitter.com/ansdor/status/1660993989238198272?ref_src=twsrc%5Etfw)



 


En [Blue Hour](https://store.steampowered.com/app/2334980/Slipstream_Blue_Hour/) podremos conducir al anochecer, con efectos de iluminación en bellos escenarios. Estas son las novedades que encontraremos en esta DLC del juego:


* 3 nuevos coches para escoger basados en modelos conocidos (Wellenreiter, Spectre y Wildfire)
* 2 nuevos modos de juego: Blue Hour Cup y Random Grand Prix
* 5 nuevas canciones compuestas por Effoharkay, el compositor de la banda sonora del juego original.
* 5 nuevos escenarios: Marsella, Pompeya, **Andalucía** (seeee) y Edinburgo.


El último punto de estas novedades, los escenarios, es gracias a la actualización (1.3) que acaba de ser estrenada hoy mismo y que permite mejoras sustanciales en el motor gráfico del juego, y representan a la perfección su evolución. Además de estas mejoras gráficas esta nueva versión da una nueva vuelta de tuerca a como el juego gestiona la dificultad de la IA, el rebanlanceo de los atributos de los coches, los nuevos modos "random" basados en los actuales, o las mejoras en el sistema de mods. Podeis consultar todos estos cambios en la siguiente [noticia de Steam](https://store.steampowered.com/news/app/732810/view/3695811362844172401?l=spanish).


Para quien no conozca el juego , en esta web hemos hablado largo y tendido sobre [Slipstream]({{ "/tags/slipstream" | absolute_url }}), e incluso escribimos en su lanzamiento, allá por el 2018, un completo [análisis]({{ "/posts/analisis-slipstream" | absolute_url }}). Por ahora esta es toda la información de la que disponemos, pero si averiguamos algo más lo iremos ampliando en esta misma noticia. Os dejamos con el video del anuncio de esta DLC:  
  



<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/5lT9P_yjUjA" title="YouTube video player" width="780"></iframe></div>


 


Podeis haceros con Slipstream en [Steam](https://store.steampowered.com/app/732810/Slipstream/), [Itch.io](https://ansdor.itch.io/slipstream) y [GOG.com](https://www.gog.com/en/game/slipstream). Si quieres comentar o preguntar algo acerca de este juego puedes hacerlo en la [Zona Racing](https://matrix.to/#/#zona_racing-jugandoenlinux:matrix.org) de nuestras salas de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro [grupo de Telegram](https://t.me/jugandoenlinux).


 

