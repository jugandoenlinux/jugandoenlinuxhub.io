---
author: Pato
category: Noticia
date: 2018-04-11 10:42:36
excerpt: "<p>Hasta ahora era posible ver los juegos de los usuarios con perfiles p\xFA\
  blicos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f77d553c52fa954c4cb29f5292f015ef.webp
joomla_id: 709
joomla_url: steam-cambia-la-privacidad-de-los-usuarios-por-defecto
layout: post
tags:
- steam
- valve
- privacidad
title: Steam cambia la privacidad de los usuarios por defecto
---
Hasta ahora era posible ver los juegos de los usuarios con perfiles públicos

El tema de la privacidad está candente desde hace unas semanas. Tras los últimos escándalos y problemas derivados de las prácticas de ciertas empresas, las leyes sobre privacidad empiezan a tomar relevancia, y la presión de los usuarios empieza a hacerse notar. Muchas empresas e instituciones comienzan a mover ficha ante estos movimientos, por ejemplo con la nueva ley europea de protección de datos para adaptarse al nuevo marco legal, y parece que Valve ha decidido poner de su parte también.


La cuestión es que hasta ahora era posible ver los juegos que poseían los usuarios que tuvieran su perfil público, cosa que al parecer en Steam venía por defecto. Sin embargo ahora esto ya no será así, y todos los perfiles públicos dejarán de compartir esta información, dando mas control sobre lo que los usuarios de Steam dejan que otros puedan ver.


Ahora podemos elegir quien puede ver nuestros detalles de juegos, y podemos configurar cómo queremos que nos vean nuestros amigos, o la comunidad de Steam, incluyendo qué información de nuestro perfil está incluida en cada categoría como la lista de juegos que has comprado, la lista de tus deseados así como los logros o el tiempo de juego.


Independientemente de la configuración de los detalles de los juegos en nuestro perfil también tenemos la opción de mantener en privado el tiempo total de juego.


Además, Valve afirma que están trabajando en un modo "invisible" que se añadirá a los estados ya existentes de "conectado", "ausente" y "desconectado", de modo que podremos aparecer como desconectados para los demás mientras que seguiremos viendo nuestra lista de amigos y enviar y recibir mensajes.


Sin embargo, no todo son "buenas noticias", o si, según se mire pues a partir de ahora, servicios como [SteamSpy](https://steamspy.com/) que se nutría de recolectar datos de los perfiles públicos de los usuarios para obtener estadísticas de juegos y uso de Steam está en grave riesgo de desaparecer, ya que ahora deja de recibir información que hasta ahora era pública. Así lo reconocían en un tweet:



> 
> Valve just made a change to their privacy settings, making games owned by Steam users hidden by default.   
>   
> Steam Spy relied on this information being visible by default and won't be able to operate anymore.<https://t.co/0ejZgRQ6Kd>
> 
> 
> — Steam Spy (@Steam_Spy) [11 de abril de 2018](https://twitter.com/Steam_Spy/status/983879694658437120?ref_src=twsrc%5Etfw)



 ¿Que te parecen las nuevas medidas de privacidad de Steam?


Tienes toda la información en el post original de Valve en el blog de Steam [en este enlace](https://steamcommunity.com/games/593110/announcements/detail/1667896941884942467).

