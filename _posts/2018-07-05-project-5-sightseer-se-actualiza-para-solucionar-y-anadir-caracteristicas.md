---
author: Pato
category: "Exploraci\xF3n"
date: 2018-07-05 09:29:32
excerpt: "<p>Entre otras cosas, solucionan el bug del agua s\xF3lida</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b184fcc64e2c630bc6793d8e1c9e3f5e.webp
joomla_id: 797
joomla_url: project-5-sightseer-se-actualiza-para-solucionar-y-anadir-caracteristicas
layout: post
tags:
- accion
- indie
- acceso-anticipado
- exploracion
title: "'Project 5: Sightseer' se actualiza para solucionar y a\xF1adir caracter\xED\
  sticas"
---
Entre otras cosas, solucionan el bug del agua sólida

Volvemos con [linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/project-5-sightseer-broken-water-fix-linux-67978/) y el anuncio de que 'Project 5: Sightseer' ha recibido una actualización que trae novedades para la versión Linux del juego. Así lo anuncian los desarrolladores en un [post oficial](https://steamcommunity.com/games/655780/announcements/detail/1692673718809780197) en Steam donde explican que  *esta actualización trae el soporte completo para Linux (no más agua rota!)*


Además de esto, ahora tendremos encuentros con NPCs en nuestros viajes de forma aleatoria, y una actualización que hace mas agresivos a las facciones que encontremos durante nuestros raids.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/9-7HI5bxmTo" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


*'Project 5: Sightseer' es un juego cooperativo sandbox multijugador ambientado en un vasto mundo generado por procedimientos. Comenzarás solo con un escáner y un láser de minería en la ubicación que elijas, aventúrate a buscar recursos, construir puestos de avanzada, tecnologías de investigación y, finalmente, descubrir los secretos del mundo.*


Si quieres saber más detalles, tienes Project 5: Sightseer [en Steam](https://store.steampowered.com/app/655780/Project_5_Sightseer/), aunque no está traducido al español y aún está en fase de acceso anticipado.


Si quieres ver la lista completa de cambios puedes verla [en este enlace](tinyurl.com/p5notes).

