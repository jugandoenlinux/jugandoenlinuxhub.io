---
author: leillo1975
category: Noticia
date: 2018-01-09 08:56:03
excerpt: <p>Nos hacemos eco de la entrevista en Boiling Steam.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/85ca514f9886a96d3c067f991afcb736.webp
joomla_id: 597
joomla_url: segun-tim-basset-el-port-de-street-fighter-v-no-esta-muerto
layout: post
tags:
- steam
- podcast
- street-fighter-v
- tim-basset
- id-software
title: "Seg\xFAn Tim Besset \"El port de Street Fighter V no est\xE1 muerto\""
---
Nos hacemos eco de la entrevista en Boiling Steam.

Nuestro grupo de [Telegram](https://t.me/jugandoenlinux) es una fuente impresionante de información, pues gracias a los enlaces que a menudo nos reportan sus usuarios estamos al tanto de muchas noticias. Ayer, gracias a  **@OdinTdh**, nos enteramos que en el magnifico Blog [Boiling Steam](https://boilingsteam.com) se publicaba una jugosa entrevista donde [Ekianjo](https://twitter.com/BoilingSteam) conversaba con [Tim Besset](https://twitter.com/TTimo), ex de Id Software y responsable entre otros de gran parte del port a Linux/SteamOS de Rocket League junto a Ryan Gordon. La entrevista en cuestión realmente es un **podcast**, pero está transcrita en su integridad y en ella Tim nos habla de su andadura profesional en relación con los juegos en Linux.  Es muy extensa y está escrita en Inglés, pero con un poco de paciencia se lee fácilmente pues resulta tremendamente interesante su contenido. Tenéis el enlace aquí debajo:


### [Podcast #9 With Tim Besset: “Street Fighter V’s Port is Not Dead”](https://boilingsteam.com/podcast-9-with-tim-besset-street-fighter-vs-port-is-not-dead/)


El titular *"El port de Street Fighter V no está muerto"* quizás sea lo más llamativo, y es esperanzador leerlo. Tim Besset compara la situación de Street Fighter V con la que tenía en su día Rocket League, que aunque parecía que no iba nunca a llegar, finalmente si lo hizo. Esperemos que esté en lo cierto. Pero para ser justos, realmente lo mejor de este podcast es que habla de un montón de cosas interesantes relacionadas con el pasado y presente de los juegos en Linux, obviamente desde el punto de vista de alguien tan relevante como él.


En ella se habla sobre su **etapa en IDSoftware**, donde él era el encargado de portar sus juegos a Linux, entre otras cosas; sobre lo que piensa sobre otras compañías como Croteam; las diferencias entre trabajar con OpenGL, DirectX y Vulkan, del que espera que sea un puntal multiplataforma en un futuro cercano; o su opinión al respecto de  SteamOS y las Steam Machines, de las que dice que no están muertas ni mucho menos y que Valve es consciente que es un proyecto de largo recorrido. Habla también del desarrollo de drivers, la realidad virtual en Linux, o sobre **su trabajo para Valve** como freelance, donde se encarga entre otras cosas de "promocionar" los ports a Linux/SteamOS junto a Pierre Loup, y donde explica que parte de su trabajo es tratar de conseguir que las grandes editoras y estudios traigan sus títulos a Linux/SteamOS porque según sus palabras *"aunque es más dificil conseguir un port de esas compañías [...] el impacto en una plataforma es significativamente mucho más grande. Valve está en una posición donde puede tener un impacto ahí, por tanto es donde estamos tratando de enfocarnos"*... como veis, completito. Estos son solo unos pocos temas de los que trata la entrevista, por lo que lo mejor es que entreis en el enlace de arriba y la leais.


Si sois seguidores de [Boiling Steam](https://boilingsteam.com/), sabréis que en el pasado se hizo una entrevista semejante a [Ryan Gordon](https://twitter.com/icculus) dividida en dos partes ([primera](https://boilingsteam.com/icculus-ryan-gordon-tells-us-everything-part-1/) y [segunda](https://boilingsteam.com/icculus-ryan-gordon-tells-us-everything-part-2/)), la cual desde aquí también os recomendamos que leais.


¿Qué os ha parecido la entrevista? Déjanos tu opinión en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux)

