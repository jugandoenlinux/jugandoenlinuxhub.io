---
author: Jugando en Linux
category: Editorial
date: 2021-05-08 13:49:48
excerpt: "<p>Aunque no, no tiene nada que ver con Street Fighter.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/headers/Proyecto logo.webp
joomla_id: 1303
joomla_url: a-new-challenger-has-entered-the-ring
layout: post
title: A new challenger has entered the ring
---
Aunque no, no tiene nada que ver con Street Fighter.


Según la RAE, una de las acepciones de ciclo es "Período de tiempo que incluye una serie de fenómenos característicos", y todo periodo comienza y finaliza.


En mi caso un fenómeno característico sería la de colaborar en jugando en linux, y como decía, los periodos comienzan, pero también terminan. La verdad es que no hay un gran motivo para no continuar, pero tampoco lo hay para hacerlo. Pato y Leillo han sido dos compañeros de viaje para quienes no tengo palabras para haceros entender la calidad de personas que son, y he estado posponiendo esta decisión en parte por ellos, pero la realidad es que ahora mismo los videojuegos y yo no estamos en la misma página, y en mi día a día cada vez tienen menos cabida. Juego, pero muy poco, prácticamente no sigo la actualidad videojueguil, y por lo tanto tampoco tengo mucho interés en escribir sobre ella.


No obstante, y afortunadamente para la comunidad y para JEL, que yo deje de ser administrador y editor no significa que la carga de trabajo vaya a recaer única y exclusivamente en Pato y Leo, sino que llega un refuerzo que ha formado parte de la comunidad desde el principio prácticamente (o al menos para mí me parece que ha estado siempre ahí), y que además es muy activo en [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux), pero creo que mejor sea él quién se presente. Yo me despido ya de este lado de la web, deseándole a Fran lo mejor en su nuevo ciclo como administrador y editor.




---


Fran, alias P_Vader para los amigos, asiduo y conocido por estos lares. Por si aun no sabes quien soy me presento:


Mi primer Linux llego en el 98 experimentando con Red hat, Suse, Mandrake o Debian. Allá por el 2004 instalé la llamativa distro  Ubuntu y poco después con la llegada de Kubuntu se quedó como mi sistema por defecto hasta hoy.


Mi andadura como jugón empezó desde que tengo uso de razón. Me crié en la época de los 8 bits entre Ataris y Spectrums que también me valieron para aprender a programar. Después llegó el PC y nunca lo abandoné, ni por las consolas. Ese juego que me viene a la mente cuando pienso en el mejor es Ghosts 'n Goblins. ¡Lo puedo acabar con los ojos cerrados!


Sin más espero que en esta nueva etapa con JEL os trasmita mi pasión por el software libre y los videojuegos.

