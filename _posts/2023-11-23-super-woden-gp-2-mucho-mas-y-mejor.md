---
author: leillo1975
category: "Análisis"
date: 10-11-2023 16:56:11
excerpt: "Por fin podemos disfrutar de la tan ansiada segunda parte del juego de @ViJuDaGD"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP2/SWGP_JEL.webp
joomla_id: 1544
joomla_url: super-woden-gp-2-tendra-correra-nativamente-en-linux
layout: post
tags:
- analisis
- nativos
- vijuda
- retro
- super-woden-gp-2
- unity3d
title: "Super Woden GP 2, mucho más y mejor"
---

Hace un par de años nos llevábamos una sorpresa mayúscula con la llegada de [Super Woden GP]({{ "/posts/analisis-super-woden-gp" | absolute_url }}), un juego del vigués [**Vi**ctor **Ju**sto **Da**cruz](https://twitter.com/ViJuDaGD), que tuvo una muy buena acogida entre el publico en general, y especialmente los que nos gustan los juegos de coches. SWGP era un canto a la nostalgia y un **homenaje a juegos como World Rally Championship o el primer Gran Turismo**, y consiguió encandilarnos a todos con esa **soberbia jugabilidad y estilo Retro**.

Pero parece que a su desarrollador se le quedaron cosas en el tintero y quiso dar otra vuelta de tuerca más en esta secuela, manteniendo obviamente muchos de los aspectos que encumbraron al juego anterior, pero añadiendo montones de novedades de lo más interesantes. Y es en eso en lo que vamos a incidir en este artículo, a medio camino entre noticia y pequeño análisis.

Como sabéis hace unas semanas [os anunciamos aquí mismo]({{ "/posts/super-woden-gp-2-tendra-correra-nativamente-en-linux" | absolute_url }}) que el juego adelantaba su lanzamiento, algo tan poco común como agradable en este mundillo de los videojuegos. Y aquí estamos puntuales celebrando su llegada para que os podáis hacer una idea lo más aproximada posible de lo que os vais a encontrar en este título nativo. En referencia a esto último, **el creador de SWGP2, ademas de querer brindarnos soporte a los que usamos GNU/Linux, ha querido que su creación funcione perfectisimamente en la Steam Deck**, algo que seguramente muchos de vosotros sabréis valorar. Y es que este tipo de título y su jugabilidad parece adaptarse como un guante a los dispositivos portátiles.

![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP2/swgp2-menu.webp)

Nada más arrancar el juego nos vamos a encontrar con un mensaje donde podréis ver los últimos cambios que se han realizado, ya que **la intención de ViJuDa es continuar sumando cosas después del lanzamiento, algo que otorga un valor añadido al juego**. Inmediatamente veremos una pantalla con la portada de SWGP2 que recuerda a las recreativas, y tras pulsar "Start" aparecerá el **nuevo menú del juego**, al estilo de los primeros Gran Turismo, como el anterior, pero **más acorde con la estética actual del juego, con mucha más calidad, y más elementos**, y es que ya aquí empezaremos a encontrar las novedades. Pero primero vamos a hablar de los menúes, que están claramente más trabajados que en el primero, pues a parte de tener un diseño más atractivo los noto **mejor ordenados y fáciles de usar**.

Tal y como os acabo de comentar, hay nuevos elementos en el menú, y sin duda llama la atención la llegada del **modo Arcade**, donde jugaremos diversas **etapas de Rally al más puro estilo de los 90**. Para más inmersión incluso **se han pixelado los gráficos**, llevándonos el recuerdo de aquellos atestados salones recreativos de antaño. Tendremos un numero determinado de créditos para intentar llegar al final de la última etapa (algo que este manco que aquí escribe aun tiene pendiente). Estas transcurrirán en países diferentes (Kenia, España, Suecia...) y por supuesto con diferentes tipos de pistas (asfalto, tierra, nieve...) y meteorología variada. Existen **dos rutas diferentes que se diferencian por su dificultad, y está pendiente una tercera** que se añadirá más adelante. Por supuesto, y **como en todas las carreras del juego, nuestros tiempos quedarán registrados y podremos compararlos con jugadores de todo el mundo, o entre nuestros amigos.**

![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP2/swgp2-arcade.webp)

En el primer SWGP, teníamos 6 **concesionarios** diferentes -Alemania (Cinder), Reino Unido (Nanwolf), USA (Soop), Japón (Raven), Francia (Mylene) y Italia (AAlia)- donde podíamos comprar los coches del juego. En esta ocasión **se añaden 3 más** como son De Castro, Enhörning y Zaphir (España, Suecia y Argentina).Y es que en el juego podremos encontrar **más de 180 coches**, algo que contrasta con los 70 del juego anterior, que ya no era una cifra para nada desdeñable. Una vez dentro de cada concesionario podremos ver la lista de todos los coches de este, y al seleccionarlos el detalle de cada uno  de ellos, viendo sus características en plan showroom. Por supuesto, al igual que en SWGP1, **encontraremos coches icónicos excelentemente modelados que forman parte de la historia del automovilismo**, encontrando vehículos de calle, Rally, GTs, Touring Cars e incluso camiones. Al igual que otros juegos de coches, el coleccionismo forma parte importante del enganche del juego.

Un puntazo es la inclusión del taller, donde a parte de poder cambiar el aceite a nuestros vehículos, para evitar la pérdida de rendimiento, **podremos mejorarlos con piezas que incrementarán su rendimiento**. Esta característica será muy apreciada especialmente cuando se nos atasque una prueba o le tengamos especial cariño a correr con un coche. Este es obviamente uno de los apartados de los que Gran Turismo es el referente. **Podremos mejorar motor, caja de cambios, neumáticos, chasis frenos y suspensión**. Al igual que en el primero también podremos lavarlos para que estén siempre perfectos y relucientes.

![swgp2 mejoras](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP2/swgp2-mejoras.webp)

A nivel gráfico hay que decir que se ve que la experiencia es un grado, pues **el modelado de los vehículos es mucho mejor que en primero**, siendo también las texturas de mayor calidad. Vemos cambios en los **escenarios, 34 en total entre pistas urbanas, tramos de rally y circuitos**, percibiéndose ahora mucho más detallados, y con una novedad más que importante, ya que ahora estos disponen de **cambios de elevación** lo que nos permite disfrutar de cambios de rasante o peraltes. Esto nos hará disfrutar de **espectaculares saltos en las etapas de rally o inclinaciones en las curvas de los ovales**. En cuanto a lo primero es un elemento más de dificultad, pues tendremos que tener el coche bien posicionado antes del salto para no "irnos a las patatas". En el caso del segundo nos permitirá tomar curvas a una mayor velocidad. También hay que decir que encontraremos bastantes pistas conocidas como pueden ser Suzuka, Brands Hatch o Laguna Seca con nombres alternativos, lo cual es un guiño más que genial a los aficionados al automovilismo.

Mantiene al igual que el primero los **diferentes estilos de visualización, donde podremos no aplicar ninguno o elegir entre GP, Pencil, Arcade, Amiga y Carbon** según sean nuestros gustos. También se ha dotado de una **cámara dinámica** que se alejará o acercará según la velocidad a la que vayamos, lo cual facilita mucho la conducción. Por supuesto también podremos ponerla a nuestro gusto.

La IA ha sido  también retocada para ser mejor en todos los aspectos. **Veremos incluso roces y accidentes entre ellos**, pero según palabras propias del desarrollador, la inteligencia artificial en los juegos arcade tiene que ser un acompañamiento del juego, no una competencia, por lo que **lo más importante es que sea predecible y no haga cosas extrañas** que puedan llevar al traste una estupenda carrera. En cuanto al nivel de dificultad (fácil y normal) está muy bien implementado y será progresivo a medida que vayamos avanzando en el juego. Algo que no se en que apartado encajar pero que me ha llamado mucho la atención es que ahora hay también **salidas lanzadas**, otro detalle con lo que seguro que el desarrollador se ha tenido que dejar unas cuantas horas, pero que es un punto más por parte del juego de abarcar todo lo que es motorsport.

![swgp2 eventos](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP2/swgp2-eventos.webp)

Sobre las **competiciones** que encontraremos, tendremos **Fines de Semana, Campeonatos, Resistencia o Rally**, y en ellos ganaremos además de los imprescindibles créditos que necesitaremos para adquirir los coches y sus mejoras, estrellas que desbloquearán nuevas competiciones. Es habitual también conseguir **vehículos exclusivos al terminar determinadas pruebas en primera posición**. También hay que añadir los **eventos de invitación** donde cada cierto tiempo encontraremos 3 pruebas diferentes en las que medirnos con otros jugadores y ver que posición tenemos en el ranking mundial.

El **apartado multijugador** es semejante al primer juego, permitiéndolo localmente a pantalla partida, algo que junto con [Remote Play Together](https://store.steampowered.com/remoteplay?l=spanish) nos dejará disfrutar del juego hasta con 4 de nuestros amigos a través de internet. Aunque hay que decir que el desarrollador nos ha comentado que **si la recepción del juego es buena, intentará desarrollar un modo online** que permita a cada jugador competir con otros, cada uno desde su PC y a pantalla completa, lo cual sería un addon perfecto para disfrutar de SWGP2 con otros poseedores del juego.

En cuanto a la **respuesta de los periféricos de control**, la mejor noticia es que no hay noticia. Todo funciona correctamente y **no hemos encontrado problemas** ni con las teclas, ni input-lag, ni nada por el estilo en los dispositivos que hemos probado, que en nuestro caso fueron el Steam Controler, Dual Shock PS3, XBOX 360 y Pro-Controller de Switch. Sobre la **jugabilidad**, como os decíamos al principio es **soberbia** y **los coches hacen exactamente lo que tienen que hacer en todo momento, con unas físicas mas que creibles para un juego arcade**. Al igual que en la primera parte es una **verdadera delicia conseguir realizar esos derrapes perfectos e infinitos** que tanto caracterizan a la saga (porque ya podemos decir SAGA, verdad?).

![swgp2 multi](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP2/swgp2-multi.webp)

El sonido en el juego no desentona para nada con el estilo retro de SWGP2. En cuanto a los efectos sonoros son todo lo que se espera de ellos, y **la banda sonora brilla como acompañante perfecto de la experiencia de juego**. Encontraremos remasterizadas algunas de las canciones del primer SWGP, y nuevas **composiciones de DJ Devito** (sobrenombre de ViJuDa) **y [DonutDroid](https://donutdroid.com/)**, que se adaptarán como un guante a nuestras sesiones de juego. También podremos deleitarnos escuchándolas en la sala de música desde el menú principal.

Como reza el título de este artículo, Super Woden GP 2 es mucho más y mejor, y **satisfará tanto a los que han disfrutado de la primera parte, como todos los nuevos jugadores** que seguro que va a tener, y que encontrarán un juego de carreras de coches con una **jugabilidad inusitada y vistosa** al estilo "viejuner" que tantos de nosotros apreciamos. Ahora solo os queda sacarle todo su jugo, que es mucho, y quien sabe si más adelante una tercera parte?

![swgp2 galicia](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP2/swgp2-galicia.webp)

Desde JugandoEnLinux.com queremos agradecer a Vijuda que otra vez haya confiado en nosotros para el testeo de la versión nativa de Linux, y el poder disfrutar de su juego durante el periodo de desarrollo.También encomendaros a que sigais nuestra redes sociales de [Mastodon](https://mastodon.social/@jugandoenlinux) y [Twitter](https://twitter.com/JugandoenLinux), pues más pronto que tarde realizaremos un gameplay en directo donde podreis verlo más de cerca y entender todo lo que aquí hemos detallado.

**Podeis comprar Super Woden GP 2 en Steam a 12.95€**, un precio de derribo teniendo en cuenta todo lo que ofrece, y **si lo comprais la primera semana os beneficiareis de un 35%** de descuento adicional para celebrar su lanzamiento, quedándose el juego en poco más de 8€.

<div class="steam-iframe">
	<iframe src="https://store.steampowered.com/widget/2083210/" width="646" height="190"></iframe>
</div>

---

### Actualización

Aquí os dejamos el vídeo del directo que hicimos del juego. Que lo disfrutéis:

<div class="resp-iframe">
	<iframe width="560" height="315" src="https://www.youtube.com/embed/MTRHpf74lHM?si=zQTJnQaO7E82soDT" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>