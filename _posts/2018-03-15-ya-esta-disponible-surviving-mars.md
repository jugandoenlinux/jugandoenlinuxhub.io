---
author: leillo1975
category: Estrategia
date: 2018-03-15 15:34:23
excerpt: "<p>El juego viene con soporte para Linux desde el primer d\xEDa.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bfea3555ad38fe476532c5b54f218c09.webp
joomla_id: 681
joomla_url: ya-esta-disponible-surviving-mars
layout: post
tags:
- paradox
- haenimont-games
- marte
title: "Ya est\xE1 disponible \"Surviving Mars\""
---
El juego viene con soporte para Linux desde el primer día.

Tal y [como lo prometieron](index.php/homepage/generos/estrategia/item/755-surviving-mars-llegara-el-proximo-15-de-marzo-y-lanza-un-nuevo-trailer), ya podemos adquirir el ultimo trabajo de los desarrolladores búlgaros [Haenimont Games](https://www.haemimontgames.com/), conocidos por ser los responsables de juegos tan conocidos en nuestra plataforma como **Victor Vran** y **Tropico 5**. El juego está editado por [Paradox Interactive](https://www.paradoxplaza.com/), y en él deberemos poblar marte con nuestras misiones de colonización. Aquí tenemos el tweet donde anuncian el estreno del juego:


 


El objetivo principal del juego será la construcción de una colonia sostenible en Marte, donde cada colono tendrá unas caracteristicas y comportamiento independiente, por lo que será más util en unas areas que en otras, o más conflictivo con otros colonos, por poner ejemplos. Para poder sobrevivir necesitarás crear cúpulas para aislar a tus habitantes de la atmosfera marciana, y diferentes estructuras para obtener servicios básicos (y no tan básicos). Podrás explorar el planeta y descubir sus secretos para conseguir beneficios o todo lo contrario.... El juego contará además con un arbol de investigación aleatorio y se podrán usar mods. Como veis suena todo de lo más interesante. Esta es la descripción oficial del juego:



> 
> **Coloniza Marte y descubre tus secretos sufriendo el menor número de bajas posibles.**
> 
> 
> ¡Bienvenido a casa! ¡Ha llegado la hora de tomar posesión del Planeta Rojo y construir las primeras colonias humanas activas en Marte! Lo único que necesitas son provisiones, oxígeno, décadas de formación, experiencia con tormentas de arena y una actitud proactiva para descubrir la finalidad de esos extraños cubos negros que han aparecido de la nada. ¡Con unos cuantos arreglos, este lugar va a ser fantástico!   
>    
> Surviving Mars es un constructor de ciudades de ciencia ficción que trata de colonizar Marte y sobrevivir al intento. Elige una agencia espacial para obtener recursos y apoyo financiero antes de decidir la ubicación de tu colonia. Construye cúpulas e infraestructuras, investiga nuevas posibilidades y utiliza drones para desbloquear maneras más elaboradas de modelar y ampliar tu asentamiento. Cultiva tus propios alimentos, extrae minerales o relájate en el bar después de una larga jornada de trabajo. Pero lo más importante es mantener con vida a tus colonos, una tarea harto compleja en este nuevo y extraño planeta.  
>    
> Encontrarás desafíos que deberás superar. Ejecuta tu estrategia y mejora las probabilidades de supervivencia de tu colonia mientras desvelas los misterios de este mundo alienígena. ¿Preparado? Marte te está esperando. 
> 
> 
> 


 


Los **requisitos técnicos** del juego son bastante accesibles y permiten usar gráficas AMD . Podeis consultarlos justo aquí debajo:


***SO:** Ubuntu 14 x64 or newer*
***Procesador:** 4th Generation Intel i3 CPU or equivalent*
***Memoria:** 4 GB de RAM*
***Gráficos:** OpenGL 4.5 (GeForce 600/AMD Radeon 7700 or higher) with 1GB of video RAM*
***Almacenamiento:** 6 GB de espacio disponible*
 
Este es el trailer de prelanzamiento para que vayais abriendo boca:
<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/7Js-9zMQ1oQ" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Podemos comprar Surviving Mars en [GOG.com](https://www.gog.com/game/surviving_mars) y Steam:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/464920/101987/" width="646"></iframe></div>


  En JugandoEnLinux.com intentaremos, si la editora nos lo facilita, realizar algún video y análisis de este juego  para que podais haceros una mejor idea de lo que podeis encontrar en este titulo. En caso afirmativo actualizaremos este artículo con la nueva información.


 


¿Estás deseando colonizar Marte? ¿Qué opinas de la idea principal del juego? Dinos tu opinión en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

