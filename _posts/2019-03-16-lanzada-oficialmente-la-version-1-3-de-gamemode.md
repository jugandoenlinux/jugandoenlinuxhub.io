---
author: Serjor
category: Software
date: 2019-03-16 10:00:17
excerpt: "<p>Peque\xF1os cambios que hacen avanzar un poco m\xE1s esta interesante\
  \ aplicaci\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a6e4d9622a775d7eed800918154a7577.webp
joomla_id: 997
joomla_url: lanzada-oficialmente-la-version-1-3-de-gamemode
layout: post
tags:
- feral-interactive
- gamemode
- overclock
title: "Lanzada oficialmente la versi\xF3n 1.3 de GameMode"
---
Pequeños cambios que hacen avanzar un poco más esta interesante aplicación

Ya os [avanzábamos](index.php/homepage/generos/software/12-software/1088-gamemode-permitira-configurar-el-overclock-de-graficas) que la nueva versión de GameMode, la utilidad de Feral Interactive que busca exprimir al máximo el rendimiento de nuestro ordenador, traería unos interesantes cambios, como el poner a nuestras gráficas a trabajar al máximo de su capacidad.


Pues bien, la [versión 1.3](https://github.com/FeralInteractive/gamemode/releases/tag/1.3) de esta utilidad ya ha sido publicada, con los siguientes cambios:


* Se deshabilita el salvapantallas cuando el juego está ejecutándose, lo cuál es interesante para juegos que no lo hacen por sí mismos, y que al jugar con mando el sistema no considere que haya interacción del usuario.
* Un script para configurar de manera automática todo lo necesario en aquellos casos que los juegos no soporten GameMode de manera nativa, así que ahora con poner en la opciones de lanzamiento `gamemoderun %command%` en los juegos de Steam o ejecutar un juego con `gamemoderun ./game`, ya es suficiente.
* Overclock para las gráficas NVIDIA y configuración del modo de trabajo en las gráficas AMD. En este punto, advierten que en ambos casos la funcionalidad está en fase experimental, por lo que está deshabilitada por defecto, y recomiendan que se use con cuidado.
* Se aumenta la prioridad de lectura y escritura en los procesos del juego.
* Ya no se configuran por defecto la prioridad del proceso y el modo softrealtime ya que requieren cambios extra en la configuración del sistema (se pueden configurar en el fichero .ini).
* Una API para un supervisor, de tal manera que una aplicación pueda activar gamemode para otra aplicación, como por ejemplo launchers como Lutris, Steam y demás, que al lanzar un juego podrían habilitar para ese juego gamemode, lo soporte o no de manera nativa.
* Correcciones y mejoras varias


La verdad, que los cambios a simple vista no es que parezcan gran cosa, al margen del overclock para NVIDIA, el cambio de modo de trabajo para AMD, aunque al estar en fase experimental, salvo que queramos correr el riesgo, es como si no estuvieran, el resto de cambios son pequeños ajustes, pero que le dan gran versatilidad a la herramienta, y facilitan que el usuario no tenga que complicarse mucho para aprovecharse de esas pequeñas mejoras que aporta la utilidad, sobretodo el poder hacer que una aplicación haga que un juego se ejecute con gamemode activo sin que el usuario tenga que hacer nada (y esperemos que los desarrolladores de estos launchers lo implementen pronto).


Y tú, ¿usas GameMode? Cuéntanos tus impresiones sobre esta herramienta en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

