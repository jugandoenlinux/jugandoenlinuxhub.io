---
author: Pato
category: Estrategia
date: 2020-02-18 16:47:28
excerpt: "<p>Gestiona un panal de abejas en el juego indie de&nbsp;@Cheeseness. \xBF\
  Te gusta la miel?</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HiveTime.webp
joomla_id: 1172
joomla_url: hive-time-ya-esta-disponible-para-linux-en-itch-io
layout: post
tags:
- indie
- estrategia
- itchio
- gestion
title: "Hive Time ya est\xE1 disponible para Linux en Itch.io"
---
Gestiona un panal de abejas en el juego indie de @Cheeseness. ¿Te gusta la miel?


Hace unos días que el juego de Cheeseness, un conocido miembro de la comunidad linuxera al que solemos seguir, ya está disponible para poderlo disfrutar desde su página de itch.io.


Se trata de **Hive Time**, un juego de gestión y construcción de una colonia de abejas, donde tendremos que desempeñar diversos roles para prosperar y conseguir ser una colonia exitosa.


Hace unos días que el autor ha publicado un nuevo vídeo promocional de su juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" class="mastodon-embed" src="https://mastodon.social/@Cheeseness/103673088811663271/embed" style="max-width: 100%; border: 0px; display: block; margin-left: auto; margin-right: auto;" width="400"></iframe></div>




<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/w4M6-zkLN6Y" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Además de publicarlo en la conocida plataforma de micromecenazgo, Cheeseness **distribuye su juego bajo la premisa "paga lo que quieras"**, de modo que aparte de sugerir un precio de 10 dólares, puedes pagar por el la cantidad que quieras, incluso nada. Eso sí, instándo a quien lo juegue y le haya gustado a pasar luego y dejarle alguna propina. 


Si te gustan las iniciativas indie de este tipo y los juegos de gestión y estratégia tienes disponible Hive Time [en su página de Itch.io](https://cheeseness.itch.io/hive-time).


 

