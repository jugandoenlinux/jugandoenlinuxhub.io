---
author: Pato
category: Software
date: 2018-06-13 17:58:16
excerpt: "<p>Nuevos elementos y un renovado aspecto y remodelaci\xF3n del chat entre\
  \ otras cosas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3d4417dea4b5985d283b0a7dfa9861de.webp
joomla_id: 768
joomla_url: steam-comienza-a-mostrar-cambios-significativos-en-su-cliente-beta
layout: post
tags:
- steam
- beta
title: Steam comienza a mostrar cambios significativos en su cliente Beta
---
Nuevos elementos y un renovado aspecto y remodelación del chat entre otras cosas

Hace bastante tiempo que sabemos que Valve tiene en mente "remodelar" su cliente y tienda de Steam para darle un aspecto y funcionalidad mas modernos. De hecho hace mas de un año que se espera algún movimiento en este aspecto pero no ha sido hasta ahora que se comienzan a ver cambios. Si bien de momento no son mas que "cambios" de interfaz y algunos iconos, la mayor remodelación ha sido la del chat y gestor de usuarios. Es posible que Valve haya visto que la comunicación de sus usuarios está en otras plataformas (Discord?) y haya decidido renovar ciertos apartados en un intento de ofrecer un servicio más atractivo. En la siguiente imagen puedes ver los cambios resaltados que nos hemos encontrado.


![Steamch1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SteamStoreNew/Steamch1.webp)


Los iconos de la barra superior han cambiado a otros mas sencillos y ahora tenemos la imagen de perfil, y clicando en tu nombre de usuario podemos desplegar el menú.


Ahora el chat estará enfocado en simplificar las comunicaciones entre tus contactos separando a los usuarios por distintos niveles, ya sea por juego, presencia etc.


Por otra parte, los chats de grupo también se han modificado y ahora es mucho mas fácil añadir amigos al mismo canal. Tan solo tienes que arrastrar y pegar en la pestaña correspondiente, o puedes enviar un enlace para invitar a cualquier amigo a un chat concreto.


Por la parte del propio chat, ahora es posible enviar archivos de imagen y vídeos, con tan solo pegar los enlaces correspondientes y es posible agrupar y crear canales dentro de un mismo grupo, a modo de "chat rooms". También tenemos los chats agrupados por pestañas, lo que queda mucho mas limpio y claro a la hora de pasar de uno a otro.


![Steamch2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SteamStoreNew/Steamch2.webp)


Los chats de voz también han sufrido su ración de cambios y ahora se basa en webRTC, utilizando un codec Opus cifrado para una seguridad máxima en las comunicaciones por voz y a la vez añadiendo una calidad superior de sonido. 


Si estas buscando todos estos cambios y no los ves en tu cliente de Steam, probablemente sea por que no estás ejecutando el cliente de Steam en su versión Beta, que es donde están llegando estas novedades. Ojo, por que al menos el chat de voz no es compatible con el del cliente estable.


Si quieres saber mas, tienes todos los cambios en la página del anuncio de la beta de Steam [en este enlace](https://steamcommunity.com/updates/chatupdate).


Si todo va bien, en poco tiempo deberían aparecer todos estos cambios en el cliente estable de Steam.


¿Que te parecen estos cambios? ¿Crees que serán atractivos para arañarle usuarios a otras plataformas de comunicación?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

