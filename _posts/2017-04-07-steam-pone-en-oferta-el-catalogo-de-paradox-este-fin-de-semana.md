---
author: leillo1975
category: Editorial
date: 2017-04-07 07:56:31
excerpt: "<p>Importantes descuentos en los t\xEDtulos del conocido editor</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/dca6745fdbb9da5b038270324f6ced2f.webp
joomla_id: 282
joomla_url: steam-pone-en-oferta-el-catalogo-de-paradox-este-fin-de-semana
layout: post
tags:
- steam
- paradox
- ofertas
- fin-de-semana
- editor
title: "Steam pone en oferta el cat\xE1logo de Paradox este fin de semana"
---
Importantes descuentos en los títulos del conocido editor

[Paradox](https://www.paradoxplaza.com/) siempre se ha caracterizado por ser una editora de juegos con un gusto especial. Sus títulos no son los típicos juegos comerciales superventas para todos los públicos, pero por norma general suelen estar cargados de calidad y buen hacer. Joyas como Europa Universalis, Magicka o Pillars of Eternity pululan por su catálogo, y aunque, como comentaba, no son para todo tipo de público, tienen una buena base de jugadores fieles. También es importante destacar que Paradox en los últimos tiempos siempre ha dado un soporte excelente lanzando sus títulos para todos los sistemas, incluyendo por supuesto GNU-Linux/SteamOS, por lo que es una de las editoras más amigables con nuestra plataforma.


Durante este fin de semana si estás pensando en comprar alguno de sus juegos, tienes una magnifica oportunidad de hacerlo, pues hay importantes **descuentos de hasta el 80%** en algunos títulos. Podeis haceros, por ejemplo, con:


  
**-Stellaris:** 23.99€ (40%)


**-Cities Skylines:** 8.95€ (68%)


**-Tyranny:** 28.13€ (33%)


**-Europa Universalis IV:** 9.99€ (75%)


**-Warlock II:** 3.99€ (80%)


**-Cities in Motion II:** 9.99€ (50%)


**-Pillars of Eternity:** 16.79€ (70%)


 


Pero si quereis más información es mejor que veais aquí **[la lista de ofertas.](http://store.steampowered.com/sale/paradox/)**


 


¿Pensais haceros con algún título de Paradox este fin de semana? ¿Habeis jugado ya a alguno de ellos? Podeis responder a estas preguntas o escribir lo que querais sobre Paradox en los comentarios, o en nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

