---
author: leillo1975
category: "Acci\xF3n"
date: 2020-06-01 22:49:07
excerpt: "<p>@unepic_fran no se cierra a una versi\xF3n para Linux.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/UnMetal/UnMetal.webp
joomla_id: 1228
joomla_url: unmetal-lo-nuevo-del-creador-de-unepic-no-tendra-soporte-nativo-a1
layout: post
tags:
- wine
- retro
- port
- unepic
- unmetal
title: "UnMetal, lo nuevo del creador de UnEpic podr\xEDa tener soporte nativo (ACTUALIZADO)"
---
@unepic_fran no se cierra a una versión para Linux.


**ACTUALIZADO 21-6-20:** Hace escasos minutos, Francisco Tellez de Meneses, en el [Q&A](https://youtu.be/aRrpnJaikgQ?t=1885) que está realizando ahora mismo en directo, **ha contestado a nuestra pregunta sobre un posible port de su juego**, y en esta ocasión **ha sido más receptivo a llevar su juego a nuestro sistema**. Concretamente ha comentado que UnMetal, al no tener un modo multiplayer representa menos problemas a la hora de portarlo entre diferentes sistemas; y que **si algún usuario o desarrollador de GNU/Linux se ofrece a portarlo el no está cerrado a esa posibilidad**. Como veis difiere de lo comunicado hace unas semanas, donde lo ponía mucho más difícil.


Esperemos que alguien con amplios conocimientos de programación de videojuegos en Linux y con ganas de traernos este apetecible título a nuestro sistema, pueda echarle una mano a uno de los mejores desarrolladores de este pais. Crucemos los dedos.




---


**NOTICIA ORIGINAL:**  
 Hace algunos días, pegándome un garbeo por las redes sociales me llamó la atención una imagen que enseguida me recordó a uno de mis juegos favoritos, [**Metal Gear Solid**](https://es.wikipedia.org/wiki/Metal_Gear_Solid). Leyendo el mensaje vi que el juego se llamaba [UnMetal](https://store.steampowered.com/app/1203710/UnMetal/), inmediatamente me dispuse a buscar información sobre él, viendo para mi sorpresa su [creador](http://www.unepicfran.com/sp/news.html) era el mismo que el del fantástico [UnEpic](https://store.steampowered.com/app/233980/UnEpic/), **Francisco Tellez de Meneses**. Por supuesto que tenía que saber si los Linuxeros podríamos disfrutar de él, y viendo que en Steam solo mostraba soporte para Windows me decidi a dejar una pregunta en la [comunidad](https://steamcommunity.com/app/1203710/discussions/0/2254560552297213809/?tscn=1591045786#c2284960483104394659) de dicha tienda, recibiendo respuesta, aunque por desgracia no la que todos esperábamos.


**Su creador nos confirmaba que el juego es difícil que tenga una versión nativa**, aunque nos aseguró que funcionará correctamente con Wine. La razón dada en las propias palabras del desarrollador es "***Poder mantener un porting a Linux constante con cada versión. No tengo conocimientos de Linux y depender de alguien ya me causó problemas con otro juego.***" Una auténtica pena, la verdad.


Con respecto a esto, es una aunténtica pena ver como ultimamente **algunos estudios que en su día nos daban soporte se están descolgando de nuestra plataforma**. En el caso de las grandes producciones de juegos AAA es habitual, pero lo más triste cuando esto se produce por parte de compañías pequeñas o indies, pues al ser juegos "minoritarios" probablemente calasen más entre las "minorias", siendo más empáticos con los que usamos Linux. En este tipo de juegos donde su base de jugadores no es tan grande, unas "pocas" copias más son siempre bienvenidas.


Aunque **también es cierto y de agradecer** que si no puede garantizar un port de calidad a la altura de la versión de Windows, es mejor que no lo haga. En este caso, al no depender este de si mismo, **tendría que confiar en que quien lo haga se comprometa a mantenerlo**, lo cual no siempre es posible. En caso contrario quien quedaría mal es el creador del juego, no el que lo porta, lo cual no sería justo. Esperemos que la palabra del desarrollador no sea definitiva y encuentre a un "porter" que le ofrezca garantías, pudiendo en un futuro comprar y jugar nativamente este apeticible título del que os dejamos aquí el trailer:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/0WEHFtFpwpI" width="800"></iframe></div>


¿Qué opinais que UnMetal no tenga soporte nativo? ¿Os gustó UnEpic? Podeis darnos vuestra opinión sobre este juego en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

