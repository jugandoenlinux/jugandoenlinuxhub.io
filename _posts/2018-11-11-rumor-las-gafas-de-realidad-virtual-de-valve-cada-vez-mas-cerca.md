---
author: Serjor
category: Realidad Virtual
date: 2018-11-11 08:22:00
excerpt: "<p>Un bundle con gafas de realidad virtual, mandos y juego podr\xEDa estar\
  \ en camino de la mano de Valve</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a6922ff8cfad5d7f36649c0f9c77253b.webp
joomla_id: 897
joomla_url: rumor-las-gafas-de-realidad-virtual-de-valve-cada-vez-mas-cerca
layout: post
tags:
- realidad-virtual
- steamvr
- valve
title: "Rumor: Las gafas de realidad virtual de Valve cada vez m\xE1s cerca"
---
Un bundle con gafas de realidad virtual, mandos y juego podría estar en camino de la mano de Valve

Normalmente no soy partidario de este tipo de noticias que tienen más pinta de clickbait que de otra cosa, pero en esta ocasión, como parece que hay datos por detrás, vamos a conceder el beneficio de la duda al artículo, pero tomemos todo esto con pinzas.


Y es que en [uploadvr](https://uploadvr.com/valve-135-vr-headset-half-life/) nos informan de que en imgur han aparecido fotos filtrando [imágenes](https://imgur.com/a/nYegjQp) de lo que serán las futuras gafas de realidad virtual de Valve.


El caso es que si miramos hacia atrás no es descabellado pensar que vayan a sacar sus propias gafas:


* Valve hace hardware
* Lleva tiempo trabajando con HTC para sacar las Vive adelante
* Sigue empujando en el tema de la realidad virtual
* Quiere ser como Nintendo y controlar hardware y software


En el artículo de uploadvr comentan que sus fuentes les han dado detalles técnicos como el tema del ángulo de visión, que será de 135º, o que vendrá con un juego que será una precuela de Half Life, pero como comentaba al principio, quitando las imágenes que parecen bastante reales, todo parece un poco una manera de generar de visitas.


Y a ti, ¿te gusta la realidad virtual? Cuéntanos tus experiencias o rumores en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

