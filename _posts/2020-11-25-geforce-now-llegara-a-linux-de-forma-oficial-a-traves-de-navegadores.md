---
author: Pato
category: Noticia
date: 2020-11-25 11:10:09
excerpt: "<p>El servicio de Nvidia hasta ahora solo era accesible en Linux mediante\
  \ un cambio de UserAgent</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Geforcenow/Geforcenow.webp
joomla_id: 1263
joomla_url: geforce-now-llegara-a-linux-de-forma-oficial-a-traves-de-navegadores
layout: post
tags:
- gog
- cyberpunk
- the-witcher-3
- geforce-now
- fortnite
title: "Geforce Now llegar\xE1 a Linux de forma oficial a trav\xE9s de navegadores"
---
El servicio de Nvidia hasta ahora solo era accesible en Linux mediante un cambio de UserAgent


 


Según informa Nvidia en su [blog](blogs.nvidia.com) sobre **Geforce NOW**, el servicio de streaming de la compañía californiana llegará a nuestros sistemas Linux en un futuro de forma oficial a través de navegadores con base Chromium, siguiendo la estela de servicios como el de Google Stadia. 


Y es que tras anunciar la llegada de **Geforce NOW** a los dispositivos de Apple mediante su navegador Safari, está claro que este tipo de servicios pretende llegar a cuantos sistemas sean capaces de ejecutar un navegador.


La prueba está en que **en Linux ya se podía ejecutar este servicio cambiando el User Agent** del navegador tal y como ya explicamos [en un anterior artículo](index.php/homepage/noticias-news/1248-ya-es-posible-jugar-en-streaming-mediante-el-servicio-geforce-now-en-linux-actualizacion), eso sí de manera extraoficial (esto fue posible por la llegada del servicio a los Chromebooks), lo que cambiará en un futuro.


Además, en el mismo post anuncian la llegada de nuevas plataformas y juegos que serán compatibles con el servicio, como **Fortnite** **o la plataforma GOG** junto a sus buques insignia **"The Witcher 3: Wild Hunt" y "Cyberpunk 2077".**


Tienes toda la información en el [blog de Geforce NOW](https://blogs.nvidia.com/blog/2020/11/19/geforce-now-on-ios-safari/).


### ¿Qué es Geforce NOW?


Geforce NOW es un servicio de streaming a través de internet que a diferencia de Stadia, (que es un servicio de publicación en si mismo) **se vincula a diferentes servicios o plataformas de videojuegos como Steam, Origin o Uplay para ofrecerte la posibilidad de jugar a los juegos que hayas comprado en esas plataformas ejecutándolos desde los servidores de Nvidia** de forma que no necesitas ningún tipo de hardware dedicado ni potente. Con un navegador y una buena conexión a internet podrás jugar a tus videojuegos mediante este servicio con características gráficas avanzadas como resolución 1080p 60FPS, DLSS 2.0 y RTX en los juegos que lo soporten.


Tienes toda la información y los planes de suscripción en [l](https://www.nvidia.com/es-es/geforce-now/)a [web del servicio Geforce NOW](https://www.nvidia.com/es-es/geforce-now/).


¿Que te parece la llegada del servicio de Nvidia Geforce NOW a Linux?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

