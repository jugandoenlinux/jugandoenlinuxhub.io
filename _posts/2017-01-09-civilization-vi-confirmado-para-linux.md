---
author: Serjor
category: Estrategia
date: 2017-01-09 19:34:19
excerpt: "<p>Confirmado oficialmente, Civilization VI tendr\xE1 versi\xF3n para Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e36a512fe5b90101a88ae780e05256f8.webp
joomla_id: 170
joomla_url: civilization-vi-confirmado-para-linux
layout: post
tags:
- estrategia
- anuncios
- civilization-vi
title: "Civilization VI confirmado para Linux (Actualizaci\xF3n: fecha de salida disponible)"
---
Confirmado oficialmente, Civilization VI tendrá versión para Linux

Ya os lo [adelantábamos](https://twitter.com/JugandoenLinux/status/818432189213839360) esta mañana através de nuestra cuenta de Twitter, y es que hoy [GOL](https://www.gamingonlinux.com/) nos ha traído en [exclusiva](https://www.gamingonlinux.com/articles/exclusive-civilization-vi-now-fully-confirmed-to-be-coming-for-steamos-linux-and-soon-too.8860) el anuncio de Civilization IV para Linux.


Si bien es cierto que Aspyr ya nos lo había dejado entrever y era un secreto a voces:



> 
> Wanted to address a section of our fans that may feel left out recently. Dear Linux users, we haven't forgotten you! CivVI is still possible
> 
> 
> — Aspyr Media (@AspyrMedia) [October 24, 2016](https://twitter.com/AspyrMedia/status/790623451660881920)



 


Hoy nos llega la [confirmación oficial](https://blog.aspyr.com/2017/01/09/sid-meiers-civilization-vi-coming-soon-linux/) por parte de la compañía, por lo que son muy buenas noticias para los amantes de este género que tanto éxito cosecha, cómo pudimos descubrir al ver que está dentro de los juegos platino en la [lista de juegos más vendidos](http://store.steampowered.com/sale/2016_top_sellers/) que confeccionó steam, y tal y como dicen en el comunicado oficial, ha sido el juego para Linux que más les han pedido.


 


Os dejamos con el teaser del juego para ir calentando motores hasta su llegada, la cuál por cierto, a la hora de escribir este artículo todavía no tiene fecha oficial de salida:


 


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/qvBf6WBatk0" width="560"></iframe></div>


 


Actualización: Aspyr ha confirmado vía tweeter lo que nos avanzaban desde [gamingonlinux](https://www.gamingonlinux.com/articles/early-exclusive-civilization-vi-to-release-february-9th-for-linux-with-a-discount-nvidia-only-for-now.9052):, que el juego será lanzado el 9 de febrero y vendrá acompañado de un descuento:


 



> 
> Civilization VI is on [#Linux](https://twitter.com/hashtag/Linux?src=hash) Feb 9! Penguins get ready to take [#onemoreturn](https://twitter.com/hashtag/onemoreturn?src=hash) with a discounted release! Base game is $47.99. [pic.twitter.com/yaZsEmMhNX](https://t.co/yaZsEmMhNX)
> 
> 
> — Aspyr Media (@AspyrMedia) [6 de febrero de 2017](https://twitter.com/AspyrMedia/status/828619257189634048)



 


 


Y tú, ¿estabas esperando este juego? Dínoslo en los comentarios, el [foro](index.php/foro/), o a través del canal de [Telegram](https://t.me/jugandoenlinux) o de [Discord](https://discord.gg/fgQubVY).

