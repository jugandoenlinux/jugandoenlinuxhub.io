---
author: Serjor
category: Noticia
date: 2018-12-04 15:25:33
excerpt: <p>No hay datos sobre si dispondremos de un cliente para GNU/Linux</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c7726f5ad947c814df35c64905184dce.webp
joomla_id: 919
joomla_url: epic-games-anuncia-su-propia-tienda-de-videojuegos
layout: post
tags:
- valve
- epic-games
- tienda
title: Epic Games anuncia su propia tienda de videojuegos
---
No hay datos sobre si dispondremos de un cliente para GNU/Linux

Bueno, parece que la moda de tener lanzadores de juegos y tiendas no ha parado, y vía [phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Epic-Games-Store) nos enteramos de que Epic Games ha sacado su propia tienda de videojuegos.


Algo se veía venir cuando Valve [anunció](https://steamcommunity.com/groups/steamworks#announcements/detail/1697191267930157838) el pasado 1 de diciembre que iba a cambiar su política de tasas que cobraría a las desarrolladoras (con gran volumen de beneficios, por cierto), y es que tal y cómo podemos [leer en el anuncio](https://www.unrealengine.com/en-US/blog/announcing-the-epic-games-store) de Epic Games, el beneficio para la desarrolladora es bastante mayor si el juego se compra en su tienda que en Steam, siendo un 12% para Epic y el 88% restante directamente para el estudio desarrollador.


![https://www.unrealengine.com/en-US/blog/announcing-the-epic-games-store](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/EpicGames/ue-profits.webp)


La verdad es que es una gran baza en favor de Epic, ya que Valve lleva años aprovechándose de su posición dominante en el mercado de PC.


La tienda incluye también un programa de partnership con los creadores de contenidos en YouTube y Twitch a través de un programa de referidos, y ofrecen control completo sobre la página del juego (por ejemplo Valve añade la información que quiere, como por ejemplo juegos relacionados).


Según comentan darán más detalles el próximo 6 de diciembre durante la gala de los "The Game Awards" de este año, así que como la esperanza es lo último que se pierde, confiaremos en que haya versión para GNU/Linux (vale, es Epic, no me lo creo ni yo, pero casi es Navidad y hay que creer en el espíritu navideño).


Y tú, ¿cómo ves este movimiento por parte de Epic Games? Cuéntanoslo en los comentarios y en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

