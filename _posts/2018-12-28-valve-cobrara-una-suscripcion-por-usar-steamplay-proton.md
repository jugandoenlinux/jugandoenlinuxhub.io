---
author: Pato
category: Noticia
date: 2018-12-28 15:42:09
excerpt: "<p>La medida pretende aprovechar el tir\xF3n de esta herramienta.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2f4e552d638dc78e77d3d5a128eaa4d7.webp
joomla_id: 943
joomla_url: valve-cobrara-una-suscripcion-por-usar-steamplay-proton
layout: post
title: "Valve cobrar\xE1 una suscripci\xF3n por usar SteamPlay/Proton (Inocentada)"
---
La medida pretende aprovechar el tirón de esta herramienta.

Era casi un secreto a voces. Como todos sabemos, Valve ha invertido cantidades ingentes de dinero en toda la tecnología que rodea Steam Play/Proton y ahora la compañía quiere comenzar a monetizar este servicio.


En un comunicado interno, un representante de Valve ha confirmado que a principios del próximo año 2019 todo el que quiera jugar con un Proton **tendrá que pagar una pequeña cuota** de tan solo 19,95€ al año.


De este modo, Valve espera que el tirón que está teniendo esta tecnología que permite ejecutar los juegos de Windos en Linux le ayude a financiar parte del carísimo desarrollo que supone Proton.


En otro orden de cosas, en el mismo comunicado anuncian que **todos los juegos de Epic Games dejarán de estar disponibles en Steam** eliminando todos los archivos de sus bases de datos, con lo que ni siquiera será posible descargarlos aunque los tengas en tu biblioteca. Según palabras del portavoz en el comunicado, "si Epic quiere comer, que vaya a su casa".


Tienes toda la información en el siguiente enlace: [Valvesoftnews.com](images/banners/Proton.jpeg)


¿que te parece la medida para financiar esta tecnología? ¿Aportarás para seguir teniendo opción a jugar los juegos de Windos en Linux? ¿Piensas que la expulsión de Epic de Steam tiene algún fundamento?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

