---
author: Serjor
category: Estrategia
date: 2017-11-29 22:26:57
excerpt: <p>Cambio de registro para una franquicia veterana en los juegos de estrategia</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8eddc4b0895231bbf7f506e7d72e890b.webp
joomla_id: 561
joomla_url: warhammer-40000-gladius-anuncia-su-beta-abierta-con-version-para-gnu-linux
layout: post
tags:
- estrategia
- warhammer
- warhammer-40k-gladius
- 4x
title: "Warhammer 40000: Gladius anuncia su beta abierta con versi\xF3n para GNU/Linux"
---
Cambio de registro para una franquicia veterana en los juegos de estrategia

Leemos vía [LinuxAdictos](https://www.linuxadictos.com/warhammer-40-000-gladius-se-anuncia-gnu-linux.html) el [anuncio](http://www.slitherine.com/news/2447/Warhammer.40,000:.Gladius.-.Relics.of.War.anunciado) de un nuevo juego basado en el universo de Warhammer 40000. En esta ocasión el juego pasa de estrategia en tiempo real a estrategia por turnos al más puro estilo Civilization, con lo que los chicos de Proxy Studios, quienes ya nos trajeron el juego Pandora: First Contact, repiten género y fórmula por primera vez en esta archiconocida franquicia.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/KyvxGl_hKf0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


El juego tiene previsto su lanzamiento antes de que acabe el año, no obstante, podéis participar en su beta abierta [aquí](http://www.slitherine.com/beta/cnda.asp?gid=701), o si preferís, podéis ver la preview que harán en [su canal de Twitch](https://twitch.tv/slitherinegroup) el próximo 1 de diciembre.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/489630/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Y tú, ¿tienes ganas de más Warhammer 40k? Cuéntanoslo en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

