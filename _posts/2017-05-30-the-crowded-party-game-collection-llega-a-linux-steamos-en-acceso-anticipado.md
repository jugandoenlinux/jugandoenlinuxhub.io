---
author: Pato
category: Arcade
date: 2017-05-30 11:39:53
excerpt: "<p>La propuesta es jugar a \"minijuegos\" en modo multijugador local con\
  \ los amigos. \xBFTen\xE9is un tel\xE9fono a mano?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4ccbd8f9cf85039af7a26ff5d030e033.webp
joomla_id: 348
joomla_url: the-crowded-party-game-collection-llega-a-linux-steamos-en-acceso-anticipado
layout: post
tags:
- accion
- indie
- acceso-anticipado
- arcade
title: '''The Crowded Party Game Collection'' llega a Linux/SteamOS en acceso anticipado'
---
La propuesta es jugar a "minijuegos" en modo multijugador local con los amigos. ¿Tenéis un teléfono a mano?

Si eres de los que gustan de reunir a los amigos en casa en torno a una pantalla y jugar videojuegos, seguro que alguna vez te has encontrado en la tesitura de que hay más amigos que posibilidades. Ya sea por que los juegos solo soportan un número limitado de jugadores simultáneos, ya sea por que no tienes suficientes mandos para todos, siempre queda alguien que "mira" mientras los demás juegan.


Pues esta es la propuesta de 'The Crowded Party Game Collection'. Una colección de juegos desenfadados con los que disfrutar en compañía, con hasta 10 jugadores simultáneos. Y no, no necesitas 10 mandos. Tan solo asegúrate que tus amigos traen su teléfono móvil.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/PVHKj5OvyGA" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


El concepto en sí es similar a otros juegos disponibles en consolas pero esta vez llevado un poco más allá. Tan solo con tu móvil y accediendo a una web (no hace falta ninguna app) ya tendrás tu mando para jugar a los minijuegos que proponen.


Hay que recalcar que se trata de un "acceso anticipado", por lo que ahora mismo hay unos pocos juegos a los que jugar, sin embargo prometen ir sacando más a medida que vayan mejorando la experiencia. Tampoco es descartable que haya algún bug, por lo que hay que tomarlo como lo que es.


Si estás interesado en este concepto, tienes toda la información y el acceso anticipado en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/618640/" width="646"></iframe></div>


¿Qué te parece el concepto? ¿Juegas con tus amigos en juegos multijugador local?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

