---
author: Serjor
category: Noticia
date: 2018-11-01 09:06:22
excerpt: "<p>Peque\xF1os cambios para mejorar la</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3f6beb4ab529bb7b00ce1447aa996693.webp
joomla_id: 887
joomla_url: nueva-version-de-proton-3-16-4
layout: post
tags:
- steam
- valve
- steam-play
- proton
title: "Nueva versi\xF3n de Proton, 3.16-4"
---
Pequeños cambios para mejorar la

Ayer Pierre-Loup Griffais, uno de los mayores responsables de Steam para GNU/Linux anunciaba vía twitter que tenemos nueva versión de Proton disponible, la 3.16-4:




> 
> New Proton 3.16 build pushed: <https://t.co/NVVCv5kvD7> [pic.twitter.com/t9K2oJFx0C](https://t.co/t9K2oJFx0C)
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [31 de octubre de 2018](https://twitter.com/Plagman2/status/1057753701186592769?ref_src=twsrc%5Etfw)



Aunque la lista de cambios no es muy larga, sí que podemos ver un par de mejoras que deberían significar mucha mejor compatibilidad, y es que por un lado han dado solución al problema de las fuentes que adolecen juegos como GTA V, con lo que todos aquellos que fallaban por no encontrar las fuentes adecuadas o se veían mal, ahora deberían funcionar sin problemas. También han solucionado problemas de compatibilidad con Steamworks, con lo que ya está disponible la [famosa solución](https://twitter.com/Plagman2/status/1056261078391316480) del problema de SoulCalibur VI (y probablemente otros) tenían para conectarse a los servidores. De hecho, según me comenta Leillo, a GTA V ya le funciona el on-line (aunque puede que se solucionara el problema en la versión anterior) (Me confirma que el on-line de GTA V ya funcionaba de antes) pero os puedo confirmar que no sirve para solucionar el problema de conexión a los servidores de MGS V :-(


Además, ahora la compilación de Proton se hace con [algunos flags](https://github.com/ValveSoftware/Proton/commit/56b174af5409e19004bbc6dbf3cb7175cbe10379) que puede que en algún caso mejoren el rendimiento.


Si queréis saber qué juegos funcionan con Proton os recordamos que tenéis disponible los siguientes enlaces:


* [Proton Compatible](https://store.steampowered.com/curator/33483305-Proton-Compatible/), a través del sistema de mentores nos dicen que juegos funcionan perfectamente (sin tener que tocar un bit de configuración) a través de Proton.
* [ProtonDB](https://www.protondb.com/), antes conocida como [Steam Play Compatibility Reports](https://spcr.netlify.com/), donde al buscar un juego nos indica si funciona o no y con qué grado de compatibilidad, y además, podemos aportar a la comunidad añadiendo nuestros informes.
* [Test de compatibilidad de nuestros juegos](index.php/foro/juegos-steam-play/127-tests-de-compatibilidad-de-tus-juegos-en-steam-play), que es un hilo de nuestro foro en el que nos gustaría que nos dijerais cómo os funcionan vuestros juegos bajo Proton.


Y tú, ¿qué opinas del avance de Proton? Cuéntanoslo en los comentarios y en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

