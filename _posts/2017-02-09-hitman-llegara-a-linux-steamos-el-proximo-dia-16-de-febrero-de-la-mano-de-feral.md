---
author: Pato
category: "Acci\xF3n"
date: 2017-02-09 17:10:27
excerpt: "<p>Lo han anunciado por sorpresa. El asesino pr\xE1cticamente ya est\xE1\
  \ aqu\xED.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/11ff9b4c887cc2e009006cc7fd3aeb4c.webp
joomla_id: 210
joomla_url: hitman-llegara-a-linux-steamos-el-proximo-dia-16-de-febrero-de-la-mano-de-feral
layout: post
tags:
- accion
- proximamente
- steam
- sigilo
- tercera-persona
- feral-interactive
title: "'Hitman' llegar\xE1 a Linux/SteamOS el pr\xF3ximo d\xEDa 16 de Febrero de\
  \ la mano de Feral (ACTUALIZADO: Livestreaming)"
---
Lo han anunciado por sorpresa. El asesino prácticamente ya está aquí.

Si parecía que Feral se había relajado un poco, era una ilusión. Casi por sorpresa han anunciado que 'Hitman' [[Steam](http://store.steampowered.com/app/236870/)] , el Agente 47, el asesino más letal del mundo está a punto de llegar a Linux/SteamOS y lo hará muy pronto. Tanto como el próximo día 16. ¡Jueves de la semana que viene!. ¿Estás preparado para asesinar?.



> 
> Agent 47 marks his target: the complete first season of HITMAN is coming to Linux on February 16. Watch the trailer: <https://t.co/oNssj1emm8> [pic.twitter.com/m24P5VLBvj](https://t.co/m24P5VLBvj)
> 
> 
> — Feral Interactive (@feralgames) [7 de febrero de 2017](https://twitter.com/feralgames/status/828996811692060675)



 


<!--
@page { margin: 2cm }
 p { margin-bottom: 0.25cm; line-height: 120% }
-->
También lo han anunciado en la [web de Feral](http://www.feralinteractive.com/es/news/730/) donde dan cuenta con todo detalle, aunque esta vez no han publicado miniweb. Igual la publican un poco mas adelante.


Además, Feral nos traerá toda la primera temporada del juego, que incluye el prólogo, los episodios del 1 al 6 y el "bonus", con lo que a poco que nos hagan un pequeño descuento por el lanzamiento estaremos ante una ocasión inigualable para disfrutar de este port, que actualmente tiene un precio de 49,99€.


Como nos pica la curiosidad, les hemos preguntado a Feral Interactive si han empleado Vulkan u OpenGL, a lo que nos han contestado "Lo segundo":



> 
> [@JugandoenLinux](https://twitter.com/JugandoenLinux) The second one.
> 
> 
> — Feral Interactive (@feralgames) [7 de febrero de 2017](https://twitter.com/feralgames/status/829004121839640577)



No es de extrañar, pues pienso que hasta al menos dentro de unos meses no veremos ningún port "serio" corriendo sobre la nueva API, aunque sabemos que Feral lo tiene en sus planes y de ahí nuestra curiosidad.


De nuevo Square Enix sigue aportando sus juegos para que nos lleguen a través de Feral o diréctamente, como por ejemplo 'Life is Strange', la serie '[Lara Croft Go](index.php/item/240-lara-croft-go-el-juego-de-rompecabezas-basado-en-la-protagonista-de-tomb-raider-ya-disponible-en-linux-steamos)' y 'Hitman Go', o '[Deus Ex: Mankind Divided](index.php/item/69-deus-ex-mankind-divided-ya-disponible-para-linux-steamos)'. Esperemos que les salga muy rentable y sigan trayéndonos el resto de sus juegos.


Acerca del Juego:


*Conviértete en un maestro asesino en una emocionante historia de espionaje ambientada en el mundo del asesinato. Viaja a Francia, Italia, Marruecos, Tailandia, los Estados Unidos y Japón para eliminar objetivos de alto nivel.*


Aquí tenéis el vídeo del anuncio que ha publicado Feral:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/t5v1ZAlM5oA" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 ¿Qué te parece la llegada de 'Hitman' a nuestro sistema favorito? ¿Piensas "asesinar" con el Agente 47?


Cuéntamelo en los comentarios, en el foro o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 


**ACTUALIZACIÓN:** Feral Interactive ha confoirmado los requisitos técnicos de Hitman:


**Mínimos:**


Intel Core i5-2500K o AMD FX-8350


Ubuntu 16.04 o SteamOS 2.0


8GB de RAM


Nvidia GeForce GTX 680/AMD R9 270X de 2GB o superior.\*


**Recomendados:**


i7 3770


Ubuntu 16.10 o SteamOS 2.0


16GB de RAM


Nvidia GeForce GTX 970/AMD R9 290 de 4GB o superior.\*


 


\*Las tarjetas gráficas Nvidia requiere la versión de controlador 375.26. Las tarjetas gráficas AMD requiere MESA 13.0.3 o posterior. Actualmente, las tarjetas gráficas Intel no son compatibles con HITMAN™.


 


**ACTUALIZACIÓN 2:** Feral acaba de anunciar que hará un livestriming en [su Twitch](https://www.twitch.tv/feralinteractive) el miercoles 15 de febrero a las 6 PM-GMT (19:00 hora española). Será nuestra primera oportunidad de ver el juego funcionando nativamente en GNU/Linux. 


 



> 
> This Wednesday, get your first look at HITMAN running on Linux, live on Twitch: <https://t.co/kbDZOaA0zS> [#FeralPlays](https://twitter.com/hashtag/FeralPlays?src=hash) We expect eyewitnesses. [pic.twitter.com/NWJpQwLzHu](https://t.co/NWJpQwLzHu)
> 
> 
> — Feral Interactive (@feralgames) [February 13, 2017](https://twitter.com/feralgames/status/831081241952866304)



 


 

