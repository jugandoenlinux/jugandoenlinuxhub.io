---
author: leillo1975
category: Carreras
date: 2018-10-27 11:20:16
excerpt: <p>@ansdor asi nos lo ha confimado</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d32703e4d6167ce27e1a9881230505a2.webp
joomla_id: 881
joomla_url: slipstream-pronto-tendra-multijugador-local
layout: post
tags:
- multijugador
- retro
- slipstream
- ansdor
title: "Slipstream pronto tendr\xE1 multijugador local."
---
@ansdor asi nos lo ha confimado

Si llevais tiempo leyendo nuestras noticias y pululando por nuestro grupo de [Telegram](https://t.me/jugandoenlinux), sabreis que un servidor es un fanático de los juegos de conducción y también de lo retro. [A mediados de mayo era lanzado al mercado Slipstream](index.php/homepage/generos/carreras/2-carreras/860-pronto-podremos-disfrutar-de-slipsteam), y poco tiempo después [le dedicábamos un completo análisis](index.php/homepage/analisis/20-analisis/915-analisis-slipstream) donde detallábamos sus numerosas virtudes.


Uno de los puntos que habían quedado pendientes era el apartado multijugador. Si bien nunca se confirmó el online, su desarrollador [Ansdor](http://www.ansdor.com/), si nos habló que estaba trabajando en una versión que incluyese la pantalla partida para poder disfrutarlo en casa con unos amigos o la familia. Ayer preguntamos a través de twiter a su creador y nos respondió esto:



> 
> yes yes yes I'm just wrapping things up and testing. It will come in time for the steam halloween sale
> 
> 
> — ansdor (@ansdor) [October 26, 2018](https://twitter.com/ansdor/status/1055931209824505856?ref_src=twsrc%5Etfw)



La traducción sería "*si, si, si. Solo estoy empaquetando las cosas y probando. Llegará a tiempo para la venta de halloween de Steam*". Por lo que podeis ver, muy pronto tendremos esta nueva característica del juego y podremos competir durante el Halloween, o Samaín, como se llama en mi tierra. Desde JugandoEnLinux os recomendamos encarecidamente este juego si os gustan los juegos de carreras de antaño, y os informaremos de cuando esté disponible esta actualización. Os dejamos con un Gameplay que grabábamos para nuestro [canal de Youtube](https://www.youtube.com/c/jugandoenlinuxcom) hace unos meses:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/LqkZebwbYyY" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 Podeis comprar Slipstream en [Itch.io](https://ansdor.itch.io/slipstream) y [Steam](https://store.steampowered.com/app/732810/Slipstream/)

