---
author: Pato
category: Editorial
date: 2016-10-04 06:59:02
excerpt: <p>Todos los meses Steam pregunta a sus usuarios acerca del hardware que
  utilizan para jugar. Por segundo mes consecutivo Linux muestra un leve crecimiento.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c82cc4e14a1d2c8c8ffff9840d24b558.webp
joomla_id: 29
joomla_url: publicada-la-encuesta-mensual-de-hardware-en-steam
layout: post
tags:
- steam
title: Publicada la encuesta mensual de Hardware en Steam
---
Todos los meses Steam pregunta a sus usuarios acerca del hardware que utilizan para jugar. Por segundo mes consecutivo Linux muestra un leve crecimiento.

Nuestro sistema del Pingüino está estabilizado desde hace tiempo en torno a un 0.8 - 0.9 por ciento en cuanto a utilización en las encuestas de hardware de Steam. Sin embargo eso no ha sido impedimento para que Linux siga creciendo en usuarios y número de juegos disponibles. El motivo es que la encuesta muestra porcentajes, no números absolutos, se basa en una encuesta que es voluntaria por lo que solo quien recibe el mensaje de invitación y decide hacerla es tomado en cuenta, y por último y no menos importante los usuarios con SteamOS o los que utilicen "Big Picture" no reciben la invitación a la encuesta, por lo que muy posiblemente los números reales bien pueden ser diferentes.


De todos modos sí es cierto que sirven de orientación de hacia donde se mueven las preferencias de los usuarios, por lo que tras unos meses en los que las encuestas han mostrado estancamiento o incluso retroceso, este es el segundo mes consecutivo que Linux muestra un aumento en su base de usuarios. Los números son los siguientes:


Total Linux:0.94% + 0.11%


Ubuntu 16.04.1 LTS 64bit 0.35% + 0.07%


Linux 64 bit 0.10% + 0.01%


Linux Mint 18 Sarah 64 bit 0.08% +0.02%


Ubuntu 14.04.5 LTS 64 bit 0.06% + 0.06%


Como puedes comprobar el crecimiento es en todas las distribuciones representativas, con especial énfasis en la última versión LTS de Ubuntu. Es posible que esto se deba a que Windows 10 dejó de ser gratuito y por tanto una parte de usuarios han ido buscando nuevos sistemas con el que reemplazarlo. No hay que olvidar que un cambio de hardware o de sistema es un buen modo de provocar que te salga la petición de la encuesta de Hardware de Steam.


Al parecer, el crecimiento que tenemos en Linux se debe a una pérdida de cuota por parte de Windows del -0.33% (Mac se llevó el resto del crecimiento). Esta tendencia bien puede cambiar el mes que viene y siendo sinceros el incremento de porcentaje es bastante pequeño. Hay quien afirma que estamos incluso dentro del porcentaje de error, pero aún así, el porcentaje de Linux sigue mas o menos donde estaba desde hace meses.


Como ya he dicho, eso no significa que Linux no esté en crecimiento, pues si la tendencia de la plataforma es de crecimiento con entre 150 - 200 millones de cuentas estimadas, el mantener el porcentaje significa una subida en el número de usuarios.


¿Estás de acuerdo con esta idea? Cuéntame en los comentarios

