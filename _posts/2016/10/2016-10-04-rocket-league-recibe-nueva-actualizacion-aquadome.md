---
author: Pato
category: Carreras
date: 2016-10-04 10:51:48
excerpt: "<p>Los chicos de Psyonix nos traen nuevo contenido para este adictivo juego\
  \ con mezcla de coches, acci\xF3n y deporte. Esta vez meten el juego bajo el agua.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/48ee1e8a0a8f50dce4f8cb9ab418e211.webp
joomla_id: 31
joomla_url: rocket-league-recibe-nueva-actualizacion-aquadome
layout: post
tags:
- accion
- deportes
- steamos
- carreras
title: "Rocket League recibe nueva actualizaci\xF3n: Aquadome"
---
Los chicos de Psyonix nos traen nuevo contenido para este adictivo juego con mezcla de coches, acción y deporte. Esta vez meten el juego bajo el agua.

Aquadome es una nueva Arena para Rocket League donde la pista de juego se encuentra... ¡en el fondo del Océano!


Esta vez nos sumergimos bajo el agua para llevar a cabo los locos partidos a los que nos tienen acostumbrados los chicos de Rocket League. Junto con la nueva Arena nos traen dos nuevos coches premium vía DLC llamados Triton y Proteus que están disponibles en la tienda del juego a un precio de 1.99 dólares. Según afirman los desarrolladores son dos de los coches mejor modelados visualmente de cuantos han hecho.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/vUVaV8ZGVbQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Además, se han rediseñado dos coches clásicos del juego como son Road Hog y Hotshot con mejoras visuales, y se han añadido un buen puñado de nuevos elementos para el garaje como nuevas ruedas, antenas, humos, un nuevo sombrero y mucho más.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/or-hotshot-roadhog.webp)


Que te parece el nuevo contenido para Rocket League?


Puedes comprar el juego en su [página de Steam](http://store.steampowered.com/app/252950/)

