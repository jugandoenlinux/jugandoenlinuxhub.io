---
author: Pato
category: Terror
date: 2016-10-10 19:55:09
excerpt: "<p>El juego de los espa\xF1oles Milkstone Studios ofrecer\xE1 una \"terror\xED\
  fica\" experiencia de juego asim\xE9trico 4 contra 1.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/feb4274796d93ff716e9650163a77fb8.webp
joomla_id: 44
joomla_url: white-noise-2-se-lanzara-en-linux-el-proximo-27-de-octubre-en-acceso-anticipado
layout: post
tags:
- indie
- terror
- acceso-anticipado
title: "White Noise 2 se lanzar\xE1 en Linux el pr\xF3ximo 27 de Octubre en Acceso\
  \ Anticipado"
---
El juego de los españoles Milkstone Studios ofrecerá una "terrorífica" experiencia de juego asimétrico 4 contra 1.

Se acerca la fecha de Halloween y como cada año los juegos de temática de terror hacen acto de presencia. En este caso, el estudio índie español Milkstone Studios [[página oficial](http://www.milkstonestudios.com/)] creadores de otros títulos que ya tenemos disponibles en Linux, como por ejemplo los excelentes [Ziggurat](http://store.steampowered.com/app/308420/), [Pharaonic](http://store.steampowered.com/app/386080/) o [Little Racers Street](http://store.steampowered.com/app/262690/) nos traen una interesante propuesta de terror asimétrico con [White Noise 2](http://store.steampowered.com/app/503350).


El juego saldrá en Steam en Acceso Anticipado el 27 de Octubre para que podamos disfrutarlo en estas fechas en las que apetece jugar a títulos de este género.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/Px41fglWYCY" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


#### Sinopsis de Steam:


**¡Vive una nueva experiencia en el horror con White Noise 2!** 


White Noise 2 es la secuela del exitoso White Noise Online. Forma parte del equipo de investigadores, ¡o controla a la criatura y devóralos a todos! White Noise 2 ofrece una experiencia de terror asimétrico 4vs1 que no te dejará indiferente.


**Juega con tus amigos**


White Noise 2 ofrece un sencillo sistema de matchmaking que te permite jugar con tus amigos sin complicaciones. Entra en la partida, elige si quieres ser el monstruo, un investigador, o dejarlo al azar, ¡y a jugar!


**Los investigadores han vuelto** 


Como investigador, explora el escenario, coopera con tus compañeros, y asegúrate de no perderte, ¡o serás presa fácil para la criatura! Evita perder la cordura y vigila el nivel de batería de tu linterna para no quedarte totalmente a oscuras.


**¡Y la criatura también!** 


Como criatura, acecha a tus presas y cázalas sin ser vista. La luz te aturde, así que utiliza tus poderes para evitarla y provocar el caos entre los investigadores, separándolos para que sean presa fácil. Invoca totems que te avisarán de la presencia de investigadores cercanos, y evita que recojan las 8 cintas antes de que acabes con ellos!


**Vuelve de la tumba** 


En White Noise 2, la muerte no es el fin de la partida. Levántate como un espectro y aprovecha tus capacidades de exploración mejoradas para ayudar a los investigadores restantes en la búsqueda. Desbloquea nuevos objetos Desbloquea nuevos personajes y linternas a medida que juegas, incluyendo los personajes clásicos de White Noise Online. Cada personaje tiene diferentes especializaciones y atributos. ¿Prefieres un estilo de juego más sigiloso, o te centras en la velocidad aunque se te oiga a kilómetros? ¿Mejor duración de linterna, o mejorar tus habilidades de exploración? Revive los mejores momentos Por supuesto, la aclamada pantalla de repetición ha vuelto en White Noise 2. Comprueba la ruta que los jugadores siguieron. ¡Cuando la terrorífica partida termina, échate unas risas viendo cómo el equipo caminaba en círculos, o uno de los jugadores se perdía!


#### Requisitos del sistema


MÍNIMO:   
SO: Ubuntu 12.10 o superior   
Procesador: Dual Core    
Memoria: 2 GB de RAM   
Gráficos: OpenGL 2   
Almacenamiento: 1 GB de espacio disponible   
Notas adicionales: para gráficas AMD, se recomienda drivers MESA superiores a 10.1.5


RECOMENDADO:   
Procesador: Quad Core    
Memoria: 4 GB de RAM   
Gráficos: GeForce 660 o superior  
Red: Conexión de banda ancha a Internet   
Almacenamiento: 1 GB de espacio disponible


[[Página de Steam](http://store.steampowered.com/app/503350/)]


Los títulos de Milkstone Studios soportan Linux desde el lanzamiento y tienen buenas críticas. ¿Piensas jugar a este White Noise 2? ¿o el terror no te dejará dormir por la noche?


Cuéntamelo en los comentarios.

