---
author: Pato
category: Terror
date: 2016-10-28 17:56:27
excerpt: "<p>Cientos de juegos terror\xEDficos en Linux/SteamOS rebajados hasta el\
  \ d\xEDa 1 de Noviembre.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9415f9bcd76598f9c08127db1641b596.webp
joomla_id: 80
joomla_url: comienzan-las-rebajas-de-halloween-en-steam
layout: post
tags:
- steamos
- steam
- oferta
- terror
- rebajas
title: Comienzan las rebajas de Halloween en Steam
---
Cientos de juegos terroríficos en Linux/SteamOS rebajados hasta el día 1 de Noviembre.

Como ya es costumbre las rebajas de Halloween han llegado hoy y ya puedes encontrar cientos de juegos rebajados para Linux en Steam de temática de Terror. 


Como en Jugandoenlinux te queremos poner las cosas fáciles, aquí tienes el enlace directo a las ofertas filtradas por sistemas Linux/SteamOS:


<http://store.steampowered.com/search/?os=linux&specials=1>


¿Tienes pensado hacerte con algún título en especial? ¿que te parecen las ofertas que hay? ¿quieres hacernos tus propias sugerencias?


Cuéntamelo en los comentarios.

