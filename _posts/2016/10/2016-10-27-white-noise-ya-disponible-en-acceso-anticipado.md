---
author: Pato
category: Terror
date: 2016-10-27 21:57:39
excerpt: "<p>Se trata de un juego de terror asim\xE9trico 4 contra 1.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/339a0e1449b6b4062056bc300d87e893.webp
joomla_id: 77
joomla_url: white-noise-ya-disponible-en-acceso-anticipado
layout: post
tags:
- indie
- steam
- terror
- acceso-anticipado
title: White Noise 2 ya disponible en acceso anticipado
---
Se trata de un juego de terror asimétrico 4 contra 1.

Tal y como ya anunciamos hace un par de semanas ya está disponible White noise, el juego de terror de Milkstone Studios que llega a Steam en acceso anticipado. La versión Linux está disponible desde el primer día, por lo que si os gusta el tema y quereis darle una oportunidad ayudando al estudio a pulir el juego este es un buen momento.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/Px41fglWYCY" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


¿Piensas pillarlo? cuéntame lo que piensas en los comentarios.


Puedes comprar White Noise en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/503350/" width="646"></iframe></div>

