---
author: Pato
category: Plataformas
date: 2016-10-19 20:49:34
excerpt: <p>Se trata de un juego Indie que destaca por su concepto y su aspecto visual.&nbsp;</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8fe3e0f34d3083cba6fe73d62a783d7f.webp
joomla_id: 62
joomla_url: scrap-garden-un-llamativo-juego-de-plataformas-y-puzzles-ya-disponible-para-linux
layout: post
tags:
- indie
- plataformas
- steam
- puzzles
title: '''Scrap Garden'' un llamativo juego de plataformas y puzzles ya disponible
  para Linux'
---
Se trata de un juego Indie que destaca por su concepto y su aspecto visual. 

Sinopsis del juego:


Scrap Garden es un fascinante juego de plataformas y puzles que sigue las aventuras de Canny, un robot solitario que despierta en un mundo postapocalíptico en el que los demás robots han sido desconectados e inutilizados. ¿Qué ha pasado en la ciudad? ¿Por qué han dejado de moverse todos los robots? ¿Acaso hay más supervivientes? El pequeño Canny tendrá que hacer todo lo posible por encontrar respuesta a estas preguntas... e intentar restaurar el orden.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/KhjUoE7iozI" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Tienes ya disponible el juego en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/465760/" width="646"></iframe></div>

