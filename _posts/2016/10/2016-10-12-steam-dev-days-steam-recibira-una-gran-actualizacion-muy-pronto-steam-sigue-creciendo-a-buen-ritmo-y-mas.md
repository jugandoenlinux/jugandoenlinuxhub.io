---
author: Pato
category: Hardware
date: 2016-10-12 22:12:24
excerpt: "<p>Algunos de los n\xFAmeros hechos p\xFAblicos en la convenci\xF3n muestran\
  \ que el juego en PC est\xE1 en plena forma.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1c0ae2205709722b62e843abc0471a55.webp
joomla_id: 48
joomla_url: steam-dev-days-steam-recibira-una-gran-actualizacion-muy-pronto-steam-sigue-creciendo-a-buen-ritmo-y-mas
layout: post
tags:
- steam
title: "Steam Dev Days: Steam recibir\xE1 una gran actualizaci\xF3n muy pronto. Steam\
  \ sigue creciendo a buen ritmo y m\xE1s"
---
Algunos de los números hechos públicos en la convención muestran que el juego en PC está en plena forma.

Más información gracias a los desarrolladores que asisten a los Steam Dev Days en Seattle.


Según parece Steam recibirá una gran actualización en las próximas semanas para darnos nuevas características, si bien no han entrado en mas detalles:



> 
> "We are going to update Steam in a major way in a couple of weeks" [#SteamDevDays](https://twitter.com/hashtag/SteamDevDays?src=hash)
> 
> 
> — Steam Spy (@Steam_Spy) [12 de octubre de 2016](https://twitter.com/Steam_Spy/status/786259109364367364)







La última vez que tuvimos una actualización importante del cliente de Steam nos trajeron una nueva apariencia, nuevas opciones de búsqueda, los mentores, nuevos filtros, etc.


Será interesante ver que nos trae esta vez Valve.


Por otra parte, en las conferencias de hoy se están ofreciendo algunas cifras de lo mas interesantes.


En primer lugar, reconocen que actualmente ya hay alrededor de unos 10.000 juegos publicados en la plataforma. Hace bien poco nos hacíamos eco en jugandoenlinux.com de que habíamos alcanzado aproximadamente unos 2.500 juegos publicados en Linux, lo cual es significativo de que estamos creciendo.


También se han hecho públicos algunos gráficos que muestran el meteórico ascenso de este dato:


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdevdays/released.webp)


[Fuente](https://twitter.com/MimimiProd/status/786256755831349248)


Este fuerte crecimiento conlleva el problema de que con tanta competencia está siendo muy complicado para muchos estudios hacerse un hueco entre tanto lanzamiento de forma que les sea rentable. Por tanto Valve tendrá que enfocar esto de la mejor manera posible para que sigamos teniendo el nivel de lanzamientos y además seguir siendo una plataforma interesante en la que seguir publicando títulos y obtener beneficios.


Otro de los gráficos que ha visto la luz es el de los beneficios según regiones:


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdevdays/revenue.webp)


[Fuente](https://twitter.com/ShogoNu/status/786259064585990144)


La región que mas crece es la asiática con un más que impresionante porcentaje cercano al 500%. En próximas fechas no os extrañeis si veis un aluvión de títulos traducidos a idiomas de lo mas exóticos. Sin embargo lo interesante está en ver cómo en la región europea el beneficio está en torno a un 50% y en Latinoamérica el porcentaje es sustancialmente superior. Esperemos que esto nos traiga más títulos traducidos al español.


No está de más que seguimos siendo importantes. ¿Qué pensais al respecto?

