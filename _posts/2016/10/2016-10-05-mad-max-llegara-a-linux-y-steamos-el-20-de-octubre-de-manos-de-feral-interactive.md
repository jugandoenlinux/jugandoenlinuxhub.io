---
author: Pato
category: "Acci\xF3n"
date: 2016-10-05 10:56:35
excerpt: <p>Los chicos &nbsp;de Feral nos traen un nuevo port de un juego triple A.
  Oficialmente confirmado para el 20 de Octubre en Linux y SteamOS.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/077ab55046ce80eaf9a3ddea999597ca.webp
joomla_id: 35
joomla_url: mad-max-llegara-a-linux-y-steamos-el-20-de-octubre-de-manos-de-feral-interactive
layout: post
tags:
- accion
- aventura
- proximamente
title: "Mad Max llegar\xE1 a Linux y SteamOS el 20 de Octubre de manos de Feral Interactive"
---
Los chicos  de Feral nos traen un nuevo port de un juego triple A. Oficialmente confirmado para el 20 de Octubre en Linux y SteamOS.


> 
> On October 20th, enter the outlandish post-apocalyptic world of Mad Max on Mac and Linux. [pic.twitter.com/8Vx0qU9YpK](https://t.co/8Vx0qU9YpK)
> 
> 
> — Feral Interactive (@feralgames) [5 de octubre de 2016](https://twitter.com/feralgames/status/783599566121738240)







Mad Max se anunció inicialmente como desarrollo para Linux hace ya bastante tiempo. Desde Marzo de 2015 no se había vuelto a tener noticias del posible port hasta hoy que además de hacerse oficial, lo han anunciado los chicos de Feral Interactive! ¡y además tenemos el lanzamiento prácticamente encima!


De su nota de prensa:


*"Entra el próximo 20 de octubre en el estrafalario mundo postapocalítico de Mad Max en Mac y Linux.*


*Conviértete en Max, el guerrero solitario que tiene que luchar por su supervivencia contra los facciones salvajes del Páramo en combates cuerpo a cuerpo y violentos enfrentamientos con vehículos.*


*[Asalta la miniweb](https://www.feralinteractive.com/xx/games/madmax) para acceder a material valioso y no olvides leer las normas de la carretera:*


*1. Mata o muere."*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/-mXWT-f9FAU" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Desde luego los chicos de Feral Interactive deben andar de trabajo hasta arriba. En apenas un mes nos han traido Dawn of War II [[nuestra reseña](index.php/item/10-warhammer-40000-dawn-of-war-ii-chaos-rising-y-retribution-ya-disponibles-para-linux-steamos)] y ahora nos traen este Mad Max.


#### Requisitos del sistema:


CPU: Intel i5 o AMD FX8350 3.4 GHz   
8GB RAM  
SteamOS 2.0 o Ubuntu 16.04 o superior


Además el juego pide una gráfica Nvidia 660ti o mejor junto con el driver 367.35 o superior. Las tarjetas de AMD e Intel **no** están soportadas oficialmente, lo que no significa que el juego pueda funcionar pero con bajo rendimiento o algún que otro glitche.


¿Qué os parece la noticia? ¿cumplís con los requisitos? ¿pensais haceros con el juego? 


Contármelo en los comentarios

