---
author: Pato
category: "Exploraci\xF3n"
date: 2016-10-07 11:28:05
excerpt: "<p>Una buena oportunidad para adquirir este gr\xE1n t\xEDtulo a un precio\
  \ de risa</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e2bf3b11df0b872112757f1c2fee6e32.webp
joomla_id: 41
joomla_url: tomb-raider-para-linux-en-oferta-en-la-tienda-de-feral
layout: post
tags:
- oferta
title: Tomb Raider para Linux en oferta en la tienda de Feral
---
Una buena oportunidad para adquirir este grán título a un precio de risa

Para todo el que esté esperando una oferta para comprar Tomb Raider en Linux, ahora teneis la oportunidad. Los chicos de Feral Interactive están ofertando el juego en su propia tienda rebajándolo hasta los 5,99€. ¿Y que mejor forma de apoyar a nuestro sistema favorito que comprando directamente al estudio que hizo el port a Linux?


Si bien el juego tiene algunos problemas de rendimiento en momentos puntuales, con ralentizaciones y bajadas de frames, lo cierto es que se puede disfrutar perfectamente. Yo tengo mi copia y lo cierto es que no he tenido problemas con el.


Si quereis comprarlo podéis visitar la tienda de Feral en este [[enlace](https://store.feralinteractive.com/es/mac-linux-games/tombraider/)]. No os durmáis, la oferta termina el 13 de Octubre.


Puede ser un buen título para jugar un fin de semana. ¿Piensas hacerte con el? ¿qué te parece el juego?


cuéntamelo en los comentarios

