---
author: Pato
category: "Acci\xF3n"
date: 2016-10-13 15:17:04
excerpt: "<p>Estar\xE1n retransmiti\xE9ndolo mediante streaming en su canal de Twitch</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/867519228d1d5325856fc61d710ded0e.webp
joomla_id: 50
joomla_url: quieres-ver-mad-max-corriendo-en-linux-feral-te-lo-ensena
layout: post
tags:
- accion
- proximamente
- feral-interactive
title: "\xBFQuieres ver Mad Max corriendo en Linux? Feral te lo ense\xF1a"
---
Estarán retransmitiéndolo mediante streaming en su canal de Twitch

Si eres de los que esperas a Mad Max en Linux, probablemente te interesará saber que Feral retransmitirá una sesión del juego esta misma tarde a las 19:00h (horario español CEST)



> 
> In two hours from now, we step into a world gone mad — and you're coming too. [#FeralPlays](https://twitter.com/hashtag/FeralPlays?src=hash) Mad Max on Linux: <https://t.co/kbDZOaRBrq> [pic.twitter.com/9MGZLmMKAQ](https://t.co/9MGZLmMKAQ)
> 
> 
> — Feral Interactive (@feralgames) [13 de octubre de 2016](https://twitter.com/feralgames/status/786582120189734912)







Podrás verlo en su canal de Twitch: <https://www.twitch.tv/feralinteractive>


No te lo pierdas!


