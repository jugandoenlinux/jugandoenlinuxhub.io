---
author: Serjor
category: "An\xE1lisis"
date: 2016-10-16 19:04:32
excerpt: "<p>Despu\xE9s de varios retrasos Rocket League, quiz\xE1s unos de los t\xED\
  tulos para Linux m\xE1s esperados de este a\xF1o, lleg\xF3 a nuestro sistema preferido\
  \ a primeros de septiembre, pero, \xBFest\xE1 el port a la altura de las espectativas?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/36fdb1a35cd2f54f95cf2119fb5bc7ed.webp
joomla_id: 56
joomla_url: rocket-league-ha-valido-la-espera
layout: post
tags:
- accion
- deportes
- carreras
title: "Rocket League, \xBFha valido la pena la espera?"
---
Después de varios retrasos Rocket League, quizás unos de los títulos para Linux más esperados de este año, llegó a nuestro sistema preferido a primeros de septiembre, pero, ¿está el port a la altura de las espectativas?

Como os [anunciamos](index.php/item/2-rocket-league-llega-a-linux-y-steamos-en-fase-beta) en su momento, Rocket League, el titulo de [Psyonix](http://psyonix.com/), está disponible para Linux en fase beta.


Durante este tiempo el juego ha recibido unas actualizaciones y algún que otro parche que ha mejorado considerablemente el desempeño del juego, pero nos hacemos la pregunta ¿merece la pena pagar por un juego en fase beta?


Desde <jugandoenlinux.com> no podemos responder por ti, pero intentaremos ayudarte a tomar esta decisión, así que para ello pongámonos en situación:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" seamless="seamless" src="https://www.youtube.com/embed/S8U-S_TJX00" width="560"></iframe></div>


Este vídeo ha sido grabado usando Ubuntu 16.04, con 16Gb de RAM, AMD Phenom II 965, y una GTX 650ti con 1Gb de memoria gráfica, lo cuál nos obliga a hacer una pequeña aclaración. Parte de los tirones es debido al programa de grabación de vídeos, pero solo parte.


Jugar a Rocket league en linux es una cuestión de loteria. En un sistema como este al menos, puedes tener la mejor y la peor de las experiencias posibles. En la gran mayoría de los casos el juego funciona bien, y no da ningún tipo de problema, pero no han sido pocas las ocasiones donde en mitad de una partida algún fallo ha generado un CTD (close to desktop, o en castellano, cierre inesperado y vuelta al escritorio), y cuando no, el rendimiento del juego ha sido parecido al del vídeo, con tirones o con un renderizado en el que se notaba que faltaba algún tipo de efecto.


![¡Enseñame la pasta!](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/RocketLeague/20161014200924_1.webp)


No obstante, la experiencia general es positiva. El juego en red rara vez da problemas, la experiencia es fluída y en términos generales el juego se deja disfrutar, y aunque el Unreal Engine 3 sobre el que está desarrollado no tenga soporte oficial para Linux, el hecho de que sea un motor tan veterano hace que no requiera de una gran máquina, porque a pesar del vídeo que hemos insertado, cuando va bien, es raro verlo bajar de 60fps o algún tipo de problema gráfico.


![Vámonos de compras](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/RocketLeague/20161014200829_1.webp)


Así que volviendo a la pregunta que nos hacíamos al comienzo del artículo, ¿merece la pena pagar por un juego que está en fase beta? La respuesta más honesta que podemos dar es que, mientras que no puedo jugar todos los días por falta de tiempo, todos los días que juego, seguro que juego a Rocket League. Si eres una persona consecuente y cada vez que el juego se ralentiza o se cierra inesperadamente simplemente recuerdas que es un juego en fase beta, RL te puede dar muchas horas de diversión. Si lo que buscas es un juego estable, yo que tú esperaría.


Si decides hacerte con él, pásate por nuestro [foro](index.php/foro/), donde puedes coordinar la compra de un pack de 4 códigos, donde cada código sale algo más económico que comprarlo por separado.


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/252950/30948/" width="646"></iframe></div>

