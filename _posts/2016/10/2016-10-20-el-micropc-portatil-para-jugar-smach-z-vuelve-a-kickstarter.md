---
author: Pato
category: Hardware
date: 2016-10-20 17:58:40
excerpt: "<p>Esta vez la compa\xF1\xEDa espa\xF1ola SMACH se ha asegurado de llevar\
  \ a t\xE9rmino su campa\xF1a.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/542390225756f78888142d54f3d17e01.webp
joomla_id: 65
joomla_url: el-micropc-portatil-para-jugar-smach-z-vuelve-a-kickstarter
layout: post
tags:
- proximamente
- portatil
- crowdfunding
title: El microPC portatil para jugar SMACH Z vuelve a Kickstarter
---
Esta vez la compañía española SMACH se ha asegurado de llevar a término su campaña.

SMACH Z es un prometedor microPC portatil al estilo de las consolas portátiles que todos conocemos. Está aún en desarrollo según el estandar clickARM por SMACH, una compañía española que por segunda vez acude a Kickstarter para financiar el proyecto con este nombre. Anteriormente también lo habían intentado con el nombre de "SteamBoy" sin mucho éxito. Esta vez, a diferencia de la anterior campaña, donde pedían alrededor de 900.000€ (y no llegaron a finalizarla) solo piden 250.000€. En el momento de escribir estas líneas ya han conseguido el objetivo y aún les quedan 30 días más de campaña.


Lo interesante de este microPC está en el hecho de que al utilizar arquitectura estandar de PC podrá ejecutar tanto sistemas operativos como juegos compatibles de PC. En proyectos anteriores el SMACH Z se apoyaba fuertemente en SteamOS como sistema operativo con el fin de convertirse en una "Steam Machine" portatil. Sin embargo los planes ahora parecen haber cambiado y ofrecen dos opciones: o Windows 10 o una distribución Linux personalizada por ellos mismos, la SMACH Z OS.


Por otra parte, SMACH Z tendrá los paneles hápticos y los botones típicos de los Steam Controllers y podrá mover el catálogo disponible en Steam siempre dentro de sus posibilidades o haciendo "streaming" o retransmisión mediante la característica "In Home Streaming" de Steam.


Los desarrolladores también afirman que se podrán instalar juegos de otras plataformas, como GOG o Uplay (en versiones Windows) así como cualquier otro software compatible con PC.


Puedes ver el prototipo de SMACH Z en ación en el siguiente vídeo de la campaña:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/ZkmZY0xe6w8" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


El SMACH Z estará disponible en dos versiones, la versión básica que saldrá por 299€ y la versión PRO que saldrá por 499€. En la campaña se puede conseguir el paquete más básico por 279€ (existencias limitadas) o por su valor final.


Las especificaciones de los modelos finales serán:


CPU: AMD Merlin Falcon RX-421BD (12-15w) SoC at 2.1 GHz  
Núcleos: 4/4  
iGPU: Radeon R7 at 800 MHz  
RAM: 4GB (SMACH Z) || 8GB (SMACH Z PRO) DDR4 2133 MHz  
HD: 64GB (SMACH Z) || 128GB (SMACH Z PRO)  
Pantalla: 6” FULL HD (1920x1080). pantalla tactil capacitiva  
Battery: 5 horas de juego.  
Ranura MicroSD.  
USB 3.0 type C.  
Salida video HDMI.  
Wi-Fi connectivity 5.0 Ghz. 4G LTE mobile network connectivity (Solo modelo PRO)  
Bluetooth.  
Cámara frontal 1.3-megapixels (Solo modelo PRO)


Tienes toda la información de la campaña en su [página de Kickstarter](https://www.kickstarter.com/projects/smachteam/smach-z-the-handheld-gaming-pc/description).


¿Qué te parece la SMACH Z? ¿Te interesa acudir a su Kickstarter o lo ves demasiado arriesgado?


Cuéntame lo que piensas en los comentarios.

