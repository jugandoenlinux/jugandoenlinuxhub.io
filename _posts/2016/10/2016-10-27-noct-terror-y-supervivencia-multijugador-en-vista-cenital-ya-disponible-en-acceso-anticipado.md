---
author: Pato
category: Terror
date: 2016-10-27 22:27:07
excerpt: "<p>Sobrevive en un mundo apocal\xEDptico donde extra\xF1as criaturas intentar\xE1\
  n matarte. Y no solo criaturas.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/83c2446a0896df0a1f4af01c940ae1d9.webp
joomla_id: 78
joomla_url: noct-terror-y-supervivencia-multijugador-en-vista-cenital-ya-disponible-en-acceso-anticipado
layout: post
tags:
- terror
- acceso-anticipado
- supervivencia
title: '''Noct'' terror y supervivencia multijugador en vista cenital ya disponible
  en acceso anticipado'
---
Sobrevive en un mundo apocalíptico donde extrañas criaturas intentarán matarte. Y no solo criaturas.

El concepto de Noct desde luego llama la atención. Se trata de un juego en perspectiva cenital con un aspecto visual al estilo de las imágenes térmicas vía satélite que hemos visto en algunas películas. Eso y lo acertado de los movimientos y tonalidades le da un aspecto ciertamente inquietante.


Además de combatir al estilo arcade a mónstruos te encontrarás con otros jugadores a los que podrás unirte o enfrentarte. Eso si, también tendrás que tener cuidado por que también puedes traicionar y ser traicionado. 


Puedes ver lo que te ofrece Noct en su vídeo de presentación:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/JHBPeijIBiQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Puedes encontrar Noct en acceso anticipado en Steam, eso si, no está localizado al español:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/330570/" width="646"></iframe></div>

