---
author: Pato
category: Hardware
date: 2016-10-12 20:27:06
excerpt: <p>Se muestra una demo del entorno de realidad virtual corriendo en Linux.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/01f1a05053c6242fcfa23075e5b963c1.webp
joomla_id: 46
joomla_url: steam-dev-days-el-soporte-para-la-realidad-virtual-en-linux-practicamente-es-un-hecho
layout: post
tags:
- steamos
- steam
- realidad-virtual
title: "Steam Dev Days: El soporte para la Realidad Virtual en Linux pr\xE1cticamente\
  \ es un hecho"
---
Se muestra una demo del entorno de realidad virtual corriendo en Linux.

Los Steam Dev Days acaban de comenzar y mis redes sociales se han vuelto locas literalmente. Desde esta tarde la información respecto a la feria donde Valve reune a los desarolladores en Seattle ha comenzado a fluir con mucha información interesante.


Lo cierto es que desde el primer momento la cosa prometía. En un [tweet](https://twitter.com/Plagman2/status/786032888156295168), Pierre Loup uno de los máximos responsables de SteamOS en Valve mostraba una fotografía donde no dejaba lugar a dudas:


 ![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdevdays/Steamvrlinuxdemo.webp)


En la imagen se ve claramente entre mandos y Steam Machines un equipo con el letrero "Linux VR Demo". 


Pero no acaba ahí la cosa. Ya en la feria, han comenzado a surgir informaciones de que "Steam VR" corría en línux gracias a la API Vulkan:



> 
> Steam VR runs on Linux with Vulkan [#SteamDevDays](https://twitter.com/hashtag/SteamDevDays?src=hash)
> 
> 
> — Steam Spy (@Steam_Spy) [12 de octubre de 2016](https://twitter.com/Steam_Spy/status/786265057676013568)


 







Aún más significativo es el tweet de otro de los asistentes a los Steam Dev Days, donde muestra una fotografía del entorno de realidad virtual corriendo bajo Linux:



> 
> [#SteamDevDays](https://twitter.com/hashtag/SteamDevDays?src=hash) [pic.twitter.com/UlV4ZVXVzz](https://t.co/UlV4ZVXVzz)
> 
> 
> — Kevin Lee (@infinite_lee) [12 de octubre de 2016](https://twitter.com/infinite_lee/status/786265121228136448)







Blanco y en botella. Según parece, Valve y HTC han estado esperando a que los drivers en Linux estén maduros y den soporte adecuado en Vulkan para traernos la realidad virtual.


Se avecinan tiempos muy interesantes.


Los Steam Dev Days se llevan a cabo hoy y mañana. Estaremos atentos a todas las informaciones que surjan de allí.

