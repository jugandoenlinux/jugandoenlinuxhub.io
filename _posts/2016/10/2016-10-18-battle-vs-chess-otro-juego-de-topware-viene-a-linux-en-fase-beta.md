---
author: Pato
category: Estrategia
date: 2016-10-18 19:59:36
excerpt: "<p>La compa\xF1\xEDa sigue portando sus juegos ant\xEDguos utilizando Wine.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8b6e33345ac8d5ffd9cf0d107a7d9e9d.webp
joomla_id: 61
joomla_url: battle-vs-chess-otro-juego-de-topware-viene-a-linux-en-fase-beta
layout: post
tags:
- steam
- estrategia
- beta
title: '''Battle Vs Chess'', otro juego de Topware viene a Linux en fase beta'
---
La compañía sigue portando sus juegos antíguos utilizando Wine.

Topware Interactive ha publicado una versión beta de su juego 'Battle Vs Chess' para Linux en Steam que al igual que sus otros títulos como [Jagged Alliance 2 - Wildfire](index.php/item/37-jagged-alliance-2-wildfire-esta-en-beta-para-linux-en-steam) que reseñamos ayer utiliza Wine para el port, cosa que realmente está muy bien.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/VXQtTeihG5g" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Tienes toda la información sobre la beta en [este enlace](http://steamcommunity.com/games/211050/announcements/detail/834681361294427130)  



 


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/211050/" width="646"></iframe></div>

