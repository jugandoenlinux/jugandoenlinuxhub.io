---
author: Pato
category: Apt-Get Update
date: 2016-12-02 18:37:11
excerpt: <p>Repasamos todas las novedades que nos dejamos en el tintero esta semana.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1379b5de7cb668c186ab48a9361eabd7.webp
joomla_id: 136
joomla_url: apt-get-update-gray-destiny-inner-chains-master-of-orion-unity-5-5
layout: post
title: Apt-Get Update Gray Destiny & Inner Chains & Master of Orion, Unity 5.5...
---
Repasamos todas las novedades que nos dejamos en el tintero esta semana.

Nuevo viernes, nuevo Apt-Get Update para estar informados en todas esas novedades que no nos da tiempo a cubrir. ¿Comenzamos?


 


- Gracias a los chicos de [linuxgamenews.com](http://linuxgamenews.com/post/153919342613/inner-chains-reveals-first-8-minutes-of-gameplay#.WEEucHXhA8o) nos enteramos que [Inner Chains](http://innerchainsgame.com/)[web oficial], un juego de terror muy prometedor sigue su desarrollo y nos muestra 8 minutos de su jugabilidad. Llegará en el primer cuarto del próximo año.


- También en [linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/gray-destiny-3d-horror-escape-game-coming-2017-42168/) nos encontramos con otro prometedor juego llamado Gray Destiny [[web oficial](http://visualpath.net/projects/gray-destiny/index.html)], otro juego de terror donde tendrás que resolver puzzles y (re)correr mucho. Actualmente tienen en marcha [una campaña de crowdfunding en su propia web](http://visualpath.net/projects/gray-destiny/donate.html). También está pevista su llegada a Linux en el primer cuarto del próximo año.


- Seguimos con juegos de temática de Terror, esta vez en [gamingonlinux.com Liam nos cuenta](https://www.gamingonlinux.com/articles/drift-into-eternity-a-single-player-sci-fi-survival-experience-is-coming-to-linux.8640) que Drift into Eternity [[web oficial](http://www.wearebots.fr/drift-into-eternity), [Steam](http://store.steampowered.com/app/431830)] vendrá a Linux próximamente. En esta aventura para un solo jugador tendrás que sobrevivir en una nave espacial que, digamos no funciona muy bien.


- Haciendo nuestro repaso habitual de [juegos que llegarán próximamente a Linux en Steam](http://store.steampowered.com/search/?os=linux&filter=comingsoon) encontramos el juego ASURA [[web oficial](http://www.asurathegame.com/)] juego de rol "Hack&Slash" que tiene una pinta estupenda. Llegará también en el primer cuarto del próximo año como podeis ver en su [web de Steam](http://store.steampowered.com/app/524640).


- Volvemos con Liam en [gamingonlinux.com](https://www.gamingonlinux.com/articles/unity-55-released-removes-legacy-opengl-support.8627) donde encontramos la noticia de que ya está disponible Unity 5.5 y que deja de dar soporte a las versiones más antiguas de OpenGL para centrarse en las más modernas.


- También gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/the-new-master-of-orion-has-a-major-update-inspired-by-the-original-moo2-and-a-new-dlc.8639) nos enteramos que Master of Orion pasa a la ofensiva y lanza una actualización y un DLC que acerca al juego a su antecesor Master of Orion 2.


Para terminar, os dejamos el vídeo del trailer del juego Gray Destiny:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/0x16D26AuXg" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


La semana que viene más y mejor en otro apt-get update.

