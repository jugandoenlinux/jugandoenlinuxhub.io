---
author: leillo1975
category: "Simulaci\xF3n"
date: 2016-12-05 14:56:19
excerpt: "<p>SCS Software edita una nueva expansi\xF3n que nos adentrar\xE1 en territorio\
  \ Franc\xE9s</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/78a38d90a5f5af5857b8e93fa4dd5a84.webp
joomla_id: 139
joomla_url: nueva-dlc-para-euro-truck-simulator-vive-la-france
layout: post
tags:
- ets2
- dlc
title: Nueva DLC para Euro Truck Simulator, Vive la France!
---
SCS Software edita una nueva expansión que nos adentrará en territorio Francés

En el día de hoy ha sido lanzada oficialmente una nueva expansión para Euro Truck Simulator 2. En esta ocasión el territorio añadido al mapa vuelve a crecer, pero en este caso en dirección suroeste. En anteriores ocasiones creció hacia el este (Going East) donde podíamos viajar a Polonia, República Checa, Eslovaquia y Hungría; más tarde hacía el norte (Scandinavia) visitando Dinamarca, Noruega y Suecia. Ahora los chicos de [SCS Software](http://www.scssoft.com/) han profundizado en el mapa de Francia, ya que con el juego original solo abarcaba la zona este del pais galo.


Como siempre, según se aprecia en videos y fotos, se ve un trabajo excelente de modelado, como suele ser norma de la casa. En este caso podemos disfrutar de 15 nuevas ciudades francesas, 20000 nuevos Kilometros de nuevas carreteras y autopistas, lugares facilmente reconocibles de la geografía francesa, nuevas compañías de transporte, nuevos peajes y nueva vegetación y ecosistemas, [entre otras novedades](http://www.eurotrucksimulator2.com/buy.php#vivelafrance). 


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/xm3T-3IzigM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 Si quieres comprar Vive la France! DLC para ETS2 puedes hacerlo en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/531130/" width="646"></iframe></div>


 


Fuentes: [Steam](http://store.steampowered.com/app/531130), [SCS Software](http://www.scssoft.com/), [Youtube](https://www.youtube.com/watch?v=xm3T-3IzigM), [eurotrucksimulator2.com](http://www.eurotrucksimulator2.com/buy.php#vivelafrance)

