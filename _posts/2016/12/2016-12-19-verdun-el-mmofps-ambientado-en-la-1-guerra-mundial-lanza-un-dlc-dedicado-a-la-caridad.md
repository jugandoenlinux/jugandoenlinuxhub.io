---
author: Pato
category: "Acci\xF3n"
date: 2016-12-19 16:51:33
excerpt: "<p>El juego que est\xE1 disponible para Linux/SteamOS donar\xE1 todo lo\
  \ recaudado a la asociaci\xF3n \"War Child UK\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/935dbd09c0a7727e2143877810820513.webp
joomla_id: 155
joomla_url: verdun-el-mmofps-ambientado-en-la-1-guerra-mundial-lanza-un-dlc-dedicado-a-la-caridad
layout: post
tags:
- accion
- steamos
- primera-persona
- steam
- dlc
title: "'Verdun' el MMOFPS ambientado en la 1\xAA Guerra Mundial lanza un DLC dedicado\
  \ a la caridad"
---
El juego que está disponible para Linux/SteamOS donará todo lo recaudado a la asociación "War Child UK"

Verdun [[web oficial](https://www.verdungame.com/)] es un juego de acción multijugador online en primera persona centrado en las batallas de la 1ª Guerra Mundial que acaba de recibir un DLC llamado "Christmas Truce - War Child". Así nos lo han anunciado en un tweet:



> 
> Verdun Christmas Truce DLC Supports Charity - <https://t.co/QBTOFYya3F> | [#gaming](https://twitter.com/hashtag/gaming?src=hash) [#Verdun](https://twitter.com/hashtag/Verdun?src=hash) [#WarChildArmistice](https://twitter.com/hashtag/WarChildArmistice?src=hash) [pic.twitter.com/5s7Z5iY76K](https://t.co/5s7Z5iY76K)
> 
> 
> — TechRaptor (@TechRaptr) [18 de diciembre de 2016](https://twitter.com/TechRaptr/status/810274214762737664)







Se trata de un DLC muy especial, puesto que todo lo recaudado por el DLC irá destinado a una organización de ayuda a niños víctimas de las guerras. Está basado en la historia real ocurrida durante la Gran Guerra cuando en la Navidad de 1914 tropas de ambos bandos hicieron una tregua para pasar esta festividad tan especial en paz.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/oPj5-J_6mQY" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Tienes disponible este DLC tan especial en su web de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/439480/" width="646"></iframe></div>


Aquí te ponemos la opción más económica, pero hay tres opciones a elegir con distintas cantidades. Puedes verlas en [este enlace](http://store.steampowered.com/app/439480/?snr=1_5_1100__1100).

