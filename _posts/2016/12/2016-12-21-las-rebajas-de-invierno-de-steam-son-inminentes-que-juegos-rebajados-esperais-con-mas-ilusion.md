---
author: Pato
category: Editorial
date: 2016-12-21 18:24:14
excerpt: "<p>Ma\xF1ana comienza la Navidad para los jugadores de PC. \xBFqu\xE9 \"\
  regalos\" en forma de rebajas esper\xE1is de Pap\xE1 Gabe?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ef8bd3de49a270d49d40baf602fbfad5.webp
joomla_id: 161
joomla_url: las-rebajas-de-invierno-de-steam-son-inminentes-que-juegos-rebajados-esperais-con-mas-ilusion
layout: post
tags:
- steamos
- steam
title: "Las rebajas de invierno de Steam son inminentes. \xBFQu\xE9 juegos rebajados\
  \ esper\xE1is con m\xE1s ilusi\xF3n?"
---
Mañana comienza la Navidad para los jugadores de PC. ¿qué "regalos" en forma de rebajas esperáis de Papá Gabe?

Tal y como ya anunciamos en Jugando En Linux se sabe que las rebajas de invierno de Steam llegarán mañana 22 de Diciembre gracias a una filtración. ¿Tenéis preparadas las carteras?


Yo por mi parte os voy a contar algunos (solo algunos, ¿eh?) de los juegos que espero ver rebajados con más ilusión. Los pongo como me vienen a la cabeza, tal cual. No hay un orden determinado. Ahí van:


**Aragami**


![aragami banner](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/aragami_banner.webp)


El juego de los españoles Lince Works está en mi lista y no tardará mucho en caer. A poco que se ponga a tiro no lo pienso dejar escapar.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/280160/" width="646"></iframe></div>


 


**Darkest Dungeon**


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DarkestDungeon.webp)


Hace mucho que le tengo ganas, y creo que le echaré el guante sin pensar. 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/262060/33877/" width="646"></iframe></div>


 


**Salt and Sanctuary**


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SaltandSanctuaryBanner.webp)


Si Darkest Dungeon es el "Dark Souls" de la estrategia por turnos, Salt and Sanctuary es el "Dark Souls" de la acción 2D. En cuanto lo vea rebajado este se viene a casa.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/283640/" width="646"></iframe></div>


 


**Marooners**


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Marooners_header.webp)


El juego es ideal para pasar una tarde en familia pasando el rato. Este cae para echarnos unas risas en el salón de casa.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/423810/" width="646"></iframe></div>


 


**Motorsport Manager**


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Motorsportmanagbanner.webp)


Todo el que conozco que lo tiene me habla maravillas de este juego. Además me gusta el mundillo de la F1 así que le tengo unas ganas tremendas.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/415200/" width="646"></iframe></div>


 


**Slain: Back from Hell**


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SlainBackfromhell.webp)


 Otro juego al que después del lavado de cara que le hicieron tras un lanzamiento algo desastroso le tengo unas ganas tremendas. Soy un ferviente admirador de la saga Castlevania por lo que este me recuerda mucho a aquellos juegos de la SuperNes. Caerá a poco que se ponga a tiro.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/369070/" width="646"></iframe></div>


 


Estos son solo algunos de los juegos de los que estaré atento para ver si cae una buena oferta. ¿Me cuentas en los comentarios cuales estás esperando tu con más ganas?

