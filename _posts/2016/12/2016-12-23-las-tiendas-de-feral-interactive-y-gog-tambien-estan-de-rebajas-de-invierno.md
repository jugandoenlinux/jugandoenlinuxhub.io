---
author: Pato
category: Software
date: 2016-12-23 19:45:13
excerpt: "<p>\xA1Aprovechad insensatos!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fd57315048b2a0e2ee02ed04b0927842.webp
joomla_id: 165
joomla_url: las-tiendas-de-feral-interactive-y-gog-tambien-estan-de-rebajas-de-invierno
layout: post
tags:
- gog
- oferta
- feral-interactive
- rebajas
title: "Las tiendas de Feral Interactive y GOG tambi\xE9n est\xE1n de rebajas de invierno"
---
¡Aprovechad insensatos!

Las ofertas de Steam eclipsan casi todo, pero junto a la tienda de Valve queda hueco para las ofertas navideñas de las tiendas de Feral y GOG. Así nos lo recuerdan en respectivos Tweets:



> 
> This Christmas, faithful friends get big discounts on an immense variety of games from the Feral Store: <https://t.co/xxE0rcyrXF> [pic.twitter.com/6yK5HquYqV](https://t.co/6yK5HquYqV)
> 
> 
> — Feral Interactive (@feralgames) [23 de diciembre de 2016](https://twitter.com/feralgames/status/812259204719329280)








> 
> Let's wave Goodbuy to 2016 & celebrate the holidays with tonnes of great deals in the last [#sale](https://twitter.com/hashtag/sale?src=hash) of the year! <https://t.co/P0ns5LA24l> [pic.twitter.com/HCEE7BZFnX](https://t.co/HCEE7BZFnX)
> 
> 
> — GOG.com (@GOGcom) [23 de diciembre de 2016](https://twitter.com/GOGcom/status/812252730173587456)







Así que si consideras que las ofertas de Steam no te ofrecen lo suficiente, o no tiene lo que andas buscando a lo mejor puedes encontrarlo en estas otras tiendas, y en el caso de Feral apoyarás diréctamente a los responsables de algunos de los mejores ports que nos han llegado a Linux. Ya sabes:


<https://store.feralinteractive.com/es/offers/>


<https://www.gog.com/>


¿Vas a comprarte algo en estas tiendas?


Cuéntamelo en los comentarios.

