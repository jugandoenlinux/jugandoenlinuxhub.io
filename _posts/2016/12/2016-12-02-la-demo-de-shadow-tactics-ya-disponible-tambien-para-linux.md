---
author: Serjor
category: "Acci\xF3n"
date: 2016-12-02 16:26:16
excerpt: <p>Demo para Linux ya disponible</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e67ec824afbc9f855ad850f1b49c5b05.webp
joomla_id: 138
joomla_url: la-demo-de-shadow-tactics-ya-disponible-tambien-para-linux
layout: post
tags:
- demo
- shadow-tactics
title: "La demo de Shadow Tactics ya disponible tambi\xE9n para Linux"
---
Demo para Linux ya disponible

Según acaban de anunciar en [Steam](http://steamcommunity.com/games/shadowtactics/announcements/detail/585862253590190026), la demo de Shadow Tactics ya está disponible para Linux.


Os recordamos que el juego debutará el próximo 6 de diciembre, así que parece un buen momento para probarlo y saber si tenemos que ir preparando la cartera.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/ZZDEg8R0g7c" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/418240/" width="646"></iframe></div>

