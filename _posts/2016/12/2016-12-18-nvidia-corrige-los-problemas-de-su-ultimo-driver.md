---
author: leillo1975
category: Hardware
date: 2016-12-18 15:23:04
excerpt: "<p>Es lanzada la versi\xF3n 375.26 del driver privativo.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/28b8f12309e9ac4afaade20e5d04ffc2.webp
joomla_id: 153
joomla_url: nvidia-corrige-los-problemas-de-su-ultimo-driver
layout: post
tags:
- drivers
- nvidia
title: "Nvidia corrige los problemas de su \xFAltimo driver"
---
Es lanzada la versión 375.26 del driver privativo.

Hace un mes exactamente [Nvidia lanzó la versión estable](index.php/item/103-nuevo-driver-nvidia-375-20-estable-lanzado) de su último driver para GNU/Linux, que en este caso se correspondía con la **versión 375.20**, cuya principal ventaja entre otras era que se incrementaba el tamaño máximo de la cache para shaders de 64 a 128MB, con lo que los tiempos de carga en determinados juegos se reducía.


A los pocos días, Nvidia recomendaba a todos los usuarios que experimentaran problemas gráficos que hiciesen un downgrade a una versión anterior del driver debido a [la inestabilidad de este](index.php/item/227-problemas-con-el-ultimo-driver-estable-de-nvidia-375-20). Yo, personalmente, pude dar buena cuenta de ello, ya que al jugar a **Deus Ex: Mankind Divided**, si bien las cargas eran sensiblemente más rápidas; el juego tenía parones que obiligaban a "matar" el proceso del juego y salidas inexperadas al escritorio.


El pasado día 14, Nvidia sacó a la luz la versión 375.26, la cual corrige esos problemas, entre otros. En mi caso decidí esperar como siempre a que se lanzara en el [PPA oficial de Ubuntu](https://launchpad.net/~graphics-drivers/+archive/ubuntu/ppa) simplemente por la facilidad de instalación. He estado testeandolo estos días y he podido jugar horas y horas a DeusEX sin ningún tipo de problema, y con las cargas rápidas. Visto que ahora todo parece funcionar correctamente me he decidido a publicar este post.


También hay que decir que Nvidia ha sacado versiones nuevas de sus drivers [340](http://www.nvidia.com/download/driverResults.aspx/112998/en-us)(.101, de la serie 8000 a 700 y 800M) y [304](http://www.nvidia.com/download/driverResults.aspx/112990/en-us)(.134, de la serie 6000 a la 600 ) para tarjetas más antiguas, en las que basicamente se ha añadido **soporte para el Xorg 1.19.**

