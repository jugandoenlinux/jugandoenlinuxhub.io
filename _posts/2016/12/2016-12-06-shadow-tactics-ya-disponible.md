---
author: Serjor
category: Estrategia
date: 2016-12-06 17:52:28
excerpt: "<p>El juego debuta con soporte para Linux desde el primer d\xEDa</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fedea746cd0ecb257a1249d3a2a80bb1.webp
joomla_id: 142
joomla_url: shadow-tactics-ya-disponible
layout: post
tags:
- shadow-tactics
title: Shadow Tactics ya disponible
---
El juego debuta con soporte para Linux desde el primer día

Como ya os [adelantábamos](index.php/item/238-la-demo-de-shadow-tactics-ya-disponible-tambien-para-linux), Shadow Tactics se lanza hoy 6 de diciembre tanto para Linux, Mac y Windows, lo hace con  una rebaja del 10% respecto a su precio final, y lo podéis encontrar tanto en [Steam](http://store.steampowered.com/app/418240/) como en [GOG](https://www.gog.com/game/shadow_tactics_blades_of_the_shogun).


Si os gustan los juegos tipo Commandos o Desperados, no dudéis ni un segundo en haceros con el juego, aunque siempre podéis probar la beta antes, la cuál funciona bastante bien en un equipo modesto, aunque eso sí, los tiempos de carga son bastante elevados (cosa que ya advierten los propios desarrolladores).


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/BPuPywdVAeg" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/418240/" width="646"></iframe></div>


 

