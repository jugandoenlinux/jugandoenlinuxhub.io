---
author: Pato
category: Puzzles
date: 2016-12-05 17:14:08
excerpt: "<p>Est\xE1 disponible con un 20% de descuento por su lanzamiento.&nbsp;</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/269b36e876e375e05083f78293992209.webp
joomla_id: 140
joomla_url: lara-croft-go-el-juego-de-rompecabezas-basado-en-la-protagonista-de-tomb-raider-ya-disponible-en-linux-steamos
layout: post
tags:
- aventura
- steam
- puzzles
- rompecabezas
title: '''Lara Croft GO'' el juego de rompecabezas basado en la protagonista de Tomb
  Raider ya disponible en Linux/SteamOS'
---
Está disponible con un 20% de descuento por su lanzamiento. 

Lara Croft GO [[web oficial](http://www.laracroftgo.com/)] es otro título de Square Enix Montreal lanzado ayer mismo del mismo estilo que (el también disponible en Linux) [Hitman GO](http://store.steampowered.com/app/427820/) que está cosechando vastante buenas críticas. Tal y como nos avisaron desde Steam:



> 
> Now Available on Steam - Lara Croft GO, 20% off! [#SteamNewRelease](https://twitter.com/hashtag/SteamNewRelease?src=hash) <https://t.co/v96msPnMS6> [pic.twitter.com/J12b1xacWN](https://t.co/J12b1xacWN)
> 
> 
> — Steam (@steam_games) [4 de diciembre de 2016](https://twitter.com/steam_games/status/805324089619574784)







El juego es un remozado rompecabezas en el que tendrás que jugar tu estrategia por turnos para conseguir tus objetivos.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/wQptvhwRa5A" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


#### Sinopsis del juego:


*Lara Croft GO es un juego de aventuras y rompecabezas basado en turnos que transcurre en un mundo olvidado. Explora las ruinas de una antigua civilización, descubre secretos bien guardados y enfréntate a desafíos letales a medida que revelas el mito de la Reina del Veneno.*


Tienes ya disponible 'Lara Croft GO' completamente en español en su web de Steam:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/540840/" width="646"></iframe></div>


¿Que te parecen este tipo de juegos rompecabezas? ¿Piensas jugar 'Lara Croft GO'?


Cuéntamelo en los comentarios.


¿Tienes Lara Croft GO y quieres que publiquemos tu review? ¡[Mándanosla](index.php/contacto/envia-un-articulo)!

