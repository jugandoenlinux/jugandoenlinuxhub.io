---
author: leillo1975
category: "Simulaci\xF3n"
date: 2016-12-12 16:03:22
excerpt: "<p>El mapa del juego ha sido reescalado en su \xFAltima actualizaci\xF3\
  n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0bfc0ce99892772fc285e10ee3943d9a.webp
joomla_id: 148
joomla_url: american-truck-simulator-crece-con-la-actualizacion-1-5
layout: post
tags:
- american-truck-simulator
- reescalado
- ats
title: "American Truck Simulator crece con la actualizaci\xF3n 1.5"
---
El mapa del juego ha sido reescalado en su última actualización

 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/rNcWEk-ROzM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Estos chicos de [SCS Software](http://blog.scssoft.com/2016/12/ats-world-rescale-is-released-in-update.html) están que no paran. Si bien el otro día nos sorprendían con la [DLC Vive la france!](index.php/item/243-analisis-ets2-vive-la-france-dlc) para Euro Truck Simulator 2, esta semana le toca en forma de actualización a su hermano "pequeño", **American Truck Simulator.**  La principal caracteristica que ofrece esta anovación, [entre muchas otras](http://blog.scssoft.com/2016/12/ats-world-rescale-is-released-in-update.html) es el reescalado del mapa, que anteriormente **tenía una proporción de 1:35 y pasa a ser de 1:20**, con lo que crece sustancialmente sobre un 75%. Como comentan los chicos de SCSSoft, este proyecto le ha llevado sobre 6 meses, lo cual no me extraña absolutamente nada viendo la magnitud del proyecto.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AMSreescalado_mapa.webp)*En esta imagen se puede apreciar como ha crecido el mapa*



Además de este notable incremento de tamaño también se ha añadido una **nueva ciudad en California, Santa María**, y la de **Oxnard ha sido completamente remodelada**. No solo se han "alargado" las carreteras, sino que para esta ocasión se han creado algunas nuevas y remodelado bastantes, se han hecho multitud de nuevas y más grandes interconexiones, areas de descanso, y también hay más lugares famosos reconocibles.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATSreescaladosecciones.webp)*Aquí podemos ver las nuevas secciones que se han añadido al mapa*



Personalmente llevo algunas semanas probando en la versión beta de Steam estas nuevas caracteristicas y he de decir que realmente si se notan y mucho. Ahora realmente si sientes las distancias más que antes. Recuerdo que antes llegar por ejemplo de San Francisco a Las Vegas se hacía en un periquete, lo cual no era muy real, ya que la distancia es de unas 600 millas (o unos 900 kilómetros aproximadamente). Ahora mismo cuesta bastante más llegar que antes, lo cual si tenemos en cuenta que es un juego de simulación, se agradece mucho. Otra cosa que notas a la primera es que por ejemplo los nudos o intersecciones de carreteras están muchísimo más trabajados, encontrándote con montones de pistas que se entrecruzan entre si para llegar a las diferentes localizaciones del mapa. Si bien el trabajo de modelado en ATS ya era muy detallado, ahora con todas estas mejoras se puede decir que es de sobresaliente, acrecentando aun si cabe, mucho más, la sensación de estar conduciendo por el lejano oeste.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AMSreescalado.webp "http://blog.scssoft.com")


 


Sin más, os conmino a que os dispongais a descargar esta actualización lo antes posible, ya que lo vais a agradecer. También me gustaría agradecer a SCSSoft el trabajo que realizan con sus juegos, ofreciendo siempre mejoras constantes en sus productos, y en este caso sobre todo, al tratarse de una actualización gratuita. Si no teneis este juego, ahora teneis una razón más para adquirirlo, ya que con él podreis circular por 3 enormes estados (California, Nevada y Arizona), y con esta actualización mucho más grandes aun, si cabe.


Podeis adquirir este juego en Steam:


 <div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/270880/" width="646"></iframe></div>


 


Fuentes: [SCS Software Blog](http://blog.scssoft.com/), [Steam](https://steamcommunity.com/app/270880/allnews/)

