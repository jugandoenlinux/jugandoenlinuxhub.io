---
author: Pato
category: "Exploraci\xF3n"
date: 2016-09-14 16:29:08
excerpt: "<p>Event[0] es un juego de exploraci\xF3n y tem\xE1tica espacial donde habr\xE1\
  \ que relacionarse con una computadora para poder volver a la tierra.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ffee2447b152494b43d9816faaea83c8.webp
joomla_id: 23
joomla_url: event-0-el-juego-de-narrativa-y-exploracion-espacial-esta-en-desarrollo-para-linux
layout: post
tags:
- espacio
- aventura
- proximamente
- primera-persona
title: "Event[0], el juego de narrativa y exploraci\xF3n espacial est\xE1 en desarrollo\
  \ para Linux"
---
Event[0] es un juego de exploración y temática espacial donde habrá que relacionarse con una computadora para poder volver a la tierra.

 


El juego es gráficamente muy vistoso y según han declarado al portal [www.gamingonlinux.com](https://www.gamingonlinux.com/articles/event0-a-sci-fi-narrative-exploration-game-is-coming-to-linux-befriend-a-spaceship-computer-to-get-home-to-earth.8083) están trabajando en una versión para Linux, si bien no pueden dar una fecha estimada de lanzamiento.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/_2wOweBRcHo" width="640"></iframe></div>


La [página oficial de Event[0]](http://event0game.com/)

