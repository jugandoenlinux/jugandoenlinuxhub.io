---
author: Pato
category: Plataformas
date: 2016-09-16 17:45:24
excerpt: "<p>Seg\xFAn el desarrollador el juego podr\xEDa ser portado gracias a la\
  \ tecnolog\xEDa FNA.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9caa2793658f3cc387f216157300b1ce.webp
joomla_id: 25
joomla_url: owlboy-el-juego-de-aventuras-2d-puede-llegar-a-linux-proximamente
layout: post
tags:
- accion
- plataformas
- aventura
title: "Owlboy, el juego de aventuras 2D puede llegar a Linux pr\xF3ximamente"
---
Según el desarrollador el juego podría ser portado gracias a la tecnología FNA.

 


Aunque aún están en fase de pruebas, han confimado en un tweet que el port tiene buena pinta y podría ver la luz en un futuro.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/p9VwGaycmCQ" width="640"></iframe></div>


 


#### Información sobre el juego:


Explora un mundo vibrante hecho del pixeles en esta aventura voladora.   
  
A ser mudo, Otus encuentra dificultades para estar a la altura de las expectativas de la comunidad buhistica.   
  
Las cosas van de mal en peor cuando repentinamente aparece un cielo de piratas.   
  
Lo que sigue es un viaje a través de ruinas infestadas de monstruos, con encuentros inesperados y cargas que nadie debería soportar. 

