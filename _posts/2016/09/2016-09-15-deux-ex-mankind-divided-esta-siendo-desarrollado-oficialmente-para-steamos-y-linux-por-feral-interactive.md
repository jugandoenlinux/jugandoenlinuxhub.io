---
author: Pato
category: "Acci\xF3n"
date: 2016-09-15 16:57:16
excerpt: "<p>Otro gr\xE1n t\xEDtulo se viene a Linux de manos de los maestros del\
  \ Port.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ada9a09acea936d776a6f55c82778c43.webp
joomla_id: 24
joomla_url: deux-ex-mankind-divided-esta-siendo-desarrollado-oficialmente-para-steamos-y-linux-por-feral-interactive
layout: post
tags:
- accion
- proximamente
title: "Deux Ex: Mankind Divided est\xE1 siendo desarrollado oficialmente para SteamOS\
  \ y Linux por Feral Interactive"
---
Otro grán título se viene a Linux de manos de los maestros del Port.

 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DXMD_Mac___Linux.webp)


A los chicos de Feral Interactive deveríamos hacerles un monumento. Tras traernos juegos como Tomb Raider, Xcom 2 o Grid Autosport ahora nos anuncian que Deus Ex Mankind Divided está en desarrollo para Linux!



> 
> You can't kill progress – we can confirm that Deus Ex: Mankind Divided is coming to Mac using Metal, Apple’s new graphics API. [pic.twitter.com/BS6uDsCgew](https://t.co/BS6uDsCgew)
> 
> 
> — Feral Interactive (@feralgames) [22 de septiembre de 2016](https://twitter.com/feralgames/status/778896521123229697)







Han confirmado que el juego saldrá este año, aunque la fecha aún no se ha hecho pública. Las especificaciones recomendadas se publicarán cuando estemos mas cerca de la fecha de lanzamiento.


Enlace a la miniweb:


<https://www.feralinteractive.com/es/games/deusexmd/>


##### Info del juego:


Corre el año 2029 y los humanos aumentados mecánicamente han sido relegados al estatus de parias que viven totalmente marginados y aislados del resto de la sociedad.   
  
Adam Jensen es ahora un experimentado agente encubierto en un mundo que ha dado en despreciar a los de su clase. Equipado con un nuevo arsenal de armas y aumentos de última generación, Adam tendrá que escoger bien la estrategia adecuada y en quién confiar para destapar una conspiración a escala global.

