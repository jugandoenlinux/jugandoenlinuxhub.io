---
author: Pato
category: Rol
date: 2016-09-19 18:16:36
excerpt: "<p>El juego de King Art est\xE1 en fase Beta y la versi\xF3n de Linux est\xE1\
  \ disponible.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d61d44254608dd06ccdd2ff02982d14d.webp
joomla_id: 27
joomla_url: the-dwarves-un-juego-de-rol-y-fantasia-llega-a-linux-steamos-el-dia-1-de-diciembre
layout: post
tags:
- proximamente
- fantasia
- rol
title: "The Dwarves, un juego de rol y fantas\xEDa llega a Linux/SteamOS el d\xED\
  a 1 de Diciembre"
---
El juego de King Art está en fase Beta y la versión de Linux está disponible.

 


El desarrollador ha confirmado el lanzamiento del juego para el día 1 de Diciembre lo que junto a la beta de Linux hace pensar que el lanzamiento será simultáneo para los dos sistemas.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/rc3Y1hKdHL8" width="640"></iframe></div>


The Dwarves es un juego de rol fantástico con una profunda historia donde las batallas tácticas en tiempo real tendrán un gran protagonismo. Tendrás a tu disposición 15 héroes con sus diferentes características que podrás desarrollar.


Basado en la novela de Markus Heitz, en 'The Dwarves' te sumergirás en una historia acerca de esta raza fantástica  


