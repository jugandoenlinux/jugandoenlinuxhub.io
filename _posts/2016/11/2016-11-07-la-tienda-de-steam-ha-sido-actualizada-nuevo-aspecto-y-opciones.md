---
author: Pato
category: Software
date: 2016-11-07 23:17:00
excerpt: <p>Sin embargo, Valve sigue sin filtrar bien por defecto los juegos por plataforma
  en el "frontpage".</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d48ed900e79fa9547169c26138b4cd8d.webp
joomla_id: 93
joomla_url: la-tienda-de-steam-ha-sido-actualizada-nuevo-aspecto-y-opciones
layout: post
tags:
- steam
- editorial
title: 'La tienda de Steam ha sido actualizada: Nuevo aspecto y opciones'
---
Sin embargo, Valve sigue sin filtrar bien por defecto los juegos por plataforma en el "frontpage".

Desde hace ya un tiempo que [sabemos](item/31-steam-dev-days-valve-espera-haber-vendido-1-000-000-de-steam-controllers-para-principios-del-proximo-ano-y-dar-soporte-a-otros-mandos-en-steam) que Valve iba a actualizar su tienda Steam, y esta noche por fin han liberado la actualización. El resultado es una tienda remozada que sin embargo aún parece tener algunos problemas a la hora de filtrar los contenidos por plataforma. 


En la cabecera de la tienda, si situamos el ratón en la sección deslizante de artículos recomendados aparece un pequeño icono que nos permite cambiar la configuración de cómo queremos que Steam nos muestre la tienda:


![steam2 7](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SteamStoreNew/steam2-7.webp)


Una vez configurado seleccionando "solo" mostrar juegos en SteamOS+Linux, guardamos y volvemos a cargar la tienda, y tras varios minutos aún nos muestra juegos recomendados para otros sistemas.


![steam2.8](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SteamStoreNew/steam2.8.webp)


Dejando de lado el filtrado, la nueva tienda tiene nuevas opciones y características. Ahora existe una barra lateral donde puedes seleccionar opciones de exploración, listas y categorías, más visitados etc.


También han hecho otras listas mas atractivas y claras  como filtrar los juegos que ya posees o los que has marcado como que no te interesan.


Si quieres ver todas las novedades de la tienda de Steam puedes verlas en [este enlace](http://store.steampowered.com/about/newstore2016/).


¿Que te parecen estos cambios en la tienda de Steam?


Cuéntamelo en los comentarios.

