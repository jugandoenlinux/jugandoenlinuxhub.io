---
author: Serjor
category: "Simulaci\xF3n"
date: 2016-11-30 23:20:54
excerpt: "Salva tu planeta de ser destru\xEDdo manteniendo en funcionamiento los sat\xE9\
  lites que lo protegen"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8e5f062e9750688c028aaa3058da9ec4.webp
joomla_id: 133
joomla_url: satellite-repairman-un-juego-hecho-en-godot-y-con-soporte-para-linux-que-busca-ayuda-en-greenlight
layout: post
title: Satellite Repairman, un juego hecho en godot y con soporte para Linux, que
  busca ayuda en greenlight
---
Salva tu planeta de ser destruído manteniendo en funcionamiento los satélites que lo protegenVía [reddit](https://www.reddit.com/r/linux_gaming/comments/5fhx08/im_working_on_a_linux_game_and_was_told_to_post/?ref=share&ref_source=link) nos enteramos de que [Satellite Repairman](http://www.satelliterepairman.com/), un interesante building simulator desarrollado con el motor open source [Godot](https://godotengine.org/), busca votos en greenlight.


En esta ocasión encarnaremos a un técnico de satélites encargado de reparar, construir, actualizar y mejorar la red de satélites que protegen tu planeta después de que todos los planetas del sistema hayan decidido atacarse entre ellos.


Si te interesa este tipo de propuestas, no dudes en apoyarlo en el siguiente enlace: <http://steamcommunity.com/sharedfiles/filedetails/?id=796135160>


Os dejamos con el vídeo promocional


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/iu4l3Sh3Vh0" width="560"></iframe></div>

