---
author: Pato
category: Software
date: 2016-11-04 17:21:33
excerpt: "<p>Valve public\xF3 hace poco una actualizaci\xF3n del Cliente beta de Steam\
  \ que introdujo algunos problemas.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1698b847c2e4fe98c05adcdc9d420590.webp
joomla_id: 87
joomla_url: la-ultima-actualizacion-del-cliente-beta-de-steam-soluciona-las-descargas-y-actualizaciones-en-linux
layout: post
tags:
- steam
- beta
title: "La \xFAltima actualizaci\xF3n del cliente beta de Steam soluciona las descargas\
  \ y actualizaciones en Linux"
---
Valve publicó hace poco una actualización del Cliente beta de Steam que introdujo algunos problemas.

Tal y como publicamos hace un par de días los problemas del Cliente se producían a la hora de descargar o actualizar los juegos. Ahora Valve ha liberadouna nueva actualización del Cliente Beta que aparentemente soluciona estos errores.


Además de esto, Valve ha añadido el soporte para el mando de PS4 Slim V2.


Puedes ver la lista completa de cambios en [este enlace](http://steamcommunity.com/groups/SteamClientBeta/announcements/detail/803158406281970552). (En inglés)


Personalmente utilizo el cliente Beta de Steam, mas que nada por el soporte y novedades respecto a los Steam Controllers y Steam Link que ofrece respecto a la rama estable.


¿Estás utilizando la versión beta del cliente de Steam? ¿Has tenido algún problema con el?


Cuéntamelo en los comentarios.

