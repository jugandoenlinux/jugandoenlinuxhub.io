---
author: Pato
category: "Simulaci\xF3n"
date: 2016-11-23 20:02:15
excerpt: "<p>El juego llega a Linux tras el pol\xE9mico lanzamiento donde mostraban\
  \ el icono de estar disponible, sin estarlo.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e7c0584255fa6f2981e510285a9e9e4f.webp
joomla_id: 120
joomla_url: motorsport-manager-ya-disponible-en-linux-steamos-tras-su-retraso
layout: post
tags:
- carreras
- steam
- simulador
- manager
title: Motorsport Manager ya disponible en Linux/SteamOS tras su 'retraso'
---
El juego llega a Linux tras el polémico lanzamiento donde mostraban el icono de estar disponible, sin estarlo.

![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Motorsportmanagbanner.webp)


Por fin, y tras el lanzamiento en otros sistemas Motorsport Manager [[web oficial](http://www.motorsportmanager.com/)] llega a Linux y esta vez si lleva el icono de Linux/SteamOS en propiedad.


La polémica surgió cuando tras su lanzamiento hace unos días el juego mostraba el icono de estar disponible para nuestro sistema [tal y como nos hicimos eco](https://www.jugandoenlinux.com/item/81-motorsport-manager-ha-sido-publicado-oficialmente-pero-no-descarga-el-juego-en-linux) en Jugando En Linux.


En aquel momento el juego aún no estaba disponible ni siquiera para descarga lo que hizo que el desarrollador tuviese que eliminar el icono para evitar males mayores tras algunas quejas de los usuarios.


Ahora si, el juego está disponible, y además nos llega con el primer parche que añade una buena cantidad de mejoras en la IA, la estabilidad del juego, las opciones etc...


Puedes ver los cambios del parche [en este enlace](http://steamcommunity.com/app/415200/discussions/1/152390014778733204/). (En inglés)


Sinopsis del juego:


¿Tienes lo que hace falta para dirigir un equipo de alto rendimiento en el mundo del motor?


Motorsport Manager es la mejor experiencia de gestión detallada para los amantes del automovilismo. Contratarás a los pilotos, diseñarás los coches y te sumergirás en el dinámico mundo del motor.


Deberás considerar cada detalle si quieres tener alguna opción de optar a vencer el campeonato. Las decisiones durante la carrera se combinan con la estrategia planteada antes de ella. Todo contribuirá en tu camino hacia la gloria o el fracaso, desde la construcción y personalización de tus coches hasta las tácticas del día de la carrera.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/ZPzu6GmIQg0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Requisitos mínimos:


Ubuntu 14.04+, SteamOS  
● Procesador: Intel Core i5-650, 3.20GHz or AMD FX-7500 APU, 2.1Ghz  
● Memoria: 6 GB RAM  
● Grafica: nVIDIA GeForce GTX 440, 1 GB o AMD Radeon HD 5670, 1 GB o Intel HD 5000 Series  
● Disco duro: 16GB


 Tienes ya disponible Motorsport Manager en español en su página de Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/415200/" width="646"></iframe></div>


¿Que te parece la propuesta de Motorsport Manager? ¿Te gustan los juegos de gestión deportiva?


Cuéntamelo en los comentarios.


¿Tienes Motorsport Manager y quieres que publiquemos tu review? ¡[Envíanosla](https://www.jugandoenlinux.com/contacto/envia-un-articulo)!

