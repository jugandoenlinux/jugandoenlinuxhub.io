---
author: Pato
category: Plataformas
date: 2016-11-25 16:16:59
excerpt: "<p>\xBFAlguna vez has tenido la loca idea de que un \"pollo\" salvar\xED\
  a al mundo? Puede que haya llegado el momento... de tener esa idea.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9ded0288e863fbe79d863f606cb05c21.webp
joomla_id: 123
joomla_url: cyber-chicken-un-llamativo-y-disparatado-juego-de-accion-y-plataformas-ya-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- plataformas
- aventura
- 2-5d
title: "'Cyber Chicken' un llamativo y disparatado juego de acci\xF3n y plataformas\
  \ ya disponible en Linux/SteamOS"
---
¿Alguna vez has tenido la loca idea de que un "pollo" salvaría al mundo? Puede que haya llegado el momento... de tener esa idea.

'Cyber Chicken' es un juego de acción y plataformas en 2.5D donde el protagonista es... un pollo cibernético. Armado hasta los dientes tendrás que ir recorriendo los distintos escenarios en esta "quijotesca" aventura para poder salvar al mundo.


La historia de Cyber Chicken comienza cuando tras sofocar una revuelta cyberpunk en una futurista Nueva York distóptica Cyber Chicken se da cuenta de que ha hecho el tonto y debe enfrentarse a las fuerzas intergalácticas. CyberChicken revelará una gran conspiración que involucrará a grandes corporaciones como "Big Bucks Coffee, The Fakebook y al mismísimo lider intergaláctico.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/ji30rdHzQyw" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Cyber Chicken no está disponible en español, pero si eso no es problema para ti tienes el juego disponible en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/339710/" width="646"></iframe></div>


¿Te gustan los juegos de aventuras como este Cyber Chicken? ¿piensas jugarlo?


Cuéntamelo en los comentarios.


¿Tienes Cyber Chicken y quieres que publiquemos tu review? ¡[Mándanosla](https://www.jugandoenlinux.com/contacto/envia-un-articulo)!

