---
author: Pato
category: Software
date: 2016-11-02 21:30:14
excerpt: "<p>Valve a\xF1ade nuevas caracter\xEDsticas de cara a la anunciada actualizaci\xF3\
  n del cliente oficial.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4695cb3b19cbf906e45dac0da0913068.webp
joomla_id: 83
joomla_url: una-nueva-actualizacion-del-cliente-beta-de-steam-trae-novedades-importantes
layout: post
tags:
- steamos
- steam
title: "Una nueva actualizaci\xF3n del cliente beta de Steam trae novedades importantes"
---
Valve añade nuevas características de cara a la anunciada actualización del cliente oficial.

Tal y como [anunciaron](item/31-steam-dev-days-valve-espera-haber-vendido-1-000-000-de-steam-controllers-para-principios-del-proximo-ano-y-dar-soporte-a-otros-mandos-en-steam) en los Steam Dev Days Valve planea lanzar una gran actualización del cliente de Steam donde se esperan grandes novedades, pero antes estas novedades van llegando a la rama beta del cliente en forma de actualizaciones. 


En concreto en la última actualización del cliente beta Valve ha añadido soporte nativo para el mano dual shock de PS4, si bien el soporte aún está en desarrollo y puede tener errores o faltar algunas características. También han añadido un modo "joystick-mouse" para poder utilizar de foma mas adecuada los mandos que no tengan trackpads como los Steam Controllers, es decir gamepads con doble stick.


Por otra parte, Valve ha solucionado un problema con el cliente de Linux donde los ficheros con sistemas como ZFS tenían tamaños erróneos. También han mejorado la velocidad de descargas y actualizaciónes del cliente con discos duros no SSD. Sin embargo parece que esto ha introducido problemas para algunos usuarios que están experimentando problemas con las descargas y actualizaciones, como [Liam de GoL](https://www.gamingonlinux.com/articles/the-latest-steam-client-beta-has-sorta-broken-downloads-and-updates-for-some-linux-users.8453) y que ya están reportados en [Github](https://github.com/ValveSoftware/steam-for-linux/issues/4686) por lo que se espera que estos problemas se solucionen rápidamente.


Puedes ver todos los cambios en las [notas de la actualización](http://steamcommunity.com/groups/SteamClientBeta/announcements/detail/834683256454278286) (en inglés)


Por último, Valve está recomendando a todos los estudios que publican en Steam que actualicen los perfiles de sus juegos de cara a la próxima actualización del Cliente. Recomienda sustituir las imágenes que no sean própiamente del juego en cuestion para dar mejor idea de lo que los clientes van a comprar. Esta información ha sido descubierta por un usuario que ha [compartido en Neogaf](http://www.neogaf.com/forum/showthread.php?t=1305159) una captura de los foros para desarrolladores de Steam.


¿Que te parecen los nuevos cambios en el Cliente de Steam? 


Cuéntamelo en los comentarios.

