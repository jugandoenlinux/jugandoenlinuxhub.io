---
author: Pato
category: "Simulaci\xF3n"
date: 2016-11-29 08:45:08
excerpt: "<p>Laminar Research sigue avanzando hacia el lanzamiento de esta nueva versi\xF3\
  n de su simulador de aviaci\xF3n.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f7b9be29873ad525695063e6e748eae3.webp
joomla_id: 130
joomla_url: x-plane-11-entra-en-fase-beta-y-ofrece-la-posibilidad-de-descargarla-como-demo
layout: post
tags:
- demo
- simulador
- beta
title: X-plane 11 entra en fase beta y ofrece la posibilidad de descargarla como demo
---
Laminar Research sigue avanzando hacia el lanzamiento de esta nueva versión de su simulador de aviación.

X-Plane 11 [[página oficial](http://www.x-plane.com/)] ha entrado en fase beta y ofrece la posibilidad de descarcarla para probar como va en nuestros equipos. Como es normal en esta fase, podeis esperar bugs y otros problemas, y así nos lo avisan desde el anuncio de la publicación.


Si aún así quereis probar a ver como os va, recordaros que tal y como ya os anunciamos en su momento este simulador requiere de equipos vastante potentes, con un mínimo de 8 GB de RAM y drivers propietarios. Tenéis todos los requisitos necesarios en [nuestro artículo](https://www.jugandoenlinux.com/item/86-publicados-los-requisitos-necesarios-para-x-plane-11-en-linux).


También podeis ver las notas de la instalación [en este enlace](http://www.x-plane.com/kb/digital-download-install/) (en inglés).


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/Ov0Sm9GDha4" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


En estos momentos estoy ya descargando la beta. Espero poderos comentar que tal me va con este "hard Sim".


Podeis comprar X-Plane 11 y acceder ya mismo a la Beta, y con la compra también obtendreis acceso a la versión X-Plane 10 mientras la nueva versión no sea lanzada definitivamente:


<http://www.x-plane.com/desktop/buy-it/>


¿Piensas probar X-Plane 11? ¿te gustan este tipo de simuladores?


Cuéntamelo en los comentarios.

