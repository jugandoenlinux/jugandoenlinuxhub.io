---
author: Pato
category: "Exploraci\xF3n"
date: 2016-11-09 19:10:53
excerpt: "<p>El juego ha estado en desarrollo durante alg\xFAn tiempo en fase \"Early\
  \ Access\" desde 2014. Ahora ya sale en versi\xF3n definitiva.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/852967248dd3e6cb3942a1fe6af42945.webp
joomla_id: 96
joomla_url: planet-explorers-un-rpg-sandbox-de-mundo-abierto-ya-disponible-con-soporte-en-linux
layout: post
tags:
- indie
- rol
- steam
- sandbox
title: '''Planet Explorers'', un RPG Sandbox de mundo abierto ya disponible con soporte
  en Linux'
---
El juego ha estado en desarrollo durante algún tiempo en fase "Early Access" desde 2014. Ahora ya sale en versión definitiva.

 Planet Explorers [[página oficial](http://planetexplorers.pathea.net/)] fue un juego que nació de una campaña de Kickstarter allá por 2013 donde recaudó algo más de 137.000 dólares, lo que para un juego indie es un buen logro. Sin embargo esta cantidad se antoja pequeña respecto a las cantidades que manejan estudios con proyectos similares, y es que este Planet Explorer es vastante ambicioso. 


Tiene múltiples modos de juego, un modo historia donde aparentemente no tienes por qué seguirla para progresar. Un modo aventura en un mapa aleatorio donde tendrás que vencer a varios jefes, misiones aleatorias etc...


También tiene un modo de "construcción libre" donde puedes hacer lo que quieras y dos modos online.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/pm_59UFrEIA" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Planet Explorers no está traducido al español pero si eso no es problema para ti puedes comprar Planet Explorers en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/237870/" width="646"></iframe></div>

