---
author: Pato
category: Estrategia
date: 2016-11-24 19:04:26
excerpt: "<p>Los desarrolladores estar\xEDan d\xE1ndole los \xFAltimos retoques.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7a6fe08027b80ee08bda1ed60d73e334.webp
joomla_id: 121
joomla_url: cossacks-3-cerca-de-llegar-a-linux
layout: post
tags:
- proximamente
- steam
- estrategia
title: Cossacks 3 cerca de llegar a Linux
---
Los desarrolladores estarían dándole los últimos retoques.

Según publicó [Liam de GOL](https://www.gamingonlinux.com/articles/cossacks-3-to-come-to-linux-soon-finishing-touches-being-done.8578) Cossacks 3 [[Steam](http://store.steampowered.com/app/333420), [página oficial](http://www.cossacks3.com/)] estaría muy cerca de ser publicado en Linux. Tras un lanzamiento algo accidentado por la cantidad de bugs, el juego ha ido recibiendo parche tras parche y parece que ahora está bien valorado por los usuarios.


Hace poco que añadieron nuevos mapas y la posibilidad de jugar hasta 8 usuarios en la misma partida.


#### Sinopsis del juego:


*¡Vuelve el legendario Cossacks! La secuela de la galardonada serie de estrategia.*   
*Apoyado en la historia del siglo 17 y 18, la estrategia en tiempo real permite batallas colosales de hasta 16.000 soldados simultáneos en el campo de batalla.*   
*Este nueva versión sobre el juego clásico, que originalmente se lanzó en 2000, contiene todos los elementos que distinguen al exitoso juego de Cossacks combinando además gráficos 3d contemporáneos. Cossacks 3 proporciona al jugador con opciones tácticas infinitas, incluyendo no solo la construcción de edificios, la producción de materiales primas, sino también una amplia selección de unidades diferentes y la influencia del terreno.*


 

