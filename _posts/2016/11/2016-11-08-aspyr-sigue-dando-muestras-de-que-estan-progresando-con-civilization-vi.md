---
author: Pato
category: Estrategia
date: 2016-11-08 23:48:18
excerpt: "<p>El estudio dice que tiene buena pinta y que har\xE1n un anuncio en caso\
  \ de lanzar el juego en Linux.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/deb45d333d0414ba3de42155789fdb4a.webp
joomla_id: 95
joomla_url: aspyr-sigue-dando-muestras-de-que-estan-progresando-con-civilization-vi
layout: post
tags:
- steam
- estrategia
- aspyr-media
title: "Aspyr sigue dando muestras de que est\xE1n progresando con Civilization VI"
---
El estudio dice que tiene buena pinta y que harán un anuncio en caso de lanzar el juego en Linux.

Así lo afirman en un escueto Tweet a la pregunta de si anunciarían el lanzamiento:



> 
> [@draxil](https://twitter.com/draxil) Absolutely. We won't keep you guys in the dark... But it's looking good, so don't count us out yet. :D
> 
> 
> — Aspyr Media (@AspyrMedia) [8 de noviembre de 2016](https://twitter.com/AspyrMedia/status/796063144548429824)







Aunque hace algún tiempo hicieron algunas declaraciones afirmanto estar teniendo [algunos problemas](https://blog.aspyr.com/2016/10/04/sid-meiers-civilization-vi-faq/) con el port a Linux lo cierto es que con las últimas declaraciones respecto al desarrollo del juego para Linux sería extraño que al final no terminase saliendo. ¿No os parece?


Seguiremos atentos a lo que puedan anunciar.


¿Te gustaría que Civilization VI llegase a Linux? ¿le darás una oportunidad?


Cuéntamelo en los comentarios.

