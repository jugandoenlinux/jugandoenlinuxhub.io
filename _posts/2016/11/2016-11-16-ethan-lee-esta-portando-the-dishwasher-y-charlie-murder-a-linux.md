---
author: Pato
category: "Acci\xF3n"
date: 2016-11-16 16:10:04
excerpt: <p>Los dos juegos pertenecen a Ska-Studios y hasta ahora solo estaban disponibles
  para consolas.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c1011ef61ed9937904f4938c63d014ea.webp
joomla_id: 109
joomla_url: ethan-lee-esta-portando-the-dishwasher-y-charlie-murder-a-linux
layout: post
tags:
- accion
- proximamente
- steam
title: "Ethan Lee est\xE1 portando 'The Dishwasher' y 'Charlie Murder' a Linux"
---
Los dos juegos pertenecen a Ska-Studios y hasta ahora solo estaban disponibles para consolas.

Ethan Lee es un programador con un largo historial de 'ports' a Linux de grandes juegos. Incluso ha publicado su propio software para ayudar a realizar estas tareas llamado [FNA](http://fna-xna.github.io/).


Esta vez Ethan nos va a traer dos juegos de Ska Studios, los mismos desarrolladores del excelente 'Salt and Sanctuary' que él mismo portó y que [está ya disponible](http://store.steampowered.com/app/283640/) en Steam para Linux, llamados '[Charlie Murder](http://imcharliemurder.com/)' y '[The Dishwasher: Vampire Smile](http://www.vampiresmile.com/)'. Ambos juegos harán uso de su nuevo software [FNA.Steamworks](https://github.com/flibitijibibo/FNA.Steamworks).


![dish](http://www.flibitijibibo.com/skafriday/Vampire4.png)


El juego incluirá todas las características que ya tenían en sus versiones de consolas, como multijugador, logros y rankings.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/c1NJoEJQ7RE" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Según afirma en un post de su [Patreon](https://www.patreon.com/posts/big-pc-project-7218392) espera lanzar ambos títulos este próximo Diciembre 2016 o Enero 2017, y estarán disponibles en Steam con soporte en Linux de salida.


¿Te gustan los juegos en los que trabaja Ethan Lee? Cuéntamelo en los comentarios.


¿Tienes 'Salt and Sancturary' y quieres que publiquemos tu review? ¡[Envíanosla](contacto/envia-un-articulo)!


Este artículo está basado en uno de [Gamingonlinux](https://www.gamingonlinux.com/articles/the-dishwasher-vampire-smile-charlie-murder-coming-to-linux-porting-done-by-ethan-lee.8545)
