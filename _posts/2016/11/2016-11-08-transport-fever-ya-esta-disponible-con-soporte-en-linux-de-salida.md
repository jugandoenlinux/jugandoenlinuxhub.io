---
author: Pato
category: Estrategia
date: 2016-11-08 17:18:35
excerpt: "<p>El juego de gesti\xF3n de transportes llega a Linux de la mano de Urban\
  \ Games, creadores tambi\xE9n de Train Fever.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/13f34e2b533e12c6166f88368dcd8c07.webp
joomla_id: 94
joomla_url: transport-fever-ya-esta-disponible-con-soporte-en-linux-de-salida
layout: post
tags:
- steam
- gog
- simulador
- estrategia
title: "Transport Fever ya est\xE1 disponible con soporte en Linux de salida"
---
El juego de gestión de transportes llega a Linux de la mano de Urban Games, creadores también de Train Fever.

En este juego además de trenes y tranvías deberás llevar la gestión de transportes por carretera, mar y aire. Los creadores de Train Fever han ampliado la gestión para abarcar nuevos transportes en un juego que recuerda mucho a este último. De hecho muchos de los usuarios están reportando el grán parecido de este Transport Fever con Train Fever en los menús y opciónes comentando en algunos casos que mas que un juego nuevo es casi una expansión o evolución de este. Sin embargo para muchos esto no es malo, mas bien al contrario aumenta las posibilidades de gestión que ofrece este título.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/pAbaLPKFAdg" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


#### Sinopsis del juego:


*Transport Fever es un juego de gestión centrado en el transporte. El jugador comienza su trayectoria en 1850 y crea una próspera compañía de transporte. Como magnate emergente de la industria transportista, construyes estaciones, aeropuertos, puertos y haces tu fortuna al interconectar zonas que requieren servicios de transporte.*


*Construye una compleja red de carreteras, ferrocarril y transporte marítimo en un juego infinitivo y explora más de 150 años de la historia del transporte. Satisface las necesidades de la gente y observa a las ciudad crecer de forma dinámica. Suministra mercancías a las industrias, desarrolla cadenas completas de cargamento y facilita el crecimiento económico. ¡Construye un imperio del transporte!*


#### Características:


*Dos modos de juego: juego infinito y modo de campaña.*   
*Más de 120 trenes, aviones, barcos, autobuses, tranvías y camiones con todo lujo de detalles.*   
*Construcción intuitiva e intensa de vías férreas y calles.*   
*Estaciones de tren, autobús, aeropuertos y puertos ampliables.*   
*Campaña europea y americana con 10 misiones históricas respectivamente.*   
*Terrenos generados de forma aleatoria, modificables y con dimensiones realistas.*   
*Entornos de juego plenamente desarrollados de Europa y América.*   
*Desarrollo urbano y movimiento de pasajeros simulados de forma dinámica.*   
*Modelo económico y simulación de transporte complejos.*   
*Contenido de más de 150 años de historia del transporte.*   
*Simulación, colores y envejecimiento realistas de los vehículos.*   
*Gráficos, iluminación y simulación basados en principios físicos.*   
*Más de 25 logros desafiantes.*   
*Steam Workshop y amplia asistencia de moderadores.*


Puedes comprar Transport Fever en [GOG](https://www.gog.com/game/transport_fever) o en su página de Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/446800/" width="646"></iframe></div>


¿Que te parece este Transport Fever? ¿piensas darle una oportunidad?


Cuéntamelo en los comentarios.


¿Tienes el juego y quieres que publiquemos tu review? ¡[Envíanosla](contacto/envia-un-articulo)!

