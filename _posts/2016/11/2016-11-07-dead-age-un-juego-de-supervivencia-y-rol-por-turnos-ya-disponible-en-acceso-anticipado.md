---
author: Pato
category: Rol
date: 2016-11-07 18:36:59
excerpt: "<p>Se trata de un llamativo juego de supervivencia con batallas por turnos\
  \ y una historia con m\xFAltiples variantes que lo diferencia de otros juegos de\
  \ zombies.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5483e331a9bace540b3a2478fc014e25.webp
joomla_id: 90
joomla_url: dead-age-un-juego-de-supervivencia-y-rol-por-turnos-ya-disponible-en-acceso-anticipado
layout: post
tags:
- rol
- steam
- zombies
- acceso-anticipado
- supervivencia
title: '''Dead Age'' un juego de supervivencia y rol por turnos ya disponible en acceso
  anticipado'
---
Se trata de un llamativo juego de supervivencia con batallas por turnos y una historia con múltiples variantes que lo diferencia de otros juegos de zombies.

Puedes ver un ejemplo de su jugabilidad en el vídeo de presentación:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/LP6FS3hyVfY" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Dead Age llega a Linux en acceso anticipado y sin doblaje al español. Si el idioma no es impedimento para tí, puedes conseguirlo en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/363930/" width="646"></iframe></div>


¿Que te parece este tipo de juegos? ¿piensas darle una oportunidad?


Cuéntamelo en los comentarios.

