---
author: leillo1975
category: Hardware
date: 2016-11-19 12:30:40
excerpt: "<p>Disponible la nueva versi\xF3n del driver privativo de Nvidia con novedades\
  \ importantes</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/aaa036e4cb16038f90e128d8e39c714f.webp
joomla_id: 111
joomla_url: nuevo-driver-nvidia-375-20-estable-lanzado
layout: post
tags:
- drivers
- nvidia
- privativo
title: Nuevo driver Nvidia 375.20 estable lanzado (Actualizado)
---
Disponible la nueva versión del driver privativo de Nvidia con novedades importantes

En el día de ayer ha sido lanzada esta nueva build del driver privativo de Nvidia. Se corresponde con la rama 375 y ha dejado el periodo de beta para convertirse en estable. Tiene importantes novedades entre las que destacan el soporte para las tarjetas GTX 1050, soporte para los nuevos kernels, la versión 1.19 del servidor Xorg (ABI23), soporte para Nvidia 3D Vision 2  y muchas otros añadidos y correcciones.


Pero si por algo destaca esta versión para los más jugones es por el incremento del tamaño máximo de la cache para shaders de 64 a 128MB, lo que va a ayudar por ejemplo en juegos muy exigentes como Deus Ex a aligerar el tiempo de carga.


Podemos descargarlos directamente de la [página de Nvidia](http://www.nvidia.es/download/driverResults.aspx/111631/es) o esperar un poco a que vayan apareciendo en los diferentes respositorios de las distribuciones. En el caso de Ubuntu normalmente suelen aparecer a los pocos días en el [repositorio de gráficos oficial](https://launchpad.net/~graphics-drivers/+archive/ubuntu/ppa), por lo que es recomendable esperar un poco, ya que suele ser más sencillo de instalar.


 


**Actualización:** Ya está disponible en el [repositorio oficial de gráficos de Ubunt](https://launchpad.net/~graphics-drivers/+archive/ubuntu/ppa)u la nueva versión del driver 

