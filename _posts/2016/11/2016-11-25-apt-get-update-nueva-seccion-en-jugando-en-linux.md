---
author: Pato
category: Apt-Get Update
date: 2016-11-25 18:29:18
excerpt: "<p>Por que no podemos estar en todas partes ni a todas horas, aqu\xED va\
  \ un resumen de lo que nos hemos dejado en el tintero.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ee842e3019fee30e4ca87cc93974d54b.webp
joomla_id: 126
joomla_url: apt-get-update-nueva-seccion-en-jugando-en-linux
layout: post
title: "Apt-Get Update: Nueva 'secci\xF3n' en Jugando En Linux"
---
Por que no podemos estar en todas partes ni a todas horas, aquí va un resumen de lo que nos hemos dejado en el tintero.

Estrenamos esta sección que intentaremos llevar a cabo semanalmente para ir resumiendo todas esas "cosillas" que o bien no dan para un artículo, o no tenemos tiempo suficiente para cubrirlas, o simplemente por que nos apetece... ¿y que mejor nombre que un comando de actualización?


Bueno, al menos en Debian y derivadas ;-D  Comenzamos:


-[El Driver Open Source de Vulkan para AMD 'radv' sigue mejorando rápidamente.](https://cgit.freedesktop.org/mesa/mesa/log/)


[Llega la actualización número 34 del driver.](https://github.com/KhronosGroup/Vulkan-Docs/blob/1.0/ChangeLog.txt)


Añadidas también nuevas extensiones, soporte anisotrópico en las familias Sea Islands y Southern Islands, y diversos parches.


Hablando de Mesa, 


-[Feral Interactive pide a Canonical que meta las actualizaciones del driver Mesa en un PPA oficial para que sea más sencillo actualizarlos en sus sistemas y poder darle soporte oficial en sus juegos.](https://lists.ubuntu.com/archives/ubuntu-desktop/2016-November/004841.html)


-[Planetary Anihilation: TITANS y Anihilation recibe un gran parche en la beta con mejoras en el rendimiento habilitando el procesado multihilo y otras mejoras.](https://forums.uberent.com/threads/the-performance-update-titans-build-99377.72108/)


-Moon Hunters recibe una actualización con un montón de novedades de forma gratuita. [Nos lo cuenta Liam de GOL.](https://www.gamingonlinux.com/articles/moon-hunters-updated-with-a-bunch-of-new-content-for-free.8579)


-También Liam nos cuenta que [el port de Killing Floor está parado a la espera de encontrar un desarrollador para continuar con el.](https://www.gamingonlinux.com/articles/the-linux-steamos-port-of-killing-floor-2-has-been-put-on-hold-it-needs-a-developer.8567)


-Ballistic Overkill, el FPS recibe una actualización con nuevo sistema de progresión y Skins para las armas. [Las notas de la actualización aquí](http://steamcommunity.com/games/ballistic/announcements/detail/582483144491454442).


Y ahora una de Steam Workshop:


-[Rocket League espera dar soporte para mods en Steam Workshop en Diciembre](https://www.rocketleaguegame.com/news/steam-workshop-support-this-december/), y por otra parte [Saints Row IV ya da soporte para Steam Workshop en Windows y esperan anunciar el soporte en Linux próximamente](http://steamcommunity.com/games/206420/announcements/detail/341540564442677295).


Por último, os dejamos el vídeo de la primera muestra de la Beta de X Plane 11:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/w8nqEtAl0rk" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Espero que el resumen sea de vuestro agrado. Si crees que se ha quedado algo importante o interesante en el tintero, o quieres avisarnos sobre otras novedades puedes hacerlo [enviándonos un aviso](https://www.jugandoenlinux.com/contacto/avisa-a-un-editor).


La semana que viene, mas y mejor en otro Apt-Get Update.

