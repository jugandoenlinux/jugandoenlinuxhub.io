---
author: Pato
category: Terror
date: 2016-11-21 18:34:30
excerpt: "<p>Durante unos d\xEDas puedes conseguir el juego rebajado y de paso apoyar\
  \ al desarrollador que nos lo trajo a Linux.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/948378d6a67ac0d7c7c6728581b072ab.webp
joomla_id: 116
joomla_url: alien-isolation-esta-de-rebajas-en-la-tienda-de-feral
layout: post
tags:
- oferta
- terror
- feral-interactive
title: "'Alien Isolation' est\xE1 de rebajas en la tienda de Feral"
---
Durante unos días puedes conseguir el juego rebajado y de paso apoyar al desarrollador que nos lo trajo a Linux.

Durante unos días Alien Isolation está disponible en la [tienda](https://store.feralinteractive.com/es/) de Feral Interactive con un descuento "mortífero" del 70%, quedando en 13,79€. Un precio muy apetecible que hace dificil resistirse a los "encantos" de nuestro Xenomorfo favorito.


Puedes comprar el juego rebajado en la Tienda de Feral [en este enlace](https://store.feralinteractive.com/es/mac-linux-games/alienisolation/) y así además apoyarás a Feral para que siga trayéndonos más juegos a nuestro sistema favorito.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/SUX25d0pZy8" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


¿Que te parece la oferta? ¿Piensas comprar Alien Isolation?


Cuéntamelo en los comentarios.


¿Tienes Alien Isolation y quieres que publiquemos tu review? ¡[Mándanosla](https://www.jugandoenlinux.com/contacto/envia-un-articulo)!

