---
author: Pato
category: "Acci\xF3n"
date: 2016-11-30 19:39:32
excerpt: <p>El juego fue publicado en Junio y tras unos meses de espera nos llega
  a Linux.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e1e1ad60f07c4aa3ccbcb2973e9d7007.webp
joomla_id: 132
joomla_url: el-estudio-valenciano-anima-project-lanza-anima-gate-of-memories-para-linux
layout: post
tags:
- accion
- aventura
- rol
title: El estudio Valenciano Anima Project lanza 'Anima Gate of Memories' para Linux
---
El juego fue publicado en Junio y tras unos meses de espera nos llega a Linux.

¡Vaya sorpresa cuando esta tarde al abrir mi cuenta de Steam [me salta el mensaje](http://steamcommunity.com/games/380750/announcements/detail/289750474005981863) de que ya está disponible 'Anima Gate of Memories' para nuestro sistema favorito!


El estudio Valenciano Anima Project junto a Bad Land como editora nos trae este "Hack and Slash" o juego de rol y acción en tercera persona de corte japonés donde tendremos que afrontar distintos retos.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/B7-wSCo0TKg" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


[En este hilo](http://steamcommunity.com/app/380750/discussions/0/357285398693568156/?ctp=6#c152390648075888282) de su foro de Steam hemos podido estar al tanto de los esfuerzos del estudio por traernos Anima Gate of Memories a Linux. Lo cierto es que este tipo de juegos no son abundantes en Linux ni mucho menos, así que es de aplaudir que nos haya llegado.


Sinopsis del Juego:


 *Anima: Gate of Memories trae a los videojuegos el mundo de Gaïa, de la conocida saga de libros de rol Anima Beyond Fantasy. En él disfrutaras de una historia donde tus acciones y elecciones decidirán el curso del viaje y el destino de los personajes.*


Anima Gate of Memories está disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/380750/" width="646"></iframe></div>


Puedes leer también el artículo del lanzamiento en inglés en [gamingonlinux.com](https://www.gamingonlinux.com/articles/anima-gate-of-memories-a-third-person-action-rpg-is-now-on-linux-some-quick-thoughts.8626)


¿Que te parece este Anima Gate of Memories? ¿Le darás una oportunidad?


Cuéntamelo en los comentarios.


¿Tienes Anima Gate of Memories y quieres que publiquemos tu review? ¡[Mándanosla](contacto/envia-un-articulo)!


 

