---
author: Pato
category: Terror
date: 2016-11-10 19:56:16
excerpt: "<p>El juego de Ballistic Interactive ya sac\xF3 adelante una campa\xF1a\
  \ de Steam Greenlight.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/00e7056ec788d5b42162a6c13dbba43c.webp
joomla_id: 100
joomla_url: hellhunter-un-juego-de-terror-en-vista-isometrica-busca-apoyos-en-kickstarter-aseguran-soporte-en-linux
layout: post
tags:
- indie
- rol
- terror
- vista-isometrica
- kickstarter
title: "'Hellhunter' un juego de terror en vista isom\xE9trica busca apoyos en Kickstarter.\
  \ Aseguran soporte en Linux"
---
El juego de Ballistic Interactive ya sacó adelante una campaña de Steam Greenlight.

Hellhunter [[página oficial](http://hellhuntergame.com/)] es un juego de terror donde seremos un "buscador" de fantasmas "freelance" que tendrá que hacerse su propia carrera buscando la más terrorífica experiencia. La identidad y características de tus enemigos no son conocidas por lo que tendrás que investigar mientras eres perseguido para descubrir con qué armas y como afrontar cada enfrentamiento en base a tus deducciones en una frenética lucha hasta cada final de nivel.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/hKxz9azuu7Q" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Si te gustan este tipo de juegos puedes apoyar a Hellhunter en su [campaña de Kickstarter](https://www.kickstarter.com/projects/686144604/hellhunter/description). En el momento de escribir estas líneas llevan recaudados unos 18.500 dólares de una meta de 50.000 a falta de 16 días para finalizar la campaña. 


Desde mi punto de vista, siempre me han llamado la atención este tipo de juegos y este desde luego que si cumple lo que promete puede ser un gran título.


El juego saldrá en PC en Steam con soporte en Linux ya anunciado.


¿Que te parece la propuesta de Hellhunter? ¿Piensas apoyarlo en Kickstarter? 


Cuéntamelo en los comentarios.

