---
author: Pato
category: Hardware
date: 2016-11-21 17:11:59
excerpt: "<p>Casi duplican el dinero que ped\xEDan para financiar el proyecto. La\
  \ compa\xF1\xEDa seguir\xE1 buscando financiaci\xF3n en otras plataformas de Crowdfunding.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d6e7bc44feb1613d041d5385e5745b10.webp
joomla_id: 114
joomla_url: smach-z-finaliza-con-exito-su-campana-de-kickstarter
layout: post
tags:
- portatil
- crowdfunding
- kickstarter
title: "SMACH Z finaliza con \xE9xito su campa\xF1a de Kickstarter"
---
Casi duplican el dinero que pedían para financiar el proyecto. La compañía seguirá buscando financiación en otras plataformas de Crowdfunding.

El microPC ultraportatil SMACH Z acudió hace un mes a Kickstarter buscando financiación para hacer realidad este proyecto y parece que ahora sí lo ha conseguido. Como ya publicamos en Jugando En Linux [en un post anterior](https://www.jugandoenlinux.com/item/47-el-micropc-portatil-para-jugar-smach-z-vuelve-a-kickstarter) donde explicamos pormenorizadamente en qué consiste SMACH Z, la compañía española SMACH está detrás de este proyecto que ya acudió en otras ocasiones a las plataformas de financiación sin conseguir el éxito esperado, pero esta vez con una planificación económica más modesta si han conseguido financiarse.


En concreto, el dinero que la compañía pedía para realizar este proyecto era de unos 250.000€, y al finalizar la campaña han conseguido algo más de 574.000€. Casi duplica lo que pedían, si bien es cierto que el "mínimo" que pedían era ciertamente ajustado. No hay que olvidar que en otras campañas el presupuesto que pedían se acercaba al millón de euros.


Según han comunicado en su [web de Kickstarter](https://www.kickstarter.com/projects/smachteam/smach-z-the-handheld-gaming-pc/posts/1742737) agradeciendo a todos los que han apoyado el proyecto, seguirán buscando fondos en otras campañas de financiación en distintas plataformas como [IndieGOGO](https://www.indiegogo.com/projects/smach-z-the-handheld-gaming-pc-games).


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/ZkmZY0xe6w8" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


¿Que te parece la propuesta de SMACH Z? ¿Crees que tendrá éxito este proyecto en otras plataformas de financiación?


Cuéntamelo en los comentarios.

