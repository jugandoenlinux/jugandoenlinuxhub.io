---
author: Pato
category: Estrategia
date: 2016-11-12 01:03:19
excerpt: "<p>As\xED lo han anunciado en su cuenta de Twitter. La fecha de lanzamiento\
  \ est\xE1 cerca.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0d0c19531d6e29f793ed165732978408.webp
joomla_id: 103
joomla_url: feral-retransmitira-una-sesion-de-total-war-warhammer-en-linux-el-proximo-16-de-noviembre
layout: post
tags:
- proximamente
- feral-interactive
- estrategia
title: "Feral retransmitir\xE1 una sesi\xF3n de Total War: WARHAMMER en Linux el pr\xF3\
  ximo 16 de Noviembre"
---
Así lo han anunciado en su cuenta de Twitter. La fecha de lanzamiento está cerca.

Total War: WARHAMMER es el último juego en salir de la serie Total War, y uno de los juegos más esperados de la saga al unir la saga de estratégia Total War con el vasto universo de WARHAMMER.


Desde hace algún tiempo esperamos su llegada a Linux de manos de Feral Interactive que son los que se están ocupando de portar el juego. Ahora han anunciado mediante un Tweet que el próximo miércoles día 16 a las 6pm GTM (7 de la tarde en España) retransmitirán una sesión del juego corriendo en Linux en su canal de Twitch:



> 
> First steps in a ceaseless battle! On Wednesday, [#FeralPlays](https://twitter.com/hashtag/FeralPlays?src=hash) Total War: WARHAMMER for Linux. Follow us on Twitch: <https://t.co/kbDZOaA0zS> [pic.twitter.com/LWxmTXIMA9](https://t.co/LWxmTXIMA9)
> 
> 
> — Feral Interactive (@feralgames) [11 de noviembre de 2016](https://twitter.com/feralgames/status/797103433232879616)



Normalmente Feral hace este tipo de retransmisiones cuando el juego está próximo a su salida como forma de mostrar qué tal se mueve, así que debemos estar muy cerca de saber la fecha de lanzamiento en Linux. Desde luego Feral este año está que no para.


Respecto a las retransmisiones de Feral, otras comañías y "Streamers" estar atentos a Jugando En Linux pues pronto tendremos novedades para todos vosotros.


¿Que te parece la llegada de Total War: WARHAMMER a Linux? ¿te apuntarías a ver la sesión de streaming con nosotros?


Cuéntamelo en los comentarios.

