---
author: Pato
category: Aventuras
date: 2016-11-24 19:23:48
excerpt: <p>El juego se ha lanzado con soporte en Linux de salida.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/39a27618f1dc54b80987c6706135e6b7.webp
joomla_id: 122
joomla_url: ittle-dew-2-un-juego-de-aventura-y-accion-tipo-zelda-ya-disponible-en-linux
layout: post
tags:
- accion
- indie
- aventura
- steam
title: "'Ittle Dew 2' un juego de aventura y acci\xF3n tipo Zelda ya disponible en\
  \ Linux"
---
El juego se ha lanzado con soporte en Linux de salida.

Ittle Dew 2 es un juego de acción y aventuras desarrollado por Ludosity cuya jugabilidad es parecida a los Zelda. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/89cBMA4B9rA" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Lo cierto es que nunca viene mal tener a mano un juego de este tipo, sobre todo para que disfruten los pequeños de la casa... o no tan pequeños.


Tienes disponible Ittle Dew 2 en su página de Steam con menús y subtítulos en español:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/395620/" width="646"></iframe></div>


¿Te gustan los juegos "tipo Zelda"? ¿Piensas jugar a este Ittle Dew2?


Cuéntamelo en los comentarios


¿Tienes Ittle Dew 2 y quieres que publiquemos tu review? ¡[Envíanosla](https://www.jugandoenlinux.com/contacto/envia-un-articulo)!

