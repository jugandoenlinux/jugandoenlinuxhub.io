---
author: Serjor
category: Noticia
date: 2018-08-15 08:49:42
excerpt: <p>Radio patio vuelve a poner su maquinaria en marcha</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/51fccc7d7519fea84c85898572d90843.webp
joomla_id: 831
joomla_url: valve-podria-estar-desarrollando-un-sistema-para-ejecutar-juegos-para-otras-plataformas-desde-steam
layout: post
tags:
- steam
- wine
- valve
- rumor
title: "Valve podr\xEDa estar desarrollando un sistema para ejecutar juegos para otras\
  \ plataformas desde Steam"
---
Radio patio vuelve a poner su maquinaria en marcha

Saltaba la noticia en [reddit](https://www.reddit.com/r/linux_gaming/comments/979m4u/steam_play_compatibility_for_wine_seems_to_be/) y [GamingOnLinux](https://www.gamingonlinux.com/articles/valve-may-be-adding-support-for-using-compatibility-tools-for-playing-games-on-different-operating-systems.12349) se hacía eco de la misma, y es que la rumorología de internet dice que es posible que Valve esté trabajando en "algo" para poder ejecutar juegos de una plataforma en otra para la que no había sido diseñada, o lo que es lo mismo, integrar una especie de Wine dentro del propio cliente Steam.


No está claro (repetimos, es rumorología y conjeturas sacadas de [SteamDB](https://github.com/SteamDatabase/SteamTracking/blame/6cb3db9ac4cb1f7d0bf218721f4e6f2d173c5b9b/ClientExtracted/public/steamui_english.txt#L4375), por lo que el grado de fiabilidad es muy pequeño) cómo lo harán. Sin saber cómo, y aportando nuestro granito de arena a los rumores sin fundamento, una opción podría ser algo tipo Wine, que se ejecuta por encima y ya. Otra opción podría ser que los desarrolladores en vez de compilar contra las librerías del sistema operativo compilaran contra una API genérica de Valve y luego el cliente de cada plataforma provea las librerías necesarias, algo parecido a lo que hacen Java o UWP de Microsoft, o a saber, es Valve.


Y siendo Valve, ya famosa por su Valve Time, tampoco hay indicios del cuando, pero podrían aprovechar el lanzamiento del cliente con la nueva interfaz y [soporte para 64 bits](index.php/homepage/noticias/item/947-la-version-beta-de-steam-se-actualiza-y-anade-soporte-para-64-bits) para anunciar esta nueva funcionalidad.


De todos modos, y en caso de ser cierto, esperemos que Valve haya tomado una solución que no esté basada en el mismo concepto que Wine, ya que de esa manera el juego nativo en GNU/Linux tendría problemas para despegar, ya que si ahora es difícil, en ese caso imposible, porque los desarrolladores generarían el juego para el entorno "Valve" (o Windows si luego es Valve quién hace una especie de Wine) y sería muy difícil ver estos juegos en otras tiendas como GoG o Humble Bundle (que también distribuye binarios), y nos encontraríamos en una especie de monopolio dentro de GNU/Linux, lo cuál nunca es positivo.


Y a ti, ¿qué te parecería este movimiento por parte de Valve? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

