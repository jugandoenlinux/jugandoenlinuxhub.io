---
author: Pato
category: Ofertas
date: 2020-01-23 18:54:25
excerpt: "<p>De nuevo tenemos que exprimir las carteras...</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamoferta/rebajassteamene2020.webp
joomla_id: 1152
joomla_url: comienzan-las-rebajas-de-steam-por-el-ano-nuevo-lunar-chino
layout: post
title: "Comienzan las rebajas de Steam por el a\xF1o nuevo lunar chino"
---
De nuevo tenemos que exprimir las carteras...


 


Aún no nos hemos recuperado de las "largas" rebajas de Navidad de Steam de este pasado año y ya tenemos un nuevo periodo de rebajas, esta vez por el año nuevo lunar chino.


Desde que el mercado chino comenzó a expandirse y se ha ido haciendo más y más importante se ha popularizado las rebajas dedicadas a los videojuegos de cara al país asiático, y ya de paso el resto del mundo tenemos una nueva escusa para sacar a pasear las ya maltrechas tarjetas de crédito.


Así pués, desde hoy y hasta el próximo día 27 de este mes de Enero tenemos miles de juegos rebajados en ese gran bazár que es Steam.


¿A qué esperáis? ¡ya tardáis en pasar por [store.steampowered.com](https://store.steampowered.com/) a ver que vais a comprar de rebajas!

