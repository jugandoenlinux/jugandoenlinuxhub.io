---
author: leillo1975
category: Estrategia
date: 2017-02-07 14:15:12
excerpt: <p>Parece que los dioses han escuchado nuestras plegarias...</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/35b3ab2a19f46ac3056fff8ccff085c4.webp
joomla_id: 209
joomla_url: banished-para-linux-da-senales-de-vida
layout: post
tags:
- banished
- shining-rock-software
title: "Banished para Linux da se\xF1ales de vida"
---
Parece que los dioses han escuchado nuestras plegarias...

...Pues sorpresa agradable la que me he llevado hace un momento cuando visitaba la fantástica [GamingOnLinux.com](https://www.gamingonlinux.com/articles/the-linux-port-of-banished-is-still-alive-the-developer-plans-their-next-game-to-launch-cross-platform-on-day-1.9060), ya que según reza el artículo de Liam, la gente de Shining Rock Software ha hecho una [actualización en su blog](http://www.shiningrocksoftware.com/2017-02-06-mega-update/) en la que explican un montón de detalles sobre el port de su conocido juego, entre otras cosas.


Probablemente recordeis que hace poco más de dos meses mencionaba en el artículo "[Lo que pudo haber sido y no fué](index.php/item/234-lo-que-pudo-haber-sido-y-no-fue)", lo que parecía ser el abandono por parte de **Shining Rock Software** del port a GNU/Linux de su conocido juego. En el os describía la desazón que me producía la falta de más información por parte del equipo de desarrollo, ya que por lo que parecía en su última comunicación, el juego parecía estar cerca del fin de su proceso de portado a GNU/Linux.


Ahora, muchos meses después de esa ultima comunicación, el equipo de desarrollo ha colgado un extenso mensaje en su blog donde informan, entre otras muchas cosas, del estado actual de dicho port. Según aclaran en dicho mensaje, el juego, aunque realmente corre en Linux, está lejos de ser un producto acabado, ya que quedan muchos pequeños detalles que corregir, así como pruebas. Sobre OpenGL han comentado que en este momento no existen dos versiones, una para Windows y otra para Linux/Mac, sino que es todo una unidad. Han detallado el mensaje con multitud de información bastante técnica en la que no pienso ahondar, más que nada por mi propio desconocimiento sobre este tipo de cuestiones (es mejor que recurrais a la [fuente original](http://www.shiningrocksoftware.com/2017-02-06-mega-update/)). También aclaran que han estado muy ocupados haciendo que sus herramientas de desarrollo sean multiplataforma, lo que les será muy útil tanto en el desarrollo de Banished, como de futuros proyectos.


Y es en el tema de los futuros proyectos donde han reconocido que han estado trabajando, y donde también han avanzado que estos estarán disponibles para todos los sistemas desde el primer día gracias al desarrollo de las herramientas que antes comentabamos. También han reconocido que están bastante atentos al desarrollo de **Vulkan** y que están pensando la posibilidad de implementarlo en sus productos en un futuro.


![Banished](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Banished02.webp)


 


Como veis, el estudio no solo nos ha sorprendido con la noticia del "no abandono" de la versión para GNU/Linux de Banished, sinó anunciandonos que tienen cosas nuevas entre manos y que nos harán a los usuarios del pingüino  participes a ellas desde el primer día. Por supuesto estaremos atentos a futuras actualizaciones por parte de este estudio de las que os informaremos en cuanto se produzcan. Si quereis comentar cualquier cosa sobre este artículo podeis hacerlo en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord.](https://discord.gg/ftcmBjD)


 


**FUENTES:** [GamingOnLinux](https://www.gamingonlinux.com/articles/the-linux-port-of-banished-is-still-alive-the-developer-plans-their-next-game-to-launch-cross-platform-on-day-1.9060), [Shining Rock Software](http://www.shiningrocksoftware.com/)

