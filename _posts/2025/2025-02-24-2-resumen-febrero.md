---
author: Son Link
category: "Apt-Get Update"
date: 24-02-2025 16:00:00
excerpt: "Resumen de las noticias publicadas en Mastodon en las últimas semanas"
layout: post
tags:
- noticias
- resumen
title: "Desde como hacer el cabra en la Steam Deck hasta matar aliens sin saber el motivo"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumenes/02-febrero-2025.webp
---
Tras otro par de semanas con poca actividad, volvemos con un nuevo resumen de lo que hemos ido compartiendo en nuestra cuenta de [Mastodon](https://mastodon.social/@jugandoenlinux). Vayamos al grano:

## [Como hacer mejor el cabra en Steam Deck](https://mastodon.social/@p_vader/114032111886708503)

¿Tienes el juego **Cult of the Lamb** y notas que va algo mal?

A raíz de una pregunta lanzada en Mastodon por la usuaria [@IstharVega](https://mastodon.social/@IstharVega), nuestro compañero [@p_vader](https://mastodon.social/@p_vader/) publico como optimizar este gran juego:

- En la #SteamDeck 🎮  
	- Limité FPS/HZ a 40  
	- Limité el TDP a 5 vatios.

- En el juego:  
	- Activé la sincronización vertical.  
	- Calidad gráfica en normal o baja.

Ademas la duración de la batería pasa de 1:50 a 3 horas y sin "calenton".

---

## [Nuevo golpe en la Mesa](https://mastodon.social/@jugandoenlinux/114035696899786717)

Se ha publicado **Mesa 25.0.0**, los drivers libres para Linux, y que añade soporte **Vulkan 1.4** para multitud de tarjetas gráficas de **Nvidia**, **AMD**, **Intel**, **Qualcomm**, **Mali** y **Apple**.

Podéis ver el anuncio en la lista de [correo de Mesa](https://lists.freedesktop.org/archives/mesa-dev/2025-February/226464.html).

---

## [Nunca hay suficientes metroidvenia](https://mastodon.social/@jugandoenlinux/114031870443614672)

**Maseylia Echoes of the Past** en un metroidvania 3d con un marcado estilo gráfico inspirado en **Moebius**.

Recientemente han sacado una demo y están buscando feedback antes de sacar su videojuego.

Podéis probarlo descargando la demo nativa para Linux en Steam.

<div class="steam-iframe">
<iframe src="https://store.steampowered.com/widget/2938870/Maseylia__Echoes_of_the_Past"></iframe>
</div>

---

## [Vamonos de torneo](https://fosstodon.org/@onfoss/114021313122275983)

El 15 de Marzo hay nueva edición del torneo de [@onFOSS](https://fosstodon.org/@onfoss/). Para los que no lo sepáis son unos torneos en los que a lo largo de un día se realizan varias competiciones de juegos libres, en esta ocasión los elegidos son:

* [Tanks of Freedom](https://github.com/P1X-in/Tanks-of-Freedom-II)
* [0 A.D.](https://mastodon.social/@play0ad)
* [Hurry Curry!](https://codeberg.org/hurrycurry/hurrycurry)
* [OpenArena](https://openarena.ws/download.php)
* [Unvanquished](https://idtech.space/users/UNVofficial) 

Más información y horarios en el [anuncio oficial](https://onfoss.org/news/2025/02/15/Next-event-March-15th.html)

---
## [Nueva versión de Minet..., digo, de Luanti](https://mastodon.social/@jugandoenlinux/114007368249565192)

**Luanti** (anteriormente **Minetest**) es una base para cientos de juegos con un estilo voxel.   
Se ha actualizado a la versión 5.11 destacando:  
- Los shaders ya son obligatorios y la versión mínima de OpenGL es 2.  
- Se ha añadido un menú de configuración durante el juego. Se acabó salirse de la partida para cambiar cosas.  
- Desde el menú puedes ver más información de los servidores públicos, como mods, usuarios, etc.

Más información en el [anuncio oficial](https://blog.luanti.org/2025/02/14/5.11.0-released)

---
## [Sobrevive y construye mientras defiendes a tus aldeanos](https://mastodon.social/@jugandoenlinux/113998065541852724)

El juego **Dawnfolk** del genial [@darennkeller](https://mastodon.gamedev.place/@darennkeller) se lanzo hace poco. Ademas es nativo para Linux 🐧 y verificado para la Steam Deck, de hecho su propio creador lo muestra en el siguiente vídeo:

<div class="resp-iframe">
	<iframe width="1366" height="480" src="https://www.youtube.com/embed/sxFGXCbpiYM" title="Why Steam Deck is the PERFECT Fit for Dawnfolk!" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

<div class="steam-iframe">
	<iframe src="https://store.steampowered.com/widget/2308630"></iframe>
</div>

100% recomendado desde jugandoenlinux, aunque siempre puedes probar su demo y convencerte tú mismo 😊 

---

## [Como estar de mala leche con unos alienígenas y no saber el por qué]

Hace unos días a traves de [@AlphaBetaGamer](https://mastodon.gamedev.place/@AlphaBetaGamer) supimos de la existencia de Mala Petaca un FPS con unos gráficos pixel art y muy coloridos y que hace uso del motor GZDoom, por lo que, aunque no hay descargas para Linux, gracias a unos sencillos pasos podremos jugarlo en nuestro sistema:

* Lo primero es descargar el juego desde su página en [Itch](https://sanditiojitok.itch.io/mala-petaka-demo)
* Una vez descargado, descomprimimos el zip en cualquier carpeta que queramos.
* Dentro de la carpeta del juego vamos a crear un archivo, por ejemplo mala-petaka.sh con el siguiente contenido:

```  
#!/bin/bash  
gzdoom -iwad malapetakademo.ipk3  
```  

* Le damos permisos de ejecución, y lo ejecutamos haciendo doble click en el navegador de archivos o desde una terminal, y a disfrutar

---

Y se acabó el resumen de noticias. Como siempre podéis comentar estas noticias en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), [Telegram](https://t.me/jugandoenlinux) o en nuestro [Mastodon](https://mastodon.social/@jugandoenlinux)
