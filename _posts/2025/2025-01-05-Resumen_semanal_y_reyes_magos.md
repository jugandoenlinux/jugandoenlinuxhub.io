---
author: Serjor
category: "Apt-Get Update"
date: 05-01-2025 19:00:00
excerpt: "Resumen semanal primera semana 2025"
layout: post
tags:
- noticias
- resumen
title: "Reyes Magos, CES, y feliz año 2025"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/kurpfalzring_720.webp
---

Aprovechando que es el primer resumen del año, desde la redacción de Jugando En Linux esperamos que este año os vaya de lujo, y recordad, si habéis sido buenos, mañana los Reyes Magos nos pueden traer en el CES un anuncio de una nueva consola con SteamOS, estad atentos a nuestra cuenta de Mastodon (link al final del post).

## [Bye, bye, 2024](https://mastodon.social/@jugandoenlinux/113743018074581261)

Como cada año, parte del staff de Jugando en Linux hacemos un resumen de lo que ha sido el año, además de los juegos libres y no libres que más nos han gustado este año que termina mañana.

[Leer más en Jugando en Linux](https://jugandoenlinux.com/posts/resumen-anual/)

También os animamos a comentar cómo ha sido vuestro año jugando en Linux, ya sea en PC, Steam Deck, etc.

Por último, os deseamos a todos un feliz y próspero año nuevo de parte de nuestro staff.



## [Dentro de ti, hay una estrella.... (si has pillado esto, eres muuuuy mayor)](https://mastodon.social/@jugandoenlinux/113747787953210887)

Si esta noche quieres dar rienda suelta a tus capacidades (o no) de canto 🎵, te recomendamos el juego UltraStar Deluxe.

Es un clon del famoso SingStar pero libre ❤️ donde puedes cantar a modo de karaoke con un micrófono y según cómo afinas te puntuará.

En Linux 🐧 se puede instalar fácilmente como flatpak:
[flathub.org/apps/eu.usdx.UltraStarDeluxe](https://flathub.org/apps/eu.usdx.UltraStarDeluxe)

Y las canciones se pueden meter en la carpeta:
`~/.var/app/eu.usdx.UltraStarDeluxe/.ultrastardx/songs/`



## [A ver si llegamos a tiempo!!](https://mastodon.social/@jugandoenlinux/113749095845972447)

En GOG regalan el juego Vambrace: Cold Soul por tiempo limitado:  
[https://www.gog.com/en/game/vambrace_cold_soul](https://www.gog.com/en/game/vambrace_cold_soul)

Nativo para Linux 🐧



## [Trata de arrancarlo, Carlos!!](https://mastodon.social/@jugandoenlinux/113753293974560068)

¡Celebramos la llegada del 2025 con una nueva versión de Stunt Rally! La 3.3 incluye tres nuevas pistas, mejoras gráficas y en la interfaz.

Podéis ver la lista de cambios completa en:  
[https://github.com/stuntrally/stuntrally3/blob/main/docs/Changelog.md](https://github.com/stuntrally/stuntrally3/blob/main/docs/Changelog.md)



## [Los juegos open source comienzan el año a tope](https://mastodon.social/@jugandoenlinux/113757755269023423)

Hace poco que estrenamos el nuevo año y ya tenemos dos buenas noticias de dos grandes juegos de Código Abierto:

Por un lado, el equipo de [Speed Dreams](https://mastodon.social/@speed_dreams_official) ha terminado la migración de SVN a GIT, un proceso que les llevó un tiempo. [Más información](https://mastodon.social/@speed_dreams_official/113747048283798620)

Por otro lado, [0 A.D.](https://mastodon.social/@play0ad) publicó ayer una imagen con el Coloso de Rodas anunciando que la tan esperada Alpha 27 saldrá el primer trimestre de este año. [Más información](https://mastodon.social/@play0ad/113755216763972447)



## [Hasta el infinito y más allá](https://mastodon.social/@jugandoenlinux/113758824053825347)

Cerramos 2024 con un gran año en Linux 🐧 como sistema para el videojuego y las estadísticas 📈 lo corroboran de nuevo.

En Steam, diciembre cerró con un 2.29% de usuarios en Linux/SteamOS.

Por otro lado, en StatCounter la tendencia se mantiene por encima del 4% (sin contar el casi 2% de ChromeOS que viene a ser otro Linux).

Si además en este 2025 llegan nuevas "consolas" de otras marcas con SteamOS/Linux, es probable que el dato mejore. 🚀



## [Porque hay cosas que nunca pasan de moda](https://mastodon.social/@jugandoenlinux/113774690360650044)

Hace poco se ha lanzado el tráiler de presentación de Batocera 41, una popular distro Linux enfocada al retrogaming. Entre sus novedades tenemos:

* Soporte para múltiples pantallas.
* Añadidos motores libres para Doom 3, Star Wars: Dark Forces, etc.
* Nuevos emuladores, como Hypseus Singe (Laserdisc)
* Añadido soporte para diversos dispositivos
* Actualizaciones de motores y emuladores ya presentes

Podéis ver el tráiler en https://www.youtube.com/watch?v=G_8JI5HSDM4

<iframe width="560" height="315" src="https://www.youtube.com/embed/G_8JI5HSDM4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



Animaros a comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).
