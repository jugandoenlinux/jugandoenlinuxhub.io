---
author: Son Link
category: "Carreras"
date: 02-02-2025 20:00:00
excerpt: "Despues de meses de desarrollo, se lanza la nueva versión de este simulador de carreras"
image: https://www.speed-dreams.net/user/pages/03.blog/01.release-2-4-0/NewRelease240@3x.jpg
layout: post
tags:
  - carreras
  - Open Source
  - Speed Dreams
  - SimRacing
title: "Speed Dreams 2.4.0"
---
Aprovechando la **FOSDEM 2025**, el equipo de Speed Dreams ha lanzado la versión 2.4.0.

Esta es la primera versión que se lanza tras la marcha del proyecto de su principal desarrollador, **Xavier Bertaux**, de lo cual hablamos en este mismo blog por el lanzamiento de su fork, [Cars Sports Racing](https://jugandoenlinux.com/posts/Cars-Sports-Racing-1_0/), así como tras migrar el proyecto de **SVN y SourceForge** a [Git y Forgeo](https://forge.a-lec.org/speed-dreams/)

## Novedades

Son varias las novedades que trae esta nueva versión, entre ellas:

* Gestor de descargas: Una de las novedades más importantes es que incorpora un gestor de descargas, mediante el cual podremos añadir más coches y pistas, si bien aún quedan migrar varios de ellos. Además, esto también ha ayudado a que el tamaño de descarga del juego se reduzca considerablemente.
* Nuevos sonidos: Debido a que varios de los sonidos no contaban con licencia libre, el equipo ha estado trabajando en incorporar y añadir sonidos que si lo sean. En [este vídeo](https://youtu.be/ys9RqZ-SaYI) podéis escuchar varios de ellos.
* Reducción de requisitos: Algo de lo que pocos juegos pueden presumir es que una nueva versión reduce los requisitos mínimos del sistema:
    * **OpenGL 2.1** o superior (se había pasado a 3.3.0 tras la versión 2.3.0)
    * **GLSL 1.1** o superior (se había pasado a 3.3.0 tras la versión 2.3.0)
    * **162 MiB** para Debian/Ubuntu más **107 MiB** para los paquetes .deb
    * Se han implementado un limitador de FPS, reduciendo el consumo de la CPU, tanto en los menús como jugando.
    * Se han reducido de **58 a 7** las librerías compartidas por los conductores, reduciendo así el consumo de memoria.

* Nuevo modelo de neumático, con degradación y temperatura del neumático mucho más realistas.
* Posibilidad de elegir compuesto de neumático (**Blando**, **Medio**, **Duro**, **Lluvia** y **Lluvia extrema**).
* Eliminado el tiempo registrado y sustituido por una fórmula que toma datos reales, con posibilidad de elegir la estación.
* OSG Hud completamente renovado, con nuevos widgets y la posibilidad de personalizarlo usando *Ctrl+8*.
* Toneladas de mejoras en el trackeditor, permitiendo un mayor control sobre la colocación de objetos en pista.
* Los cambios introducidos en **Simu 4.1** se han añadido y mejorado en **Simu 5.0**

Hay varios cambios más a niveles técnicos. Si queréis saber más, aquí tenéis [la noticia en su web](https://www.speed-dreams.net/es/blog/release-2-4-0)

Si os gustan los simuladores de carreras, seguro que este juego os encantara.


Podeis comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).
