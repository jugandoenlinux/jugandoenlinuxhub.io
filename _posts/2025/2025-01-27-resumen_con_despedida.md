---
author: P_Vader
category: "Apt-Get Update"
date: 27-01-2025 09:42:00
excerpt: "Resumen semanal de las noticias publicadas en Mastodon en las 2 últimas semanas"
layout: post
tags:
- noticias
- resumen
title: "Resumen semanal y una despedida"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20250127/deadlock.webp
---

Esta semana el resumen llega con retraso y es que por desgracia tenemos una despedida en Jugando en Linux.
Nuestro apreciado compi Serjor deja sus tareas en JugandoEnLinux (por tercera vez?), es que le cuesta irse y a veces vuelve. Como siempre desde JEL te deseamos lo mejor con un fuerte "Hasta Luego" y nos seguimos leyendo.

**Recordad que JugandoEnLinux es una comunidad abierta a la colaboración de cualquier entusiasta de nuestro sistema operativo. Si tienes algo que aportar ¡Anímate y danos un toque!**

Vamos con esas noticias que han circulado por Mastodon estas dos últimas semanas:

## [Deadlock no para](https://mastodon.social/@jugandoenlinux/113849280207637670)

Salió una nueva actualización de Deadlock el último juego de Valve, el cual trae 4 nuevos héroes con los que podremos jugar en las partidas públicas: Holliday, Víbora, El Magnífico Sinclair y Felina.

Además se ha puesto fin al evento de invierno y los que hayan participado lo verán reflejado en sus logros.

[Noticia oficial](https://store.steampowered.com/news/app/1422450/view/786541361952194832?l=spanish)

---

## [Final Fantasy 7 en la Deck](https://mastodon.social/@jugandoenlinux/113850424477027855)

El remake de uno de los juegos JRPG más emblemáticos de la PlayStation, Final Fantasy 7, va a salir verificado para Steam Deck.

Siempre es bueno que empresas grandes como SquareEnix se molesten en que sus juegos salgan verificados para la Deck.

Hay que decir que este juego es la segunda parte de la trilogía que compone el remake del Final Fantasy 7.

Saldrá el 23 de Enero en [Steam](https://store.steampowered.com/app/2909400/FINAL_FANTASY_VII_REBIRTH/)

---

## [Emuladando tempranamente la PS4:](https://mastodon.social/@jugandoenlinux/113876660738712760)

Desde que Sony empezó a portar sus videojuegos a PC, cada vez son menos los juegos de PS4 que son exclusivos de dicha consola. 

Uno de los que quedan sin publicarse en PC y con más renombre es Bloodborne.

Recientemente, gracias al emulador shadps4, se puede ejecutar Bloodborne en Linux.

Podéis instalar el emulador de PS4 para Linux en cualquier distribución gracias a Flatpak y [Flathub](https://flathub.org/apps/net.shadps4.shadPS4)

---

## [Nuevo SDL3](https://mastodon.social/@jugandoenlinux/113873185950933535)

Han publicado recientemente la primera versión oficial de SDL 3, que incluye una gran cantidad de novedades.

Para quien no la conozca, SDL es una biblioteca que ha sido clave a la hora de portar y crear videojuegos para Linux y que soporta una gran cantidad de plataformas.

Entre las mejoras tenemos nuevas API para GPU, cámaras web, tabletas gráficas, etc. Además se ha realizado mucho trabajo en mejorar la documentación.

[Su historial de cambios](https://github.com/libsdl-org/SDL/releases/tag/release-3.2.0)

---

## [Empresas que apuestan por lo libre](https://gamedev.lgbt/@nokorpo/113862350408440236)

NOKORPO:

> "Somos una empresa colaborativa española de desarrollo de videojuegos especializada en software libre.
> 
> 🌱 Publicamos videojuegos y herramientas bajo licencias de software libre, y nos preocupamos por que corran en Linux.
> 
> 🛠️ Programamos herramientas libres para el Fediverso, porque creemos en la filosofía de este lugar.
> 
> 🤝 Trabajamos con Blender, Godot, Linux, y otras herramientas de software libre, e intentamos promover su uso en la industria."


Seguiremos sus andaduras sin duda. 

---

## [¡¿Un Souls a la inversa?!](https://mastodon.social/@jugandoenlinux/113854520440033587)

A veces te encuentras genialidades como esta:

[The Dark Queen of Mortholme](https://qwertyprophecy.itch.io/mortholme)

PRUEBALO.

---

Terminado el resumen de noticias. Como siempre podéis comentar estas noticias en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), [Telegram](https://t.me/jugandoenlinux) o en nuestro [Mastodon](https://mastodon.social/@jugandoenlinux)