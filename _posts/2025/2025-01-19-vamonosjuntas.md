---
author: P_Vader
category: "Editorial"
date: 2025-01-19 12:00:00
excerpt: "Apoyo a la inciativa VamonosJuntas"
layout: post
tags:
- editorial
title: "Nos fuimos hace tiempo, pero #VamonosJuntas"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/VamonosJuntas/vamonosjuntas.webp
---

Aprovechando la **iniciativa [VamonosJuntas](https://vamonosjuntas.org/) desde Jugando En Linux apoyamos e invitamos a mas gente a abandonar las redes sociales privativas en favor del Fediverso.**
Dado el devenir de sitios como X es comprensible que muchas personas estén hartas de la hostilidad y toxicidad, que unicamente es aprovechada para crear adicción y  negocio.

Desde JEL hemos procurado ser consecuentes con el software libre como la mejor opción, y siempre que hubo ocasión hemos querido tener herramientas y plataformas libres frente a las privativas. Véase el caso de Telegram y Matrix, Discord y Mumble/Jitsi o Twitter y Mastodon.

Nuestra historia en Twitter ha sido mayormente la de ser una herramienta para divulgar nuestro contenido, que poco a poco pasó a ser irrelevante y finalmente abandonarla.

El primer tropiezo con Twitter fue al cerrar su API, que nos dejaba como única forma de publicar la manual. El pequeño grupo de gente que compone esta sitio no le sobra el tiempo y poco a poco fuimos dandonle menos uso. A esto se sumaba los constantes cambios de políticas y desvaríos de la plataforma, dando lugar a lo que es hoy: **un vertedero (peligroso).** 

Desde 2018 que abrimos nuestra cuenta en Mastodon poco a poco fuimos intentando mantener el mismo contenido que en Twitter. Viendo que pasaba con la que sería X hace mas de dos años decidimos invertir mas tiempo en Mastodon y probar que resultado nos daba. 
Paso a paso y de manera natural nuestro cuenta en Mastodon fue creciendo a cotas que sinceramente no esperábamos, una red donde se va cosechando las interacciones sin algoritmos forzados y donde hemos conseguido muchísimos mas usuarios que X y lo mejor, con una mayor interacción.

Como anunciamos, hace mas de un año se consensuó en JEL la transformación de nuestra web y comunidad, **eso incluía que nuestro primer canal para todo iba a ser Mastodon. Teniamos claro que el Fediverso nos da la seguridad que no nos da otros medios.**

Oye ¿Por qué no os vais a Bluesky que hay mas gente y no es un vertedero? Lee la última frase del anterior párrafo, por seguridad. Bluesky se basa en un protocolo abierto pero está en manos de inversores en busca de beneficios que en cualquier momento puede revertir todo lo andado. Y sinceramente no tenemos ganas de otra red mas de lo mismo. Somos muy pocos y no nos sobra el tiempo.

Tenemos un usuario en bluesky donde un compi iba publicando algo de vez en cuando a modo de espejo de Mastodon, pero mayormente la cuenta era por reservar el nombre y evitar suplantaciones. **No interactuamos allí.**

En el caso de X ya la teníamos abandonada, avisando que estábamos en Mastodon. De nuevo por seguridad mantendremos la cuenta para evitar suplantaciones (que se nos han dado a lo largo de los años), sin mas soporte.


Por último **un consejo a quien quiera probar Mastodon**. No dejamos de oir quejas de que en Mastodon no se encuentra contenido para seguir. Nuestra recomendación es que sigas o busques etiquetas, es uno de los puntos fuerte de Mastodon (nosotros usamos mucho #linuxgaming o #steamdeck) y a partir de esas etiquetas y temas que te interesen puedes encontrar otras cuentas. Es un crecimiento natural, no es forzado e inmediato por un algoritmo.
 
**Mastodon es como el Twitter de hace mucho mucho tiempo. No el basurero de hoy.**

