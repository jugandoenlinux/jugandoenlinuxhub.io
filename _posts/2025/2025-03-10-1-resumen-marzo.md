---
author: P_Vader
category: "Apt-Get Update"
date: 10-03-2025 17:50:00
excerpt: "Resumen quincenal de las noticias publicadas en Mastodon"
layout: post
tags:
- noticias
- resumen
title: "Heroic, NextFest o algunas recomendaciones."
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20250310/heroic.webp
---

Por fin llegó el esperado resumen quincenal, con cosas muy interesantes y variadas de lo acontecido en nuestra cuenta de [Mastodon](https://mastodon.social/@jugandoenlinux)

---

## [Otra vez el Next Fest de Steam](https://mastodon.social/@jugandoenlinux/114065770681152931):

Una buena manera de dar a conocer juegos siempre han sido las demos.  
Steam las promociona en sus Next Fest donde muchos desarrolladores, grandes y pequeños, presentan sus juegos.

[Aquí](https://store.steampowered.com/sale/nextfest) podéis verlo que juegos hubo.  


¿Has probado alguna y quieres compartírnoslo?

Os dejo un juego que me gustó: [Genokids](https://store.steampowered.com/app/1239360/Genokids/).

---

## [Escribimos sobre Deadlock](https://mastodon.social/@jugandoenlinux/114074556956000006):

Valve ha lanzado una nueva gran actualización de su último juego, Deadlock, con muchas y jugosas novedades, cambiando muchos aspectos del juego, especialmente el mapa. [Aquí el artículo](https://jugandoenlinux.com/posts/deadlock-2025-02-25/).

---

## [Portando Xbox360 a Linux](https://mastodon.social/@jugandoenlinux/114092698238743757):

XenonRecomp es una herramienta, no oficial, que permite recompilar ejecutables de Xbox 360 en ejecutables para Windows y Linux.

Si te preguntas que tal funciona, lo cierto es que sus desarrolladores ya han portado el videojuego Sonic Unleashed a Linux.

- [XenonRecomp](https://github.com/hedge-dev/XenonRecomp)  
- [UnleashedRecomp](https://github.com/hedge-dev/UnleashedRecomp)

---

## [Un nuevo Heroic llegó](https://mastodon.social/@jugandoenlinux/114093270800085905):

[@heroiclauncher](https://mastodon.social/@heroiclauncher) es un lanzador de videojuegos para diferentes tiendas como Epic o GOG que ha ido ganando popularidad por lo bien que hace las cosas.

Acaba de actualizar a la versión 2.16 con muchísimos arreglos para mejorar la experiencia con tus juegos:  
[Anuncio](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher/releases/tag/v2.16.0)

Una novedad importante en Linux/SteamOs es que a partir de ahora usará por defecto [Unified Launcher](https://github.com/Open-Wine-Components/umu-launcher) (UMU) para los juegos que usen Proton.

---

## [Mazmorras Software Libre](https://mastodon.social/@jugandoenlinux/114093903097122122):

Shattered Pixel Dungeon ⚔️ es un RPG tipo roguelike de mazmorra clásica y con un estilo pixelado.
Lo mejor es que es Software Libre ❤️ y acaba de lanzar su [versión 3.0](https://shatteredpixel.com/blog/shattered-pixel-dungeon-v300.html).

Se puede obtener de muchas maneras, pero si te gusta y deseas colaborar, lo puedes comprar en diferentes plataformas:  
[Steam](https://store.steampowered.com/app/1769170/Shattered_Pixel_Dungeon/)  
[GOG](https://www.gog.com/game/shattered_pixel_dungeon)  
[Itch](https://shattered-pixel.itch.io/shattered-pixel-dungeon)

Sígelo en [@ShatteredPixel](https://mastodon.gamedev.place/@ShatteredPixel)

---

## [Recomandación de juego](https://mastodon.gamedev.place/@Terebimagazine/114098517785576291) por [Terebi Magazine](https://mastodon.gamedev.place/@Terebimagazine):


> El primero que os recomendamos es "Is this seat taken?", de Poti Poti Studio. Tendremos que ir colocando a diferentes personajes por el escenario según sus preferencias, mientras nos cuentan la historia de uno de ellos, que no termina de sentir que encaja.
> Lo puede encontrar en [Steam](https://store.steampowered.com/app/3035120/?snr=1_5_9__205).

Sigue a [Terebi Magazine](https://mastodon.gamedev.place/@Terebimagazine) en Mastodon.

---

## [Super ayuda para VR](https://mastodon.social/@jugandoenlinux/114116671578029405):

Aunque la realidad virtual 😎 no es una tecnología que termine de llegar, es bastante espectacular y divertida cuando se usa en video juegos por ejemplo.

En Linux también se puede utilizar y hoy queremos compartiros un proyecto web donde recopilan muchísima información sobre la VR en Linux:  
[https://vronlinux.org/](https://vronlinux.org/)

Tienen una base de datos en la que los usuarios comentan su experiencia en según que juegos, muy parecido a ProtonDB:  
[https://db.vronlinux.org/](https://db.vronlinux.org/)

---

## [El mando recomendado](https://mastodon.social/@edlinks/114116728063969660) de [Eduardo Medina](https://mastodon.social/@edlinks)


> Como he acabado harto del pésimo soporte que 8BitDo da a Linux, me he tirado a la piscina para comprar un GuliKit KK3 Pro.

> Es mi segundo día con el mando y mi opinión puede cambiar, pero he podido darle la caña suficiente como para al menos ofrecer unas primeras impresiones.

> El principal motivo por el que me he comprado el GuliKit KK3 Pro es debido a que los mandos de 8BitDo, con su pincho, resultaban incompatibles con los juegos nativos para Linux hechos con Unity.


---

## [Two Point Museum en Linux](https://mastodon.social/@jugandoenlinux/114120239997003082):

Two Point Museum es el último juego de la saga creada por Two Point Studio y ha sido publicado, recientemente, en Steam con versión nativa para Linux.

Inspirado en clásicos como Theme Hospital o Theme Park, encantará a los aficionados de los juegos de gestión y estrategia.

En [Steam](https://store.steampowered.com/app/2185060/Two_Point_Museum/).

---

## [Nueva versión Retrodeck](https://mastodon.social/@jugandoenlinux/114137413764122416):

Publicada nueva versión de Retrodeck.  
Incluye multitud de correcciones y actualizaciones de nuevas versiones de emuladores.

Podéis ver la lista completa de cambios en su wiki:  
 (https://retrodeck.readthedocs.io/en/latest/wiki_rd_versions/version_0.9.0b/0.9.1b/)

---

Y hasta aquí el resumen de noticias. Como siempre podéis comentar estas noticias en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), [Telegram](https://t.me/jugandoenlinux) o en nuestro [Mastodon](https://mastodon.social/@jugandoenlinux)