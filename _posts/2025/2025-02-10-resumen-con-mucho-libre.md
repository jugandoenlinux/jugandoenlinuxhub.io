---
author: P_Vader
category: "Apt-Get Update"
date: 10-02-2025 18:24:00
excerpt: "Resumen de las noticias publicadas en Mastodon en las últimas semanas"
layout: post
tags:
- noticias
- resumen
title: "Mucho juego libre en este resumen"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Resumen20250210/kingdom-come-deliverance-2.webp
---

Otro par de semanas con un ritmo bajito de noticias, aunque intentamos siempre dejaros cosas interesantes en nuestra cuenta de [Mastodon](https://mastodon.social/@jugandoenlinux). Allá vamos con el resumen:

---

## [Kingdom hasta en la sopa o hasta en Linux](https://mastodon.social/@jugandoenlinux/113941710788211011):

[Kingdom Come Deliverance 2](https://store.steampowered.com/app/1771300/Kingdom_Come_Deliverance_II/), el esperado simulador RPG de la edad media, ha recibido la calificación de jugable para la Steam Deck por parte de Valve.

El juego está recibiendo críticas bastante positivas en diferentes medios y hay reportes de usuarios que indican que se puede jugar en la Deck por encima de los 30 FPS en ajustes bajos y medios.

---

## [Nuevo OpenHV](https://mastodon.social/@jugandoenlinux/113975702996348351):

Ha sido lanzada una nueva versión de OpenHV un juego de estrategia en tiempo real que usa el motor openra. Entre los cambios trae:

+ Nuevas voces  
+ Rehechos varios sprites  
+ 4 nuevos mapas y eliminado otro  
+ Corregidos textos de las facciones  
+ Corregido el comportamiento de las unidades aéreas al atacar  
+ Añadida una unidad de observación  
+ Añadidas unidades de aviones AA

Más información en la página de [descargas](https://github.com/OpenHV/OpenHV/releases/tag/20250209)

---

## [XBox360 en Linux](https://mastodon.social/@jugandoenlinux/113974825182281063):

La verdad es que a nivel de emulación en Linux no nos podemos quejar. Tenemos versiones nativas de casi todos los emuladores. Por desgracia hay algunos que todavía no tienen versión nativa. 

¡ La buena noticia es que de esa lista ya podemos tachar Xenia, el popular emulador de Xbox 360 ! Recientemente sus desarrolladores han sacado una versión experimental con [ejecutable para Linux](https://github.com/xenia-canary/xenia-canary-releases/releases).


---

## [Port curioso y libre](https://mastodon.social/@jugandoenlinux/113969486871085056):

El usuario [@nevat](https://social.linux.pizza/@nevat/113957824770197893) ha portado a Godot el juego gratuito Abbaye des Morts de Locomalito y Gryzor87.

El juego original se creo usando el motor privativo GameMaker, y ha decidido crear este port para aprender a usar Godot, y solo esta para linux y para arquitecturas x86, amd64, arm32 y arm64.

Tanto el código, bajo licencia GPL3, como las descargas de los ejecutables, [aquí ](https://git.disroot.org/nevat/abbayedesmorts-godot)lo tenéis.

---

## [Aventura gráfica de la buena](https://mastodon.social/@jugandoenlinux/113967446963041554):

Hace mucho que probamos el prólogo del juego Slender Threads y nos maravilló el nivel de esta aventura gráfica del estudio argentino blyts.

Por fin han lanzado el juego y no podemos más que recomendarlo en [Steam](https://store.steampowered.com/app/1116480/Slender_Threads/)

Está nativo para Linux 🐧 y en oferta de lanzamiento.

Si quieres probarlo, busca su prólogo que lanzaron como adelanto en Steam.

---

## [Nuevo Speed Dreams](https://mastodon.social/@jugandoenlinux/113936617307809819):

Tras meses de desarrollo, y aprovechando la celebración de la fosdem, se ha publicado la versión 2.4.0 de [@speed\_dreams\_official](https://mastodon.social/@speed_dreams_official), con varias novedades, hasta ha sido sometido a dieta

[El artículo](https://jugandoenlinux.com/posts/speed-dreams-2.4.0/)

---

## [0AD ha sido una larga espera](https://mastodon.social/@jugandoenlinux/113917571647257718):

Por fin la (larguísima) espera ha terminado: ha sido lanzado 0 A.D. Alpha 27.

Esta nueva versión trae muchas novedades y mejoras. 

[El artículo.](https://jugandoenlinux.com/posts/0ad-alpha-27/)

---

## [SPRAWL 96](https://mastodon.social/@jugandoenlinux/113901838106352377):

¿Te gusta los Quake originales y/o los FPS donde apenas tienes un respiro?

SPRAWL 96 es una reconversión total de Quake y en el tendremos que deslizarnos para saltar más lejos, deslizarnos por las paredes, incluso un modo Tiempo Bala, donde se ralentizara el juego durante un tiempo.

El juego esta disponible en flathub y usa LibreQuake y el motor Darkplaces y no requiere una copia original de Quake.

[Flatpak](https://flathub.org/apps/io.github.voidforce.qsprawl)

<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/XzOye97fWz0?si=w04nNxQo1qG7vxly" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

Y se acabó el resumen de noticias. Como siempre podéis comentar estas noticias en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), [Telegram](https://t.me/jugandoenlinux) o en nuestro [Mastodon](https://mastodon.social/@jugandoenlinux)