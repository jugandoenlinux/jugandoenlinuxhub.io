---
author: Son Link
category: "Apt-Get Update"
date: 13-01-2025 14:00:00
excerpt: "Resumen semanal segunda semana 2025"
layout: post
tags:
- noticias
- resumen
title: "Un legionario, más protones y algo más"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/protonup-qt/pupgui2.webp
---

Como cada semana, os traemos el resumen semanal, con pocas, pero jugosas novedades

## [¿A quién no le va a gustar una legión lenova del siglo 21?](https://mastodon.social/@jugandoenlinux/113788257527565724)

Ya es oficial, Lenovo Legión Go S será la siguiente consola Pc portátil que usará SteamOS con la colaboración de Valve, según confirman ambas compañías a The Verge.

Tendrá un precio de salida de 499$ lo que la convierte en un gran "competidor" para Valve, teniendo en cuenta que mejorará características de la deck incluso que igualará o mejorará la duración de batería.

## [Más protones)](https://mastodon.social/@jugandoenlinux/113747787953210887)

ProtonUp-Qt 🔧facilita la instalación y gestión de diferentes versiones de proton entre aplicación como Steam, Heroic launcher o Lutris. Se ha actualizado a la versión 2.11.1 con un par de novedades y algunos arreglos:  
- Se ha añadido compatibilidad con proton-ge-rtsp  
- Se ha añadido la opción de usar WineZGUI en las instalaciones.

Lo puedes instalar desde flathub (en steam deck por ej.) o usando la appimage desde su [web](https://davidotek.github.io/protonup-qt/)



## [¡No puedo esperar!](https://mastodon.social/@jugandoenlinux/113749095845972447)

Linus Tech Tips no puede esperar más para que SteamOS, un Linux, conquiste el mundo:


<div class="resp-iframe">
	<iframe src="https://www.youtube.com/embed/tdR-bxvQKN8?si=znFpPum1hAHmID3J"></iframe>
</div>



Animaros a comentar esta noticia en nuestros grupos de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).
