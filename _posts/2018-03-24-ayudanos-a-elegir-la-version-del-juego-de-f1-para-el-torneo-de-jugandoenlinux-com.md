---
author: Serjor
category: Carreras
date: 2018-03-24 16:08:10
excerpt: "<p>F1 2015 o F1 2017, he ah\xED la cuesti\xF3n...</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/068055ed933d2e69cfeb9ff2d23bac50.webp
joomla_id: 693
joomla_url: ayudanos-a-elegir-la-version-del-juego-de-f1-para-el-torneo-de-jugandoenlinux-com
layout: post
tags:
- competitivo
- comunidad
- f1-2017
- f1-2015
- torneo
title: "Ayudanos a elegir la versi\xF3n del juego de F1 para el torneo de jugandoenlinux.com\
  \ (Actualizaci\xF3n)"
---
F1 2015 o F1 2017, he ahí la cuestión...

Actualización: El ganador ha sido F1 2015. Os pedimos a todos los que habéis votado que paséis por el hilo del foro para apuntaros y coordinarnos.


Actualización: El hilo en el foro con más explicaciones y para coordinarnos lo tenéis [aquí](index.php/foro/foro-general/104-mundial-f1-jugandoenlinux-com)


 


Este fin de semana ha dado comienzo el campeonato de F1 2018, y para celebrar esta ocasión os [comentamos](index.php/homepage/generos/carreras/item/812-consigue-f1-2015-completamente-gratis) que tenemos unas ofertas interesantes para poder hacernos con el F1 2015 de manera gratuita o para probar el F1 2017 durante este fin de semana, y poder hacernos con él para siempre a precio reducido.


El caso es que en jugandoenlinux.com nos gusta mucho la F1 (aunque se puede ver que se nos da muy mal conducir como se puede apreciar en el vídeo que acompaña el artículo), por lo que hemos pensado en organizar un torneo de F1 con la gente de la comunidad, de tal manera que cada fin de semana que haya carrera en el mundial, nosotros corramos esa semana también en ese circuito virtual (u otro a elegir si no está incluido en el juego).


El caso es que tenemos un dilema que queremos que nos ayudéis a resolver, ¿F1 2015 o F1 2017?


Os dejamos una encuesta para que nos ayudéis a decidir. Seguramente la encuesta la cerremos el domingo al mediodía más o menos para poder dar tiempo a la gente a hacerse con el F1 2017 de oferta, si es que interesa. Aunque la encuesta no podemos cerrarla, y podéis encontrar el enlace [aquí](https://www.strawpoll.me/15351733/r), oficialmente ya hemos terminado nuestras votaciones, y el ganador ha sido el F1 2015:


![votacion torneo f1 2018](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/F1%202015/votacion_torneo_f1_2018.webp)


Para qué veais en qué fregados os podríais meter, os dejamos con el vídeo de la partida de la comunidad de este viernes, que casualmente ha sido con el F1 2017:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/lLUBSeRjUX8" width="560"></iframe></div>


A parte del tema de la versión del juego, abriremos un hilo en el foro para gestionarnos durante la temporada, pero eso iremos avisando cuando esté.


Ya sabes, corre, y vota a tu opción preferida.

