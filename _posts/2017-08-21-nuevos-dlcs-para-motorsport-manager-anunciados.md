---
author: Serjor
category: Deportes
date: 2017-08-21 15:51:39
excerpt: "<p>Descuento del 66% en el juego principal hasta el final del GP de B\xE9\
  lgica</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/665e96c29d55b13435d7a8d39deafe53.webp
joomla_id: 440
joomla_url: nuevos-dlcs-para-motorsport-manager-anunciados
layout: post
tags:
- oferta
- dlc
- motorsport-manager
title: Nuevos DLCs para Motorsport Manager anunciados
---
Descuento del 66% en el juego principal hasta el final del GP de Bélgica


> 
> Today's the day! New Challenge Pack DLC & a whopping free update is launching later. 66% off base game! Deets -> <https://t.co/OiGkLmsbaz> [pic.twitter.com/L3417cX45q](https://t.co/L3417cX45q)
> 
> 
> — Motorsport Manager (@PlayMotorsport) [21 de agosto de 2017](https://twitter.com/PlayMotorsport/status/899646630617505796)



Vía twitter nos enterábamos esta mañana de que la gente de Playsport Games anunciaba por un lado un nuevo DLC de pago, Challenge Pack, donde nos propondrán nuevos desafíos basados en eventos de la historia del automovilismo, y también un DLC gratuíto, El diablo está en los detalles, con nuevos añadidos como ajustar la IA de los pilotos o guardar la configuración del coche entre otros.


<div class="steam-iframe"><iframe frameborder="0" height="190" src="httpS://store.steampowered.com/widget/639680/" width="646"></iframe></div>


Así mismo, y para aquellos que no teneís el juego, desde hoy a las 19:00 hasta que termine el gran premio de Bélgica, el juego tendrá una rebaja del 66% (así que si lees esto antes de que lancen el descuento, aguanta un poco).


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/415200/" width="646"></iframe></div>


Tenéis el anuncio completo [aquí](https://steamcommunity.com/games/415200/announcements/detail/2436865112319185785) y si queréis podéis leer el [análisis](index.php/homepage/analisis/item/388-analisis-motorsport-manager) del juego.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/BEfs87EdT3o" width="560"></iframe></div>


Y tú, ¿qué piensas de lo nuevo de Motorsport Manager? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

