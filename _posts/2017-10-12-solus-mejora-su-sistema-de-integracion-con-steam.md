---
author: Serjor
category: Software
date: 2017-10-12 13:58:04
excerpt: "<p>Las distribuciones GNU/Linux contin\xFAan en su carrera por ofrecer el\
  \ mejor entorno para jugar</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e3017719190ff1e83db12ee09e7beada.webp
joomla_id: 482
joomla_url: solus-mejora-su-sistema-de-integracion-con-steam
layout: post
tags:
- steam
- solus
title: "Solus mejora su sistema de integraci\xF3n con Steam"
---
Las distribuciones GNU/Linux continúan en su carrera por ofrecer el mejor entorno para jugar

Vía [reddit](https://www.reddit.com/r/linux_gaming/comments/75pq3q/solus_developers_fix_steam_fullscreen_video_issue/?st=j8oj5i1n&sh=3b976b61) leemos que la gente de [Solus](https://solus-project.com/) ha mejorado su ya excelente gestor de la integración de Steam con GNU/Linux.


Para los que no conozcan Solus, es una distro rolling release muy particular, donde han llegado a desarrollar un escritorio nuevo, Bundgie, que es realmente recomendable. De hecho, tiene hasta su propio spin dentro de la familia Ubuntu.


A parte de Bundgie, la gente de Solus también desarrolla otros componentes, y uno que destaca entre los jugadores es su aplicación para integrar Steam en el sistema, esto es, hacer que Steam use o no las librerías del sistema operativo o no activando o desactivando un switch.


Pues bien, ahora han añadido una nueva opción, y es que de estar activada Solus detectará e interceptará qué librerías va a usar el cliente de Steam, haciendo que por defecto linke con las librerías del sistema y el cliente sólo podrá usar las librerías que él instala en caso de que estén en una lista blanca.


Según los desarrolladores al hacer que el propio cliente use las librerías del sistema consiguen evitar algunos bugs de librerías obsoletas.


Es muy interesante ver cómo la comunidad se mueve para circunvalar problemas en aplicaciones de terceros.


Y tú, ¿usas Solus como tu distro para jugar? Cuéntanos qué distro usas en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

