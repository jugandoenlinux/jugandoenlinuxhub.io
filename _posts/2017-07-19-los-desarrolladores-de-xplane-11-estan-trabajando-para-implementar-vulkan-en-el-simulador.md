---
author: Pato
category: "Simulaci\xF3n"
date: 2017-07-19 12:05:46
excerpt: "<p>La API estar\xE1 disponible para Linux entre otros sistemas para mejorar\
  \ el rendimiento</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4fa58a4e3bff7c98c12520b84ea1dcd5.webp
joomla_id: 412
joomla_url: los-desarrolladores-de-xplane-11-estan-trabajando-para-implementar-vulkan-en-el-simulador
layout: post
tags:
- simulador
- vulkan
title: "Los desarrolladores de Xplane 11 est\xE1n trabajando para implementar Vulkan\
  \ en el simulador"
---
La API estará disponible para Linux entre otros sistemas para mejorar el rendimiento

Durante una conferencia ofrecida en vivo por Laminar Research, empresa responsable de la saga de simuladores Xplane en su [canal oficial de Youtube](https://www.youtube.com/channel/UCii88kCi1QERIVBa-bqydJg), varios de los responsables del simulador incluido el propio Austin Meyer han respondido a varias preguntas de los usuarios que han asistido a la misma, y entre ellas una en concreto llamó mi atención por que supone la confirmación de la apuesta por el desarrollo multiplataforma y las nuevas APIs del estudio.


En concreto, preguntados por la API Vulkan los responsables han comentado que están trabajando actívamente para portar el motor gráfico del simulador a Vulkan (y tienen intención de implementar Metal para sistemas MAC). Con este movimiento esperan ofrecer soporte multiplataforma en los sistemas con soporte para dicha API y a su vez mejorar el rendimiento ya que según sus palabras esto les permitirá superar las limitaciones de APIs más antiguas y mejorar el rendimiento.


Si estás interesado en ver la conferencia, a continuación tienes el vídeo. La respuesta sobre Vulkan está en el minuto 32'22:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/a9NAvy8HcVc" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Xplane es una saga de simuladores de vuelo realista multiplataforma que ofrece soporte en Linux desde hace años, y que en su última versión Xplane 11 ha cosechado muy buenas críticas y comentarios de los usuarios.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/Ov0Sm9GDha4" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Si estás interesado en este tipo de simuladores, puedes probar la demo y comprarlo en su [página web oficial](http://www.x-plane.com/), o también lo tienes disponible en Steam donde está disponible en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/269950/" width="646"></iframe></div>


¿Qué te parece que cada vez más desarrollos estén apostando por implementar Vulkan? ¿Has "volado" en Xplane?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

