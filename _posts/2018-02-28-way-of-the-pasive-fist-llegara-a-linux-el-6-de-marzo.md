---
author: Pato
category: "Acci\xF3n"
date: 2018-02-28 12:46:28
excerpt: "<p>El juego de acci\xF3n de Household Games ya tiene fecha de salida en\
  \ Steam</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/dd9b4b0a80eff28fc65a7f105431f022.webp
joomla_id: 661
joomla_url: way-of-the-pasive-fist-llegara-a-linux-el-6-de-marzo
layout: post
tags:
- accion
- indie
- beat-em-up
title: "'Way of the Pasive Fist' llegar\xE1 a GNU-Linux/SteamOS el 6 de Marzo"
---
El juego de acción de Household Games ya tiene fecha de salida en Steam

El típico post de SteamDB nos puso sobre la pista de este "brawler" atípico, que será lanzado el próximo día 6 de Marzo. Se trata de un juego de acción arcade en 2d y gráficos "de la vieja escuela" donde tendremos que esquivar, bloquear y embestir a los enemigos para poder sobrevivir.


*"Una misteriosa figura, conocida únicamente como lo Errante, viaja a través del lejano planeta Zircon V, una antigua y próspera colonia minera que ahora está al borde de la destrucción debido a su agonizante estrella. Mediante la técnica del Puño Pasivo, lo Errante puede salvarse de cualquier enemigo bloqueando sus ataques y agotando su energía. Como los colonizadores han ido desapareciendo y los maniáticos andan sueltos, lo Errante debe mantenerse firme, en calma, y luchar para obtener respuestas en este brawler único y moderno!"*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/4MMdb_Zu2HA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 En 'Way of the Pasive Fist' [[web oficial](http://www.wayofthepassivefist.com/)] tendremos que tener muy en cuenta el ritmo y el tiempo a la hora de definir los combates, ya que de ello dependerá nuestro éxito. Ejecutar las acciones adecuadas en el momento oportuno será crucial y por tanto tendremos que estudiar los patrones de los enemigos, ejecutar combos y super-ataques y bloquear los golpes para desgastar la energía de los contrincantes.


Además de esto tendremos la posibilidad de cambiar el ritmo de los combates para adecuarlo a nuestro nivel de dificultad preferido, podremos configurar los controles a nuestro antojo y tendremos dos modos de juego, arcade e historia.


En cuanto a la versión de Linux/SteamOS, el juego ya aparece en la [Humble Store](https://www.humblebundle.com/store/way-of-the-passive-fist) con Linux como sistema soportado y cuenta con los requisitos mínimos en su página de Steam:


**Mínimo:**  

+ **SO:** Ubuntu 12.04 LTS
+ **Procesador:** 1.8Ghz
+ **Memoria:** 1 GB de RAM
+ **Gráficos:** Geforce 6800 / Radeon X1300, o mas nuevo (OGL3.2 + GLSL1.5)
+ **Red:** Conexión de banda ancha a Internet
+ **Almacenamiento:** 100 MB de espacio disponible
+ **Tarjeta de sonido:** Tarjeta de sonido o mejor integrado.


Además, la propia desarrolladora nos ha confirmado el lanzamiento del juego en nuestro sistema favorito:



> 
> Yes, absolutely! :)
> 
> 
> — Household Games (@HHGamesInc) [February 28, 2018](https://twitter.com/HHGamesInc/status/968870854569336832?ref_src=twsrc%5Etfw)



 


Si quieres echarle un vistazo, puedes verlo en su página de Steam donde estará disponible el próximo 6 de Marzo traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/625680/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>
 


