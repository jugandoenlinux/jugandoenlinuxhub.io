---
author: Son Link
category: "Exploraci\xF3n"
date: 2022-12-06 16:42:00
excerpt: "<p>Ya esta disponible la nueva gran actualizaci\xF3n de Valheim: Mistlands</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/rumbo_a_mistlands.webp
joomla_id: 1510
joomla_url: valheim-rumbo-a-mistlands
layout: post
tags:
- supervivencia
- actualizacion
- valheim
title: "(Actualizaci\xF3n) Valheim: Mistlands"
---
Ya esta disponible la nueva gran actualización de Valheim: Mistlands


### Actualización (6/12/2022):


La espera ha finalizado, desde hace pocas horas ya esta disponible esta nueva gran actualización de Valheim, Mistlands (Tierras Brumosas), que corresponda a la **0.212.7**, y tras nueve meses de desarrollo.


Como paso con la anterior nos han dejado un maravilloso tráiler animado, protagonizado por los mismos personajes y el cual podéis ver a continuación:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="440" src="https://www.youtube.com/embed/TyHAvmBKK50" title="Valheim: Mistlands Animated Release Trailer" width="780"></iframe></div>


Recuerda que este bioma solo se generara en zonas inexploradas del mundo, así que si ya has viajada mucho, quizás sea una buena idea empezar uno nuevo.


¡Que Odin os proteja!




---


  
Hace meses que Iron Gate Studio anuncio el próximo lanzamiento de la próxima gran actualización, Mistlands (Tierras Brumosas). Hoy ya es posible visitar este nuevo bioma antes de su lanzamiento final apuntándonos a la beta pública.


Este nuevo bioma, que ya estaba presente desde el inicio, aunque vació (solo gigantescos árboles, telarañas y calaveras), pasa a ser el bioma más peligroso del juego, al que iremos tras derrotar al que hasta ahora es el último jefe, Yagluth, en las Praderas. Estará lleno de nuevos y peligrosos enemigos, por lo que para sobrevivir tendremos que tener la mejor equipación posible, además de alimentarnos lo mejor posible.


Para "disfrutar" del nuevo paisaje en nuestras partidas tendremos que viajar hasta una zona que previamente no haya sido visitada, todas las Tierras Brumosas que hayamos visitado, o por las que hayamos pasado muy cerca, se mantendran tal cual. Si tienes una partida donde hays explorado mucho, lo mejor es empezar una nueva partida.


El nuevo contenido que trae, entre otros:


#### Nuevas mecánicas:


* 9 nuevas criaturas + el jefe de las Tierras Nublosas
* Más de 20 nuevos materiales de artesanía
* 2 nuevas estaciones de artesanía, 3 extensiones de estaciones de artesanía y otras 3 construcciones de recursos/artesanía
* 15 nuevos alimentos
* 3 nuevas pociones
* Más de 25 nuevos objetos artesanales (armas, armaduras y herramientas)
* Más de 35 nuevas piezas de construcción/muebles para construir, decorar y defender tu base
* Nuevo tipo de mazmorra
* Nuevas piedras de historia
* Nuevos sueños
* Nueva música


#### Otros:


* Actualización de la pesca
* 12 nuevos emoticonos
* 9 nuevos peinados y 7 nuevos estilos de barba


#### Correcciones y mejoras:


* Varias mejoras en los comandos de la consola
* Se han actualizado y mejorado múltiples animaciones
* Se han actualizado y mejorado varios efectos visuales
* Otros ajustes varios


### Como unirse a la beta:


Para unirse a la Beta hay que seguir los siguientes pasos:


* Antes de nada, es recomendable hacer una copia de seguridad de los archivos de guardado y de los mundos que tengas, ya que una vez que entres a la beta no podrás volver atrás, además de por sí estos se dañan durante la beta. Estos están en **~/.config/unity3d/IronGate/Valheim/**
* Abre **Steam** y pulsa en tu biblioteca sobre **Valheim** con el botón derecho del ratón o dentro de la ficha del juego, pulsar sobre la rueda dentada que está arriba a la derecha, y selecciona **Propiedades**.
* Selecciona Betas e introduce el siguiente código: **yesimadebackups**
* Una vez introducido podrás seleccionar unirte a la beta.


Si te unes recuerda colaborar reportando fallos, comentando que te parece esto o aquello, para que los desarrolladores lo soluciones antes de la salida de la actualización estable, bien en [Discord](https://discord.gg/valheim) y/o los [subforos de Steam](https://steamcommunity.com/app/892970/discussions/5/)


Tenéis más información, **CON SPOILERS**, en la noticia [es su web](https://valheim.com/es/news/mistlands-public-test/), así como el tráiler, también con spoilers, en [YouTube](https://www.youtube.com/watch?v=cZOuBjvETR8)


Aún no hay fecha de lanzamiento, aunque entre la beta y la salida de la versión estable de la anterior gran actualización pasaron unas 2 semanas, así que si la cosa marcha, dentro de poco, todos podremos viajar hasta estas nuevas zonas.


¡Que Odin sea con vosotros!


Y aprovecho que hace poco un servidor (mi personaje se llama Ylvie), **Carla** y **Lhrod** (de izquierda a derecha respectivamente) acabamos con Yagluth en el servidor de la comunidad.


![photo 2022 11 20 18 22 25](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/photo_2022-11-20_18-22-25.webp)

