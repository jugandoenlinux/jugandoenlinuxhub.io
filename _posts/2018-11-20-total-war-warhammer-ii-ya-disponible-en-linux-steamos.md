---
author: Pato
category: Estrategia
date: 2018-11-20 18:11:59
excerpt: <p>Un nuevo port gracias a @feralgames ... y van...</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b80ea3ea38e44cb9b41c65c79ffde000.webp
joomla_id: 906
joomla_url: total-war-warhammer-ii-ya-disponible-en-linux-steamos
layout: post
tags:
- feral-interactive
- estrategia
title: 'Total War: Warhammer II ya disponible en Linux/SteamOS'
---
Un nuevo port gracias a @feralgames ... y van...

Gran noticia para los que gustamos de los juegos de estrategia. [Tal y como prometieron](index.php/homepage/generos/estrategia/8-estrategia/938-total-war-warhammer-ii-llegara-a-linux-steamos-este-proximo-otono) los chicos de Feral Interactive ya tenemos disponible su nuevo port de la saga Total War: WARHAMMER II. 


*"Total War: WARHAMMER II, la segunda entrega de la trilogía y secuela del juego de Creative Assembly que ha ganado multiples premios, Total War: WARHAMMER, ofrece a los jugadores una nueva e imponente campaña narrativa que se desarrolla en los enormes continentes de Lustria, Ulthuan, Naggaroth y las Tierras del Sur."*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/VUJce5B_WdM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


### Dirige Altos Elfos, Hombres Lagarto, Elfos Oscuros o Skaven en la campaña del Gran Vórtice


Sumérgete en una intensa y atractiva campaña jugando como uno de los ocho Señores legendarios pertenecientes a cualquiera de las cuatro asombrosas razas. Debes realizar una serie de rituales arcanos para controlar El Gran Vórtice, una poderosa vorágine de enorme energía mágica.


### Conquista mundial


Emplea tu arte para gobernar, entabla relaciones diplomáticas, explora y construye tu imperio turno tras turno. Has que tus líderes militares — Señores legendarios y Héroes — suban de nivel y equípalos con armas y armaduras mágicas. Recluta enormes ejércitos y captura asentamientos, negocia alianzas o declara la guerra total para someter a todo aquel que se interponga en tu camino.


### Épicas batallas en tiempo real


Dirige gigantescas legiones de guerreros preparados para la batalla. Ordena atacar a feroces y deformes monstruos, dragones que escupen fuego y domina poderosa magia. Emplea estrategias militares o usa la fuerza bruta para cambiar el curso de una batalla y llevar a tus tropas a la victoria.


### Conquista dos mundos en la campaña Imperios Mortales


Quienes poseen WARHAMMER y WARHAMMER II están invitados a embarcarse en la campaña Imperios Mortales que tiene lugar en el Viejo y el Nuevo Mundo.


 


Si quieres conseguir Total War: WARHAMMER II y apoyar al estudio que lo ha portado, puedes comprarlo en la misma [tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/warhammer2tw/?utm_source=Newsletter&utm_medium=email&utm_campaign=null_Newsletter) (**recomendado**) o en su [página de Steam](https://store.steampowered.com/app/594570/Total_War_WARHAMMER_II/) donde también contará como venta en Linux.


¿Qué te parece el nuevo port de Feral Interactive? ¿Te gustan los ports que nos traen?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [discord](https://discord.gg/ftcmBjD).

