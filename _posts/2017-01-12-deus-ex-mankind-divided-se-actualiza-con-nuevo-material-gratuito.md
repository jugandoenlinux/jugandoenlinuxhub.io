---
author: leillo1975
category: "Acci\xF3n"
date: 2017-01-12 14:40:54
excerpt: "<p>Los fan\xE1ticos de Adam Jensen estamos de enhorabuena</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/48689e827932dc70ec0a6e6067e8a72b.webp
joomla_id: 174
joomla_url: deus-ex-mankind-divided-se-actualiza-con-nuevo-material-gratuito
layout: post
tags:
- deus-ex
- dlc
- mankind-divided
- feral
title: 'Deus Ex: Mankind Divided se actualiza con nuevo material gratuito'
---
Los fanáticos de Adam Jensen estamos de enhorabuena

Hace unos dias, concretamente el 7 de Enero , la última aventura de nuestro aumentado favorito se actualizó en Steam con un parche bastante voluminoso. Picado por la curiosidad me dirijí inmediatamente al departamento de prensa de [Feral Interactive](http://www.feralinteractive.com/es/) para preguntar por dichas novedades. Hoy he recibido respuesta, y en primer lugar me han pedido disculpas por la tardanza. En cuanto a la actualización me comentan que en este parche se incluyen gratuitamente varias cosas que en su día se ofrecían en la versión de coleccionista. En este caso no hay ninguna corrección de errores o mejora de rendimiento, Las novedades que vamos a encontrar son las siguientes:


 


-Una misión extra llamada Medidas desesperadas


-Varios Skins para adam donde podemos encontrar armaduras y vestimentas, nuevos aspectos para la pistola y el rifle de combate; y varios consumibles.


-También se pueden descargar 3 libros digitales


 


La verdad es que es un gesto muy de agradecer para todos los amantes de la Saga Deus Ex. Esperemos en un futuro poder disponer de más contenido gratuito de este tipo. Si quereis más información sobre el parche podeis consultar el [anuncio oficial.](http://steamcommunity.com/games/337000/announcements/detail/641034377684596720)


¡¡¡Bravo Square Enix y bravo Feral Interactive!!!


 

