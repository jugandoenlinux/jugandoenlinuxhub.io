---
author: Serjor
category: Software
date: 2019-02-26 21:48:04
excerpt: "<p>Scummvm tiene compa\xF1\xEDa nueva en el mundo de implementaciones libres\
  \ de motores para aventuras gr\xE1ficas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/73d7edd1405081e5433c41d9ad2ee18c.webp
joomla_id: 983
joomla_url: engge-un-motor-open-source-para-poder-jugar-a-thimbleweed-park
layout: post
tags:
- open-source
- engge
- thimbleweed-park
title: "Engge, un motor open source para poder jugar a Thimbleweed Park\u2122"
---
Scummvm tiene compañía nueva en el mundo de implementaciones libres de motores para aventuras gráficas

Vía [reddit](https://www.reddit.com/r/linux_gaming/comments/av0tg3/engge_the_open_source_reimplementation_of/) nos enteramos de la existencia de Engge, una reimplementación de código abierto del motor del videojuego Thimbleweed Park™.


Como anuncian en su página de [GitHub](https://github.com/scemino/engge), el motor está en una fase experimental y, aunque la intención es poder jugar al juego completo. Eso sí, para poder jugarlo hay que tener los ficheros originales, con lo que previamente habrá que adquirir el videojuego, aunque podremos ejecutarlo con un motor libre.


La verdad es que este tipo de proyectos son un ejemplo de ingenio que no siempre se reconoce, y esperemos que el proyecto llegue a buen puerto, y se pueda ejecutar el juego completo con él.


Os dejamos un vídeo que muestra hasta donde han podido llegar hasta ahora:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/09VEPoX5SZk" width="560"></iframe></div>


Y tú, ¿jugarías a Thimbleweed Park™ con esta implementación libre? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

