---
author: Pato
category: Noticia
date: 2021-07-14 16:31:46
excerpt: "<p>Google no se da por vencida, y sigue atrayendo nuevos proyectos y caracter\xED\
  sticas exclusivas. \xBFAlguien dijo FIFA 22?</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/stadia072021.webp
joomla_id: 1324
joomla_url: stadia-incentivara-a-los-estudios-y-les-hara-la-vida-mas-facil-para-entrar-en-la-plataforma
layout: post
tags:
- stadia
- google
title: "Stadia incentivar\xE1 a los estudios y les har\xE1 la vida m\xE1s facil para\
  \ entrar en la plataforma"
---
Google no se da por vencida, y sigue atrayendo nuevos proyectos y características exclusivas. ¿Alguien dijo FIFA 22?


Después de ciertas turbulencias en la plataforma de videojuegos de Google que llevó al **cierre de sus estudios internos** para centrarse en atraer desarrollos de terceros (sacando a pasear la billetera para arrancarles los ports como es lógico), todo parecía indicar que **Stadia** no iba por tan buen camino como nos hacían creer. Sin embargo, con la promesa de que llegarían 200 nuevos juegos durante el 2021 y el progresivo goteo de anuncios, parece que Google no se quiere dar por vencida en esto del videojuego en streaming.


Respecto al número de títulos, todo parece indicar que van por buen camino y si no llegan a 200 nuevos títulos a final de año se quedarán muy cerca, y por la parte de incentivar a los estudios acabamos de saber [mediante una *keynote*](https://www.youtube.com/watch?v=I0oNK-XHp0I)que Stadia **va a rebajar la comisión que se lleva por los títulos que lleguen a la plataforma al 15% desde finales de año y hasta el 2023**, eso sí con límite a los primeros 3.000.000 de dólares y **solo a títulos que lleguen al servicio de suscripción Stadia Pro**. Esto puede que no vaya dirigido a títulos triple A, pero puede atraer a estudios más modestos y hacerlos estar en el plan Stadia Pro durante más tiempo, aunque otra cosa es cómo calculen los ingresos, ya que por ventas las cuentas están claras, pero cuando entra en juego el servicio de suscripción el cálculo de ingresos se hace con el tiempo que dedican los jugadores en cada título. Como veis, Stadia no termina de definir un modelo que sea claro y los incentivos no parece que siquiera se acerquen a otras plataformas (Xbox Game Pass... cof cof... EGS... cof cof...). Y siendo Google por dinero no será.


Volviendo a los títulos, las novedades más destacadas son la llegada de **Streets of Rage 4** (ya disponible) y que ya está disponible la [pre-compra](https://stadia.google.com/store/details/d45d390acba5479db7c47170b09d517drcp1/sku/83e8fd585222451790d89f3a279e9391p) de **FIFA 2022**, anuncio que viene con polémica ya que será la única plataforma aparte de las consolas de nueva generación que recibirá la nueva característica estrella del juego, la tecnología *HiperMotion* que utiliza el aprendizaje automático para unas animaciones y movimientos mucho más fluidos. Si, Windows se queda *fuera de juego*.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/MXKRiSLJ3k0" title="YouTube video player" width="560"></iframe></div>


Puedes ver los títulos y novedades de la plataforma [en este enlace](https://stadia.google.com/games).


En otro orden de cosas, según leemos en [9to5google.com](https://9to5google.com/2021/07/13/stadia-porting-toolkit-windows-games/) Stadia está trabajando en una **herramienta para facilitar los ports desde Windows** hacia la plataforma de forma que los desarrolladores encuentren menos problemas a la hora de traer sus juegos. En concreto se habla de una capa de compatibilidad para que no haya que trasladar la API gráfica de DirectX a Vulkan (en Google saben usar un buscador y poner DXVK) o herramientas de análisis de código para poder compilar código de 32 a 64 bits, entre otras cosas.


Con todo y con esto, Stadia de momento sigue siendo un quiero y no puedo. Lo cierto es que es un servicio que aún padece de cierta *indefinición*respecto a cosas tan básicas como planes de suscripción poco claros, funcionalidades de la plataforma o una tienda que palidece en características e información respecto a casi cualquier otra del mundo videojueguil, unido a la falta de mucho "marketing" (el hecho de que tuvieran que explicar cómo funciona el servicio **un año después** de su puesta en marcha nos dice mucho sobre cómo de bien están promocionando la plataforma) e información (aún hay muchas personas que aún estando interesadas no saben que **no hace falta una suscripción** para jugar en Stadia).


En cualquier caso, recordar que aunque es un servicio de streaming **los servidores de Stadia se mueven gracias a Debian con la API gráfica Vulkan**, con lo que seguimos *jugando en Linux*.


Seguiremos al tanto de las novedades de Stadia.


¿Que te parecen las nuevas condiciones de la plataforma para los desarrolladores? ¿Piensas jugar FIFA 2022 en todo su esplendor en Stadia?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

