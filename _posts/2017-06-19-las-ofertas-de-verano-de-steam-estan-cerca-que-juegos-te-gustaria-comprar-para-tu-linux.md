---
author: Pato
category: Ofertas
date: 2017-06-19 12:30:16
excerpt: "<p>Seg\xFAn varias filtraciones las ofertas de este a\xF1o dar\xE1n comienzo\
  \ este Jueves d\xEDa 22</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6489cb4e35e6273902967d50b104e32a.webp
joomla_id: 378
joomla_url: las-ofertas-de-verano-de-steam-estan-cerca-que-juegos-te-gustaria-comprar-para-tu-linux
layout: post
tags:
- steam
- ofertas
title: "Las ofertas de verano de Steam est\xE1n cerca: \xBFque juegos te gustar\xED\
  a comprar para tu Linux?"
---
Según varias filtraciones las ofertas de este año darán comienzo este Jueves día 22

Este año no hemos tenido una filtración "oficial" sobre la fecha en la que darán comienzo las rebajas de Steam, pero si han habido ciertas filtraciones de supuestos avisos que reciben los estudios sobre las rebajas que se realizarán en la plataforma con el fin de que estén preparados y preparen sus ofertas.


Por ejemplo, hay varios hilos en [Reddit con imágenes](https://www.reddit.com/r/Steam/comments/6aec94/steam_summer_sale_2017_date/) en las que se puede ver un aviso con el comienzo de las rebajas para el 22 de Junio. Esto es, ¡dentro de 3 días!


Por otra parte, la también "fiable" [whenisthenextsteamsale.com](http://whenisthenextsteamsale.com/)  tiene puesto su contador para la misma fecha, eso sí con un "no confirmado" por si las moscas; Pero como se suele decir, cuando el río suena... ¡Ofertas a la vuelta de la esquina!


 Por mi parte, tengo una extensa lista de títulos que espero, y algunos seguro que caerán a poco que se pongan a tiro como por ejemplo "**Anima: Gate of Memories**", "**Candle**", "**Disgaea 2**" o "**MicroMachines**". Y vosotros...


¿Tenéis preparada la cartera? ¿la tarjeta de crédito? ¿Qué títulos estáis esperando en oferta para echarles el guante? ¿Es muy larga vuestra lista de deseados?


Contármelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

