---
author: Pato
category: Apt-Get Update
date: 2017-04-14 17:36:33
excerpt: <p>Viernes Santo, buen momento para un breve repaso de la semana</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ceec46ac75e6be7f73874fd5573b298d.webp
joomla_id: 291
joomla_url: apt-get-update-mr-shifty-asura
layout: post
title: Apt-Get Update Mr Shifty & Asura % ...
---
Viernes Santo, buen momento para un breve repaso de la semana

Primero de todo, hoy el Apt-Get va a ser un poco más conciso, ya que el equipo de Jugando en Linux andamos disfrutando de unos días de asueto...


Comenzamos recomendando la lectura de un interesantísimo artículo de Muylinux donde Eduardo Medina reflexiona sobre las ventajas que puede suponer volcar los ports de juegos y otro software que requiera de gráficos intensivos de Directx a Vulkan en vez de a OpenGL. Podeis verlo [en este enlace](http://www.muylinux.com/2017/04/14/portando-directx-11-opengl-vulkan).


Dos lanzamientos que me gustaría resaltar, y que no hemos podido tratar estos días, son:


- 'Mr Shifty' es un juego se acción en vista cenital con un interesante desarrollo en el que tenemos el poder de teletransportarnos rápidamente para acabar con nuestros enemigos. Recuerda mucho a poderes que hemos visto en películas como Xmen:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/GMHXflP-Vf0" width="640"></iframe></div>


Está traducido al español y puedes encontrarlo en su página de Steam con un 10% de descuento por su lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/489140/" width="646"></iframe></div>


 


- 'Asura' es un juego "Hack 'n Slash" de tipo "roguelike" basado en la mitología india:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/WZmE9mDUpik" width="640"></iframe></div>


 No está traducido al español, pero si eso no es problema lo tienes disponible en su página de Steam con un 10% de descuento por su lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/524640/" width="646"></iframe></div>


 


Esto es todo en este Apt-Get. Es corto si, pero la semana que viene volveremos a la carga de la forma habitual. Hasta entonces, como siempre estaremos en nuestros canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

