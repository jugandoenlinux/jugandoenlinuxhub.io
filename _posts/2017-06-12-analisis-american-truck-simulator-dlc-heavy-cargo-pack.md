---
author: leillo1975
category: "An\xE1lisis"
date: 2017-06-12 13:09:59
excerpt: "<p>Te desgranamos las caracter\xEDsticas de la nueva DLC de SCS Software</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a01253be6ea05e1e8a72b1a2a0636467.webp
joomla_id: 370
joomla_url: analisis-american-truck-simulator-dlc-heavy-cargo-pack
layout: post
tags:
- dlc
- american-truck-simulator
- ats
- scs-software
- heavy-cargo-pack
title: "An\xE1lisis: American Truck Simulator - DLC Heavy Cargo Pack"
---
Te desgranamos las características de la nueva DLC de SCS Software

Os ponemos en antecedentes. Hace justo un mes se lanzaba la DLC Heavy Cargo Pack para Euro Truck Simulator 2 (que [probábamos en un video](https://youtu.be/s1SaGrMcTXQ) en nuestro canal de Youtube), y poco antes llegaba la [actualización gratuita que permitía remolques dobles](index.php/homepage/generos/simulacion/item/430-scs-software-permitira-camiones-de-doble-remolque-en-sus-juegos). Unos días atrás os informábamos de la llegada de la misma DLC, pero [en este caso para American Truck Simulator](index.php/homepage/generos/simulacion/item/485-lanzado-el-dlc-heavy-cargo-para-american-truck-simulator), la cual venía a completar la experiencia de los transportes especiales en los juegos de la compañía checa.


 


Y en esto vamos a ocupar este mini-análisis que vamos a hacer a esta interesante DLC. Como siempre y en primer lugar nos gustaría mostrar **nuestro agradecimiento a SCS Software** por la clave facilitada para la elaboración de este artículo, ya que sin ella no podríamos ofreceros este contenido. Sin más preámbulo vamos al lio.


 


Lo primero que tenemos que decir sobre esta DLC es que como bien su nombre indica **nos va a permitir llevar equipamentos muy pesados** entre los que vamos a encontrar tractores oruga, excavadoras, carretillas elevadoras, montacargas, rollos enormes de cable, fresadoras, gruas, transformadores... es decir, mercancías de no fácil transporte que nos van a obligar a manejar remolques especiales. En cuanto a estos últimos se caracterizan por un elevado tamaño especialmente en el largo, lo que va a obligar a presentar múltiples puntos de articulación y una buena cantidad de ejes y ruedas para facilitar la maniobrabilidad del camión, así como para evitar la fricción de las ruedas. En la trasera y delantera advertiremos el cartel indicador para los vehiculos con los que compartimos carretera de "OVERSIZE LOAD".


 


![Articulado](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisATS_HCP/ATS_HC01.webp)


 *Los remolques disponen de múltiples puntos de artículación*


 


 En cuanto a la conducción sentiremos notables diferencias, siendo la más evidente el tener que **afrontar las intersecciones y giros pronunciados con mucho más cuidado**, obligandonos estos a tener que medirnos mucho más y abrirnos para evitar comernos muros, señales, edificios... o simplemente evitar salirnos de la carretera. También notaremos que **nuestro camión será sensiblemente más lento**, siendo la aceleración mucho menor, lo cual es lógico al llevar más peso. En cuanto a lo contrario, **frenar será mucho más costoso** y recorreremos muchos más metros hasta llegar a la velocidad adecuada que con una carga convencional, por lo que debemos ser mucho más prudentes y anticiparnos mucho más a la ruta y el tráfico. Para frenar consecuentemente es muy necesario usar el freno-motor y el retarder, por lo que poner estas opciones en automático puede ser tremendamente útil. He advertido también que **el consumo de combustible es bastante superior al normal**, por lo que nos veremos obligados a reponer con más frecuencia.


 


Hay que destacar también que ahora, cuando vayamos a "enganchar" nuestro mega-remolque se nos mostrará en la puerta del almacén donde la recogeremos un **nuevo diálogo donde nos indicará la idoneidad del camión** que conducimos para transportar este tipo de mercancías, por lo que se aconseja que para estos viajes utilicemos un cabeza tractora con la máxima potencia que podamos tener, y un chasis adecuado con una buena cantidad de ejes para que el peso vaya más repartido en ellos.


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisATS_HCP/ATS_HC04.webp)*Este diálogo nos indicará la idoneidad de nuestro camión*
 


Todas estas nuevas "inconveniencias" van a incrementar notablemente la dificultad del transporte, aunque si bien es cierto, en American Truck Simulator las carreteras son más sencillas que en Euro Truck Simulator 2, pues normalmente nos moveremos por carreteras bastante anchas y/o rectas que no suponen un reto tan elevado como en la DLC de su hermano europeo. Hablando de ETS2 - Heavy Cargo Pack, hay que decir que **los remolques americanos difieren mucho de los europeos**, ya que estos últimos no presentan tantas articulaciones, y además las ruedas del remolque son direccionales, pudiendo girar para facilitar las maniobras en gran medida.


 


Otro detalle que no se me ha escapado, y que podría ser uno de los pocos defectos que presenta Heavy Cargo Pack, es que no vemos en el tráfico otros camiones que estén realizando este tipo de trabajos. **Podría ser interesante cruzarnos por una carretera estrecha con otro transporte especial** para ponernos las cosas más difíciles. Supongo que los desarrolladores estarán viendo esta posibilidad para una futura actualización del juego, y si no es así, aquí lo dejo como una idea.


 


![escavadora](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisATS_HCP/ATS_HCP02.webp)*Podremos transportar las más variadas y voluminosas cargas*
 


Como conclusión cabe decir que esta DLC es super-recomendada para todos los que disfrutamos de este juego, pues como dije va a añadir mayor variedad al juego, además de aumentar considerablemente la dificultad a la hora de realizar los trabajos, por lo que, desde mi punto de vista, es un auténtico "must-have" para todos los jugones de American Truck Simulator, además de tener un precio más que recomendable. Si quereis, **podeis ver el video** que hemos realizado para nuestro canal de Youtube en el que realizamos un viaje con una una de estas cargas especiales:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/RFpH124ustQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 Podeis comprar ATS: Heavy Cargo Pack en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/620610/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Que os parece esta expansión? ¿Sois fans también de los juegos de SCS Software? Deja tus opiniones en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord.](https://discord.gg/ftcmBjD)

