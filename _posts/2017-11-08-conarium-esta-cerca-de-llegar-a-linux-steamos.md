---
author: Pato
category: Terror
date: 2017-11-08 16:33:37
excerpt: <p>Se trata de un juego inspirado en una novela de H.P. Lovecraft</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/389b1419d386024814b9710bfcb9f5a1.webp
joomla_id: 520
joomla_url: conarium-esta-cerca-de-llegar-a-linux-steamos
layout: post
tags:
- indie
- aventura
- terror
- hp-lovecraft
title: "'Conarium' est\xE1 cerca de llegar a Linux/SteamOS"
---
Se trata de un juego inspirado en una novela de H.P. Lovecraft

Ya hace unos días que ha pasado Halloween, pero hay aún juegos de temática "terrorífica" que siguen en desarrollo para Linux/SteamOS. En concreto, hace ya unos meses [nos hicimos eco](index.php/homepage/generos/terror/item/462-los-desarrolladores-de-conarium-estan-trabajando-en-una-version-del-juego-para-linux-steamos) de que este 'Conarium' estaba siendo desarrollado para Linux, aunque luego los desarrolladores han estado en silencio hasta ahora, que un usuario ha vuelto a preguntar sobre el estado del desarrollo para nuestro sistema favorito.


La respuesta no deja lugar a dudas:


"*Estamos trabajando actualmente en la versión para Linux, esperamos habilitarla para la próxima actualización"*


Puedes ver la respuesta original [en este enlace](http://steamcommunity.com/app/313780/discussions/0/1319962514590534729/?tscn=1510141635).


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/LNUzrWv50EI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 *Conarium es un espeluznante juego de terror cósmico en el que descubrirás una trama apasionante en la que participan cuatro científicos que tratan de desafiar a lo que normalmente consideramos los límites «absolutos» de la naturaleza. La historia, inspirada en la novela de H.P. Lovecraft «En las montañas de la locura», narra, en su mayor parte, los acontecimientos posteriores al relato original.*


¿Qué te parece 'Conarium'? En Jugando en Linux estaremos atentos a las novedades sobre este juego así como de su fecha de salida.

