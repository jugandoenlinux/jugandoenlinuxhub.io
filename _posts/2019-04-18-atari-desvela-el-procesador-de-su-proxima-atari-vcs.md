---
author: Pato
category: Hardware
date: 2019-04-18 08:15:37
excerpt: "<p>Hasta ahora solo sab\xEDamos que ser\xEDa una APU con n\xFAcleos Zen\
  \ y gr\xE1fica Vega</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/535e73c0cd26e4bd6923084a855b0e04.webp
joomla_id: 1022
joomla_url: atari-desvela-el-procesador-de-su-proxima-atari-vcs
layout: post
tags:
- hardware
- atarivcs
title: "Atari desvela el procesador de su pr\xF3xima Atari VCS"
---
Hasta ahora solo sabíamos que sería una APU con núcleos Zen y gráfica Vega

Mediante una [nota de prensa](https://www.amd.com/en/press-releases/2019-04-16-amd-expands-embedded-product-family-adds-design-wins-and-customers-new?utm_source=Subscribers&utm_campaign=aca752517b-EMAIL_CAMPAIGN_2018_12_22_05_00_COPY_01&utm_medium=email&utm_term=0_7859bb25ac-aca752517b-16958109), indirectamente AMD ha desvelado lo que parecía un secreto a voces. Hasta ahora tan solo conocíamos que sería una **APU con núcleos Zen y gráfica Vega** el procesador que llevaría la próxima consola de Atari, la **Atari VCS** pero ahora AMD ha desvelado su serie de APU's **Ryzen Embedded R1000** incluyendo una mención expresa a las palabras del director de operaciones de dispositivos de Atari, Michael Arzt:


*"Con el AMD Ryzen Embedded R1000 impulsando la Atari VCS, podemos respaldar el contenido HDR de 4K a 60 fps que los usuarios esperan de un sistema moderno y seguro de juegos y entretenimiento. Con el nuevo Ryzen Embedded SoC de AMD También ayudamos a proteger el entorno y el contenido de VCS, ya que admitimos un modelo de acceso abierto sin precedentes que permite a la comunidad altamente creativa de Atari instalar cualquier otro sistema operativo junto con el sistema operativo Atari".*


Está claro entonces que estamos ante el que será el procesador que mueva a la próxima consola de Atari con corazón Linux. En concreto se han desvelado las características de dos modelos:


![R1000APU](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/R1000APU.webp)


Como se puede apreciar, la única diferencia entre los dos modelos son las frecuencias  base y turbo, y la frecuencia de la parte gráfica, contando ambos con dos núcleos Zen con cuatro hilos de procesamiento y 3 unidades de cómputo en la gráfica Vega. Habrá que ver cual de los dos modelos acaba integrando Atari en su nueva máquina.


Además de esto, esta APU puede d**ecodificar vídeo H.265 4K**, tiene capacidad para red **ethernet dual a 10Gb** y soporte para **Secure Boot y encriptado de memoria**. Para saber más, podéis visitar la web de promoción de la serie R1000 de AMD donde podéis ver características y mucho más en [este enlace](https://www.amd.com/en/products/embedded-ryzen-r1000-series).


A continuación, el vídeo de promoción:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/rsVHLdv27hE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


¿Que os parece el procesador de la Atari VCS? ¿Consideráis buena la gráfica Vega para mover juegos en los tiempos que corren?


Podéis contármelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

