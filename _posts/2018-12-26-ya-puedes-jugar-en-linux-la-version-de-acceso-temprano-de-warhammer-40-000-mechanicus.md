---
author: Pato
category: Estrategia
date: 2018-12-26 15:18:32
excerpt: "<p>Bulwark Studios anunci\xF3 en un comunicado la forma de acceder al juego</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/daaafcf8e134c30f448bb4ca14d1f6c4.webp
joomla_id: 939
joomla_url: ya-puedes-jugar-en-linux-la-version-de-acceso-temprano-de-warhammer-40-000-mechanicus
layout: post
tags:
- estrategia
- estrategia-por-turnos
- warhammer-40k
title: "Ya puedes jugar en Linux la \"versi\xF3n de acceso temprano\" de Warhammer\
  \ 40,000: Mechanicus"
---
Bulwark Studios anunció en un comunicado la forma de acceder al juego

Hace ya casi un año que [anunciamos](index.php/homepage/generos/estrategia/8-estrategia/779-warhammer-40-000-mechanicus-llegara-a-linux-steamos-a-finales-de-ano) que '**Warhammer 40,000: Mechanicus**', el juego desarrollado por Bulwark Studios y editado por Kasedo Games llegaría a nuestros sistemas, y ahora parece que lo tenemos al alcance de la mano.


En uno de los [últimos comunicados](https://steamcommunity.com/app/673880/discussions/2/2806204040006332078/#c1743355067084191759) del estudio anuncian el *"**lanzamiento suave**" (soft launch)* de lo que sería una versión beta del juego para Linux/SteamOS. En concreto, para poder acceder al juego hay que entrar en la rama beta del juego dentro de Steam **denominada** ***p-t-r**.* Según anuncian, es una de las primeras versiones del juego para Linux, por lo que es muy posible que os encontréis con bugs y errores específicos en ciertas distribuciones Linux. Estos errores pueden ser reportados en el hilo del juego destinado a ello [en este enlace](https://steamcommunity.com/app/673880/discussions/2/).


Por otra parte, anuncian que debido a las fiestas de Navidad no será posible dar respuesta a los errores hasta el mes que viene, y que ya está planeado el lanzamiento oficial de la versión Linux/SteamOS del juego para principios de este próximo año 2019.


*Warhammer 40,000: Mechanicus es un juego de estrategia por turnos dentro del mundo Warhammer 40,000. Ponte al mando de las tropas humanas más tecnológicamente avanzadas del Imperium: los Adeptus Mechanicus. En este juego táctico por turnos, toda decisión que tomes será clave de cara al resultado de la misión. ¿Recibirás la bendición del Omnissiah?*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/9gIMZ0WyY88" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tienes toda la información del juego en su [página web oficial](https://www.mechanicus40k.com/) o en su [página de Steam](https://store.steampowered.com/app/673880/Warhammer_40000_Mechanicus/) (aún no tenemos requisitos recomendados, pero no creemos que tarden mucho en publicarlos).


¿Que te parece la estrategia de "Mechanicus"? ¿Te gusta el universo Warhammer 40,000?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

