---
author: Pato
category: "Acci\xF3n"
date: 2017-09-08 14:15:38
excerpt: "<p>Adem\xE1s obteni\xE9ndolo (incluso gratis) obtendr\xE1s un 10% de descuento\
  \ en su pr\xF3ximo juego</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5b28f48ced011fc04be950095172835a.webp
joomla_id: 456
joomla_url: monaco-what-s-yours-is-mine-gratis-durante-unas-horas-mas
layout: post
tags:
- accion
- indie
- sigilo
- cooperativo
title: '''Monaco: What''s Yours Is Mine" gratis durante unas horas mas'
---
Además obteniéndolo (incluso gratis) obtendrás un 10% de descuento en su próximo juego

Casi llegamos tarde, pues tan solo quedan unas horas para que termine la promoción, pero aún estáis a tiempo de haceros con el notable 'Monaco: What's Yours Is Mine' ¡de forma gratuita!.


Además, obteniendo el juego, incluso gratis obtendremos un 10% de descuento en la pre-compra de su próximo juego que también llegará a Linux: [Tooth and Tail](http://store.steampowered.com/app/286000/Tooth_and_Tail/).


Monaco: What's Yours Is Mine [[web oficial](http://www.monacoismine.com/)] es un juego de atracos para jugar solo o en modo cooperativo. Reúne a un equipo de ladrones de élite, estudia el terreno y consigue dar el golpe perfecto. Juega con hasta cuatro participantes online o en una misma pantalla. Compite con otros mediante tablas de clasificación diarias. Descubre por qué ganó en el IGF de 2010 y ha sido descrito por Anthony Carboni, de Rev3Games, como "muy posiblemente el mejor juego cooperativo jamás creado".


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/hC7b6642AWM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


'Monaco: What's Yours Is Mine' no está disponible en español, pero ¡es gratis por unas horas! ¿a que estás esperando? lo tienes disponible en su página de Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/113020/206095/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

