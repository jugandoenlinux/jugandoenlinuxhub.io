---
author: Pato
category: "Acci\xF3n"
date: 2018-03-16 10:22:16
excerpt: <p>Juega contra otros jugadores en modalidades PvP</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cf0c9728d8bedab6603748ee4cc837b5.webp
joomla_id: 683
joomla_url: spellsworn-un-juego-multijugador-free-to-play-de-tipo-arena-ya-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- free-to-play
- sandbox
title: '''Spellsworn'' un juego multijugador "free to play" de tipo "arena" ya disponible
  en Linux/SteamOS'
---
Juega contra otros jugadores en modalidades PvP

Cada vez más, los juegos de tipo "free to play" de tipo "arena" se van fijando en nuestro sistema favorito para traernos nuevos desarrollos. En este caso hablamos de "Spellsworn", un juego donde tendremos que combatir en un mundo mágico dentro de una arena de batalla limitada, y donde las elecciones de echizos serán determinantes:


*Inspirado por los juegos clásicos de arena y jugador contra jugador, como League of Legends, Super Smash Brothers y Warcraft 3 Mod, Warlocks, Spellsworn es un juego PvP en el que debes usar reflejos mágicos y rápidos para burlar y aniquilar a tu oponente. Ambientado en un mundo atrapado por una guerra mágica, tú y otros lanzadores de conjuros lucharéis por el control de 5 rondas de batalla en distintas áreas.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/j4XcDCrxORk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Características:


Pon a prueba tu ingenio y reflejos en el combate multijugador lleno de hechizos, ya sea solo o en equipos: admite hasta 8 jugadores.  
Elije su carga en 5 disciplinas diferentes: Ofensiva, Defensiva, Área, Utilidad y Viaje.  
Participa en la batalla en 3 escenarios únicos, cada uno con sus propios peligros y demandas.  
Adquiere experiencia en la batalla, gane pieles, armas y más a medida que subes de nivel.


Además de esto, se organizan torneos para competir en la comunidad como por ejemplo un 2vs2 que se llevará a cabo [este mismo Domingo](http://steamcommunity.com/gid/[g:1:10235165]/announcements/detail/1651005907672172791).


'Spellsworn' no está disponible en español, pero si no es problema para ti y te van este tipo de juegos, puedes ver más detalles en su [web oficial](https://www.spellsworn.com/game/) o jugarlo en su página de Steam. ¡Es "free to play"!


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/360620/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Te gustan los juegos jugador vs jugador?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

