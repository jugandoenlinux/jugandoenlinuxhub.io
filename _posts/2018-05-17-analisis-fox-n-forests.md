---
author: leillo1975
category: "An\xE1lisis"
date: 2018-05-17 14:11:49
excerpt: "<p>Plataformas al m\xE1s puro estilo de los 90</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f886601d84b861950b399355947b6811.webp
joomla_id: 736
joomla_url: analisis-fox-n-forests
layout: post
tags:
- plataformas
- retro
- fox-n-forests
- bonus-level-entertaiment
title: "An\xE1lisis: Fox n Forests."
---
Plataformas al más puro estilo de los 90

Hoy mismo, tal y como [habíamos comentado](index.php/homepage/generos/accion/item/816-fox-n-forests-llegara-esta-primavera-a-nuestros-pc-s) hace algún tiempo, acaba de salir al mercado Fox n Forests, un juego que llega a nuestras pantallas con la premisa de ofrecernos un juego al más puro estilo de los clásicos de los 16bits (Megadrive-Genesis o SuperNintendo, entre otros), y el resultado no puede ser más satisfactorio, pues el juego lo consigue tanto para bien como para mal.


En primer lugar nos gustaría agradecer tanto a [Bonus Level Entertaiment](https://bonuslevelentertainment.com/) como a [StridePR](http://www.stridepr.com/) la clave utilizada tanto para este análisis como para el video (al final del artículo); y la colaboración que nos han brindado, facilitándonos anticipadamente la copia del juego para realizar este análisis, como por responder puntualmente a nuestros correos electrónicos. Hay precisar que **este artículo está realizado con una versión preliminar del juego**, por lo que puede que no se ajuste completamente a la versión final que todos vais a disfrutar.


Fox n Forests es un **arcade de plataformas con algunos elementos de rol** donde tendremos que guiar a nuestro protagonista, Rick el Zorro, a través de diferentes fases y estaciones para lograr librar al bosque de una maldición que se cierne sobre él. Una fuerza maligna pretende introducir una nueva estación en la naturaleza y para ello ha creado monstruos y criaturas malvadas, mitad animales, mitad plantas que harán que nuestro objetivo sea más complicado.


![FoxnForestsGameplay1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/FoxNForests/FoxnForestsGameplay1.webp)


Para ello, Rick, **dispondrá del poder de cambiar las estaciones a su antojo** (pero de forma limitada por el maná), para beneficiarse de las características de cada una de estas. Por ejemplo, se pasará del verano al invierno para congelar los rios y poder cruzarlos, o de la primavera al otoño para poder utilizar los frutos como plataformas donde saltar, o las hojas que caen como escalones. Es en esta característica donde reside la verdadera gracia del juego, ya que le otorga un elemento diferenciador con respecto a otros juegos.


Además de esta asombrosa capacidad, podremos realizar más acciones con nuestro personaje, pudiendo realizar **doble salto** para alcanzar lugares más inaccesibles; usar nuestro **arco mágico** para disparar a distancia, o usar **golpes cuerpo a cuerpo**, tanto agachados como saltando. Existe un pequeño problema en estos movimientos, en los que no sabemos si es parte del diseño del juego o es un problema, y es que si estamos disparando y de repente nos agachamos o miramos hacía arriba, nuestro protagonista no reacciona como es debido. Tenemos que realizar primero el movimiento y después el golpe. Este tipo de "problema" tambien lo vimos en su día en [Wonder Boy - The Dragon's Trap](index.php/homepage/analisis/item/564-analisis-wonder-boy-the-dragon-s-trap). Además Rick, podrá comprar golpes en la armería, así como pociones mágicas y más vida en las tiendas asignadas.


![FoxnForestsTiendas](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/FoxNForests/FoxnForestsTiendas.webp)


Existe también un elemento interesante en el juego que le aporta rejugabilidad, y es que **hay zonas e items que solo podremos acceder a ellos si disponemos de los elementos necesarios**. Por poner un ejemplo, existen dianas a las que solo podremos disparar para activarlas si tenemos la flecha adecuada. También existen items especiales y **zonas secretas** que tendremos que conseguir y descubrir. El juego, además nos obliga a volver a la zona inicial cada cierto tiempo para comprar items que nos permitan superar las fases.


Los enemigos en el juego son variados, tanto en aspecto, como en características y dificultad, y para superarlos debemos adecuarnos a cada uno de ellos, pues no todos permiten ser atacados de la misma manera, ni tienen el mismo número de puntos de vida. Además, en Fox n Forests, cada cierto número de fases, habrá un nivel especial donde tendremos que enfrentarnos a un **enemigo especial o jefe**, en el que tendremos que emplearnos más a fondo y con una mecánica diferente para poder superarlo. Existen además NPC's que nos aportarán información sobre el juego y la historia, como Patty; o el tejón Retro, un coleccionista de juegos retro que nos permitirá **guardar la partida a cambio de dinero**, y que de vez en cuando suelta algun chiste o guiño relaccionado con los juegos clásicos.


![FoxnForestsJefe](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/FoxNForests/FoxnForestsJefe.webp)


En cuanto al acabado gráfico, el juego tiene unos **escenarios cuidados y un diseño de personajes bueno**, por supuesto entendiendo que **el estudio de desarrollo se ha autoimpuesto ciertas limitaciones técnicas** a la hora de diseñarlo. Como hemos repetido a lo largo de este análisis multitud de similitudes con juegos de los 80-90, y se advierten claras influencias de juegos como [Mickey Mouse: Castle of Illusion](https://es.wikipedia.org/wiki/Castle_of_Illusion_Starring_Mickey_Mouse), **Midnight Wanderers** (de [Three Wonders](https://en.wikipedia.org/wiki/Three_Wonders)), [Wonder Boy](https://es.wikipedia.org/wiki/Wonder_Boy) o [Ghosts and Goblins](https://es.wikipedia.org/wiki/Ghosts_%27n_Goblins); y es exactamente lo que pretende ser. Si cabe, se le puede achacar que no hayan introducido algunos elementos nuevos en el proceso de creación, como la posibilidad de jugar con una resolución más alta y no tan pixelada, unos menús más trabajados o una música y efectos sonoros más actualizados; tal y como hemos visto en otros juegos retro que nos encontramos en la actualidad, como el analizado [Wonder Boy - The Dragon's Trap](index.php/homepage/analisis/item/564-analisis-wonder-boy-the-dragon-s-trap). Pero también es cierto, que en este caso se trata de un juego nuevo, y no de un remake como otros, por lo que quizás esto se pueda entender mejor y esté más justificado.


Nosotros recomendamos este título para todos los amantes de los juegos antiguos, pues el juego no les va a defraudar, y les ofrecerá una buena cantidad de horas de diversión acompañando a este zorro por los bosques de Maná. En JugandoEnLinux.com hemos grabado un video donde nos puedes ver jugar un rato mientras analizamos algunos de los puntos que comentamos este análisis:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/CeI680AXYus" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 Si quereis comprar Fox n Forests podeis hacerlo en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/603400/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Qué opinión os merece este juego de Bonus Level Entertaiment? ¿Le dareis una oportunidad a Fox n Forests? Puedes dejar tus respuestas en los comentarios o en nuestros grupos de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/fgQubVY)

