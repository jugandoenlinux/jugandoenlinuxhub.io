---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-05-28 07:32:30
excerpt: "<p>Nueva versi\xF3n de este juego libre de comercio y combates espaciales.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fbce18293084d3ac0d97e393ad7b6ff7.webp
joomla_id: 1050
joomla_url: endless-sky-se-actualiza-tras-casi-dos-anos-sin-noticias
layout: post
tags:
- mundo-abierto
- open-source
- 2d
- endless-sky
- comercio
title: "Endless Sky se actualiza tras casi dos a\xF1os sin noticias."
---
Nueva versión de este juego libre de comercio y combates espaciales.

Mucha gente desprecia injustamente los proyectos de juegos de software libre, diciendo que no están a la altura, que no ofrecen nada nuevo, o que están desfasados tecnológicamente, pero el problema que tienen es que con sus prejuicios en muchas ocasiones se pierden grandes juegos . Además siempre hay que tener en cuenta las herramientas, tiempo y dedicación de sus creadores, que desinteresadamente ceden sus creaciones a todo el mundo, ya no solo ofreciendo gratuitamente su juego, sinó facilitando el código a cualquiera que le interese colaborar o utilizarlo. Endless Sky es uno de estos juegos, y con motivo de la publicación ayer de su **versión 0.9.9**, os vamos a hablar un poco de él.


En [Endless Sky](https://endless-sky.github.io/), encontrarás un juego de **simulación de comercio y combates espaciales** en **2D**, similar a Elite, Escape Velocity, o Star Control, donde comenzarás poniendote al cargo de una pequeña nave en un **mundo abierto**. Desde ella podrás seguir una **trama principal** o buscarte la vida realizando transportes de personas o mercancias, realizando actos de piratería, escoltando naves, cazando recompensas o participando en la guerra civil que tiene lugar en el juego. A medida que vayamos progresando en el juego **podremos adquirir mejores equipos** con los que actualizar las múltiples naves que podremos usar. Al ser un juego open source, **es posible editar los elementos del juego** para poder cambiarlo a nuestro gusto. Existe incluso [una página](https://opengamemods-group.github.io/ES-Mod-Share/) donde podremos encontrar muchos de esos recursos para modificar nuestro juego.


Una vez hechas las presentaciones, os vamos a hablar de la [actualización](https://github.com/endless-sky/endless-sky/releases/tag/v0.9.9) que ocupa nuestra noticia, y es que trás bastante tiempo el juego ha recibido ayer una actualización, la cual avisan que **es inestable** y previa  a otra versión, el mes que viene, que corregirá los errores de esta (estaremos atentos). Entre las **novedades** que encontraremos están las siguientes:


*-Optimizaciones significativas de motores gráficos y físicos.*  
 *-Las naves camufladas ahora pueden ser alcanzadas por proyectiles.*  
 *-Las torretas giran más despacio, lo que permite a los barcos rápidos esquivarlas más fácilmente.*  
 *-La efectividad de Ramscoop y de la energía solar ahora varía de una estrella a otra.*  
 *-Nuevos barcos, trajes y misiones.*  
 *-Comprar un Bactrian ahora requiere completar una cadena de misión.*  
 *-Añadido un "escáner de asteroides" que le permite apuntar a asteroides minables.*  
 *-"noticias" experimentales en el puerto espacial.*  
 *-Todos los sprites de los barcos se vuelven a renderizar con un ángulo de iluminación más dramático.*  
 *-El astillero muestra ahora una vista en ángulo de cada barco en lugar de de arriba hacia abajo.*


Endless Sky es un juego **Multiplataforma** y podreis descargarlo también para Windows y Mac. Para instalarlo en Linux existen muchas vias, pudiendo hacerse desde un [PPA para Ubuntu](https://launchpad.net/~mzahniser/+archive/ubuntu/endless-sky) y derivadas, [Debian](https://mentors.debian.net/package/endless-sky), [Flatpak](https://flathub.org/apps/details/io.github.EndlessSky.endless-sky) (no sabemos si está ya actualizado), e incluso [Steam](https://store.steampowered.com/app/404410/Endless_Sky/). Puedes dejarnos tus impresiones sobre el juego en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux). Os dejamos con un video de su creador, [Michael Zahniser](https://www.youtube.com/channel/UCK-JQtpwTSwcKBkjlh8W3IQ), donde podeis ver algunas de sus características:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/dHbq781jm6M" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 

