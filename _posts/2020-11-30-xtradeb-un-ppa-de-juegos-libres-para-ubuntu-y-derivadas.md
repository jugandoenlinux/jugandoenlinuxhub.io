---
author: leillo1975
category: Software
date: 2020-11-30 16:10:20
excerpt: "<p>El repositorio permite la instalaci\xF3n de conocidos proyectos de juegos\
  \ Open Source.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Xtradeb.webp
joomla_id: 1265
joomla_url: xtradeb-un-ppa-de-juegos-libres-para-ubuntu-y-derivadas
layout: post
tags:
- xonotic
- open-source
- warzone-2100
- speed-dreams
- vdrift
- stunt-rally
- ubuntu
- repositorio
- urban-terror
- playdeb
- xtradeb
title: 'XtraDeb, un PPA de juegos libres para Ubuntu y derivadas. '
---
El repositorio permite la instalación de conocidos proyectos de juegos Open Source.


Como sabeis muchos de los usuarios de Ubuntu y otras distribuciones derivadas, **la instalación de ciertos juegos libres en este sistema es complicada** en muchos casos, principalmente a que muchos de estos no están incluidos en los repositorios oficiales, no disponen de un paquete snap,  o sus desarrolladores no proporcionan un canal sencillo de instalación de su software (lo cual a mi juicio personal es un craso error, pues impide que mucha gente conozca su trabajo y pueda contribuir a él).


Ante esta situación, un usuario, [Jhonny Oliveira](https://launchpad.net/~jhonny-oliveira), tomó la decisión de tomar cartas en el asunto e intentar **recopilar algunos de estos juegos en un repositorio**, intentando replicar lo que en su día hacía [el desaparecido PlayDeb](https://web.archive.org/web/20150221022341/http://www.playdeb.net/welcome/). En este momento, la lista de juegos es pequeña, pero entre los juegos que podemos instalar facilmente estan [Speed Dreams](https://sourceforge.net/projects/speed-dreams/), [Stunt Rally](https://stuntrally.tuxfamily.org/), [Urban Terror](https://www.urbanterror.info/home/), [Vdrift](https://vdrift.net/), [Warzone 2100](https://wz2100.net/es/) y [Xonotic](https://www.xonotic.org/).Para instalar [este repositorio](https://launchpad.net/~xtradeb/+archive/ubuntu/play) en Ubuntu, tan solo teneis que abrir una terminal y teclear los siguientes comandos:



```
sudo add-apt-repository ppa:xtradeb/play
sudo apt-get update  
  

```

Esperemos que con el tiempo, esta lista vaya aumentando, por lo que si quereis que algún juego en concreto sea incluido, podeis solicitarlo [abriendo un bug en este Link](https://bugs.launchpad.net/xtradeb/+filebug) con la siguiente nomenclatura:



```
Package request - <nombre del juego>
```

¿Cual es vuestra opinión sobre este tipo de repositorios? ¿Echais en falta algún juego? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

