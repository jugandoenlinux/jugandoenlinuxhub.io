---
author: Pato
category: Noticia
date: 2018-08-24 19:49:50
excerpt: "<p>\xA1Otro port se nos viene a Linux! \xA1Se admiten apuestas!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ac0239369a9f7bffa6ab3e8391443ad3.webp
joomla_id: 842
joomla_url: el-radar-de-feral-vuelve-a-detectar-un-nuevo-ofni
layout: post
tags:
- proximamente
- feral-interactive
- feral
title: El radar de Feral vuelve a detectar un nuevo OFNI
---
¡Otro port se nos viene a Linux! ¡Se admiten apuestas!

¡No paran las buenas noticias! Según el comunicado de Feral un nuevo OFNI ha aparecido en su particular radar:



> 
> \*Sigh\*  
>   
> 5PM on a Friday, we're getting ready for the [#BankHolidayWeekend](https://twitter.com/hashtag/BankHolidayWeekend?src=hash&ref_src=twsrc%5Etfw) to start, and the Feral Radar decides to kick into life with a new blip for macOS and Linux. Well, whatever. We'll fix that rusty old thing on Tuesday. Okay, bye!<https://t.co/huXFd95Dab> [pic.twitter.com/rJQgb0Pxg4](https://t.co/rJQgb0Pxg4)
> 
> 
> — Feral Interactive (@feralgames) [August 24, 2018](https://twitter.com/feralgames/status/1033027634865864706?ref_src=twsrc%5Etfw)



 Como podéis ver, se aprecia una estrella sobre una textura (¿puede ser arena?) y la leyenda "working with fire and steel"(trabajando con fuego y acero).


¿Alguna idea de qué puede ser?


Se admiten apuestas en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


Gracias a [Odin](https://t.me/jugandoenlinux/99907) por el aviso.

