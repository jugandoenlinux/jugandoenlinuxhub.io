---
author: leillo1975
category: Estrategia
date: 2018-08-31 12:06:17
excerpt: "<p>@feralgames nos trae en tiempo record esta enorme actualizaci\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/32cf2b18bc061b77e56b5783aff01222.webp
joomla_id: 848
joomla_url: allegiance-update-es-el-nuevo-parche-para-thrones-of-britannia-y-nueva-dlc
layout: post
tags:
- feral-interactive
- total-war
- creative-assembly
- thrones-of-britannia
- allegiance-update
title: "\"Allegiance Update\" es el nuevo parche para Thrones of Britannia, y tambi\xE9\
  n nueva DLC."
---
@feralgames nos trae en tiempo record esta enorme actualización

Tan solo dos días han bastado para que los chicos de Feral adaptaran el último parche de "[Total War Saga: Thrones of Britannia](index.php/homepage/generos/estrategia/item/879-total-war-saga-thrones-of-britannia-ya-esta-en-gnu-linux-steamos)". Se llama "Allegiance Update" (Lealtad) y no se trata de un parche cualquiera ya que añade, corrige y balancea muchos aspectos del juego, por lo que trata de una gran actualización que tiene como principales caracteristicas las siguientes:



> 
> -La **lealtad** ahora está basada en el trabajo visto en Total War: Attila, con respecto a la religión, y en Rome II en el tema de las mecánicas culturales. La presencia de otras Lealtades en tu territorio ahora causa desordenes públicos.
> 
> 
> -Es posible usar **decretos** (basados en los ritos de Warhammer II) y para ello tendremos un nuevo menú en la pantalla. Existen 4 decretos diferentes y dan bonuses a la facción que los utilice por un determinado número de turnos, teniendo coste en oro para quien haga uso de ellos. Para poder usarlos será necesario desbloquearlos previamente ganando batallas, construyendo determinado tipo de edificios o desarrollando una tecnología concreta.
> 
> 
> -Hay dos facciones (Dyflin y Sudreyar) que siendo **paganas** tendrán ciertos beneficios, y a medida que el jugador conquista más provincias con iglesias aparecerá un evento con tres opciones: Permanecer Pagano, Convertir al Rey o la Conversión en Masa.
> 
> 
> -Los edificios menores que no sean religiosos al llegar al nivel 4 permitirán varias opciones para especializar la provincia.
> 
> 
> -Hay montones de aspectos cambiados en el juego para una experiencia más balanceada, tanto en el plano de gestión y estrategia, como en las batallas.
> 
> 
> -Se han corregido gran cantidad de bugs
> 
> 
> 


 Por supesto hay mucho más en "Allegiance Update", por lo que podeis consultar la lista de todos los [cambios en el Blog de Total War](https://www.totalwar.com/blog/thrones-of-britannia-allegiance-update). Os dejamos con un video donde se muestran algunas de estas novedades:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/ug9EAEX3Qrs" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 También hay que reseñar que se ha puesto a la venta la DCL "[Blood, Sweat and Spears](https://store.steampowered.com/app/884620/Total_War_Saga_THRONES_OF_BRITANNIA__Blood_Sweat_and_Spears/)" (tan solo 2.49€) que añade toques gore y más violencia a las batallas tal y como podeis "disfrutar" en el siguiente video:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/bHb8xlVK4bY" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 Si estais interesados en comprar "Total War Saga: Thrones of Britannia" podeis hacerlo en la **[tienda de Humble Bundle](https://www.humblebundle.com/store/total-war-saga-thrones-of-britannia?partner=jugandoenlinux)** (patrocinado) o en [Steam](https://store.steampowered.com/app/712100/Total_War_Saga_THRONES_OF_BRITANNIA/).


 


¿Se os ha actualizado ya el juego? ¿Qué os parece esta actualización?¿Y la DLC? Déjanos tus impresiones en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

