---
author: leillo1975
category: Aventuras
date: 2018-03-22 15:40:21
excerpt: "<p>Una vez m\xE1s, gracias a <a href=\"https://www.gog.com/game/maniac_mansion\"\
  \ target=\"_blank\" rel=\"noopener\">GamingOnLinux</a>, nos llega la noticia de\
  \ que <strong>Maniac Mansion tiene soporte para nuestro sistema si lo compramos\
  \ en GOG.com</strong>. La conocida tienda de \"Buenos Juegos Antiguos\" (y no tan\
  \ antiguos) vuelve a rescatar otro cl\xE1sico para nuestra Linuxera biblioteca.\
  \ En este caso se ha usado el <a href=\"https://www.scummvm.org/\" target=\"_blank\"\
  \ rel=\"noopener\">ScummVM</a> para traernos este t\xEDtulo. Esperemos que con el\
  \ tiempo nos hagan lo mismo con otros fant\xE1sticos juegos de <strong>LucasArts</strong>\
  \ como Loom, las sagas Monkey Island o Indiana Jones, entre muchos otros.</p>\r\n\
  <p>Como sabeis GOG utiliza a menudo este tipo de m\xE9todo para traer a nuestro\
  \ sistema sus juegos cl\xE1sicos, y ya no es la primera vez que vemos que saca partido\
  \ tanto de este ScummVM, como de DOSBOX o Wine. Por supuesto, nosotros los usuarios\
  \ podemos utlizar los mismos m\xE9todos, pero, a nivel personal me parece una muy\
  \ buena iniciativa que esta tienda nos facilite las cosas y empaquete ya los juegos\
  \ con todo lo necesario para poder ejecutarlos. As\xED que ya sabes, si te apetece\
  \ disfrutar de una buena aventura gr\xE1fica de las de anta\xF1o, Maniac Mansion\
  \ te va a encantar, si es que no lo has jugado aun; y si lo has hecho, es una buena\
  \ ocasi\xF3n de recordar buenos tiempos.</p>\r\n<p>Puedes comprar Maniac Mansion\
  \ con soporte para Linux en <a href=\"https://www.gog.com/game/maniac_mansion\"\
  \ target=\"_blank\" rel=\"noopener\">GOG.com</a>. Podeis ver un peque\xF1o video\
  \ del juego aqu\xED debajo:</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/J_lwPX9JZZ0\"\
  \ width=\"800\" height=\"450\" style=\"display: block; margin-left: auto; margin-right:\
  \ auto;\" allowfullscreen=\"allowfullscreen\"></iframe></p>\r\n<p>&nbsp;</p>\r\n\
  <p>\xBFHas jugado ya a este cl\xE1sico? \xBFQu\xE9 opinas que GOG de soporte usando\
  \ estos m\xE9todos? D\xE9janos tus respuestas en los comentarios, o en nuestro grupo\
  \ de <a href=\"https://t.me/jugandoenlinux\" target=\"_blank\" rel=\"noopener\"\
  >Telegram</a>.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/94f6f996ec3a866ce6d587d28bd5b809.webp
joomla_id: 688
joomla_url: gog-trae-a-linux-al-clasico-maniac-mansion
layout: post
tags:
- gog
- maniac-mansion
- scummvm
- lucasarts
title: "GOG trae a Linux al cl\xE1sico Maniac Mansion"
---
Una vez más, gracias a [GamingOnLinux](https://www.gog.com/game/maniac_mansion), nos llega la noticia de que **Maniac Mansion tiene soporte para nuestro sistema si lo compramos en GOG.com**. La conocida tienda de "Buenos Juegos Antiguos" (y no tan antiguos) vuelve a rescatar otro clásico para nuestra Linuxera biblioteca. En este caso se ha usado el [ScummVM](https://www.scummvm.org/) para traernos este título. Esperemos que con el tiempo nos hagan lo mismo con otros fantásticos juegos de **LucasArts** como Loom, las sagas Monkey Island o Indiana Jones, entre muchos otros.


Como sabeis GOG utiliza a menudo este tipo de método para traer a nuestro sistema sus juegos clásicos, y ya no es la primera vez que vemos que saca partido tanto de este ScummVM, como de DOSBOX o Wine. Por supuesto, nosotros los usuarios podemos utlizar los mismos métodos, pero, a nivel personal me parece una muy buena iniciativa que esta tienda nos facilite las cosas y empaquete ya los juegos con todo lo necesario para poder ejecutarlos. Así que ya sabes, si te apetece disfrutar de una buena aventura gráfica de las de antaño, Maniac Mansion te va a encantar, si es que no lo has jugado aun; y si lo has hecho, es una buena ocasión de recordar buenos tiempos.


Puedes comprar Maniac Mansion con soporte para Linux en [GOG.com](https://www.gog.com/game/maniac_mansion). Podeis ver un pequeño video del juego aquí debajo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/J_lwPX9JZZ0" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Has jugado ya a este clásico? ¿Qué opinas que GOG de soporte usando estos métodos? Déjanos tus respuestas en los comentarios, o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

