---
author: Pato
category: "Acci\xF3n"
date: 2017-11-08 17:05:23
excerpt: <p>El juego tiene claras influencias de Quake o Unreal</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b7776ccd86677e084c73066d56234a62.webp
joomla_id: 521
joomla_url: alien-arena-un-fps-de-accion-rapida-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- gore
- violento
title: "'Alien Arena' un FPS de acci\xF3n r\xE1pida ya est\xE1 disponible en Linux/SteamOS"
---
El juego tiene claras influencias de Quake o Unreal

A falta de los grandes del género de la acción en primera persona, nos llega este 'Alien Arena' que viene a hacer las delicias de todo aquel que guste de la acción rápida de gatillo fácil y sin complicaciones.


'Alien Arena' pretende recuperar el estilo de los títulos del género "old school" dándoles nueva vida en un entorno "retro-Sci-Fi" de atmósfera "arcade-like". Lucha en docenas de arenas ambientadas en mundos extraterrestres donde tendrás que luchar contra alienígenas en frenéticos combates contra la IA o contra otros jugadores. Tendrás a tu disposición diversos modos de juego, "deathmatch", PvP, multijugador en línea y mucho más a través de 23 niveles. Elige entre 10 personajes, diversos "power ups" y 10 armas con dos modos de disparo cada una.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/6JpqgrOWUsY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Alien Arena no está disponible en español, pero si no es problema para ti y te interesa, lo tienes disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/629540/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Qué te parece 'Alien Arena'? ¿Eres de los que les gusta la "acción rápida"?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

