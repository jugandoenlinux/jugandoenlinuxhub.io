---
author: Serjor
category: Software
date: 2017-08-29 14:07:53
excerpt: "<p>El c\xF3digo libre y las licencias con car\xE1cter abierto ser\xE1n las\
  \ protagonistas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b190b9bcae39ee6b68c160e3e6f45456.webp
joomla_id: 446
joomla_url: open-jam-una-jam-de-videojuegos-con-espiritu-open-source
layout: post
tags:
- open-source
- open-jam
- itchio
title: "Open Jam, una jam de videojuegos con esp\xEDritu open source"
---
El código libre y las licencias con carácter abierto serán las protagonistas

Vía [opensource.com](https://opensource.com/article/17/8/open-jam-announcement) nos enteramos de una más que interesante propuesta, y es que desde la tienda de vídeojuegos <itch.io> están organizando una jam de videojuegos dónde las principales características frente a otras jams es que aquí el juego final por un lado tiene que ser licenciado bajo una licencia open source/creative commons, tanto el código cómo las imágenes, música y sonidos, y por otro tiene que funcionar sí o sí o bien en GNU/Linux o bien en un navegador Web, lo cuál es de agradecer, porque de un modo u otro podremos probar todos los juegos que se presenten al concurso.


Además, aunque no es obligatorio (o eso me ha parecido entender), animan a los concursantes a usar herramientas de código libre para realizar los diferentes proyectos, y para ello presentan un extenso listado de diversas herramientas, desde motores para el juego como editores de sonido e imágenes. Si queréis podéis encontrar este listado [aquí](https://notabug.org/Calinou/awesome-gamedev/src/master/README.md).


Esta jam, que se celebrará del 6 al 8 de octubre de este año, no tiene premios en metálico, pero los tres juegos ganadores serán expuestos en la conferencia [All Things Open](https://allthingsopen.org/) de octubre.


Así que si te gusta programar videojuegos, te gusta el software libre, y te gustaría participar, pásate por la [web](https://itch.io/jam/open-jam-1) para apuntarte.


Y tú, ¿te animas a participar? Pásate por nuestro foro de programación para buscar con quién formar parte del evento, así cómo por nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

