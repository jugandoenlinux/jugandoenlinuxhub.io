---
author: Pato
category: Software
date: 2019-03-14 19:07:15
excerpt: "<p>La funcionalidad a\xFAn en fase beta permite hacer streaming a cualquier\
  \ dispositivo fuera de la red local</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1e6a90ec1f25564654b093c54a8ddd7e.webp
joomla_id: 996
joomla_url: valve-lanza-la-funcionalidad-de-steam-link-anywhere-y-abre-su-propia-red-a-los-desarrolladores
layout: post
tags:
- steam
- valve
- steam-link
- steam-link-anywhere
title: Valve lanza la funcionalidad de Steam Link Anywhere y abre su propia red a
  los desarrolladores
---
La funcionalidad aún en fase beta permite hacer streaming a cualquier dispositivo fuera de la red local

Valve parece estar moviendo fichas ante los movimientos que empiezan a vislumbrarse en el mercado del videojuego. En su [habitual comunicado](https://steamcommunity.com/app/353380/discussions/0/3362406825533023360/) de novedades en el foro de Steam Link han publicado una actualización donde anuncian el lanzamiento en fase beta de una nueva "funcionalidad" llamada **Steam Link Anywhere** con la que será posible hacer streaming de los juegos de tu PC hacia cualquier dispositivo compatible con Steam Link Anywhere ya sea el propio **hardware Steam Link, la Raspberry Pi o un dispositivo Android** fuera de tu red local.


*Steam Link Anywhere te permite transmitir juegos a tu Steam Link desde cualquier computadora que ejecute Steam, siempre y cuando tu computadora tenga una buena velocidad de  subida y tu dispositivo Steam Link tenga una buena conexión de red.*


Para poder utilizar esta funcionalidad, aún en fase beta temprana los pasos son los siguientes:


1.- Actualiza tu cliente Steam a la versión beta, con fecha del 13 de marzo o más reciente.


2.- Agrega una computadora y selecciona "Otra computadora"


3.- Sigue las instrucciones de emparejamiento en pantalla.


Eso es todo. Eso si, es muy recomendable que tal y como explican en el comunicado tu conexión sea competente. Imaginamos que lo recomendable son conexiones de fibra con al menos 100 megas simétricos para una funcionalidad óptima, aunque conexiones con menores velocidades pueden funcionar bien siempre que no tengan un ping excesivo. De todos modos, al ser una funcionalidad en fase beta lo adecuado es probar y reportar cualquier irregularidad.


Por otra parte, nos hacemos eco también de la noticia de [gamingonlinux.com](https://www.gamingonlinux.com/articles/valve-announces-new-networking-apis-for-developers-and-steam-link-anywhere.13761) en la que Valve anuncia la disponibilidad de una nueva API de redes disponible para todos los desarrolladores asociados. Según explican en su comunicado, *durante los últimos años, hemos estado [trabajando](https://www.youtube.com/watch?v=2CQ1sxPppV4) en la mejora de la calidad de las experiencias multijugador en DOTA y CS:GO al transmitir el tráfico y portándolo en nuestra red troncal. Esto protege a nuestros servidores de ataques de denegación de servicio y brinda a los jugadores de todo el mundo una conexión de baja latencia y alta calidad. Hoy lanzamos las [API](https://partner.steamgames.com/doc/api/ISteamNetworkingSockets) que hacen que este servicio esté disponible para todos los asociados de Steam. Esto te ofrece:*


* Acceso a nuestra red, brindando a tus jugadores protección contra ataques, NAT 100 % confiable y con mejor conectividad.
* Herramientas para estimar instantáneamente el ping entre dos hosts arbitrarios sin enviar ningún paquete.
* Un protocolo UDP cifrado de alta calidad de extremo a extremo y confiable.


Valve anuncia que posee 128 servidores desplegados en 30 puntos en la red en todo el mundo y varios terabits de ancho de banda. Usando estas API, se puede aprovechar esta infraestructura. Retransmitir el tráfico de los juegos a través de su red brinda varios beneficios como anonimizar el tráfico evitando ataques, enrutamiento dinámico óptimo de paquetes y mejoras a nivel de ping. Además ponen a disposición de sus asociados otras herramientas de estimación de ping, protocolo abierto punto a punto cifrado y autenticación.


Toda la información la tienes [en su comunicado](https://steamcommunity.com/groups/steamworks#announcements/detail/1791775741704351698), y el código del proyecto junto con las instrucciones las tienes disponibles en su [página de Github](https://github.com/ValveSoftware/GameNetworkingSockets).

