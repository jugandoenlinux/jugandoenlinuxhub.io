---
author: Pato
category: Hardware
date: 2018-12-21 12:18:50
excerpt: "<p>La compa\xF1\xEDa @SlimbookEs sigue a\xF1adiendo equipos para la \"inform\xE1\
  tica de potencia\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4c55c93b61ad68ff535629a2f86c996e.webp
joomla_id: 934
joomla_url: slimbook-lanza-su-primer-portatil-pensado-para-jugar-en-linux
layout: post
tags:
- portatil
- slimbook
title: Slimbook lanza su primer portatil pensado para jugar en Linux
---
La compañía @SlimbookEs sigue añadiendo equipos para la "informática de potencia"

Tras haber lanzado una línea de equipos de sobremesa pensando en la "informática de potencia", o sea videojuegos o aplicaciones profesionales "pesadas", **Slimbook** nos vuelve a traer un equipo para jugar, esta vez portátil.


Se trata del **Slimbook Eclipse**, un ordenador portátil con una configuración puntera que nos permitirá jugar o lanzar nuestras aplicaciones con garantías. Sus características básicas son:


Procesador Intel i7-7700HQ


Gráfica dedicada: NVIDIA 1060 6GB


Pantalla 15,6"


Puertos USB: 4


Memoria RAM: 8GB


Disco duro M.2: 120GB SSD


Medidas 390 x 269.5 x 27.9mm


Peso 2,6 ~ Kg (batería incluida)


Teclado LED: Español Inglés ANSI


![slimbookeclipse](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/slimbook/slimbookeclipse.webp)A esto y como viene siendo marca de la casa, el Slimbook Eclipse se le pueden añadir distintas mejoras eligiendo parte de sus características, como disco duro secundario, aumento de memoria ram hasta 16 GB, discos duros de hasta 1Tb, chipset wifi dual band y teclados en distintas configuraciones, sin olvidar como no la elección del sistema operativo entre distintas distribuciones Linux (Ubuntu, Kubuntu, Linux Mint, KDE Neon etc...) e incluso Windows ya sea solo o en "dualboot". Tomando como base estas características **el Slimbook Eclipse se perfila como un portatil muy capaz para mover aplicaciones pesadas, o jugar con garantías a los últimos títulos del mercado con calidades altas e incluso mover los juegos actuales de Realidad Virtual**.


Además, Slimbook ha publicado [una comparativa](https://slimbook.es/tutoriales/slimbook/374-comparativa-eclipse-con-otros-portatiles-gaming) en cuanto a precios/prestaciones respecto a otros equipos de la competencia que no tiene desperdicio.


En su configuración más básica, montando: Procesador Intel i7-7700HQ, Gráfica dedicada: NVIDIA 1060 6GB, Memoria RAM: 8GB, Disco duro M.2: 120GB SSD, Intel Dual Band 3165AC y Ubuntu, el **Slimbook Eclipse** **sale por tan solo 899,00€.**


Si quieres ver el portátil, configurarlo a tu gusto y hacer el PRE-PEDIDO (los ordenadores se entregarán a partir de finales de Enero) puedes visitar su [página de la tienda](https://slimbook.es/pedidos/slimbook-eclipse-15/eclipse-intel-i7hq-comprar) o su [página de producto](https://slimbook.es/eclipse-linux-profesional-workstation-and-gaming-laptop).


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/8_Ai-KM5qVY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


¿Qué te parece el Slimbook Eclipse? ¿Te gusta la configuración? ¿Te gustaría comprar uno? ¿Con qué distribución?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [discord](https://discord.gg/ftcmBjD).

