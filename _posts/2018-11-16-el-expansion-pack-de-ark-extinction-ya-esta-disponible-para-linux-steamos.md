---
author: Pato
category: "Acci\xF3n"
date: 2018-11-16 12:31:52
excerpt: <p>Los chicos de Studio Wildcard <span class="username txt-mute"><a class="account-link
  link-complex" href="https://twitter.com/survivetheark" target="_blank" rel="noopener
  user" data-user-name="survivetheark">@survivetheark</a>&nbsp;</span>nos traen su
  nuevo contenido descargable</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/91a3fd8ae5d32ee912c8819cabab1e0b.webp
joomla_id: 904
joomla_url: el-expansion-pack-de-ark-extinction-ya-esta-disponible-para-linux-steamos
layout: post
tags:
- accion
- aventura
- supervivencia
title: "El Expansion Pack de ARK: Extinction ya est\xE1 disponible para Linux/SteamOS"
---
Los chicos de Studio Wildcard [@survivetheark](https://twitter.com/survivetheark) nos traen su nuevo contenido descargable

Gracias a un tweet recibimos la información de que Studio Wildcard, responsables del juego de acción y supervivencia ARK: Survival Evolved [[Steam](https://store.steampowered.com/app/346110/ARK_Survival_Evolved/)] nos traen su último contenido descargable a nuestro sistema favorito, llamado "Extinction":



> 
> Mac/Linux Extinction is now live on Steam! v286.104. Please let us know if you experience any issues with it. ?
> 
> 
> — ARK: Extinction (@survivetheark) [November 15, 2018](https://twitter.com/survivetheark/status/1062918976387407873?ref_src=twsrc%5Etfw)


  





En este caso, volveremos a los orígenes del juego original para terminar la aventura donde la empezamos:


*"Termina tu viaje a través de los mundos de ARK en "Extinción", donde la historia comenzó y termina: ¡en la Tierra misma! Un planeta devastado e infestado de elementos, lleno de criaturas fantásticas tanto orgánicas como tecnológicas, la Tierra guarda los secretos del pasado y las claves de su salvación. Como un sobreviviente veterano que ha vencido todos los obstáculos anteriores, su desafío final lo espera: ¿puede derrotar a los gigantes titanes que dominan el planeta y completar el ciclo ARK para salvar el futuro de la Tierra?"*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/_XYWO64b_xc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Si quieres saber más sobre ARK: Extinction o su juego base, puedes visitar su [página web oficial](http://playark.com/) o su [página de Steam](https://store.steampowered.com/app/887380/ARK_Extinction__Expansion_Pack/) donde lo tienes ya disponible para Linux/SteamOS por 16,79€.


¿Que te parece el universo de ARK: Survival evolved? ¿Has jugado ya a sus otras expansiones?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [discord](https://discord.gg/ftcmBjD).

