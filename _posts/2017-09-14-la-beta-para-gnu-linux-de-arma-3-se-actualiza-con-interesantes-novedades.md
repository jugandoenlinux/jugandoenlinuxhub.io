---
author: Serjor
category: "Simulaci\xF3n"
date: 2017-09-14 21:05:32
excerpt: <p>El port avanza con paso firme</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/72d71c3e1a75301c5a2942cd172af179.webp
joomla_id: 464
joomla_url: la-beta-para-gnu-linux-de-arma-3-se-actualiza-con-interesantes-novedades
layout: post
tags:
- simulador
- beta
- bohemia-interactive
- arma-iii
title: La beta para GNU/Linux de Arma 3 se actualiza con interesantes novedades (ACTUALIZADO)
---
El port avanza con paso firme

**ACTUALIZACIÓN a 20-9-17:** Nos enteramos de nuevo a través de [GamingOnLinux](https://www.gamingonlinux.com/articles/arma-3-176-for-linux-is-planned-work-on-it-to-start-soon.10387) que Bohemia Interactive y Virtual Programming se pondrán manos a la obra "pronto" para portar el juego a la versión 1.76, para igualarla a la versión actual para Windows. Esta es una gran noticia por que puede significar que el juego finalmente salga del periodo Beta y sea posible jugar en online con el resto de jugadores, además de disponer de más DLC's


 


En [GamingOnLinux](https://www.gamingonlinux.com/articles/arma-3-for-linux-updated-to-170-its-now-64bit-and-solves-the-texture-issue-i-had.10351) leemos algo que será de interés para los aficionados a los simuladores tácticos, y es que Arma III, de Bohemia Interactive tiene nueva versión en su beta para GNU/Linux, y en esta ocasión viene con un ejecutable para arquitecturas de 64bits, con lo que es de esperar una mejora en el rendimiento.


Así mismo, con esta nueva versión la versión para GNU/Linux es también compatible con el DLC de los Jets, así que podremos surcar los cielos.


Y poco más, más allá de corrección de errores. No obstante nos alegra saber que el port no ha sido abandonado, y aunque no se anuncie de manera oficial sabemos que es jugable y que los chicos de Bohemia siguen trabajando junto Virtual Programming para no dejarnos en las trincheras.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/107410/31539/" width="646"></iframe></div>


Y tú, ¿cómo ves la evolución de este port? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

