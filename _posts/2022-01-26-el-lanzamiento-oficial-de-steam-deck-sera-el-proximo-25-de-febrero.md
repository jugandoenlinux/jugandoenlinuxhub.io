---
author: Pato
category: Hardware
date: 2022-01-26 18:47:13
excerpt: "<p>Valve acaba de confirmarlo de forma oficial en su \xFAltimo comunicado</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/Steamdeckhands.webp
joomla_id: 1414
joomla_url: el-lanzamiento-oficial-de-steam-deck-sera-el-proximo-25-de-febrero
layout: post
tags:
- steam-deck
title: "El lanzamiento oficial de Steam Deck ser\xE1 el pr\xF3ximo 25 de Febrero"
---
Valve acaba de confirmarlo de forma oficial en su último comunicado


Una de las incógnitas que nos quedaba por saber era la fecha exacta en la que las Steam Deck comenzarían a llegar a los afortunados usuarios que la reservaron en un primer momento, pero Valve acaba de anunciar que será el próximo día 25 de Febrero cuando empiecen a llegar los correos avisando de que ya se pueden hacer efectivas las reservas:



> 
> "El 25 de febrero enviaremos los primeros correos electrónicos para iniciar pedidos a quienes hayáis hecho una reserva. Los clientes tendrán 3 días (72 horas) para realizar la compra a partir de la fecha en que reciban el correo electrónico para iniciar el pedido. Transcurrido ese plazo, la reserva se cederá a la siguiente persona en la cola. Las primeras unidades empezarán a enviarse a los clientes a partir del día 28 y tenemos previsto seguir enviando correos electrónicos para iniciar pedidos cada semana.2
> 
> 
> 


 Además, ese mismo día ser retira el embargo informativo (NDA) con lo que muy posiblemente comenzaremos a ver reviews e información de los afortunados que ya tengan alguna disponible.


También indican que irán enviando unidades a los medios de comunicación para las pertinentes pruebas y reviews. ¿Estaremos entre los afortunados?...


Puedes ver toda la información en el [comunicado oficial de Valve en Steam](https://store.steampowered.com/news/app/1675180/view/3117055056380003048).

