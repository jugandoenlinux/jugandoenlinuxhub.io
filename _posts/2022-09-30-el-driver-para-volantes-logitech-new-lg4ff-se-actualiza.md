---
author: leillo1975
category: Hardware
date: 2022-09-30 08:20:32
excerpt: "<p>Nuestro amigo <span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo\
  \ r-qvutc0\">@bernat_arlandis</span> acaba de publicar la versi\xF3n 0.4.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/new-lg4ff/new-lg4ff.webp
joomla_id: 1490
joomla_url: el-driver-para-volantes-logitech-new-lg4ff-se-actualiza
layout: post
tags:
- driver
- logitech
- oversteer
- ffb
- new-lg4ff
- bernat
title: 'El driver para volantes Logitech new-lg4ff se actualiza '
---
Nuestro amigo @bernat_arlandis acaba de publicar la versión 0.4.


Parece mentira pero [ya han pasado casi 3 años]({{ "/posts/new-lg4ff-un-nuevo-driver-mucho-mas-completo-para-nuestros-volantes-logitech" | absolute_url }}) desde que los aficionados a los juegos de conducción pudiésemos usar nuestros volantes Logitech en condiciones, y para estas lides olvidarnos en muchos casos y definitivamente de Windows, o del sufrimiento de no tener Force Feedback,. En ese momento un miembro de nuestra comunidad, [Bernat](https://twitter.com/bernat_arlandis), lanzaba un driver experimental que nos permitía sentir como nunca nuestro dispositivo favorito. Para quien no lo sepa también es el creador de la magnífica utilidad de configuración de volantes [Oversteer](https://github.com/berarma/oversteer).


Para ser justos, en ese momento, parte del mérito había que otorgárselo a Simon Wood ([@mungewell](https://twitter.com/mungewell)), uno de los desarrolladores de [Speed Dreams]({{ "/tags/speed-dreams" | absolute_url }}) ([web oficial](https://www.speed-dreams.net/)), que había conseguido un driver básico que entre otras cosas activaba el efecto de fuerza constante, y que es él que actualmente está incluido en el kernel de Linux. Partiendo de esa gran base, Bernat ha conseguido crear un controlador mucho más completo, no solo activando más efectos, sino dotándolo de más funciones.


En estos años el desarrollo no se ha parado, y hace un rato ha publicado la [versión 0.4](https://github.com/berarma/new-lg4ff/releases/tag/0.4.0). Los cambios desde la 0.3 están en Github y son los siguientes:


* Arreglados problemas de compilación en algunos sistemas.
* Se ha eliminado cierta aspereza en los efectos desactivando el efecto que la causa cuando no se está usando.
* No se quedan efectos activados al quitar el módulo.
* Mejora de compatibilidad con los kernels antiguos.
* Compatibilidad con sistemas de 32bits.
* Añadido soporte para volantes **Logitech G923 de PS4/PC**.


Si quieres descargar el código pásate por [este enlace](https://github.com/berarma/new-lg4ff/releases/tag/0.4.0). Tienes información sobre la compilación e instalación [aquí](https://github.com/berarma/new-lg4ff#readme). También debes saber que los fanáticos de los juegos de coches en nuestro sistema operativo tienen su punto de encuentro en nuestra [Zona Racing](https://matrix.to/#/#zona_racing-jugandoenlinux:matrix.org), en el [espacio en Matrix de JugandoEnLinux.com](https://matrix.to/#/#jugandoenlinux.com:matrix.org). Allí estaremos encantados de echarte una mano para instalar este driver o configurar tus juegos de conducción favoritos, y por supuesto echarnos unas carreras si se tercia.

