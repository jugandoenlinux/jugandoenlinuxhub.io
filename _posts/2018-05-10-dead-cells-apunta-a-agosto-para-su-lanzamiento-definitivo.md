---
author: Pato
category: "Acci\xF3n"
date: 2018-05-10 20:00:47
excerpt: "<p>El juego de acci\xF3n y plataformas de Motion Twin est\xE1 mas cerca\
  \ de llegar a nuestros sistemas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6825866abf6ddc014340326b35addd51.webp
joomla_id: 732
joomla_url: dead-cells-apunta-a-agosto-para-su-lanzamiento-definitivo
layout: post
tags:
- accion
- indie
- plataformas
- roguelike
title: '''Dead Cells'' apunta a Agosto para su lanzamiento definitivo'
---
El juego de acción y plataformas de Motion Twin está mas cerca de llegar a nuestros sistemas

El nuevo juego juego de acción y plataformas de tipo roguelike está cada vez mas cerca de llegar a nuestros sistemas. Gracias a [Linuxgameconsortium](https://linuxgameconsortium.com/linux-gaming-news/dead-cells-update-issues-linux-release-details-66387/) nos enteramos que Motion Twin está pensando en lanzar su esperado 'Dead Cells' el próximo mes de Agosto.


Como sabéis, [hace ya mas de un año](index.php/homepage/generos/accion/item/273-dead-cells-una-mezcla-de-generos-muy-interesante-llegara-a-linux) que sabemos que 'Dead Cells' llegará a Linux/SteamOS, pero aunque el juego ya lleva un tiempo en fase de acceso anticipado no será hasta que sea lanzado definitivamente cuando Motion Twin nos traerá su juego a nuestro sistema favorito.


*"Esta séptima actualización es la primera de unas cuantas actualizaciones menores antes de que el lore llegue al lanzamiento. Y estamos trabajando duro en las versiones de Mac y Linux! No olvidéis que haremos un gran DLC gratuito después del lanzamiento!"*


Lo que el estudio no aclara es si estas versiones llegarán en la misma fecha de lanzamiento o en algún momento posterior. Podéis ver el comunicado [en este enlace](https://steamcommunity.com/games/588650/announcements/detail/1655513946245883515).


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/KV6fBYuuPMg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Estaremos atentos a mas detalles sobre el juego así como a la fecha  de salida para Linux/SteamOS. Mientras tanto, podeis ver mas detalles sobre el juego en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/588650/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

