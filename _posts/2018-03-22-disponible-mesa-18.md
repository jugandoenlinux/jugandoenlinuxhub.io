---
author: leillo1975
category: Software
date: 2018-03-22 09:04:47
excerpt: "<p>La actualizaci\xF3n incluye, como suele ser habitual, multitud de novedades</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2e83d7bc595a142d5b8cc7504455fc0e.webp
joomla_id: 687
joomla_url: disponible-mesa-18
layout: post
tags:
- amd
- intel
- gallium3d
- mesa
- nouveau
title: Disponible Mesa 18.
---
La actualización incluye, como suele ser habitual, multitud de novedades

Finalmente ya tenemos entre nosotros la nueva y flamante versión de Mesa, en este caso la 18, que se corresponde como sabeis con el año en el que estamos. En esta ocasión ha tenido un ciclo de desarrollo un pelín más accidentado de lo normal debido a la corrección de multiples errores que se han encontrado durante las versiones candidatas (5 en total) que han servido para pulir esta anciana (desde 1993 dando guerra) pero en forma, biblioteca gráfica.


En esta versión nos vamos a encontrar con múltiples e importantes novedades que van a sacar aun más provecho de nuestras castigadas tarjetas gráficas, especialmente si somos usuarios de soluciones gráficas de AMD e Intel. Entre las novedades que vamos a encontrar estan las siguientes:


-Más trabajo realizado sobre el soporte de las tarjetas [Radeon r600](https://en.wikipedia.org/wiki/Radeon_HD_2000_series), que como sabeis se corresponde con las tarjetas de la serie HD 2xxx. Se ha conseguido dotar de **soporte OpenGL 4.3** , aunque la especificación 4.4 está realmente muy cerca, estando casi implementada.


-La extensión **ARB_get_program_binary** necesara para algunos juegos, entre otros el conocido Dead Island, ha sido corregida y ahora funciona correctamente.


-Más soporte para el **driver de Vulkan RADV**, con la activación de nuevas extensiones y mejoras en el rendimiento.


-El controlador de **Vulkan ANV para gráficas Intel** incluye ahora almacenamiento para los 16bits y punteros variables. También el soporte para PRIME con dicho driver.


-Múltiples **mejoras en OpenGL para gráficas Intel**.


-El back-end **NIR de RadeonSI** ahora es compatible con GLSL 4.50. Valve y AMD han trabajado arduamente para conseguir esto, Este desarrollo es importante para alcanzar el **cumplimiento de la especificación OpenGL 4.6** y el mejor **reutilizado del código entre OpenGL y Vulkan**. Ha habido también múltiples optimizaciones de NIR Gallium3D.


Obviamente existen muchas más mejoras notables, pero se trata de información muy técnica y dificil de entender para los no expertos en estas lides, entre los que me incluyo. Si quereis información más completa sobre todas las novedades seguid [este enlace](https://www.mesa3d.org/relnotes/18.0.0.html).


En resumen, nos encontramos ante una nueva versión de Mesa que viene a mejorar un poco más las posibilidades que nos ofrecen nuestras gráficas a día de hoy. El avance de este desarrollo en los últimos años es espectacular, y ha conseguido que veamos como viables chips gráficos que hace menos de un lustro eran castañas en nuestros equipos con linux, y lo mejor de todo es que lo hacen desde el Software Libre. Este desarrollo nos permite tener un abanico más amplio de posibilidades a la hora de adquirir hardware que hace unos años, donde este mercado, en Linux, estaba única y exclusivamente copado por las soluciones que presentaba NVIDIA.


Si sois usuarios de Ubuntu, y quereis instalar esta última versión de Mesa disponeis del [repositorio de Padoka Estable](index.php/foro/drivers-para-graficas-amd-intel/7-ppa-drivers-mesa-no-oficial-de-paulo-dias), aunque en estos momentos no aparece aun disponible, se espera que en los próximos días se actualice y podais disfrutarla.


¿Sois usuarios de Mesa?¿Qué os parece el avance que ha tenido estos últimos años? Déjanos tu opinión en los comentarios, o charla con nosostros sobre este tema en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

