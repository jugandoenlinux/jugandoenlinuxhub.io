---
author: Jugando en Linux
category: Editorial
date: 2018-08-22 16:45:02
excerpt: <p>En Jugando en linux tratamos de analizar la "jugada" de Valve con Steam
  Play</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3d105248b10b0d962a881701f9482c8d.webp
joomla_id: 839
joomla_url: parece-que-valve-sabe-a-lo-que-juega-o-el-gigante-parece-haber-despertado
layout: post
tags:
- steamos
- steam
- valve
- steam-play
title: "Parece que Valve s\xED sabe a lo que juega (o el gigante parece haber despertado)"
---
En Jugando en linux tratamos de analizar la "jugada" de Valve con Steam Play

Los titulares rimbombantes que se podrían usar hoy son muchos: "Valve asalta el juego en PC con Steam Play", "Valve ha dado un golpe en la mesa llamado "SteamPlay", "Steam Play es la respuesta de Valve para su independencia", "El nuevo Steam Play es la respuesta de Valve para levantar el juego en Linux/SteamOS"...


Pero lo que importa realmente no es el titular, si no lo que supone el nuevo movimiento de Valve dentro del ecosistema del videojuego en PC. Analicemos el tema desde un principio:


Desde sus inicios, **Valve es una empresa dedicada principalmente al videojuego en PC**, en un principio como desarrolladora y posteriormente también como distribuidora a través de Steam. Los que ya peinamos alguna cana recordamos el estado en que se encontraba el videojuego en PC **antes de la aparición de Steam, donde la piratería salvaje estaba acabando con los desarrollos de videojuegos para compatibles**, y muchos auguraban el fin del videojuego en la plataforma. Tanto es así que la mayoría de las editoras y las desarrolladoras se centraban en el entonces muy lucrativo negocio de las consolas, desarrollando sus grandes títulos en exclusiva para ellas y dejando de lado al PC donde incluso antes de lanzar sus videojuegos ya existían versiones piratas circulando por internet sin ningún pudor.


Valve supo ver la oportunidad en un ecosistema donde **ni siquiera Microsoft** con su posición dominante apostaba un euro. Muchos recordamos la llegada de Steam con Half Life, y la que algunos liamos por que nos "ataba" a una conexión a internet para poder jugarlo. Sin embargo, **Valve sabía a lo que estaba jugando**, y junto a un puñado de desarrolladoras indie comenzó a expandir su cliente con cada vez más y más opciones enfocadas a hacerle la vida mas fácil a los jugadores a la par que simplificar la distribución a los desarrolladores, y junto a una agresiva política de precios y descuentos (en la época se hizo famosa la frase de que el éxito de Steam era el hacerle la competencia a la piratería) la plataforma despegó como un cohete. Así fue como **solo una empresa** levantó el negocio del videojuego en el PC, y el resto es historia. Luego llegaron otras con mayor o menor éxito, pero la referencia es claramente Steam.


Sin embargo, Valve sabía que tenía un talón de Aquiles: El juego en PC está ligado a Windows. Desde el momento en que la empresa afincada en Bellevue se dio cuenta de que dependía completamente de lo que Microsoft hiciera con su sistema operativo vió el peligro de que en el futuro su negocio quedara "eclipsado" por una empresa externa que, ajena a sus intereses decidiera que el sistema operativo mayoritario en los videojuegos dejase de dar soporte para la base de aplicaciones de Steam, **o que Steam ya no tiene cabida en un Windows cerrado sobre el ecosistema Xbox y su tienda de aplicaciones**. Por que al fin y al cabo es lo que se vislumbra con la nueva política de Microsoft y su Sistema como Servicio, donde las campanas suenan a un salto a un Windows mas liviano y centrado sobre las UWP para competir de tu a tu con los nuevos sistemas y dispositivos de Google y Apple. Respecto a este tema, ya hemos hablado con anterioridad y podéis profundizar [en una editorial anterior](index.php/homepage/editorial/item/760-windows-s-windows-core-os-polaris-despierta-valve) de Jugando en Linux.


Siendo así, **Valve comenzó a buscar una alternativa** que le permitiese "independizarse" y en el hipotético caso de que llegase el momento en que Windows dejase a Steam poco menos que como el "segundón" dentro de su propio negocio, poder tener un plan B, un respaldo en el que poder apoyarse cuando viniesen mal dadas. Y así comenzó la andadura para dar soporte a Linux, un sistema libre que nunca tendrá el problema de "cerrarle" la puerta y dejarla en la estacada. De ahí salió el desarrollo de su propio sistema para videojuegos, SteamOS y de rebote el soporte para todo el ecosistema Linux por parte de Steam.


**Hasta ahora, hemos recibido algunos grandes títulos**, muchos de ellos gracias al esfuerzo de compañías "porteadoras" que con encomiable esfuerzo han convencido a algunas desarrolladoras para lanzar sus juegos en Linux, **y una buena cantidad de indies** que han engrosado el número de títulos disponibles para nuestro sistema favorito hasta un número que algunos jamás habrían soñado. Sin embargo, el número de jugadores en Linux no ha despegado, y los grandes títulos no acaban de llegar de forma masiva (o al menos a un ritmo que Valve considere aceptable) pues las editoras de los "triple A" no ven atractivo el esfuerzo que supone portar los títulos para una base de usuarios tan pequeña que no compensa el ofrecer el soporte para este sistema operativo e incluso ven como algunos desarrollos afirman "no ganar" dinero con los ports a Linux.


Valve es consciente de esto, pero como hemos dicho en tantas ocasiones, es la pescadilla que se muerde la cola. No llegan grandes desarrollos por que no hay jugadores, y no llegan mas jugadores por que no llegan los grandes desarrollos. En los círculos de jugadores, no es raro encontrar quien afirma tener dos particiones en su Pc, una con Windows para jugar y otra con una distro Linux para todo lo demás. Y es aquí donde Valve quiere poner remedio al problema, introduciendo una "disrupción" que pueda remover los cimientos de un todopoderoso "Windows gaming".


El nuevo Steam Play es una nueva vuelta de tuerca para lograr el objetivo de "independencia". De otra forma **sería difícil justificar todo el ingente dinero y esfuerzo que Valve ha invertido y sigue invirtiendo para que el videojuego en sistemas Linux sea de primer orden**. Algo ha debido suceder para que ahora Valve "pise el acelerador" y lance Steam Play allanando el camino a la llegada de una ingente cantidad de juegos que hasta ahora era impensable que pudiéramos jugar, si no es ejecutando Steam mediante capas de compatibilidad externas, y haciendo a veces mil y una virguerías y configuraciones que el usuario promedio apenas llegará nunca a entender.


**Steam Play supone el comienzo de una nueva batalla por atraer desarrollos y jugadores hacia el ecosistema de SteamOS** y de rebote a Linux, lo que al final llevará a Steam a ser "sistemáticamente independiente". Como dijo hace unos días **el gran Ekianjo** en [BoilingSteam](https://boilingsteam.com/is-valve-working-on-a-windows-compatibility-layer/), este movimiento **solo es entendible desde la óptica de Valve y Steam**, y su necesidad de conseguir que Windows no sea la única opción. ¿Lo conseguirá? está por ver.


Lo que sí es cierto es que el nuevo movimiento de Valve va a tener consecuencias dentro del mundo de desarollo para Linux. Surgen algunas cuestiones que aún no tienen respuesta, pero será interesante ver como se desarrollan los acontecimientos. Algunos ejemplos:


 -Si estáis en esto hace años conoceréis la polémica que hay en cuanto al uso de Wine para poder disfrutar de software que de por si está vetado para GNU-Linux. Mucha gente opina, y no sin cierta razón que usar este wrapper no beneficia al desarrollo de juegos para nuestra plataforma, ya que básicamente estamos usando programas con soporte para Windows, y **con ello favorecemos que los desarrolladores no gasten sus esfuerzos en portar su trabajo a nuestro sistema**, haciendo nosotros con este wrapper el trabajo que deberían hacer ellos. Obviamente, con Proton, el soporte lo da Valve y la comunidad, pero **puede suponer la des-incentivación de las compañías para hacer versiones nativas de sus juegos**. ¿Será realmente así? solo el tiempo lo dirá.  
   
-**Con Proton es muy probable que los juegos no rindan al mismo nivel que en el sistema original**, ya que interponemos una capa de compatibilidad en el medio que exige que nuestros equipos tengan que invertir tiempo y recursos en convertir lo que está hecho para otro sistema. ¿Estará dispuesto el jugador de Linux/SteamOS o lo que es peor, el jugador que salte desde Windows a Linux a lidiar con esta perdida de rendimiento?


-**¿Qué pasará con los desarrolladores?** En el hipotético caso de que aumente la base de jugadores en Linux, ¿verán que podría llegar a ser rentable desarrollar un port nativo u optarán por la solución fácil pero sin soporte de permitir que un tercero (o la comunidad) otorgue el soporte que ellos no brindan? Si el juego funciona bien ellos se lavan las manos y no invierten un duro en atender a una parte de los que pagan su juego. En el caso contrario, también podrían ver malas criticas por el funcionamiento deficiente de su software.


-Otro aspecto a tener en cuenta es **la situación en la que dejan a compañías como Feral, Aspyr o Virtual Programming** , que en su día apostaron por el juego en Linux y ahora ven como estas herramientas les quitan el trabajo de desarrollar versiones "nativas" de los juegos más populares. ¿Continuaran con su ardua tarea o se verán abocadas a cerrar el chiringuito? Siempre les puede quedar el trabajo de portar los juegos con rendimiento deficiente o que no funcionen, pero obviamente el catálogo de posibles juegos a convertir se les reduce.


Estas son algunas de las cuestiones que por ahora se nos plantean sobre este nuevo movimiento por parte de Valve. Puedes plantear las tuyas como siempre o mostrar tu opinión en los comentarios, o charlar con nosotros en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

