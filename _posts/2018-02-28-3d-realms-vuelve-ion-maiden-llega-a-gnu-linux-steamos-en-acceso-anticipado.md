---
author: Pato
category: "Acci\xF3n"
date: 2018-02-28 15:26:47
excerpt: "<p>@voidpnt ha a\xF1adido un nuevo nivel a su juego, entre otras novedades.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3cc3ea028ec2c5bc49d14fb381a9929f.webp
joomla_id: 664
joomla_url: 3d-realms-vuelve-ion-maiden-llega-a-gnu-linux-steamos-en-acceso-anticipado
layout: post
tags:
- accion
- indie
- acceso-anticipado
- gore
- violento
- voidpoint
title: "\xA13D Realms vuelve! Ion Maiden llega a Linux/SteamOS en Acceso Anticipado\
  \ (ACTUALIZADO 4)"
---
@voidpnt ha añadido un nuevo nivel a su juego, entre otras novedades.

**ACTUALIZACIÓN 23-1-19:** Ha pasado medio año ya desde la última vez que informamos sobre este juego, y en aquella ocasión os hablamos sobre el modo multijugador y la salida oficial del juego a principios de este año (ver anterior actualización). Como podreis ver parece que llevan bastante retraso porque a día de hoy, que [han publicado una actualización](https://steamcommunity.com/games/562860/announcements/detail/1719714341795113370) sobre el juego, no vemos ni rastro de esto que acabamos de comentar. Pero tampoco han estado de brazos cruzados y el juego ahora mismo cuenta con un nuevo nivel y más mejoras que os detallamos a continuación:


- "**Heskel’s House of Horrors**" es el nombre del nuevo nivel que podremos jugar.


- Nuevos enemigos como los **Cybercultistas con ballesta**.


- Nuevas armas como la **ballesta**, o la posibilidad de usar **dos Submachineguns** a la vez


- Las armas ahora tienen un **disparo secundario**


Esperamos tener pronto más noticias de ION Maiden, e intentaremos ofreceros pronto un Stream enseñandoos este nuevo nivel, por lo que os recomendamos que permanezcais atentos a nuestros canales de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux). Mientras tanto os dejamos con el trailer del nuevo nivel:
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/OYmMPo6ckqY" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Os recordamos que podeis comprar Ion Maiden (Acceso Anticipado) en [Steam](https://store.steampowered.com/app/562860/Ion_Maiden/) y [GOG](https://www.gog.com/game/ion_maiden).




---


**ACTUALIZACIÓN 18-7-18:** Hace tan solo unos días que me preguntaba como iría el desarrollo de este juego, pues la verdad es que no hay muchas noticias ni parches, pero esta misma mañana nos acaba de llegar un correo electrónico de [StridePR](http://www.stridepr.com/), la agencia que lleva las relaciones publicas de este juego, donde nos informaba de algunos temas que conviene que no pasemos por alto.


El principal es que el juego finalmente **incluirá un apartado Multijugador** y todos aquellos que hayan comprado el juego en acceso anticipado podrán disfrutarlo antes de la salida oficial. En cuanto a esto último **se ha cambiado la fecha de salida del juego** pasando de ser el tercer cuarto de 2018 a principios de 2019. Como ellos mismos comentan, quieren asegurarse de que la experiencia de los fans del juego sea la mejor, y para ello tienen que retrasar unos meses el fin del Acceso Anticipado.


En otro orden de cosas nos informan que la "**Ion Maiden’s Founder’s Edition**" estará limitada a 500 copias firmadas y numeradas a un precio de 99.99$ ya se puede reservar ([ahora mismo quedan 220](http://www.ionmaiden.com/)), incluyendo el juego en una gran caja de cartón, el juego en un disquete-USB, una figura de 6" de "Bombshell" (la protagonista), un poster A3 que incluye un mapa del juego en el reverso, la banda sonora en CD y Digital, pegatinas, alfombrilla para el ratón, tarjetas llave con el arte del juego, y una cinta para el cuello. Como veis la caja viene completita y será una delicia para los fans del juego. Os dejo con un video donde podeis ver mejor el contenido de la caja:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/C3n0we2L0Yc" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>




---


 **ACTUALIZACIÓN 13-4-18**: Termina de llegarnos el [aviso en Steam](https://steamcommunity.com/games/562860/announcements/detail/2631667440819477911) de que tenemos una nueva actualización que viene cargada de cambios, entre las que destaca un nuevo modo de juego que nos hará más fácil la espera por la versión definitiva del juego. Este modo es "La reina de la colina" (Queen of the Hill), y se trata de un modo de supervivencia en el que tendremos que aguantar oleadas de Cyborgs con una "Chaingun". A parte de esto, los cambios más reseñables son:



> 
> -El formato de salvado de partidas ha cambiado, por lo que las anteriores no estarán disponibles.
> 
> 
> -Las opciones de filtrado de texturas han sido desactivadas temporalmente pues se va a continuar trabajando en mejorar el renderizador del juego. Las texturas filtradas volverán en el próximo parche.
> 
> 
> -Scripts optimizados para aumentar el framerate global y reducir los parones.
> 
> 
> -Implementado el precacheado de los archivos de sonido para reducir los parones
> 
> 
> -Mejorado el rendimietno del renderizador OpenGL.
> 
> 
> 


....como veis hay cambios importantes que estamos deseando testear, pero si quereis echar un ojo a todos ellos pasaos por el [anuncio de Steam](https://steamcommunity.com/games/562860/announcements/detail/2631667440819477911).


 




---


**ACTUALIZACIÓN 22-3-18**: [GamingOnLinux](https://www.gamingonlinux.com/articles/3d-realms-voidpoints-build-engine-shooter-ion-maiden-is-now-on-gog.11448) acaba de anunciar que el juego también está disponible ahora en [GOG.com](https://www.gog.com/game/ion_maiden), por lo que ahora teneis más opciones a la hora de comprar tan frenético juego. Para que la versión de GOG os funcione correctamente tendreis que instalar "libsdl2-mixer-2.0-0"..... y ya de paso os dejamos con un video que emitimos hace un par de semanas en nuestro canal de Youtube:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/4OpSjWC4anw" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL**: ¡Sorpresa! 3D Realms, la compañía tras míticos juegos de acción como Duke Nukem 3D, Prey o Max Payne han vuelto a resucitar su motor gráfico "Build" con el que movieron joyas como Blood o Shadow Warrior y otros títulos legendarios de los 90 para traernos este ION MAIDEN:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/QcAwzusZUrQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Características:


* El verdadero sucesor de los shooters clásicos como Duke Nukem 3D o Shadow Warrior
* Artwork creado utilizando las herramientas y metodologías originales de la época.
* 7 excitantes zonas, con múltiples mapas cada una.
* Toneladas de enemigos, armas y casquería!
* Lo mejor de los dos mundos! ION MAIDEN trae de vuelta los FPS clásicos de los 90 que amas con añadidos actualizados como físicas y checkpoints.
* Diseño de niveles clásico, sin generación procedural! tendrás múltiples caminos, efectos geniales y piezas complejas!
* Increíble armamento con modos de disparo alternativos!
* Acceso sencillo a herramientas de desarrollo y modding (Steam Workshop) desde día 1!
* ... y sobre Multijugador... Estar atentos!


En cuanto a los requisitos, son:


* Requiere un procesador y un sistema operativo de 64 bits
* **SO:** Ubuntu 14.04 o Steam OS 2.0 (64 bit)
* **Procesador:** Intel i3 o AMD FX-6300
* **Memoria:** 2048 MB de RAM
* **Gráficos:** Nvidia GeForce 640 con 1GB o AMD R7 260X
* **Almacenamiento:** 100 MB de espacio disponible


Si te gustan los FPS de la vieja escuela, tienes disponible toda la información de este ION MAIDEN en su [página oficial](http://www.ionmaiden.com/), y también lo puedes jugar en acceso anticipado desde su página de Steam, eso si, no está traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/562860/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Qué te parece ION MAIDEN? ¿Jugaste a los clásicos de 3D Realms?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

