---
author: Pato
category: Noticia
date: 2020-03-25 17:26:07
excerpt: "<p>El servicio va tomando impulso poco a poco con Doom Eternal,&nbsp;Tom\
  \ Clancy\u2019s The Division 2, The Crew 2,&nbsp;Serious Sam Collection...</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/Stadiapro25mar.webp
joomla_id: 1196
joomla_url: stadia-sigue-ampliando-catalogo-y-juegos-gratis-para-los-suscriptores
layout: post
tags:
- stadia
title: "Stadia sigue ampliando cat\xE1logo y juegos gratis para los suscriptores"
---
El servicio va tomando impulso poco a poco con Doom Eternal, Tom Clancy’s The Division 2, The Crew 2, Serious Sam Collection...


Stadia suma y sigue. Vamos primero con la incorporación de tres nuevos títulos gratis para los que están suscritos al servicio de Streaming este próximo mes de Abril.


En concreto tendremos la posibilidad de reclamar **Spitlings**, un rápido juego arcade que podrás jugar con hasta 4 amigos online y que requerirá de la máxima coordinación.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/TGo9AvzDO6o" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


También tendremos disponible **Serious Sam Collection**, acción a raudales para soltar adrenalina sin contemplaciones con la colección que incluye las remasterizaciones de Serious Sam: The First Encounter, Serious Sam: The Second Encounter, y Serious Sam 3: BFE.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/J52VjtGa0pI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Por último, Abraza a tu surrealista interior con **Stacks On Stacks (On Stacks)**, un viaje al colorido mundo del absurdo. Apila pilas de bloques cada vez más altas en torres que se derrumban mientras un elenco de personajes estrafalarios te observan.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/tu-_1blRpBM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Por otra parte, también nos anuncian la llegada de novedades para esta semana como son **The Crew 2**, un juego de intensas carreras de estilo libre con los vehículos más variopintos y con el estreno de la característica **Stream Connect** con lo que podrás ver la pantalla de tus oponentes dentro de la tuya para poder ver como van tus carreras, y **Lost Words: Beyond the Page**, un juego en el que tendremos que recorrer los recuerdos y los pensamientos de una joven en una innovadora aventura en 2D que da vida a las palabras de formas sorprendentes. 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/X9-bWlWilps" width="560"></iframe></div>


 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/DoxT8wvjqVc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Los precios para The Crew 2 incluyen descuentos para los suscriptores:


* The Crew 2: $49.99 USD o $15.00 USD con descuento Stadia Pro
* The Crew 2 Deluxe Edition: $69.99 USD, o $18.00 USD con descuento Stadia Pro
* The Crew 2 Gold Edition: $99.99 USD, o $27.00 USD con descuento Stadia Pro
* The Crew 2 Season Pass: $39.99 USD, o $20.00 USD con descuento Stadia Pro


Las ofertas de descuento terminan el próximo día 1 de Abril, así que si te interesa no te duermas en los laureles!.


Así mismo, Lost Words: Beyond the Page tendrá un precio de 14,99$ y estará disponible este próximo Viernes.


Seguimos con las novedades, pues una nueva campaña llegará a **Borderlands 3** este próximo Jueves titulada *Guns, Love, and Tentacles: The Marriage of Wainwright & Hammerlock* que nos llevará a explorar el planeta helado de Xylourgos con un precio de 14,99$ y estará disponible mañana mismo.


Por último, vamos con los lanzamientos ya disponibles de **Doom Eternal,** el FPS más frenético al ritmo de Heavy Metal:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/cX9zdPqCfkA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Y **Tom Clancy’s The Division 2,** y su expansión**Warlords of New York**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/T_FJwyytVRs" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Y por último nos anuncian la llegada de un nuevo "héroe" a la ciudad de Mortal Kombat 11: **Spawn**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/M4o1vuT5lV8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Toda la información sobre estos juegos y los precios y promociones la tienes en los post de los anuncios [en este enlace](https://community.stadia.com/t5/Stadia-Community-Blog/This-Week-on-Stadia-Three-new-free-Stadia-Pro-games-and-much/ba-p/17880) y [en este enlace](https://community.stadia.com/t5/Stadia-Community-Blog/This-Week-on-Stadia-DOOM-Eternal-The-Division-2-and-Spawn/ba-p/17180).


Si quieres disfrutar de Stadia, puedes suscribirte en su [página web oficial](https://stadia.google.com).


Recuerda que **Stadia es un servicio de juego en Streaming** desde los servidores de Google Stadia que corren bajo distribución Debian con la API Vulkan y que es necesaria una suscripción (por el momento) para poder disfrutar del servicio.



 

