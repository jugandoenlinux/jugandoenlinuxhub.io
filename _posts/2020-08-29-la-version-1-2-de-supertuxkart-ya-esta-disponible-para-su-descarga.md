---
author: Serjor
category: Carreras
date: 2020-08-29 08:41:19
excerpt: "<p>Con numerosas cambios, tanto en el apartado gr\xE1fico como en la experiencia\
  \ de juego</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperTuxKart/supertuxkart.webp
joomla_id: 1252
joomla_url: la-version-1-2-de-supertuxkart-ya-esta-disponible-para-su-descarga
layout: post
tags:
- open-source
- codigo-abierto
- supertuxkart
- supertux-kart
title: "La versi\xF3n 1.2 de SuperTuxKart ya est\xE1 disponible para su descarga"
---
Con numerosas cambios, tanto en el apartado gráfico como en la experiencia de juego


Parece que la gente de SuperTuxKart ha pisado el acelerador, publicando una segunda actualización menor en lo que vamos de año.


Y es que este pasado 28 de agosto han publicado la versión [1.2](https://blog.supertuxkart.net/2020/08/supertuxkart-12-release.html) de este longevo juego de carreras, y a pesar de ser una revisión menor, no está exenta de muchas mejoras, de las cuales destacamos:


* Soporte mejorado de gamepads
* Soporte para configurar la cámara en las opciones del juego
* Se añade un nuevo tema, Cartoon, para cambiar el juego de iconos del juego
* Mejorado el sistema de clasificaciones online. Esto ha implicado tener que reiniciar las clasificaciones anteriores.
* Se ha añadido la ubicación de los balones de baloncesto en el minimapa
* Nuevos karts
* Mejoras y nuevas funcionalidades en el apartado online, tanto en el lado del servidor, como en el cliente


De todos modos, la lista es de todo menos corta, así que os animamos a visitar el anuncio oficial del juego para ver la lista completa de mejoras.


Eso sí, nos gustaría destacar el soporte mejorado de gamepads. Han empezado a usar SDL2 en determinadas partes del juego, entre otras, para la gestión de entradas de dispositivos y creación de algunas ventanas, con lo que han conseguido solucionar prácticamente todos los problemas que tenían en este sentido, soportan conectar y desconectar mandos en mitad de una partida y el remapeo de los controles es más sencillo.


En el anuncio comentan también cuales son los planes de futuro. Tienen previsto sacar otra revisión de la versión 1, en la que prevén cambios que si bien no serán tan llamativos como nuevas pistas, cambios en la jugabilidad o efectos visuales, piensan que mejorará notablemente la experiencia de juego, cosa que personalmente pienso que es tan importante (o más) que tener efectos visuales muy bonitos.


A partir de ahí, empiezan a hablar de dar soporte para Vulkan, que si bien STK no es un juego tremendamente exigente, subirse al carro de Vulkan aporta otros beneficios, sobretodo de cara a futuro, permitiéndoles añadir capacidades al juego con una tecnología más actual.


También comentan, que el ritmo de desarrollo depende de nosotros, sus jugadores, y es que como todo proyecto Open Source, hay mucho trabajo y pocos fondos, con lo que nos instan a colaborar, ya sea de manera económica o participando en el proyecto de alguna manera.


Os dejamos con el trailer oficial de esta versión 1.2, la cuál, como es habitual siempre que la gente de STK saca una nueva versión, pide a gritos, y así lo han manifestado algunos miembros de nuestra comunidad, una sesión online, así que pasaros por nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux), que seguro que no van a faltar voluntarios para echar unas carreras.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/_kNtEKfN4Hg" width="560"></iframe></div>

