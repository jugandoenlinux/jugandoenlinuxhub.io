---
author: Serjor
category: Terror
date: 2017-07-26 08:57:06
excerpt: "<p>Techland agradece as\xED a la comunidad la acogida que ha tenido este\
  \ juego</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5709ab37f70d899bd3794356bcaad57a.webp
joomla_id: 420
joomla_url: ya-esta-aqui-el-primer-dlc-gratuito-del-programa-10-in-12-para-dying-light
layout: post
tags:
- dlc
- dying-light
title: "Ya est\xE1 aqu\xED el primer DLC gratuito del programa 10-IN-12 para Dying\
  \ Light"
---
Techland agradece así a la comunidad la acogida que ha tenido este juego

Hace poco Techland nos anunciaba su programa "10-in-12" para Dying Light, un programa en el cuál durante 1 año irían liberando hasta en 10 ocasiones contenido nuevo para el juego de manera gratuita.


Vía actualizaciones de steam nos enteramos de que el primer DLC ya está aquí. En esta ocasión el pack #0, llamado refuerzos, incluye nuevos enemigos, los soldados, un nuevo tipo de zombi, matón mutado y un nuevo skin para nuestro personaje.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/crQdkigSNTo" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Además podemos conseguir también de manera gratuita un nuevo arma, el [Harram Military Rifle](https://gemly.com/shop/Dying-Light---Harran-Military-Rifle-4100000/4100000101), si nos damos de alta en la tienda digital de Techland, gemly.


Nos alegramos mucho de que haya este tipo de iniciativas para alargar más la vida de un juego, lo malo es que ahora nos toca esperar hasta septiembre para ver qué trae el nuevo DLC.


 


Y tú, ¿juegas a Dying Light? Cuéntanos qué te parecen estas iniciativas en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

