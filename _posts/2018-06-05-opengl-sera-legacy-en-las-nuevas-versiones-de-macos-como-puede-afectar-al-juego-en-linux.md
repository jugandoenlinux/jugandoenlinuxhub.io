---
author: Pato
category: Editorial
date: 2018-06-05 15:59:16
excerpt: "<p>Analizamos la decisi\xF3n de Apple y su repercusi\xF3n en cuanto al juego\
  \ en Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9e18ae44efb984e430f0539350fef656.webp
joomla_id: 754
joomla_url: opengl-sera-legacy-en-las-nuevas-versiones-de-macos-como-puede-afectar-al-juego-en-linux
layout: post
tags:
- vulkan
- opengl
title: "OpenGL ser\xE1 \"legacy\" en las nuevas versiones de MacOS. \xBFComo puede\
  \ afectar al juego en Linux?"
---
Analizamos la decisión de Apple y su repercusión en cuanto al juego en Linux

Poco podíamos imaginar que ayer se iba a desatar una "tormenta" que nos ha alcanzado de refilón. Hablo del anuncio de la nueva versión de **macOS** "Mojave" y la intención de Apple de hacer de OpenGL una API con soporte "Legacy" dejando como única opción viable de cara al desarrollo en su plataforma a su propia API propietaria "Metal".


Un momento... ¿Esto no es Jugando en Linux? ¿o es que hemos cambiado a "jugando en Mac y no nos hemos enterado? ¿Que hacemos hablando de Mac aquí?


Pues es sencillo de entender si tomamos en cuenta que la **decisión de dejar de soportar OpenGL** por parte de Apple nos puede afectar en cuanto al juego en Linux, aunque no tenemos claro si para bien o para mal. Para intentar aclararlo, vamos a poner unos cuantos puntos sobre la mesa:


* Tras el anuncio del cese de soporte de OpenGL por parte de Apple, multitud de desarrolladores, sobre todo pequeños estudios o programadores "indie" han puesto el grito en el cielo por esta decisión puesto que desarrollar con una API "cruzada" para todos los sistemas como es OpenGL les supone una gran ventaja al poder desarrollar con menos esfuerzo y menor coste, y así poder llegar a mas sistemas y usuarios. Por contra, pasar sus desarrollos a una nueva API, además exclusiva como es Metal y que no tiene tanto "atractivo" en cuanto a base de usuarios les supone un sobre-esfuerzo de tiempo de aprendizaje y desarrollo que muchos no están dispuestos a asumir. Como ejemplos mas claros podéis ver los tweets de [Lars Doucet](https://twitter.com/larsiusprime/status/1003730058488483841), [J. Kyle Pittman](https://twitter.com/PirateHearts/status/1004000225285980161) o [Tom Coxon](https://twitter.com/tccoxon/status/1003947194775494657), por citar solo tres, pero hay muchísimos más.
* Por otra parte, hay quien afirma que nada ha cambiado (de momento), ya que aunque el soporte pasa a ser "legacy" para OpenGL es cierto que las aplicaciones no van a dejar de funcionar de la noche a la mañana, dando tiempo a que los desarrolladores vayan introduciendose en Vulkan y Metal para sus próximos desarrollos y así haciendo que la transición no sea tan dura.
* También hay quien argumenta que fuera de los desarrollos basados en motores de grandes compañías, como Unity o Unreal que ofrecen soporte para las tres APIs principales, para el resto son muy malas noticias. Así piensa por ejemplo el ex de Feral "[Marc Di Luzio](https://twitter.com/mdiluz/status/1003753839051329543)", ahora trabajando en Unity.
* Hay muchos que tienen las esperanzas puestas en **MoltenVK**, el "puente" open source que Valve se sacó de la chistera para poder "traducir" Vulkan a Metal, aunque el desarrollo en ese ámbito apenas acaba de comenzar y supondría el giro de todo estudio o desarrollador indie hacia Vulkan, lo que no es malo "per-se" pero si supone un sobreesfuerzo adicional.
* La última "perla" que ha llegado es la de "**Codeweavers**". El giro parece que les va a afectar también, y aunque los adalides de "Wine" afirman estar trabajando en el futuro soporte en Mac, el presidente de la compañía ya está "[haciendo las maletas](https://www.codeweavers.com/about/blogs/jramey/2018/6/1/linux-the-final-frontier-part-one)" hacia Linux, "la última frontera".


Sea como fuere, lo que si es cierto es que buena parte de los desarrollos que nos llegan lo hacen también gracias a que desarrollar para Mac supone que los costes que se necesitan para llegar a Linux son menores que si tuviesen que "portar" tan solo para Linux y su base de usuarios.


Sumados a los de Mac somos un mercado al menos a tener en cuenta. Sin ese mercado, ¿Crees que merecería la pena desarrollar para nuestro sistema favorito si los desarrolladores dejan de lado a Apple por su "aparente mala" decisión? ¿Crees que esto hará replantearse a los estudios el soporte a Linux? ¿O piensas que esto ayudará a implantar definitivamente a Vulkan y nos traerá muchos más desarrollos y de mejor calidad?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

