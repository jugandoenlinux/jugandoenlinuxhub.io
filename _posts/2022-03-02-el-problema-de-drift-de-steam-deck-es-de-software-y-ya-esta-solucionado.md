---
author: Pato
category: Hardware
date: 2022-03-02 09:49:21
excerpt: "<p>El propio&nbsp;@lawrenceyang ha confirmado que el problema deber\xED\
  a estar arreglado en la \xFAltima actualizaci\xF3n</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/image_2021-07-16_17-53-14.webp
joomla_id: 1442
joomla_url: el-problema-de-drift-de-steam-deck-es-de-software-y-ya-esta-solucionado
layout: post
tags:
- steamos
- steam-deck
title: "El problema de drift de Steam Deck es de software y ya est\xE1 solucionado"
---
El propio @lawrenceyang ha confirmado que el problema debería estar arreglado en la última actualización


En las últimas horas han comenzado a aparecer noticias de que **algunas Steam Deck estarían sufriendo un problema de "drift"** o "deslizamiento" del joystick derecho. El problema supone que cuando el joystick se mueve hacia uno de sus lados y luego retorna a su posición central, el sistema lo detecta como que sigue activado hacia ese lado ya sea por un problema de hardware o software.


Es un problema conocido en el mundo de los mandos para juegos, ya que se viene sufriendo desde hace algún tiempo en diversos dispositivos y se achaca a una mala calidad en la construcción de dichos joysticks.


![Thumbstick](https://valkyrie.cdn.ifixit.com/media/2022/02/15101829/joystick.jpg)


*imagen del detalle del stick de [ifixit.com](https://www.ifixit.com/News/57101/steam-deck-teardown)*


En el caso de la Deck, y según las palabras de su diseñador Lawrence Yang **parece que el problema es de software** tal y como explica en su tweet:


>
> Hi all, a quick note about Steam Deck thumbsticks. The team has looked into the reported issues and it turns out it was a deadzone regression from a recent firmware update. We just shipped a fix to address the bug, so make sure you’re up to date.
>
>
> — Lawrence Yang (@lawrenceyang) [March 2, 2022](https://twitter.com/lawrenceyang/status/1498839079558926341?ref_src=twsrc%5Etfw)


>
> *Hola a todos, una nota rápida sobre los sticks de Steam Deck. El equipo ha investigado los problemas reportados y resulta que fue una regresión de zona muerta de una reciente actualización de firmware. Acabamos de enviar una solución para abordar el error, así que asegúrate de estar actualizado.*
>
>
>


**El problema de hecho debería estar ya solucionado** pues parece que algunos usuarios que denunciaron el problema en reddit ya están reportando que todo vuelve a la normalidad.


En el caso de que el problema hubiera sido de hardware, la única solución sería sustituir el joystick afectado por otro, cosa que en los mandos de otros dispositivos puede suponer un problema. Sin embargo en el caso de Steam Deck, **Valve se ha asegurado que las placas de los mandos sean independientes, y que sea sencillo extraer de ellas los mandos para poder sustituirlos en caso de problemas**. Tan solo hay que quitar un par de tornillos y desconectar un cable. Eso es todo. Es más, según anunciaron desde Valve, se pondrán a la venta repuestos para la Steam Deck en distintos distribuidores autorizados siendo [ifixit](https://www.ifixit.com/News/57101/steam-deck-teardown) uno de ellos.


A continuación podéis ver el despiece que la propia Valve publicó en su momento para mostrar lo "sencillo" del proceso:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Dxnr2FAADAs" title="YouTube video player" width="560"></iframe></div>
