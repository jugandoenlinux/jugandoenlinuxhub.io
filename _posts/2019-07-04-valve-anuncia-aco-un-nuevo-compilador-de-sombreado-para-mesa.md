---
author: Pato
category: Software
date: 2019-07-04 16:26:56
excerpt: "<p>Sus \xFAltimos avances est\xE1n teniendo unos resulados impresionantes\
  \ con gr\xE1ficas AMD Radeon GCN 1.0. </p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/09dad4a644cf4e7edd2e0dd7a337d51f.webp
joomla_id: 1071
joomla_url: valve-anuncia-aco-un-nuevo-compilador-de-sombreado-para-mesa
layout: post
tags:
- software
- drivers
- mesa
title: "Valve anuncia ACO, un nuevo compilador de sombreado para Mesa (ACTUALIZACI\xD3\
  N). "
---
Sus últimos avances están teniendo unos resulados impresionantes con gráficas AMD Radeon GCN 1.0. 


**ACTUALIZACIÓN 31-1-20:** Ayer veíamos un artículo en la siempre genial [Phoronix](https://www.phoronix.com/scan.php?page=article&item=radv-aco-gcn10&num=1) donde se realizaban montones de tests a diferentes juegos usando este compilador de sombreado, y los resultados arrojan una **diferencia de rendimiento abrumadora del 9%** en gráficas AMD Radeon GCN 1.0 con respecto a LLVM. Esto quiere decir que tarjetas tan longevas como las **Serie HD 7000** (Southern Islands) podrían revitalizarse y alargar un poco más su vida.


Para ello, tal y como podeis ver en [el artículo original](https://www.phoronix.com/scan.php?page=article&item=radv-aco-gcn10&num=1), se han realizado pruebas usando la versión de Mesa 20.0-devel por defecto y comparándola con la misma pero usando ACO. Los resultados muestran que en juegos como Dota 2, Hitman 2, Mad Max, Rise y Shadow of the Tomb Raider y otros muchos había una notable diferencia, por lo que todos aquellos que tengais gráficas de esta serie deberíais pensar en usar este compilador de Sombreado.


 




---


**NOTICIA ORIGINAL:**


De nuevo es Valve quien mueve ficha. Esta vez Pierre Loup, el cabeza visible de la programación de Valve y Steam para Linux [ha anunciado la creación de **ACO**](https://steamcommunity.com/games/221410/announcements/detail/1602634609636894200), un nuevo compilador de sombreado para los drivers Mesa, o lo que es lo mismo, para las gráficas de AMD e Intel.


Según cuenta en el post del anuncio, tras haber logrado el éxito con el grupo de ingenieros de Valve para el proyecto de drivers Mesa (lo que se tradujo en la evolución increíblemente exponencial de los drivers Mesa que hoy podemos disfrutar) con la consabida creación del grupo de drivers gráficos Open Source de Valve, pudieron dar soporte directo a los usuarios de gráficas de ambas compañías. Gracias a la ayuda de la comunidad, pudieron trabajar en solucionar los problemas que fueron surgiendo en los juegos que tenían problemas o que directamente no funcionaban con los drivers Mesa.


Según cuenta Pierre, a principios de año y con el crecimiento del grupo de ingenieros se plantearon la idea de llevar el soporte a una mayor escala, ya que según sus palabras la mayor parte de los problemas que han ido solucionando a lo largo del tiempo tienen que ver con problemas en el compilado de sobreados. Siendo así, en vez de esperar a ir solucionando los problemas de los juegos según vayan surgiendo, de lo que se trata es de atacar el problema desde un punto de vista mas práctico, y para eso han creado su propio compilador de sombreado.


*Los controladores AMD OpenGL y Vulkan actualmente utilizan un compilador de sombreado que forma parte del proyecto LLVM. Ese proyecto es muy grande y tiene muchos objetivos diferentes, y la compilación en línea de sombreadores de juegos es solo uno de ellos. Eso puede dar lugar a compromisos de desarrollo, donde la mejora de la funcionalidad específica de los juegos es más difícil de llevar a cabo de lo que lo haría de otra manera, o donde las características específicas de los juegos a menudo se rompen accidentalmente por los desarrolladores de LLVM que trabajan en otras cosas. En particular, la velocidad de compilación del sombreador es un ejemplo: no es realmente un factor crítico en la mayoría de los otros proyectos, aunque es bueno tenerla. Pero para los juegos, el tiempo de compilación es crítico, y la compilación lenta del sombreado puede resultar en un tironeo que hace casi imposible jugar.*


De este modo, hace un año se pusieron manos a la obra y ahora nos presentan **ACO,** cuyo objetivo es convertirse en **un compilador de sombreado específico pensado por y para juegos en el que la prioridad será la mejor generación de código posible a la mayor rapidez posible**. Por el momento han empezado con **RADV,** y tras conseguir hacer funcionar varios juegos han logrado una diferencia sustancial en la rapidez de compilado:


![radvfasterACO](/https://steamcdn-a.akamaihd.net/steamcommunity/publichttps://gitlab.com/jugandoenlinux/imagenes/-/raw/main/clans/4178173/e1a045a816b805c0273ffc133c3f4f5b7bffbb39.webp)


Por ahora ACO solo se centra en el compilado de píxeles y "*compute shaders*" pero esperan seguir implementando el resto de fases de compilado y reducir estos tiempos aún mas.


Según las pruebas realizadas, el código generado por ACO no tiene por qué tener un impacto directo en los FPS de los juegos, aunque dependiendo del caso pueden darse ciertas mejorías de rendimiento:


![ACOShaderFPS](/https://steamcdn-a.akamaihd.net/steamcommunity/publichttps://gitlab.com/jugandoenlinux/imagenes/-/raw/main/clans/4178173/156f8bb448b74a32a6e89eed634823b2f329bd71.webp)


De momento ACO está siendo revisado por los desarrolladores de Mesa, y como es normal en este tipo de proyectos cuantos más usuarios reporten sus resultados mejor podrán los desarrolladores seguir mejorando ACO en beneficio de todos. Si quieres colaborar y **tienes una gráfica AMD corriendo Vulkan, GCN 3.0+ y los drivers Mesa RADV** puedes pasarte por el post de instrucciones para testear ACO [**en este enlace**](https://steamcommunity.com/app/221410/discussions/0/1640915206474070669/).


¿Qué te parece el desarrollo de un nuevo compilador de sombreado aparte de LLVM? ¿ves positiva la implicación de Valve y su gran impulso para el juego en Linux con estas iniciativas Open Source?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

