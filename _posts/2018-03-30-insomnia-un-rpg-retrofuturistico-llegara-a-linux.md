---
author: leillo1975
category: Rol
date: 2018-03-30 11:33:25
excerpt: "<p><span class=\"username u-dir u-textTruncate\" dir=\"ltr\" data-aria-label-part=\"\
  \">Nuevas noticias sobre la versi\xF3n de Linux de <span class=\"css-901oao css-16my406\
  \ r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\">@InsomniaTheArk</span><br /></span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3329d3457818ad53d5d679622eeb71cc.webp
joomla_id: 695
joomla_url: insomnia-un-rpg-retrofuturistico-llegara-a-linux
layout: post
tags:
- rol
- greenlight
- kickstarter
- unreal-engine-4
- artdeco
- noir
- postapocaliptico
title: "\"InSomnia: The Ark\", un RPG retrofutur\xEDstico, llegar\xE1 a Linux (ACTUALIZADO\
  \ 5)"
---
Nuevas noticias sobre la versión de Linux de @InsomniaTheArk  


**ACTUALIZACIÓN 30-9-19:** Por desgracia el equipo de desarrollo de "[Insomnia: The Ark](https://insomnia-project.com/)", mono estudios han vuelto a retrasar una vez más el lanzamiento de la versión de Linux de su juego, posponiéndola una vez más después de la versión 1.7. Según podemos leer en la respuesta a nuestra pregunta en los [comentarios de la noticia del pimer aniversario del juego en Steam](https://steamcommunity.com/app/783170/eventcomments/1629664606990443683/?ctp=2#c1629664607000719650), la razón que esgrimen en esta ocasión es que **los bugs que están causando esta demora siguen estando, por desgracia, presentes**. Esperemos que más pronto que tarde podamos jugar en nuestros equipos a este juego de forma nativa, ya que además por lo que se ve en [ProtonDB](https://www.protondb.com/app/783170) los reportes de su funcionamiento con Steam Play/Proton no son todo lo buenos que deberían para plantearse esta opcón como una alternativa adecuada.




---


**ACTUALIZACIÓN 16-08-19:** Como veis ya han pasado bastantes meses desde la última vez que os hablamos de este juego, y parece que finalmente la versión para nuestro sistema está realmente cerca, según lo que hamos podido leer en el [recién publicado post](https://steamcommunity.com/games/783170/announcements/detail/1607142740272385149) que habla de la próxima versión 1.6 en Steam. Sus desarrolladores, Mono Studios, han reiterado en varias ocasiones su intención de lanzar una versión para nuestro sistema, tal y como habían prometido en sus campañas de Kickstarter en el pasado y en sus frecuentes comunicaciones con los usuarios. Por ahora **aun no hay una fecha definitiva para que podamos disfrutar de este juego de Rol en Linux/SteamOS**, pero a partir del futuro lanzamiento de la próxima versión esto podría ser una realidad. Esperemos que sea más pronto que tarde, ya que el juego en nada cumplirá un año desde su lanzamiento en Windows.




---


**ACTUALIZACIÓN 30-11-18:** Segun nos han comunicado los desarrolladores de este título, la versión para GNU-Linux de InSomnia tendrá que esperar aun unos cuantos meses, pues están teniendo problemas con la versión de Windows. Según Studio Mono, se encuentran trabajando actualmente en corregir y mejorar partes importantes del juego, y cuando concluyan estos trabajos, que estiman que será a partir de Febrero de 2019, se pondrán a tiempo completo para lanzar las versiones tanto de Mac, como de Linux/SteamOS.


Nos aseguran que sus planes con respecto a nuestro sistema no han cambiado y que siguen siendo una realidad. También lamentan que les esté llevando más tiempo de lo esperado poder ofrecernos el soporte.




---


**ACTUALIZACIÓN 6-9-18:** Desde los [foros de Steam](https://steamcommunity.com/app/783170/discussions/0/1697169163396167381/?ctp=2#c1733212454824470813) nos acabamos de enterar que al parecer el juego no estará en las "estanterias" virtuales para nuestro sistema al mismo tiempo que en Windows debido a que están teniendo problemas que tienen que corregir en la beta privada que utilizan para testear:



> 
> *"We will finish our work Linux version after the initial release. The beta test showed us many things we had to update so it took more time than we expected."*
> 
> 
> 


Esperemos que la versión de GNU-Linux/SteamOS, que llegará a Windows el próxima 27 de Septiembre, no se posponga mucho en el tiempo y pronto podamos disfrutarla, pues el juego promete mucho.




---


**ACTUALIZACIÓN 21-5-18**: Receintemente se acaba de publicar un nuevo y espectacular trailer del juego donde se muestra más contenido de este. La verdad es que consigue aumentar bastante el Hype... Podeis verlo justo aquí debajo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/Pd_g_9WFfvo" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL**: Acabamos de recibir gracias a [@**G4LNevertheless,**](https://twitter.com/G4LNevertheless)  información de este interesante proyecto, que está siendo desarrollado por la compañía Rusa [Studio Mono](http://mono.studio/). El juego, que consiguió hace un par de años financiación para su creación en [Kickstarter](https://www.kickstarter.com/projects/1892480689/insomnia-pc-max-linux/description), y aprobación en el "difunto" [Steam Greenlight](https://steamcommunity.com/sharedfiles/filedetails/?id=269018411), parece que está en sus últimos estadios antes del lanzamiento y acaban de anunciar que será puesto a la venta este 2018. También han aclarado quien será su editora, su compatriota [HeroCraft](http://www.herocraft.com/), especializada en juegos indies.  



Se trata de un juego de Rol en tiempo real ambientado en una metrópolis espacial llena de secretos de una civilización anterior. Nosotros seremos **#KZ0012**, un habitante de este inquietante lugar, que acaba de despertar de una especie de criogenización y se encuentra de repente envuelto en unos disturbios que provocan el estado de escepción. Poco a poco se ve envuelto en un unas circunstancias misteriosas que hacen que el futuro de los habitantes de esta ciudad esté en sus manos.  



Nos encontraremos con escenarios diseñados tomando como referencia el siglo XX, elementos de **Art-Decó** y estilo "**Noir**" post-apocaliptico.  Todo ello está siendo creado con **Unreal Engine 4**, por lo que la facilidad para traerlo a nuestro sistema sin problemas es mucho más alta. Tal y como han comentado sus desarrolladores en los [foros de Steam](http://steamcommunity.com/app/783170/discussions/0/1697169163396167381/#c1697169163396224192), su idea es traerlo a todos los sistemas a la vez, por lo que probablemente lo veamos el primer día junto con la versión de Mac y Windows, algo que por supuesto es digno de agradecer y elogiar. Pero mejor que muchas palabras son las imágenes, y para haceros una idea más aproximada de lo que vais a encontrar, podeis ver el Trailer oficial justo aquí debajo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/vNbGbR_2IIQ" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 Como veis el juego promete y es de lo más interesante. Nosotros por supuesto lo vamos a seguir y cualquier actualización que se produzca os la ofreceremos lo más puntualmente posible.El juego podreis adquirirlo en la **[tienda de Humble Bundle](https://www.humblebundle.com/store/insomnia-the-ark?partner=jugandoenlinux)**, donde podeis añadirlo a la lista de deseos para estar más informados de su salida oficial.


¿Qué te parece este "InSomnia: The Ark"? ¿Disfrutas de los títulos de Rol? Déjanos tus impresiones en los comentarios o charla sobre el en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

