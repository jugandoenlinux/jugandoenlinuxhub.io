---
author: leillo1975
category: Entrevistas
date: 2018-03-06 20:54:06
excerpt: "<p>Hablamos con dos de sus desarrolladores</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e50216f3bf021078f78a0e2e997a0047.webp
joomla_id: 662
joomla_url: entrevista-speed-dreams-un-simulador-de-coches-libre
layout: post
tags:
- simulador
- open-source
- racing
- speed-dreams
- madbad
title: 'Entrevista: Speed Dreams, un simulador de coches libre.'
---
Hablamos con dos de sus desarrolladores


Si recordais, a principios de verano del año pasado, publicamos un par de videos ([1](https://youtu.be/jXCngyTtCQc) y [2](https://youtu.be/TjqLT28tYlk)) en nuestro canal de [Youtube](https://www.youtube.com/channel/UC4FQomVeKlE-KEd3Wh2B3Xw) sobre [Speed Dreams](http://www.speed-dreams.net), un simulador de coches Open Source basado en el trabajo anteriormente hecho en [TORCS](http://torcs.sourceforge.net/). En aquel momento intentamos contactar con el equipo de desarrollo del proyecto sin mucha suerte. Recientemente lo hemos vuelto a intentar y ha sonado la flauta por partida doble. En primer lugar nos ha contestado **MadBad**, uno de los integrantes del equipo de desarrollo. Poco después conseguimos contactar con el programador principal, **Xavier Bertaux**. Como queremos respetar el esfuerzo que han realizado tanto uno como otro dándonos parte de su codiciado tiempo vamos a publicar las dos aunque donde las respuestas puedan ser iguales o semejantes vamos a obviar la respuesta.


Y sin más dilación vamos al lio y empezemos con la entrevista:


**JugandoEnLinux (JeL):** *¿Podeis darnos una pequeña explicación sobre vuestro proyecto?  (desarrolladores, cuando comenzaste en el proyecto, historia)*



> 
> **MadBad (MB):** Hay 5 desarrolladores activos que yo conozca:  
> -Xavier (fundador y desarrollador principal)  
> -Joe (desarrrollador y jefe de lanzamientos)  
> -Kilo-Kristof  (desarrollador de la mayor parte de las físicas)  
> -Leopjing-jjsca (modelado/gráficos)   
> -Madbad ("random crap developer" ... no lo voy a traducir - webserver y force feedback)  
> Speed-Dreams fué iniciado por Xavier y otro desarrollador (Jean-Philippe) en 2008 como un fork de el simulador de carreras TORCS, destinado a ser más orientado al usuario que el original. El proyecto está basado en contribuciones voluntarias. Personalmente mi primera contribución al código corresponde al año 2012 (<https://sourceforge.net/p/speed-dreams/tickets/568/>)
> 
> 
> 



> 
> **Xavier Bertaux (XB)**:  En 2008 le escribí a Jean Philippe para ofrecerle un fork de TORCS a partir de la renuncia del desarrollador del proyecto a incluir en el proyecto algunos parches que yo creí necesarios para los usuarios. Sin noticias de él durante 3 semanas, empecé el proyecto de migrar el codigo en SVN. Jean Philippe se unió a mi después de ello. Después de algunos meses, algunos desarrolladores se unieron a nosotros para participar en la funcionalidad que ellos querían en el proyecto. Hoy en día, muchos nos han abandonado por culpa de la vida diaría (falta de tiempo) o desacuerdos internos entre los integrantes. Desafortunadamente esto solo ocurre en los proyectos de Código Libre.
> 
> 
> 


 ![speed dreams 20 car ls1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/speed-dreams-20-car-ls1.webp)


  
**JeL:** *¿Trabajais o habeis trabajado antes en otros proyectos (juegos, software)? (tu y el resto de desarrolladores)*



> 
> **MB:** No, no trabajo para ningún otro proyecto y realmente no se si otros desarrolladores lo hacen. Estoy bastante seguro de que un antiguo desarrollador, Simon Wood, envió algunos parches al kernel de Linux principalmente sobre volantes Logitech.
> 
> 
> **XB**:Si, Simon ha enviado algunos parches al kernel de Linux. Yo tenía una pequeñá contribución en el proyecto FlighGear y también participé en el desarrollo de una distribución francesa de Linux, Mageia. Pero desafortunadamente, tampoco hay tiempo para hacer más. Para otros desarrolladores, pues no lo sé, pero muchos programan en sus vidas profesionales.
> 
> 
> 


  
**JeL:** *¿Qué distribuciones de Linux usais para desarrollar Speed Dreams?*



> 
> **MB:** Yo soy un fan de Fedora desde hace mucho tiempo. Se que Xavier (el desarrollador principal) usa linux como sistema operativo pero no tengo ni idea de que distribución. Realmente no se nada sobre el SO del resto de desarrolladores.
> 
> 
> **XB**: Uso la distribución de Mageia, con la que tengo una implicación, ya que después de haber probado todas las primeras distribuciones importadas en Francia, descubrí  Mandrake (un fork de Redhat, pues Fedora aún no existía) Mandrake se convirtió en Mandriva siguiendo un cambio de dirección. Después de  unos pocos años florecientes, las elecciones del comité ejecutivo llevaron a esta empresa más o menos a la cesación de actividad con el despido resultante. El equipo técnico que había sido 'purgado' decidió fundar la Mageia en una distribución puramente comunitaria.  
> Del resto, algunos desarrolladores usan Windows y otros usan Debian y derivados.
> 
> 
> 


  
**JeL:** *¿Qué motor y herramientas usais principalmente para desarrollar Speed Dreams?*



> 
> **MB:** Como debes saber, Speed Dreams es un juego hecho desde cero, por lo que no usa ningún motor existente. Para renderizar los gráficos, nosotros actualmente tenemos dos opciones, PLIB (módulo SSG) o OpenScenegraph (módulo OSG). Las físicas están hechas todas desde cero también. Hablando sobre las herramientas las más usadas son Geany (como editor del código), SVN (como sistema de control de versiones), Blender (para modelar los objetos 3D) y GIMP (para las texturas).
> 
> 
> **XB**:Al principio usamos el mismo código que TORCS, por lo tanto PLIB para código de renderizado y scratch OpenGL 1.2 para simulación. PLIB ya no es soportado por ningún desarrollador, el software que usa migró poco a poco en otras librerias gráficas (OGRE, [Openscenegraph](http://www.openscenegraph.org/), etc...). Tras varias discusiones finalmente escogimos Openscenegraph para el futuro (soporta OpenGL 3). Personalmente uso Qt-creator para editar y compilar.
> 
> 
> 


![speed dreams 20 daytime dusk](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/speed-dreams-20-daytime-dusk.webp)


**JeL:** *¿Cómo es el proceso de desarrollo? Hablanos un poco sobre él.*



> 
> **MB:** Hablaré por mi solamente. Empecé a contribuir en Speed Dreams unicamente por que quería algunas características que no estaban presentes en el juego en ese momento. Todavía es lo mismo hoy para mí, realmente me gustaría contribuir más a este proyecto, pues hay muchas cosas que me gustaría implementar, pero me falta tiempo/concentración/capacidad para hacerlo. Solo programo cuando tengo el tiempo y la voluntad para hacerlo.
> 
> 
> **XB**: En teoría, integramos funcionalidades a medida que avanzamos en la rama principal, hasta que decidimos corregir esta rama para el lanzamiento. De acuerdo con el tiempo disponible por cada uno y nuestro grado de motivación, este tiempo puede ser más o menos mayor. Para el juego en red nosotros necesitamos encontrar un desarrollador el cual pueda terminarlo. Debe conocer la librería "enet".  En este momento este código funciona en una red local pero no muy bien en internet ( tiene demasiado lag y problemas con el módulo de simulación)
> 
> 
> 


 **JeL:** *¿Cual es el estado actual del proyecto en este momento?*



> 
> **MB:** ¿Honestamente? Un poco parado para mi gusto. Ya ha pasado bastante tiempo desde la última versión. Me encantaría ver más frecuentemente lanzamientos y apuesto que tambien podría ayudar a conseguir algunas contribuciones del exterior. Ya hay bastantes caracteristicas nuevas casi listas para ser lanzadas  como Force Feedback, webserver con registro de usuarios y recolección de datos, un primer HUD para el módulo OSG, incluso un prototipo (necesita más trabajo) de multiplayer via LAN.
> 
> 
> **XB**:Estoy de acuerdo con MadBad, demasiado tranquilo, pero desafortunadamente todos dependemos de nuestra propia vida.
> 
> 
> 


  
**JeL:** *¿Con que frecuencia lanzais actualizaciones? ¿Teneis algún plan de lanzar alguna pronto? Háblanos de las próximas características*



> 
> **MB:** No hay una fecha fijada para las actualizaciones. Hacemos una reunión de chat e intentamos discutir los desarrollos futuros con una hoja de ruta y todo, pero eso generalmente termina no siendo cumplido debido al estancamiento del desarrollo. Es posible que un nuevo lanzamiento no esté demasiado lejos (enumeré algunas de las características nuevas en la pregunta anterior), pero no quiero prometer nada, no quiero ver a la gente cabreada por el incumplimiento de fechas límite.
> 
> 
> **XB**: Madbad ha respondio perfectamente a esta cuestión.
> 
> 
> 


![speed dreams 20 menue 07](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/speed-dreams-20-menue-07.webp)  
**JeL**: *¿Cuales son vuestras metas a corto y largo plazo? ¿Cual es el objetivo final del proyecto?*



> 
> **MB**: ¿Para Speed Dreams?, me encantaría que se convirtiese en un completo simulador de carreras, algo así como rFactor2 (este simulador se come todo mi tiempo libre)
> 
> 
> **XB**:¡La misma respuesta! Soy un apasionado de los simuladores tipo rFactor2, Grand Prix Legends, Grand Prix 2....   Personalmente me gustaría que Speed Dreams estuviese en mi lista de simuladores favoritos. 
> 
> 
> 


 


**JeL**:  *A nivel personal, soy un gran aficionado a los simuladores de carreras, y estoy muy interesado en saber si vosotros teneis en mente implementar el Force Feedback en volantes en un futuro próximo.*



> 
> **MB**: ¡Yo también soy un gran fan del Simracing! Hay en este momento una implementación del Force Feedback en la versión de desarrollo. Puede que no sea el Force Feedback más detallado del mercado, pues necesita muchos ajustes, pero siento como él comienza a entregar buenas sensaciones. Aun no estoy seguro si el será añadido como una nueva característica en el siguiente lanzamiento.
> 
> 
> **XB**:Si, existe una implementación del Force Feedback funcional, pero actualmente nosotros tenemos un problema con algunos drivers el el núcleo de Linux. Por ejemplo, no funcionan todos los efectos de mi G25, solo la fuerza constante funciona bien.
> 
> 
> 


 


**JeL**: *¿Teneis algún tipo de soporte en vuestro proyecto? Me refiero tanto a soporte económico, como feedback de los usuarios, etc*



> 
> **MB y XB**: No hay contribuciones económicas que yo sepa. Nosotros apreciamos mucho cualquier feedback contructivo de los usuarios, y el reporte de bugs siempre es bienvenido.
> 
> 
> 


 ![speed dreams 20 car supercars](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/speed-dreams-20-car-supercars.webp)


**JeL**: *¿Como veis la escena gaming del software libre en Linux?*



> 
> **MB**: Como siempre en el Open Source hay un montón de proyectos, pero solo algunos de ellos pueden despuntar del resto y atraer los suficientes desarrolladores para crecer en calidad y número de usuarios; pero creo que está en buena forma.
> 
> 
> **XB**: Estoy de acuerdo que muchos proyectos nacen y mueren  en el open source, pero todos los proyectos no tienen por que morir pues es suficiente abrir un Fork para reiniciar un nuevo proyecto sobre la base de este (OpenOffice/LibreOffice, MySQL/MariaDB), incluso si para esos proyectos el objetivo de realizar forks no tiene otra meta que escapar de una compañía. En los juegos OpenSource sigo muchos proyectos, pero muchos de ellos tienen un desarrollo muy largo por falta de integrantes.
> 
> 
> 


**JeL**: *¿Cual es tu opinión sobre la llegada de los juegos privativos a Linux? ¿Crees que es positivo o negativo?*



> 
> **MB**: ¡Yo creo que es algo positivo! Estos juegos han dado a la plataforma una gran oportunidad de crecer. ¿Recordais que malos eran los drivers de la gráficas antes de la llegada del cliente de Steam? Siempre estoy a favor de "cuanto más, mejor".
> 
> 
> **XB**: No estoy seguro de que esto sea algo bueno para los juegos de código abierto, muchos programaron un juego porque faltaba en Linux y buscaron reproducir determinados juegos. Con la llegada de los juegos comerciales, ¿para qué querrías  programar un juego cuando ya existe? El problema que tengo con los productos comerciales es que ellos no tienen seguimiento, especialmente los juegos. El día en que dejan de ser rentables se dejan de distribuir. ¡Esto es solo mi opinión! La ventaja que actualmente nosotros tenemos de todo esto, es mejor soporte de nuestros drivers.  
> 
> 
> 
> 


  
**JeL**: *Recientemente SuperTuxKart fué aprovado por la comunidad en el desaparecido Steam Greenlight, y él será lanzado en esta plataforma en el futuro. ¿Podríais llegar a usar plataformas como Steam, GOG o Itch.io para distribuir vuestro juego? ¿Qué pensais sobre estas plataformas?*



> 
>   
> **MB**:  Incluso si yo pienso que podría ser algo bueno para conseguir mayor viisbilidad para el proyecto , no creo que estemos premparados para tan gran audiencia. Somos un equipo pequeño, y no estoy seguro de que nosotros podamos manejar bien toda la presión que esto podría poner en nosotros. Vamos a mantener nuestro lento crecimiento, atraer a algunos desarrolladores más y quizás un día nosotros podríamos estar listos.
> 
> 
> **XB**:¡No! Por razones de tiempo otra vez. Actualmente tenemos un montón de problemas para administrar el tiempo de nuestro lanzamiento, si tenemos que dispersarnos para administrar todas las plataformas esto puede ser ingobernable para nosotros. Personalmente no soy un seguidor de la plataforma Steam, y si ya damos soporte a Linux, asi como Windows, y nuestro primer trabajo para Mac está echo, ¿para que añadir algo encima de esto?
> 
> 
> 


![speed dreams 20 menue 06](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/speed-dreams-20-menue-06.webp)  
**JeL**: *¿Qué clase de juegos tomais como referencia o inspiración para vuestros desarrollos? ¿Cuales son vuestros juegos favoritos?*



> 
> **MB**: Como dije antes, soy un gran fan del sim-racing, y veo rFactor2 como el más juego más completo en su categoría.
> 
> 
> **XB**: ¡Idem!
> 
> 
> 


 


**JeL**: *Si quereis podeis decir lo que querais sobre vuestro proyecto y vosotros mismo, o dejar un mensaje a nuestros lectores*



> 
>   
> **MB**: Happy gaming!
> 
> 
> **XB**: Todavía tengo un montón de ideas en mente para este proyecto, aunque están lejos de ser realizadas y quizás nunca lleguen a ocurrir. Pero cuando veo hoy la lista de tareas pendientes de TORCS, veo que las funciones implementadas en Speed-Dreams son las correctas, aunque nos faltan muchos participantes y algún día será necesario que el proyecto cambie de manos.....Y en la dirección de Madbad, ¡¡¡Buen juego a todos!!!  
>   
> 
> 
> 
> 


Y con esto terminamos la entrevista con dos de los integrantes del equipo de desarrollo de Speed Dreams. Espero que os haya parecido interesante, y ello os anime a probar el juego. El proyecto, como podeis ver en la entrevista necesita más manos, por lo que si sois desarrolladores y os apetece ayudar, seguro que recibiran vuestras contribuciones con los brazos abiertos. Por último nos gustaría **agradecer tanto a MadBad como a Xabier Bertaux el tiempo que nos han dedicado,** y que la espera hasta la siguiente versión del juego venga pronto y cargada de suculentas novedades. Quien sabe, quizás algún vernes noche nos encontremos online conduciendo por sus circuitos...


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/vYo5r4drlH8" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


¿Habeis jugado a Speed Dreams?¿Qué os parece el proyecto? Dinos lo que quieras en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

