---
author: Pato
category: Rol
date: 2018-03-01 18:27:54
excerpt: "<p>\xA1El sucesor del excelente Hammerwatch ya est\xE1 aqu\xED!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c3562f2e9521020de502f370ac3fbe48.webp
joomla_id: 667
joomla_url: heroes-of-hammerwatch-ya-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- rol
title: '''Heroes of Hammerwatch'' ya disponible en Linux/SteamOS'
---
¡El sucesor del excelente Hammerwatch ya está aquí!

Como ya estaba anunciado hace tiempo y nosotros [te lo habíamos contado](https://www.jugandoenlinux.com/index.php/homepage/generos/accion/item/630-heroes-of-hammerwatch-llegara-a-linux-steamos-a-principios-del-ano-que-viene), Heroes of Hammerwatch por fin ha llegado. Se trata de la secuela del excelente ['Hammerwatch'](http://store.steampowered.com/app/239070/Hammerwatch/) también disponible en Linux/SteamOS y que propone un juego de rol y acción al estilo de la vieja escuela y con gráficos "pixel art" en partidas que podrás afrontar solo o con hasta tres amigos en modo cooperativo, **ya sea en local o multijugador online**.


*"Heroes of Hammerwatch" [[web oficial](http://www.heroesofhammerwatch.com/)] es un juego de rol de acción y aventura de estilo "rogue-lite" dentro del mismo universo que Hammerwatch. Enfréntate a innumerables hordas de enemigos, trampas, puzzles, secretos y multitud de elementos, a la vez que te abres camino a través de niveles generados proceduralmente para alcanzar la cima de la "aguja abandonada".*


*Heroes of Hammerwatch tiene una progresión persistente, comenzando cada partida en "Outlook", una ciudad que puedes actualizar para mejorar tus héroes con diversas mejoras que te ayudarán a llegar más lejos en tu camino. Los mismos héroes también permanecerán persistentes, de modo que puedes llevar tus propios héroes a las partidas de tus amigos!"*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/uTIVDKdNvms" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Si te gustan los juegos desafiantes de este tipo o quieres saber más detalles, lo tienes ya disponible en su página de Steam, eso sí, no está traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/677120/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

