---
author: P_Vader
category: "V\xEDdeos JEL"
date: 2022-11-08 18:13:22
excerpt: "<p>\xBFSe puede jugar a Minetest en la Steam Deck?</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Minetest/minetest-deck-caratula.webp
joomla_id: 1507
joomla_url: minetest-steam-deck
layout: post
tags:
- exploracion
- construccion
- minetest
- steam-deck
title: Minetest - Steam Deck
---
¿Se puede jugar a Minetest en la Steam Deck?


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/78YZSnNMkMY" title="YouTube video player" width="720"></iframe></div>


 


Instalar Minetest en Steam Deck:


1. Cambia a modo escritorio en la steam deck (Apagar ➔ Cambiar modo escritorio)
2. Dentro del escritorio abre Discover ➔ busca e instala Minetest.
3. Ahora abre Steam en el escritorio ➔ en el menú "Games" ➔ "Add Non-Steam Game..." ➔ Selecciona Minetest en la lista de aplicaciones.
4. Ya te aparecerá el juego en la biblioteca de steam deck. Selecciona una plantilla de teclado para el mando. En el video puedes ver el ejemplo que hemos usado en el minuto 05:48.


 

