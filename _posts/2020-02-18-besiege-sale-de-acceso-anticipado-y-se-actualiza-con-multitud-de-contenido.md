---
author: Pato
category: "Simulaci\xF3n"
date: 2020-02-18 18:08:02
excerpt: "<p>El juego sandbox de simulaci\xF3n de f\xEDsicas de asedio de @spiderlinggames\
  \ alcanza la versi\xF3n 1.0</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Besiege/Besiege.webp
joomla_id: 1174
joomla_url: besiege-sale-de-acceso-anticipado-y-se-actualiza-con-multitud-de-contenido
layout: post
tags:
- indie
- sandbox
- construccion
- simulacion
- fisicas
title: Besiege sale de acceso anticipado y se actualiza con multitud de contenido
---
El juego sandbox de simulación de físicas de asedio de @spiderlinggames alcanza la versión 1.0


Buenas noticias para los que gustan de destruir y asediar aldeas, ya que **Besiege**  del estudio indie Spiderling Games ha salido de acceso anticipado alcanzando su **versión 1.0** y añadiendo nuevo contenido como Krolmar, la isla final de la campaña de un jugador, nueva categoría de bloques de automatización, y multitud de corrección de errores.


*Besiege es un juego de construcción basado en la física en el que creas máquinas de asedio medievales para sembrar el caos en fortalezas enormes y en apacibles aldeas. Construye una máquina capaz de destruir molinos de viento, aniquilar batallones de valientes soldados y transportar valiosos recursos, y defiéndela de cañones, arqueros o cualquier otra cosa que los desesperados enemigos tengan a su disposición. Crea un titán rodante o elévate torpemente hacia los cielos y provoca verdaderas carnicerías en entornos totalmente destructibles*.
 
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/27OvwrsOLL8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>




Si quieres saber más sobre esta actualización, puedes ver el [post del anuncio](https://steamcommunity.com/gid/103582791437259357/announcements/detail/1710741388954894185), visitar su [web oficial](http://www.besiege.spiderlinggames.co.uk/index.html)y si te interesa la destrucción tienes **Besiege** disponible [en su página de Steam](https://store.steampowered.com/app/346010/Besiege/) **con un 50% de descuento** por su lanzamiento.



 

