---
author: Serjor
category: Plataformas
date: 2018-06-13 21:26:25
excerpt: "<p>La cosa en las oficinas de la desarrolladora deber\xE1n estar que arden</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/10dd7f46ca2f22ce7ce9308c89d2507c.webp
joomla_id: 770
joomla_url: hot-lava-comienza-a-dar-senales-de-vida-en-steam
layout: post
tags:
- beta
- klei-entertaiment
- hot-lava
title: "Hot Lava comienza a dar se\xF1ales de vida en Steam"
---
La cosa en las oficinas de la desarrolladora deberán estar que arden

Vía twitter




> 
> New Game:  
> Hot Lava<https://t.co/WbiX6eXx23>
> 
> 
> — SteamDB Linux Update (@SteamDB_Linux) [13 de junio de 2018](https://twitter.com/SteamDB_Linux/status/1006993896101031936?ref_src=twsrc%5Etfw)



Leemos que Hot Lava, lo nuevo de Klei Entertaiment, los creadores entre otros del maravilloso Mark Of The Ninja, han publicado en Steam la beta pública, lo malo es que por el momento solamente está para windows, ya que como [podemos leer](https://steamcommunity.com/app/382560/discussions/0/1696049513760048391/#c1696049513785529231) de uno de sus desarrolladores, las versiones para GNU/Linux y Mac aún no están del todo listas, y prefieren centrarse en corregir problemas del juego en una versión (aunque tristemente para nosotros sea en windows), y cuando lo tengan pulido, corregir los errores específicos de cada plataforma.


Os recordamos que Hot Lava es un juego de parkour en primera persona donde deberemos recorrer el escenario sin tocar jamás el suelo, porque este es de lava.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/f6992PRpn4k" width="560"></iframe></div>


Y tú, ¿esperas con ganas a este Hot Lava? Cuéntanoslo en los comentarios, o en los canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

