---
author: leillo1975
category: Carreras
date: 2018-10-29 14:30:08
excerpt: <p>El juego de @ansdor ya se puede comprar en GOG.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/60095eb5824669bc8a1e99665c2e3a96.webp
joomla_id: 883
joomla_url: slipstream-actualizado-con-soporte-multijugador
layout: post
tags:
- multijugador
- retro
- slipstream
- ansdor
- pantalla-partida
- modos-de-juego
title: Slipstream actualizado con soporte multijugador (ACTUALIZADO)
---
El juego de @ansdor ya se puede comprar en GOG.

**ACTUALIZADO 13-2-19:** A día de hoy [ya podemos comprar en GOG](https://www.gog.com/game/slipstream) sin DRM, este fantástico juego que [analizamos hace algún tiempo](index.php/homepage/analisis/20-analisis/915-analisis-slipstream) . Aquí está el tweet con el que [**Ansdor**](https://www.ansdor.com/), su creador, nos avisaba de su disponibilidad en esta tienda:



> 
> Slipstream is now available on [@GOGcom](https://twitter.com/GOGcom?ref_src=twsrc%5Etfw), and it's 25% off there <https://t.co/JN5XQnJkmQ>
> 
> 
> — ansdor (@ansdor) [February 13, 2019](https://twitter.com/ansdor/status/1095698846531964930?ref_src=twsrc%5Etfw)


  





Desde JugandoEnLinux os recomendamos completamente este juego, ya que os lo vais a pasar en grande con él con su multitud de modos diferentes, su jugabilidad, sus mecánicas y su inconfundible estilo retro. Además ahora mismo tiene un precio realmente bueno , **6.19€ ahora mismo en GOG**.




---


**NOTICIA ORIGINAL:** Hace un par de días [os comentábamos](index.php/homepage/generos/carreras/2-carreras/1003-slipstream-pronto-tendra-multijugador-local) que el desarrollador de Slipstream [nos había respondido](https://twitter.com/ansdor/status/1055931209824505856) en twitter sobre la actualización pendiente de su juego anunciándonos que llegaría muy pronto, y fiel a su palabra, ayer mismo a última hora publicaba la **versión 1.1 de Sleepstream**, que añade importantes cambios para el disfrute de este gran juego. En un [comunicado](https://www.kickstarter.com/projects/noctet/slipstream/posts/2328213) que hacía en la página de Kickstarter de su proyecto nos daba numerosa información sobre este parche, que si teneis este título comprado seguro que ya se ha aplicado.


En él, entre otras muchas cosas, nos dice que **el parche añade el multijugador local (pantalla partida)** que nos permitirá poder disfrutar de Slipstream con hasta 4 amigos en nuestra pantalla, usando para ello nuestros mandos favoritos o el teclado de nuestro ordenador. También nos comenta que **multitud de bugs y arreglos han sido aplicados**, pero que no piensa añadir nuevas características al juego, por que necesita pasar página tras trabajar durante años en este juego y los objetivos que tenía para él ya los ha superado. Por supuesto seguirá otorgando soporte para los posibles bugs que pueda tener la actual versión, pero quiere ponerse a trabajar en otros proyectos para los que tiene muchas ideas.


Nos habla también de que **ha vendido hasta el momento un total de 5270 copias** de Slipstream y que con ello ha vendido ya más de la mitad de su objetivo, que eran 10000. De esas ventas, el 20% se produjeron los primeros días y el resto se enorgullece de que se produjesen sin grandes descuentos ni en bundles. También nos comenta que ha sido invitado al [GSQ 2019](https://gamesdonequick.com/), por lo que se siente muy orgulloso, ya que se declara seguidor de este evento desde hace tiempo y es para él un honor el poder presentar Slipstream en él.


![Monument](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SleepStream/Monument.webp)


En cuanto a la **actualización**, vamos a detallaros los [cambios que ha introducido](https://steamcommunity.com/games/732810/announcements/detail/1709572995858372929) en ella (Traducción Online):



> 
> *Slipstream 1.1 finalmente está aquí, y es una gran actualización.*
> 
> 
> ***MULTIJUGADOR LOCAL***  
> *El mayor cambio de todos, Slipstream ahora soporta multijugador local de hasta 4 jugadores. Hay cuatro modos de juego disponibles para el multijugador, la ya establecida carrera única y el gran premio, y...*
> 
> 
> ***NUEVO MODO DE JUEGO: CANNONBALL***  
> *Un nuevo modo de juego tanto para un jugador como para varios jugadores. Funciona como una mezcla de todos los otros modos: Puedes elegir una secuencia fija de hasta 15 pistas, y correr en ellas espalda con espalda, con o sin tráfico, rivales y/u otros corredores. Todo es personalizable en este modo. Desde una carrera en solitario por 15 circuitos hasta una carrera caótica con rivales y tráfico al mismo tiempo. Pero eso no es todo...*
> 
> 
> ***NUEVO MODO DE JUEGO: BATTLE ROYALE***  
> *Todos los videojuegos de 2018 necesitan un modo de "Battle Royale" y no podía dejar pasar la oportunidad. Es un modo simple: 16 corredores, 15 pistas al azar. En cada pista, el último corredor es eliminado, sólo uno puede llegar al final. Disponible tanto para un jugador como para varios jugadores.*
> 
> 
> ***NUEVO MODO DE JUEGO: TIME TRIAL***  
> *El último modo de juego nuevo es una contrarreloj clásica con cero aleatoriedad y repetición de fantasmas para los mejores momentos. Este es un modo exclusivo para un solo jugador.*
> 
> 
> ***NUEVA FÍSICA, MEJORES CONTROLES***  
> *La física del juego se estaba volviendo más y más extraña con cada versión, cada vez que intentaba arreglar algo, terminaba rompiendo algo más. Esta vez empecé de cero y creo que he solucionado todos los problemas más comunes, pero, como siempre, esta parte podría recibir pequeñas actualizaciones en un futuro próximo en caso de que aparecieran nuevos errores.*
> 
> 
> ***PERSONALIZACIÓN DE BANDAS SONORAS***  
> *Ahora puedes elegir tus propias canciones para jugar en el juego, junto con el OST. Sólo tienes que colocar tus MP3s adquiridos legalmente en la carpeta correspondiente\* y aparecerán automáticamente en el juego. OGG y WAV también son compatibles.*
> 
> 
> *Las carpetas lo son:*
> 
> 
> *En Windows: inicio usuario]/AppData/Roaming/ansdorGames/Slipstream/música*  
>  *En MacOS:[user home]/Biblioteca/Asistencia para aplicaciones/ansdorGames/Slipstream/música*  
>  *En Linux: Inicio usuario]/.config/ansdorGames/Slipstream/música*
> 
> 
> *El juego buscará archivos de música en todos los pliegues y subcarpetas dentro de ese. Además, los enlaces simbólicos funcionan en linux, pero no he probado en otros sistemas. La única limitación es que sólo se cargan las primeras 256 pistas personalizadas, el juego ignorará cualquier otra más allá de ese número.*
> 
> 
> ***ESQUEMA DE CONTROL SIMPLIFICADO***  
> *Algunas personas tienen problemas con la mecánica de derrape, así que decidí incluir un esquema de control simplificado opcional, multijugador exclusivo, en el que el derrape ocurre automáticamente y sólo tienes que guiar el coche por la pista. Esta función está pensada para jugadores novatos y escenarios de "party game", en los que algunas personas pueden no tener el tiempo (o estar demasiado borrachas) para aprender a derrapar correctamente. No lo incluí en el jugador individual porque siento que desplazaría a una de las mecánicas y desafíos más importantes del juego.*
> 
> 
> ***MEJORES OPCIONES GRÁFICAS***  
> *Ahora Slipstream tiene soporte 4K "verdadero" (bueno, tan verdadero como puede ser para un juego de pixel art). La configuración de la resolución/ventana en las versiones anteriores era un poco confusa, así que intenté simplificar todo. Ahora puede elegir un tamaño de ventana y una resolución interna, y ambos llegan hasta 3840x2160. Las resoluciones más bajas proporcionan una ligera mejora en el rendimiento, pero no mucho. El ajuste "auto" se recomienda en todos los casos, ya que ajustará la resolución para que coincida con el tamaño de la ventana/pantalla.*
> 
> 
> ***OTROS PEQUEÑOS CAMBIOS***  
> *- controlador de vibración ahora soportado*  
> *- el infame fallo de audio, en el que todo el sonido deja de sonar después de un tiempo, se corrige*   
> *- los circuitos en espejo están ahora disponibles en modo de carrera individual*  
> *- pantallas post-carrera completamente renovadas*  
> *- multitud de pequeños cambios gráficos y correcciones de errores en la lista*
> 
> 
> *Lamentablemente, esta será la última gran actualización de contenido para este juego. Soy un desarrollador solitario y ya no tengo el tiempo ni los recursos para seguir añadiendo contenido y proporcionando soporte activo para este juego. He estado trabajando exclusivamente en Slipstream durante los últimos 3 años más o menos, estoy muy cansado de ello, y hay muchos otros juegos que quiero hacer, así que es hora de pasar a nuevos proyectos.*
> 
> 
> *Considero que esta es la versión "gold" de Slipstream, tiene todas las características planeadas y algunas más, y ya está completa. Sin embargo, a medida que pase el tiempo, seguirá recibiendo pequeñas actualizaciones de back-end y correcciones de errores, pero eso será todo.*
> 
> 
> *Espero que disfruten el juego, y gracias por el apoyo, ¡son increíbles!*
> 
> 
> 


Como veis las actualizaciones no son moco de pavo y bien merecen que nos pongamos, una vez más, a disfrutar de este mágnífico título. Si no lo teneis aun, con esta enorme actualización, pienso que teneis más razones, aun si cabe, para comprarlo. Además se trata de un juego que tiene un precio realmente bajo (8,19€, aunque ahora mismo **está de oferta por tan solo 6.96€**) para la cantidad de diversión y calidad que ofrece, y es justo no, justísimo, recompensar a su creador por su esfuerzo. Podeis comprar Slipstream en [Steam](https://store.steampowered.com/app/732810/Slipstream/).


 En JugandoEnLinux estamos deseando tener pronto noticias sobre su próximo trabajo y poder ofreceroslas lo antes posible. Mientras tanto podeis disfrutar del Trailer de Lanzamiento en este video:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/2955JiYgcQQ" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>

