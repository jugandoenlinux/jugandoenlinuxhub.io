---
author: Pato
category: Terror
date: 2017-08-17 20:50:00
excerpt: "<p>El juego de Acid Wizard Studio llevaba mas de 4 a\xF1os de desarrollo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7c8a032eb1fd580d4ed0e1065f1e3688.webp
joomla_id: 435
joomla_url: darkwood-por-fin-sale-de-acceso-anticipado-y-llega-oficialmente-a-linux-hoy-dia-de-lanzamiento
layout: post
tags:
- indie
- rol
- terror
- supervivencia
- vista-cenital
title: "'Darkwood' por fin sale de acceso anticipado y llega oficialmente a Linux\
  \ hoy, d\xEDa de lanzamiento"
---
El juego de Acid Wizard Studio llevaba mas de 4 años de desarrollo

'Darkwood' es uno de esos casos en los que el tiempo de desarrollo se alarga y alarga quizás más de lo deseable. Personalmente le llevo siguiendo la pista desde hace bastante tiempo, quizás desde poco después de que se anunciase su desarrollo en Linux o en su campaña en Greenlight, no lo recuerdo, pero lo cierto es que después de algo más de 4 años de desarrollo por fin lo han lanzado oficialmente en Linux hoy, día de lanzamiento. Lo que más me llamó la atención de Darkwood [[web oficial](http://www.darkwoodgame.com/)] fue el original acercamiento al género de terror con un juego en vista cenital que a priori no parece el mejor planteamiento, y que sin embargo los chicos de Acid Wizard Studio han sabido desarrollar con acierto una jugabilidad que llega a ser brillante. Las situaciones de máxima tensión y los sustos estarán siempre acechando tu pantalla.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/3S3tmWfFACQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


**Sinopsis de Steam:**



> 
> *Darkwood ofrece una nueva perspectiva en los juegos de terror y supervivencia. Explora un mundo rico y cambiante, donde podrás vagar libremente durante el día. Después cobíjate en tu escondite y reza para ver la luz del día.*
> 
> 
> * *Terror y spervivencia en vista cenital con una jugabilidad terrorífica.*
> * *Por el día explora el siniestro bosque generado aleatoriamente, explora para conseguir materiales, encuentra armas y descubre nuevos secretos.*
> * *Por la noche encuentra escondites, haz barricadas, pon trampas y escóndete o defiéndete de los horrores que se esconden en la oscuridad.*
> * *Obtén habilidades y ventajas extrayendo una esencia extraña de la flora y fauna mutante e inyectándotela en tu torrente sanguíneo. Cuidado con las consecuencias inesperadas.*
> * *Toma decisiones que tendrán impacto en el mundo de Darkwood, sus habitantes y la historia que experimentas.*
> * *Conoce a personajes misteriosos, aprende sus historias y decide su destino. Y recuerda - no confíes en nadie.*
> * *A medida que pasan las noches, las líneas entre la realidad y las fantasías de pesadilla empiezan a difuminarse. ¿Estás listo para entrar en Darkwood?*
> 
> 
> 


'Darkwood' no está disponible en español, pero si te gustan los juegos de terror, te atrae el planteamiento y la traducción no es problema para ti, lo tienes disponible en su página de Steam con un 10% de descuento por su lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/274520/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Yo por mi parte le daré una oportunidad próximamente.


¿Te gustan los juegos en vista cenital? ¿Te atrae el planteamiento de Darkwood? ¿Te adentrarás en el bosque?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

