---
author: leillo1975
category: "Acci\xF3n"
date: 2019-11-11 12:22:56
excerpt: "Este conocido juego libre se ha publicado finalmente en Steam"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/07758ee08f7e16a0b15b0d98a56d204a.webp
joomla_id: 1130
joomla_url: red-eclipse-2-sera-lanzado-con-un-nuevo-motor-grafico
layout: post
tags:
- red-eclipse
- cube-engine-2
- shooter
- open-souce
- tesseract
title: "\"Red Eclipse 2\" ha sido lanzado con un nuevo motor gr\xE1fico (ACTUALIZADO)"
---
Este conocido juego libre se ha publicado finalmente en Steam


**ACTUALIZACIÓN 20-12-19**:@franciscot, un miembro de nuestra comunidad nos acaba de avisar ya está disponible para descargar en la [web oficial de Red Eclipse](https://www.redeclipse.net/) la versión 2.0 del juego, para todos aquellos que no deseeis usar Steam, por lo que teneis una alternativa más aparte del [código fuente](https://github.com/redeclipse/base). La pagina para descargar esta **version 2.0 Jupiter Edition** podeis encontrarla en el siguiente link:


<https://www.redeclipse.net/download>


<div class="resp-iframe"><iframe height="450" src="https://steamcdn-a.akamaihd.net/steam/apps/256768722/movie_max.webm" style="display: block; margin-left: auto; margin-right: auto;" type="video/webm" width="800"> </iframe></div>




---


**ACTUALIZACIÓN 19-12-19**: Acaba de producirse el lanzamiento de **Red Eclipse 2** en Steam, por lo que a partir de ahora tendremos, además de la versión independiente del cliente de Valve, una nueva forma de instalarlo y mantenerlo actualizado de una forma más sencilla, además de aumentar su visibilidad en el "mercado", ya que la intención del equipo que está detrás de su desarrollo es que llegue a más gente. Desde [ya podeis descargar gratuitamente Red Eclipse desde Steam](https://store.steampowered.com/app/967460/Red_Eclipse_2/). Os dejamos con un video ddel juego en sus últimos estados de desarrollo:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/KtzMsUXEPCo" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Como muchos sabreis, mañana día 20 de Diciembre, jugaremos una de nuestras "Partidas del Viernes", para celebrar la salida de Red Eclipse 2. Si quieres unirte a esta partida solo tienes que preguntar como en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux). Si tan solo quieres ver como jugamos, emitiremos en directo la partida a través de nuestro canal de "[Twitch](https://www.twitch.tv/jugandoenlinux)".

---


**Noticia Original:**


Sorpresón el que nos hemos llevado hace un momento al leer la noticia en [GamingOnLinux](https://www.gamingonlinux.com/articles/red-eclipse-2-is-a-revamp-of-the-classic-free-arena-shooter-coming-to-steam.15389), y es que [Red Eclipse](https://www.redeclipse.net/) se actualizará proximamente a la [versión v2](https://www.redeclipse.net/docs/Information-for-v2), que tendrá como principal característica la **inclusión de un motor gráfico nuevo**. Red Eclipse, hasta la [versión 1.6](index.php/homepage/generos/accion/5-accion/736-red-eclipse-un-shooter-libre-y-con-toques-de-parkour), lanzada hace casi dos años, usaba el **motor Cube2** (Sauerbraten). Para la versión 2 han decidido portar el juego a [Tesseract](http://tesseract.gg/), una derivación de Cube2 mucho más avanzada que permite *técnicas modernas de renderizado mejoradas, tales como sombras omnidireccionales totalmente dinámicas, iluminación global, iluminación HDR, sombreado diferido, antialiasing morfológico/temporal/multimuestreo, y mucho más.*


Este cambio de motor implicará que **se aprovecharán mejor las capacidades del nuevo hardware**, aunque sus desarrolladores advierten que por desgracia **el juego necesitará más recursos para poder jugarse y no podrá “correr en una patata”**. De esta forma el juego lucirá mucho mejor para los tiempos que corren y **será un juego mucho más atractivo para el gran público**. Además del cambio de motor gráfico también se han rediseñado los menues e interfaz del juego, se están portando los mapas actuales y creando mapas nuevos exclusivos para nuevo motor, entre otras muchas cosas que podeis consultar [aquí](https://www.redeclipse.net/docs/Information-for-v2).


Por si esto fuese poco, **el juego será lanzado en Steam** de forma completamente gratuita a partir del **19 de Diciembre**, por lo que estará mucho más accesible a mucha gente que hasta ahora no lo conocía. Por supuesto el juego seguirá siendo multiplataforma, permitiéndonos competir con jugadores de otros sistemas operativos como Windows o Mac.


Para quien no conoza este juego, hay que decir que es un **shooter Open Source de factura clásica**, que se caracteriza por su **velocidad**, él tener una **gran cantidad de modos de juego** y la posibilidad de realizar **Parkour**, pudiendo utilizar las paredes para impulsarnos o realizar complejos movimientos. Como se se suele decir, vale más una imagen que mil palabras, y por eso os dejamos con el video que grabamos en “La partida del Viernes” de hace algún tiempo jugando a la versión 1.6 del  juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/5CCPTm6U4Jo" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Desde JugandoEnLinux.com os recordaremos la salida de Red Eclipse 2 en la la fecha de su lanzamiento y os proponemos que juguemos una partida para celebrarlo y probar entre todos estas novedades. ¿Te gustan los shooters clásicos? ¿Que te parece este proycto de software libre? Cuéntanoslo en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

