---
author: leillo1975
category: Rol
date: 2019-09-23 14:34:30
excerpt: <p>El estudio ruso AtomTeam ( <span class="username u-dir" dir="ltr">@the_atomgame
  </span>) nos trae esta importante caracteristica.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2e8bf6f45fb2b01f7bc63e367c45fa81.webp
joomla_id: 1113
joomla_url: atom-rpg-se-actualiza-incluyendo-la-perspectiva-isometrica
layout: post
tags:
- rol
- atom-rpg
- postapocaliptico
- unity
- atom-team
title: "\"ATOM RPG\" se actualiza incluyendo la perspectiva isom\xE9trica."
---
El estudio ruso AtomTeam ( @the_atomgame ) nos trae esta importante caracteristica.

Una de las sorpresas recientes dentro de lo que es el panorama de los juegos de Rol más clásicos, la dió recientemente el estudio ruso [Atom Team](https://atomrpg.com/), que hace cosa de casi dos años **lanzaba en Early Access un fantástico juego diseñado con el motor Unity,** y que tenía claras referencias a algunos de los juegos postapocalipticos más famosos, como las saga **Wasteland** o la archiconocida **Fallout**.


El proyecto, que arrancaba gracias a la financiación conseguida en [Kickstarter](https://www.kickstarter.com/projects/atomrpg/atom-rpg), nos transportaba a una **distópica Unión Sovietica**, donde todo está destruido después de una guerra nuclear ocurrida en el año 1986, al enfrentarse los dos bloques de la guerra fría. Como **superviviente del holocausto nuclear** deberás recorrer el yermo, con los peligros y oportunidades que eso representa. 


A lo largo del periodo de Acceso Anticipado, e incluso después de haber lanzado la versión 1.0, con la que [el juego se lanzaba oficialmente](index.php/homepage/generos/rol/7-rol/677-atom-rpg-post-apocalyptic-indie-game-llega-en-early-access), el equipo de de desarrollo no ha parado de trabajar este fantástico proyecto con **añadidos y mejoras constantes** como la inclusión de la "Dead City", el soporte de Mandos, actualizaciones de calidad, mejoras en la traducción inglesa, y por supuesto montones de bugs corregidos.


En esta ocasión, ATOM Team, nos trae la **perspectiva isométrica**, que seguramente hará las delicias de muchos fans de este tipo de juegos. Este forma parte del trabajo realizado para **Trudograd**,  la próxima DLC independiente que se lanzará en breve y que contará con más de 20 nuevos personajes, más de 10 nuevas misiones, nuevas mecánicas.... Habrá que estar muy atento. Entre otras cosas, en esta actualización también encontraremos:


*-El modo isométrico ahora está disponible en la configuración del juego;*  
 *-Se ha añadido la función "Pausa cuando el juego se minimiza" como una opción que puede activarse y desactivarse;*  
 *-Añadida la opción "clean log on load";*  
 *-Las teclas Left alt, shift y ctrl funcionan ahora como las de la derecha;*  
 *-Nuevo Logro de Steam;*  
 *-Añadidos varios sonidos nuevos al juego y sonidos de interfaz de usuario reacondicionados;*  
 *-Nueva IA para el monstruo del Shog;*  
 *-La mirilla se resalta cuando apunta a un objeto interactivo;*  
 *-La información del enemigo se muestra en el modo de pedido de miembros del grupo;  
-Daño cuerpo a cuerpo adicional causado a los enemigos caídos;  
-Viaje más rápido en el mapa global (la velocidad depende de la habilidad de supervivencia);  
-Los osos ahora pueden pararse sobre las patas traseras a veces, también se mejoró su IA;  
-Al verificar la evitación de encuentros, las bonificaciones de ítems y rasgos se consideran correctamente;  
-La bonificación del kit de inicio ahora se calcula correctamente;  
-Ducharse en Red Fighter curó 300 intoxicaciones por radiación en lugar de 50;  
-Caballero de cuatro patas y pretoriano son fijos;  
-Los jugadores de distinción tecnófoba no obtienen recetas al comienzo, incluso con INT grande;  
-Se corrigieron los bonos de distinción de Cazador salvaje;  
-Se corrigió el equilibrio de distinción de Circus Education;  
-Escudo salvaje ahora agregado a recetas básicas;  
-Los efectos de café, chifir y hierbas ahora duran más;  
-Se arregló la búsqueda del bandido herido;  
-Se corrigió el bloqueo raro del juego que ocurría cuando el jugador se movía a una nueva ubicación usando el traje de materiales peligrosos;  
-Las ubicaciones abiertas en el mapa global se destacan más;  
-Se agregó un marcador que señala dónde está parado el personaje cuando no está en la pantalla;  
-Se agregaron estadísticas de logros al juego;  
-Se agregaron mejores modelos animales no mutantes;  
-Ahora puedes pausar y cargar durante el turno enemigo;  
-Se corrigió el error del conducto de ventilación en el búnker de Dead City;  
-Nuevo y mejorado sistema de muñeca de trapo finalizado;  
-Se corrigió el error visual de Skybox en ciertas cinemáticas;  
-Las zonas de salida en encuentros aleatorios de metro son fijas;  
-Se corrigió un error exclusivo de Linux con símbolos aleatorios que aparecían en biografías de personajes;  
-Nuevas animaciones agregadas;  
-Se corrigió el equilibrio del encuentro Dzhulbars;  
-Descripciones estadísticas únicas para Dzhulbars;  
-Cocinar en una fogata o estufa ahora beneficia a toda la fiesta;  
-Sprites mejorados en el mapa global;  
-Todos los animales del juego tienen frases nuevas cuando se les habla después de consumir una poción;  
-Dual Shock 4 Wireless ahora es compatible;  
-Se corrigió una gran cantidad de errores menores, errores tipográficos y problemas de traducción.*


Ya veis que a esta gente les gusta trabajar, y nosotros como jugadores se lo agradecemos. A ver si en un futuro añaden la prometida tradución al Castellano y podemos disfrutar de esta joya de una forma mucho más sencilla para los hispanohablantes. Podeis comprar ATOM RPG en [Steam](https://store.steampowered.com/app/552620/ATOM_RPG_Postapocalyptic_indie_game/) y [GOG.com](https://www.gog.com/game/atom_rpg_postapocalyptic_indie_game).


¿Qué os parecen este tipo de juegos de Rol? ¿Os va el tema postapocaliptico? ¿Habeis jugado ya a ATOM RPG?. Podeis dejar vuestras impresiones  sobre esta gran juego en los comentarios de esta noticia,  o vuestros mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


