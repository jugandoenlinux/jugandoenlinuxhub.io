---
author: Pato
category: Apt-Get Update
date: 2017-02-24 18:23:04
excerpt: "<p>Llegamos otro Viernes m\xE1s al Update de la semana</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7ced67c0d648122bcae10129de981341.webp
joomla_id: 231
joomla_url: apt-get-update-dungeon-3-albion-online-solstice-cronicles-mia-the-exiled-railway-empire
layout: post
title: 'Apt-Get Update Dungeon 3 & Albion Online & Solstice Cronicles: MIA & The Exiled
  & Railway Empire...'
---
Llegamos otro Viernes más al Update de la semana

Otra semana se esfuma, y volvemos a repasar lo que no hemos podido publicar... empezamos:


Desde [www.gamingonlinux.com](http://www.gamingonlinux.com)


- Wine 2.2 ya está disponible con mejoras en las instrucciones Shader Model 5 y Direct 3D. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/wine-22-released-with-even-more-shader-model-5-instructions-and-work-towards-direct3d-command-stream.9141).


- Recounter es un RPG de tipo "Roguelike" con una pinta interesante y ya está disponible para Linux. Liam le da un pequeño repaso [en este enlace](https://www.gamingonlinux.com/articles/rencounter-a-turn-based-roguelike-rpg-adds-linux-support-a-quick-look.9147).


- 'I am the Hero', un juego de tipo "Beat 'Em Up" ya está disponible para Linux. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/i-am-the-hero-a-surprisingly-great-beat-em-up-has-linux-support.9148).


- 'Dungeon 3' de Kalipso Media y Realmforge tendrá versión para Linux. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/i-am-the-hero-a-surprisingly-great-beat-em-up-has-linux-support.9148).


- 'Albion Online', el MMO con soporte en Linux dejará de estar en Beta y se lanzará oficialmente el día 17 de Julio. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/the-mmo-albion-online-finally-has-a-release-date-new-huge-update-in-march.9165).


- 'Solstice Cronicles: MIA', un juego de Aliens y disparos en vista isométrica y una pinta absolutamente fantástica tendrá soporte en Linux. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/solstice-chronicles-mia-an-incredible-looking-top-down-alien-shooter-will-support-linux.9167).


- Serious Sam VR: The first encounter' de Croteam ya está disponible en Linux. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/serious-sam-vr-the-first-encounter-is-now-officially-on-linux.9169).


Desde [www.linuxgameconsortium.com:](http://www.linuxgameconsortium.com)


- 'Orbiz' es un juego "Roguelike" multijugador ya disponible en Linux en acceso anticipado. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/orbiz-roguelike-multiplayer-now-available-early-access-linux-mac-pc-48148/).


- 'Night in the Woods' ya está disponible para Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/night-in-the-woods-story-adventure-launches-linux-mac-pc-games-48266/).


- 'Wartile' una mezcla de juego de estratégia en tiempo real y tablero llega a Linux en acceso anticipado. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/wartile-tabletop-rts-coming-early-access-linux-games-48332/).


- 'The Exiled' un juego de Rol Masivo que tendrá cuotas para jugar llega en acceso anticipado y es gratis por 7 días. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/exiled-mmo-rpg-launches-early-access-games-linux-mac-pc-free-7-days-48350/).


- 'Railway Empire' Un juego de gestión de líneas ferroviarias y ferrocarriles se anuncia para final de año con versión para Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/railway-empire-management-simulator-announced-linux-pc-games-48365/).


 


Vamos con los vídeos de la semana:


Solstice Cronicles: MIA


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/Q3g1BADBjAk" width="640"></iframe></div>


 Dungeon 3


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/Yh2aRaWh5dI" width="640"></iframe></div>


 I Am The Hero


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/2mTTd1uXD7Y" width="640"></iframe></div>


 Railway Empire


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/vXgooLKxXaA" width="640"></iframe></div>


 The Exiled


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/STmKFoFV_Do" width="640"></iframe></div>


 


Esto es todo por este Apt-Get Update. ¿Que te ha parecido?. Seguro que me he dejado muchas cosas en el tintero.


Puedes contármelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD). 

