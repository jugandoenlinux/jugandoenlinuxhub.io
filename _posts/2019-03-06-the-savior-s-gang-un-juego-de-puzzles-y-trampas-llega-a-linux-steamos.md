---
author: Pato
category: Aventuras
date: 2019-03-06 15:11:45
excerpt: "<p>Es obra de @CatnessGames autores de HIVE: Altenum Wars que tambi\xE9\
  n llegar\xE1 a Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/786f856dfc58fd9ed7b812d2cbc13c76.webp
joomla_id: 985
joomla_url: the-savior-s-gang-un-juego-de-puzzles-y-trampas-llega-a-linux-steamos
layout: post
tags:
- accion
- indie
- aventura
- 2-5d
- violento
title: The Savior's Gang, un juego de puzzles y trampas llega a Linux/SteamOS
---
Es obra de @CatnessGames autores de HIVE: Altenum Wars que también llegará a Linux

Nos hacemos eco de un lanzamiento que nos pasó desapercibido la semana pasada. '**The Savior's Gang**' es un juego desarrollado por el estudio castellonense **Catness Games** donde tendremos que tomar el papel de "El Salvador", una entidad poderosa que es crucificada y queda atrapada entre el cielo y la tierra. Esta injusta muerte provoca la ira de su padre, una todopoderosa paloma, que desata una serie de trampas, desastres y peligros por todo el mundo. El Salvador deberá negociar con su Padre para detener estas catástrofes y salvar solo a sus fieles de todas estas maldades y de paso, tratar de salir del limbo y recuperar su poder. Para ello, contará con su buen amigo Stan y la intermediación de su Madre y Padrastro.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/MukZ0Lyse94" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


En 'The Savior's Gang' controlas a un grupo de fieles con los que tendrás que superar multitud de trampas para alcanzar la salvación. En cada nivel, el número de adeptos podrá ir en aumento, dificultando su control y disminuyendo las probabilidades de que todos sobrevivan. Estos fieles pueden fallecer a causa de desmembramientos, aplastados, quemados, ahogados e infectados entre muchas otras calamidades. Tu habilidad entrará en juego para determinar cuántos adeptos terminan cada nivel con vida.


**Caracteristicas:**


* 17 Niveles basados en varias localizaciones.
* Mas de 20 maneras diferentes de morir.
* Niveles desafiantes y realmente difíciles.
* Realismo en físicas, muertes y estilo artístico
* Historia intensa, gamberra y divertida


The Savior's Gang está desarrollado con Unreal Engine 4, y como requisitos mínimos solo pide:


* **SO:** Ubuntu 14.04 o superior
* **Procesador:** Intel i3
* **Memoria:** 1 GB de RAM
* **Gráficos:** Intel HD 3000
* **Almacenamiento:** 3 GB de espacio disponible


Si quieres saber mas sobre 'The Savior's Gang' puedes visitar su página web o su [página de Steam](https://store.steampowered.com/app/938480/The_Saviors_Gang/) donde está rebajado un 20% por su lanzamiento quedando en el ridículo precio de 3,19€.


#### ¿Y que hay de HIVE: Altenum Wars?


Como ya [publicamos anteriormente](index.php/homepage/generos/accion/5-accion/288-hive-altenum-wars-un-shooter-competitivo-2-5d-llegara-el-24-de-marzo), Catness Games es el estudio que está tras el desarrollo del "shooter" 2,5D **HIVE: Altenum Wars**, un juego competitivo multijugador online en el que los jugadores tienen que luchar en un entorno hexagonal cambiante eligiendo un héroe con características propias. En Jugando en Linux hemos podido saber que **el estudio sigue desarrollando una versión para Linux**, y según parece llegará en alguna fecha aún por determinar en los próximos meses.


¿Qué te parece 'The Savior's Gang'? ¿Sigues esperando la llegada de 'Hive:Altenum Wars' a nuestro sistema favorito?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux)


Os dejo un vídeo "gameplay" de los 5 primeros minutos de 'The Savior's Gang.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/KKpBu3qJUMM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>

