---
author: leillo1975
category: Software
date: 2018-06-18 08:01:31
excerpt: "<p>Hace unos d\xEDas os habl\xE1bamos de <a href=\"index.php/homepage/generos/carreras/item/881-trackmania-nations-forever-esta-disponible-gratis-como-snap-desde-snapcraft\"\
  \ target=\"_blank\" rel=\"noopener\">Trackmania Nations Forever</a>, un juego arcade\
  \ de carreras online para Windows que nos llegaba empaquetado en un Snap junto con\
  \ Wine para poder instalarlo en nuestros sistemas sin m\xE1s complicaciones que\
  \ la de instalar un archivo de estas caracter\xEDsticas. En esta ocasi\xF3n os vamos\
  \ a hablar de <strong>Winepak</strong>, que es basicamente lo mismo, pero usando\
  \ Flatpak en vez de Snap.</p>\r\n<p>De todos es conocida la potencia del bendito\
  \ Wine, y creo que no hace falta hablar mucho de sus bondades. En los \xFAltimos\
  \ a\xF1os la compatibilidad de esta implementaci\xF3n que permite ejecutar apliaciones\
  \ creadas para Windows en nuestros sistemas GNU-Linux ha crecido exponecialmente,\
  \ llegando incluso a emular DirectX10 y 11 con buenos resultados en bastantes juegos;\
  \ pero cierto es que para conseguirlo a veces es un verdadero calvario para los\
  \ usuarios menos avezados (entre los que me incluyo) . Loables son los intentos\
  \ de PlayOnLinux o Lutris de automatizar estas tareas creando instaladores, pero\
  \ aun as\xED, en muchas ocasiones pueden llegar&nbsp; a ser un problema debido a\
  \ la cantidad de opciones y configuraciones que presentan.</p>\r\n<p>Debido a esto,\
  \ han nacido iniciativas como las que estamos habando, y si antes tratamos el caso\
  \ de Trackman\xEDa Nations, ahora le toca a Winepak. Ya van varios d\xEDas desde\
  \ que se han empezado a ver art\xEDculos por todos lados sobre este, pero no ha\
  \ sido hasta hace un momento cuando hemos encontrado en <a href=\"https://ubunlog.com/winepak-flatpak-aplicaciones-windows/\"\
  \ target=\"_blank\" rel=\"noopener\">Ubunlog</a> uno donde se explica todo con pelos\
  \ y se\xF1ales. En JugandoEnLinux <strong>nos vamos a basar en \xE9l</strong> para\
  \ ofreceros de una manera sencilla la manera de acceder a este repositorio. <strong>Todo\
  \ lo que vamos a necesitar es una distribuci\xF3n que nos permita usar Flatpak</strong>\
  \ y por supuesto cumplir m\xE1s o menos los requisitos t\xE9cnicos de la aplicaci\xF3\
  n que vayamos a ejecutar.</p>\r\n<p>En <a href=\"https://winepak.org/\" target=\"\
  _blank\" rel=\"noopener\">Winepak</a> encontraremos algunos juegos y apliaciones\
  \ empaquetados con todo lo necesario para funcionar a la primera, sin m\xE1s complicaciones\
  \ ni problemas. Por ahora la lista de programas que encontraremos es recortada,\
  \ pero se espera que en un futuro, poco a poco vayamos encontrando m\xE1s y m\xE1\
  s software que disfrutar. La lista de aplicaciones y juegos no es f\xE1cil de encontrar,\
  \ pero en la <a href=\"https://github.com/winepak/applications\" target=\"_blank\"\
  \ rel=\"noopener\">p\xE1gina del proyecto en Github</a> podremos encontrar m\xE1\
  s informaci\xF3n.</p>\r\n<p>Como es l\xF3gico, si no lo hemos hecho aun, para empezar\
  \ debemos habilitar el uso de Flatpak en nuestro sistema, y para ello podremos encontrar\
  \ las instrucciones de como hacerlo en la <a href=\"https://flatpak.org/setup/\"\
  \ target=\"_blank\" rel=\"noopener\">p\xE1gina oficial</a>. Despu\xE9s <strong>debemos\
  \ instalar los repositorios de Flathub y Winepak</strong>. Para ello ejecutaremos\
  \ los siguientes comandos en una consola:</p>\r\n<blockquote>\r\n<p><code>flatpak\
  \ remote-add --if-not-exists flathub <a href=\"https://dl.flathub.org/repo/flathub.flatpakrepo\"\
  >https://dl.flathub.org/repo/flathub.flatpakrepo</a></code></p>\r\n<p><code>flatpak\
  \ remote-add --if-not-exists winepak <a href=\"https://dl.winepak.org/repo/winepak.flatpakrepo\"\
  >https://dl.winepak.org/repo/winepak.flatpakrepo</a></code></p>\r\n</blockquote>\r\
  \n<p>Desde este momento podemos listar los programas y juegos disponibles en Winepak\
  \ escribiendo lo siguiente en la terminal:</p>\r\n<blockquote>\r\n<p><code>flatpak\
  \ remote-ls winepak</code></p>\r\n</blockquote>\r\n<p><strong>En este momento</strong>\
  \ podremos encontrar las que vemos en la siguiente captura:</p>\r\n<p><img class=\"\
  img-responsive\" style=\"display: block; margin-left: auto; margin-right: auto;\"\
  \ src=\"images/Articulos/Winepak/ListaWinepak.jpg\" alt=\"ListaWinepak\" /></p>\r\
  \n<p>Como veis hay \"cosillas\" bastante interesantes como \"World of Tanks\", \"\
  Starcraft 2\", \"Path of Exile\", \"League of Legends\".... Para instalar cualquiera\
  \ de estas apliaciones, por ejemplo el emulador de WiiU \"Cemu\", simplemente teclearemos\
  \ lo siguiente:</p>\r\n<blockquote>\r\n<p><code>flatpak install winepak info.cemu.Cemu</code></p>\r\
  \n</blockquote>\r\n<p>Como veis primero tecleais \"flatpak install\", despu\xE9\
  s le dec\xEDs al programa que use el repositorio \"winepak\" y por \xFAltimo el\
  \ nombre del paquete flatpak, \"info.cemu.Cemu\". <code></code>Todo muy sencillo...\
  \ Es importante decir que las apliaciones contenidas en este repositorio son simplemente\
  \ enlaces de descarga a aplicaciones de libre distribuci\xF3n empaquetadas conjuntamente\
  \ con todo lo necesario para ejecutarse.</p>\r\n<p>Desviandonos un poco del tema,\
  \ hablar de Wine como siempre genera controversia, incluso dentro de nuestra redacci\xF3\
  n hay divisi\xF3n. Para mucha gente se trata de un software maravilloso e imprescindible,\
  \ d\xE1ndo la oportunidad de disfrutar de aplicaciones y juegos que de otra manera\
  \ les estar\xEDan vedados; pero para otras personas es simplemente mandar un mensaje\
  \ equvocado a las desarrolladoras de software, ya que estas pueden entender que\
  \ no es necesario portar su software, ya que la comunidad se busca la vida para\
  \ poder usarlo. Sea cual sea vuestro parecer, aqu\xED en JugandoEnLinux entendemos\
  \ las dos posturas, y en vosotros, como siempre est\xE1 la decisi\xF3n de sacarle\
  \ provecho o no; pero creemos que es importante que la gente conozca todas las opciones\
  \ de las que dispone.</p>\r\n<p>\xBFHabeis usado ya Winepak? \xBFQu\xE9 opini\xF3\
  n os merece el uso de Wine en Gnu-Linux? D\xE9janos tu opini\xF3n en los comentarios\
  \ o en nuestros canales de <a href=\"https://t.me/jugandoenlinux\" target=\"_blank\"\
  \ rel=\"noopener\">Telegram</a> y <a href=\"https://discord.gg/fgQubVY\" target=\"\
  _blank\" rel=\"noopener\">Discord</a>.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9ee28f01f100eeeb1372f6866ddd0d3a.webp
joomla_id: 772
joomla_url: instala-juegos-de-windows-facilmente-gracias-a-winepak
layout: post
tags:
- wine
- windows
- flatpak
- winepack
title: Instala juegos de Windows facilmente gracias a "WinePak"
---
Hace unos días os hablábamos de [Trackmania Nations Forever](index.php/homepage/generos/carreras/item/881-trackmania-nations-forever-esta-disponible-gratis-como-snap-desde-snapcraft), un juego arcade de carreras online para Windows que nos llegaba empaquetado en un Snap junto con Wine para poder instalarlo en nuestros sistemas sin más complicaciones que la de instalar un archivo de estas características. En esta ocasión os vamos a hablar de **Winepak**, que es basicamente lo mismo, pero usando Flatpak en vez de Snap.


De todos es conocida la potencia del bendito Wine, y creo que no hace falta hablar mucho de sus bondades. En los últimos años la compatibilidad de esta implementación que permite ejecutar apliaciones creadas para Windows en nuestros sistemas GNU-Linux ha crecido exponecialmente, llegando incluso a emular DirectX10 y 11 con buenos resultados en bastantes juegos; pero cierto es que para conseguirlo a veces es un verdadero calvario para los usuarios menos avezados (entre los que me incluyo) . Loables son los intentos de PlayOnLinux o Lutris de automatizar estas tareas creando instaladores, pero aun así, en muchas ocasiones pueden llegar  a ser un problema debido a la cantidad de opciones y configuraciones que presentan.


Debido a esto, han nacido iniciativas como las que estamos habando, y si antes tratamos el caso de Trackmanía Nations, ahora le toca a Winepak. Ya van varios días desde que se han empezado a ver artículos por todos lados sobre este, pero no ha sido hasta hace un momento cuando hemos encontrado en [Ubunlog](https://ubunlog.com/winepak-flatpak-aplicaciones-windows/) uno donde se explica todo con pelos y señales. En JugandoEnLinux **nos vamos a basar en él** para ofreceros de una manera sencilla la manera de acceder a este repositorio. **Todo lo que vamos a necesitar es una distribución que nos permita usar Flatpak** y por supuesto cumplir más o menos los requisitos técnicos de la aplicación que vayamos a ejecutar.


En [Winepak](https://winepak.org/) encontraremos algunos juegos y apliaciones empaquetados con todo lo necesario para funcionar a la primera, sin más complicaciones ni problemas. Por ahora la lista de programas que encontraremos es recortada, pero se espera que en un futuro, poco a poco vayamos encontrando más y más software que disfrutar. La lista de aplicaciones y juegos no es fácil de encontrar, pero en la [página del proyecto en Github](https://github.com/winepak/applications) podremos encontrar más información.


Como es lógico, si no lo hemos hecho aun, para empezar debemos habilitar el uso de Flatpak en nuestro sistema, y para ello podremos encontrar las instrucciones de como hacerlo en la [página oficial](https://flatpak.org/setup/). Después **debemos instalar los repositorios de Flathub y Winepak**. Para ello ejecutaremos los siguientes comandos en una consola:



> 
> `flatpak remote-add --if-not-exists flathub <https://dl.flathub.org/repo/flathub.flatpakrepo>`
> 
> 
> `flatpak remote-add --if-not-exists winepak <https://dl.winepak.org/repo/winepak.flatpakrepo>`
> 
> 
> 


Desde este momento podemos listar los programas y juegos disponibles en Winepak escribiendo lo siguiente en la terminal:



> 
> `flatpak remote-ls winepak`
> 
> 
> 


**En este momento** podremos encontrar las que vemos en la siguiente captura:


![ListaWinepak](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Winepak/ListaWinepak.webp)


Como veis hay "cosillas" bastante interesantes como "World of Tanks", "Starcraft 2", "Path of Exile", "League of Legends".... Para instalar cualquiera de estas apliaciones, por ejemplo el emulador de WiiU "Cemu", simplemente teclearemos lo siguiente:



> 
> `flatpak install winepak info.cemu.Cemu`
> 
> 
> 


Como veis primero tecleais "flatpak install", después le decís al programa que use el repositorio "winepak" y por último el nombre del paquete flatpak, "info.cemu.Cemu". Todo muy sencillo... Es importante decir que las apliaciones contenidas en este repositorio son simplemente enlaces de descarga a aplicaciones de libre distribución empaquetadas conjuntamente con todo lo necesario para ejecutarse.


Desviandonos un poco del tema, hablar de Wine como siempre genera controversia, incluso dentro de nuestra redacción hay división. Para mucha gente se trata de un software maravilloso e imprescindible, dándo la oportunidad de disfrutar de aplicaciones y juegos que de otra manera les estarían vedados; pero para otras personas es simplemente mandar un mensaje equvocado a las desarrolladoras de software, ya que estas pueden entender que no es necesario portar su software, ya que la comunidad se busca la vida para poder usarlo. Sea cual sea vuestro parecer, aquí en JugandoEnLinux entendemos las dos posturas, y en vosotros, como siempre está la decisión de sacarle provecho o no; pero creemos que es importante que la gente conozca todas las opciones de las que dispone.


¿Habeis usado ya Winepak? ¿Qué opinión os merece el uso de Wine en Gnu-Linux? Déjanos tu opinión en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/fgQubVY).

