---
author: Pato
category: Aventuras
date: 2017-05-09 08:46:31
excerpt: "<p>El juego consigui\xF3 financiarse con m\xE1s de 190.000$. Tiene demo\
  \ Linux disponible</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8376aace7af18ea8cafa499d7e69a6ec.webp
joomla_id: 310
joomla_url: chasm-un-prometedor-juego-de-aventuras-y-plataformas-llega-a-su-version-alfa-final-despues-de-4-anos
layout: post
tags:
- accion
- indie
- plataformas
- aventura
- rol
- metroidvania
title: "'Chasm' un prometedor juego de aventuras y plataformas llega a su versi\xF3\
  n \"alfa final\" despu\xE9s de 4 a\xF1os"
---
El juego consiguió financiarse con más de 190.000$. Tiene demo Linux disponible

'Chasm' es uno de esos casos en los que el desarrollo se alarga más de lo deseable. El juego está en desarrollo por "Bit Kid Inc." [[web oficial](http://bitkidgames.com/)] llevó a cabo una exitosa campaña en Kickstarter en la que consiguieron financiarse con mas de 190.000$ y en la que anunciaban la entrega del juego para 2014. Sin embargo, a fecha de hoy el juego aún sigue en desarrollo, aunque los desarrolladores acaban de anunciar que el juego ha llegado ya a fase "alfa final", tal y como puede [leerse en su blog](http://bitkidgames.com/?p=3409).



> 
> *Hemos obtenido mucho "feedback" sobre la actualización de la alfa final por parte de la comunidad durante las últimas semanas [...] es un gran alivio saber que finalmente hemos llevado al juego al lugar donde todos sienten que no solo es divertido y excitante, si no muy cercano a lo que prometimos en un principio"*
> 
> 
> 


 Según los desarrolladores, el juego ya ha sido completado con todas las opciones y características que querían introcudir, con lo que a partir de ahora se centrarán en pulirlo de cara a un próximo lanzamiento oficial.


#### Sobre Chasm:


Chasm es un juego de aventura y plataformas proceduralmente generado e inspirado en juegos de tipo "Dungeon Crawlers" y plataformas de estilo "metroidvania". Te sumergirá en un mundo procedural de fantasía lleno de tesoros, enemigos mortales y abundantes secretos.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/sjXoop3LnB8" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


#### Características:


- Explora seis áreas generadas proceduralmente.


- Disfruta con un desafiante juego retro con auténtico "pixel Art" (resolución nativa 384x216)


- Lucha contra jefes finales masivos y descubre nuevas habilidades para acceder a áreas inaccesibles préviamente.


- Personaliza a tu personaje equipándolo con armas, escudos y hechizos.


 


'Chasm' estará disponible con versión Linux, y de hecho tiene una demo para poder probar el juego. Puedes descargarla [desde este enlace](http://www.indiedb.com/games/chasm/downloads/chasm-105-linux).


 


¿Qué te parece este 'Chasm'? ¿Has probado la demo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

