---
author: Serjor
category: Software
date: 2019-03-19 15:17:14
excerpt: "<p>A falta de lanzadores oficiales, GameHub busca facilitar la descarga\
  \ e instalaci\xF3n de nuestros juegos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4cbaabc235457a2dc97a1a76ff39c8ea.webp
joomla_id: 999
joomla_url: gamehub-un-lanzador-de-videojuegos-con-integraciones-muy-interesantes
layout: post
tags:
- lutris
- lanzador
- gamehub
- launcher
title: GameHub, un lanzador de videojuegos con integraciones muy interesantes
---
A falta de lanzadores oficiales, GameHub busca facilitar la descarga e instalación de nuestros juegos

Leemos en [GamingOnLinux](https://www.gamingonlinux.com/articles/gamehub-is-another-open-source-game-launcher-giving-lutris-some-competition.13778) sobre [GameHub](https://tkashkin.tk/projects/gamehub/) un frontend para manejar nuestra librería de juegos.


GameHub viene a solucionar el mismo problema que Lutris, y no es otro que tener en una única aplicación toda nuestra colección de juegos, independientemente de en qué tienda digital lo hayamos adquirido. Además, también puede hacer de lanzador para juegos que ya tengamos instalados en el disco duro.


Y al igual que Lutris, permite ejecutar juegos no nativos, pero curiosamente, GameHub puede hacer uso directamente de Proton para ejecutar los juegos, aunque en mis pruebas, no he sido capaz de ejecutar ningún juego no nativo a través de GameHub, aunque también hay que decir que estamos ante una versión de desarrollo, y que todavía no ha llegado a la versión 1.0.


Pero a pesar de no haber llegado a la versión 1.0, y de los fallos o carencias que esto implica, he de reconocer que la aplicación me ha sorprendido gratamente. Por ejemplo, podemos vincular con Steam (aunque en el caso de steam nos obliga a tener la aplicación ejecutándose por el tema del DRM), GOG y Humble Bundle, y en estas dos últimas tiendas la integración es sencilla y cómoda, más que la que tenemos en Lutris con GOG (y recordemos que aún no tenemos integración con Humble Bundle en Lutris), tanto, que para estas dos tiendas casi prefiero GameHub a Lutris (bueno, lo de Humble no cuenta por razones obvias).


Al margen de las integraciones, GameHub tiene un aspecto estético cuidado. Tenemos para elegir las clásicas vistas en lista o grid, podemos filtrar por tags, ajustar la configuración de los juegos, y además para cada juego podemos tener instancias diferentes para los diferentes DLCs o mods, sin afectar al resto de instalaciones.


Si queréis probar este nuevo frontend para nuestros juegos, podéis ir a su página en [GitHub](https://github.com/tkashkin/GameHub) y descargaros el programa en formato .deb, AppImage, Flatpak, y por supuesto, el código fuente.


Y tú, ¿qué opinas de este GameHub? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

