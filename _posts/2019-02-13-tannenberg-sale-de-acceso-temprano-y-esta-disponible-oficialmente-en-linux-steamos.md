---
author: Pato
category: "Acci\xF3n"
date: 2019-02-13 17:15:38
excerpt: "<p>El juego de Blackmill @WW1GameSeries viene adem\xE1s con mas contenido\
  \ y novedades tanto para este juego como para Verdun</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/64ca0a634602805d9129303f1b94d936.webp
joomla_id: 970
joomla_url: tannenberg-sale-de-acceso-temprano-y-esta-disponible-oficialmente-en-linux-steamos
layout: post
tags:
- accion
- multijugador
- fps
- online
- wwi
title: "'Tannenberg' sale de acceso temprano y est\xE1 disponible oficialmente en\
  \ Linux/SteamOS"
---
El juego de Blackmill @WW1GameSeries viene además con mas contenido y novedades tanto para este juego como para Verdun

Buenas noticias para los que buscan acción en la Primera Gran Guerra. Tal y como [anunciamos](index.php/homepage/generos/accion/5-accion/1085-tannenberg-hace-publica-su-fecha-de-lanzamiento-para-el-proximo-13-de-febrero) en su momento '**Tannenberg**', el juego de Blackmill Games responsables del también notable [Verdun](https://store.steampowered.com/app/242860/) acaba de salir de su fase de acceso temprano y ya está disponible oficialmente para Linux/SteamOS.


*Tannenberg recrea las descomunales batallas del frente oriental en la Primera Guerra Mundial con 64 jugadores luchando por el control de los sectores clave del campo de batalla, cada uno con sus corresponientes ventajas estratégicas. Los conflictos entre el Imperio ruso, Alemania y sus aliados ofrecen una novedosa experiencia tanto a los usuarios de juegos de disparos en primera persona como a los apasionados de la historia.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/9RW4pfqNyeg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


*Seis pelotones, más de 50 armas, seis mapas enormes que ofrecen libertad táctica a los jugadores y un modo de juego de 64 jugadores con apoyo de la IA para que puedas experimentar batallas épicas en cualquier momento! Todo se ha recreado con el mismo cuidado de los detalles que se observa en Verdun, la primera entrega de la saga de videojuegos de la Primera Guerra Mundial.*


Según el anuncio oficial, tanto Tannenberg como Verdun han recibido una serie de características y nuevo contenido. En concreto, para Tannemberg el estudio anuncia *un nuevo escuadrón, un mapa, armas y ... ¡un evento secreto!  
La nueva escuadra búlgara utiliza una serie de armas icónicas como la carabina Mannlicher M90 y el venerable rifle Martini-Henry. Como escuadrón de apoyo, pueden llamar a los vuelos de reconocimiento y tener acceso a una serie de opciones de equipamiento equilibrado con rifles, pistolas y granadas.*


*Los búlgaros se unirán para luchar por el nuevo mapa, que se encuentra en la región de Dobrudja, entre Bulgaria y Rumania. Con las aguas estratégicamente importantes del río Danubio visibles en la distancia, hay una posición central en la cima de la colina para competir, con pequeñas fortificaciones dispersas alrededor de la colina que conducen a un valle en el norte.*


Además, anuncian un nuevo evento especial secreto que será desvelado próximamente y del que os mantendremos informados.


**Novedades en Verdún**


En cuanto a Verdún, el estudio no deja de ampliar contenido, anunciando *correcciones de errores y mejoras de bots, se ha agregado soporte de idioma para turco, polaco y portugués brasileño, ¡y pronto se publicarán el japonés y el chino simplificado! Además, **el precio de Verdún se reducirá ligeramente**, por lo que si conoces a amigos que han estado interesados, ¡ahora puede ser un buen momento para que lo vean! puedes ver el registro de cambios completo [en nuestro log](https://steamcommunity.com/app/242860/discussions/0/3160848559787186112/).*


En cuanto a los requisitos de Tannenberg para Linux, son:


**MÍNIMO:**


+ **SO:** Ubuntu 18.04+ 64bit . Otras distros probablemente funcionarán, pero no están oficialmente soportadas.
+ **Procesador:** Intel CPU Core i5-2500K 3.3GHz, AMD CPU Phenom II X4 940
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** Geforce GTX 960M / Radeon HD 7750 o superior, Gráfica con 2GB o superior
+ **Red:** Conexión de banda ancha a Internet
+ **Almacenamiento:** 6 GB de espacio disponible
+ **Notas adicionales:** Para Multijugador, asegúrate de tener una conexión a internet rápida y estable.


Si quieres saber más, puedes visitar su [página web oficial](https://www.ww1gameseries.com/tannenberg/), o también tienes Tannenberg disponible en español en [Humble Bundle](https://www.humblebundle.com/store/tannenberg?partner=jugandoenlinux) (enlace patrocinado) o en su [página de Steam](https://store.steampowered.com/app/633460/).

