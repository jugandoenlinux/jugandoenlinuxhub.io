---
author: leillo1975
category: Estrategia
date: 2021-02-04 15:00:07
excerpt: "<p>El juego de @CAGames y @SEGA_Europe llegar\xE1&nbsp; gracias a @feralgames.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/TotalWar-Warhammer3/total-war-Warhmmer3-feral-Linux.webp
joomla_id: 1273
joomla_url: total-war-warhammer-iii-llegara-a-linux-de-la-mano-de-feral-a
layout: post
tags:
- feral-interactive
- estrategia
- sega
- warhammer
- total-war
- creative-assembly
title: "Feral nos traer\xE1 a Linux Total War: Warhammer III"
---
El juego de @CAGames y @SEGA_Europe llegará  gracias a @feralgames.


 Ya no sabemos cuanto tiempo habrá pasado desde la última vez que el estudio Británico [Feral Interactive](https://www.feralinteractive.com/es/) anunció su último juego, y es que la en otra hora prolífica compañía, que tan felices nos hizo con sus ports de juegos; parece ahora habernos olvidado en favor de los usuarios de Switch y móviles. Atrás quedó aquella época en la que los jugadores Linuxeros esperábamos avidamente nuevas noticias de esta gente que como sabeis **nos trajo títulos tan importantes** como la saga [Tomb Raider](index.php/component/search/?searchword=tomb%20raider&ordering=newest&searchphrase=all&limit=20&areas[0]=tags), [XCOM]({{ "/tags/xcom-2" | absolute_url }}), [DIRT Rally]({{ "/posts/analisis-dirt-rally" | absolute_url }}), [F1 2017]({{ "/posts/analisis-f1-2017" | absolute_url }}) o [Shogun II: Total War](index.php/component/k2/1-blog-principal/analisis/487-analisis-total-war-shogun-2-y-fall-of-the-samurai). Como sabeis, este estudio también fué **pionero en el uso de Vulkan** en sus ports, así como de integrar el conocido [GameMode]({{ "/tags/gamemode" | absolute_url }}).


Pero esa racha se acabó, no sabemos si por las **"escasas" ventas de los juegos** en nuestra plataforma, o por el **uso generalizado de Steam Play/Proton**; aunque lo más seguro es que fuese por una mezcla de ambas. El caso es que en los últimos años sobran los dedos de una mano para enumerar los juegos nuevos que ha lanzado esta compañía con soporte para nuestro sistema. Nos gustaría pensar que se trata de un bache pasajero, y que en un futuro veamos más ports de calidad... sigamos rezando por el milagro.


Pero bueno, no hemos venido aquí a recordar los buenos tiempos de los ports a Linux, sinó a celebrar el anuncio realizado ayer en el cual se confirma que este estudio trabajará para traernos el nuevo Total War: Warhammer III tal y como podemos ver en este tweet:  
  




> 
> Total War: WARHAMMER III is also coming to macOS & Linux!  
>   
> Pre-purchase now from Steam: <https://t.co/fRbTvdBeRo> <https://t.co/oIEYpO0oNh>
> 
> 
> — Feral Interactive (@feralgames) [February 3, 2021](https://twitter.com/feralgames/status/1356982376673509380?ref_src=twsrc%5Etfw)


  





Según hemos podido saber el juego **se intentará que llegue lo más cerca posible de la fecha de lanzamiento en Windows**, lo cual es una buena noticia para los fans de esta saga. Es reseñable comunicaros que a pesar de que también saldrá en la polémica (y tan "adorada" por los Linuxeros) Epic Store, **se lanzará a la vez y esta vez sin exclusividades en [Steam](https://store.steampowered.com/app/1142710/Total_War_WARHAMMER_III/)** (al contrario que Total War: Troya) , donde ya podeis precomprar y poner en vuestra lista de deseos el juego. Según la descripción oficial que podemos encontrar en Steam, en Total War: Warhammer III encontraremos:


  
*Más allá del mundo y sus insignificantes guerras, existe una dimensión de magia puramente malévola: el Reino del Caos. Es un lugar terrible que las mentes mortales no alcanzan a comprender. Te susurra promesas de poder, pero si lo contemplas, te seduce. Para que le entregues tu alma. Para que te conviertas en él.*  
   
 *Los cuatro Poderes Ruinosos gobiernan este lugar en una pugna constante por escapar a sus ataduras y sumir al mundo en la corrupción demoníaca. Nurgle, el Dios de la Plaga; Slaanesh, el Dios del Exceso; Tzeentch, El Que Cambia Las Cosas; y Khorne, el Dios de la Sangre y la Matanza.*  
   
 *En el confín entre los mundos, dos poderosos reinos oponen resistencia: los adustos guerreros de Kislev y el vasto imperio de la Gran Catai. Aunque cada uno debe hacer frente a sus propios problemas, ahora ambos tienen motivos para cruzar la frontera y enviar a sus ejércitos al Reino del Caos.*  
   
 *El mundo se encuentra al borde del abismo. Un solo empujón desatará un cataclismo.*  
   
 *Además, hay alguien que conspira con tal fin, una figura ancestral que no desea más que ostentar el poder supremo. Sin embargo, para conseguirlo, precisará de un paladín...*  
   
 *El conflicto que se avecina repercutirá en todo cuanto existe. ¿Conquistarás a tus demonios? ¿O los dirigirás?*


En JugandoEnLinux.com por supuesto seguiremos informándoos sober el desarrollo y lanzamiento de este juego y todos en los que esté involucrada Feral Interactive. Os dejamos con el trailer del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/cGCdgnT8INw" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


¿Qué te parecen los juegos de Total War? ¿Qué opinión te merece el trabajo de Feral? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

