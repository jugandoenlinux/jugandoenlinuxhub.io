---
author: Serjor
category: Estrategia
date: 2019-09-19 20:41:17
excerpt: "<p>Llegar\xE1 a lo largo del 2020 de la mano de Feral Interactive</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cf543ccbc908c161905844b62e978c76.webp
joomla_id: 1112
joomla_url: troy-un-nuevo-juego-de-la-saga-total-war-tendra-version-nativa-para-gnu-linux
layout: post
tags:
- rts
- feral-interactive
- total-war
- troy
title: "TROY, un nuevo juego de la saga Total War, tendr\xE1 versi\xF3n nativa para\
  \ GNU/Linux"
---
Llegará a lo largo del 2020 de la mano de Feral Interactive

ía nota de prensa nos enteramos de que la saga de Total Wars contará con un nuevo título, TROY, y está basada en la Ilíada de Homero.


El anuncio de Feral Interactive, que podéis encontrar [aquí](http://www.feralinteractive.com/es/news/1001/ "http://www.feralinteractive.com/es/news/1001/"), nos dice lo siguiente:



> 
> TROY es el título de Total War Saga más reciente. Inspirado en la "Ilíada" de Homero, se centra en el punto histórico de la guerra de Troya. A través de la combinación única de Total War de gestión de imperios por turnos y espectaculares batallas en tiempo real, TROY explora este conflicto épico desde los puntos de vista griego y troyano. Experimenta los acontecimientos que ocurrieron alrededor de la guerra de Troya como nunca antes. Realiza tu leyenda como uno de los ocho icónicos héroes. Sumérgete en una aventura narrativa basada en los personajes y vence a tus enemigos. Construye tu imperio a través de estrategia, habilidad política, diplomacia y guerra sin cuartel, mientras conquistas esta vasta y sorprendente recreación del Mediterráneo de la Edad de Bronce. A Total War Saga: TROY llegará a macOS y Linux poco después de Windows en 2020.
> 


La verdad es que aunque es una nueva iteración a una saga con una gran cantidad de juegos, es más que positivo ver que Feral Interactive sigue trabajando de forma activa en traernos juegos nativos a GNU/Linux, y si queréis más información del juego, podéis visitar su página en [Total War](https://www.totalwar.com/blog/a-total-war-saga-troy-faq/).


Os dejamos con el vídeo del anuncio del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/NsrC52Xehzw" width="560"></iframe></div>


Y tú, ¿eres fan del género? Cuéntanos qué te parece en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux "https://t.me/jugandoenlinux") y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org "https://matrix.to/#/+jugandoenlinux.com:matrix.org")

