---
author: Serjor
category: Noticia
date: 2018-05-31 19:35:22
excerpt: "<p>Un anuncio sin anunciar nada pero que todos queremos saber qu\xE9 es</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1b56d005723ad175e526bbe57567b8ab.webp
joomla_id: 752
joomla_url: feral-nos-anuncia-a-su-manera-un-nuevo-juego-para-gnu-linux
layout: post
tags:
- feral-interactive
- anuncios
- feral
- teaser
title: Feral nos anuncia a su manera un nuevo juego para GNU/Linux
---
Un anuncio sin anunciar nada pero que todos queremos saber qué es

Leemos en [gamingonlinux.com](https://www.gamingonlinux.com/articles/beep-boop-the-feral-interactive-port-radar-has-a-ufo-sighting-for-a-new-linux-port.11887) que Feral Interactive nos vuelve a poner en ascuas al añadir un nuevo juego en su ya famoso [radar de ufos](https://www.feralinteractive.com/en/upcoming/).


Como siempre en estos casos reddit ha comenzado con sus [locas teorías](https://www.reddit.com/r/linux_gaming/comments/8njcp3/beep_boop_the_feral_interactive_port_radar_has_a/?st=jhuxkm48&sh=9398a4c1), como por ejemplo Doom o Barbie, aunque por desgracia o afortunadamente (que cada uno valore qué juego le merece qué opinión) es poco probable que sea alguno de estos dos.


Aún es pronto para saber qué juego será, o cuando harán el siguiente anuncio, pero os mantendremos informados puntualmente.


Eso sí, ¿qué juego crees que será el afortunado? Déjanos en los comentarios tus apuestas.

