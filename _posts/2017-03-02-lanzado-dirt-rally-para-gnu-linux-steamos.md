---
author: leillo1975
category: Carreras
date: 2017-03-02 14:27:18
excerpt: <p>Feral Interactive acaba de liberar el port de este conocido juego de Rallys</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3b77d3f73b59742412f393cd0d264b14.webp
joomla_id: 236
joomla_url: lanzado-dirt-rally-para-gnu-linux-steamos
layout: post
tags:
- feral-interactive
- dirt-rally
title: 'Lanzado DIRT: Rally para GNU-Linux / SteamOS'
---
Feral Interactive acaba de liberar el port de este conocido juego de Rallys

Todos los amantes linuxeros de los juegos de conducción están de enhorabuena, pues en el día de hoy ha sido lanzado definitivamente el mejor juego de Rallys hasta la fecha  gracias al genial trabajo de Feral Interactive. Primero nos trajeron otros grandes juegos como **GRID Autosport**, donde podíamos conducir coches de todo tipo en 5 pruebas diferentes. Más tarde le toco a la categoría reina del automovilismo con **F1 2015**. Ahora por fin podremos disfrutar de las pistas más irregulares y los tramos más enrevesados en nuestros PCs.


 


Hace menos de un mes [os informábamos](index.php/homepage/generos/carreras/item/326-dirt-rally-disponible-el-2-de-marzo-para-gnu-linux) de que el juego sería lanzado tal día como hoy, y tal y como nos han prometido lo tenemos aquí. El juego hace uso de OpenGL [según nos informaron en un tweet](https://twitter.com/feralgames/status/827124492593950722) cuando se anunció el juego y sus requisitos mínimos son los siguientes:


 




|  Mínimo | Recomendado |
| --- | --- |
| Procesador:Intel Core i3-4130T 2.90GHz, AMD FX6300OS:Ubuntu 16.10 (64bit) o SteamOS (2.0 : 2.110)RAM:8GBEspacio Libre:42GBGrafica:1GB NVIDIA 650ti o mejor (driver version 370.28), 1GB AMD R9 270 o mejor (MESA 13.0.3 o mejor),Intel Iris Pro 6200 o mejor (MESA 13.0.3 oo mejor). | Procesador:Intel Core i5-6500 3.20GHzOS:Ubuntu 16.10 (64bit) o SteamOS (2.0 : 2.110)RAM:8GBEspacio Libre:42GBGrafica:4GB NVIDIA 970 or mejor (driver version 370.28). |


 


Como veis, se puede jugar con gráficas AMD e incluso Intel Iris. Según arrojan los primeros tests de Phoronix ([AMD](http://www.phoronix.com/scan.php?page=article&item=rally-dirt-radeon&num=1) y [Nvidia](http://www.phoronix.com/scan.php?page=article&item=rally-dirt-nv14&num=1)), el rendimiento del juego es excelente, por lo que en este caso el trabajo de optimización por parte de Feral parece que ha sido fantástico.Os dejo la descripción oficial de DIRT Rally:


*Te damos la bienvenida al mundo del rally*


*Vive la emoción de las carreras al límite con DiRT Rally, el juego de rally más realista jamás creado.*


*DiRT, la emblemática saga de rallies del estudio especializado en competiciones de carreras Codemasters, llega a Linux con una jugabilidad inmejorable, con la que solo podrás ganar si arriesgas. Ponte el cinturón y recorre a toda velocidad las carreteras más peligrosas del mundo, desde los densos pinares de Gales hasta las accidentadas laderas griegas.*


*Pon a prueba tus nervios, tu habilidad y tu coche en carreras de Rally, Rallycross y Hillclimb que te llevarán a los límites del control, y gana dinero ganando campeonatos para hacerte con bólidos más potentes. Necesitarás de toda tu capacidad de concentración y tacto al volante para alcanzar la cima del espectacular mundo de las carreras todoterreno.*


* *Disfruta el mundo de los rallies con mayor realismo. El sofisticado modelo de conducción de DiRT Rally hará que sientas cada derrape como si condujeras coches de rally reales.*
* *Aprende y domina tres disciplinas: las carreras entre dos puntos de Rally, los circuitos estrechos y cambiantes de Rallycross y las pendientes legendarias de Hillclimb. Compite en campeonatos de todas las disciplinas para hacerte con mejores coches e ingenieros para tu propio equipo de rally.*
* *Pon a prueba tu valía contra rivales de Linux y Windows en el modo multijugador multiplataforma.*
* *Pisa a fondo con 45 de los coches más icónicos: podrás conducir desde coches de época hasta los más modernos, como el Peugeot 205 T16 Evo 2 y el Lancia Delta S4 de la etapa Grupo B de carreras de rally.*
* *Conduce a toda velocidad en más de 70 etapas por Grecia, Gales, Mónaco, Alemania, Finlandia y Suecia. Cada país presenta sus propios desafíos con recorridos y climas diferentes, desde estrechos tramos helados hasta carreras asfaltadas de alta velocidad que se pueden hacer extremadamente peligrosas con la lluvia.*
* *Pon tu coche a punto realizando los ajustes necesarios, como calibrar los frenos o configurar los ratios del cambio, para que se adapte a la carretera, al clima y a tu propio estilo de conducción.*


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/3USVRp0j8sc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


También me gustaría informaros que proximamente os ofreceremos un completo analisis de este título en esta web, por lo que os recomiendo que esteis atentos. Si quereis adquirir este gran juego podeis hacerlo en la [**tienda oficial de Feral Interactive**](https://store.feralinteractive.com/es/mac-linux-games/dirtrally/) o en Steam; 


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/310560/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


FUENTES: [Feral Interactive](http://www.feralinteractive.com/es/linux-games/dirtrally/), [Phoronix](https://www.phoronix.com/scan.php?page=home)

