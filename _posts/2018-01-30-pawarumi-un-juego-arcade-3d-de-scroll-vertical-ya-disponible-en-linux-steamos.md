---
author: Pato
category: Arcade
date: 2018-01-30 17:09:23
excerpt: "<p>El juego hace gala de unos gr\xE1ficos bastante llamativos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a05812a6ea54f32db80b865f7cd66f25.webp
joomla_id: 624
joomla_url: pawarumi-un-juego-arcade-3d-de-scroll-vertical-ya-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- arcade
- shoot-em-up
title: '''PAWARUMI'' un juego arcade 3D de scroll vertical ya disponible en Linux/SteamOS'
---
El juego hace gala de unos gráficos bastante llamativos

Seguimos recibiendo títulos arcade a través de campañas de financiación. Esta vez nos llega 'PAWARUMI', un juego arcade de acción 3d y scroll vertical que hace gala de unos gráficos bastante llamativos y que estuvo en [campaña en kickstarter](https://www.kickstarter.com/projects/pawarumi/pawarumi-a-neo-aztec-arcade-shootem-up-game?ref=nav_search&result=project&term=pawarumi) donde levantó casi 16.000€.


'PAWARUMI' [[web oficial](http://pawarumi.com/)] es un shoot'em up ambientado en un universo pre-Colombino retrofuturista. Tomarás el control de la nave Chukaru y sus tres armas únicas para disparar a todo lo que se mueva, ¡incluyendo un super ataque!.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/QNW6uA0RO3I" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Características principales:


* Dirección artística original: ¡Ambientada en un universo sci-fi pre-colombino!
* Presentado totalmente en 3D: ¡Permite un escenario cinematográfico espectacular!
* Cinco niveles únicos: Viajando alrededor del mundo e inspirado en los clásicos de la cultura pop de los 90s (Indiana Jones, James Bond, Star Wars…)
* Mecánica de juego Trinity: ¡Tres mecánicas fusionadas que permiten infinitas estrategias!
* Sistema de puntuación Weapon Mastery: Configura tu propia estrategia de puntuación con un cuidadoso aprovechamiento del armamento. Sin tener que conseguir aquellas largas combinaciones, ¡los jugadores más competitivos siempre podrán lucirse! ¿Estás preparado para el reto de llegar a lo más alto de la clasificación?
* Música dinámica en diferentes capas: ¡Increíble banda sonora de electro metal peruano!
* Soporte completo para teclado y controlador: ¡con entradas redefinibles para una compatibilidad máxima!


 En cuanto a los requisitos, son:


**Mínimo:**  

+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 16.04 (64 bits)
+ **Procesador:** Core i3
+ **Memoria:** 2 GB de RAM
+ **Gráficos:** GeForce GTX 460
+ **Almacenamiento:** 2 GB de espacio disponible


**Recomendado:**  

+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 16.04 (64 bits)
+ **Procesador:** Core i5
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** GeForce GTX 960
+ **Almacenamiento:** 2 GB de espacio disponible



Si te interesan los shoot'em ups puedes echar un vistazo a este PAWARUMI en su página de Steam donde se encuentra traducido al español:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/610410/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>
 
¿Que te parece este PAWARUMI? ¿Te gustan los shoot'em ups verticales?

Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).



 