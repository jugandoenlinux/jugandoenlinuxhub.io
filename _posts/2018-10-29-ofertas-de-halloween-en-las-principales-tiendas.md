---
author: leillo1975
category: Ofertas
date: 2018-10-29 19:57:59
excerpt: <p>Montones de juegos irresistibles en Humble Store, Steam y GOG</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/eda8d07e2f8b89e75897e379264dc261.webp
joomla_id: 884
joomla_url: ofertas-de-halloween-en-las-principales-tiendas
layout: post
tags:
- steam
- gog
- ofertas
- humble-store
- halloween
title: "\xA1Ofertas de Halloween en las principales tiendas!"
---
Montones de juegos irresistibles en Humble Store, Steam y GOG

Empezamos con la temporada de ofertas en las principales tiendas, y como viene siendo habitual **comenzamos con las de Halloween**. Tanto Humble Store, como Steam, como GOG.com están ofreciendo juegos, principalmente de terror, con grandes descuentos que nos ponen los dientes largos a todos y acaban con nuestras carteras, aunque también hay que decirlo, nos ahorran bastante dinero en los juegos que hace tiempo queremos comprar.


Comenzamos con las **[ofertas de Humble Store](https://www.humblebundle.com/store/promo/halloween-sale?partner=jugandoenlinux)**, ya que como sabeis nos patrocinan y recibimos algunos centimillos por cada venta que viene a través de nuestros enlaces, que nos vienen de perlas para pagar los gastos de Hosting y Dominios:



> 
> -[7 days to Die](https://www.humblebundle.com/store/7-days-to-die?partner=jugandoenlinux) (-64%): **8.27€**
> 
> 
> -[Darkwood](https://www.humblebundle.com/store/darkwood?partner=jugandoenlinux) (-50%): **6.99€**
> 
> 
> -[Amnesia: The Dark Descent](https://www.humblebundle.com/store/amnesia-the-dark-descent?partner=jugandoenlinux) (-75%): **4.99€**
> 
> 
> -[SOMA](https://www.humblebundle.com/store/soma?partner=jugandoenlinux) (-75%): **6.99€**
> 
> 
> -[Dying Light: The Following Enhanced Edition](https://www.humblebundle.com/store/dying-light-the-following-enhanced-edition?partner=jugandoenlinux) (-67%): **16.49€**
> 
> 
> -[Mount & Blade: Warband](https://www.humblebundle.com/store/mount-blade-warband?partner=jugandoenlinux) (-60%): **7.99€**
> 
> 
> -[XCOM 2](https://www.humblebundle.com/store/xcom-2?partner=jugandoenlinux) (-75%): **12.49€**
> 
> 
> -[Outlast](https://www.humblebundle.com/store/outlast?partner=jugandoenlinux) (-75%): **4.99€**
> 
> 
> -[White Noise 2](https://www.humblebundle.com/store/white-noise-2?partner=jugandoenlinux) (-70%): **2.99€** (Juego del estudio español [Milkstone](index.php/buscar?searchword=milkstone&ordering=newest&searchphrase=all))
> 
> 
> -[Spec Ops: The Line](https://www.humblebundle.com/store/spec-ops-the-line?partner=jugandoenlinux) (-80%): **3.99€**
> 
> 
> -[Layers of Fear: Masterpiece Edition](https://www.humblebundle.com/store/layers-of-fear-masterpiece-edition?partner=jugandoenlinux) (-75%): **5.37€**
> 
> 
> 


Ahora continuamos con las **[ofertas de Steam](https://store.steampowered.com/sale/halloween2018/)**, que como podreis ver también son bastante jugosas:



> 
> -[Don't Starve Together](https://store.steampowered.com/app/322330/Dont_Starve_Together/) (-60%): **5.99€**
> 
> 
> -[Dead Island Definitive Edition](https://store.steampowered.com/app/383150/Dead_Island_Definitive_Edition/) (-70%): **5.99€**
> 
> 
> -[Grim Fandango Remastered](https://store.steampowered.com/app/316790/Grim_Fandango_Remastered/) (-75%): **3.74€**
> 
> 
> -[Crypt of the Necrodancer](https://store.steampowered.com/app/247080/Crypt_of_the_NecroDancer/) (-80%): **2.99€**
> 
> 
> -[Alien Isolation](https://store.steampowered.com/app/214490/Alien_Isolation/) (-75%): **9.24€**
> 
> 
> -[Conarium](https://store.steampowered.com/app/313780/Conarium/) (-66%): **6.79€**
> 
> 
> -[Salt and Sanctuary](https://store.steampowered.com/app/283640/Salt_and_Sanctuary/) (-50%): **8.99€**
> 
> 
> 


Por último, y no menos importantes también tenemos [grandes ofertas en GOG.com](https://www.gog.com/games?page=1&sort=popularity&system=lin_ubuntu,lin_mint,lin_ubuntu_18&price=discounted) como podeis ver a continuación:



> 
> -[Trine 3: The arctifacts of Power](https://www.gog.com/game/trine_3_the_artifacts_of_power) (-75%): **5.49€**
> 
> 
> -[Aragami](https://www.gog.com/game/aragami) (-60%): **7.99€** (Juego del estudio español [Lince Works](index.php/buscar?searchword=lince&ordering=newest&searchphrase=all&limit=20))
> 
> 
> -[>Observer_](https://www.gog.com/game/observer) (-50%): **13.99€**
> 
> 
> -[Firewatch](https://www.gog.com/game/firewatch) (-70%): **7.99€**
> 
> 
> -[Sunless Sea](https://www.gog.com/game/sunless_sea) (-66%): **6.49€**
> 
> 
> -[Hard West: Collector's Edition](https://www.gog.com/game/hard_west_collectors_edition) (-85%): **3.79€**
> 
> 
> -[Black Mirror](https://www.gog.com/game/black_mirror) (-50%): **14.99€**
> 
> 
> 


Como veis las ofertas son numerosas y muy tentadoras. Podeis dejarnos más juegos con descuento en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

