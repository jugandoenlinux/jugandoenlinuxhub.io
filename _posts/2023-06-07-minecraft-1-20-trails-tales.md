---
author: Laegnur
category: "Exploraci\xF3n"
date: 2023-06-07 16:28:55
excerpt: "<p>Os traemos todos los detalles de la ultima versi\xF3n de este popular\
  \ juego, que un a\xF1o despu\xE9s del anterior lanzamiento, se actualiza hoy.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Minecraft/Minecraft-Update-120.webp
joomla_id: 1539
joomla_url: minecraft-1-20-trails-tales
layout: post
tags:
- actualizacion
- minecraft
title: 'Minecraft 1.20: Trails & Tales'
---
Os traemos todos los detalles de la ultima versión de este popular juego, que un año después del anterior lanzamiento, se actualiza hoy.


¡Salve jugadores!


Como digo, hoy 7 de Junio de 2023 se lanza la versión 1.20, conocida como Trails & Tales, algo así como, Senderos y Cuentos. Y es que esta actualización esta enfocada en aumentar la necesidad de explorar, para desentrañar la Historia o el Lore del mundo de Minecraft.


Para ello se añade al juego la nueva mecánica de Arqueología. Utilizando uno de los nuevos objetos que veremos a continuación, sobre algunos de los bloques nuevos, podremos desenterrar objetos arqueológicos o tesoros.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Minecraft/minecraft_arqueologia.webp)


Veamos pues, las novedades de esta versión.


### Bloques


Se añaden dos nuevos tipos de madera al juego.


* Madera de Bambú
* Madera de Cerezo


El Cerezo sera una madera exactamente igual a las demás, pero con una textura rosada. Teniendo el bloque de Tronco de Cerezo, los Tablones de Cerezo y todos sus bloques derivados (Puertas, Vallas, Escaleras, Losas, …). Asociada a esta madera tendremos ademas el bloque de Hojas de Cerezo y los Pétalos rosas que cubrirán el suelo donde haya un árbol de Cerezo.


Si bien, el Bambú ya existía en el juego, se actualizan las mecánicas del mismo, convirtiéndolo en una madera mas, y no solo una forma de obtener palos.


Uniendo 9 piezas individuales de Bambú, obtendremos el Bloque de Bambú, que seria el tronco en el resto de las maderas, y que nos permitirá pelar-lo con un hacha; y a partir de este, sacaremos los Tablones de Bambú, con sus bloques derivados, al igual que el resto de maderas.


Ademas tendremos un tercer bloque exclusivo del Bambú, el Mosaico de Bambú, que tendrá una textura similar al parqueé, muy conveniente para la fabricación de suelos.


En cuanto a bloques decorativos, tendremos los siguientes:


* Estantería Cincelada: podremos crear este nuevo bloque a partir de cualquier madera y nos permitirá guardar nuestros libros dentro del juego de una manera mas ordenada y visual, que meterlos dentro de un cofre como hasta ahora.
* Cartel colgante: Un nuevo tipo de cartelería que nos permitirá colocarlo colgando o al lado de otros bloques.
* Cabeza de Piglin: Se obtendrá matando a un Piglin mediante un Creeper Cargado, tarea complicada. Otro bloque decorativo que se une a las cabezas de Creeper, Esqueleto y Zombie.


Con la introducción de la Arqueología, recibiremos los siguientes bloques:


* Arena y Grava Sospechosas: Dos nuevos bloques que aparecerán en ciertos lugares, y que podremos “limpiar” con la nueva herramienta para desenterrar de su interior objetos arqueológicos, recursos, o objetos mundanos. Se convierte en su versión normal, Arena o Grava, si se mueve o se recoge, perdiendo lo que haya en su interior.
* Vasija decorada: Se fabricara a partir de 4 Fragmentos de Cerámica desenterrados de la Arena o Grava Sospechosas.
* Huevo de Sniffer: Cambien podremos desenterrar de la Arena Sospechosa este nuevo objeto, que nos permitirá obtener la nueva criatura, el Sniffer, una vez eclosione.


Otros bloques son:


* Plantorcha: Un nuevo tipo de flor, que podremos plantar a partir de las Semillas de Plantorcha, que encontraremos gracias al Sniffer.
* Planta Odre: Otro nuevo tipo de flor, al igual que la anterior, habrá que plantar-la a partir de su propia semilla, encontrada con el Sniffer.
* Sensor de Sculk calibrado: Similar al Sensor de Sculk introducido en la actualización anterior, pero que nos permitirá filtrar el tipo de vibraciones a las que reacciona, permitiéndonos circuitos Red Stone mas complejos.


### Objetos


* Pincel: El objeto principal del nuevo sistema de Arqueología y que nos permitirá cepillar la Arena Sospechosa y la Grava Sospechosa buscando tesoros.
* Fragmentos de Cerámica: Objetos que permitirán crear las nuevas Vasijas Decoradas. Existen hasta 20 diseños distintos, y podremos combinarlos creando nuestras combinaciones personales.
* Moldes de Herrería: Compuestos por 16 Diseños de Armaduras y Mejoras de Netherita, que permitirán actualizar nuestra armadura de Diamante a Netherita, o cambiar el diseño de todas nuestras armaduras, añadiéndoles un patrón de otro material al que estén formadas.
* Semillas de Plantorcha y de Odre: Las semillas para las nuevas flores.


### Criaturas


Se añaden dos nuevas criaturas al juego.


* Camellos: Ya que nuestras exploraciones arqueológicas nos llevaran al desierto, que mejor manera de recorrerlo que a lomos de un camello.
* Sniffer: La criatura ganadora de la votación en el Minecraft Live 2022. No se podrá domesticar, pero se podrá utilizar para encontrar semillas de los nuevos tipos de flores.


### Biomas y Estructuras


Se añade un nuevo bioma, el Cerezal, compuesto por arboles de Cerezo, y un manto de Pétalos Rosas cubriendo el suelo.


En estructuras, se modifica ligeramente el diseño de los Templos del Desierto, de los Pozos del Desierto o de las Ruinas Sumergidas para añadir Arena y Grava Sospechosa. Y se crean nuevas estructuras, Ruinas del sendero, compuestas de Terracota, Barro, Grava y Grava Sospechosa donde también podremos buscar tesoros arqueológicos.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/Rla3FUlxJdE" title="YouTube video player" width="720"></iframe></div>


Como siempre, os remito a que veais la [Wiki de Minecraft](https://minecraft.fandom.com/wiki/Java_Edition_1.20), donde podeis ver una lista completa de los cambios de esta version.


¡Me despido hasta la próxima!

