---
author: Serjor
category: Noticia
date: 2018-12-07 15:27:54
excerpt: "<p>Una nueva iteraci\xF3n en un a\xF1o marcado por grandes avances en los\
  \ drivers libres</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f64f51f7a96bb14202569a0586002bf4.webp
joomla_id: 924
joomla_url: la-version-18-3-de-mesa-ya-disponible
layout: post
tags:
- drivers
- amd
- intel
- mesa
- mesa-18
title: "La versi\xF3n 18.3 de MESA ya disponible"
---
Una nueva iteración en un año marcado por grandes avances en los drivers libres

Leemos en [GOL](https://www.gamingonlinux.com/articles/mesa-1830-for-those-of-you-using-the-open-source-drivers.13118) que los usuarios de gráficas Intel y AMD están de enhorabuena (y los usuarios de gráficas NVIDIA algo viejunas), y es que la versión 18.3 de mesa ha sido liberada, trayendo un buen puñado de mejoras y correcciones, entre otros, quizás el más destacado sea el soporte de manera estable de la extensión "VK_EXT_transform_feedback", la cuál permite a DXVK dar el soporte correcto a juegos como Witcher 3.


Las notas del lanzamiento las podéis encontrar [aquí](https://www.mesa3d.org/relnotes/18.3.0.html) y como podréis ver, hay unas cuantas mejoras, y un largo listado de corrección de errores.


Ahora toca esperar a la llegada de la versión 19.0, en la que como nos informaban desde [phoronix](https://www.phoronix.com/scan.php?page=news_item&px=RX-590-Linux-All-Is-Good), ya hay parches para mejorar el rendimiento de la Radeon RX 590.


Y tú, ¿esperabas este lanzamiento o eres de los que usan las versiones beta? Si es así, cuéntanos cómo rinden estos drivers en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

