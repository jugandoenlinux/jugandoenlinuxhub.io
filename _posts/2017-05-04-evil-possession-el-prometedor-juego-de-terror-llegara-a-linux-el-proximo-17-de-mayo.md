---
author: Pato
category: Terror
date: 2017-05-04 07:49:56
excerpt: "<p>Realizar\xE1s un exorcismo en primera persona... o acabar\xE1s poseido\
  \ por un esp\xEDritu</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2d535442c2c0b0669d8f5a051ed00bcc.webp
joomla_id: 304
joomla_url: evil-possession-el-prometedor-juego-de-terror-llegara-a-linux-el-proximo-17-de-mayo
layout: post
tags:
- indie
- aventura
- primera-persona
- terror
title: "'Evil Possession' el prometedor juego de terror llegar\xE1 a Linux el pr\xF3\
  ximo 17 de Mayo"
---
Realizarás un exorcismo en primera persona... o acabarás poseido por un espíritu

'Evil Possession' ha aparecido en la [base de datos](https://twitter.com/SteamDB_Linux/status/859939906587680768) de Steam con el icono de Linux para alegría de todo aquel que disfrute pasando malos ratos. Y es que la premisa de este juego es ponerte en la piel de un exorcista en una mansión de Andalucía de mediados del siglo pasado con la misión de investigar los sucesos paranormales que allí suceden. De la sinopsis de Steam:



> 
> Encarnamos un investigador de sucesos paranormales que únicamente con la ayuda de un detector de actividad paranormal (EMF detector), una pequeña lámpara de aceite y sus conocimientos sobre el mal, nos la ingeniaremos para expulsar al demonio. Tendremos que buscar los utensilios y realizar los ritos necesarios para completar el exorcismo mientras escapamos de las fauces de un espíritu endemoniado. Según vamos completando el ritual todo se irá complicando cada vez más e iremos descubriendo lo que realmente allí sucedió…
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/FWq65FzxfIY" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


El estudio 2Dragons Games se ha basado en una leyenda de la Andalucía profunda para dar vida a su primer título con el que pasaremos buenos "malos" ratos, en un estilo que en ciertos aspectos recuerda a la saga "Amnesia". El juego también pasó con éxito su [campaña de Greenlight](https://steamcommunity.com/sharedfiles/filedetails/?id=823429964) en Enero.


Si estás interesado en 'Evil Possession' lo podrás conseguir el próximo 17 de Mayo en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/620700/" width="646"></iframe></div>


¿Te gustan los juegos de terror?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

