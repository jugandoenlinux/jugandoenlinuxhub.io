---
author: yodefuensa
category: "An\xE1lisis"
date: 2020-04-15 16:27:14
excerpt: "<p>Nuestro colaborador Yodefuensa nos analiza este cl\xE1sico. \xA1No te\
  \ lo pierdas!</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisThisisthepolice/thisisthepolice.webp
joomla_id: 1207
joomla_url: analisis-this-is-the-police
layout: post
title: "An\xE1lisis this is the police "
---
Nuestro colaborador Yodefuensa nos analiza este clásico. ¡No te lo pierdas!


 


Cuando vi que en el humble choice de este mes estaba incluido el **This is the police 2**, supe que era un buen momento para retomar la primera parte de esta saga. Al  primer juego en total le había dedicado en su momento unas 10 horas, pero por pura frustración y por hacerlo mal lo abandoné. Es una pena porque recuerdo que el juego me gustó y me enganchó y que quizás si me hubiera dado un poco igual el devenir del personaje me lo hubiera pasado. Pero quería hacerlo bien.


[<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/1Vi7AEY-Y-I" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>](https://www.youtube.com/watch?v=1Vi7AEY-Y-I)



> 
> *"En This is the police encarnamos a Jack Boyd donde debemos enfrentarnos a las entrañas de Freeburg, una ciudad en plena decadencia. ¿Se retirará Jack con una bonita suma de dinero? ¿Acabará sin blanca? ¿O algo peor?*
> 
> 
> *Experimenta una controvertida historia de corrupción, crimen e intriga. Gestiona tu equipo, responde a emergencias e investiga crímenes en una ciudad al borde del caos. La mafia maquina entre bambalinas, clavando sus garras en la ciudad mientras el alcalde explota cualquier situación para medrar en su carrera política. Elige cómo te enfrentarás a cada situación. A veces deberás plantar cara a una crisis en pleno escenario del crimen o negociar con los jefes del crimen de Freeburg. A veces te encontrarás esquivando preguntas en la sala de prensa, o incluso puede que seas interrogado en el estrado. ¿Podrás conseguir que no estalle esta olla a presión, por lo menos hasta que encuentres un nidito en el que retirarte? ¿O quizás acabarás entre rejas... o algo peor?"*
> 
> 
> 


This is the police es un juego de gestión, la principal parte del juego se realiza sobre el tablero de la ciudad atendiendo avisos. Por cada aviso bien superado sumaremos puntos para que nuestros policías sean más eficaces. Si enviamos más policías a una misión mayor es la probabilidad de superarla con éxito pero puede hacer que nos quedemos sin efectivos para atender otro aviso. Cada aviso será diferente al anterior pues están predefinidos por días y no son aleatorios por lo que no veremos la misma emergencia dos veces si no reiniciamos la partida.


![thisisthepolice2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisThisisthepolice/thisisthepolice2.webp)


Aunque en un principio la mecánica es simple debemos saber que por falsos avisos los policías no ganan puntos de experiencia y si siempre mandamos los peores agentes a estas alertas, nunca progresarán, si un caso de poca importancia se complica, puede que un agente acabe muerto y repercuta en el estado anímico de la comisaría y el descontento del ayuntamiento. Porque pese a que la acción del juego transcurre en este tablero, hay mucho más. Hay tres pilares fundamentales, comisaría, ayuntamiento y mafia. Debemos satisfacer las necesidades de cada uno para no acabar mal, aunque tengo que decir que me ha parecido algo descompensado. Pues es el ayuntamiento quien tiene más peso en el juego, pues es el encargado de mejorar el número de policías o dispositivos especiales que tengamos. Si empezamos con mal pie nos sera muy difícil remontar cada fallo y cada agente muerto bajará nuestra efectividad y todo se nos hará cuesta arriba si no conseguimos mejorar nuestro cuerpo policial.


La otra mecánica principal del juego son las investigaciones, en base a unos relatos debemos esclarecer como aconteció el crimen organizando unas fotografías hasta cuadrar el relato. Si queremos atrapar a la mafia y conseguir suculentas recompensas debemos destinar a nuestros mejores agentes. Igualmente es una mecánica un poco rota, porque si destinamos a alguien no muy cualificado no avanzamos pero siempre podemos destinar a nuestro mejor detective a un caso, resolverlo y volver a reutilizarlo.


El aspecto negativo está en la jugabilidad, y es que al cabo de unas horas podemos cogerle el tranquillo, por lo que el juego tiende a subir la dificultad de manera muy artificial. Difícilmente puedes pasarte algunas situaciones a la primera pese a tener todos los datos. El propio juego te permite volver a atrás en el tiempo y retroceder varios días, y parece asumir que abusaremos de este mecanismo, rompiendo un ritmo y haciendo un juego repetitivo con tal de llevar la historia al punto que queremos.


Porque el punto fuerte de This is the police es la historia. El drama familiar de Jack Boyd, el estar a 180 días de la jubilación por orden del alcalde. Estamos ante un hombre que ha llevado una carrera ejemplar y que parece estar apunto de echar todo por tierra por el retiro que nos merecemos, sea limpio o no. Y en palabras del propio juego lo más difícil a veces es mantenerse recto. 


En el apartado gráfico contamos con unos acabados *low poly* con escenarios sin excesivos detalles pero en ningún momento desentona o echamos en falta algo más cuidado. Puede ser que hayan optado por esta estética para cubrir carencias, pero cumplen sobradamente. A la hora de contar la historia el juego optará por el uso de viñetas y cortes como en los comics.


![thisisthepolice1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisThisisthepolice/thisisthepolice1.webp)


Destacar positívamente la interfaz del juego, la mayor parte del tiempo estamos en la ciudad o gestionando diferente aspectos del juego y lo hacemos de forma clara y simple. Todo lo que hay es sencillo y cumple a la perfección pues nos ofrece información sin intrincados menús.


En cuanto al apartado sonoro, la música del juego es bastante variada. Antes de empezar cada día tendremos la opción de elegir la canción que nos acompañará dentro de un catálogo de jazz, soul y música clásica que podemos ir ampliando con dinero del juego. Los efectos sonoros están ahí y cumplen, hablamos de la sirena de policía o el sonido de las esposas al apresar a alguien.


![thisisthepolice3](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisThisisthepolice/thisisthepolice3.webp)


Por último respecto al port de linux y su rendimiento poco podemos decir, es un juego que pide muy pocos recursos:


**Procesador:** Dual Core CPU


**Memoria:** 2 GB de RAM


**Gráficos:** GeForce GT 330M, AMD Radeon HD 5750M, Intel HD 4000 o similar


La única pega que he encontrado es que el ratón del sistema se superpone al ratón del juego, salvo por este pequeño detalle que no impide el desarrollo ni es demasiado molesto el juego es perfectamente jugable.


En definitiva **This is the police es un buen juego**, no aquella maravilla que me engancho la primera vez que lo jugué. Pero si un juego al que le daría una oportunidad con una buena rebaja en el precio. Por supuesto que Jack Boyd ha hecho que me quede, pero han estado los pequeños detalles de la jugabilidad y esa dificultad un poco tramposa, que espero que se mejore y perfeccione la formula en la segunda entrega.


Si quieres darle una oportunidad, tienes This is the police en [Humble Bundle](https://www.humblebundle.com/store/this-is-the-police?partner=jugandoenlinux) (enlace patrocinado) o en su [página de Steam](https://store.steampowered.com/app/443810/This_Is_the_Police/).


**Título:** This is the police


**Género:** Estrategia / Aventura


**Publicado:** 2/8/2016

