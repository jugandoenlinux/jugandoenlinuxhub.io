---
author: Pato
category: Apt-Get Update
date: 2017-05-26 16:53:04
excerpt: <p>Volvemos a las buenas costumbres, que no se nos han olvidado</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e2ead9c1349a48396463990fe7b55a21.webp
joomla_id: 345
joomla_url: apt-get-update-verdun-geneshift-planet-nomads-a-hole-new-world-hypernova-day-of-infamy-starcrawlers
layout: post
title: Apt-Get Update Verdun & Geneshift & Planet Nomads & A Hole New World & Hypernova
  & Day of Infamy & StarCrawlers...
---
Volvemos a las buenas costumbres, que no se nos han olvidado

Hace ya un tiempo que no hacemos un Apt-Get Update, y como nunca es tarde, como dice el refrán, allá vamos a repasar lo que se nos queda sin tocar:


**Desde [gamingonlinux.com](https://www.gamingonlinux.com/):**


- El juego FPS **'Verdun'** ambientado en la I Guerra Mundial recibe una expansión llamada 'Tannenberg' que puede jugarse también de forma independiente, llevando el frente hasta el este europeo. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/ww1-fps-verdun-to-get-a-standlone-expansion-tannenberg-that-sees-you-going-east.9723).


- **'Geneshift'** es un juego de acción brutal en vista cenital con aspecto "cartoon"y está ya disponible en acceso anticipado. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/geneshift-the-brutal-top-down-shooter-with-vehicles-has-released-into-early-access.9718).


- **'Planet Nomads'** el juego "sandbox" de supervivencia llega a Linux/SteamOS en acceso anticipado. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/planet-nomads-officially-lands-into-early-access.9733).


**Desde [linuxgameconsortium.com](https://linuxgameconsortium.com):**


- **'A Hole New World'** es un desafiante juego de plataformas del que [ya os hablamos hace un tiempo](https://www.jugandoenlinux.com/index.php/homepage/generos/accion/item/447-a-hole-new-world-llegara-a-linux-steamos-el-proximo-viernes-dia-19-de-mayo) en Jugando en Linux, ahora ya está disponible. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/hole-new-world-available-steam-linux-games-52927/).


- **'Stardrift Nomads'** es un juego de acción espacial 2D de los de manejo por "doble stick" que puede ser interesante. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/stardrift-nomads-gets-full-linux-support-steam-games-52884/).


- **'Hypernova Espcape from Hadea'** es un RTS (estrategia en tiempo real) con una pinta más que interesante que está en campaña en Indiegogo y Greenlight. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/hypernova-rts-now-indiegogo-52754/).


- Llegan los autralianos a **'Day of Infamy'** gracias a su última actualización. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/day-of-infamy-gets-aussie-update-linux-steam-games-52723/).


- El "Dungeon crawler" ambientado en el espacio **'StarCrawlers'** ya está disponible en Linux/SteamOS. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/starcrawlers-dungeon-crawler-launches-linux-games-steam-52985/).


- **'Gold Rush! 2'**, un juego de estrategia y aventuras ya está disponible en Linux/SteamOS. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/gold-rush-2-officially-launch-linux-support-steam-games-52933/).


 


Vamos con los vídeos de la semana:  
Tannenberg:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/sE32n9kUddU" width="640"></iframe></div>


Geneshift:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/TOFMNI4Wh88" width="640"></iframe></div>


Planet Nomads:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/mU07yeMZ-JA" width="640"></iframe></div>


A Hole New World:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/qQNRU5UD6aM" width="640"></iframe></div>


Stardrift Nomads:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/1aOIaZIzAeo" width="640"></iframe></div>


Hypernova:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/0pdw1MmIx6k" width="640"></iframe></div>


Day of Infamy Aussie Update:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/OEkkgarJ5Xs" width="640"></iframe></div>


StarCrawlers:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/fiBLsm5haqs" width="640"></iframe></div>


Gold Rush! 2


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/bduVhMTRgb8" width="640"></iframe></div>


Esto es todo por esta semana... La semana que viene tendremos más y más bonito. ¿Qué te ha parecido?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

