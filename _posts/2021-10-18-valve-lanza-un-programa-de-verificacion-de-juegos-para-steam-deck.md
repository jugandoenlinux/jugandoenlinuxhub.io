---
author: Pato
category: Hardware
date: 2021-10-18 19:46:23
excerpt: "<p>El programa verificar\xE1 toda la biblioteca de Steam para indicarte\
  \ si los juegos pueden correr en la portatil de Valve</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/SteamDeckVerified.webp
joomla_id: 1378
joomla_url: valve-lanza-un-programa-de-verificacion-de-juegos-para-steam-deck
layout: post
tags:
- steam
- valve
- steam-deck
title: "Valve lanza el programa de verificaci\xF3n de videojuegos \"Deck Verified\"\
  \ para Steam Deck"
---
El programa verificará toda la biblioteca de Steam para indicarte si los juegos pueden correr en la portatil de Valve


Era un paso lógico el que acaban de dar los chicos de Valve. Según han anunciado desde **la cuenta oficial de Steam Deck** [en Twitter](https://twitter.com/OnDeck/status/1450162541812875264), la **Steam Deck** saldrá con una -muy- amplia lista de juegos compatibles, entre los que son nativos para Linux/SteamOS3, y los que se podrán ejecutar vía Proton. Sin embargo, y aunque Valve haga todo lo posible por que toda la biblioteca sea jugable, hay que recordar que **habrán ciertos juegos que no puedan ejecutarse con garantías en la Steam Deck**, al menos de salida.


Por poner un ejemplo, los juegos que implementen un **DRM incompatible** de forma nativa con Linux ni con Wine/Proton será muy difícil que puedan ejecutarse correctamente, o los ya archi-famosos **sistemas anti-cheats** que no tengan soporte para Wine/Proton o juegos que aun teniendo la posibilidad de activarlos, opten por no hacerlo. 


Tampoco hay que olvidar los juegos que utilicen ciertos "**middlewares**" que no son compatibles, y que salvo milagro no se podrán ejecutar en la máquina. Es por esto que **Valve ha implementado un programa de verificación** que pretende revisar toda la biblioteca de Steam para que cuando un usuario busque un juego en Steam mediante Steam Deck vea de un vistazo si ese juego se podrá ejecutar, atendiendo a diversos niveles de compatibilidad.


Así es como nos lo anuncian desde Steam:



>
>  *Con Steam Deck traemos los juegos de Steam a un nuevo formato: un gaming PC portatil. Colocar un PC completo en este nuevo diseño trae muchas preguntas, y una de las más importantes es: "¿Cómo funcionarán mis juegos ahí?". Creemos que los jugadores necesitan saber cómo funcionará cada juego antes de comprarlo o iniciarlo. Por ello, creamos una forma de ver rápidamente el funcionamiento de un juego en este nuevo dispositivo.*
>
> *Hoy, les presentamos "Deck Verified", un programa de análisis de compatibilidad con Steam Deck. Creamos un video y documentación detallada para explicarte todo lo que necesitas saber sobre la compatibilidad de Steam Deck como desarrollador o editor.*
>
>
>


 A continuación, el vídeo explicativo:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/a8tNvhwkth8" title="YouTube video player" width="560"></iframe></div>


 


 Puedes ver el anuncio en la web de Steam [en este enlace](https://steamcommunity.com/groups/steamworks/announcements/detail/4820532832261272816)


En cuanto a los niveles de compatibilidad, **habrá cuatro niveles** queestarán visiblesen las páginas de los juegos con iconos descriptivos, y que clasificarán los juegos según sea: 


**"Verificado"**: El juego pasa todas las comprobaciones de compatibilidad. No se requiere ningúna configuración para que los usuarios accedan a todas las funciones del juego.


**"Jugable"**: El juego funciona en Deck, pero puede requerir configuración adicional por parte del usuario. (Ejemplo: seleccionar manualmente una configuración de controlador de comunidad, requerir que el usuario utilice el teclado en pantalla o requerir que el usuario use la pantalla táctil para navegar por un lanzador).


**"no soportado"**: El juego no funciona en Deck debido a la incompatibilidad con Proton o componentes de hardware específicos.


**"Desconocido"**: El juego no ha completado el proceso de revisión de compatibilidad. La información sobre cómo se jugará el juego en Deck no está disponible.


![Steam Deck Verified1](https://steamcdn-a.akamaihd.net/steamcommunity/public/images/steamworks_docs/spanish/store_Tabs.jpg)


*Así es como se verán los iconos en los banners de los juegos*


Teniendo en cuenta estos criterios, aún así **siempre podrás intentar ejecutar los juegos**en la Steam Deck, eso sí bajo tu cuenta y riesgo.


Por otra parte, en la web de Steamworks de Steam Verified se explican los criterios que se examinarán de cara a los desarrolladores de videojuegos, a saber:


"**Parámetros de entrada**", donde se analizarán los soportes de texto, mandos, y "mandos virtuales".


"**Presentación en pantalla**", donde se analizarán el soporte a la resolución de la máquina, configuración específica por defecto para Deck, y legibilidad del texto.


"**Fluidez",**donde se verificará que el juego no presente ningún mensaje al usuario de que el juego tiene alguna incompatibilidad con el sistema o el hardware de la Deck, y que los lanzadores de juegos externos cumplan con los requisitos anteriores. (Aunque ya avisan de que desde Valve desaconsejan vívamente el utilizar cualquier tipo de lanzador para lanzar los juegos).


![Steam Deck Verified2](https://steamcdn-a.akamaihd.net/steamcommunity/public/images/steamworks_docs/spanish/TF2.jpg)


*Una muestra de la información que veremos para saber la compatibilidad de los juegos*

Si tienes interés, **Valve ha añadido un apartado para la verificación de los juegos en la propia web de la [Steam Deck](https://www.steamdeck.com/es/verified)** y ademas tienes toda la información e[n la página de Steamworks](https://partner.steamgames.com/doc/steamdeck/compat) del programa de desarrolladores del PC portatil.

Un último apunte que no debe pasarse por alto. Si te gusta estar informado sobre los juegos compatibles con la portatil de Valve y ver qué tal se mueven, es aconsejable seguir [la cuenta de Twitter de Steam Deck](https://twitter.com/OnDeck/status/1450162541812875264) donde van mostrando información y vídeos sobre la máquina ejecutando juegos.


¿Qué te parece la planificación que está llevando a cabo Valve con Steam Deck? ¿Te parece acertada la campaña que está haciendo para promocionar la máquina?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

