---
author: Serjor
category: "Acci\xF3n"
date: 2018-06-30 09:29:13
excerpt: "<p>Hasta la llegada de Serious Sam 4, podemos jugar con Sam en este trepidante\
  \ t\xEDtulo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d18d1528ee4c2188221135de26d79754.webp
joomla_id: 790
joomla_url: i-hate-running-backwards-disponible-para-gnu-linux
layout: post
tags:
- croteam
- i-hate-running-backwards
- binx-interactive
title: I Hate Running Backwards disponible para GNU/Linux
---
Hasta la llegada de Serious Sam 4, podemos jugar con Sam en este trepidante título

Gracias a [phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Croteam-IHRB-Linux) nos enteramos de que ya tenemos [disponible para GNU/Linux](http://www.croteam.com/ihrb-available-linux/) el juego "I hate running backwards", un juego desarrollado por Binx Interactive y producido por Croteam, en el que llevaremos a Sam, de la saga Serious Sam, en un juego muy peculiar, que mezcla voxels, escenarios generados de manera procedural, vista isométrica, y acción, mucha acción, aunque quizás la única pega es que su modo multijugador es local y no tiene soporte para jugar por internet.


No obstante, el juego tiene muy buena pinta y parece entretenido, y además ahora está de rebajas en la tienda de Steam, así que si te convence no dudes en hacerte con él.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/RrVUenKXyBM" style="border: 0px;" width="560"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/575820/" style="border: 0px;" width="646"></iframe></div>


Y tú, ¿también odias correr hacia atrás? Cuéntanos qué te parece este juego en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

