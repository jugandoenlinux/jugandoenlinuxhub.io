---
author: Serjor
category: "An\xE1lisis"
date: 2018-01-29 22:06:45
excerpt: "<p>Ad\xE9ntrate en un viaje por la mente humana, donde lo que veas te perturbar\xE1\
  </p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e88c403143ae0a81dde94b0909e552aa.webp
joomla_id: 622
joomla_url: analizamos-observer
layout: post
tags:
- indie
- terror
- aspyr-media
- observer
- bloober-team
title: Analizamos >Observer_
---
Adéntrate en un viaje por la mente humana, donde lo que veas te perturbará

*Disclaimer* La clave del juego ha sido proporcionada por Aspyr.


Hace unos meses [tuvimos la oportunidad](index.php/homepage/generos/terror/item/637-noche-de-halloween-en-jugando-en-linux-streaming-y-sorteo) de hacer un "unboxing" del >observer_, un juego desarrollado por [Bloober Team](https://blooberteam.com/), quienes ya nos trajeron anteriormente el conocido Layers of Fear.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/X7khnBsOwRA" style="border: 0px;" width="560"></iframe></div>


En el momento de escribir este artículo aún no he tenido la oportunidad de probar este Layers of Fear, aunque como ya os [anunciamos](index.php/foro/ofertas/64-layers-of-fear-gratis-en-humblebundle-hasta-el-dia-20-de-diciembre) en el foro de ofertas, estuvo disponible de manera gratuita. El caso es que al no haberlo jugado aún voy a hacer una serie de suposiciones que puede que no sean del todo exactas, y es que por lo que he podido ver del Layers of Fear, este juego nos presenta una mansión de la que tendremos que escapar o sobrevivir explorando dicha mansión, y presumiblemente, huyendo de aquello que nos aceche. El caso es que este tipo de juegos, conocidos medio en broma medio en serio como Walking simulators, pretenden crear una sensación opresiva en el jugador, privándole de medios para sobrevivir, como armas, y que de esta manera se vea obligado a huir o esconderse para poder llegar hasta el final, algo así como una versión más trabajada del clásico "Slender".


El caso es que este >observer_ es aparentemente una evolución del Layers of Fear, en el cuál nos encontramos un juego en el que el estudio polaco va un paso más allá del terror opresivo y busca llevar al jugador a través de un viaje por la mente humana.


Y esto es de manera literal, ya que encarnamos a Daniel Lazarski, miembro de la policía de Cracovia, más concretamente, él forma parte de una unidad de élite conocidos como "Obsever", quienes tienen la capacidad de conectarse al cerebro de las personas y vivir sus últimos recuerdos, así cómo disponer de varios implantes que nos permiten ver en la oscuridad y analizar lo que vemos en busca de pistas, como ADN o información concreta de los objetos que nos encontramos.


Al comienzo del juego recibimos un mensaje de nuestro hijo que nos obliga a ir a un edificio de viviendas de uno de los barrios más pobres de la ciudad y es en este edificio de viviendas donde transcurre todo el juego.


Sin pretender hacer ningún tipo de spoiler, siendo el protagonista un policía con la capacidad de conectarse a los recuerdos de la gente, es bastante fácil intuir que una vez que lleguemos allí las circunstancias hacen que pase algo que nos obligue a  investigar qué es lo que ha pasado.


Y esto es todo lo que se puede contar de la trama sin correr el riesgo de desvelar demasiado.


Al margen del planteamiento narrativo, >observer_ nos lleva a un futuro cyberpunk en el que la estética nos recordará tremendamente a Blade Runner, cosa que sinceramente, se agradece. El apartado estético es, al menos para mí, muy bueno, ya que mezcla tonos muy vivos con una oscuridad deprimente que contrasta estupendamente, y a pesar de que toda la tecnología que nos rodea es imposible a día de hoy, tiene un toque retro y anticuado que hace que parezca obsoleta incluso para nuestros tiempos. Para mi gusto, lo mejor del juego.


![observer1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/AnalisisObserver/observer1.webp)


La música y los efectos sonoros también son destacables y juegan un papel muy importante. Quizás lo más justo sea decir que son apropiados y que encajan bien en todo momento, y aunque en mi caso gana el aspecto visual, no quita que el sonido no sea la clave que crea la ambientación necesaria para meterse en el juego.


Tristemente, y aquí quiero quiero dejar muy claro que esto es una opinión más que una análisis objetivo, todo lo bueno del juego termina aquí, en su planteamiento y en su puesta en escena, porque en su ejecución falla, y falla por muchos lados.


No dispongo de una instalación con Windows para comparar su rendimiento, pero en GNU/Linux, es horrible. Lo he jugado en un i5 7600 (antes de los parches para Meldown y Spectre) con una GTX 1060 6GB en una Ubuntu 17.10, y el juego rinde mal. Es bastante decepcionante ver que el Unreal Engine 4 tiene problemas de rendimiento cuando, quitando el colorido, no hay nada en pantalla que a priori requiera de una máquina potente, pero parece ser que sí.


Y no tan solo es que el juego tenga problemas con llegar y mantener los 60fps, sino que además en mi caso marea. Llevo tiempo jugando a FPSs y es raro que un juego me genere nauseas, pero este >observer_ lo consigue, y no he podido tener sesiones de juego largas.


![observer7](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/AnalisisObserver/observer7.webp)


No obstante, es casi de agradecer, porque la historia no atrae. Afortunadamente el juego es corto, pero dura lo suficiente para aburrir en cada sesión, y para que avanzar sea más un acto de fe que un impulso creado por una historia absorbente. Sí que es cierto que en un momento dado del juego parece que estamos ante un survival horror donde tendremos que avanzar usando el sigilo, pero nada más lejos de la realidad. >observer_ se basa en qué hay en nuestras mentes, y esto hace que cuando nos conectamos a otros veamos el mundo con sus ojos donde las locuras y fantasías se mezclan, y aunque en el tintero esto puede sonar muy bien, el juego abusa de la idea y nos propone puzles que tendremos que resolver muchas veces porque sí, otras por aburrimiento.


![observer4](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/AnalisisObserver/observer4.webp)


Y es que desde lo que entiendo que es Layers of Fear, da la sensación de que este >observer_ quiere ser una versión diferente de un mismo juego. Situaciones imposibles para la mente humana que nos perturben y asusten para mantenernos en un estado de alerta constante. No sé si Layers of Fear da la talla en este sentido, en mi opinión, y para mi gusto, este juego no.


Pero debo ser el único, porque las valoraciones de este juego en general son muy positivas, así que o este juego no es para mi, o no lo he sabido entender, pero si observáis el gameplay que hicimos la noche de Halloween, creo que se nota hasta en la cara la sensación de aburrimiento.


<div class="resp-iframe"><iframe height="102" src="https://opencritic.com/game/4639/score" style="border: 0px;"></iframe></div>


Eso sí, me gustaría destacar el minijuego que nos encontraremos en los ordenadores que hallaremos a lo largo de la investigación, y es que dicho minijuego, muy ochentero, es bastante más entretenido que el juego principal, y desafortunadamente, no he encontrado todos los ordenadores necesarios para terminarlo.


![observer5](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/AnalisisObserver/observer5.webp)


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/514900/" style="border: 0px;" width="646"></iframe></div>


Y tú, ¿has podido probar este >observer_? Cuéntanos qué te parece en los comentarios o en los grupos de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

