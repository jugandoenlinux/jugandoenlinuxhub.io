---
author: Pato
category: Software
date: 2022-01-26 20:59:53
excerpt: "<p>El lanzador open source de la tienda de Epic evoluciona a buen ritmo</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HeroicGamesLauncher/HGLRayleigh.webp
joomla_id: 1415
joomla_url: heroic-games-launcher-se-actualiza-con-nuevas-caracteristicas
layout: post
tags:
- heroic-games-launcher
title: "Heroic Games Launcher se actualiza con nuevas caracter\xEDsticas"
---
El lanzador open source de la tienda de Epic evoluciona a buen ritmo


Heroic Games Launcher, el lanzador-frontend de la tienda de Epic Games continúa su evolución. Ayer mismo fue actualizado a la **versión 2.1.0 Rayleigh** trayendo unas cuantas novedades interesantes. Entre ellas destaca la posibilidad de **navegar por Heroic mediante gamepads** como los de Xbox o Playstation, o algunos genéricos (¿a tiempo para la Steam Deck?), la inclusión de un teclado virtual o un menú de descargas de Wine para poder descargar distintas versiones de Wine-GE o Proton-GE:


* [General] Ahora es posible navegar por Heroic usando un Gamepad. Admite Xbox One, controladores de Playstation y algunos genéricos por ahora. También puede usar un teclado virtual para la entrada de búsquedas por @arielj
* [General] Heroic ahora recordará el tamaño de la ventana y la posición por @Nocccer
* [General] Compatibilidad con el acceso directo Ctrl+F y CMD+F para enfocar la barra de búsqueda por @arielj
* [Linux] Agregada la función Wine Downloader para facilitar la descarga de nuevas versiones de Wine-GE y Proton-GE (Beta) por @Nocccer y @flavioislima
* [General] Verifica el estado de los servidores Epic y muestra mensajes de advertencia en la instalación, actualización, lanzamiento, etc. por @flavioislima
* [General] Heroic ahora almacenará un registro para cada sesión en una carpeta llamada Registros en la carpeta de configuración de Heroic por @Nocccer
* [General] Agreguado el botón borrar (x) a la barra de búsqueda por @TabulareJarl8
* [General] Agregadas más advertencias y diálogos de error por @flavioislima
* [General] Mejoras y correcciones de Webview por @wiryfuture en #836
* [General] Actualizaciones y correcciones de la interfaz de usuario y otras optimizaciones.
* [General] Actualizaciones de traducciones por @weblate
* [General] Versión de Legendary actualizada a v0.20.25


Para ver todos los cambios, otras versiones y descargar Heroic Games Launcher puedes visitar su web de github donde se desarrolla el proyecto [en este enlace](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher/releases/tag/v2.1.0).

