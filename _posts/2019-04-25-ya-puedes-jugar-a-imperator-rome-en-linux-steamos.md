---
author: leillo1975
category: Estrategia
date: 2019-04-25 18:46:42
excerpt: <p class="ProfileHeaderCard-screenname u-inlineBlock u-dir" dir="ltr"><span
  class="username u-dir" dir="ltr">@PdxInteractive estrena el juego en todas las plataformas,
  incluida la nuestra</span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/788ff92e9a59aeb146ed681b6c832028.webp
joomla_id: 1030
joomla_url: ya-puedes-jugar-a-imperator-rome-en-linux-steamos
layout: post
tags:
- estrategia
- paradox
- imperator-rome
title: 'Ya puedes jugar a "Imperator: Rome" en Linux/SteamOS.'
---
@PdxInteractive estrena el juego en todas las plataformas, incluida la nuestra

Mucho se venía hablando de este juego desde hace tiempo, y es que no es para menos, ya que sus creadores son el estudio responsable de las sagas Europa Universalis, Crusader Kings, Hearts of Iron o el fántástico Stellaris, juegos que nos han regalado horas y horas de diversión sin fin a todos los que gustan de la **estrategia con mayúsculas**. Si recordais, hace un año por estas fechas [os anunciábamos el desarrollo de este título](index.php/homepage/generos/estrategia/8-estrategia/866-lo-nuevo-de-paradox-interactive-imperator-rome-llegara-a-linux) donde os confirmábamos  que tendría soporte como suele ser habitual en los juegos de la editora. Hoy, finalmente ha llegado tal y como estaba previsto, y en JugandoEnLinux estamos una vez más dispuestos para anunciaroslo a bombo y platillo. Aquí teneis el **retweet** que hacía la editora [(Paradox Interactive](https://www.paradoxplaza.com/)) del anuncio oficial:



> 
> Dive into the epic conquests and geopolitical drama of the classical world in Imperator: Rome – the latest grand strategy game from Paradox Development Studio, available now!  
> BUY ON STEAM: <https://t.co/2OrjGCv6Xn>   
> BUY ON THE STORE: <https://t.co/oSTUeXqj8n> [pic.twitter.com/jDA5iwrEOE](https://t.co/jDA5iwrEOE)
> 
> 
> — Imperator (@gameimperator) [25 de abril de 2019](https://twitter.com/gameimperator/status/1121444383440670723?ref_src=twsrc%5Etfw)



 En "**Imperator: Rome**" podremos encontrar, según la descripción oficial, los siguientes contenidos y características:


Alexander. Hannibal. César. Estos grandes hombres y docenas como ellos dieron forma al destino de un continente. Poderosos reyes, generales inteligentes y posibles dioses dejaron su huella en el Mediterráneo antiguo. Alrededor de este mar, naciones muy unidas pusieron a prueba su temple y virtud en feroces combates, su legado cultural y político que ahora es inseparable de lo que entendemos como la civilización occidental. Pero nada estaba garantizado. ¿Puedes cambiar el curso de la historia en Imperator: Rome?  
  
**Imperator: Rome** es el nuevo gran título de estrategia de Paradox Development Studio. Situado en los tumultuosos siglos desde los Imperios sucesores de Alejandro en el Este hasta la fundación del Imperio Romano, **Imperator: Roma** te invita a revivir la pompa y los desafíos de la construcción del imperio en la era clásica. Maneje su población, esté atento a la traición y mantenga la fe en sus dioses.  
  
![](/https://steamcdn-a.akamaihd.net/steam/apps/859580/extras/title_character_management.webp)  
Un mundo vivo de personajes con diferentes habilidades y rasgos que cambiarán con el tiempo. Ellos dirigirán su nación, gobernarán sus provincias y comandarán sus ejércitos y flotas. También presentamos nuestro nuevo arte de carácter más humano.  
  
![](/https://steamcdn-a.akamaihd.net/steam/apps/859580/extras/title_diverse_populations.webp)  
Ciudadanos, hombres libres, hombres de tribus y esclavos - cada población con su propia cultura y religión. Ya sea que llenen sus ejércitos, sus arcas o sus colonias, mantenga un ojo en su felicidad - su éxito depende de su satisfacción.  
  
![](/https://steamcdn-a.akamaihd.net/steam/apps/859580/extras/title_battle_tactics.webp)  
Elige tu enfoque antes de la batalla para contrarrestar las estratagemas de tus enemigos.  
  
![](/https://steamcdn-a.akamaihd.net/steam/apps/859580/extras/title_military_traditions.webp)  
Cada cultura tiene una forma única de hacer la guerra. Los romanos y los celtas tienen diferentes opciones disponibles para ellos. Desbloquea bonos, habilidades y unidades únicas.  
  
![](/https://steamcdn-a.akamaihd.net/steam/apps/859580/extras/title_different_government_types.webp)  
Dirige el senado en una república, mantén tu corte unida en una monarquía, responde a los clanes en un sistema tribal.  
  
![](/https://steamcdn-a.akamaihd.net/steam/apps/859580/extras/title_barbarian_rebellions.webp)  
Los bárbaros que emigran pueden saquear o asentar sus mejores tierras, mientras que los gobernadores o generales desleales pueden volverse contra usted, ¡llevándose a sus ejércitos con ellos!  
  
![](/https://steamcdn-a.akamaihd.net/steam/apps/859580/extras/title_trade.webp)  
Los bienes proporcionan bonos a su provincia de origen. ¿Aprovechará las reservas para la fuerza local o comercializará los bienes sobrantes para repartir la riqueza?  
  
![](/https://steamcdn-a.akamaihd.net/steam/apps/859580/extras/title_provincinal_improvement.webp)  
Invierte en edificios, carreteras y defensas para hacer tu reino más fuerte y rico.




---


Como veis el juego promete mucho, y **siendo quienes son sus creadores es una garantía** de que a bien seguro será un juego de calidad. Si quereis disfrutar de "Imperator: Rome", al menos debeis cumplir los siguientes requisitos, que como veis no son nada exagerados:


+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 16.04
+ **Procesador:** Intel® iCore™ i3-550 or AMD® Phenom II X6 1055T
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** Nvidia® GeForce™ GTX 460 or AMD® Radeon™ HD 6970


Si quereis comprar **Imperator: Rome**, podeis adquirirlo en la tienda de Humble Store en su [versión normal,](https://www.humblebundle.com/store/imperator-rome?partner=jugandoenlinux) o la [Deluxe](https://www.humblebundle.com/store/imperator-rome-deluxe-edition?partner=jugandoenlinux), que incluye entre otras muchas cosas el "**Hellenistic World Flavor Pack**" que os permitirá jugar con los ejercitos de Alejandro Magno, y por supuesto un montón de contenido adicional. Aquí podeis ver el video del lanzamiento:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/y7rLbP0_0dg" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Y tu, disfrutas de los juegos de Paradox? ¿Qué te parece este Imperator: Rome? Déjanos tus impresiones sobre él en los comentarios, o charla sobre el juego en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

