---
author: leillo1975
category: "Acci\xF3n"
date: 2019-10-15 14:14:08
excerpt: <p>@feralgames acaba de anuciar su fecha de lanzamiento en nuestro sistema.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b2891bff8bec583930a24fd93c9066ee.webp
joomla_id: 1120
joomla_url: shadow-of-the-tomb-raider-desembarcara-pronto-en-linux-steamos
layout: post
tags:
- feral-interactive
- square-enix
- shadow-of-the-tomb-raider
- eidos
title: "\"Shadow of the Tomb Raider\" desembarcar\xE1 pronto en Linux/SteamOS."
---
@feralgames acaba de anuciar su fecha de lanzamiento en nuestro sistema.

Siendo sinceros, ya estábamos un tanto preocupados por el desarrollo de este port tan esperado. Si haceis memoria, la última vez que hablamos de este juego [fué hace casi un año](index.php/homepage/generos/accion/5-accion/1027-shadow-of-the-tomb-raider-llegara-a-linux-steamos-el-proximo-ano), y desde entonces las noticias sobre el juego han sido nulas. Menos mal que Feral Interactive ha puesto fin a las especulaciones y finalmente ha comunicado la fecha definitiva del lanzamiento de este port, concretándola para el **5 de Noviembre**, conjuntamente con la versión de MacOS. El anuncio nos llegaba de Feral a través del correo electrónico, y también de sus cuentas de Redes sociales, como este tweet:



> 
> Shadow of the Tomb Raider Definitive Edition is destined for macOS and Linux on 5 November.  
>   
> In this gripping action-adventure, Lara must save the world from an apocalypse and forge herself into the Tomb Raider.  
>   
> Leap into the minisite for action shots: <https://t.co/7PltE0oclH> [pic.twitter.com/CE4mnY3wFN](https://t.co/CE4mnY3wFN)
> 
> 
> — Feral Interactive (@feralgames) [October 15, 2019](https://twitter.com/feralgames/status/1184095836738859009?ref_src=twsrc%5Etfw)



 Y será en esa fecha cuando al fin podamos disfrutar de manera nativa del éxitoso título de **Square Enix** y **Eidos-Montreal**, portado a nuestro sistema al igual que Tomb Raider (2013), y [Rise of the Tomb Raider](index.php/homepage/generos/accion/5-accion/835-rise-of-tomb-raider-20-aniversario-disponible-en-gnu-linux-steamos), publicado el año pasado. En esta ocasión Lara Croft correrá, saltará y luchará para salvar al mundo de un apocalipsis maya, viajando desde la ciudad Mexicana de Cozumel hasta la selva Peruana, navegando por complejas cavernas submarinas, atravesando paisajes espectaculares y explorando tumbas llenas de trampas letales.


En **Shadow of the Tomb Raider Definitive Edition** encontraremos además del juego base, las **siete tumbas de desafío** (DLC's) y **todas las armas, trajes y habilidades descargables**. Por supuesto, y como viene siendo costubre en los últimos ports de Feral, se contará con la potencia de **Vulkan** como API gráfica, por lo que el rendimiento es de esperar que sea bueno. En el [anuncio en la página web de Feral](https://www.feralinteractive.com/en/news/1005/) y en su [minisitio](https://www.feralinteractive.com/en/games/shadowofthetombraider/) no encontramos ninguna noticia sobre los requisitos mínimos para poder disfrutar de este juego, pero tan pronto dispongamos de esta información (o cualquier otra), os lo haremos saber lo antes posible, por lo que os recomendamos que esteis atentos a nuestra web, o nuestras redes sociales ([Twitter](https://www.feralinteractive.com/en/news/1005/) y [Mastodon](https://mastodon.social/@jugandoenlinux)).


El juego llegará con un precio de 59.99€ (Definitive Edition). Como siempre os dejamos con un video del anuncio del juego para Linux:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/bugahtf9Sdg" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Seguro que estabais deseando que llegase, ¿verdad? Cuentanos tus impresiones, opiniones o lo que quieras sobre Shadow of the Tomb Raider en los comentarios, deja tus mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

