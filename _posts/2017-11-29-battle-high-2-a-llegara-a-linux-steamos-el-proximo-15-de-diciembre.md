---
author: Pato
category: "Acci\xF3n"
date: 2017-11-29 17:45:30
excerpt: "<p>El juego de lucha ha sido portado de la versi\xF3n de Ouya tras su paso\
  \ por consolas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e2dc343852dcc66e453f7f9901577ec5.webp
joomla_id: 557
joomla_url: battle-high-2-a-llegara-a-linux-steamos-el-proximo-15-de-diciembre
layout: post
tags:
- accion
- proximamente
- lucha
title: "'Battle High 2 A+' llegar\xE1 a Linux/SteamOS el pr\xF3ximo 15 de Diciembre"
---
El juego de lucha ha sido portado de la versión de Ouya tras su paso por consolas

Buenas noticias para los que gustan de los juegos de lucha al estilo retro. Battle High 2A+ llega para contentar a los que buscan títulos de lucha en Linux/SteamOS tras su paso por consolas. Los juegos de lucha no son abundantes en nuestro sistema favorito y no está de más tener más títulos donde elegir.


Desarrollado originalmente para Ouya, ha sido evolucionado para ofrecer mejor balanceo, gráficos y solucionar algunos bugs.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Px6T_EHr03k" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Entre las características principales, tendremos:


* 15 Personajes jugables
* Modos Arcade y Challenge
* 2 pistas de música por personaje
* Multijugador local


¿Lucharás en las calles de San Bruno High?


Battle High 2A+ no estará disponible en español, pero siendo el tipo de juego que es no creo que sea mucho problema. Si estás interesado podrás encontrarlo en su página de Steam el próximo 15 de Diciembre:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/755590/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

