---
author: leillo1975
category: Software
date: 2019-06-07 07:24:29
excerpt: "<p>La herramienta se actualiza a la versi\xF3n 4.2-7 para corregir problemas\
  \ de audio</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6df901b16372d215be01b1ccfd4892aa.webp
joomla_id: 1060
joomla_url: proton-recibe-una-actualizacion-la-4-2-6
layout: post
tags:
- wine
- valve
- steam-play
- dxvk
- proton
- faudio
title: "Proton recibe una actualizaci\xF3n, la 4.2-6 (ACTUALIZACI\xD3N)"
---
La herramienta se actualiza a la versión 4.2-7 para corregir problemas de audio

**ACTUALIZACIÓN 11-6-19:** Proton acaba de recibir una pequeña actualización (4.2-7) que arregla problemas con el rendimiento y tambié la regresión de sonido que afectaba a algunos juegos, como Wolfenstein: El Nuevo Orden. Como veis simplemente se trata de una pequeña revisión de la versión anterior, pero que da un pasito más para mejorar este fenomenal software.




---


**NOTICIA ORIGINAL:**  
Hace unos 15 días recibimos la [anterior versión](index.php/homepage/generos/software/12-software/1170-proton-se-actualiza-a-la-version-4-2-5) de esta fantástica herramienta de compatibilidad basada principalmente en Wine y DXVK. Esta madrugada (menudas horas) han lanzado una nueva revisión, la [4.2-6](https://github.com/ValveSoftware/Proton/releases/tag/proton-4.2-6), que no trae mejoras muy notables, pero que supone un nuevo avance en aras a mejorar la experiencia con algunos juegos. Entre las principales novedades que incluye encontraremos las siguientes:


*-Más correcciones para la nueva **API de red** de Steam. El modo multijugador online de **Hat In Time** ya debería estar operativo.*  
 *-**DXVK 1.2.1** ha sido reconstruido con un compilador moderno. Esto debería aumentar el rendimiento, especialmente en los juegos de **32 bits**. (Nota: si construyes Proton tú mismo en Vagrant, querrás hacer una provisión limpia y vagrant para aprovechar el nuevo compilador; o simplemente destruir y recrear la VM.)*  
 *-Actualizado **FAudio a 19.06**.*  
 *-Corrección para más juegos en **localizaciones no inglesas**, como GRID.*  
 *-Arreglado el **rumble del mando en algunos juegos**, incluyendo el **Team Sonic Racing**.*  
 *-Mejora en el **render de fuentes** en **SpellForce 3**.*


Como veis, un pasito más de este gran proyecto que es Steam Play - Proton. Nosotros ahora nos vamos a probar unos cuantos juegos. Vosotros podeis dejarnos vuestras impresiones sobre esta version en los comentarios o charlar sobre el tema en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

