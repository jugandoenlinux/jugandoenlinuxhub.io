---
author: Serjor
category: Puzzles
date: 2018-09-02 16:30:57
excerpt: <p>Un party game muy libre</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0afa57c41887ce47355dc00226c6156f.webp
joomla_id: 850
joomla_url: super-tux-party-un-party-game-open-source
layout: post
tags:
- open-source
- itchio
- godot
- super-tux-party
- blender
- krita
- gimp
title: Super Tux Party, un party game open source
---
Un party game muy libre

Gracias a [reddit](https://www.reddit.com/r/linux_gaming/comments/9c5z2m/super_tux_party_an_opensource_party_game_demo_v01/) nos enteramos de que ha sido publicada la beta de Super Tux Party. En palabras de los propios creadores:



> 
> Un juego tipo party open source, cuyo objetivo es replicar la experiencia de juegos como Mario Party, pero siendo software libre
> 
> 
> 


Además, no tan solo el juego es de código abierto, sino que está hecho con Godot, Blender, Gimp y Krita, toda una suit de herramientas open source, así que un 2x1 perfecto.


Aún está en fase beta como decíamos al principio, y los desarrolladores nos piden ideas sobre minijuegos para añadir, así que si se te ocurre alguna, puedes aportar tu pequeño granito a este juego.


Puedes descargar el juego en [su página de itch.io](https://anti.itch.io/super-tux-party) y el código fuente en su página de [gitlab](https://gitlab.com/SuperTuxParty/SuperTuxParty)


Y tú, ¿jugarás a este Super Tux Party en la próxima cena con amigos? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

