---
author: Serjor
category: "Simulaci\xF3n"
date: 2017-12-27 18:20:46
excerpt: <p>Gracias a la comunidad, los poseedores de un volante Logitech pueden hacer
  uso del FF</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9e02b4257c05e19434bd28a1f3ded3e5.webp
joomla_id: 589
joomla_url: los-volantes-logitech-consiguen-soporte-nativo-para-el-force-feedback-en-euro-american-truck-simulator
layout: post
tags:
- american-truck-simulator
- scs-software
- euro-truck-simulator-2
- logitech
title: Los volantes Logitech consiguen soporte "nativo" para el force feedback en
  Euro/American Truck Simulator
---
Gracias a la comunidad, los poseedores de un volante Logitech pueden hacer uso del FF

Grandísima noticia que hará las delicias de los poseedores de un volante Logitech, y es que leemos en [PlayingTux](https://playingtux.com/articles/euro-truck-simulator-2-american-truck-simulator-finally-force-feedback-logitech-wheels) que el usuario de los foros de SCS Soft, 50keda, ha sido capaz de dar soporte al Force Feedback a los volantes de dicha marca, desarrollando un plugin que ofrece esta funcionalidad.


Además, este usuario ha conseguido hacer uso de los LEDs que algunos volantes traen para reflejar cosas, como las RPM del motor o lo que interese (esta funcionalidad es limitada).


 


Podéis encontrar el plugin en este [hilo](https://forum.scssoft.com/viewtopic.php?f=109&t=249622), y aunque las instrucciones son muy sencillas, os dejamos aquí los pasos que hay que seguir por si el inglés os da algún problema:


* En la carpeta de instalación del juego (si no lo tenéis claro en steam, en propiedades del juego podéis ver abrir los archivos locales), y dentro de la carpeta bin\linux_x64\plugins (si no tenéis la carpeta plugins crearla) arrastrar los ficheros real_ffb_x64.so y real_ffb.ini
* Ejecutar el juego (tiene que ser la versión de 64bits, eso sí)


Y ya está, así de sencillo. Al arrancar el juego os avisará de que estáis usando un plugin y que eso puede liarla parda, pero ya está.


No obstante, para poder usar la funcionalidad de los LEDs tenéis también dentro del comprimido un fichero .rules, de la gente de Feral para el F1 2017 si he entendido bien, que corrige el tema de los LEDs. Para instalarlo correctamente podéis hacer uso del fichero .sh que lo acompaña. Como mi volante no tiene LEDs no lo he probado, pero para quién le interese, comentar que el fichero está ahí.


La verdad es que el plugin funciona bastante bien, las pruebas realizadas, con un Driving Force GT, han ido a la primera, aunque en mi caso toca ajustar algún parámetro porque en mi caso el volante ofrece poca resistencia (el juego tiene varios parámetros de configuración del FF, así que toca jugar con ellos). Podeis ver algunas impresiones que nos ha producido este plugin en este video que recientemente realizamos:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/3mGYjc67buk" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Y tú, ¿recorrerás las carreteras con tu camión y su force feedback? Cuéntanos qué te parece este plugin y qué tal te funciona en los comentarios o también en el hilo del force feedback que tenemos en el [foro](index.php/foro/tutoriales/11-como-conseguir-que-los-volantes-logitech-funcionen-adecuadamente-en-linux#128).

