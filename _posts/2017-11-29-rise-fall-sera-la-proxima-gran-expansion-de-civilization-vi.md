---
author: leillo1975
category: Estrategia
date: 2017-11-29 15:29:16
excerpt: "<p>Firaxis anuncia esta nueva DLC para principios del a\xF1o que viene.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/717c6fb0dff4b7bf3cb4cc45603e902e.webp
joomla_id: 556
joomla_url: rise-fall-sera-la-proxima-gran-expansion-de-civilization-vi
layout: post
tags:
- aspyr-media
- dlc
- civilization-vi
- firaxis
title: "Rise & Fall ser\xE1 la pr\xF3xima gran expansi\xF3n de Civilization VI (ACTUALIZADO)"
---
Firaxis anuncia esta nueva DLC para principios del año que viene.

**ACTUALIZACIÓN (23-1-18):** Aspyr Media ha anunciado en los [foros de Steam](http://steamcommunity.com/app/289070/discussions/0/144512942755435118/?ctp=77#c1693785035830434158) que esta expansión no estará lista para Linux/SteamOS el día del lanzamiento, sinó que tendremos que esperar un poco más, siendo en este momento la fecha indeterminada. Esperemos que no sufra mucha demora y podamos disfrutar de sus bondades lo antes posible.




---


**ACTUALIZACIÓN (17-1-18)**: Firaxis acaba de publicar en Youtube un video donde explica las novedades de Rise and Fall. Si os interesa esta expansión os recomendamos que no perdais detalle, pues el contenido es muy suculento:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/h31myLyc_qk" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL**


Hay que reconocer que el constante trabajo de este estudio Norteamericano es digno de elogiar. Su afán por ofrecer nuevas mejoras y contenidos de calidad para sus juegos es una constante en esta empresa. De nuevo nos han vuelto a sorprender con una nueva expansión, y este caso de las serias. Según comentan en el apartado de su web dedicado a esta expansión de [Civilization VI](index.php/homepage/analisis/item/352-analisis-sid-meier-s-civilization-vi), esta estará disponible para el público, el día **8 de Febrero del proximo año**, que como sabeis está a la vuelta de la esquina. Por lo tanto en poco más de dos meses podremos disfrutar de sus virtudes.


 


Según lo que hemos podido averiguar, esta nueva expansión tendrá como principal cambio con respecto al juego original, que **las civilizaciones dejarán de ser lineales a pasar a ser dinámicas**. Con esto lo que queremos decir es que como en la historia real habrá épocas de bonanza, explendor y expansión; y también tiempos de crisis, siglos oscurso y recesión. Veremos como cambian las fronteras de nuestro imperio, como la lealtad de nuestros ciudadanos varía, como los acontecimientos históricos afectan al devenir de nuestra cultura. Por supuesto, el resto de jugadores de la partida pasarán las mismas calamidades o beneficios que nosotros, pudiendo estar a la par de nosotros o al revés, por lo que podremos aprovechar, por ejemplo que un contendiente esté en horas bajas para maniobrar contra él. Tambén será intesante ver como ciudades dejarán de ser leales y en épocas oscuras apuesten por el caballo del vecino o se independicen.


 


Entre otras cosas podremos tener hasta 7 **Gobernadores** como máximo que podrán hacer politicas activas en nuestras ciudades para llevarlas en una dirección determinada, algo así como los distritos. Para ello podremos especializar a cada uno de ellos con un arbol de ascensos , donde cada uno puede elegir su propio camino. También es importante el trabajo que se hará con la **Diplomacia**, especialmente con las alianzas, pues ahora será mucho más beneficioso establecer lazos con otras civilizaciones para obtener beneficios comunes y no simplemente pactos de no agresión.


 


Estas son algunas de las características principales que vamos a encontrar en esta expansión, pero hay muchas más, como podreis comprobar en [este enlace](https://civilization.com/es-ES/news/entries/announcing-civilization-vi-rise-and-fall).  Como veis son **cambios de calado** y no simplemente la adición de nuevas civilizaciones y alguna nueva característica, como habíamos visto hasta el momento. Ahora solo nos queda confiar que Aspyr lo tenga a tiempo para poder volver a no tener vida social y dormir poco, y volver a decir.... "*un turno más y lo apago*". Sin más os dejamos con su fantástico trailer para que se os vaya abriendo el apetito de turnos:


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/IOT9T15mkX0" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Por cierto, en este momento el juego base está con un descuento  del 50%, por lo que es una buena oportunidad de hacerse con él:


 <div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/289070/123215/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Habeis jugado ya a Civilization VI? ¿Qué os parecen las nuevas caracteríticas de esta expansión? Dinos que te parece en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

