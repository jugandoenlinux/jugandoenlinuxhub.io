---
author: Pato
category: Arcade
date: 2017-03-01 20:00:32
excerpt: "<p>El juego ya ha superado el Greenlight y promete versi\xF3n para Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/218fa54275e0e31c37b4e5091d9112ba.webp
joomla_id: 235
joomla_url: caveman-warrior-un-llamativo-arcade-multijugador-esta-en-campana-en-kickstarter
layout: post
tags:
- plataformas
- multijugador
- kickstarter
- arcade
title: "'Caveman Warrior' un llamativo arcade multijugador est\xE1 en campa\xF1a en\
  \ Kickstarter"
---
El juego ya ha superado el Greenlight y promete versión para Linux

'Caveman Warrior' es la última producción de Jandusoft [[web oficial](http://www.jandusoft.com/)], estudio español que hasta ahora andaba centrado en los juegos para móviles y que ahora nos traen un proyecto al terreno del PC. Para ello están en campaña en [Kickstarter](https://www.kickstarter.com/projects/jandusoft/caveman-warriors-multiplayer-platformer-arcade-gam/description) donde aún les quedan 15 días para terminar, y también han conseguido pasar con éxito su [campaña de Greenlight](http://steamcommunity.com/sharedfiles/filedetails/?id=686374739).


'Caveman Warriors' es un arcade y plataformas para hasta 4 jugadores que se desarrolla en una época prehistórica. 


"Todo estaba en calma hasta que los aliens descendieron a la tierra y pusieron todo patas arriba".


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/D3efZBYDD58" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Como se puede ver, el juego es un arcade de acción y plataformas en 2D donde cada personaje tiene sus características y puntos fuertes y tiene influencias reconocidas en juegos como "Joe & Mac: Caveman Ninja", "New Super Mario Bros" o "Trine".


Desde luego no tiene mala pinta, y según puede verse prometen versión para Linux tanto en su web, como en Kickstarter o Greenlight.


Si estás interesado en participar en la campaña y ayudarles a llegar a la meta puedes hacerlo en el siguiente enlace:


<div class="resp-iframe"><iframe height="420" src="https://www.kickstarter.com/projects/jandusoft/caveman-warriors-multiplayer-platformer-arcade-gam/widget/card.html?v=2" width="220"></iframe></div>


 En el momento de escribir estas líneas van por el 61% y solo piden 10.000 dólares, por lo que la inversión parece segura.


¿Qué te parece este 'Caveman Warriors'? ¿Piensas aportar en su Kickstarter?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

