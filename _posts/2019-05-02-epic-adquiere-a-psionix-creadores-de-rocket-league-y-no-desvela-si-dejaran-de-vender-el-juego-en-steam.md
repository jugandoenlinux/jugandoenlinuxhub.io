---
author: Pato
category: Noticia
date: 2019-05-02 09:03:42
excerpt: <p>Hablamos sobre las posibles consecuencias para los jugadores en Linux
  de este movimiento</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/451bf7d0eb68e09e196cbaba47814353.webp
joomla_id: 1036
joomla_url: epic-adquiere-a-psionix-creadores-de-rocket-league-y-no-desvela-si-dejaran-de-vender-el-juego-en-steam
layout: post
tags:
- rocket-league
- epic-games
- epic-game-store
- psionix
title: "Epic adquiere a Psyonix, creadores de Rocket League y no desvela si dejar\xE1\
  n de vender el juego en Steam"
---
Hablamos sobre las posibles consecuencias para los jugadores en Linux de este movimiento

La bomba cayó ayer. Psyonix, creadores de Rocket League, uno de los juegos más exitosos en los últimos años anunciaba en su [blog oficial](https://www.rocketleague.com/news/psyonix-is-joining-the-epic-family-/) que se unían a "la familia de Epic" y anunciaban los cambios que se producirán como consecuencia de este movimiento.


Según el comunicado, Psyonix se une a Epic para "servir mejor a su comunidad y en mejores condiciones". En principio, el juego no sufrirá cambios, pero admiten que **el objetivo a medio o largo plazo es lanzar Rocket League en la Epic Game Store** cosa que no debería ser mala "per-se" si no fuera por que la red se llenó de *rumores y comentarios* sobre una posible exclusividad del juego para la tienda de Epic, dejando de vender Rocket League en Steam a final de año e incluso dejando de soportar el juego en la tienda de Valve a largo plazo.


Sin embargo, desde Psyonix quisieron calmar un poco las aguas añadiendo el siguiente comentario al final del comunicado:


*"Nota del editor: Queríamos aclarar algo después de las noticias de hoy: Rocket League está y permanece disponible en Steam. Cualquiera que posea Rocket League a través de Steam todavía puede jugarlo y puede esperar su soporte en el futuro. ¡Gracias!"*


Suficientemente ambiguo como para no desvelar los planes que la compañía tiene para el futuro una vez forme parte de Epic. Pero hay un grupo de usuarios de Rocket League, del que aunque pequeño nadie se acuerda: los jugadores que lo juegan en sistemas Linux.


Recordemos que Rocket League llegó a Linux en un momento en que Steam estaba promocionando fuertemente el juego en Linux, lo que atrajo a varios estudios a portar sus juegos. En el caso de Rocket League **la propia Valve llegó a promocionarlo** en Linux ofreciéndolo en diversos bundles de hardware. Y aquí es donde reside el problema. Si ya de por si la forma de competir de Epic es discutible, pues en vez de ofrecer ventajas y mejoras a los usuarios para convencerlos de usar su tienda se dedican a rebajar el margen para atraer a desarrolladores sin ofrecer ventajas o descuentos por ello a los usuarios, o "comprar" exclusivas forzando a los usuarios a pasar por una Epic Games Store que a día de hoy aún está a mucha distancia de cualquier otra tienda disponible en PC por su falta de características (alguna de las cuales ya han dicho que no implementarán), **la falta de soporte a Linux por parte de la Epic Games Store** puede suponer un problema ya que **en el hipotético caso de que en un futuro dejasen de vender el juego en Steam**, los jugadores que ya tengan el juego en Linux es de suponer que seguirán pudiendo jugar y recibir soporte tal y como dicen en el comunicado, pero **ya no sería posible comprar el juego por parte de nuevos jugadores que quisieran adquirirlo para jugar en un sistema Linux**, al menos de forma oficial.


Quizás esto quede en nada, ya que Epic [ha dicho recientemente](https://www.usgamer.net/articles/epic-promises-support-for-rocket-league-on-all-platforms-no-announced-plans-to-pull-it-from-steam) que aún no han decidido nada sobre los planes de futuro de Rocket League y su posible cese de venta en la tienda de Steam, afirmando que *siguen comprometidos con el soporte en todas las plataformas*, sin mencionar nada sobre la venta del juego en Steam lo que también puede suponer que en un futuro el juego pase a ser "free to play". Sin embargo todo sigue en el aire dada la medida ambigüedad de los comunicados por parte de ambas empresas.


¿Que pensáis de la compra de Psyonix por parte de Epic? ¿Creeis que Rocket League puede correr peligro de dejar fuera a los usuarios de Linux?


Cuéntamelo en los comentarios, o en el canal de Jugandoenlinux de [Telegram](https://telegram.me/jugandoenlinux).

