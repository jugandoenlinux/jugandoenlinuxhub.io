---
author: Pato
category: Aventuras
date: 2017-09-13 17:28:42
excerpt: "<p>El juego de D-Pad Studio recibir\xE1 una actualizaci\xF3n con multitud\
  \ de idiomas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0d7764695c55c5d1c3edadce4a2a2cb9.webp
joomla_id: 460
joomla_url: el-notable-owlboy-pronto-estara-traducido-al-espanol
layout: post
tags:
- indie
- plataformas
- aventura
- pixel-art
title: "El notable 'Owlboy' pronto estar\xE1 traducido al espa\xF1ol"
---
El juego de D-Pad Studio recibirá una actualización con multitud de idiomas

Hace ya un tiempo que [os anunciamos](index.php/homepage/generos/aventuras/item/295-owlboy-el-galardonado-juego-de-aventuras-ya-disponible-en-linux) la disponibilidad de 'Owlboy' [[web oficial](http://www.owlboygame.com/)] en Linux gracias a Etha Lee, pero aparte de ser un gran juego teníamos la espina de tenerlo que jugar en inglés. 


Pues bien, D-Pad Studio [anunció ayer mismo](http://steamcommunity.com/games/115800/announcements/detail/1449453249111011481) que pronto recibiremos una actualización de este magnífico juego de aventuras y plataformas que nos traerá la traducción del juego a multitud de idiomas, entre ellos al español.


'Owlboy' es un juego de aventura donde exploraremos mundos abiertos con una mezcla única de vuelo y plataformas.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/p9VwGaycmCQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Si te gustan los buenos plataformas, lo tienes disponible en su página de Steam y en próximas fechas en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/115800/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


¿Qué opinas de Owlboy? ¿Te gustan los plataformas?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD). 

