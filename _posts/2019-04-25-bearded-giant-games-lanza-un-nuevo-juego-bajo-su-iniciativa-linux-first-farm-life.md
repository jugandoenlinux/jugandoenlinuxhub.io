---
author: Pato
category: Puzzles
date: 2019-04-25 06:41:27
excerpt: "<p>El juego de tipo \"match 3\" sigue con la filosof\xEDa de su creador\
  \ <span class=\"css-76zvg2 css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\" dir=\"\
  auto\">@store_giant</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/03224d1fcb0108937e9b07deb18cf473.webp
joomla_id: 1029
joomla_url: bearded-giant-games-lanza-un-nuevo-juego-bajo-su-iniciativa-linux-first-farm-life
layout: post
tags:
- indie
- puzzles
title: 'Bearded Giant Games lanza un nuevo juego bajo su iniciativa "Linux First":
  Farm Life'
---
El juego de tipo "match 3" sigue con la filosofía de su creador @store_giant

Gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/farm-life-the-match-3-game-about-restoring-a-farm-has-been-released-for-linux-and-its-lovely.13984) descubrimos un "one man studio" con una filosofía cuanto menos curiosa, y es que **Bearded Giant Games** publica sus juegos siempre antes en Linux que en otras plataformas o sistemas simplemente por lo que el llama su "[iniciativa Linux primero](https://beardedgiant.games/linux-first-initiative/)".


Bearded Giant Games es obra de un solo desarrollador y debido a sus recursos limitados tiene que centrarse en diseñar, producir y programar todos los juegos con un presupuesto reducido no pudiendo permitirse extender el desarrollo durante mucho tiempo, y como Linux es su plataforma favorita de desarrollo y uso regular, s**e centra específicamente en Linux** portando luego a otras plataformas si es económicamente viable agregando también que **sus desarrollos son Open Source** (con licencia propietaria). Tal es el caso de juegos que ya ha lanzado como [Ebony Spire Heresy](https://beardedgiant.games/product/ebony-spire-heresy/) o [Roguesweeper](https://beardedgiant.games/product/roguesweeper/) . Además de esto, también trabaja portando otros juegos cuando se prestan a ello, y este Farm Life es la primera prueba al ser un port de un juego de Android de la compañía [RVL Games](http://rvlgames.com/).


 **Farm Life** es un juego de tipo "match 3" portado desde  donde además tendremos que centrarnos en mantener una granja con los items que vayamos consiguiendo en un meta-juego dentro del mismo:


*"Farm Life es un juego de tipo match 3 que gira en torno a Claire Barnes y sus luchas por restaurar y heredar la granja de su tía. Ayúdala a reconstruir, hacer crecer y recuperar las tierras y los edificios que se perdieron con el paso del tiempo.*


*Cada nivel te acercará un paso más hacia tu objetivo y hacia la restauración de la gloria de la granja de la tía Linda.*


*No estás solo en esta historia, ya que los coloridos personajes te llevarán por el camino correcto hacia el éxito, mientras que otros reclamantes esperarán que fracases."*


![Farmlife](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Farmlife/Farmlife.webp)


La historia detrás del desarrollo de este juego desde luego es cuando menos curiosa, haciendo Farm Life un juego con una propuesta única. Si quieres saber mas sobre este lanzamiento [puedes visitar el blog](https://beardedgiant.games/farm-life-launches-on-linux-on-april-23rd/) de Bearded Giant Games donde explica el por qué del lanzamiento de un juego Match 3 "premium" en Linux.


Salvo Ebony Spire Heresy que sí [está disponible en Steam](https://store.steampowered.com/search/?developer=Bearded%20Giant%20Games), los juegos de Bearded Giant Games solo se pueden conseguir [en su propia tienda](https://beardedgiant.games/). Si te gustan este tipo de juegos, o te gustaría apoyar la iniciativa y filosofía de este desarrollador, pásate por [este enlace](https://beardedgiant.games/product/farm-life/) donde aparte de poder comprar Farm Life podrás conocer y comprar otros juegos suyos.


¿Que te parece Bearded Giant Games? ¿Te gustan los juegos de tipo match 3?


Cuéntamelo en los comentarios o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

