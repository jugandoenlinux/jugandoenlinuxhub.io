---
author: leillo1975
category: Carreras
date: 2018-06-08 14:06:10
excerpt: <p>La desarrolladora italiana <span class="username u-dir" dir="ltr">@ya2tech
  acaba de lanzarla</span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bf1e20a4462b71e3cc4cece2a8c96ac8.webp
joomla_id: 763
joomla_url: el-juego-libre-yorg-acaba-de-publicar-la-version-0-9-rc1
layout: post
tags:
- open-source
- yorg
- flavio-calva
- codigo-abierto
title: "El juego libre \"YORG\" acaba de publicar la versi\xF3n 0.9 RC1 ."
---
La desarrolladora italiana @ya2tech acaba de lanzarla

Hace tiempo que no teníamos [noticias](index.php/buscar?searchword=Yorg&ordering=newest&searchphrase=all) oficiales de este juego, pero los que llevais más de medio año con nosotros, probablemente recordeis la **entrevista** que realizamos a su desarrollador principal, [Flavio Calva](index.php/homepage/entrevistas/item/587-entrevista-a-flavio-calva-de-ya2-yorg), y la salida de su [anterior versión](index.php/homepage/generos/carreras/item/686-yorg-alacanza-la-version-0-8), la 0.8. Se trata de un juego que JugandoEnLinux sigue muy de cerca, ya que colaboramos en el testeo, las traducciones y el trato con Flavio y Luca es exquisito, antendiendonos puntualmente cada vez que necesitamos cualquier cosa. Incluso nos han dedicado una valla publicitaria en el circuito de Roma (Gracias!!!). Hace menos de una hora recibimos un correo avisándonos de la salida de su primera versión candidata antes de llegar a la definitiva 0.9. En ella encontramos importantes novedades:



> 
> - Inclusión de un **nuevo circuito**, en este caso se trata de Toronto, con un escenario nevado ambientado en la ciudad Canadiense (ver video al final del artículo).
> 
> 
> - Un **nuevo tema musical** en su banda sonora
> 
> 
> - Traducción a **nuevos idiomas** entre los que se encuentra el **Español** y el Gallego, además de el Francés.
> 
> 
> - Mejoras gráficas incluyendo el **efecto Bloom.**
> 
> 
> - Algunas **mejoras en el sistema de conducción**.
> 
> 
> -.... y la que es la más importante, **inclusión del modo Multijugador** (**en fase experimental y no definitiva**). Para poder jugarlo necesitais una cuenta XMPP que os serviría para loguearos y poneros en contacto con otros jugadores.
> 
> 
> 


Para quien no lo conozca, [Yorg](http://www.ya2.it/articles/yorg-09-release-candidate-is-ready.html#yorg-09-release-candidate-is-ready) es un juego de coches al estilo MicroMachines tremendamente divertido, y que recomendamos a todos vosotros que probeis sin más dilación. El juego se puede descargar tanto para 32 como para 64 bits, y dispone soporte también para otros sistemas (MacOS y Windows). Podeis haceros con el en su página de descargas (al fondo de todo, donde pone "The last Release Candidate"):


<http://www.ya2.it/pages/download.html>


Aquí teneis un video de como les ha quedado el nuevo circuito:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/897VQRhkg5A" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Qué opinais de los cambios introducidos? ¿Habeis jugado con anterioridad a YORG? Déjanos tus impresiones en los comentarios de este artículo o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

