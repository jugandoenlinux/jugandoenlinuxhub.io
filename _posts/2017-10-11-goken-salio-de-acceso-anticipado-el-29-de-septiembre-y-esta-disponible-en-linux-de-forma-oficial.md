---
author: Pato
category: Rol
date: 2017-10-11 14:08:16
excerpt: "<p>El juego de rol y acci\xF3n llega tras recibir el apoyo de la comunidad</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e3d995a152e18bb86fa197fdc98b26b8.webp
joomla_id: 481
joomla_url: goken-salio-de-acceso-anticipado-el-29-de-septiembre-y-esta-disponible-en-linux-de-forma-oficial
layout: post
tags:
- accion
- rol
- jrpg
title: "'GOKEN' sali\xF3 de acceso anticipado el 29 de Septiembre y est\xE1 disponible\
  \ en Linux de forma oficial"
---
El juego de rol y acción llega tras recibir el apoyo de la comunidad

Como ya publicamos [hace un par de meses](index.php/homepage/generos/rol/item/551-goken-ya-esta-disponible-en-linux-en-acceso-anticipado-gracias-al-apoyo-de-la-comunidad), el juego de rol y acción (ARPG) 'GOKEN' [[web oficial](https://www.gianty.co.jp/goken/)] llegaría a Linux gracias al empuje y apoyo de la comunidad, y ahora ya lo tenemos disponible. En concreto, el pasado día 29 de septiembre el estudio Gianty.co anunció su salida de la fase de acceso anticipado. 


'GOKEN' bebe de la jugabilidad de los juegos de rol y acción japoneses con gráficos preciosistas en vista isométrica, y con mecánicas "old school" que recuerdan a los juegos de hace unos años.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/g0V0cwqM9k0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


'GOKEN' no está disponible en español, pero si eso no es impedimento y te va la temática, lo tienes disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/671260/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

