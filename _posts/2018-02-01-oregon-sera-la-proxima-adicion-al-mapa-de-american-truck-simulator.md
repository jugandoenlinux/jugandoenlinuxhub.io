---
author: leillo1975
category: "Simulaci\xF3n"
date: 2018-02-01 16:05:40
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\">@SCSsoftware pronto nos traer\xE1 esta\
  \ expansi\xF3n</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c36ca621a5bd9f22ed82eb87110f16dc.webp
joomla_id: 631
joomla_url: oregon-sera-la-proxima-adicion-al-mapa-de-american-truck-simulator
layout: post
tags:
- american-truck-simulator
- ats
- scs-software
- oregon
title: "Oreg\xF3n ser\xE1 la proxima adici\xF3n al mapa de American Truck Simulator\
  \ (ACTUALIZADO)"
---
@SCSsoftware pronto nos traerá esta expansión

**ACTUALIZADO 27-9-18**: Hace un par de dias SCS publicaba en [su blog](http://blog.scssoft.com/2018/09/bridges-of-oregon.html) unas cuantas instantaneas de los puentes que veremos en Oregón. Hoy, después de incrementar un poco el Hype finalmente han anunciado la fecha definitiva de salida, que será justamente dentro de una semana tal y como anuncian en twitter:



> 
> The wait is almost over!  
>   
> The Oregon map expansion for [#AmericanTruckSimulator](https://twitter.com/hashtag/AmericanTruckSimulator?src=hash&ref_src=twsrc%5Etfw)  
> will come out on October 4, 2018!<https://t.co/Sf03tSbqVk> [pic.twitter.com/ErnSaJMw4x](https://t.co/ErnSaJMw4x)
> 
> 
> — SCS Software (@SCSsoftware) [27 de septiembre de 2018](https://twitter.com/SCSsoftware/status/1045296966614351874?ref_src=twsrc%5Etfw)



  Por supuesto en JugandoEnLinux.com os ofreceremos toda la información que os mereceis sobre esta nueva expansión para American Truck Simulator, y para ir abriendo apetito de carretera os dejamos también un trailer de lo que pronto veremos en nuestras pantallas:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/wilAbkIhZcE" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>





---


**NOTICIA ORIGINAL:** Hace poco tiempo, los que somos seguidores del Blog de [SCS Software](https://scssoft.com/), nos vimos hypeados por la [publicación](http://blog.scssoft.com/2018/01/somethings-cooking.html) de unas fotos, en la que muchos de los fans pudieron reconocer ciertos paisajes y edificaciones, y si, estaban en lo cierto. Los modelados están inspirados y representan el estado de Oregón. Al menos así lo acaban de confirmar con este tweet hace escasos minutos:



> 
> Our next step in American Truck Simulator world development? Beaver knows!   
>   
> Check out our blog for more info about future map expansion ?<https://t.co/8wLYs6T2zn> [pic.twitter.com/0yNvkroNXK](https://t.co/0yNvkroNXK)
> 
> 
> — SCS Software (@SCSsoftware) [February 1, 2018](https://twitter.com/SCSsoftware/status/959093060838838274?ref_src=twsrc%5Etfw)



 Como sabeis hace poco tiempo SCS lanzó una expansión para ATS que ampliaba el mapa hacia el **sureste**, concretamente el estado de [Nuevo México](index.php/homepage/analisis/item/656-analisis-american-truck-simulator-dlc-new-mexico), y poco después hacían lo propio con Euro Truck Simulator 2 y su expansión "[Italia](index.php/homepage/analisis/item/688-analisis-euro-truck-simulator-2-italia-dlc)". En jugando en Linux analizamos ambas, y como es de esperar en los trabajos de la desarrolladora sueca, el resultado es simplemente alucinante. Siguiendo esta tónica, es seguro que el trabajo a realizar en Oregon será, al menos, igual de bueno.Tal y como indican en su [blog](http://blog.scssoft.com/2018/02/which-way-to-go-beaver-knows.html), la naturaleza de Oregon sobrepasa todo cuanto se han encontrado hasta ahora, donde podremos encontrar bosques profundos, lagos, matorrales densos, rios anchos, montañas, la costa pacifica... También las industrias cambian, tomando protagonismo la explotación maderera, con talado de arboles, aserradores, papeleras, fábricas de muebles, etc.


La verdad es que **se agradece que se vayan un poco al norte**, pues sus últimos trabajos se habían hecho un pelín repetitivos, ya que el paisaje desértico y desolado de Nevada, Arizona y Nuevo México ya es suficiente por ahora y hacía falta un poco más de diversidad. No se ha indicado previsiones, ni mucho menos fechas de lanzamiento, pero como como siempre en JugandoEnLinux.com estaremos atentos tanto a todas las actualizaciones referentes a esta nueva expansión de ATS, como a otros desarrollos paralelos.


¿Has jugado ya a American Truck Simulator?¿Qué opinas de la elección del estado de Oregón como la próxima expansión del mapa? Respóndenos en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

