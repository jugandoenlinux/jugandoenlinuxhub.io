---
author: leillo1975
category: Arcade
date: 2020-04-02 11:18:57
excerpt: "<p>Su desarrollador <span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x\
  \ r-bcqeeo r-qvutc0\">@Antisc2 actualiza su juego #OpenSource . </span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperTuxParty/SuperTuxParty02.webp
joomla_id: 1199
joomla_url: super-tux-party-llega-a-la-version-0-7-alpha-a1
layout: post
tags:
- open-source
- software-libre
- super-tux-party
- anti
title: "Super Tux Party llega a la versi\xF3n 0.9 Alpha"
---
Su desarrollador @Antisc2 actualiza su juego #OpenSource . 


**ACTUALIZACI(ÓN 20-4-20:** Ayer se lanzaba una nueva versión de este juego libre creado con [Godot Engine]({{ "/tags/godot" | absolute_url }}) que poco a poco va tomando forma. En esta ocasión su desarrollador, [Anti](https://anti.itch.io/), se ha **centrado más en pulir su software** más que en añadir nuevas características, con lo que en esta ocasión el juego funcionará mucho mejor y de una forma más congruente. La idea es tener el juego lo más estable posible para **el próximo paso, que será el multijugador en linea**, que se empezará a desarrollar a partir de ahora.


Entre los principales cambios podemos encontrar la **mejora de las pantallas previas y posteriores al juego escogido**, con una información más clara y atractiva; y la forma en la que funciona el **modo 2vs2**, teniendo cada equipo una barra unica a llenar en vez de una por cada jugador. Su desarrollador también destaca la ayuda de [Franzopow](https://soundcloud.com/franzcorradomusic) por hacer la música del **minijuego "Kernel Compiling"**, autor que está colaborando con su trabajo en multitud de proyectos de software libre como [Super Bombinhas]({{ "/posts/disfruta-de-super-bombinhas-un-plataformas-libre" | absolute_url }}), entre otros; y también conocido Streamer (**OpenSource_Gaming** en [Twitch](https://www.twitch.tv/opensource_gaming) y [Youtube](https://www.youtube.com/channel/UCtWVpbOMKm06qjRjEI9pIFg)). La lista de cambios de esta [versión 0.9](https://gitlab.com/SuperTuxParty/SuperTuxParty/-/tags/v0.9) es la siguiente:


* *Se rediseñó la pantalla final del minijuego.*
* *Nuevo diseño de pastel*
* *Soporte hidpi habilitado*
* *Agrega un temporizador para Dungeon Parkour y Forest Run*
* *Se corrigió que la IA se quedara atascada en el minijuego Escape from Lava*
* *Cambios en la calidad de vida del minijuego:* 
	+ *Se redujo el tiempo de selección de plantas en el minijuego "Harvest Food" de 10 segundos a 8 segundos.*
	+ *Genera menos bombas en el minijuego "Boat Rally"*
	+ *Se agregó una cuenta regresiva previa al inicio en los minijuegos "Escape from Lava", "Harvest Food" y "Hurdle"*
	+ *Capturas de pantalla de minijuegos actualizadas*
	+ *Proporcionar mejores combinaciones de teclas predeterminadas*
* *Se corrigió que el modelo de personaje de Beastie y Godette se recortara en el suelo*
* *La creación de capturas de pantalla en Windows funciona ahora*
* *Nuevo diseño y animaciones del menú principal*
* *Nueva pantalla previa al minijuego*
* *Mejoras en el tema de la interfaz de usuario*
* *El menú de créditos ahora obedece correctamente al tema.*
* *Se corrigió el minijuego "Harvest Food" que mostraba la capacidad de saltar, mientras que el minijuego no lo permite.*
* *Muestra los controles para voltear una carta en el minijuego "Memoria"*
* *Se modificó el "minijuego de compilación del kernel".* 
	+ *El estilo visual ahora coincide con el aspecto caricaturesco deseado.*
	+ *En la versión 2v2, los jugadores del equipo ahora llenan la misma barra en lugar de barras separadas*
* *Los iconos de los jugadores ahora se muestran correctamente en el minijuego "Memoria".*
* *Se corrigió un comportamiento erróneo al voltear una carta boca arriba en el minijuego "Memoria".*
* *El minijuego "Memory" ahora no regresa instantáneamente a la pantalla final del minijuego, pero te da un poco de tiempo para mirar las estadísticas.*
* *Utilice Liberation Sans en lugar de Noto Sans como fuente alternativa*
* *Muestra el número total de turnos en el tablero.*
* *La música de fondo en el tablero de prueba ahora se repite*
* *El juego ahora reproduce un sonido cuando pasas un espacio en el tablero.*


Podeis encontrar el código fuente de la [página de su proyecto](https://gitlab.com/SuperTuxParty/SuperTuxParty) en GitLab, o descargar los binarios directamente en [Flathub](https://flathub.org/apps/details/party.supertux.supertuxparty), [Itch.io](https://anti.itch.io/super-tux-party) o su [página web](https://supertux.party/).


 




---


**NOTICIA ORIGINAL 2-4-20:** Hace más o menos un año y medio que nuestro colega @Ser_jor [nos descubría este juego](index.php/component/k2/10-puzzles/972-super-tux-party-un-party-game-open-source) **Open Source multiplataforma** en el que se su desarrollador intenta replicar la experiencia de juegos del estilo de "Mario Party", pero desde el punto de vista del **software libre** y usando sus personajes más conocidos. El juego está licenciado bajo la **GNU GPL 3.0**, y además está **desarrollado con herramientas libres** tales como Godot, GIMP, Krita o Blender.


En esta nueva versión 0.7 Alpha **la cantidad de trabajo introducido entre novedades y mejoras es importante** por lo que a continuación os vamos a detallar la extensa [lista de cambios](https://gitlab.com/SuperTuxParty/SuperTuxParty/-/releases/v0.7):


**Nuevas características**


-Se agregaron pantallas de carga


-Se agregó música al minijuego "Haunted Dreams"


-Opciones gráficas agregadas


Requiere reiniciar el juego


-Pantalla de victoria agregada con resumen del juego


-Se agregaron 3 nuevos tipos de minijuegos:


Gnu Solo: desafío para un jugador que se recompensa con un elemento  
 Nolok Solo: desafío para un jugador para evitar que te roben un pastel  
 Nolok Coop: todos los jugadores deben trabajar juntos para ganar el minijuego o perder 10 galletas cada uno


-Se agregaron 3 nuevos minijuegos:


Forest Run: Salta y corre plataformas (minijuego Gnu Solo)  
 Dungeon Parcour: Salta y corre plataformas (minijuego Nolok Solo)  
 Boat Rally: conduce un bote a través de las rocas y evita ser golpeado por bombas que caen (Nolok Coop)


-Pantalla de créditos añadidos


**Mejorado / cambiado**


-Movimiento más suave del jugador en los tableros.


-Arrleglado Botón de desplazamiento fijo no pulsable.


-Se corrigieron las traducciones faltantes


-Contornos mejorados al seleccionar artículos / jugadores


-Mejor textura de la suciedad


-Los pasteles no reaparecen en el mismo lugar (si es posible)


-Se corrigió la configuración de dificultad de la IA en juegos guardados


-Guardar cuando se le solicite un minijuego ya no se saltará dicho minijuego después de cargar


-Se corrigió el bloqueo al cargar el minijuego "Haunted Dreams"


-Se corrigió la configuración de giros del tablero que se ignoraba


Pero no hay mejor manera de comprobar de que van todos estos cambios que lo jugueis con vuestra gente en estos días tan "especiales" para todos. Podeis descargaroslo desde su [página en itch.io](https://anti.itch.io/super-tux-party); o compilar o consultar su código desde la [página de su proyecto en Gitlab](https://gitlab.com/SuperTuxParty/SuperTuxParty). Os dejamos con un trailer del juego de su anterior versión:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/_5wRDGvFRMA" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>  
¿Os gustan este tipo de juegos tupo "Party"? ¿Qué os parece este proyecto de Software Libre? Podeis darnos vuestra opinión sobre Super Tux Party en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

