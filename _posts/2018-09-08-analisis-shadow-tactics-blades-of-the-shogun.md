---
author: Serjor
category: Estrategia
date: 2018-09-08 15:16:53
excerpt: "<p>La crudeza del Jap\xF3n feudal nunca hab\xEDa sido tan entretenida</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3fcd8f90952a19354e6b0c4b58be99e3.webp
joomla_id: 853
joomla_url: analisis-shadow-tactics-blades-of-the-shogun
layout: post
tags:
- shadow-tactics
- analisis
- commandos
- mimimi-productions
- daedalic-entertainment
title: "An\xE1lisis: Shadow Tactics: Blades of the Shogun"
---
La crudeza del Japón feudal nunca había sido tan entretenida

Hay ocasiones en las que las expectativas ante un juego son tan grandes que el juego, por muy bueno y AAA que este sea, nunca podría estar a la altura de lo que se espera de él. En mi caso, cuando este Shadow Tactics: Blades of the Shogun salió a la venta y pude probar su demo gratuita, mis expectativas sobre este juego no hicieron más que aumentar, y una vez que pude hacerme con él y jugarlo de principio a fin, tengo que reconocer que las expectativas eran demasiado altas, aunque el juego, y eso que es un indi, ha podido con ellas.


No diré que soy un gran seguidor de la saga Commandos, pero sí que el primer Commandos fue un juego que en su momento me atrapó, y que disfruté muchísimo, así que un juego que se inspira abiertamente en la saga Commandos, modernizado y emplazado en el Japón feudal tenía que ser un juego al que tenía que jugar sí o sí, por eso, si tú eres como yo, y te gustó el primer Commandos y te gusta el ambiente japonés, deja de leer, no tengo nada que contarte y estás perdiendo el tiempo, juégalo y cuando lo termines, cuéntame en los comentarios qué te ha parecido.


Para el resto, y sobre todo para aquellos que no conocen la saga Commandos y por lo tanto no tienen claras cuales son las mecánicas del juego os haremos un pequeño resumen de las mecánicas más básicas. El juego se compone de una serie de misiones, las cuales tienen lugar en entornos diferentes, un pueblo, un fuerte, una ciudad, una cárcel... y para superar cada misión deberemos cumplir una serie de objetivos. Para ello tendremos a nuestra disposición, dependiendo de la misión, entre 1 y 5 personajes diferentes, con habilidades diferentes cada uno. Dos ninjas, un samurai, una ladrona aprendiz de ninja y un francotirador. Además, como decíamos, cada uno tiene habilidades diferentes, y por ejemplo, el ninja dispone de un shuriken, el samurai es capaz de matar a dos enemigos a la vez si estos están lo suficientemente juntos, la ninja se puede disfrazar permitiendo andar por delante de enemigos sin ser detectada, la ladrona puede colocar trampas y el francotirador dispone de granadas.


Gracias a las diferentes habilidades las misiones se pueden enfocar de diversas maneras, lo que hace que cada misión sea rejugable y única para cada jugador. Además el diseño de los escenarios y las rutinas de los enemigos están pensadas para que podamos hacer uso de esta libertad de juego, lo que lo hace tremendamente divertido al permitir al jugador llevar a cabo cada misión con su estilo de juego propio y no tener que afrontar cada reto pensando en cómo han querido los desarrolladores que resolvamos el puzzle.


Además, para aumentar las opciones disponibles, podremos también hacer uso del modo sombra, en el que a cada personaje que queramos podemos indicarle una única acción a realizar, y una vez que tengamos para cada personaje la acción que nos interesa, podemos, con una sola tecla, activarlas todas a la vez, haciendo que los personajes ejecuten la acción configurada al unísono. Quizás así explicado no parezca tener sentido o que sea interesante, pero es una mecánica tremendamente divertida, y necesaria, ya que podemos hacer que un personaje vaya de un punto a otro mientras un segundo mata al guardia que nos hubiera descubierto.


![shadow tactics 2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/shadowtactics/shadow-tactics-2.webp)


¿Y cómo es que este juego ha sido capaz de superar todas mis expectativas y salir airoso? Sinceramente, no sabría dar una única respuesta. Este Shadow Tactics es una clara muestra de que el todo es más que la suma de sus partes. Por un lado gráficamente, aunque no es la mayor maravilla del mundo, está cuidado y luce tremendamente bien. Los escenarios son variados, las texturas son agradables a la vista, es colorido sin ser recargado y todo encaja perfectamente y es que se nota que en el apartado visual hay mucho cariño puesto. Las animaciones por ejemplo son tremendamente sencillas, sobretodo en las partes en la que los personajes hablan donde no podemos jugar (de hecho, el juego saca un par de franjas negras para simular que estamos viendo una película), y a pesar de la sencillez de las animaciones, donde se nota que el presupuesto es el de un indi y no un triple A, han sabido encontrar el equilibrio para que cumplan con su objetivo.


Pero lo que yo quizás más destacaría es el apartado sonoro, y por dos principales motivos. Por un lado el juego nos da la opción de poner las voces en inglés o en japonés, y aunque el trabajo de los dobladores ingleses está bien, en japonés el juego gana en autenticidad y gana muchísimo en inmersión. De hecho, me atrevería a decir que inicialmente estaba diseñado para las voces japonesas y luego se dobló al inglés, porque en japonés todo encaja demasiado bien.


Y si las voces en japonés consiguen meternos en el juego y hacernos creer que estamos en Japón, la banda sonora no es para menos, y algunas de las canciones escogidas son, para mi gusto desde luego, simplemente perfectas para un juego como este, y no solamente logran ambientar debidamente, sino que sin ellas el juego tendría un vacío.


Así que la suma de unas mecánicas muy buenas, un diseño de escenarios tremendamente cuidado, no tan solo en el apartado estético y una calidad sonora de quitarse el sombrero dan como resultado un juego que ni cansa ni aburre, que tiene sus retos y que nos permite resolver las situaciones con diferentes herramientas y donde todo funciona muy bien.


![shadow tactics 3](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/shadowtactics/shadow-tactics-3.webp)


Lamentablemente, por mucho que piense que Shadow Tactics es un juego que todo fan de los juegos tipo Commandos debería jugar sin pensárselo, tengo que ponerle algún pero, aunque si no habéis jugado aún, y os hacéis con él ahora, los peros que tengo para él ya no os afectarán, me explico. Cuando me hice con este juego los tiempos de carga eran terriblemente altos, de hecho, si veis el vídeo que hicimos en directo veréis como para cargar la misión el juego se tira un buen rato. Además, no solamente los tiempos de carga eran excesivamente altos para un juego que no es tan exigente gráficamente, sino que, al menos jugándolo con teclado y ratón, desconozco si esto pasaba también al jugar con mando, al mover el puntero del ratón en las intersecciones entre suelo y pared el juego no siempre calculaba bien la posición de la retícula y al mandar al personaje al destino este podía acabar a unos píxeles de distancia del punto donde pensaba que iba a acabar, con lo que podía ser descubierto. Pero el pero del pero (por ser un poco redundantes) es que mientras jugaba la gente de Mimimi Productions sacaron un parche, el cuál solucionaba todo esto. Los tiempos de carga son realmente bajos y no he vuelto a encontrarme con el problema del cursor, aunque también es cierto que este parche salió cuando ya casi estaba terminándome el juego, así que no sé si es que está solucionado o simplemente no me ha pasado más (el problema no pasaba siempre, pasaba pocas veces y en contextos particulares). Lo único malo, es que perdí mis partidas guardadas ya que el nuevo parche hacía uso de un nuevo formato de guardado que lo hacía incompatible. No perdí el progreso, y todas las misiones desbloquedas seguían desbloquedas, pero las partidas guardadas, con estados de misiones a medio hacer y demás, perdidas.


Y poco más podemos añadir. Quizás decir que el juego ha sido acusado de que sus misiones son muy largas, pero en mi caso eso me parece una virtud, a parte de que cada misión tiene sus propios desafíos, como terminar la misión sin matar a alguien, o haciendo uso del entorno... y diría que casi todas las misiones tienen el clásico desafío de completar la misión en menos de X minutos (yo alguna en la que el desafío ponía 5 minutos he tardado cerca de la hora...), así que es posible que estas críticas sean más un problema de la torpeza del jugador que del propio juego.


Si os interesa este juego, podéis haceros con él a través del [link de afiliado de Humble Bundle](https://www.humblebundle.com/store/shadow-tactics-blades-of-the-shogun?partner=jugandoenlinux), aprovechando la oferta que tienen de 19.99€ , por lo menos durante la redacción del artículo, o también en el [Unity Bundle](https://www.humblebundle.com/games/unity-bundle?partner=jugandoenlinux) por 12€.


Os dejamos con un vídeo del canal donde jugamos al nivel de la demo, para evitar posibles spoilers:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/VYHupWaSLJU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Y tú, ¿envainarás tu espada o la usarás para acabar con los enemigos del Shogun? Cuéntanoslo en los comentarios y en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

