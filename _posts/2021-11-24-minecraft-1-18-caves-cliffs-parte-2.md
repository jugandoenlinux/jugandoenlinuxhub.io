---
author: Laegnur
category: "Exploraci\xF3n"
date: 2021-11-24 10:51:26
excerpt: "<div>Nuestro colaborador @Laegnur nos trae lo \xFAltimo de @Mojang.</div>\r\
  \n<div>&nbsp;</div>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Minecraft/caves_cliffs_pt2.webp
joomla_id: 1389
joomla_url: minecraft-1-18-caves-cliffs-parte-2
layout: post
tags:
- minecraft
title: 'Minecraft 1.18: Caves & Cliffs Parte 2'
---
Nuestro colaborador @Laegnur nos trae lo último de @Mojang.
 
¡Salve jugadores!


Una vez mas vuelvo a vosotros con las novedades de **Minecraft**, pues es hoy cuando se lanza la ultima versión de este juego, Minecraft **1.18** **Caves and Cliffs Parte 2**.


Veamos cuales son las novedades y los cambios en esta nueva versión.


Como os comentaba en el articulo del lanzamiento de la versión Minecraft 1.17, la idea era lanzar esta actualización completa en la versión anterior, pero entre el confinamiento del Covid, y que al final intentaron abarcar mas de lo que se habían propuesto inicialmente lo dividieron en dos partes. Lanzando en la versión 1.17 los nuevos bloques, y dejando el nuevo sistema de generación del mundo y los nuevos biomas para esta versión 1.18.


Y es precisamente en los biomas donde veremos los cambios mas gordos.


Y es que a partir de ahora la generación del terreno, es independiente de los biomas. Esto es, si antes los biomas determinaban la altura a la que podían aparecer, modificando el terreno para ajustarlo a las necesidades de cada uno, ahora al ser independientes, sera el bioma el que se adapte al terreno.


Y debido a esto precisamente, nos encontramos con una serie de biomas clásicos que determinaban cambios drásticos en el terreno, que ya no forman parte del juego:


* Badlands Plateau
* Bamboo Jungle Hills
* Birch Forest Hills
* Dark Forest Hills
* Deep Warm Ocean
* Desert Hills
* Desert Lakes
* Giant Spruce Taiga Hills
* Giant Tree Taiga Hills
* Gravelly Mountains+
* Jungle Hills
* Modified Badlands Plateau
* Modified Jungle
* Modified Jungle Edge
* Modified Wooded Badlands Plateau
* Mountain Edge
* Mushroom Field Shore
* Shattered Savanna Plateau
* Snowy Mountains
* Snowy Taiga Hills
* Snowy Taiga Mountains
* Swamp Hills
* Taiga Hills
* Taiga Mountains
* Tall Birch Hills
* Wooded Hills


Y otros tantos que ven modificado su nombre


* Tall Birch Forest => Old Growth Birch Forest
* Giant Tree Taiga => Old Growth Pine Taiga
* Giant Spruce Taiga => Old Growth Spruce Taiga
* Snowy Tundra => Snowy Plains
* Jungle Edge => Sparse Jungle
* Stone Shore => Stony Shore
* Mountains => Windswept Hills
* Wooded Mountains => Windswept Forest
* Gravelly Mountains => Windswept Gravelly Hills
* Shattered Savanna => Windswept Savanna
* Wooded Badlands Plateau => Wooded Badlands


¡Pero no todo va a ser quitar cosas! Ademas de los nuevos biomas subterráneos que ya conocíamos desde la actualización 1.17, **Cuevas kársticas** y **Cuevas frondosas**, al cambiar la generación del terreno para dar cabida a las nuevas montañas, se añaden nuevos biomas que las forman, y que dependerán de la temperatura de los biomas que rodeen a las montañas.


Así, tendremos:


* **Prado**: En los biomas templados la base de las montañas estar formada por el nuevo bioma de Prado.
* **Arboleda**: La parte baja de las montañas rodeadas de biomas fríos y con bosque estará formada por el bioma Arboleda.
* **Laderas nevadas**: Si la montaña se encuentra en biomas fríos, sin arboles, su parte baja estará formada por el bioma Laderas Nevadas.
* **Cumbres rocosas**: La cima de las montañas en climas cálidos, o templados, sera el bioma Cumbres rocosas, formado por bloques de piedra, grava y calcita.
* **Cumbres escarpadas y Cumbres heladas**: En los biomas fríos, dependiendo de la altura de las montañas, las cumbres estarán formadas por bloques de Nieve o Hielo, formando los biomas de Cumbres escarpadas y Cumbres heladas.


Ademas se ha suavizado la transición entre biomas, para que el cambio de temperatura de uno a otro sea gradual.


Se modifica la generación de las playas para hacer que sean menos frecuentes, y a la vez de mayor extensión.


Se modifica también la generación de los ríos para asegurar que sean completamente navegables.


Otro de los grandes cambios que veremos, es el rango de alturas del mundo. Si antes en Minecraft la dimensión del mundo en el eje Y iba desde la altura 0 hasta la altura 256, ahora para dar cabida a las nuevas montañas, y a las nuevas cuevas, se amplia el rango quedando **la dimensión en el eje Y desde la altura -64 hasta la altura 320**.


El ultimo de los grandes cambios tiene que ver con el comportamiento de los monstruos. Pues si bien antes era necesario un nivel de luz inferior a 7 para que apareciesen monstruos, ahora sera necesaria una ausencia total de luz, con lo que ya no hay excusa para adentrarse en las profundidades del nuevo sistema de cuevas y explorar a nuestras anchas.


En cuanto a los nuevos objetos, en esta actualización no veremos grandes novedades. A parte de los objetos ya añadidos en la versión anterior, y cuyas recetas aun no estaban disponibles, tan solo se añade un nuevo objeto. Un disco de vinilo que se podrá encontrar en los cofres de los pasillos de las Fortalezas.


En concreto el disco se llamara “**otherside**” y contendrá esta pieza de la compositora [Lena Raine](https://lena.fyi/)  quien ya había compuesto piezas musicales para la actualización 1.16, Nether Update, y que añade, junto a la compositora **Kumi Tanioka**, otras 8 piezas musicales a la banda sonora ademas de la del disco de vinilo.


A la hora de actualizar, se modifican muchas texturas de bloques ya existentes, se modifica la distribución de los distintos minerales ajustándolos al nuevo rango de alturas,…


Y hablando de los minerales. Otro cambio muy interesante es que ahora los bloques de minerales de hierro y cobre, ademas de aparecer en pequeños grupos, podrán aparecer en **grandes betas de minerales** compuestos por bloques del mineral, bloques de mineral crudo y bloques de toba o granito. Lo que permitirá minar una gran cantidad de mineral de hierro o cobre en poco tiempo.


 


 


 ¿Que os parecen todas estas novedades y cambios? Contadnos lo que opináis en los comentarios o en nuestras redes sociales en [Telegram](https://t.me/jugandoenlinux) y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


¡Me despido hasta la próxima!

