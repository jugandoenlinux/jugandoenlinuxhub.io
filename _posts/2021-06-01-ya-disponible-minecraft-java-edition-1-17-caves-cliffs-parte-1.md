---
author: Laegnur
category: "Exploraci\xF3n"
date: 2021-06-01 19:35:51
excerpt: "<p>@Mojang sigue ampliando contenido de @Minecraft con Caves &amp; Cliffs\
  \ Parte 1.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Minecraft/caves26cliffscoverart2.webp
joomla_id: 1313
joomla_url: ya-disponible-minecraft-java-edition-1-17-caves-cliffs-parte-1
layout: post
tags:
- actualizacion
- minecraft
title: 'Ya disponible Minecraft Java Edition 1.17: Caves & Cliffs Parte 1'
---
@Mojang sigue ampliando contenido de @Minecraft con Caves & Cliffs Parte 1.


 


¡Salve jugadores! Me llamo Laegnur y hoy tengo el placer de ser el elegido para traeros el anuncio del lanzamiento de la última actualización del juego Minecraft.


Y es que hoy, como digo, se lanza la ultima versión de este fantástico juego, que pese a no ser *opensource* es muy popular en nuestra plataforma.


Nos encontramos ante la versión 1.17, Caves & Cliffs. Y esta es una versión especial por varios motivos. Principalmente porque por fin **los desarrolladores de Mojang Studios han escuchado a la comunidad y se han animado a rehacer el sistema de generación de cuevas** que tantos años llevamos pidiendo. Y en segundo lugar, porque para esta actualización **los desarrolladores han intentado abarcar de más, viéndose sobrepasados y obligados a dividir el plan inicial en dos lanzamientos muy próximos.**


De esta forma, como decíamos, hoy se lanza la versión 1.17 conocida inicialmente como Caves & Cliffs y que finalmente se lanzara como Caves & Cliffs Parte 1. Y en un futuro próximo, a finales de año, tendremos la versión 1.18 Caves & Cliffs Parte 2.


Aunque ya se pueden probar muchos de los cambios venideros utilizando cualquiera de las betas anteriores a la 21w15a, momento en el que se separo esta versión en las dos actuales, o añadiendo un [datapack experimental](https://launcher.mojang.com/v1/objects/6b510a715701aec5e601c7966d87922a300e0c73/CavesAndCliffsPreview.zip) en la versión 1.17 que habilita algunos de los futuros cambios de la 1.18.


Vamos a echarles un vistazo a las novedades de esta nueva versión 1.17:


Bajo la superficie **nos podemos encontrar dos nuevos biomas, aunque de momento no se generaran de forma natural** y solo serán accesibles creando un mundo personalizado utilizando el datapack experimental. **Sus bloques si son accesibles utilizando un mundo creativo**. Ademas se añade una nueva formación natural en las cuevas y se modifica la generación de las Minas abandonadas.


* Cuevas kársticas: nueva generación de cuevas repletas de estalactitas y estalagmitas.
* Cuevas frondosas: cuevas con abundancia de vegetación subterránea.
* Geoda de amatista: son formaciones que nos encontraremos bajo tierra, compuesta por los nuevos bloques de amatista.
* Minas abandonadas: Se modifica la apariencia de las minas, incluyendo muchas mas pasarelas flotantes que antes, sostenidas utilizando cadenas o pilares de madera.


Veamos ahora **los nuevos bloques, objetos y criaturas** que darán vida a las nuevas cuevas y biomas:


* Rocas: Se nos añaden 6 nuevos bloques.
	+ Tierra enraizada: Un nuevo tipo de tierra rara que no se vera cubierta por hierba con el tiempo y que nos permitirá crear una nueva textura de caminos. Se encuentra en los biomas de cuevas frondosas, como el nombre indica, alrededor de las raíces de los arboles de azaleas.
	+ Nieve en polvo: Utilizando un Caldero en un bioma nevado, este se ira llenando de nieve en polvo y con un cubo podremos recogerla y poner donde queramos un bloque de nieve en polvo. Este bloque parece solido, pero permite atravesarlo, atrapando a los personajes en su interior, y provocando una bajada de temperatura dañina, acompañada por un nuevo overlay de congelación. Se puede utilizar como trampa para defenderse de los monstruos o otros jugadores.
	+ Basalto liso, Amatista y Calcita: Estos tres bloques solo los encontraremos formando geodas. Aunque el Basalto liso se puede conseguir también quemando en un horno Basalto del Nether. La Amatista la encontraremos en dos variedades, Amatista y Brotador de Amatista, que producirá al minarlo Cristales de Amatista.
	+ Espeleotema: Esta nueva roca sera la que forme las cuevas kársticas y ademas podremos encontrarlo raramente en cuevas normales. Si en el bloque por encima del bloque de espeleotema se encuentra una fuente de agua, el espeleotema comenzará a gotear y poco a poco ira formando una estalactita y una estalagmita hasta llegar a formar una columna de espeleotema.
	+ Pizarra profunda y sus variantes, pizarra profunda rocosa y pizarra profunda pulida: Es un bloque de características similares a la piedra, pero con textura mas oscura. Nos permite crear los mismos bloques que la piedra (Losas, escaleras, muros… ) y también nos permitirá crear herramientas de piedra. Se generará del nivel 16 hacia abajo, de forma aleatoria al igual que el resto de rocas.
	+ Toba: El mas inútil de los nuevos bloques. Una roca formada por cenizas volcánicas, mas lenta de minar que una roca normal y que no permite crear nada a partir de ella. Meramente ornamental.
* Metales:
	+ Hierro, oro, lapislázuli, redstone, carbón o diamante: Se ha cambiado la textura normal de estos minerales y se añade la opción de que además de las menas normales en roca, ahora nos encontremos una nueva variedad de estas menas en bloques de pizarra profunda.
	+ Cobre: Se añade este nuevo metal que se encontrara al igual que los demás en menas en roca o en pizarra profunda. Al fundirlo en un horno producirá lingotes de cobre y podremos agrupar estos en bloques de cobre. Con cobre podremos fabricar alguno de los objetos nuevos. A diferencia de los minerales clásicos, si dejamos un bloque de cobre a la intemperie este ira cambiando de estado a medida que se oxide por las lluvias.
	+ Bloque de metal crudo: Esto no es un nuevo metal, sino que se añadió un bloque nuevo para el hierro, oro y cobre, que a partir de ahora, ademas de fundirlo para crear lingotes, podremos agrupar 9 menas de ellos para crear un bloque de metal crudo, al igual que se puede hacer con el lapislázuli, el redstone o el carbón.
	+ Fragmento de amatista: Aunque no es un mineral como tal, ya que no se pueden crear lingotes a partir de él. Si es un nuevo bloque minable a partir de otro. Picando dentro de una geoda los cúmulos de amatistas recibiremos fragmentos de amatistas.
* Plantas:
	+ Liquen luminoso: Es un bloque nuevo similar al bloque de enredaderas pero que se generara de forma aleatoria en las cuevas y aportara algo de iluminación a las mismas.
	+ Bloque de musgo: Es un nuevo bloque que aparecerá en los biomas de cuevas frondosas. Usando polvo de uso sobre un bloque de musgo hará que este se expanda a los bloques de roca adyacentes, sustituyéndolos (menos las menas de minerales). Se puede obtener rápidamente con una azada, lo que abre una nueva puerta a la hora de minar en busca de minerales utilizando polvo de huevo y musgo para abrirse camino a través de la roca.
	+ Plantaforma: Una nueva planta, que de forma momentánea (apenas unos segundos) sostendrá los objetos que caigan sobre ella, incluido personajes, antes de dejarlos caer a través del bloque que ocupa. Aparecerá en los biomas de cuevas frondosas y la idea es utilizarla como decoración, o para crear circuitos de “parkour”.
	+ Flor de esporas: Una nueva flor decorativa que aparecerá en los biomas de cuevas frondosas, floreciendo en el techo y esparciendo partículas de esporas al aire, meramente atmosféricas.
	+ Bayas luminosas: Una nueva planta, similar a las Bayas dulces ya existentes. Se generaran de forma natural en los cofres en las minas abandonadas y en el nuevo bioma de cuevas frondosas, creciendo hacia abajo desde el techo de las mismas, arrojando luz a la cueva. Sirve de alimento.
	+ Azaleas y árbol de Azaleas: Un nuevo bloque decorativo que aparecerá en los biomas cuevas frondosas. Es un pequeño arbusto que crecerá y florecerá sobre los bloques de musgo. De forma natural en las cuevas frondosas se podrá hallar en su forma crecida en forma de árbol de Azaleas. Y mediante el uso de polvo de huevo sobre un arbusto de Azaleas, se podrá obtener igualmente un árbol, en cualquier otra cueva.
	+ Raíces colgantes: Bajo el árbol de azaleas, se generará un “árbol de raíces” con bloques de Tierra enraizada culminados en su punto mas bajo con un bloque de raíces colgantes.
* Objetos:
	+ Velas: utilizando los panales obtenidos de las abejas junto a la tela de araña, se pueden fabricar una nueva fuente de luz en forma de velas, que utilizando los distintos tintes se podrán personalizar en colores. Y al igual que los pepinillos de mar, se pueden agrupar hasta cuatro velas por bloque. Además se podrá poner una vela sobre las tartas.
	+ Pararrayos: creado a partir de cobre sirve para desviar los rayos de las tormentas.
	+ Catalejo: Utilizando lingotes de cobre con fragmentos de ametista podremos fabricar este objeto que nos permitirá ver a lo lejos.
	+ Cristal opaco: Al igual que utilizando bloques de cristal con tintes obtenemos cristales tintados, si utilizamos bloques de cristal con fragmentos de ametista obtendremos cristal opaco, que a diferencia de los cristales tintados, no dejara pasar la luz a través. Ademas si se rompe con la mano, a diferencia del cristal normal, no se romperá y podremos recuperarlo.
	+ Saco: Uno de los objetos mas útiles introducidos, a parte del catalejo, para mi gusto. Utilizando pieles de conejo e hilo podremos fabricar un saco, que es un contenedor con 64 espacios, donde podremos guardar items variados, ocupando así un único espacio de inventario. Utilizándolo dentro del inventario podremos sacar el último objeto introducido, y usándolo fuera del inventario sacaremos todos los objetos de golpe.
* Animales:
	+ Ajolote: Son criaturas acuáticas, pasivas hacia el jugador, pero que atacan a otras criaturas acuáticas. Pueden ser guiados con pescados o con cubos de pescados. Y pueden ser reproducidos igualmente con pescado.
	+ Cabra: Criaturas neutrales, que solo atacan si una criatura o un jugador en su campo de visión no se mueve durante los últimos 10 segundos. Al igual que las vacas, se pueden reproducir utilizando trigo y utilizando un cubo obtenemos un cubo de leche.
	+ Calamar brillante: Los calamares brillantes aparecerán en las fuentes de agua bajo tierra. Al igual que los calamares al morir sueltan un saco de tinta, que en este caso es brillante y servirá para iluminar carteles.


Como veis se añaden bastantes bloques, **pero los cambios mas gordos los veremos a final de año en la segunda parte de la actualización cuando se activen los nuevos biomas.**


¿Qué os parecen las mejoras introducidas en esta versión? ¿Sois jugadores habituales de Minecraft Java? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux) o nuestras salas de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


¡Me despido hasta la próxima!


 


 

