---
author: Pato
category: Estrategia
date: 2019-02-20 12:38:01
excerpt: <p>Se trata de un desarrollo entre @daedalic y Unfrozen Studios</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d43bc3919908507e9e38413a7691e181.webp
joomla_id: 975
joomla_url: iratus-lord-of-the-dead-llegara-a-linux-steamos-a-partir-del-proximo-verano-en-acceso-anticipado
layout: post
tags:
- indie
- proximamente
- rol
- estrategia
- estrategia-por-turnos
title: "'Iratus: Lord of the Dead' llegar\xE1 a Linux/SteamOS a partir del pr\xF3\
  ximo verano en acceso anticipado"
---
Se trata de un desarrollo entre @daedalic y Unfrozen Studios

Un nuevo desarrollo ha llamado nuestra atención. Se trata de '**Iratus: Lord of the Dead**', un juego de rol y estrategia por turnos que bebe directamente de juegos como Darkest Dungeon, Dungeon Keeper y Disciples, tanto en la temática como en la estética.


*La historia de Iratus: Lord of the Dead se centra en un nigromante encarcelado en una mazmorra hace siglos y que lucha por liberarse a toda costa. Tendremos que mejorar la guarida subterránea, crear súbditos no muertos de las partes del cuerpo de los muertos, tomar decisiones desafiantes y participar en intrincadas peleas tácticas donde cualquier error puede significar la derrota.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/OqMkW95JEW8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Características:


- Desarrolla y mejora tu guarida subterránea con poderes necrománticos.  
- Crea minions no muertos con las partes del cuerpo de los enemigos muertos.  
- Sistema de combate avanzado: conoce los puntos fuertes y los puntos débiles de tus enemigos para lograr la victoria.  
- Sistema de batalla por turnos con hasta 50 talentos de siervos originales.  
- Inteligente IA del enemigo, desafiando incluso a estrategas de RPG experimentados.  
- Tres tipos de talentos de nigromante para el juego: alquimia, talentos mágicos o habilidades tácticas.  
- Asaltar en territorio enemigo lleno de peligros y sorpresas.  
- Consecuencias irreversibles. Las características clásicas de roguelike incluyen la muerte permanente del personaje.  
- Gráficos 2D estilizados detallados en el espíritu de fantasía oscura.  
- Animación esquelética detallada realizada con espina.  
- Diseñado por Oles Ivanchenko: conocido videoblogger, especialista en mecánica de RPG y autor de uno de los manuales más influyentes de Darkest Dungeons.


Iratus: Lord of the Dead llegará a Linux/SteamOS a partir del próximo verano en acceso temprano tal y como [ha anunciado](https://steamcommunity.com/app/807120/discussions/1/3774483849443185088/) el estudio, e incluso tiene ya una demo disponible para aquellos que se suscriban al canal de Discord, aunque solo para la versión de Windows.


Según la [web de Steam del juego](https://store.steampowered.com/app/807120?snr=2_9_100006_100202_apphubheader), Iratus: Lord of the Dead no estará traducido al español, y sus requisitos son:


**MÍNIMO:**


+ **Procesador:** Intel Core 2 Duo 2.4 GHz
+ **Memoria:** 1 GB de RAM
+ **Gráficos:** Open GL 3.2+
+ **Almacenamiento:** 1 GB de espacio disponible


**RECOMENDADO:**


+ **Procesador:** Intel Core i5 3.0 GHz
+ **Memoria:** 2 GB de RAM
+ **Gráficos:** NVIDIA GeForce GTX 760, AMD Radeon R9 280X
+ **Almacenamiento:** 1 GB de espacio disponible


Si quieres saber más sobre Iratus: Lord of the Dead puedes visitar su [página web oficial](https://iratus.org/).


¿Que te parece lo nuevo de Daedalic? ¿Te gustan los juegos de acción estratégica por turnos?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux)


Seguiremos este desarrollo con atención, a la espera de confirmación de lo último de Red Hook Studios.

