---
author: leillo1975
category: Carreras
date: 2021-08-24 08:36:41
excerpt: "<p>El equipo de desarrollo la ha publicado tras una beta y una versi\xF3\
  n candidata.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/SD223b1-logo.webp
joomla_id: 1346
joomla_url: speed-dreams-alcanza-oficialmente-la-version-2-2-3
layout: post
tags:
- carreras
- ppa
- open-source
- flatpak
- racing
- speed-dreams
- sonlink
- appimage
title: "Speed Dreams alcanza oficialmente la versi\xF3n 2.2.3"
---
El equipo de desarrollo la ha publicado tras una beta y una versión candidata.


 Si haceis un poco de memoria, hace más o menos 3 meses [os anunciábamos el lanzamiento de la la beta 2.2.3]({{ "/posts/speed-dreams-estrena-version-con-una-beta-la-2-2-3-b1-a2" | absolute_url }}), donde podíamos ver el silencioso pero extenso trabajo realizado desde la anterior versión ([2.2.2]({{ "/posts/speed-dreams-disponible-en-flatpak-1" | absolute_url }})). Tras corregir multitud de fallos y publicar hace un mes una versión candidata, finalmente tenemos lista para su uso y disfrute la versión definitiva y oficlal.


Para quien no lo conozca, [Speed Dreams](http://www.speed-dreams.net) es un fork del también conocido y primigenio [TORCS](torcs.sourceforge.net), pero mucho más avanzado, donde el jugador disfrutará conduciendo montones de coches de diferentes categorias en múltiples pistas, y pudiendo disputar sesiones de **practica**, **carrera rápida**, **campeonatos** o incluso un **modo de carrera profesional**, además del modo **multijugador**, que permite el juego a **pantalla partida** y a través de la **LAN** (sobre internet por ahora es muy inestable).


En el artículo de la beta os detallabamos las novedades de este nuevo lanzamiento, pero los vamos a publicar de nuevo por si no lo pudisteis leer en aquel momento o no los recordais:


*-Nueva pista de Gran Premio, la de **Sao Paulo**, basada en el circuito José Carlos Pace, o más comunmente conocido como "Interlagos".*


*-Nueva categoría y colección de coches "**Monoposto 1"** (MP1) basada en los monoplazas de la Formula 1 de 2005.*


*-Nueva categoría y colección de coches "**1967 Grand Prix**" (67MP1), basada en la Formula 1 de 1967.*


*-Nuevos modelos en la categoría Supercars, el **Deckard Conejo RR** (basado en el Chevrolet Camaro SS de 2010) y el **Kanagawa Z35** (basado en Nissan 350z)*


*-Añadido soporte para **desgaste y degradación de neumáticos**, por ahora solo presente en Monoposto 1.*


*-**Nuevos robots de IA "shadow"** en múltiples categorias. Son más rápidos y arriesgan frenando más tarde que otros robots.*


*-Actualizado el código de los robots de IA "dandroid" y "USR".*


*-Se puede configurar el **tiempo real en los circuitos basándose en la meteorología** actual de las distintas localizaciones. (Por ejemplo, si en el momento justo llueve en Sao Paulo, el juego consulta el tiempo real en esa zona y lo reproduce en el juego).*


*-Nueva **sección de "setups" en los menús del garage** para configurar a nuestro gusto las diferentes opciones posibles en la mecánica de los automóbiles.*


![speed dreams 20 car ls1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/speed-dreams-20-car-ls1.webp)


Como veis, **el equipo de desarrollo ha trabajado durísimo a lo largo de este tiempo**, y nos complace decir que hemos participado ayudando en el proceso de testeo para corregir bugs y mejorar el funcionamiento del juego. También nos gustaría resaltar el **esfuerzo que se ha hecho por facilitar la distribución del juego gracias a la creación de paquetes binarios alternativos al código fuente**, pues en ese sentido los usuarios de GNU-Linux estábamos en desventaja con los de Windows o MacOS.


Ahora solo queda que disfruteis del proyecto, descargando el **código fuente** ([guia compilación](index.php/foro/tutoriales/108-como-compilar-la-ultima-version-de-speed-dreams-gracias-gnuxero26)) o la **AppImage** de los [repositorios oficiales en SourceForge](https://sourceforge.net/projects/speed-dreams/files/2.2.3/); en formato **Flatpak** desde [Flathub](https://flathub.org/apps/details/org.speed_dreams.SpeedDreams), o usando el [PPA de Xtradeb]({{ "/posts/xtradeb-un-ppa-de-juegos-libres-para-ubuntu-y-derivadas" | absolute_url }}) para instalarlo facilmente en Ubuntu y derivadas. Antes de terminar este artículo nos gustaría reconocer la colabaración de @**SonLink**, un destacado miembro de nuestra comunidad de JugandoEnLinux.com, con el proyecto, siendo responsable, entre otras cosas, de la creación de la AppImage el Flatpak del juego. Os dejamos con el video que creamos hace algún tiempo repasando las novedades de esta versión:  
  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/wwQxyzsAkUM" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


 


¿Conocíais este proyecto de Software Libre? ¿Qué os parecen las novedades introducidas en esta nueva versión? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux) o nuestras salas de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

