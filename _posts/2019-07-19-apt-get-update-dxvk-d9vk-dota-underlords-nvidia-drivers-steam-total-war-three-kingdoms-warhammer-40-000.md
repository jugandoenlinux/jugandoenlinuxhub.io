---
author: Pato
category: Apt-Get Update
date: 2019-07-19 17:45:17
excerpt: "<p>Repasamos lo m\xE1s destacado de la semana</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a2d776612246d598c744792a62711a11.webp
joomla_id: 1077
joomla_url: apt-get-update-dxvk-d9vk-dota-underlords-nvidia-drivers-steam-total-war-three-kingdoms-warhammer-40-000
layout: post
tags:
- steam
- nvidia
- total-war
- warhammer-40k-gladius
- dxvk
- d9vk
- dota-underlords
title: 'Apt-get update DXVK & D9VK & DOTA Underlords & Nvidia Drivers & Steam & Total
  War: Three Kingdoms & Warhammer 40.000 ...'
---
Repasamos lo más destacado de la semana

Como sabéis, hay semanas que no nos da la vida para poder escribir todo lo que quisiéramos, pero para eso tenemos esa sección especial "Apt-Get Update" que nos sirve de recopilación para que podamos actualizar la información y ponernos al día. Sin más preambulos, comenzamos:


- Los desarrollos de los "traductores" DXVK y D9VK siguen progresando y no paran de mejorar para ayudar a ejecutar los juegos mediante Wine. Podéis ver las novedades en [este enlace (DXVK)](https://github.com/doitsujin/dxvk/releases/tag/v1.3) y en [este enlace (D9VK)](https://github.com/Joshua-Ashton/d9vk/releases/tag/0.13f) respectivamente.


- Siguiendo con la "infraestructura", **Canonical va a distribuir los drivers privativos de Nvidia en sus versiones LTS de Ubuntu** (18.04, 20.04 y se supone que también en las derivadas) de modo que ya no será necesario tirar de PPA's de terceros (o al menos eso dice la teoría). Tenéis toda la información en el artículo de los chicos de [muylinux.com](https://www.muylinux.com/2019/07/12/ubuntu-lts-nvidia/).


- Continuamos con Steam. El cliente se ha actualizado con unas cuantas mejoras, como la visibilidad del cursor en ciertas circunstancias, mejoras en los diálogos, soporte en algunas tarjetas de sonido, shaders y algunas cosas mas. Tenéis la información en el anuncio oficial [en este enlace](https://store.steampowered.com/news/52467/).


- Seguimos con Valve, ya que su nuevo juego "Battle Chess" gratuito basado en el universo DOTA, "**DOTA Underlords**" sigue en fase beta pero introduce un buen puñado de nuevas características, y un "proto"-pase de batalla. Tenéis toda la información de las novedades [en este enlace](https://steamcommunity.com/games/underlords/announcements/detail/1600384168204543553), y sobre el pase de batalla en [este otro enlace](https://steamcommunity.com/games/underlords/announcements/detail/1600384168189707985).


-Vamos ahora con **Total War: THREE KINGDOMS** que introduce un nuevo DLC "**Eight Princess**" que estará disponible para Windows el día 8 de Agosto y que **Feral traerá a Linux un poco después**, lo que siempre es de agradecer. Tenéis el anuncio del DLC en [este enlace](https://store.steampowered.com/app/1102310/Total_War_THREE_KINGDOMS__Eight_Princes/), y el juego [en este otro](https://store.steampowered.com/app/779340/Total_War_THREE_KINGDOMS/).


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/NnRSGkfHpO0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


- **Warhammer 40.000: Gladius** también recibe un nuevo DLC con los "**Chaos Space Marines**" como novedad jugable. Tenéis toda la información del DLC [en este enlace](https://store.steampowered.com/app/1084820/Warhammer_40000_Gladius__Chaos_Space_Marines/), También en [Humble Bundle](https://www.humblebundle.com/store/warhammer-40000-gladius-chaos-space-marines?partner=jugandoenlinux) (afiliado), y el juego en [este otro enlace](https://www.humblebundle.com/store/warhammer-40000-gladius-relics-of-war?partner=jugandoenlinux) (afiliado de Humble Bundle).


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/mRooRyTz9M0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


- Gracias al incombustible Liam y [Gamingonlinux.com](https://www.gamingonlinux.com/articles/live-and-survive-as-a-dinosaur-in-path-of-titans-a-new-survival-game-coming-to-linux.14590) sabemos que hay en desarrollo un nuevo juego de supervivencia pero desde un punto de vista algo atípico, ya que se trata de sobrevivir en un mundo lleno de dinosaurios siendo... un dinosaurio. Con esta premisa se nos presenta "**Path of Titans**" un juego con un aspecto visual notable y que tiene pinta de ser un desarrollo a seguir. El juego no estará en Steam si no que apostará por su campaña de [crowdfunding](https://www.indiegogo.com/projects/path-of-titans#/) y su propio launcher. Tenéis toda la información en su [página web oficial](https://pathoftitans.com/).


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Bo85dFGnE08" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


- Para terminar, el juego indie de acción en vista isométrica "**Jupiter Hell**" aún en acceso anticipado recibe a Vulkan en su última actualización, además de guardado en la nube, logros y muchas cosas mas. Tenéis la información en su [post de actualización](https://steamcommunity.com/games/811320/announcements/detail/1611643799417055650) y el juego [en este enlace](https://store.steampowered.com/app/811320/Jupiter_Hell/).


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/uj1-KQEGmsM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


¿Que os parecen las novedades de esta semana? sabemos que nos dejamos muchas en el tintero. ¿Qué tal si nos lo cuentas los comentarios o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)?


 

