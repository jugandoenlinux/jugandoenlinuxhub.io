---
author: Serjor
category: Carreras
date: 2017-04-03 20:32:16
excerpt: <p>Las malas ventas no dan para un nuevo port</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d7a332411e14769f300879cf8b1d4da6.webp
joomla_id: 278
joomla_url: no-habra-version-para-linux-del-f1-2016
layout: post
tags:
- feral-interactive
- codemasters
- f1-2016
title: "No habr\xE1 versi\xF3n para Linux del F1 2016"
---
Las malas ventas no dan para un nuevo port

No nos gusta tener que dar noticias de este tipo, pero cuando toca, toca.


Esta mañana [Feral Interactive](http://www.feralinteractive.com) anunciaba en su cuenta de twitter el lanzamiento este mismo jueves del F1 2016 en su versión para Mac, y en la que no se hacía ningún comentario sobre Linux. [Liam](https://www.gamingonlinux.com/profiles/1), de [GOL](http://www.gamingonlinux.com), ha preguntado acerca de la versión del pingüino, y lamentablemente le han respondido con algo que no querríamos haber leído:


 



> 
> [@gamingonlinux](https://twitter.com/gamingonlinux) F1 2016 isn't coming to Linux. Sales of F1 2015 weren't strong enough to support a port of 2016. Penguin racers do have DiRT Rally though!
> 
> 
> — Feral Interactive (@feralgames) [April 3, 2017](https://twitter.com/feralgames/status/848848174886400000)



 


Y es que debido a las malas ventas que tuvo la edición del 2015, no se ve viable un port de la versión del 2016. Es una situación un tanto extraña, ya que el F1 2015 fue un juego duramente criticado por sus fallos y limitaciones, en todas sus versiones, lo que sin duda redujo la cantidad de compradores, por lo que nos preguntamos si en vez de haber portado la edición anterior hubieran portado el 2016, ya que recordemos la versión para windows del F1 2016 fue lanzado días antes que el port para Linux, quizás el número de ventas hubiera sido superior.


No obstante, esta es la realidad que tenemos, y nos tememos que tengamos que esperar algunos años hasta que volvamos a ver un nuevo título de F1 en nuestro sistema preferido.


 


Y a ti, ¿qué te ha parecido el anuncio de Feral? ¿Esperabas un port linuxero o eres más de Tux Kart? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

