---
author: Son Link
category: Estrategia
date: 2023-03-27 14:47:46
excerpt: "<p>Hace unas horas se ha publicado una nueva versi\xF3n de este gran cl\xE1\
  sico de estrategia de c\xF3digo abierto</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Warzone2100/portada_4.3.4.webp
joomla_id: 1531
joomla_url: warzone-2100-4-3-4
layout: post
title: Warzone 2100 4.3.4
---
Hace unas horas se ha publicado una nueva versión de este gran clásico de estrategia de código abierto


Ha sido lanzada una nueva versión de Warzone 2100, uno de los clásicos dentro de la estrategia y del código abierto, y que no para de recibir nuevas versiones con muchas mejoras y correcciones.


Veamos varios de los cambios:


* Añadido: Opciones separadas para Ban / Kick en el lobby del juego
* Añadido: Comando de lobby: /makeplayer
* Numerosas correcciones de cuelgues, problemas de limpieza y fugas de memoria
* Corrección: Que algunos mapas antiguos vuelvan a aparecer en las listas de mapas
* Corrección: Siempre se cancela la investigación al iniciarla en otro laboratorio
* Corrección: Faltan secciones en Tank Killer super cyborg
* Corrección: Ahora se detiene el tiempo de misión y se almacena cuando aparece la pantalla de resultados
* Cambio: Gamma 4: Eliminar las trampas de tanques intermedias que protegen al equipo Alfa y eliminar las unidades de reparación para tener en cuenta la nueva micro-AI de reparación.
* Cambio: Volar siempre los muros y las trampas de tanques durante las transferencias de Nexus en la campaña.
* Cambio: Mejorar las diferencias de dificultad en Gamma 9
* Cambio: Mejorar la fluidez de las misiones Alfa 1-3; aumentar el alcance de los proyectiles del jugador.
* Cambiar: Facilitar Gamma 1 activando las fábricas en función de la parte del valle por la que salgas.


También trae varios cambios en el multijugador:


* Añadido: Nueva arma Heavy Rocket Array que depende de MRA y HEAT Rocket Warhead Mk2
* Modificación: Se ha reducido la efectividad del modificador de artillería sobre búnkeres 40% a 20% y reducida la efectividad de los modificadores de artillería contra Orugas 40% a 30%
* Cambio: Hacer que la torreta de reparación pesada y la instalación de reparación aparezcan al mismo tiempo, mejorada la velocidad de reparación de la instalación en 10, y hacer que la torreta de reparación ligera cueste menos y se construya más rápido
* Cambio: Aumentados los PV del Cañón de Plasma a 125 para igualar los PV del Cañón de Asalto Gemelo
* Cambio: Mover el Láser de Pulso alrededor del Misil Serafín y eliminado el requisito de Mejora de Sensor Mk3
* Cambio: Aumentar los costes de la investigación posterior de MG y del Cañón de Asalto (Gemelo)
* Cambio: Reducido el tiempo de recarga de los Asesinos de Tanques al de los Lancer y mejorada la precisión a largo alcance de los Pods Cohete de 45% a 50%
* Cambio: El tiempo de recarga de la Aguja se iguala al de las últimas armas de cañón, se reducen los daños y el alcance del artillero del Super Rail, se reducen los daños y el radio de la Aguja/Rail VTOL y se aumentan los pesos
* Cambio: Incluir el Mortero en las Bases Avanzadas eliminando el requisito del módulo de fábrica
* Cambio: Reducido el tiempo y coste de investigación de la Torreta de Mando en un 50%
* Cambio: Reducido un 50% el tiempo y coste de la investigación de la mayoría de las defensas
* Cambio: Aumentada la recompensa de algunas mejoras de ingeniería a un 30%


Tenéis la lista completa de cambios [aquí](https://github.com/Warzone2100/warzone2100/raw/4.3.4/ChangeLog)


Y como siempre, os esperamos en nuestra canal de Telegram y en la sala [A Jugar](https://matrix.to/#/#a_jugar:matrix.org) en Matrix si os animáis a jugar

