---
author: Serjor
category: Noticia
date: 2017-12-07 10:08:38
excerpt: <p>Valve da visibilidad por plataformas a la lista de deseados</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cf4d07138ccd7b2e00d844e525c2ae86.webp
joomla_id: 567
joomla_url: cambios-en-steam-para-mejorar-la-informacion-de-los-juegos-deseados-por-los-usuarios
layout: post
tags:
- steam
- valve
title: "Cambios en Steam para mejorar la informaci\xF3n de los juegos deseados por\
  \ los usuarios"
---
Valve da visibilidad por plataformas a la lista de deseados

Amanecíamos esta mañana con un Tweet de Pier-Loup Griffais:


 



> 
> Just rolled out a feature to help Steam users signal their interest in ports to other platforms to developers. <https://t.co/Y5PqsfbhC4> [pic.twitter.com/0nwXYSColG](https://t.co/0nwXYSColG)
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [6 de diciembre de 2017](https://twitter.com/Plagman2/status/938541873328615424?ref_src=twsrc%5Etfw)


 


Y nos reportaban el [anuncio](http://steamcommunity.com/games/221410/announcements/detail/1475356649450732547) también por el canal de [Telegram](https://telegram.me/jugandoenlinux), y es que Valve ha introducido un cambio en la información que reportan a las desarrolladoras en lo que respecta a las listas de deseos de los usuarios.


Y es que el cambio introducido, aunque aparentemente pequeño, reporta a las desarrolladoras para qué plataformas desea tener el usuario el juego, es decir, ahora un estudio puede ver que un juego ha sido añadido a la lista de deseados para plataformas para las que no está desarrollando el juego:


![steam whislist platform](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SteamWhislist/steam-whislist-platform.webp)


Para tener esta visibilidad y que aparezca correctamente en los datos del juego, hay que **[actualizar las preferencias de steam](https://store.steampowered.com/account/preferences/)** (al fondo de todo) indicando para qué plataformas queremos ver juegos:


![steam platform](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SteamWhislist/steam-platform.webp)


Así que ya podemos añadir todos los juegos que **realmente** queramos que así mostraremos de manera real el número de usuarios interesados en el juego, de tal manera que no tendremos que ir foro a foro abriendo o apoyando los diferentes hilos que solicitan el port a GNU/Linux.


Y a ti, ¿qué te parece este cambio? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o Discord(<https://discord.gg/ftcmBjD>).

