---
author: Pato
category: Estrategia
date: 2018-05-22 11:38:45
excerpt: <p>El estudio ha confirmado a gamingonlinux el soporte para nuestro sistema</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f5a85333d553b860310d6f60f5af8288.webp
joomla_id: 744
joomla_url: lo-nuevo-de-paradox-interactive-imperator-rome-llegara-a-linux
layout: post
tags:
- proximamente
- estrategia
- paradox
title: "Lo nuevo de Paradox Interactive 'Imperator: Rome' llegar\xE1 a Linux"
---
El estudio ha confirmado a gamingonlinux el soporte para nuestro sistema

De nuevo gracias a Liam y a [gamingonlinux.com](https://www.gamingonlinux.com/articles/paradox-havent-decided-if-their-new-game-imperator-rome-will-be-on-linux.11822) tenemos la confirmación de que el próximo juego de estrategia de Paradox Interactive llegará a Linux. Se trata de '**Imperator: Rome**' [[web oficial](https://www.gameimperator.com/)] un juego de estrategia desarrollado por la propia **Paradox Development Studio** que ha sido anunciado recientemente en la PDXCON. En un principio no confirmaron el soporte para Linux, pero en un mail posterior a Liam le han confirmado que sí tienen planes para su lanzamiento en nuestro sistema favorito. Respecto al juego, poco más se sabe, salvo que teniendo el sello de Paradox apunta a un buen juego de estrategia y que acaban de lanzar un vídeo promocional que podéis ver a continuación:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/AGTifuEu6hw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 *Paradox Development Studio vuelve a la historia antigua con Imperator: Rome, un nuevo título sobre el crecimiento del poder Romano en el teatro del Mediterráneo. Unifica Italia y luego el mundo bajo las águilas de tus legiones. O dirige una monarquía en el Este que reclame la sombra de Alejandro. Esclavos, bárbaros y elefantes de guerra trae el pasado a la vida en Imperator: Rome. ¿Puedes ser el Cesar?*


Estaremos atentos a cualquier información que surja respecto a este nuevo 'Imperator: Rome'.


¿Te gustan los juegos de Paradox? ¿Que te parece este nuevo desarrollo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

