---
author: P_Vader
category: "Exploraci\xF3n"
date: 2022-02-19 11:22:04
excerpt: "<p>El RPG multijugador libre avanza mucho en calidad y novedades.<strong>\
  \ Habr\xE1 una fiesta multitudinaria a las 19:00 (UTC+1) hoy 19 de febrero en sus\
  \ servidores. \xA1Te esperamos!</strong></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Veloren/veloren_0-12.webp
joomla_id: 1431
joomla_url: veloren-llega-a-la-version-0-12
layout: post
tags:
- multijugador
- rpg
- mundo-abierto
- pixel-art
- mmorpg
- voxel
title: "Veloren llega a la versi\xF3n 0.12"
---
El RPG multijugador libre avanza mucho en calidad y novedades. **Habrá una fiesta multitudinaria a las 19:00 (UTC+1) hoy 19 de febrero en sus servidores. ¡Te esperamos!**


Tenemos nueva versión de [Veloren](https://veloren.net/) y fieles a su cita **organizarán una fiesta multitudinaria a las 19:00 (UTC+1) hoy 19 de febrero en su servidor dedicado**, para mostrarnos las novedades y de paso hacer una prueba de rendimiento masiva en el servidor. Deciros que como viene siendo habitual **JugandoEnLinux estará en la fiesta. Animaros a participar, pasaros por nuestros canales habituales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) si tenéis duda de como uniros a la partida con nosotros.**


Recuerda que Veloren es un RPG multijugador masivo, aunque si lo prefieres lo puedes jugara solo. Con un magnifico estilo gráfico pixelado a modo de voxeles y que paso a paso está demostrando una enorme calidad.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/Vz9pAOMgyn8" title="YouTube video player" width="720"></iframe></div>


Entre las novedades y cambios que podemos destacar son:


* Se han **añadido arbustos**, con un nuevo sistema para generar plantas más pequeñas parecidas a árboles en el mundo.
* **Nuevas cascadas** que lucen muy bien a lo largo de los ríos.
* Además ahora **los ríos hacen sonidos ambientales**, como el chapoteo y correr del agua claro.
* Nuevos **sombreadores (shaders) muy espectaculares**, destacando dentro del agua como en lagos o en sus profundidades. Hay una mejora en ciertos puntos de iluminación como linternas.
* Se puede **correr un poco por la pared**.
* Se han añadido muchos y variados **artrópodos**.
* El sistema de **montaje de animales y mascotas se ha renovado**, con muchas y bonitas animaciones. Incluso puedes hacer combate desde tu montura. Montar un animal como un caballo se ve mucho mas real.
* Nuevos **fuertes de Gnarling** ¡se aconseja ir en compañía para conquistarlos! Son muy duros estos personajes.
* Nuevos **barcos de vela**, aunque se necesita usar un comando para que aparezcan porque aun no está añadido por defecto.
* Se ha asignado una **tecla (clic en rueda de raton) para poder añadir marcadores en tu mapa**.
* Puntos de guardado (fogatas) en las ciudades.
* Puedes escabullirte con las armas desenvainadas.
* Nuevas compilaciones para Linux Aarch64 (¿para RaspberryPi?)
* Traducido al catalán.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/604JC5QdYQE" title="YouTube video player" width="720"></iframe></div>


En su [repositorio](https://gitlab.com/veloren/veloren/-/tags/v0.12.0) puedes encontrar la larga lista de cambios con mas detalle y mas técnico si cabe.


 


Para quien no sepa, **Veloren se puede instalar desde [flatpak en linux.](https://flathub.org/apps/details/net.veloren.veloren)**


Si quieres **asegurarte tener la ultima versión al instante, también puedes usar su instalador [Airshipper](https://github.com/veloren/airshipper/releases/latest/download/airshipper-linux.tar.gz)**. Descargalo, descomprimelo y ejecutalo. Si la interfaz gráfica te da problemas, ejecútalo en una terminal e igualmente se instalará, por ejemplo con: **`./airchipper update`**  y **`./airshipper start`**


 


Recuerda que estamos en [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) para cualquier duda que os surja sobre el juego.


 

