---
author: Pato
category: Rol
date: 2018-02-02 18:23:49
excerpt: "<p>El juego ya est\xE1 disponible en acceso anticipado, en modalidad \"\
  free to play\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1e6006496cec394e7da669516f53f970.webp
joomla_id: 634
joomla_url: tale-of-toast-un-juego-de-rol-multijugador-online-llegara-en-acceso-anticipado-el-23-de-febrero
layout: post
tags:
- accion
- indie
- proximamente
- rol
- mmorpg
title: "'Tale of Toast' un juego de Rol multijugador online llegar\xE1 en Acceso Anticipado\
  \ el 23 de Febrero (actualizado)"
---
El juego ya está disponible en acceso anticipado, en modalidad "free to play"

**(Actualización)** Ya tenemos disponible este 'Tale of Toast', un MMORPG al estilo de la vieja escuela que nos llega [no sin algunos problemas](http://steamcommunity.com/gid/[g:1:30651748]/announcements/detail/1672396103799709591), ya que parece que no esperaban una afluencia de jugadores tan grande como están teniendo, y los servidores andan saturados. Pero viendo el mimo y la seriedad de los desarrolladores parece que todo irá volviendo a la normalidad. Además, hay que recordar que el juego llega en estado de acceso anticipado por lo que es de esperar que surjan problemas y hayan cosas por pulir. Aún así, y teniendo en cuenta que no está traducido al español, si te llama la atención puedes instalarlo desde el enlace que hay al final de este artículo y me cuentas qué tal es ya sea en los comentarios o en los canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD). Total, es gratis. ¿Verdad? ;-)




---


**(Artículo original)**


Un nuevo MMORPG ha llamado mi atención. 'Tale of Toast' [[web oficial](https://taleoftoast.com/)] es un juego de Rol multijugador online (MMORPG) inspirado en los clásicos del género y que trata de rescatar la esencia de estos haciéndolo divertido y desafiante.


Con esta premisa, los desarrolladores (solo dos personas) nos prometen un mundo donde jugar en modos PvP con un enfoque de combates de componente táctico, relaciones sociales con otros personajes, comercio, mazmorras generadas proceduralmente y mucho más.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/cztgyjhn8D4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Los desarrolladores inciden en que el enfoque del juego será "free to play" sin componentes de "pay to win", tan solo se podrán comprar mejoras estéticas dentro del juego. Por otra parte tendremos disponibles diferentes clases de personajes donde elegir, quests, un vasto mundo y mucho más.


Los requisitos publicados a día de hoy son:


**Mínimo:**  

+ **SO:** Ubuntu 16.04.3 LTS
+ **Procesador:** Intel Core i3
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** Intel HD Graphics 3000 o equivalente
+ **Red:** Conexión de banda ancha a Internet
+ **Almacenamiento:** 4 GB de espacio disponible


**Recomendado:**  

+ **SO:** Ubuntu 17.10.1
+ **Procesador:** Intel i5 o superior
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** NVIDIA® GeForce® GTX 750ti, ATI Radeon™ HD 7850 o superior
+ **Red:** Conexión de banda ancha a Internet
+ **Almacenamiento:** 4 GB de espacio disponible




'Tale of Toast' estará disponible en acceso anticipado el próximo 23 de Febrero con versión Linux confirmada, aunque no tendrá traducción al español.


Puedes ver toda la información de 'Tale of Toast' en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/640150/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

