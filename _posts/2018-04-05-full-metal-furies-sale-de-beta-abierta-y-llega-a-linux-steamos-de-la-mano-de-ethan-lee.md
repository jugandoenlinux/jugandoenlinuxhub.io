---
author: Pato
category: "Acci\xF3n"
date: 2018-04-05 20:04:14
excerpt: "<p>Adem\xE1s recibe una actualizaci\xF3n a\xF1adiendo un sistema de Lobby</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e7a6f6b910c37abcb54adf404e3a392a.webp
joomla_id: 705
joomla_url: full-metal-furies-sale-de-beta-abierta-y-llega-a-linux-steamos-de-la-mano-de-ethan-lee
layout: post
tags:
- accion
- indie
- aventura
- rol
- cooperativo
title: Full Metal Furies sale de beta abierta y llega a Linux/SteamOS de la mano de
  Ethan Lee
---
Además recibe una actualización añadiendo un sistema de Lobby

Ya informamos [hace un par de meses](index.php/homepage/generos/accion/item/754-full-metal-furies-de-los-creadores-de-rogue-legacy-llegara-dentro-de-unas-horas-en-una-suerte-de-beta-abierta) de la llegada de este 'Full Metal Furies' a Linux/SteamOS en una especie de beta abierta de la mano del conocido porter Ethan Lee. Ahora por fin ha llegado el lanzamiento oficial en la última actualización del juego que llega a su versión 1.1.0.


El propio Ethan nos lo hizo saber mediante un tweet, haciéndonos llegar además las novedades de esta versión:



> 
> Full Metal Furies is now available for Linux and macOS: <https://t.co/mIhwg86hng>
> 
> 
> — Ethan Lee (@flibitijibibo) [April 3, 2018](https://twitter.com/flibitijibibo/status/981211018457174018?ref_src=twsrc%5Etfw)



 Aparte de los arreglos pertinentes, el juego trae como principal novedad la inclusión de un sistema de lobby para poder acceder a las partidas de un modo más dinámico ya que permite entrar en partidas con jugadores de forma aleatoria. Esta función llega aún en fase beta. Podéis ver todos los cambios en el post del anuncio del parche [en este enlace](http://steamcommunity.com/games/416600/announcements/detail/1645377767711097389).


*FULL METAL FURIES [[web oficial](http://cellardoorgames.com/our-games/full-metal-furies/)] enfatiza el trabajo en equipo con un original sistema de combate en el que todos son importantes. Trabaja codo con codo para derrotar a enemigos especiales, encadena combos orgánicos para infligir un daño enorme y salva a un mundo devastado por la guerra que está al borde de la extinción. Juega en el sofá o en línea con amigos, ¡y disfruta de una fiesta para 4 jugadores! O juega en el modo para un jugador y alardea de tus habilidades con el sistema de cambio rápido de 2 personajes que conserva todos los matices del modo multijugador.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/6wUFjPdDo6Q" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Los requisitos para mover el juego son:


**Mínimo:**  

+ **OS:** glibc 2.17+, 32/64-bit
+ **Procesador:** 2.0 Ghz
+ **Memoria:** 2 MB RAM
+ **Graficos:** 8800 GTS, HD 2900 Pro, Intel HD 530
+ **Almacenamiento:** 900 MB
+ **Notas Adicionales:** Dispositivos SDL_GameController totalmente soportados


**Recomendado:**  

+ **OS:** glibc 2.17+, 32/64-bit
+ **Procesador:** 2.4 Ghz
+ **Memoria:** 2 MB RAM
+ **Graficos:** GTX 285, HD 6850, Intel Iris 6200
+ **Almacenamiento:** 900 MB
+ **Notas Adicionales:** Dispositivos SDL_GameController totalmente soportados


Si te gustan las buenas propuestas multijugador, no puedes dejar pasar este Full Metal Furies. ¡Viniendo de Ethan Lee la calidad del port está garantizada!
Tienes Full Metal Furies traducido al español en su página de Steam:
 
<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/416600/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>

