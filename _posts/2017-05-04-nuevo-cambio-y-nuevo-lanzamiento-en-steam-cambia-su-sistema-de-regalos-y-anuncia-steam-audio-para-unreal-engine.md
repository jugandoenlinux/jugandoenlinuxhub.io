---
author: Pato
category: Software
date: 2017-05-04 07:12:07
excerpt: "<p>Valve sigue con los cambios y las actualizaciones de software. \xBFEstar\xE1\
  \ cerca la anunciada renovaci\xF3n de su tienda?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6cdb4ac6ccf86fc9922b1b1ecf5faa0d.webp
joomla_id: 303
joomla_url: nuevo-cambio-y-nuevo-lanzamiento-en-steam-cambia-su-sistema-de-regalos-y-anuncia-steam-audio-para-unreal-engine
layout: post
tags:
- steam
- steam-audio
- unreal-engine
title: 'Nuevo cambio y nuevo lanzamiento en Steam: cambia su sistema de regalos y
  anuncia Steam Audio para Unreal Engine'
---
Valve sigue con los cambios y las actualizaciones de software. ¿Estará cerca la anunciada renovación de su tienda?

Si ayer nos teníamos que hacer eco de los [cambios en el servicio de soporte](index.php/homepage/editorial/item/423-steam-mejor-su-servicio-de-soporte) de Steam, hoy nos desayunamos con nuevos anuncios desde la tienda de Valve. En concreto, Steam [ha cambiado la forma de hacer regalos a través de la tienda](https://steamcommunity.com/games/593110/announcements/detail/1301948399254001159), de modo que ahora ya no pasarán por el inventario si no que se entregarán directamente en la cuenta del destinatario. Tampoco estará disponible la opción de enviar los regalos a través del correo electrónico, lo que parece un intento por evitar que las claves se pierdan por el camino.


Desde Steam aseguran que ahora será posible comprar un regalo con antelación y programar su entrega de forma que el destinatario lo recibirá siempre a tiempo. Además, si el destinatario ya tiene el regalo o lo rechaza, este se tramitará de forma que ahora no regresará a tu inventario, si no que se te abonará en tu cuenta. 


Desde hace un tiempo se rumorea que Steam quiere permitir el entregar juegos sin necesidad de una clave en ciertas circunstancias para evitar que dichas claves terminen en "mercados grises". De esta forma, los estudios podrían entregar juegos para beta testing o para reviews sin que se pierdan claves por el camino, lo que sería un claro intento por limitar el actual problema de las claves. Con los cambios que están haciendo ahora con los regalos parece que quieren ir por este camino.


#### Steam Audio + Unreal Engine


Por otra parte, Valve ha anunciado el lanzamiento de la primera beta de Steam Audio para Unreal Engine 4. El plugin ahora estará integrado por defecto en el motor de forma que ya se puede utilizar a partir de la versión 4.16 Preview 2. Esto permitirá a los desarrolladores crear experiencias de audio inmersivas para juegos y entornos de realidad virtual.


El plugin es gratuito y no está sujeto a ningun sistema operativo concreto o sistema de realidad virtual.


Puedes ver el anuncio donde podrás descargar las actualizaciones del motor necesarias y la documentación, así como una descripción mucho más exhaustiva [en este enlace](http://steamcommunity.com/games/250820/announcements/detail/1292941199999967977).

