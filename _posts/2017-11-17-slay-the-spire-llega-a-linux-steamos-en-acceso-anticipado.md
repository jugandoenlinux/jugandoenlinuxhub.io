---
author: Pato
category: Estrategia
date: 2017-11-17 12:11:24
excerpt: "<p>Construye tu mazo de cartas y l\xE1nzate a la lucha</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ef76ec555f88001101e0e225c3aa91e9.webp
joomla_id: 544
joomla_url: slay-the-spire-llega-a-linux-steamos-en-acceso-anticipado
layout: post
tags:
- indie
- acceso-anticipado
- estrategia
- cartas
title: '''Slay the Spire'' llega a Linux/SteamOS en acceso anticipado'
---
Construye tu mazo de cartas y lánzate a la lucha

Buenas noticias para aquellos que gustan de confeccionar mazos de cartas y entablar combates frente a una amplia gama de monstruos. 'Slay the Spire' es, según sus creadores "el mejor juego para un jugador de creación de mazos que han podido crear".


Para ello ha fusionado el juego de cartas clásico con el "roguelike" para conseguir este resultado único.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/NHRpS2DzIAI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Entre sus características destacadas están:


- Dos personajes principales cada uno con su configuración única de cartas. (Hay planeados más)


- Más de 200 cartas implementadas


- Más de 50 combates únicos


- Más de 100 items diferentes para encontrar


- Niveles generados proceduralmente


'Slay the Spire' no está disponible en español, pero si te interesan los juegos de cartas con un toque diferente y no es problema para ti, puedes encontrarlo en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/646570/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

