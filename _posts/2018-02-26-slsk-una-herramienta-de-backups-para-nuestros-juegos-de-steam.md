---
author: leillo1975
category: Software
date: 2018-02-26 08:24:16
excerpt: "<p>Gracias al habitual paseo por nuestras muchas webs de referencia, nos\
  \ hemos topado en<a href=\"https://www.omgubuntu.co.uk/2018/02/steam-linux-backup-tool-slsk\"\
  \ target=\"_blank\" rel=\"noopener\"> OMGUbuntu!</a> con esta utilidad que viene\
  \ a <strong>facilitarnos la tarea de realizar copias de seguridad de nuestros juegos</strong>.\
  \ La herramienta en cuesti\xF3n se llama <a href=\"https://github.com/supremesonicbrazil/SLSK\"\
  \ target=\"_blank\" rel=\"noopener\">SLSK</a> (<em>Steam Linux Swiss-army Knife</em>)\
  \ y se trata de un proyecto de <strong>software libre</strong> que podemos actualmente\
  \ encontrar en Github. Con este programa podremos realizar y restaurar copias de\
  \ seguridad de nuestras partidas salvadas y configuraciones en cualquier distribuci\xF3\
  n Linux.</p>\r\n<p>SLSK es una herramienta <span id=\"result_box\" class=\"\" lang=\"\
  es\"><span class=\"\">que se basa en una <strong>base de datos mantenida por la\
  \ comunidad</strong>, y que contiene informaci\xF3n sobre las rutas de almacenamiento\
  \ y configuraci\xF3n de varios juegos para Steam en Linux. La mayor parte de los\
  \ juegos suelen guardar sus ajustes y partidas en el mismo lugar (normalmente en<em>\
  \ [usuario]/.local/share</em> y<em> [usuario]/.config</em> ), pero hay ocasiones\
  \ donde, seg\xFAn el desarrollador del juego, esta carpeta y archivos pueden variar.\
  \ En ese lugar es donde entra en acci\xF3n esa base de datos para realizar correctamente\
  \ las copias. Gracias a esto, a medida que vayan saliendo nuevos juegos, esta base\
  \ de datos se actualizar\xE1. Por el momento <a href=\"https://github.com/supremesonicbrazil/SLSK/blob/master/MISSINGLIST.md\"\
  \ target=\"_blank\" rel=\"noopener\">a\xFAn existen bastantes juegos que no tienen\
  \ soporte o este aun no est\xE1 completo</a> por que no se ha introducido en esta\
  \ base de datos, pero con el tiempo se espera que abarque todo el cat\xE1logo. Si\
  \ quereis colaborar enviando datos sobre juegos podeis ver como hacerlo <a href=\"\
  https://github.com/supremesonicbrazil/SLSK/blob/master/CONTRIBUTING.md\" target=\"\
  _blank\" rel=\"noopener\">aqu\xED</a>.<br /></span></span></p>\r\n<p>Su <strong>interfaz\
  \ y uso son muy sencillos</strong> y tan solo debemos se\xF1alar la carpeta donde\
  \ tenemos los juegos instalados y la que usaremos para hacer las copias de seguidad.\
  \ Luego, mediante un conjunto de botones podremos marcar los juegos a copiar o restaurar,\
  \ pudiendo seleccionar las partidas, conifguraciones, e incluso el juego entero\
  \ para no tener que volver a descargarlo. El juego por ahora no guarda partidas\
  \ de juegos externos a Steam, pero su desarrollador no lo descarta aunque reconoce\
  \ que no est\xE1 entre sus prioridades.</p>\r\n<p>Seguro que estais deseando instalar\
  \ esta fant\xE1stica herramienta, y para ello, por el momento no existe ning\xFA\
  n binario ejecutable o repositorio, pero<strong> se puede compilar facilmente</strong>\
  \ siguiendo estos simples pasos:</p>\r\n<p>-En primer lugar necesitas instalar las\
  \ dependencias para compilar el programa:</p>\r\n<p><code><span style=\"font-size:\
  \ 1rem;\">sudo apt install&nbsp;</span><span style=\"font-size: 1rem;\">sqlite3</span>\
  \ <span style=\"font-size: 1rem;\">qt5-default</span><span style=\"font-size: 1rem;\"\
  ></span><span style=\"font-size: 1rem;\">g++</span><span style=\"font-size: 1rem;\"\
  ></span><span style=\"font-size: 1rem;\">make</span></code><span style=\"font-size:\
  \ 1rem;\"><code>&nbsp;qt5-qmake git</code><br /></span></p>\r\n<p>-Despu\xE9s debemos\
  \ descargar el c\xF3digo fuente:</p>\r\n<p><code>git clone <a href=\"https://github.com/supremesonicbrazil/SLSK\"\
  >https://github.com/supremesonicbrazil/SLSK</a></code></p>\r\n<p>-Accederemos al\
  \ directorio donde se encuentra:</p>\r\n<p><code><span>cd ~/SLSK</span></code><br\
  \ /><br />-....y por \xFAltimo compilaremos la app:</p>\r\n<p><code><span>./BUILD.sh\
  \ &amp;&amp; sudo ./INSTALL.sh</span></code></p>\r\n<p>&nbsp;</p>\r\n<p>\xBFVerdad\
  \ que tiene buena pinta el proyecto? Esperemos que con el tiempo llegue a abarcar\
  \ la mayor parte del cat\xE1logo disponible para nuestro sistema en Steam, pues\
  \ lo que ofrece es muy \xFAtil para todos los jugones Linuxeros. Seguiremos pendientes\
  \ del proyecto por si se producen novedades destacadas.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/30acc056eefaa17655fb482e0f42c3aa.webp
joomla_id: 658
joomla_url: slsk-una-herramienta-de-backups-para-nuestros-juegos-de-steam
layout: post
tags:
- steam
- slsk
- steam-linux-swissarmy-knife
- backup
- software-libre
title: SLSK, una herramienta de Backups para nuestros juegos de Steam.
---
Gracias al habitual paseo por nuestras muchas webs de referencia, nos hemos topado en [OMGUbuntu!](https://www.omgubuntu.co.uk/2018/02/steam-linux-backup-tool-slsk) con esta utilidad que viene a **facilitarnos la tarea de realizar copias de seguridad de nuestros juegos**. La herramienta en cuestión se llama [SLSK](https://github.com/supremesonicbrazil/SLSK) (*Steam Linux Swiss-army Knife*) y se trata de un proyecto de **software libre** que podemos actualmente encontrar en Github. Con este programa podremos realizar y restaurar copias de seguridad de nuestras partidas salvadas y configuraciones en cualquier distribución Linux.


SLSK es una herramienta que se basa en una **base de datos mantenida por la comunidad**, y que contiene información sobre las rutas de almacenamiento y configuración de varios juegos para Steam en Linux. La mayor parte de los juegos suelen guardar sus ajustes y partidas en el mismo lugar (normalmente en *[usuario]/.local/share* y *[usuario]/.config* ), pero hay ocasiones donde, según el desarrollador del juego, esta carpeta y archivos pueden variar. En ese lugar es donde entra en acción esa base de datos para realizar correctamente las copias. Gracias a esto, a medida que vayan saliendo nuevos juegos, esta base de datos se actualizará. Por el momento [aún existen bastantes juegos que no tienen soporte o este aun no está completo](https://github.com/supremesonicbrazil/SLSK/blob/master/MISSINGLIST.md) por que no se ha introducido en esta base de datos, pero con el tiempo se espera que abarque todo el catálogo. Si quereis colaborar enviando datos sobre juegos podeis ver como hacerlo [aquí](https://github.com/supremesonicbrazil/SLSK/blob/master/CONTRIBUTING.md).  



Su **interfaz y uso son muy sencillos** y tan solo debemos señalar la carpeta donde tenemos los juegos instalados y la que usaremos para hacer las copias de seguidad. Luego, mediante un conjunto de botones podremos marcar los juegos a copiar o restaurar, pudiendo seleccionar las partidas, conifguraciones, e incluso el juego entero para no tener que volver a descargarlo. El juego por ahora no guarda partidas de juegos externos a Steam, pero su desarrollador no lo descarta aunque reconoce que no está entre sus prioridades.


Seguro que estais deseando instalar esta fantástica herramienta, y para ello, por el momento no existe ningún binario ejecutable o repositorio, pero **se puede compilar facilmente** siguiendo estos simples pasos:


-En primer lugar necesitas instalar las dependencias para compilar el programa:


`sudo apt install sqlite3 qt5-defaultg++make``qt5-qmake git`  



-Después debemos descargar el código fuente:


`git clone <https://github.com/supremesonicbrazil/SLSK>`


-Accederemos al directorio donde se encuentra:


`cd ~/SLSK`  
  
-....y por último compilaremos la app:


`./BUILD.sh && sudo ./INSTALL.sh`


 


¿Verdad que tiene buena pinta el proyecto? Esperemos que con el tiempo llegue a abarcar la mayor parte del catálogo disponible para nuestro sistema en Steam, pues lo que ofrece es muy útil para todos los jugones Linuxeros. Seguiremos pendientes del proyecto por si se producen novedades destacadas.

