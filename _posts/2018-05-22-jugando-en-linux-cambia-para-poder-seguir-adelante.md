---
author: Jugando en Linux
category: Editorial
date: 2018-05-22 16:12:01
excerpt: "<p>Nos adaptamos a la nueva ley europea de protecci\xF3n de datos y aprovechamos\
  \ para cambiar y tratar de sobrevivir \"de otra forma\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b221b486bfa5e70a3ef7b7059063a568.webp
joomla_id: 743
joomla_url: jugando-en-linux-cambia-para-poder-seguir-adelante
layout: post
title: Jugando en Linux cambia para poder seguir adelante
---
Nos adaptamos a la nueva ley europea de protección de datos y aprovechamos para cambiar y tratar de sobrevivir "de otra forma"

Creo que ya es sabido de forma general que la nueva ley europea de protección de datos (RGPD para los amigos) entrará en vigor el próximo día 25 de este mes, es decir esta misma semana en la que estamos.


Esta nueva ley afecta de manera significativa a las webs que almacenamos cualquier dato de carácter personal, ya sean direcciones de correo electrónico, nombre y apellidos o cualquier otro dato, incluyendo también parámetros como cookies y cualquier otro plugin o widget que haga uso de tecnologías de rastreo.


Siendo así, En Jugando en Linux vamos a implementar las mejoras necesarias para daros todo el control sobre los datos que se almacenan en la web de vosotros, incluyendo las opciones de obligado cumplimiento por la ley.


Pero no nos vamos a quedar ahí. Aprovechando que nos metemos en harina, vamos a tomar ciertas decisiones que cambiarán drásticamente nuestro modo de gestión para intentar hacer la web mas atractiva a nuestros visitantes y a la vez tratar de "sobrevivir" de mejor manera que hasta ahora. Nuestro objetivo sigue siendo el mismo que desde que comenzamos esta aventura: Conseguir que la web sea auto-sostenible, y si es posible poder "recompensar" de algún modo todo el trabajo que hacemos para llevar este proyecto adelante.


Los puntos que vamos a tocar son:


1. **Implementar los sistemas necesarios para cumplir con la nueva ley**, introduciendo las opciones necesarias para vuestro propio control de vuestros datos almacenados por la web, ya sea mediante formularios, ventanas de información o gestión desde vuestro propio perfil de usuario.
2. **Eliminamos widgets que obtengan rastreo hacia otras plataformas**. Tanto Facebook como Twitter "rastrean" cualquier actividad a través de los widgets que ofrecen su información por lo que vamos a eliminarlos, aunque mantendremos todos los enlaces en el menú de la web hacia nuestros canales en esas plataformas para que todo el que nos quiera seguir por allí pueda hacerlo.
3. **Dejamos de tener publicidad**. El rastreo de Adsense implica el tener que responsabilizarnos de la información que ese sistema realiza, y como hace tiempo que queremos dejar de tener publicidad, por fin vamos a eliminar todo rastro de ella de la web.
4. **Dejamos de emplear el sistema de rastreo de Google para analytics**. Dado que realmente no necesitamos una métrica (si no vamos a tener publicidad no tiene sentido) para "rastrear" como va la publicidad, no necesitamos utilizar Google Analytics. Con las propias estadísticas de nuestro servidor nos basta para saciar nuestra curiosidad sobre vuestras visitas a la web, y de todas formas siempre tenemos la posibilidad de implementar alternativas que no implicarían el rastreo por terceros.
5. **Abriremos nuestra propia campaña de micromecenazgo**. Dado que la publicidad ya no nos reporta beneficios, vamos a mantener la posibilidad de que podáis aportar lo que consideréis oportuno vía PayPal como hasta ahora, pero además abriremos una campaña de financiación en alguna conocida plataforma de micromecenazgo. Ahora más que nunca necesitaremos que nos ayudéis a mantenernos online. Daremos más detalles sobre esto más adelante.
6. **Comenzaremos a utilizar links de afiliados**. Sabemos que muchos de vosotros no podéis aportar vía Paypal o por plataformas de micromecenazgo, por lo que vamos a asociarnos con tiendas online de videojuegos para que si queréis, podáis comprar vuestros juegos desde nuestros enlaces y así nos ayudareis a mantener la web. A vosotros os saldrá igual de precio, y a nosotros nos reportará algún beneficio.


Esto es todo de momento. Agradecer desde estas líneas a todos los que día a día nos apoyan ya sea aportando algún dinero, colaborando con artículos, con la organización de partidas y tornemos y a todos los que en los vídeos y streamings ponéis el banner y la información de jugandoenlinux.com. Toda ayuda es bienvenida para tratar de mantener este proyecto, y trataremos de corresponder con novedades muy pronto. Esperamos que los cambios que vamos a implementar os gusten y hagan de Jugando en Linux una web que sea totalmente de vuestro agrado.


¡Seguimos adelante!


El equipo de jugandoenlinux.com Leo, Serjor y Pato

