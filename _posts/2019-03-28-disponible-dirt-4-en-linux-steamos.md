---
author: leillo1975
category: Carreras
date: 2019-03-28 11:00:09
excerpt: <p>@feralgames acaba de lanzar oficialmente el juego</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/76ba2d1d2b5528e3c2f8cce188b4d8b0.webp
joomla_id: 1006
joomla_url: disponible-dirt-4-en-linux-steamos
layout: post
tags:
- feral-interactive
- vulkan
- codemasters
- port
- dirt-4
title: Disponible DIRT 4 en Linux/SteamOS
---
@feralgames acaba de lanzar oficialmente el juego

Finalmente el día ha llegado, y Feral Interactive, fieles a sus promesas, nos anuncian la esperada llegada de este título tal y como habían dicho a principios de semana. El port de DIRT 4, que en principìo **se esperaba para finales del mes que viene**, ha llegado para amenizarnos este comienzo de primavera con las carreras más alocadas y polvorientas, fieles al estilo que marcaron con anteriores entregas, que por desgracia no pudimos disfrutar en nuestro sistema. El desembarco definitivo de DIRT 4 en nuestra plataforma ha llegado con el anuncio realizado por parte de Feral en las RRSS, tal y como podeis ver en este tweet:



> 
> Be fearless in DiRT 4, out now for macOS and Linux.  
>   
> Strap in, rev up, and buy from the Feral Store: <https://t.co/1hzUfdXg3f>  
>   
> Or the Mac App Store: <https://t.co/4zhBJl248u> [pic.twitter.com/annsbvC3qW](https://t.co/annsbvC3qW)
> 
> 
> — Feral Interactive (@feralgames) [March 28, 2019](https://twitter.com/feralgames/status/1111223949592014850?ref_src=twsrc%5Etfw)



Es imporante que no confundais este juego como una continuación del aclamado y genial **DIRT Rally**, [juego que analizamos hace algún tiempo](index.php/homepage/analisis/20-analisis/362-analisis-dirt-rally), y que tantos buenos momentos nos ha dado, cosa que sabreis si sois miembros de nuestra comunidad. En este caso DIRT 4 sigue la estela de **Colin McRae DIRT**, **Colin McRae DIRT 2** y ****DIRT 3**** siguiendo una **vertiente mucho más desenfadada y arcade** que DIRT Rally, que es un simulador. Pero que esto no os engañe, podremos ajustar el nivel de dificultad y encontrar una experiencia igual que este último si lo que nos va son las sensaciones realistas. Si por lo contrario queremos pasar un buen rato sin compliacaciones podremos disfrutar del juego en modo Arcade. El juego además ofrece una variedad en cuanto a disciplinas disponibles, pudiendo participar en carreras de **Rally Clásico**, **Rally Cross** (del que tiene licencia oficial)  y **Land Rush**. También permite, y esto es algo muy a tener en cuenta, **crear nuestras propias etapas** eligiendo entre diferentes variables de dificultad, escenario y meteorología, generando el juego la etapa automáticamente.


Según la **descripción** proporcionada por el area de prensa de Feral, en DIRT 4 nos vamos a encontrar:


*Acepta el desafío de las carreras todo terreno en el emocionante juego de rally de Codemasters.*


*DiRT® 4™ ofrece la adrenalina del deporte de motor todo terreno con una combinación intensa de las disciplinas de carreras favoritas, desde las carreras de Rally y Landrush hasta el Campeonato Mundial de Rallycross de la FIA.*


*Elije entre una gran selección de coches de rally, camiones, buggies y crosskart, y compite con ellos en las rutas de rally más exigentes del mundo, desde los senderos del bosque nevado de Suecia hasta las pistas de grava abiertas de Michigan.*


*50+ DE LOS COCHES TODO TERRENO MÁS POTENTES JAMÁS CREADOS Incluyendo el Ford Fiesta R5, Mitsubishi Lancer Evolution VI, Subaru WRX STI NR4 y Audi Sport Quattro S1 E2.*


*5 DESAFIANTES LUGARES ÚNICOS Despierte una tormenta en Värmland en Suecia, Fitzroy en Australia, Tarragona en España, Michigan en los Estados Unidos y Powys en Gales.*


*CREA TUS PROPIAS RUTAS DE RALLY Use la herramienta ‘Your Stage’ para crear millones de rutas de rally en función de su elección de ubicación y parámetros de ruta.*


*APRENDE RÁPIDO EN LA ACADEMIA DiRT Únase a DiRT Academy, basado en DiRTFish Rally School en Washington, EE. UU., Y domine las habilidades y técnicas para convertirse en el mejor.*


*FINE-TUNE YOUR CAR Ajusta tu configuración, desde la suspensión hasta los niveles de agarre, para adaptarse a la pista, el clima y tu propio estilo de carrera personal.*


Tal y como os adelantamos hace 3 dias, los **requisitos técnicos necesarios** para disfrutar del juego no son nada exagerados, y como viene siendo norma ultimamente se hace uso de Vulkan para obtener un rendimiento óptimo en nuestro sistema. Necesitareis como **mínimo** un equipo con las siguientes caracterísiticas:  
  






|  |  |
| --- | --- |
| OS | Ubuntu 18.04 (64bit) |
| Procesador | Intel Core i3-3225 3.30 GHz |
| RAM | 4 GB |
| HDD | 38 GB |
| Gráfica | Nvidia GTX 680, AMD R9 285, AMD GCN 3rd |
| Accesorios | Teclado |



Si quereis ejecutar el juego con un poco más de garantías esta sería una configuración recomendada:




|  |  |
| --- | --- |
| OS | Ubuntu 18.04 (64bit) |
| Procesador | Intel Core i5-6600K |
| RAM | 8 GB |
| HDD | 38 GB |
| Gráficos | Nvidia GeForce GTX 1070 4 GB |
| Accesorios | Volante, Game Pad |








* Es necesario usar Vulkan
* Tarjetas gráficas Nvidia requieren la versión del controlador 418.43 o posterior
* Tarjetas gráficas AMD requieren Mesa 18.3.4 o posterior  
 ​\* Las tarjetas gráficas AMD Graphics Core Next de 3ª generación incluyen la R9 285, la 380, la 380X, la Fury, la Nano, la Fury X
* Otros controladores y distribuciones modernos no son compatibles oficialmente, pero es probable que funcionen



Podeis adquirir DIRT 4 directamente en la [tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/dirt4/), en la **[tienda de Humble Bundle (patrocinado)](https://www.humblebundle.com/store/dirt-4?partner=jugandoenlinux)** o en [Steam](https://store.steampowered.com/app/421020/DiRT_4/). Os recordamos que **esta misma noche os ofreceremos un Stream** en nuestros canales de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) o [Twitch](https://www.twitch.tv/jugandoenlinux), para enseñaros más de cerca este nuevo lanzamiento de Feral Interactive. También en unas pocas semanas tendremos listo un **extenso análisis de este título** donde podreis leer todas sus caracterísicas y las sensaciones que produce. Os dejamos en compañía del video que acompaña a este lanzamiento:

<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/GprbbXnOn_M" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div> 


¿Comprareis este título de Feral/Codemasters? ¿Qué os parece lo que nos ofrece? Déjanos tus respuestas en los comentarios o charla sobre el juego en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

