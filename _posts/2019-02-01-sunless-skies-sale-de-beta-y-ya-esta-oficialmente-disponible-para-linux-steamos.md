---
author: Pato
category: Rol
date: 2019-02-01 16:16:50
excerpt: "<p>El juego de @failbettergames creadores del exitoso Sunless Sea llega\
  \ tras dos a\xF1os de desarrollo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/49e6c92010764348f7aac47a5db853a3.webp
joomla_id: 962
joomla_url: sunless-skies-sale-de-beta-y-ya-esta-oficialmente-disponible-para-linux-steamos
layout: post
tags:
- indie
- aventura
- rol
- horror
- gotico
title: "Sunless Skies sale de beta y ya est\xE1 oficialmente disponible para Linux/SteamOS"
---
El juego de @failbettergames creadores del exitoso Sunless Sea llega tras dos años de desarrollo

Volvemos con un nuevo lanzamiento, esta vez de "Sunless Skies" del estudio Failbetter Games creadores del también notable "Sunless Sea". Ya os [anunciamos](index.php/homepage/generos/aventuras/9-aventuras/785-sunless-skyes-saldra-de-acceso-anticipado-y-se-lanzara-oficialmente-en-septiembre) hace casi un año que el juego llegaría, pero el lanzamiento se retrasó en Septiembre y por fin ahora ya lo tenemos oficialmente para nuestro sistema favorito. Siguiendo la estela del anterior título, *nos encontramos ante un juego gótico de horror en perspectiva cenital donde jugaremos como el capitán de una locomotora, una máquina de vapor equipada para viajar fuera de la vía férrea: navegar por las estrellas, llevar a tu tripulación al límite y fuera de sí. El contrabando de almas, el trueque de cajas de tiempo, se detienen para el cricket y una taza de té.*  




  
*Descubre más sobre el profundo, oscuro y maravilloso Universo de un Londres caído, como se vio en nuestro juego anterior, [SUNLESS SEA](https://store.steampowered.com/app/596970). (Puedes jugar cualquiera de los dos juegos primero, las historias coexisten sin problemas).*

 
 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/2gp-GjGBaPc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Características:


- Múltiples historias entrelazadas con una profundidad y una riqueza que no se parecen a ninguna otra cosa en los juegos, inspiradas en CS Lewis, Julio Verne, Lovecraft y HG Wells.  
  
- Exuberante arte 2d dibujado a mano  
  
- Cuatro regiones gigantes para explorar: el desierto celestial, el imposible imperio industrial, la extensión pagana de la medianoche y el misterioso Reino Azul.  
  
- Una selección de estilos de juego: construye un linaje de capitanes cada vez que mueras, o vuelve a cargar a tu capitán más reciente y continúa  
  
- Reasignación de teclas y compatibilidad total con controlador


Sunless Skies no está traducido al español, pero si no es problema para ti puedes saber mas sobre el juego en su [página web oficial](http://www.failbettergames.com/), o comprarlo en [Humble Bundle](https://www.humblebundle.com/store/sunless-skies?partner=jugandoenlinux) (enlace patrocinado) o en [Steam](https://store.steampowered.com/app/596970) con un 10% de descuento por su lanzamiento.


¿Que te parece la nueva propuesta de Failbetter Games "Sunless Skies? ¿Te gustan los juegos de horror gótico?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux)

