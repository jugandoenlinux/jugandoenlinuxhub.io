---
author: Pato
category: Estrategia
date: 2018-11-28 21:27:53
excerpt: <p>Tras una larga espera, ya tenemos lo nuevo de Valve para nuestros sistemas</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5e8173bf39566531ecfe4424fce66d85.webp
joomla_id: 914
joomla_url: valve-lanza-oficialmente-artifact-con-soporte-en-linux-steamos
layout: post
tags:
- steam
- valve
- artifact
title: Valve lanza oficialmente 'Artifact' con soporte en Linux/SteamOS
---
Tras una larga espera, ya tenemos lo nuevo de Valve para nuestros sistemas

Hoy era el día, y Valve ha cumplido. Ya tenemos disponible el nuevo juego de Valve para nuestros sistemas favoritos. Tal y como prometieron, '**Artifact**' llega con soporte el Linux/SteamOS de salida y debe ser el pistoletazo de salida para los nuevos desarrollos que según la compañía con sede en Wasington están desarrollando.


'Artifact' llega de la mano de uno de los mas famosos desarrolladores de juegos de cartas Richard Garfield, responsable entre otros del famoso "Magic: The Gathering" con el objetivo de revitalizar el género junto a Valve.


*Artifact es un juego de cartas digital que combina una jugabilidad extremadamente estratégica y competitiva con el vasto universo de Dota 2. El resultado es un juego de cartas intercambiables, con una experiencia envolvente y visualmente impresionante, como ningún otro.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/ZnVuzZYdwIQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


  
***ESTRATEGIA SIN LÍMITES***  
*Maneja tu baraja en tres sendas de combate, contrarrestando cada jugada de tu rival con tus propios movimientos. No hay límite en el número de cartas en tu mano. No hay límite tampoco en el número de unidades que controlas. Y no hay límite en la cantidad de maná que puedes utilizar.*  
  
*Tú decides cuál es la mejor manera de navegar la marea cambiante de la batalla.*  
  
***DIVERSIÓN CON AMIGOS***  
*Si has jugado juegos de cartas en tu casa, sabes mejor que nadie lo divertido que es crear tus propias reglas. Artifact permite que tú y tus amigos tengáis control absoluto en la creación de un torneo. Simplemente, elige el formato de eliminación o no y las restricciones de la baraja; luego, desafía a tus amigos usando las reglas que tú defines.*  
  
***COMPITE GLOBALMENTE***  
*¿Quieres poner a prueba tus habilidades contra el resto del mundo? Los torneos y las competiciones en modo desafío patrocinados por Valve ofrecen la oportunidad a los jugadores no solo de jugar Artifact para disfrutar de la destreza que van adquiriendo, sino también para ganar premios según su nivel de juego.*


A diferencia de otros juegos de cartas gratuitos en los que luego tienes que gastar cantidades de dinero para obtener mazos competentes y mejoras adicionales para poder progresar, Artifact apuesta por una fórmula en la que pagamos el juego y ya tenemos todo. Si bien es cierto que podemos comerciar con items y otros objetos en el mercado de la comunidad, según Valve no es necesario para la progresión del juego.


Según puede leerse en la página del juego, los requisitos para Linux/SteamOS son:


* Requiere un procesador y un sistema operativo de 64 bits
* **SO:** Ubuntu 16.04 o superior
* **Procesador:** Intel i5, 2.4 Ghz o superior
* **Memoria:** 4 GB de RAM
* **Gráficos:**  GPU con soporte Vulkan de NVIDIA, AMD, o Intel
* **Red:** Conexión de banda ancha a Internet
* **Almacenamiento:** 5 GB de espacio disponible
* **Tarjeta de sonido:** Tarjeta de sonido OpenAL Compatible


Desde JugandoenLinux intentaremos haceros un streaming lo antes posible para que veáis el juego en acción en un sistema Linux.


Ya mismo está disponible 'Artifact' en su [página oficial de Steam](https://store.steampowered.com/app/583950/Artifact/) **a un precio de 17,95€**.


¿Qué te parece lo nuevo de Valve? ¿Te gustan los juegos de cartas? ¿Piensas jugarlo?


Cuéntanoslo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

