---
author: Serjor
category: Noticia
date: 2019-02-15 18:50:39
excerpt: "<p>No hay fechas concretas, pero las conversaciones ya han comenzado entre\
  \ ambas compa\xF1\xEDas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b8be0c31d8c923440f8ffc4e5e818d73.webp
joomla_id: 972
joomla_url: valve-esta-trabajando-con-eac-para-quitar-el-bloqueo-de-los-juegos-ejecutados-con-steam-play
layout: post
tags:
- valve
- steam-play
- proton
- drm
- eac
title: "Valve est\xE1 trabajando con EAC para quitar el bloqueo de los juegos ejecutados\
  \ con Steam Play"
---
No hay fechas concretas, pero las conversaciones ya han comenzado entre ambas compañías

Hace unos días se montó un pequeño revuelo en la red cuando una actualización de EAC (Easy Anti Cheat), un servicio que permite a los juegos evitar que se hagan trampas, impedía que Paladdins o Apex Legends se pudieran jugar a través de Proton/Wine.


Este movimiento, que aunque nos perjudica como linuxeros, es comprensible, ya que el objetivo de EAC es evitar que se hagan trampas, y siendo Wine código libre, cualquiera con los suficientes conocimientos podría parchearlo para obtener ventajas en un juego, por lo que lo más justo para los jugadores honestos (y lo más sencillo y barato técnicamente, tampoco vamos a mentirnos) es no permitir que el juego se ejecute vía Wine/Proton.


La respuesta de Valve no ha tardado demasiado, y es que cabía esperar que tomaran cartas en el asunto, ya que este tipo de situaciones pueden acabar con su plan de ejecutar todo su catálogo exclusivo de Windows a través de Proton, y por lo que leemos en [reddit](https://www.reddit.com/r/linux_gaming/comments/apuwxe/so_i_asked_easy_anticheats_support_in_regards_to/), EAC y Valve están en conversaciones para trabajar de manera conjunta y que EAC no impida que se ejecute un juego si este se ejecuta usando Steam Play.


Esto deja una duda en el aire, ¿EAC solamente dará luz verde si se juega desde Steam? ¿Significa eso que juegos que no estén en Steam o no se ejecuten desde Steam no pasarían el filtro? Otra vez, y esto es una teoría sin ningún tipo de rigor, la opción técnica más barata sería usar a Steam como agente de confianza y con sus ejecutables de Proton firmados o validados, y que al ejecutarse desde su entorno, como es un entorno controlado, y desde un punto de vista de evitar trampas, seguro, EAC no saltara.


De ser así, aplicaciones como Lutris o PlayOnLinux verían limitado su catálogo de juegos, por lo que lo deseable sería que la solución venga directamente desde el código en Wine y no haya que depender de binarios certificados o soluciones por el estilo, pero esto es algo que aún está por ver, y confiemos en que la solución sea sostenible a lo largo del tiempo independientemente de Valve.


Y tú, ¿tienes algún juego que usa EAC al que te gustaría volver a jugar? Cuéntanoslo en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

