---
author: Pato
category: Rol
date: 2018-07-04 11:39:46
excerpt: <p>El juego cuenta con la narrativa de Chris Avellone</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/64cd5fc4f5bc6730d92d5c2a957adb10.webp
joomla_id: 794
joomla_url: pathfinder-kingmaker-tiene-fecha-de-salida-en-agosto-pero-aun-no-tiene-version-beta-en-linux
layout: post
tags:
- indie
- aventura
- proximamente
- rol
title: "'Pathfinder: Kingmaker' Tiene fecha de salida en Agosto pero a\xFAn no tiene\
  \ versi\xF3n beta en Linux"
---
El juego cuenta con la narrativa de Chris Avellone

Hace ya un año que [nos hicimos eco](index.php/homepage/generos/rol/item/515-pathfinder-kingmaker-el-nuevo-proyecto-de-cris-avellone-que-esta-en-kickstarter-es-dificil-que-llegue-a-linux) del nuevo desarrollo de '**Pathfinder: Kingmaker**', un juego de rol basado en el universo de Pathfinder y que cuenta con **Chris Avellone** como principal diseñador narrativo. En aquel momento el desarrollo se encontraba recabando fondos en una exitosa campaña en [Kickstarter](https://www.kickstarter.com/projects/owlcatgames/pathfinder-kingmaker?ref=card) en la que consiguieron financiar el proyecto incluyendo una versión del juego para Linux.


Ahora, gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/the-impressive-looking-rpg-pathfinder-kingmaker-to-release-in-august-with-linux-support.12073) sabemos que el juego ya tiene fecha oficial de publicación **para este próximo mes de Agosto**, aunque [en la última actualización](https://steamcommunity.com/gid/[g:1:32795818]/announcements/detail/1672403714074552478), los responsables **afirman no tener aún una versión beta del juego para Linux**. Eso sí, confirman que están trabajando en ella para tenerla disponible lo antes posible.


No sabemos aún si la versión Linux estará a tiempo para el lanzamiento del juego este próximo mes, pero al menos sabemos que están en ello y tendremos disponible el juego en nuestro sistema favorito aunque sea a posteriori.


*Pathfinder: Kingmaker te pone en el papel de un valiente aventurero que lucha por sobrevivir en un mundo plagado de magia y maldad. Asume el papel de un caza astuto pirateando a los enemigos con una espada encantada, una hechicera poderosa bendecida con magia por el toque de sangre demoníaca en sus venas, un sabio clérigo de dioses benévolos o malignos, un pícaro ingenioso listo para desactivar hasta la más mortífera de las trampas, o cualquiera de los otros innumerables héroes. ¡El único límite es tu imaginación!*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/5mP0AxgTZEA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 En cuanto a los requisitos mínimos publicados para Linux, son:


**Mínimo:**  

+ Requiere un procesador y sistema de 64-bit
+ **OS:** Ubuntu 14.04 LTS 64-bit o superior
+ **Procesador:** Intel Celeron 1037U @ 1.80GHz
+ **Memoria:** 4 GB RAM
+ **Graficos:** Intel HD Graphics 3000
+ **HD:** 30 GB


**Recomendado:**  

+ Requiere un procesador y sistema de 64-bit
+ **OS:** Ubuntu 14.04 LTS 64-bit o superior
+ **Procesador:** Intel Core i7 CPU 920 @ 2.67GHz
+ **Memoria:** 8 GB RAM
+ **Graficos:** ATI Radeon HD 5770 o NVIDIA GeForce GTX 960M
+ **HD:** 30 GB


Si quieres saber más, puedes visitar la [página web oficial](https://owlcatgames.com/) del juego o su [página de Steam](https://store.steampowered.com/app/640820/Pathfinder_Kingmaker/) donde estará disponible en Linux en próximas fechas, eso sí sin traducción a español a la vista.



