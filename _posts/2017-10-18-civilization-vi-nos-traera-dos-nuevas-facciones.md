---
author: leillo1975
category: Estrategia
date: 2017-10-18 14:34:59
excerpt: <p>Indonesia y los Jemeres son las nuevas civilizaciones.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/dee4183b3eece6f1f1fda5b7115d2824.webp
joomla_id: 494
joomla_url: civilization-vi-nos-traera-dos-nuevas-facciones
layout: post
tags:
- aspyr-media
- dlc
- civilization-vi
- firaxis
- 2k-games
title: "Civilization VI nos traer\xE1 dos nuevas facciones."
---
Indonesia y los Jemeres son las nuevas civilizaciones.

Hace bien poco los afortunados poseedores de [Civilization VI](index.php/homepage/analisis/item/352-analisis-sid-meier-s-civilization-vi) recibíamos la actualización del verano ([summer update](index.php/homepage/generos/estrategia/item/584-por-fin-ha-llegado-el-summer-update-de-civilization-vi)), que incluía las civilizaciones del Nilo, Nubia y Egipto. Ahora Firaxis Games acaba de anunciar que pronto veremos dos facciones más a través de DLC's. En este caso le toca a **Indonesia** (Mayapajit) y a el **imperio de los Jemeres** (actual Camboya). Los que hayan comprado la versión Deluxe del juego recibirán estas expansiones de forma gratuita. Aun no está muy claro si vendrán por separado o en un pack.


 


Si escogemos [**Indonesia**](https://civilization.com/es-ES/news/entries/civilization-vi-gitarja-leads-indonesia) jugaremos con la reina [Dyah Gitarja](https://en.wikipedia.org/wiki/Tribhuwana_Wijayatunggadewi) y tendremos una unidad naval exclusiva llamada **Jong** (capaz de soportar el fuego enemigo y de responder con su propia artillería), una mejora llamada **Kampong** (permite alojar habitantes en el mar), la habilidad llamada **Nusantara** (Los Complejos de ocio proporcionan Servicios adicionales si están adyacentes a una casilla de Costa que no sea de lago), y la habilidad exclusiva de la lider ("**Glorificada Diosa de los tres mundos**", que otorga una bonificación de Fe a las ciudades costeras). Podeis ver un resumen en este video:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/Agma0sC1a5U" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 Si escogemos el [Impero de los Jemeres](https://civilization.com/es-ES/news/entries/civilization-vi-jayavarman-vii-leads-khmer) jugaremos con el [rey Jayavarman VII](https://es.wikipedia.org/wiki/Jayavarman_VII) y tendremos como unidad exclusiva el **Domrey** (elefantes con una ballista montada en su lomo), una mejora llamada el **Prasat** (lugar de fe Jemer), la habilidad llamada **Grandes Barays** (umenta la producción de Alimentos de los jemeres cuando se construyen granjas al lado de acueductos, y hace que estos sean mucho más versátiles al dar bonificaciones a Fe y a Servicios), la habilidad exclusiva del lider (**"Monasterios del Rey"** que permite apoderarse de los territorios adyacentes cuando completéis Lugares sagrados, además de obtener Alimentos y Alojamiento adicionales si los construís en un río ). Podeis ver un resumen en este video:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/NyLSlEcwHKY" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Como veis se trata de contenido muy jugoso para todos los que amamos este juego. No tenemos fecha de cuando serán lanzadas, pero en cuanto tengamos alguna noticia actualizaremos este artículo para que esteis informados. Si quereis comprar Civilization VI podeis hacerlo en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/289070/123216/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Habeis jugado ya a Civilization VI? ¿Qué os parecen estas nuevas DLC's? Deja tus impresiones en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

