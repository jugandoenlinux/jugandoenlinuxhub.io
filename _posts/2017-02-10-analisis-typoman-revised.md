---
author: leillo1975
category: "An\xE1lisis"
date: 2017-02-10 17:33:03
excerpt: <p>Lanzado por fin el original juego en GNULinux/SteamOS</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e9432fccf28a953514f077b86e5e657a.webp
joomla_id: 213
joomla_url: analisis-typoman-revised
layout: post
tags:
- analisis
- typoman-revised
- brainseed-factory
title: "An\xE1lisis: Typoman-Revised"
---
Lanzado por fin el original juego en GNULinux/SteamOS

Tras su paso por WiiU y Windows, en el día de hoy nos llega esta interesante propuesta por parte de el estudio Brainseed Factory. Tras un periplo lleno de premios, nominaciones y buenas críticas en general, el estudio se ha decidido a traernos este título para que nosotros, los Linuxeros, podamos disfrutarlo. En primer lugar me gustaría agradecer a Brainseed Factory la clave recibida para elaborar este análsis, así como el poder participar en el proceso de betatesting.


![06](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/typoman/typoman06.webp)


 


Nos encontramos ante un arcade de plataformas 2D con una estética oscura y onírica muy marcada, que recuerda bastante a Limbo, pero que tiene personalidad propia. En Typoman: Revised, nos vamos a encontrar un mundo destruido y sucio en el que nuestro protagonista, tendrá que avanzar y sobrevivir en diferentes situaciones. Dicho protagonista es un hombre cuyo cuerpo está formado por letras H-E-R-O (vaya, que casualidad), y en el que la reflexión y la resolución de puzzles toma gran parte del tiempo de juego.Estos puzzles se resuelven formando palabras con letras que encontramos en las diferentes zonas, y que activan elementos, protegen al personaje, provocan cambios en el entorno, etc.


En cuanto a estas palabras es importante decir que el juego exige el conocimiento básico del Inglés para poder avanzar, pues si bien el juego está supuestamente traducido al castallano, en la práctica dicha traducción solo afecta a los menús del juego. El desarrollo de la historia es integramente en Inglés, aunque en el momento que jugueis entedereis el porque. Una traducción total del juego implicaria la modificación absoluta de la jugabilidad de este, por lo que en este caso es lógico que no se haya realizado. También para no asustar, como dije antes, con un inglés muy básico se puede jugar perfectamente.


![02](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/typoman/typoman02.webp)


 


A la hora de formar palabras, algo esencial en el juego, ya que estas son las que provocan los eventos del juego, podremos ordenar y desechar letras para formalas. Estas van a conseguir, por ejemplo, abrir puertas, activar mecanismos, crear campos de protección, que nos van a permitir avanzar en las diferentes zonas del juego. Las soluciones a los problemas pueden ser evidentes o hacer que nos devanemos los sesos en más de una ocasión. También es importante decir que no solo la resolución de puzzles es importante, sino también la habilidad saltando, esquivando, midiendo los tiempos, analizando rutinas... por lo que como veis, aunque el juego parezca simple, al final tiene una mecánica que implica bastantes habilidades por parte del jugador.


En cuanto los diferentes "personajes" del juego, vamos a encontrarnos con diferentes enemigos, que igual que nuestro personaje, están formados por letras. Estos disponen de diversas habilidades y formas de afrontarlos según el tipo que sean. También vamos a encontrar trampas que nos pondrán el reto de avanzar un poco más dicícil. Es importante decir que durante el juego nos encontraremos con un Angel que nos protegerá en determinados momentos y que forma parte esencial de la historia. Existen también una especie de seres que invierten el signficado de las palabras y que pueden ser tremendamente útiles para la resolución de las diferentes zonas.


 ![10](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/typoman/Typoman10.webp)


 


El juego consta de tres capitulos precedidos de un prologo. En este último, que funciona a modo de tutorial, poco a poco vemos como se forma Typoman (HERO) hasta adquirir todas sus habilidades. Estas son saltar, mover, empujar, tirar y lanzar; y para poder realizarlas podemos usar tanto un controlador como el teclado. En mi caso he utilizado mi Steam Controller sin ningún tipo problemas. He de decir que, aunque los controles son muy sencillos e intuitivos, he tenido algún que otro problema no muy importante al agarrar objetos, ya que requiere bastante precisión en cuanto a la posición de agarre.


A nivel técnico el juego es un 2D de scroll lateral con unos gráficos muy sencillos, no vamos a encontrar nada excesivamente llamativo ni complejo; pero basicamente es por que realmente se pretende que el juego sea tal y como es. Gracias a esto no vamos a tener ningún tipo de problema al ejecutarlo en ordenadores más modestos o antiguos, funcionando en casi cualquier configuración.


En cuanto al apartado sonoro, la música es destacable su calidad, proporcionando al juego una atmosfera característica e inquietante. Obviamente no todo es perfecto, pero si algo se le puede achacar es su corta duración, ya que se puede acabar en unas pocas horas y poco rejugable; aunque también es cierto que no se trata de un juego para nada caro, por lo que al final la experiencia compensa mucho.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/wjc-Sumxo_E" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Si eres un devora-indies o te gustan los juegos con regustillo retro, no te lo puedes perder, ya que es casi seguro que no te va a defraudar. Brainseed Factory han hecho un trabajo loable con Typoman: Revised, y el que hayan decidido traernoslo a los usuarios de GNULinux/SteamOS es muy de agradecer. Permaneceremos atentos a sus próximos trabajos.


Si quieres comprar Typoman: Revised puedes hacerlo en la tienda de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/336240/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Que te ha parecido Typoman: Revised? ¿Le vas a dar una oportunidad? Dinos lo que piensas en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord.](https://discord.gg/ftcmBjD)


 


FUENTES: [Brainseed Factory](http://www.brainseed-factory.com/), [Typoman](http://www.typoman.net/), [Steam](http://store.steampowered.com/app/336240/)

