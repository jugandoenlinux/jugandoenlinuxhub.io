---
author: leillo1975
category: Estrategia
date: 2021-02-23 09:36:27
excerpt: "<p>Importantes avances en este juego #OpenSource, <span class=\"css-901oao\
  \ css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@play0ad . <br /></span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/0ad/0ADCarthaginianTown.webp
joomla_id: 1281
joomla_url: lanzada-la-alpha-24-de-0-a-d-xsayarsa
layout: post
tags:
- open-source
- alpha
- 0ad
- wildfire-games
title: "Lanzada la Alpha 24 de 0 A.D., X\u0161ay\u0101r\u0161\u0101"
---
Importantes avances en este juego #OpenSource, @play0ad .   



Existen montones de proyectos de juegos libres interesantes, unos más sencillos y otros más trabajados, y por supuesto **todos merecen atención**, tanto por nosotros, como por la comunidad del software libre. Pero si hay un proyecto que destaque entre todos, ese es [0AD](https://play0ad.com), un **RTS con una calidad impresionante**, que a pesar de no estar acabado, se come por la pata a muchos de los juegos comerciales que hay en el mercado.


Para quien no conozca este referente de la estrategia en tiempo real, hay que decir que está desarrollado por [Wildfire Games](https://wildfiregames.com/), un **grupo de desarrolladores voluntarios a nivel global**. El juego, que **recuerda en gran medida a el conocido Age of Empires II,** recrea a diversas civilizaciones entre el 500 AC al 500 DC. Usa el motor [Pyrogenesis](https://www.moddb.com/engines/pyrogenesis), creado con **C++** en exclusiva para el juego, y que permite un **entorno 3D** enriquecido, un **sistema de partículas**, gráficos **OpenGL** y color de 32 bits, todo ello sin sacrificar los requisitos técnicos del equipo donde se juegue. Para el sonido usa **OpenAL** (con la codificación .ogg), y el **código de red ENet.**


El trabajo en 0AD comenzó hace la friolera de 20 años, en el **2001, siendo en primer lugar un mod de AOEII**, y usando su propio motor en el 2003 como proyecto cerrado. **La primera versión libre y pública del juego llegaría en el año 2009**, usando la GPL y Creative Commons BY-SA para el contenido artístico.... y así hasta ahora, versión tras versión, alpha tras alpha, el juego se ha convertido en un referente, y por supuesto un orgullo para toda la comunidad del software libre.


![edificios](https://play0ad.com/wp-content/uploads/2021/02/image12.jpg)


En esta **Alpha 24, Xšayāršā** para los amigos, la cantidad de mejoras ha sido impresionante, destacando a grandes rasgos entre ellas las siguientes:


-**Ajustes en el balance del juego**. Las civilizaciones han sido retocadas para dar a los jugadores unas elecciones más viables.  
 -**Ajuste de edificios**. Ahora es posible encajar edificios uno al lado de otro usando la tecla Ctrl.  
 -**Mejoras en el renderizador**. Se ha incluido el Anti-Aliasing en el juego, pudiendo desactivar, usar FXAA o MSAA.  
 -**Editor de teclas** de acceso rápido  
 -**Mejoras en la formación**. Es posible elegir una formación predeterminada para colocar tus unidades cuando se les den órdenes de "Caminar" y "Patrullar", y también puedes optar por disolver formaciones automáticamente cuando tus unidades reciban órdenes de Atacar. o Reunir, etc.  
 -Efectos de estado (y modificadores)  
 -**Ajuste de la población mundial.** Cuando se muestran muchas unidades en pantalla el juego puede sufrir ralentizaciones. Ahora es posible ajustar un tope máximo de población, y cuando un jugador muere su cuota se reparte con el resto de jugadores.  
 -**Mejoras en la sala de partidas**. Se ha habilitado la contraseña en las partidas, y se oculta la IP de esta hasta que los jugadores están conectados para evitar ataques DDOS.  
 -**Mejoras en la interfaz de usuario** del juego (GUI)  
 -Mejoras en el **comportamiento de las unidades**  
 -Interfaz de aprendizaje de refuerzos  
 -Arte: **nuevos modelos**  
 -**Nuevos mapas** de escaramuzas


Describir todas las mejoras en un artículo sería demasiado largo, por eso si quereis entrar al detalle os recomendamos que consulteis la [noticia donde se describen](https://play0ad.com/new-release-0-a-d-alpha-24-xsayarsa/) en su web. También podeis ver un video donde podeis repasarlas de una forma más visual:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/k6scM-nwfhE" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


Por supuesto lo mejor para probar todas estas novedades es que os hagais con esta última versión del juego desde su [sección de descargas](https://play0ad.com/download/linux/). Para usuarios de Ubuntu, o quien quiera se puede descargar como [Snap](https://snapcraft.io/0ad), pues en los repositorios PPA del juego aun no está disponible.


¿Sois aficionados a los RTS? ¿Habeis jugado ya a 0AD? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

