---
author: leillo1975
category: "Simulaci\xF3n"
date: 2017-12-05 14:07:16
excerpt: "<p>SCS Software acaba de lanzar esta nueva expansi\xF3n.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/64f1bd36159ab528d3f0e600c8367f84.webp
joomla_id: 565
joomla_url: disponible-italia-la-nueva-dlc-para-euro-truck-simulator-2
layout: post
tags:
- ets2
- scs-software
- euro-truck-simulator-2
- italia
title: Disponible "Italia", la nueva DLC para Euro Truck Simulator 2.
---
SCS Software acaba de lanzar esta nueva expansión.

¡Ciao amici! , por fin vamos a poder circular por las "Autostrade Italiane" con nuestros camiones favoritos. Gracias al siempre fenomenal trabajo de la compañía checa **SCS Software**, en esta ocasión podremos recorrer "la bota" de forma oficial con esta nueva DLC. Hace unas semanas teníamos [confirmación oficial de la fecha de salida](index.php/homepage/generos/simulacion/item/552-bella-italia-sera-la-nueva-dlc-para-euro-truck-simulator-2), que situaba el lanzamiento para hoy. Acaban de anunciar en su canal ofical de twitter tan esperada noticia:



> 
> DLC [#Italia](https://twitter.com/hashtag/Italia?src=hash&ref_src=twsrc%5Etfw) for Euro Truck Simulator 2 is out! Have fun! We had a ton of fun creating it :) <https://t.co/ce7i2fe67z>
> 
> 
> — SCS Software (@SCSsoftware) [December 5, 2017](https://twitter.com/SCSsoftware/status/938064696434978816?ref_src=twsrc%5Etfw)


  
 Como sabeis,  para ir alimentando más el Hype publicaban unas estupendas fotografías y poco después un video donde nos podíamos deleitar con los siempre espectaculares paisajes y localidades del pais transalpino. A finales de la semana pasada iban preparando el terreno con la [actualización 1.30](http://blog.scssoft.com/2017/12/euro-truck-simulator-2-update-130.html) donde mejoraban las zonas italianas que ya estaban en el juego base, tal y como lo hicieron con [Vive la France!](index.php/homepage/analisis/item/243-analisis-ets2-vive-la-france-dlc), además de añadir la posibilidad de comprar los últimos modelos de la marca sueca Scania.


 


Como principales novedades de "Bella Italia" podemos encontrar:


* 11,500 km de nuevas carreteras
* 19 nuevas ciudades, Incluidas Roma, Nápoles o Palermo
* Nuevos elementos 3D únicos
* Intersecciones realistas, incluidas circunvalaciones alrededor de las grandes ciudades
* Muchas industrias y compañías regionales
* Señales y peajes típicos de Italia
* Lugares reconocibles
* Arquitectura típica Italiana
* Vegetación mejorada para el clima mediterraneo
* Interesantes nuevas cargas agregadas
* Nuevos logros de Italia para desbloquear.


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/OtYNJ077oWA" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


En JugandoEnLinux.com estamos deseando poder ofreceros pronto una **completa revisión** en los próximos días y como suele ser habitual videos para nuestro [canal de Youtube](https://www.youtube.com/channel/UC4FQomVeKlE-KEd3Wh2B3Xw?view_as=subscriber) para que podais observar con más detalle el siempre estupendo trabajo de SCS. Podeis comprar la expansión para Euro Truck Simulator 2 "Italia" en  Steam:


 <div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/558244/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Os apetece conducir vuestro camión por Italia? ¿Qué os parece el trabajo de SCS Software? Dejanos tus impresiones en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

