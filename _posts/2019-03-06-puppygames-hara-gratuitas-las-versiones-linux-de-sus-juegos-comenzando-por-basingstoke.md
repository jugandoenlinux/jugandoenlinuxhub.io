---
author: Pato
category: Arcade
date: 2019-03-06 17:41:42
excerpt: "<p><span class=\"username u-dir\" dir=\"ltr\">@puppygames adem\xE1s se encuentra\
  \ desarrollando su nuevo juego Battledroid</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/019c164a7249eb033dc2a912280a3ff0.webp
joomla_id: 986
joomla_url: puppygames-hara-gratuitas-las-versiones-linux-de-sus-juegos-comenzando-por-basingstoke
layout: post
tags:
- accion
- indie
- sigilo
- supervivencia
- arcade
- roguelike
title: "Puppygames har\xE1 gratuitas las versiones Linux de sus juegos comenzando\
  \ por Basingstoke"
---
@puppygames además se encuentra desarrollando su nuevo juego Battledroid

Siempre es bueno que te regalen cosas. Y si además son juegos para Linux aún mejor, y eso es lo que debieron pensar en Puppygames ya que han anunciado que **las versiones Linux de sus juegos estarán disponibles de forma gratuita** en itch.io en los próximos meses, comenzando con **Basingstoke** que ya lo puedes descargar desde allí. Las versiones de estos juegos para otros sistemas seguirán teniendo el precio habitual.


Basingstoke es un roguelike tenso que mezcla sigilo y arcade. Explora las ruinas humeantes de Basingstoke, Reino Unido, un mundo de gran peligro en el que feroces monstruos alienígenas reanimados vagan a sus anchas.


*"Basingstoke es una ciudad en Inglaterra, famosa por su encantadora combinación de características creadas a mano y generadas proceduralmente. Explora, por encima y bajo el suelo, y disfruta de sus numerosas atracciones. Roba una máquina de multas de estacionamiento, estropea una señal de mantenerse a la izquierda, ve de compras al OMNImart24 / 7 ... o si las cosas se ponen un poco mal, escóndete en un contenedor con ruedas y reflexiona sobre las elecciones que has hecho en la vida que te han llevado a una muerte probablemente aterradora como un sabroso aperitivo en un lugar miserable."*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/NX57xJevybY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


En el juego tendremos que utilizar el entorno y el sigilo, a la vez que tratamos de escapar utilizando cualquier elemento o arma a nuestro alcance. Además podremos elegir entre diversos personajes con características propias para tratar de sobrevivir.


¿Donde está el truco? Pues en ningún sitio. **El juego es gratuito para nuestro sistema favorito**, y el estudio tan solo piden que si te sientes generoso siempre puedes darles un donativo o pasar por su [Patreon](http://www.patreon.com/puppygames) y patrocinar el desarrollo de este y otros juegos, como su próximo [Battledroid](http://www.puppygames.net/battledroid/).


Si quieres saber mas, puedes pasarte por la [página del juego en itch.io](https://puppygames001.itch.io/basingstoke) donde lo tienes disponible para descargar, y dejarle alguna propina si te gusta la propuesta.


¿Qué te parece Basingstoke? ¿Lo vas a jugar? Recuerda... ¡es gratuito! ¿Qué te parece la propuesta de Puppygames de dar gratuitamente sus juegos **solo** para nuestro sistema favorito?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux)

