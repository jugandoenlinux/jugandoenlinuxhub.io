---
author: leillo1975
category: Plataformas
date: 2017-10-27 15:01:17
excerpt: "<p>Ya est\xE1 disponible este t\xEDtulo de plataformas.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9b12ce15495e42c004530d2e9007bac0.webp
joomla_id: 510
joomla_url: lanzado-the-end-is-night-en-beta-abierta-para-linux-steamos
layout: post
tags:
- plataformas
- retro
- tyler-glaiel
- edmund-mcmillen
- the-end-is-night
title: Lanzado "The End is Night" en beta abierta para Linux/SteamOS.
---
Ya está disponible este título de plataformas.

Parece que **Ryan C Gordon** ([@icculus](https://twitter.com/icculus)) está muy activo ultimamente. Ayer suscitaba la controversia con [sus declaraciones](https://twitter.com/icculus/status/923201260819550209) ,  lo que provocaba el debate en la comunidad Linuxera, y por supuesto en JugandoEnLinux, donde a raiz de esto publicabamos una [reflexión sobre el crecimiento de los juegos en linux](index.php/homepage/editorial/item/626-esta-creciendo-lo-suficiente-el-juego-en-linux). Pero vamos al grano y hablemos de lo que hemos venido a hablar aquí, y es que el susodicho ha publicado **otro tweet** donde anuncia la disponibilidad en modo beta de su último port "The End is Night":


 



> 
> Ok, Linux version of The End is Nigh is officially live on Steam; switch to the "linuxbeta" branch to play, report bugs to me!
> 
> 
> — Ryan C. Gordon (@icculus) [27 de octubre de 2017](https://twitter.com/icculus/status/923703635342528512?ref_src=twsrc%5Etfw)


  





 


The End is Night es un juego de plataformas creado por Edmund McMillen y Tyler Glaiel. El primero es conocido por ser el padre del vanagloriado [Super Meat Boy](http://store.steampowered.com/app/40800/Super_Meat_Boy/) y también [The Binding of Isaac](http://store.steampowered.com/app/113200/The_Binding_of_Isaac/), dos fantásticos juegos indie donde la jugabilidad toma el mando y nos hace olvidar la sencillez de su faceta gráfica. Y siguiendo estas mismas premisas encontramos al juego que hoy tratamos.


 


En este juego tomamos el control de Ash (si, como el de Pokemon), una de las pocos seres que han quedado vivas después del fin del mundo. En el debemos recoger durante más de 600 niveles,  pedazos de seres vivos para poder juntarlos y crearnos un amigo. También será importante coleccionar "tumores" para desbloquear nuevas zonas y minijuegos. Todo ello aderezado como siempre de un control y jugabilidad exqusitos marca de la casa.  Os dejo con el trailer del juego para que os hagais una idea de que va el juego:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/KJ71Wz90zt4" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Puedes comprar **The End Is Night** en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/583470/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Qué te parecen los juegos de Edmund McMillen? ¿Disfrutaste en su día con Super Meat Boy? ¿Que piensas de esta nueva propuesta? Deja tus impresiones en los comentarios, o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).


 

