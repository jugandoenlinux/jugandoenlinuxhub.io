---
author: Pato
category: Aventuras
date: 2017-08-17 10:06:09
excerpt: <p>El videojuego basado en el Best-seller es obra de Daedalic Entertainment</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7dff099a65b026895942a872c251a542.webp
joomla_id: 433
joomla_url: ken-follet-los-pilares-de-la-tierra-ya-disponible-en-linux-steamos-desde-el-dia-1
layout: post
tags:
- indie
- aventura
- casual
- steam
title: '''Ken Follet Los Pilares de la Tierra'' ya disponible en Linux/SteamOS desde
  su lanzamiento'
---
El videojuego basado en el Best-seller es obra de Daedalic Entertainment

Cuando hablamos de videojuegos basados en grandes franquicias de cine o libros no es raro encontrar casos de juegos con una calidad por debajo de las espectativas. Sin embargo en el caso de 'Ken Follet Los Pilares de la Tierra' [[web oficial](http://www.the-pillars-of-the-earth-game.com/)] parece que el desarrollo de Daedalic Entertainment si parece haber dado en el clavo. 


'Los Pilares de la Tierra' ha sido concebido como una trilogía de videojuegos de tipo novelas interactivas, basados en el famoso Best-Seller de Ken Follet en donde seguiremos las aventuras los tres protagonistas dentro del universo del libro.


**Sinopsis de Steam:**



> 
> *Basado en el superventas de Ken Follet, «Los pilares de la Tierra» cuenta la historia de la aldea de Kingsbridge con un nuevo giro interactivo. Asume el papel de Jack, Aliena y Philip: explora el universo de la obra y cambia el rumbo de la historia con tus palabras y decisiones. Una novela interactiva compuesta de tres «libros» de 7 capítulos.*
> 
> 
> *Inglaterra, siglo XII. En tiempos de guerra y pobreza, una aldea emprende la construcción de una catedral en busca de riqueza y seguridad para sus gentes. En su lucha por sobrevivir, vidas y destinos se entrecruzan. El monje Philip se convierte en prior de la pequeña abadía de Kingsbridge. Al mismo tiempo, un joven llamado Jack crece en el bosque junto a su madre proscrita. Pronto se convierte en aprendiz de albañil, labrándose un futuro como reputado constructor. Junto a la noble Aliena, caída en desgracia, Jack y Philip inician la construcción de una de las catedrales más hermosas de la historia de Inglaterra.*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/jJYR1RI9SQQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Como ya he dicho, el juego es una novela interactiva de tipo "point & clic" que está recibiendo muy buenas críticas y que está disponible en Linux desde su lanzamiento.


Si te gustan este tipo de juegos, o eres de los que ha disfrutado leyendo el libro y quieres disfrutar con las aventuras en Kingsbridge, tienes el videojuego traducido al español (subtítulos) en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/234270/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Te gustan las aventuras de tipo "point & clic"? ¿Eres de los que ha leído el libro?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

