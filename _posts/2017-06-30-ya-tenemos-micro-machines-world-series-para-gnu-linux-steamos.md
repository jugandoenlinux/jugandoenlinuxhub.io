---
author: leillo1975
category: Arcade
date: 2017-06-30 07:10:19
excerpt: <p>Virtual Programming ha sido la encargada de traernos este juego de Codemasters</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/605a5b56c8e1f29c51548653d6f1dfc8.webp
joomla_id: 391
joomla_url: ya-tenemos-micro-machines-world-series-para-gnu-linux-steamos
layout: post
tags:
- virtual-progamming
- codemasters
- micro-machines
title: Ya tenemos Micro Machines World Series para GNU-Linux/SteamOS.
---
Virtual Programming ha sido la encargada de traernos este juego de Codemasters

Hace poco más de un mes os anunciabamos que [Codemasters lanzaría Micro Machines World Series](index.php/homepage/generos/arcade/item/461-codemasters-no-descarta-traer-micro-machines-world-series-a-linux-steamos) para nuestros sistemas, y unos días después nos enterábamos que sería [gracias al trabajo de Virtual Programming](index.php/homepage/generos/arcade/item/473-micro-machines-world-series-llegara-a-linux-steamos-el-proximo-23-de-junio). Con una semana de retraso con respecto a la fecha señalada (23 de Junio) ha sido lanzado este arcade con **soporte para GNU/Linux desde el primer día**. Aquí tenemos el tweet anunciandolo:


 



> 
> ? OUT NOW! Micro Machines for [#Mac](https://twitter.com/hashtag/Mac?src=hash) & [#Linux](https://twitter.com/hashtag/Linux?src=hash) ?  
>   
> ▶️ <https://t.co/XrPDUXUGp0> ◀️ [#MicroMachines](https://twitter.com/hashtag/MicroMachines?src=hash) [#gamedev](https://twitter.com/hashtag/gamedev?src=hash) [pic.twitter.com/4CcboySkAf](https://t.co/4CcboySkAf)
> 
> 
> — Virtual Programming (@virtualprog) [June 30, 2017](https://twitter.com/virtualprog/status/880682326690328576)



 


[Virtual Programming](https://www.vpltd.com/) es conocida por ser la responsable de ports tan conocidos como **The Witcher 2, Bioshock Infinite, Arma III, DIRT Showdown, Spec Ops o la saga Saints Row** con unos resultados más que notables. Atrás quedaron los días en que fueron criticados por la falta de rendimiento en The Witcher 2 y el uso de su wrapper eON para sus ports. Ahora mismo la calidad de estos es muy buena y el rendimiento está casi a la par con el original de Windows. No tenemos referencias de como funciona este último port, pero estamos casi seguros que funcionará bien.


 


Para poder disfrutar del juego tendremos que cumplir los siguientes requisitos:


* **SO:** SteamOS 2.0, Ubuntu 16.04 or similar Linux distribution, 64-bit kernel
* **Procesador:** AMD FX Series or Intel Core i3 Series
* **Memoria:** 4 GB de RAM
* **Gráficos:** AMD HD5570 or NVIDIA GT440 with 1GB of VRAM (OpenGL 4.2 compliant)
* **Almacenamiento:** 5 GB de espacio disponible
* **Tarjeta de sonido:** PulseAudio/ALSA Compatible
* **Notas adicionales:** Gráficos Intel no soportados. Mesa 17.0.2 or superior recomendado para las gráficas AMD. driver nvidia-375 para las GPU's Nvidia . Wayland no está soportado.


 



> 
> *Acerca de este juego*
> ----------------------
> 
> 
> *¡La leyenda ha vuelto! Micro Machines World Series combina la emoción de las frenéticas carreras de vehículos en miniatura con la estrategia de los combates en equipo. ¡Y todo esto en extraordinarios entornos interactivos del día a día! ¡Coge tu pistola NERF, intenta no quedarte atascado y haz que el mundo se sumerja en un caos multijugador en miniatura!*   
>    
>  *UBICACIONES LEGENDARIAS: Además de pistas en la cocina y en mesas de billar, entre otras, Micro Machines World Series también cuenta con nuevas ubicaciones y arenas como el jardín y el taller. ¡En total, podrás disfrutar de 10 pistas de carreras y 15 arenas de batalla!*  
>    
>  *GENIALES ENTORNOS DE JUEGO INTERACTIVOS: ¡Teletransportes, catapultas, ventiladores gigantes y muchos más elementos añaden aún más diversión y harán que cada carrera sea diferente!*  
>    
>  *VEHÍCULOS PERSONALIZABLES: Una gran cantidad de vehículos en miniatura, cada uno con sus habilidades y arsenal de armas únicos, ¡además de una gran variedad de opciones personalizables!*  
>    
>  *LICENCIAS DE HASBRO OFICIALES: Incluyen NERF, G.I. JOE, HUNGRY HUNGRY HIPPOS (TRAGABOLAS) y OUIJA*  
>    
>  *CARRERAS CLÁSICAS EN MINIATURA: ¡Carreras de Micro Machines para los más puristas, con el modo Eliminación! ¡Podrás ganar una gran variedad de armas, pistas interactivas, diseños únicos, marcadores de eliminación y animaciones de victoria!*  
>    
>  *MODOS DE BATALLA TOTALMENTE NUEVOS: ¡Con un máximo de 12 jugadores en arenas enormes y varios modos, descubrirás que eliminar a tus enemigos es tan satisfactorio como ganar una carrera!*  
>    
>  *JUEGO EN EQUIPO: ¡Una sensación que cambiará tu modo de juego en cuanto unas fuerzas con tus aliados para acabar con el resto de jugadores!*  
>    
>  *MULTIJUGADOR EN LÍNEA Y LOCAL: ¡Con marcadores, competiciones y desafíos frecuentes, temporadas, listas, varias divisiones, y mucho más!*
> 
> 
> 


 


La verdad es que en JugandoEnLinux.com estamos deseando ponerle las manos encima. No podemos aseguraros que podamos realizar un análisis de este título ya que primero tenemos que ponernos en contacto con Virtual Programming, pero si tenemos la posibilidad estad seguros de que así será. Os dejamos con el trailer del juego:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/yrbukeqhhCU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Puedes comprar Micro Machines World Series en **[Deliver2.com](http://www.deliver2.com/index.php?route=product/product&product_id=343)** (recomendado) o en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/535850/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Que te parece esta nueva propuesta de Codemasters y Virtual Programming? ¿Te lo comprarías? Contéstanos en los comentarios o en nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

