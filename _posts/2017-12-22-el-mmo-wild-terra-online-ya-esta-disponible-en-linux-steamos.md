---
author: Pato
category: Rol
date: 2017-12-22 15:31:31
excerpt: <p>El juego abre nuevos servidores y lanza un trailer de lanzamiento</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c5949b98a954ba20a6a6d17790280cde.webp
joomla_id: 585
joomla_url: el-mmo-wild-terra-online-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- indie
- rol
- mmo
title: "El MMO 'Wild Terra Online' ya est\xE1 disponible en Linux/SteamOS"
---
El juego abre nuevos servidores y lanza un trailer de lanzamiento

Buenas noticias para todos los que buscan un MMO en Linux/SteamOS. El pasado día 18 fue lanzado oficialmente 'Wild Terra Online' el juego en el que tendrás que construir y evolucionar tu propio mundo desde cero y con la libertad de hacer lo que quieras dentro de su universo medieval.


*"Wild Terra Online es un sandbox de rol, en un mundo medieval completamente controlado por jugadores. No necesitarás completar una cadena de tareas y matar a mil lobos para crear equipos. ¡Todo es mucho más interesante! Deberás comprender las etapas de producción reales de fabricación de cuero endurecido, acero y otros materiales. Tendrás que sobrevivir en un bosque lleno de depredadores, construir un refugio y mejorar tus habilidades artesanales, desarrollar una granja, domesticar animales y explorar nuevos territorios y luchar por tesoros. En Wild Terra, ¡puedes convertirte en cualquier persona y hacer lo que quieras!"*


Los desarrolladores han publicado un nuevo trailer de lanzamiento:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/LGucXu_WD-o" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Características principales:


* Un universo creado y controlado por jugadores. El juego no tiene NPC ni ciudades preconstruidas. Los propios jugadores talan bosques, construyen caminos y construyen ciudades.
* La capacidad de construir cualquier cosa, en cualquier lugar. Hay más de 100 edificios diferentes. Desde una fogata por la noche a un castillo medieval, hornos, herrerías, molinos y otros edificios industriales.
* Desarrollado sistema de elaboración. Confeccionar ropa y armaduras, herramientas y armas. Desarrollar agricultura, agricultura y cocina. Las habilidades se mejoran a medida que se utilizan, lo que te permite crear ropa / armaduras, herramientas y armas de la más alta calidad.
* Casa de Subastas. Venda equipos creados, intercambie equipos raros entre otros jugadores y participe en la licitación de castillos gremiales.
* Emocionantes peleas, cuyos resultados están determinados SOLAMENTE por tus tácticas, estrategia de combate y nivel de habilidad. En Wild Terra Online, el aumento de las habilidades y el nivel de equipamiento elaborado contribuyen en menor medida al sistema de combate. El uso competente y el conocimiento de las ventajas y desventajas de los equipos y el terreno son las claves de la victoria.
* Buscando tesoros y luchando por las minas. ¡Embárcate en una aventura y lucha con los guardias del tesoro! Únete a la batalla con otros jugadores por el derecho de reunir recursos valiosos.


En cuanto a los requisitos:


**Mínimo:**  

+ **Procesador:** 2.2 GHz o superior
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** 512 MB, WebGL
+ **Almacenamiento:** 600 MB de espacio disponible


**Recomendado:**  

+ **Procesador:** Intel Core i5 o superior
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** 1 GB, WebGL
+ **Red:** Conexión de banda ancha a Internet
+ **Almacenamiento:** 600 MB de espacio disponible


Si quieres sumergirte en un universo multijugador masivo y echarle horas, tienes 'Wild Terra Online' en español en su página de Steam:


 <div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/500710/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Que te parece este 'Wild Terra Online'? ¿Te gustan los MMOs?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).



