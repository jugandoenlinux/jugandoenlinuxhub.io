---
author: leillo1975
category: Arcade
date: 2017-01-18 14:43:32
excerpt: <p>El juego del estudio italiano Trinity Team ya tiene fecha de Salida Oficial.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e17bf28887a04e7713039bc3ae638d41.webp
joomla_id: 182
joomla_url: bud-spencer-terence-hill-slaps-and-beans-llegara-a-gnu-linux
layout: post
tags:
- bud-spencer
- terence-hill
- slaps-and-beans
- trinity-team
title: "Bud Spencer & Terence Hill - Slaps And Beans llegar\xE1 a GNU/Linux (ACTUALIZADO)"
---
El juego del estudio italiano Trinity Team ya tiene fecha de Salida Oficial.

**ACTUALIZACIÓN 26-11-17:** Nos acaba de llegar la noticia gracias a una publicación de Facebook de que el juego saldrá definitivamente el día **15 de Diciembre en Steam**. Como veis, puede ser un regalo perfecto para estas navidades. En este momento podemos reservarlo en su [tienda oficial](shop.budspencerofficial.com):


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fbudspencerofficial%2Fvideos%2F767706643440632%2F&amp;show_text=1&amp;width=560" style="overflow: hidden; display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 Desde JugandoEnLinux.com os seguiremos ofreciendo toda la información referente a este títtulo en cuanto salga, y por supuesto, si tenemos la oportunidad algún gameplay comentado y un análisis. Recordad que en su día tuvimos [una entrevista](index.php/homepage/entrevistas/item/301-entrevista-con-trinity-team-bud-spencer-terence-hill-slaps-and-beans) con sus desarrolladores al poco de conocer este proyecto.


 




---


**NOTICIA ORIGINAL:** ¡Dios, que recuerdos! Hace bien poco no podiamos evitar apenarnos con la muerte de Bud Spencer, y después de conocer la existencia de este juego, no veais el alegrón que me he llevado. Seguro que no soy el único fan de estos dos grandes iconos del cine. Bud Spencer y Terence Hill, o Carlo Pedersoli y Mario Girotti, que así es como se llamaban de verdad; formaban esa pareja mágica que en mi caso me retrotrae a mi infancia, a aquellas sesiones de cine los domingos después de comer en un conocido cine de mi ciudad en compañia de los amigos o de mi padrino. Tardes de palomitas, gusanitos y petazetas divirtiendonos de lo lindo viendo a estos dos repartiendo mamporros a diestro y siniestro. "Dos super super esbirros", "y si no nos Enfadamos", "le Llamaban Trinidad"....peliculas con una calidad muy cuestionable, pero que divertían (y aun divierten).


Dejando el tema "cinéfilo", ayer supe de la existencia de un [proyecto de Kickstarter](https://www.kickstarter.com/projects/1684041218/bud-spencer-and-terence-hill-slaps-and-beans) que trata de homenajear a estos personajes y sus películas. Se trata de Slaps And Beans, un juego retro de scroll lateral al estilo  Streets of Rage, pero con posibilidad de juego colaborativo, donde aparte de soltar guantazos tendremos situaciones donde tendremos que utilizar un poco el coco, y no para dar cabezazos. En el juego veremos los típicos golpes de las peliculas, sus localizaciones, personajes, etc. También incluye minijuegos que servirán para desbloquear el progreso, como por ejemplo el concurso de cervezas y salchichas o las carreras de buggies. También contará con la banda sonora original de sus peliculas. ¿Quien no recuerda la [banda sonora](https://youtu.be/EBXedNzSC2w) de de "y si no nos enfadamos" y su [famosa escena del coro](https://www.youtube.com/watch?v=WtXeOhadmyI)?


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/jswDLLDWjJ4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


El juego está desarrollado por el estudio italiano Trinity Team, (supongo que el nombre viene de la pelicula "Le llamaban Trinidad"). Como comentan en la página de Kickstarter llevan años trabajando en el desarrollo de videojuegos y se decidieron a crear una demo de este juego, demo que podeis encontrar en itch.io  [para Window$ y MacOS](https://lochiamavanotriniteam.itch.io/schiaffifagioli). Esta obtuvo una muy buena acogida y se decidieron a crear un juego que hiciese justicia a estos dos personajes. Por cierto, han conseguido casi el doble de la cantidad que pedían en un principio, por lo que se supone que alcanzarán todas las metas de desarrollo. También cabe destacar que el juego ha sido aprobado en [Steam Greenlight](https://steamcommunity.com/sharedfiles/filedetails/?id=781553774), por lo que lo veremos a la venta en la conocida tienda digital. Su salida se espera a finales de este año...¿aguantaremos?


 


Otra cosa, estoy intentando ponerme en contacto con el equipo de desarrollo para preguntar un poco más sobre el juego y como está avanzando. Si lo consigo, daré cuenta de ello lo antes posible.


 


**ACTUALIZACIÓN:** Si quieres puedes leer la entrevista que realizamos a los desarrolladores de este juego en **[este enlace](index.php/item/301-entrevista-con-trinity-team-bud-spencer-terence-hill-slaps-and-beans)** 


 


**Fuentes:** [Kickstarter](https://www.kickstarter.com/projects/1684041218/bud-spencer-and-terence-hill-slaps-and-beans), [Steam Greenlight](https://steamcommunity.com/sharedfiles/filedetails/?id=781553774), [itch.io](https://lochiamavanotriniteam.itch.io/schiaffifagioli)

