---
author: leillo1975
category: Editorial
date: 2018-11-13 11:41:42
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\">@BoilingSteam nos trae otro de sus grandes\
  \ art\xEDculos.</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/787fe504fcc37d9fd342c3584614fb9d.webp
joomla_id: 901
joomla_url: james-ramey-de-codeweavers-es-entrevistado-en-boiling-steam
layout: post
tags:
- wine
- valve
- entrevista
- boiling-steam
- proton
- codeweavers
title: James Ramey, de Codeweavers, es entrevistado en Boiling Steam.
---
@BoilingSteam nos trae otro de sus grandes artículos.

Como viene siendo habitual, cada cierto tiempo, **Ekianjo**, el redactor de ese magníco blog que es [Boiling Steam](https://boilingsteam.com/), y que desde aquí os volvemos a recomendar una vez más que incluyais en vuestros favoritos, realiza una entrevista a una personalidad del sector de los videojuegos para Linux/SteamOS. En el pasado nos hicimos eco de la entrevista a [Tim Besset](index.php/homepage/noticias/39-noticia/719-segun-tim-basset-el-port-de-street-fighter-v-no-esta-muerto), [Peter Mulholland](index.php/homepage/editorial/4-editorial/924-interesante-entrevista-a-peter-mulholland-en-boiling-steam) o [LinuxVanGOG](index.php/homepage/generos/rol/7-rol/774-cyberpunk-2077-en-linux-si-por-favor). En esta ocasión podremos disfrutar también de un extenso artículo (y podcast) donde encontraremos suculenta y esclarecedora información sobre muchos temas, preguntados en esta ocasión a [James Ramey](https://www.codeweavers.com/about/people/jramey), Presidente de [Codeweavers](https://www.codeweavers.com/), la empresa que está detrás de Wine y Crossover, y que ahora también está involucrada junto a Valve en Proton.


No vamos a destriparos la entrevista en este artículo, porque además de que es muy extensa y preferimos que la leais en su integridad; pero podemos adelantaros algunos de los puntos sobre los que trata y que la hacen especialmente relevante. Durante esta charla, el presidente de Codeweavers, nos habla un poco en que consiste su **relacción con Valve y otros desarrolladores**, su **forma de trabajar** a la hora de afrontar el reto de hacer funcionar un juego, la **situación actual de su empresa** con respecto a los programas profesionales, su opinión sobre el **futuro de MacOS** o el **crecimiento de Linux** en el terreno de los juegos o el hardware. Sin más os dejamos el enlace a la Entrevista (y Podcast) para que vosotros mismos saqueis conclusiones:


[PODCAST #10: James Ramey (Codeweavers) on Steam Play/Proton](https://boilingsteam.com/podcast-10-james-ramey-codeweavers-on-steam-play-proton/)
================================================================================================================================================


Qué, ¿ya la habeis leido? A nuestro juicio nos resulta especialmente llamativo y esperanzador el ver la **actitud de Valve** con respecto a Linux, **poniendo delante a lo que demandan los jugadores por encima de los beneficios empresariales**, y que realmente están comprometidos con Linux. Y a vosotros ¿qué os ha parecido? ¿Cuales son los puntos más interesantes de la entrevista? Dejanos tus opiniones en los comentarios o charla con nosotros en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

