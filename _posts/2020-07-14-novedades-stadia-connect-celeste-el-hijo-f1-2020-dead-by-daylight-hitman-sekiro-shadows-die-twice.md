---
author: Pato
category: Noticia
date: 2020-07-14 19:53:41
excerpt: "<p>Y mucho, mucho m\xE1s por parte de&nbsp;@GoogleStadia</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/Stadiaconnect072020.webp
joomla_id: 1240
joomla_url: novedades-stadia-connect-celeste-el-hijo-f1-2020-dead-by-daylight-hitman-sekiro-shadows-die-twice
layout: post
tags:
- hitman
- celeste
- stadia
- f1-2020
- el-hijo
- stadia-connect
- sekiro-shadows-die-twice
title: 'Novedades Stadia Connect: Celeste, El Hijo, F1 2020, Dead by Daylight, Hitman,
  Sekiro: Shadows Die Twice...'
---
Y mucho, mucho más por parte de @GoogleStadia


 


Stadia sigue aumentando funcionalidades y catálogo. Hoy sin ir mas lejos, la plataforma de Google ha emitido un *Stadia Connect* durante el cual nos han anunciado un buen puñado de novedades que están por llegar y que podremos disfrutar en nuestro sistema favorito. Comenzamos:


La semana pasada no pudimos reseñar las novedades de la plataforma, por lo que [comenzamos por allí](https://community.stadia.com/t5/Stadia-Community-Blog/This-Week-on-Stadia-Blaze-your-way-to-Formula-1-glory/ba-p/26199), con el anuncio de la disponibilidad de F1 2020, ya sin el paraguas de Feral que nos llega en su versión [Deluxe Schumacher Edition](https://stadia.com/link/ykvHM8NbUqUyzAuUA) que incluye el juego base, el **add-on Seventy Edition** con múltiples personalizaciones y cuatro coches icónicos de Michael Schumacher, a saber:


* 1991 Jordan 191
* 1994 Benetton B194
* 1995 Benetton B195
* 2000 Ferrari F1-2000


Por otra parte, también está disponible la **F1 2020 Seventy Edition** algo más económica, con extras por en 70 aniversario de la F1.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/ufcapkFeXIw" width="560"></iframe></div>


Sigamos con **Destiny 2: Beyond Light,** que ya está disponible en *pre-compra* para cuando salga en Septiembre, y... bueno, solo 19 juegos que puedes jugar gratis si eres suscriptor de Stadia Pro este mes de Julio. Ahí es nada:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/50TS7odDCOI" width="560"></iframe></div>


En cuanto a [esta semana](https://community.stadia.com/t5/Stadia-Community-Blog/Explore-emotional-journeys-with-narrative-games-coming-soon-to/ba-p/26271), hemos tenido el anuncio de la próxima llegada de el excelente **Celeste,** del que no hacen falta muchas presentaciones y un "desconocido" **El Hijo**, un juego de puzzles y aventuras en el que tendremos que acompañar a un niño en sus periplos por el lejano oeste.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/DZGoLW4PFL4" width="560"></iframe></div>


 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/wpPbNgFEetU" width="560"></iframe></div>


Vamos con el **Stadia Connect** que ha tenido lugar hoy mismo. Esta vez, desde Stadia han hecho un gran énfasis en lo que a las funcionalidades de Stadia se refiere, a saber, la facilidad de retransmitir las partidas de Stadia a través de Youtube, y la funcionalidad "**Clic and Play**", con la cual **mediante un enlace puedes conectar inmediatamente y jugar justo en la partida que el Youtuber está emitiendo**. Esta puede ser una funcionalidad que puede marcar diferencias respecto a otras plataformas, y que a poco que Stadia sepa explotar puede dar mucho juego. En cuanto a los juegos anunciados en el Stadia Connect, son... bueno, son unos cuantos, así que mejor os pongo los vídeos, y sacáis vuestras conclusiones:


**Tom Clancy's Ghost Recon Breakpoint**


 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/rV5wg7osH9A" width="560"></iframe></div>


 


**Assassin's Creed Valhalla**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/IqDFLfBw6sg" width="560"></iframe></div> 


 


**Far Cry 6**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/6emmTdUZaVc" width="560"></iframe></div>


 


**Watch Dogs: Legion**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/6DR0qZ3MYzg" width="560"></iframe></div>


 


**SUPER BOMBERMAN R ONLINE**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/CglZtgOmvqg" width="560"></iframe></div>


 


**Dead by Daylight**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/L8SFMKv8U1c" width="560"></iframe></div>


 


**The Elder Scrolls Online - Stonethorn**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Xxlssu7KUXA" width="560"></iframe></div>


 


**Hello Neighbor**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/CQagbDaBWn0" width="560"></iframe></div>


 


**One Hand Clapping**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/EzrqMkIrQnI" width="560"></iframe></div>


 


**Outriders: Journey Into the Unknown**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/5JhOuLZGrn0" width="560"></iframe></div>


 


**Sekiro: Shadows Die Twice**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/UDnTs2RsvZo" width="560"></iframe></div>


 


**PUBG - Season 8**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/ivsA_1VNPhA" width="560"></iframe></div>


 


**Hitman 3**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/HNUsFk0-1GU" width="560"></iframe></div>


 


**Serious Sam 4**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/ByOYJuWU8Jg" width="560"></iframe></div>


 


**Outcasters**(solo en Stadia)


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/rbbUM7MVOGU" width="560"></iframe></div>


 


**Orcs Must Die! 3**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/3XFpoJsOkKI" width="560"></iframe></div>


 


No está mal, ¿verdad?. Además, Stadia está llevando a cabo sus rebajas especiales de verano, que puedes ver [en este enlace.](https://stadia.google.com/store/list/77)


Os dejo con un resumen del Stadia Connect:


 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/bzKJ9uzdH-0" width="560"></iframe></div>

