---
author: Serjor
category: Terror
date: 2017-06-25 18:19:55
excerpt: "<p>Descubre qu\xE9 sucede por una no tan tranquila casa andaluza</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bf337492a0864d5a8d1619941820aa93.webp
joomla_id: 388
joomla_url: analisis-evil-possession
layout: post
tags:
- indie
- analisis
- evil-possession
- survival-horror
title: 'Analisis: Evil Possession'
---
Descubre qué sucede por una no tan tranquila casa andaluza

Disclaimer: La clave ha sido proporcionada por la desarrolladora


Cómo ya os [anunciábamos](index.php/homepage/generos/terror/item/426-evil-possession-el-prometedor-juego-de-terror-llegara-a-linux-el-proximo-17-de-mayo), el 17 de mayo de 2017 2Dragons Games, una empresa indie del panorama nacional, presentaba Evil Possession, un survival horror ambientado en una casa andaluza de mediados del siglo XX, en la que al poco de llegar descubrimos que pasan cosas cuanto menos extrañas.


Poco se puede contar del juego como tal, el escenario es una casa de dos pisos por donde deberemos movernos teniendo que buscar elementos o llevar a cabo acciones, todo esto huyendo de un ente maligno que nos persigue, por lo que la mecánica nos recordará mucho al conocido Slender. El problema está en que el juego pretende ser un survival horror y no es un survival horror. En ningún momento se genera tensión, hay que huir del mal, al cuál se ve venir y con dar media vuelta y correr un poco escaparemos de él, pero el tema de los objetivos está muy mal llevado, hasta el punto en que para poder cumplir con ellos hay que hacerlo bajo un acto de convicción y tozudez extrema, porque el juego nos invita constantemente a que lo quitemos y nos dediquemos a otra cosa.


Francamente, es un juego bastante tedioso, donde para poner las cosas difíciles al jugador se le castiga por sus errores. Yo personalmente reconozco que no supe moverme por la casa y andaba constantemente entrando por las diferentes habitaciones buscando el objeto o el lugar que tocara en ese momento una y otra vez sin muchas veces saber donde estaba o qué era lo que ya había visitado, así que mucho del tiempo invertido en el juego ha sido dar vueltas por los mismos sitios una y otra vez sin saber ni donde estaba ni donde iba. ¿Es esto culpa del juego? Sí y no. La casa no es grande y con tener un poco de memoria podría ser suficiente, pero también es cierto que es todo el rato igual, y yo echaba de menos un pequeño mapa para ni que sea poder montar una estrategia de habitaciones a visitar o ya visitadas, porque mientras visitaba habitaciones muchas veces tenía que cambiar de escenario cuando aparecía el ente maligno.


Además, por si fuera poco, a un mal planteamiento jugable, que frustra y aburre en vez de oprimir y asustar, hay que sumarle problemas serios en el motor. El juego está basado en Unity, no sé exactamente qué versión, pero durante toda la partida la pantalla parpadeaba a negro cada dos por tres y de una manera constante y molesta. Entiendo que esto no es problema de los desarrolladores, y que es un problema del propio Unity, pero al menos en Linux no es para nada jugable porque molesta bastante.


La verdad es que me apena bastante que el juego falle en lo fundamental, que es hacer que el jugador pase miedo, o por lo menos llevarle a ese estado de tensión que hace que estemos a alerta constantemente, porque en el fondo la idea es buena. Yo no he llegado a acabarme el juego y eso que sé que es muy muy corto, y me fastidia porque algo que sí que han conseguido es que quiera saber qué pasa en la dichosa casa, pero es tan aburrido, tan injusto y plantea tan mal la forma de retar al jugador, que no he podido llegar hasta el final, así que haré como se hace en estos tiempos modernos y buscaré el final en youtube, porque bajo mi criterio, aunque no es nada nuevo ni nada que no se haya contado ya de mil formas diferentes, tenía su punto.


Si tenéis un rato largo en el que no sabéis que hacer, podéis un gameplay grabado para la ocasión. Pido perdón por el audio, no sé porqué se entrecortaba tanto al hablar y reconozco que es molesto:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/znrOZEM58Hk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Y si queréis echarle un vistazo en steam:


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/620700/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Y tú, ¿te atreves a desentrañar el misterio de una Andalucía de mediados del XX? Cuéntanos qué te parece el juego en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

