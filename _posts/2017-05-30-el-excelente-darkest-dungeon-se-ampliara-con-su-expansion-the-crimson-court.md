---
author: Pato
category: Estrategia
date: 2017-05-30 10:29:49
excerpt: "<p>La expansi\xF3n vendr\xE1 en forma de DLC de pago el pr\xF3ximo 19 de\
  \ Junio</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3bfa5a7e41ccb228078a88e28d2b13ec.webp
joomla_id: 347
joomla_url: el-excelente-darkest-dungeon-se-ampliara-con-su-expansion-the-crimson-court
layout: post
tags:
- rol
- estrategia
- turnos
- fantasia-oscura
title: "El excelente 'Darkest Dungeon' se ampliar\xE1 con su expansi\xF3n \"The Crimson\
  \ Court\" (actualizado: Ya disponible)"
---
La expansión vendrá en forma de DLC de pago el próximo 19 de Junio

En Jugando en Linux no hemos tenido oportunidad de hablar mucho sobre 'Darkest Dungeon' [[web oficial](http://www.darkestdungeon.com/)]. Hoy, gracias a [linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/darkest-dungeon-first-expansion-crimson-court-release-linux-windows-games-53125/) nos enteramos que "Red Hook Studios" planea lanzar una expansión para este excelente juego de estrategia por turnos ambientado en un universo de fantasía oscura.


*Se trara de un desafiante juego de rol gótico en mazmorras y por turnos que gira en torno al esfuerzo psicológico de la aventura.*


*Recluta, entrena y lidera a un equipo de héroes imperfectos a través de enrevesados bosques, laberintos olvidados, criptas en ruinas y más allá. Te enfrentarás no solo a enemigos inimaginables, sino al esfuerzo, el hambre, la enfermedad y la siempre acechante oscuridad. Descubre extraños misterios y enfrenta a los héroes a una serie de temibles monstruos con un innovador sistema de combate estratégico por turnos.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/h-mXN3akTPU" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


#### CrimsonCourt


#### Llega "The Crimson Court"


"The Crimson Court" es la nueva expansión que llegará el próximo día 19 de Junio y nos traerá nuevo contenido al juego original, como una nueva campaña, nuevos jefes finales, una nueva región "The Courtyard", una nueva facción de enemigos y mucho más. Puedes ver todas las características en el [anuncio oficial de la expansión](http://steamcommunity.com/games/262060/announcements/detail/1262543810907492439). El DLC tendrá un precio de 9,99$. Aún no sabemos el precio que tendrá en euros.


'Darkest Dungeon' llegó a Linux/SteamOS hace ahora aproximadamente un año, y lo puedes conseguir en su página de Steam traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/262060/33877/" width="646"></iframe></div>


**ACTUALIZACIÓN:** Ya puedes comprar la expansión "The Crimson Court" en su página de Steam donde también tienes todos los detalles:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/580100/" width="646"></iframe></div>


¿Que te parece 'Darkest Dungeon'? ¿Te gustan los juegos de fantasía oscura? ¿Piensas jugar la expansión?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

