---
author: leillo1975
category: Carreras
date: 2020-01-31 10:10:41
excerpt: "<p>Ethan Lee (&nbsp;<span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x\
  \ r-bcqeeo r-qvutc0\">@flibitijibibo</span> )<span class=\"css-901oao css-16my406\
  \ r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"> es el resposable una vez m\xE1s&nbsp;</span></p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/LittleRacersSTREET/LittleRacersStreet.webp
joomla_id: 1157
joomla_url: tras-varios-anos-little-racers-street-tiene-nueva-beta
layout: post
tags:
- ethan-lee
- milkstone
- fna
- little-racers-street
title: "Tras varios a\xF1os, Little Racers STREET tiene nueva versi\xF3n (ACTUALIZADO)\
  \  "
---
Ethan Lee ( @flibitijibibo ) es el resposable una vez más 


**ACTUALIZADO 6-2-20**: Milkstone [acaba de anunciar en Steam](https://steamcommunity.com/app/262690/eventcomments/1749021985766889311) que la actualización de la versión de linux del juego es oficial, por lo que ya no es necesario tener marcada la beta, a la vez que agradece a Ethan Lee su esfuerzo.




---


**NOTICIA ORIGINAL**: Es bien seguro que muchos de vosotros conozcais y seais poseedores de este juego, pues la verdad hace ya bastantes años que está disponible para nuestro s-istema (2014), y a decir verdad ha sido muy injustamente tratado, pues se trata de un juego muy divertido, especialmente en multijugador. Desarrollado por la **compañía asturiana** [Milkstone](http://www.milkstonestudios.com/), responsables también del conocido [Farm Together](index.php/listado-de-categorias/estrategia/875-farm-together-sale-de-early-access). A decir verdad, el port no es de los mejores que hemos visto, pues su rendimiento dejaba un poco que desear, y además desde hace algún tiempo había que hacer algunos "[chanchullos](index.php/foro/tutoriales/146-como-hacer-funcionar-little-racer-streets-en-ubuntu-18-04)" para hacerlo funcionar debido a un problema de dependencias.


Bien, pues gracias al mensaje indicándonos un [tweet](https://twitter.com/flibitijibibo/status/1222944857183981569) que esta mañana nos encontramos en nuestro grupo de [Telegram](https://t.me/jugandoenlinux) (gracias **@DNModder** ), nos hemos enterado que **Ethan Lee**, conocido por [portar multitud de juegos a nuestro sistema](http://www.flibitijibibo.com/index.php?page=Portfolio/Ports), ha retomado el proyecto y ha estado trabajando para rehacerlo desde cero usando [FNA](https://fna-xna.github.io/) , su propia implementación de Microsoft XNA, Tras hablar con los desarrolladores se puso [manos a la obra](https://icculus.org/finger/flibitijibibo) y **en dos días** ha lanzado una versión beta completamente funcional que podemos encontrar en las propiedades del juego en Steam (Pestaña Beta y seleccionar en el desplegable "osx_linux_test"). Hay que resaltar que **este port es el número 60 en su carrera**, por lo que debemos estarle completamente agradecidos por tanto y tan buen trabajo. Recordad que gracias a él podemos disfrutar de Terraria, FEZ, Capsized, Dust: An Elysian Tail, Transistor, Mercenary Kings, Owlboy, Bastion, Celeste, [Wonder Boy: The Dragons Trap](index.php/component/k2/20-analisis/564-analisis-wonder-boy-the-dragon-s-trap)....


Pues ya sabeis, ahora no tenemos excusa para volver a echar unas partidas en multi un viernes por la noche o cualquier otro día, tal y como hicimos ya hace algún tiempo como podeis en el video que teneis a continuación. Si antes se podía, ahora si que no tenemos excusa:


 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/EIue-WvbIDc" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Podeis comprar Little Racers STREET en [Steam](https://store.steampowered.com/app/262690/Little_Racers_STREET/). ¿Has jugado alguna vez a Little Racers STREET? ¿Qué te parece el trabajo de Ethan Lee? Puedes contarnos lo que quieras sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

