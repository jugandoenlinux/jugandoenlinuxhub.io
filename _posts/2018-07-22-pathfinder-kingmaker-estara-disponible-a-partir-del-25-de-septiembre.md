---
author: Serjor
category: Rol
date: 2018-07-22 13:37:39
excerpt: <p>Ya podemos adquirir el juego en pre-compra</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/48b2caa4acdcf286e67d646faa59fcbf.webp
joomla_id: 808
joomla_url: pathfinder-kingmaker-estara-disponible-a-partir-del-25-de-septiembre
layout: post
tags:
- rpg
- deep-silver
- pathfinder-kingmaker
- crpg
title: "Pathfinder: Kingmaker estar\xE1 disponible a partir del 25 de septiembre"
---
Ya podemos adquirir el juego en pre-compra

Gracias a Twitter nos encontramos con lo siguiente:



> 
> The critically acclaimed cRPG Pathfinder: Kingmaker is releasing on September 25! Aspiring adventurers can pre-order now.  
>   
> Steam:<https://t.co/u9KrMPZuN6><https://t.co/OjA17mIV9w>:<https://t.co/wDw0k92Wiw> [pic.twitter.com/3TWJ3mlRzk](https://t.co/3TWJ3mlRzk)
> 
> 
> — Official Deep Silver (@deepsilver) [20 de julio de 2018](https://twitter.com/deepsilver/status/1020357572954677249?ref_src=twsrc%5Etfw)







 Y es que como podéis ver, el próximo 25 de septiembre tendremos disponible el juego Pathfinder: Kingmaker, el cuál ha sido desarrollado por Owlcat Games, y editado por Deep Silver, la desarrolladora de Dead Island y Dying Light, ambos disponibles en GNU/Linux.


Este Pathfinder: Kingmaker es un RPG con campaña para un jugador, en vista isométrica, inspirado en los fallout originales y que se enmarca dentro del universo del juego de mesa Pathfinder. Además, fue un éxito en kickstarter, ya que con más de 18000 bakers llegó a recaudar algo más 900000$ cuando solamente necesitaban 5000000$, todo un logro, que esperemos que se haya traducido en un juego a la altura de las expectativas.


Si estáis interesados, ya podéis hacer la pre-compra tanto en Steam como en GOG, o a través de este link de afiliados de Humble Bundle: <https://www.humblebundle.com/store/pathfinder-kingmaker-explorer-edition?partner=jugandoenlinux>


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/P1ENEIytFG8" width="560"></iframe></div>


Y tú, ¿estás interesado en este Pathfinder: Kingmaker? Cuéntanoslo en los comentarios, o en nuestros canales de Telegram y Discord.

