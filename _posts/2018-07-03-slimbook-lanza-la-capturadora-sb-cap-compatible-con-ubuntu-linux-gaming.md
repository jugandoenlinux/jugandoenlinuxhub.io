---
author: Pato
category: Hardware
date: 2018-07-03 15:40:20
excerpt: "<p>La capturadora cuenta con el soporte de la compa\xF1\xEDa espa\xF1ola\
  \ @SlimbookEs</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/02c3690c1c0f065ff00387ae9425f29c.webp
joomla_id: 792
joomla_url: slimbook-lanza-la-capturadora-sb-cap-compatible-con-ubuntu-linux-gaming
layout: post
tags:
- slimbook
- sbcap
title: Slimbook lanza la capturadora SB-CAP compatible con Ubuntu Linux Gaming
---
La capturadora cuenta con el soporte de la compañía española @SlimbookEs

¿Eres de los que disfruta jugando y haciendo streamings a tus plataformas favoritas? ¿O eres de los que graban sus partidas para mostrar sus jugadas maestras?


Entonces estás de enhorabuena, ya que [Slimbook](https://slimbook.es/), la compañía española especializada en ordenadores con soporte completo en Linux nos trae esta SB-CAP, una capturadora con compatibilidad completa para Ubuntu.


Se trata de una solución "todo en uno". Hasta ahora era difícil si no imposible tener una capturadora que tuviese soporte apropiado en nuestro sistema favorito. Slimbook se dió cuenta de esta necesidad y nos trae esta solución enfocada a la captura de imagen sobre Linux:


*Capturadora FullHD 1080p compatible con Linux, Windows y Macintosh:*


*Graba lo que estás haciendo en tu ordenador (sobremesa o portátil) o en cualquier otro aparato con salida HDMI, a otro ordenador. Tan sólo has de conectar el ordenador o aparato por HDMI a la capturadora, y esta conectarse:*


*- Por USB a un ordenador con Ubuntu (el mismo pc u otro) y con un software como OBS grabar lo que recibe por el HDMI de la catpuradora.*


*- Por HDMI a un monitor, un proyector, una TV, etc. para tener en pantalla lo que ves en tu ordenador.*


![CapturadoraSlimbook1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Slimbook/CapturadoraSlimbook1.webp)


La SB-CAP cuenta con puertos HDMI de entrada y salida, entrada micro, USB 3.0 y alimentación por microUSB.


Si estás interesado, puedes visitar su tienda [en este enlace](https://slimbook.es/pedidos/imagen-y-sonido/capturadora-compatible-con-ubuntu-linux-comprar) donde está disponible al módico precio de 89€


¿Que te parece esta capturadora de Slimbook? ¿Te gustaría ver más periféricos u ordenadores de Slimbook enfocados al videojuego en Linux?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

