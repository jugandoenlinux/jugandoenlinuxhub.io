---
author: leillo1975
category: "Exploraci\xF3n"
date: 2021-08-26 15:02:05
excerpt: "<p>Ya podemos disfrutar de las novedades de @ValheimGame con esta gran actualizaci\xF3\
  n.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/HHLogoKitchen.webp
joomla_id: 1348
joomla_url: hearth-home-la-primera-gran-actualizacion-de-valheim
layout: post
tags:
- unity3d
- valheim
- iron-gate-studio
- coffee-stain
title: "HEARTH & HOME la primera gran actualizaci\xF3n de Valheim"
---
Ya podemos disfrutar de las novedades de @ValheimGame con esta gran actualización.


**ACTUALIZACIÓN 16-9-2021**: Y llegó el gran día que todos los amantes de Odin estabais esperando, el lanzamiento de esta gran actualización de Hearth & Home. En nuestros [canales](https://telegram.me/jugandoenlinux) ya se cocía la ansiedad por probar esta nueva versión y parece que la actualización en [nuestro servidor]({{ "/posts/servidor-dedicado-de-valheim-oficial-de-jugandoenlinux-com" | absolute_url }}) de Valheim ha ido como la seda.


Además de todas las novedades que ya os adelantamos, aquí van algunas mas que se han desvelado hoy con este lanzamiento:


* Se ha incluido **algo oscuro y misterioso que parece extenderse por las llanuras**. Ve a descubrirlo con tus propios ojos ¿Algún valiente?.
* Tras escuchar a la comunidad también se ha implementado el **uso compartido de mapas entre jugadores**, podrás intercambias datos con ellos.
* Otro añadido pedido por la comunidad, ahora los jugadores **pueden nombrar a sus mascotas Lox** a su gusto para infundir miedo en los corazones de sus enemigos.
* Nuevas **semillas para plantar otras especies de arboles.**
* Se puede destruir algunos tipos de objetos con una nueva extensión de destructor. Menos contaminación en los océanos de Valheim.
* **Materiales y piezas añadidas** como las vigas y postes de madera oscura decorados con intrincados adornos vikingos, un nuevo tipo de techo y adornos de madera oscura, o un trono de piedra para quienes gobiernan con puño de hierro.
* **Nuevos alimentos y recetas** como la carne de ciervo, lobo y jabalí, cecina y brochetas, junto con muckshakes, cebollas y sopa de cebolla.
* Nuevo hacha de batalla de cristal, el escudo de la torre de hueso, el escudo de hierro y el cuchillo de plata también se han agregado al arsenal.
* Ajustes en los arcos y hachas de batallas, también en los escudos y sus mecánicas de bloqueo.
* También se han hecho corregido fallos y trabajo de optimización, como siempre.


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/GlePlUL9npA" title="YouTube video player" width="780"></iframe></div>


Cuéntanos si has probado esta nueva actualización en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)


 




---


**NOTICIA ORIGINAL 26-08-2021:** Seguro que ya estais hartos de ver videos, screenshots y noticias por ahí sobre la próxima actualización importante de este auténtico bombazo de juego, pero es gracias a todo el material e información que nos ha enviado [Jesús Fabre](https://twitter.com/jesusfabre?lang=es), relacciones públicas para [Coffee Stain](https://www.coffeestainstudios.com/), (la editora del juego), que nos hemos decidido a redactarla para que tengáis mucho más claro en que han estado ocupados todos estos meses los chicos de [Irongate Studio](https://irongatestudio.se/), la compañía sueca detrás del juego.


Lo primero que tenemos que decir es que no habrá que esperar mucho para poder empezar a sacarle partido a todas las novedades que incluirá Heart & Home, que es así como se llamará está actualización. En tan solo 3 semanas, **el día 16 de Septiembre**, se instalará en nuestros equipos y comenzaremos a darle buen recaudo.


![HHFoodTable](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/HHFoodTable.webp)


Entre las **muchas características** que tendrá esta actualización destacamos:


*- **Nuevas extensiones de cocina** para agregar especiadores, mesas de carnicero, ollas y sartenes.*


*- **Sistema alimentario rediseñado:** no solo se han introducido nuevas recetas, sino que ahora se ofrece a los jugadores elecciones más interesantes con un sistema mejorado de salud y resistencia. Tendrá un impacto significativo en la experiencia de combate principal para los jugadores, permitiéndoles equilibrar finamente su dieta vikinga para adaptarse a su estilo de juego.*


*- **Cambios adicionales en los escudos y la mecánica de bloqueo**, que darán a los jugadores batallas aún más épicas. Utilizar el escudo torre o la rodela modificará la forma de bloquear de nuestro personaje.*


*- **Se agregará una cueva del tesoro con nuevas piezas de construcción**, incluido literalmente un cofre del tesoro para que los vikingos almacenen su oro ganado con tanto esfuerzo.*


*- Construcción de **trampillas de ventana completamente nuevas** y de materiales de **madera oscura**.*


![HHTreasureRoom2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/HHTreasureRoom2.webp)


Como [comentamos en anteriores ocasiones]({{ "/posts/valheim-llega-finalmente-a-acceso-anticipado-a5" | absolute_url }}) el equipo de desarrollo del juego es estaba formado inicialmente por solo **cinco personas**, usando para ello **GNU-Linux,** y ha conseguido vender más de **siete millones de copias en menos de dos meses** y alcanzar un pico de más de **500,000 jugadores simultáneos**, lo que lo convierte en el quinto juego en llegar a esta increíble marca en la historia de Steam.


Como sabeis, podeis haceros con Valheim por tan solo **16.79 Euros** en [Steam Early Access](https://store.steampowered.com/app/892970/Valheim/) para Windows y Linux. También os recordamos que tenemos un [servidor dedicado oficial de JugandoEnLinux]({{ "/posts/servidor-dedicado-de-valheim-oficial-de-jugandoenlinux-com" | absolute_url }}) para que podais jugar todos juntos, hijos de Odín! Os dejamos con con el video cinemático presentado en la Gamescon de este año:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/pZbVOl8tAmg" title="YouTube video player" width="780"></iframe></div>

