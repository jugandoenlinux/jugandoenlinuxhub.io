---
author: leillo1975
category: Estrategia
date: 2019-04-26 09:11:50
excerpt: "<p><span class=\"username u-dir\" dir=\"ltr\">@transportfever 2 se actualiza\
  \ incluyendo @VulkanAPI&nbsp; . <br /></span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/eb63149d9c12ea56cb5de9879451c7b7.webp
joomla_id: 1032
joomla_url: transport-fever-2-llegara-a-linux-a-finales-de-este-ano-a3
layout: post
tags:
- estrategia
- vulkan
- simulacion
- transport-fever-2
- urban-games
title: "\"Transport Fever 2\" disponible en Linux (ACTUALIZACI\xD3N 3) "
---
@transportfever 2 se actualiza incluyendo @VulkanAPI  .   



**ACTUALIZADO 23-2-21:**


Ha pasado bastante desde la última vez que os hablamos de este fántástico juego de **gestion y simulación de medios de transporte**, pero lo cierto es que a lo largo de este tiempo la gente de **Urban Games** ha publicado varias actualizaciones, aunque sin duda esta última merece toda nuestra atención, pues **el título acaba de incluir soporte para la API de Vulkan**, por lo que se supone que esto **ayudará a tener un mejor rendimiento de Transport Fever 2** tanto en nuestro sistema, como en Windows. La lista de cambios es extensa y sería muy largo describir todas las mejoras, arreglos y añadidos que han incluido en esta actualización, por lo que si quereis  saber más podeis hacerlo en este [enlace](https://www.transportfever2.com/wiki/doku.php?id=releasenotes).




---


**ACTUALIZADO 11-12-19:**


Nos acaba de llegar [confirmación por parte de Urban Games](https://www.transportfever2.com/transport-fever-2-officially-released/) de que el juego ha sido puesto a la venta oficialmente. En el juego podreis encontrar:


*Proporciona al mundo la infraestructura de transporte que necesita y haz una fortuna con servicios de transporte a medida. Observa cómo tus trenes circulan sobre rieles, cómo tus autobuses y camiones truenan a lo largo de las carreteras, cómo tus barcos navegan a través del agua y cómo tus aviones vuelan a través de los cielos. Llevar a la gente de camino al trabajo o al recreo, y ser la razón por la que las ciudades crecen y prosperan. Suministra materias primas y bienes para impulsar la economía. Experimenta los mayores desafíos logísticos desde 1850 hasta la actualidad, y construye un imperio de transporte sin igual en todo el mundo.*


* *Juego libre con innumerables posibilidades de configuración*
* *Tres campañas en tres continentes con más de 20 horas de juego*
* *Editores para crear mapas y modificar partidas guardadas*
* *Tres tipos de paisaje: moderado, seco y tropical*
* *Vehículos de Europa, América y Asia modelados de manera realista*
* *Más de 200 vehículos: trenes, autobuses, tranvías, camiones, aviones y barcos*
* *Estaciones de tren, paradas de autobuses y camiones, aeropuertos y puertos modulares*
* *Simulación de transporte realista que incluye calles de sentido único y semáforos*
* *Terreno editable que se puede pintar con efectos realistas*
* *Herramientas de construcción intuitivas para construir ferrocarriles y más*
* *Visualización de datos importantes, como el tráfico y las emisiones, en capas separadas*
* *Simulación de ciudades y de economía dinámica*
* *Más de diez cadenas económicas con fábricas y bienes asociados*
* *Ciudades con innumerables edificios residenciales, comerciales e industriales*
* *Mundo de juego detallado con iluminación basada en la física*
* *Animales terrestres, aves y peces simulados individualmente*
* *Más de 50 logros desafiantes en el modo de juego libre*
* *Posibilidad de usar mods a través de Steam Workshop*


Desde JugandoEnLinux.com **os recomendamos este juego si os gustan los títulos de gestión y simulación de Transporte**, pues gracias a Urban Games hemos tenido la oportunidad de probarlo durante la fase de Betatesting, y ciertamente resulta muy divertido.Os dejamos con el trailer de lanzamiento para que se os haga la boca agua:


**<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/yK2__WGmhPw" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>**


 


Podeis adquirir Transport Fever 2 en la [Humble Store (patrocinado)](https://www.humblebundle.com/store/transport-fever-2?partner=jugandoenlinux). 


 




---


**ACTUALIZADO 6-11-19:** 


Acabamos de recibir un correo por parte del departamento de prensa de [Urban Games](https://www.transportfever.com/about/urban-games/), que la esperada segunda parte de su conocido Transport Fever, llegará a [Steam](https://store.steampowered.com/app/1066780/Transport_Fever_2/) el **Miércoles, 11 de Diciembre** a nuestros PC's, incluyendo desde el primer día a nuestro sistema operativo. También, para celebrar el tercer aniversario del lanzamiento, se pondrá a la venta hasta el viernes 8 de Noviembre, el primer Transport Fever con un [descuento del 80% (6.40€)](https://store.steampowered.com/app/446800/Transport_Fever/). Os dejamos con el "Release Date Trailer" que acompaña este anuncio:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/HHji2C9thqw" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL:**


Una vez más gracias a [LinuxGameConsortium](https://linuxgameconsortium.com/linux-gaming-news/transport-fever-2-simulation-announcement-75909/) os traemos esta jugosa información de que el juego creeado por los suizos [Urban Games](https://www.transportfever.com/about/urban-games/) y distribuido por los holandeses [Good Shepherd Entertaiment](https://goodshepherd.games/en/) **llegará a nuestro sistema a finales de este año**. tal y como podemos ver en la [página de Steam,](https://store.steampowered.com/app/1066780/Transport_Fever_2/) que ya está habilitada, donde **el juego lista requisitos técnicos para poder disfrutarlo incluyendo nuestro sistema**. Se sigue así la estela abierta con [la primera parte del juego](https://www.humblebundle.com/store/transport-fever?partner=jugandoenlinux), que lanzada hace poco más de dos años ya incluía soporte.


En **Transport Fever 2** podrás convertirte en un magnate del transporte **comenzando en 1850** y **creando una compleja red de ferrocarriles, carreteras, aeropuertos, puertos**... pudiendo utilizar **hasta 200 tipos de vehículos diferentes basados en modelos realistas de Europa, América y Asia**. Como novedades con respecto a la anterior versión encontraremos un juego mucho más desarrollado con mayor profundidad, con la **posibilidad de crear contenido personalizado** y mods a través de **Steam Workshop**. Habrá 3 campañas y por supuesto juego libre, pudiendo encontrar terrenos tropicales, moderados y secos.  Las posibilidades de edición permitirán editar y pintar el terreno, entre otras muchas herramientas de construcción intuitivas para la construcción de ferrocarriles, por ejemplo. También podremos desbloquear 50 logros. Los **requisitos técnicos** que necesitaremos cumplir ( Intel i5-2300 o AMD FX-4300, 8 GB de RAM, NVIDIA GeForce GTX 560 o AMD Radeon HD 7850, 2 GB VRAM) , serán de lo más comedidos, por lo que la mayoría de los jugadores podrán disfrutarlo


Por supuesto, en JugandoEnLinux.com os seguremos informando de las novedades que poco a poco vayamos conociendo de este juego y por supuesto cuando finalmente esté disponible. Os dejamos disfrutando con el trailer del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/8zaV9Up9g6w" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Habeis jugado ya a la primera parte de Transport Fever? ¿Qué os parecen los juegos de este tipo? Déjanos tus impresiones sobre él en los comentarios, o charla sobre el juego en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

