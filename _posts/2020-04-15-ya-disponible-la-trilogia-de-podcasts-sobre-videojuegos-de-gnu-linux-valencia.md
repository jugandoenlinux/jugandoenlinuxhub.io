---
author: Jugando en Linux
category: Noticia
date: 2020-04-15 18:37:10
excerpt: "<p>Jugandoenlinux.com colabora con la asociaci\xF3n valenciana ofreciendo\
  \ informaci\xF3n sobre el mundo del videojuego en nuestro sistema favorito</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/GNULINUXpodcast.webp
joomla_id: 1209
joomla_url: ya-disponible-la-trilogia-de-podcasts-sobre-videojuegos-de-gnu-linux-valencia
layout: post
tags:
- podcast
- podcast-linux
title: "Ya disponible la trilog\xEDa de podcasts sobre videojuegos de GNU/Linux Valencia"
---
Jugandoenlinux.com colabora con la asociación valenciana ofreciendo información sobre el mundo del videojuego en nuestro sistema favorito


 


Jugandoenlinux.com y [GNU/Linux Valencia](https://gnulinuxvalencia.org/) se complacen en invitaros a escuchar la trilogía de podcast sobre videojuegos que se han grabado para amenizar nuestras ganas linuxeras de jugar en estos tiempos de confinamiento. En esta ocasión, Pato colabora en los podcast de la asociación valenciana aportando su particular visión del mundo de los videojuegos, tanto del software como del hardware bajo GNU/Linux.


A continuación os dejamos los enlaces a cada uno de los episodios:


Podcast 1: [JUEGOS EN GNU/LINUX, EN ESTOS TIEMPOS DE CONFINAMIENTO. UNA OFERTA CADA VEZ MAYOR Y MAS ATRACTIVA.](https://gnulinuxvalencia.org/podcast-juegos-en-gnu-linux-en-estos-tiempos-de-confinamiento-una-oferta-cada-vez-mayor-y-mas-atractiva/)


Podcast 2:[EL HARDWARE EN LOS VIDEOJUEGOS. SU EVOLUCIÓN Y ESTADO ACTUAL EN GNU/LINUX. DESDE LOS PRIMEROS TIEMPOS, HASTA LA ACTUALIDAD.EL HARDWARE EN LOS VIDEOJUEGOS. SU EVOLUCIÓN Y ESTADO ACTUAL EN GNU/LINUX. DESDE LOS PRIMEROS TIEMPOS, HASTA LA ACTUALIDAD.](https://gnulinuxvalencia.org/podcast-el-hardware-en-los-videojuegos-su-evolucion-y-estado-actual-en-gnu-linux-desde-los-primeros-tiempos-hasta-la-actualidad/)


Podcast 3: [LOS MANDOS EN LOS VIDEOJUEGOS Y SU COMPATIBILIDAD CON GNU/LINUX. SU HISTORIA Y EVOLUCIÓN A LO LARGO DEL TIEMPO.](https://gnulinuxvalencia.org/podcast-los-mandos-en-los-videojuegos-y-su-compatibilidad-con-gnu-linux-su-historia-y-evolucion-a-lo-largo-del-tiempo/)


Esperamos que os gusten y los disfrutéis y nos contéis que os ha parecido en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

