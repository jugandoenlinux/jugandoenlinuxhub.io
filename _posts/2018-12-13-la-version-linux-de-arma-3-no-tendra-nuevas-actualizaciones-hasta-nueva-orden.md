---
author: Pato
category: "Acci\xF3n"
date: 2018-12-13 16:49:00
excerpt: "<p>Con la \xFAltima actualizaci\xF3n el estudio @bohemiainteract anuncia\
  \ un alto en el camino para nuestro sistema</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5ff2da698b3b76e8cdb9d0576e73d737.webp
joomla_id: 931
joomla_url: la-version-linux-de-arma-3-no-tendra-nuevas-actualizaciones-hasta-nueva-orden
layout: post
title: "La versi\xF3n Linux de ArmA 3 no tendr\xE1 nuevas actualizaciones hasta nueva\
  \ orden"
---
Con la última actualización el estudio @bohemiainteract anuncia un alto en el camino para nuestro sistema

Malas noticias para los que disfrutan del simulador bélico de Bohemia Interactive 'ArmA 3'. Como sabéis, ArmA 3 ofrece soporte en Linux de manera "experimental" a través de Virtual Programming, la compañía de ports a otros sistemas que en este caso se encargan de actualizar el juego para que sea compatible con Linux.


En su último comunicado, Bohemia Interactive anuncia nuevas actualizaciones y características, pero dichas mejoras no llegarán a la versión Linux, al menos de momento. Según explican, la situación de la versión Linux es que ésta queda en alto hasta nueva orden:


*"La situación es que nos gustaría mucho que Arma 3 esté disponible en esas plataformas, y hemos estado colaborando con nuestro socio comercial Virtual Programming para hacer que esto sea una posibilidad (siendo la actualización 1.82  la más reciente). Sin embargo, cada una de estas actualizaciones experimentales son bastante exigentes en términos de preparación y prueba. Además, el trabajo solo puede comenzar después de que la versión oficial de Arma 3 para Windows se haya implementado con éxito para el público. Además de eso, las actualizaciones del port son simplemente bastante caras. Por lo tanto, como nuestro tiempo y otros recursos son limitados, debemos considerar el aspecto comercial de las cosas. Con eso en mente, **actualmente no estamos planeando una nueva actualización del port\***. Sin embargo, **es algo que podríamos considerar nuevamente en el futuro\***, cuando se esperan menos actualizaciones para Arma 3. No obstante, a pesar de que siempre hemos tenido cuidado de comunicar los ports como experimentales, nos damos cuenta de que algunos de ustedes podrían estar decepcionados y apreciamos mucho su comprensión. Por ahora, en caso de que quiera jugar con amigos que ejecutan la versión principal de Arma 3 para Windows, nos aseguraremos de que haya una rama heredada de Arma 3 versión 1.82 disponible para ellos. Por favor, visite la página web de los ports experimentales para más información."* 


*\*(énfasis nuestro)*


Así pues, si juegas a ArmA 3 en Linux, la versión queda congelada en la 1.82 a la espera de que Bohemia Interactive decida volver a actualizar nuestra versión, con lo que si quieres jugar con amigos bajo Windows estos tendrán que "bajar" su versión a la misma que la tuya.


Si quieres saber mas, puedes visitar el [post de actualización](https://dev.arma3.com/post/sitrep-00230) donde dan todos los detalles (en inglés), o la [página del port experimental](https://dev.arma3.com/ports), donde encontrarás un apartado con preguntas y respuestas.


Te dejamos el último trailer de ArmA 3 "Warlords".


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/yACvGPlHejU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


¿Qué te parece la decisión de Bohemia Interactive? ¿Juegas con tus amigos a ArmA 3 en Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [discord](https://discord.gg/ftcmBjD).


 

