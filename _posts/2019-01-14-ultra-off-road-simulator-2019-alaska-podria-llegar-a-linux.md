---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-01-14 14:34:38
excerpt: "<p>El juego desembarcar\xE1 en nuestro sistema si sus desarroladores <span\
  \ class=\"nickname\">@IceTorch</span> ven que hay demanda.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/81d7959d6ac82061e9b059ba05e22fc8.webp
joomla_id: 949
joomla_url: ultra-off-road-simulator-2019-alaska-podria-llegar-a-linux
layout: post
tags:
- steam
- ultra-offroad-simulator-2019-alaska
- icetorch-interactive
title: "\"Ultra Off-Road Simulator 2019: Alaska\" podr\xEDa llegar a Linux."
---
El juego desembarcará en nuestro sistema si sus desarroladores @IceTorch ven que hay demanda.

Una vez más, gracias a [GamingOnLinux](https://www.gamingonlinux.com/articles/ultra-off-road-simulator-2019-alaska-will-be-supported-on-linux-with-enough-requests.13342) nos enteramos que la polaca [IceTorch Interactive](https://www.icetorchinteractive.com/#) podría dar soporte para Linux para su próximo juego, **Ultra Off-Road Simulator 2019: Alaska**, que saldrá a la venta el próximo 1 de Febrero. Según acabamos de leer en uno de los hilos de los foros de Steam de este título, la compañía asegura que si hay suficiente demanda realizarán la versión de Linux a la vez que desarrollan la de MacOS. concretamente [han dicho esto:](https://steamcommunity.com/app/957050/discussions/0/3300476731143271212/#c3300476731144477381)



> 
> *"En realidad, no hay problema con eso. De todas formas estamos haciendo una versión para Mac, que con Linux son sistemas operativos UNIX. Por lo tanto, crear un port de Linux es bastante sencillo. Si un número suficiente de usuarios de Linux viene y solicita la versión de Linux - obtendrá la versión de Linux :D"*
> 
> 
> 


Más tarde han añadido algo sorprendente y que daja muy buen sabor de boca al leerlo (Traducción Online):



> 
> *"A continuación, para profundizar más en el tema:*  
> *Como dije en los foros: sí, no descartamos el puerto de Linux. **En realidad - Ultra Off-Road está SIENDO HECHO en Linux**. Nuestro programador principal es un gran evangelista de Linux y hace todo el código en su máquina pingüinera. Tanto Linux como iOS son sistemas operativos UNIX. Y como tenemos la intención de ir con nuestro juego en el escaparate de Apple, tener una versión del juego compatible con UNIX siempre estaba en nuestra lista de cosas por hacer. Heck - es por eso que **descartamos todas las liberaciones de DirectX y sólo usamos OpenGL**. Dicho esto -portar a Linux, cuando es bastante fácil- no es sólo un'clic de un botón'. Todavía necesita un poco de trabajo. Por lo tanto, algún día.*
> 
> 
> *Así que lo que **puedo prometer, es que Ultra Off-Road WILL soportará Linux - dalo por sentado**. Pero está en sus manos - qué tan rápido y qué tan pronto. Defina su prioridad. Como se me ha citado: spam el infierno vivo fuera de nuestro foro de juego exigiendo el puerto de Linux - y usted lo conseguirá, **tal vez incluso en el mismo día de lanzamiento**.*
> 
> 
> *Esto no es un truco de marketing o de relaciones públicas. Son matemáticas sencillas: hacemos lo que nuestros jugadores quieren. Si nuestros jugadores quieren un puerto Linux, lo obtendrán. Después de todo, no hacemos juegos para nosotros mismos. Lo hacemos por usted."*
> 
> 
> 


El juego, según su descripción de Steam contará con las siguientes características:


~❄️ Características ?~
----------------------


☑ Mundo Abierto  
☑ Terreno deformable  
☑ Ciclo Día y Noche  
☑ Clima dinámico  
☑ Física realista  
☑ Autos únicos, completamente equipados y destruibles  
![](https://steamcdn-a.akamaihd.net/steam/apps/957050/extras/Ultra_Off-Road_Simulator_2019_Alaska_-_Sep3.gif)


~❄️ Rápido y Sucio ?~
---------------------


Ultra Off-Road Simulator 2019: Alaska' (o simplemente'Ultra Off-Road', para abreviar :P) es un simulador realista de una experiencia off-road de la vida real y la primera entrega de una próxima serie. Maneje a través de uno de los ambientes más extremos del mundo - la salvaje y fría Alaska.


~❄️ Simulador Realista ?~
-------------------------


No hay caminos, no hay senderos - sólo las huellas de los que se aventuraron antes que tú. Los coches se averían, el combustible es limitado y sus suministros, escasos. Perderse es normal y hay que andar con cuidado, pero rápidamente. La noche limita su visión y sentido de la orientación. Los coches se atascan en el barro si te apresuras sin pensar, lo que hace que tengas que usar el cabrestante de tu coche para salir. Cruzar ríos y estanques es una apuesta que no se puede ganar. Lluvia, nieve, niebla y ventisca - aumenta aún más la dificultad. ¿Debería conducir en línea recta a través de un terreno incierto y arriesgar su coche, o dar la vuelta y quemar más combustible y suministros?  
Todo depende de ti, de tus habilidades y agallas.


![](https://steamcdn-a.akamaihd.net/steam/apps/957050/extras/Ultra_Off-Road_Simulator_2019_Alaska_-_Sep2.gif)


~❄️ Verdadero OFF-ROAD ?~
-------------------------


**Sin reglas, sin barreras, sin "zonas seguras". Sólo usted, su coche y el mapa - contra las probabilidades de la salvaje e inexplorada Alaska. El desafío final y la prueba final. El hombre contra la naturaleza, en toda su gloria y fuerza implacable.  
¿Estás listo para el viaje...? =)**


Como siempre os dejamos con un video del juego, en este caso el trailer para que veais el juego en acción:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/eEBLlQTtD0Q" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Bueno... pues ya sabeis, si os interesa ver el juego en nuestra plataforma, a [dejar constancia de ello en el post](https://steamcommunity.com/app/957050/discussions/0/3300476731143271212/) para que los creadores de este interesante título vean que no somos cuatro gatos y se animen a traernos su juego a nuestras queridisimas distribuciones favoritas.
