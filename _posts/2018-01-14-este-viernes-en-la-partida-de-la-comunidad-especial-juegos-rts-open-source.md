---
author: Serjor
category: Web
date: 2018-01-14 15:11:43
excerpt: <p>No tener el juego ya no es una excusa para no unirse</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/59d925d75340d64d31d5ce71bf6ceaf4.webp
joomla_id: 605
joomla_url: este-viernes-en-la-partida-de-la-comunidad-especial-juegos-rts-open-source
layout: post
tags:
- rts
- estrategia
- open-source
- comunidad
- lutris
- partidas-con-la-comunidad
title: 'Este viernes en la partida de la comunidad: Especial juegos RTS Open Source'
---
No tener el juego ya no es una excusa para no unirse

Vuelven una semana más las partidas con la comunidad y lo hacen con un clásico de los viernes en JugandoEnLinux.com, y es que en esta ocasión la estrategia vuelve para no dejar descansar nuestras neuronas.


La votación, que se está llevando a cabo en el canal de [Telegram](https://telegram.me/jugandoenlinux), es la siguiente:







Y si os habéis fijado, todos los juegos son open source, así que muy probablemente podréis haceros con ellos desde los repos de vuestra distribución favorita, sino os recordamos que [Lutris](index.php/homepage/editorial/item/559-lutris-un-gestor-de-videojuegos-open-source) está ahí para echaros una mano.


Como siempre recordaros que la partida será el viernes a las 22:30 (UTC+1), y que os recomendamos que os conectéis a la sala de [Discord](https://discord.gg/ftcmBjD) para poder coordinaros más fácilmente.

