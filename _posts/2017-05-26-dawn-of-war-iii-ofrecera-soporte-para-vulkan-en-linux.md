---
author: Pato
category: Estrategia
date: 2017-05-26 09:56:43
excerpt: "<p>As\xED lo ha anunciado Feral Interactive, aunque la opci\xF3n por defecto\
  \ ser\xE1 OpenGL</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ae490490adff4f695d8831b6d20b97cf.webp
joomla_id: 343
joomla_url: dawn-of-war-iii-ofrecera-soporte-para-vulkan-en-linux
layout: post
tags:
- feral-interactive
- vulkan
- dawn-of-war
title: "'Dawn Of War III' ofrecer\xE1 soporte para Vulkan en Linux"
---
Así lo ha anunciado Feral Interactive, aunque la opción por defecto será OpenGL

Acaba de saltar la noticia de que el próximo lanzamiento de Feral Interactive 'Warhammer 40.000: Dawn of War III' tendrá la opción de ejecutarse bajo Vulkan para una experiencia superior:



> 
> Hammer of steel and fire: Dawn of War III for macOS and Linux forged with next-generation graphics technology. News: <https://t.co/8mWp3Q5kNk> [pic.twitter.com/gYQHc5J6MY](https://t.co/gYQHc5J6MY)
> 
> 
> — Feral Interactive (@feralgames) [May 26, 2017](https://twitter.com/feralgames/status/868044376487997440)



 ![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/banners/DOWIII_MetalVulkan_260x163_Main.webp)


Así, tal y como puede leerse en el [post del anuncio oficial](https://www.feralinteractive.com/es/news/766/) el juego se ejecutará por defecto en OpenGL, pero habrá una opción en la ventana de opciones de lanzamiento de Feral donde se podrá elegir ejecutar el juego con la nueva API.


Como ya os contamos en el [anuncio](index.php/homepage/generos/estrategia/item/453-warhammer-4000-dawn-of-war-iii-tendra-version-para-linux) aquí en Jugando en Linux 'Dawn of War' llegará a Linux el próximo día 8 de Junio.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/xsqXBje0n44" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


¿Qué te parece poder ejecutar el juego con Vulkan? ¿Piensas probarlo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

