---
author: P_Vader
category: Noticia
date: 2021-12-14 23:51:25
excerpt: "<p>Todo ello para el desarrollo de su servicio en la nube Luna, dedicada\
  \ al videojuego en streaming.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Luna/luna_linux.webp
joomla_id: 1395
joomla_url: amazon-esta-contratando-ingieneros-con-conocimientos-en-linux-proton-o-dxvk
layout: post
tags:
- wine
- dxvk
- proton
- luna
- amazon
title: "Amazon est\xE1 contratando ingieneros con conocimientos en Linux, Proton o\
  \ DXVK"
---
Todo ello para el desarrollo de su servicio en la nube Luna, dedicada al videojuego en streaming.


Este año un grande como Valve nos dió la campanada apostando por Linux y Proton como base para su futura plataforma de videojuegos, pero no vamos a despedir el año sin otro campanazo, si no igual o mas sonoro aún, y es que via [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Amazon-Linux-Graphics-Jobs) hemso sabido que **Amazon ha publicado anuncios de trabajo buscando ingenieros informáticos con conocimientos en videojuegos Linux, Proton, DXVK, Vulkan, OpenGL y un largo etcétera.**


Todo ello enfocado a su servicio de video juegos en la nube [Luna](https://www.amazon.com/luna/). En un principio los juegos en Luna iban a ejecutarse bajo instancias Windows. No nos queda claro si este cambio de parecer será para ofrecer a los desarrolladores de videojuegos diferentes entornos de ejecución o simplemente se trata de una transición de Windows a Linux.


Entre los detalles que podemos extraer de las ofertas es que "***Luna se compromete a trabajar con la comunidad de código abierto de Proton.* Este puesto se encargará de aportar código a proyectos de código abierto como Proton y Wine en busca de la ejecución de juegos de forma estable y con un buen rendimiento."**, queda claro que Amazon va a colaborar conjuntamente con  Proton y Wine. Este detalle es una noticia brutal para el proyecto, que un gigante de semejante envergadura se una en el desarrollo puede ser el empujón que lo consolide como una opción mas valida aun si cabe para el juego en Linux.


 


![luna linux oferta](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Luna/luna_linux_oferta.webp)


 


Las **consecuencias de esta inversión claramente será en una mejora en nuestros drivers gráficos, Proton y DXVK**, y como comentaba al principio el impulso para que Proton se convierta en una plataforma de referencia a la hora de desarrollar videojuegos muy seria. **Para los Linuxeros está claro que será una mayor compatibilidad de videojuegos y mayor rendimiento**.


¿Qué os parece el futuro cada vez mas prometedor de Proton para el juego en Linux? ¿Aun no te convence? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)


 

