---
author: Serjor
category: Noticia
date: 2018-10-01 18:04:08
excerpt: "<p>Como en las anteriores ocasiones, Feral Interactive ser\xE1 el encargado\
  \ del port</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f8ea1c7aff521bedaac5eab4cbe3ce1e.webp
joomla_id: 864
joomla_url: life-is-strange-2-tendra-version-nativa-para-gnu-linux
layout: post
tags:
- feral-interactive
- square-enix
- life-is-strange
- life-is-strange-2
title: "Life Is Strange 2 tendr\xE1 versi\xF3n nativa para GNU/Linux"
---
Como en las anteriores ocasiones, Feral Interactive será el encargado del port

Seamos sinceros, se veía venir, y es que Feral Interactive nos han anunciado oficialmente lo que era un secreto a voces, y es que traerán de forma nativa a GNU/Linux Life Is Strange 2, el cuál ha sido estrenado para el resto de plataformas esta misma semana pasada.


Según comentan en su nota de prensa el juego será lanzado durante el próximo 2019, aunque no indican si de manera episódica o será el juego completo como ya pasó con la primera parte.



> 
> En esta ocasión Life Is Strange 2 nos cuenta la historia de los hermanos Sean y Daniel Diaz, quienes después de un trágico y misterioso accidente huyen de su casa hacia Ciudad de México. Durante el duro viaje Sean, el hermano mayor, se da cuenta de que tiene una responsabilidad con su hermano ya que las decisiones que toma afectará a sus vidas para siempre.
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/MSB1yCANBAE" width="560"></iframe></div>


Y tú, ¿tienes ganas de jugar a este Life Is Strange 2? Cuéntanoslos en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

