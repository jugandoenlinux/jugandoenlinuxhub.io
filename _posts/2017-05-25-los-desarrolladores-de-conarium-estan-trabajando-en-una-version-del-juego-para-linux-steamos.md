---
author: Pato
category: Terror
date: 2017-05-25 07:30:52
excerpt: <p>Se trata de una aventura de terror inspirada en el universo de H.P. Lovecraft</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1a9baee2108527eeffc063ebca11f242.webp
joomla_id: 340
joomla_url: los-desarrolladores-de-conarium-estan-trabajando-en-una-version-del-juego-para-linux-steamos
layout: post
tags:
- indie
- aventura
- terror
- hp-lovecraft
title: "Los desarrolladores de 'Conarium' est\xE1n trabajando en una versi\xF3n del\
  \ juego para Linux/SteamOS"
---
Se trata de una aventura de terror inspirada en el universo de H.P. Lovecraft

En el habitual repaso de juegos que hago a diario, me he topado con 'Conarium' [[web oficial](http://www.conariumgame.com/)] un interesante desarrollo del estudio indie "Zoetrope Interactive" sobre una aventura de terror inspirada en la novela del conocido autor H.P. Lovecraft "En las montañas de la locura". Como es habitual en desarrollos interesantes, la pregunta de rigor es: ¿llegará a Linux?


Pues según [ha confirmado el desarrollador](http://steamcommunity.com/app/313780/discussions/0/1319962514590534729/) en el foro del juego en Steam, están trabajando en una versión para nuestro sistema favorito, y aunque [probablemente no esté para la salida](http://steamcommunity.com/app/313780/discussions/0/1319962514590534729/#c1291817208486023945) si tienen intención de publicarla más adelante lo que no dejan de ser buenas noticias.


#### Sobre 'Conarium':



> 
> *Conarium es un espeluznante juego de terror cósmico en el que descubrirás una trama apasionante en la que participan cuatro científicos que tratan de desafiar a lo que normalmente consideramos los límites «absolutos» de la naturaleza. La historia, inspirada en la novela de H.P. Lovecraft «En las montañas de la locura», narra, en su mayor parte, los acontecimientos posteriores al relato original.*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/LNUzrWv50EI" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


####  Características:


*Una historia intensa de terror cósmico llena de suspense, secretos y huevos de Pascua.*   
*Gráficos siniestros a la par que maravillosos creados con el motor Unreal 4.*  
*Diversos finales.*  
*Una banda sonora inquietante y adaptada al ambiente.*


'Conarium' llegará a Windows el próximo 6 de Junio, pero en Jugando en Linux estaremos atentos al desarrollo del juego para informaros de su lanzamiento en Linux/SteamOS.


¿Qué te parece este 'Conarioum'? ¿Te gustan los juegos con temática de terror "lovecraftiano"?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

