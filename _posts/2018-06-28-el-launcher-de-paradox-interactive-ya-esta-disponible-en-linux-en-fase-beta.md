---
author: Pato
category: Software
date: 2018-06-28 07:12:39
excerpt: "<p>De momento solo podemos lanzar 'Stellaris', pero se a\xF1adir\xE1n m\xE1\
  s juegos en un futuro pr\xF3ximo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/da3dd9e0dfc073a9278eecf902a909a6.webp
joomla_id: 787
joomla_url: el-launcher-de-paradox-interactive-ya-esta-disponible-en-linux-en-fase-beta
layout: post
tags:
- software
- paradox-interactive
- lanzador
title: "El launcher de Paradox Interactive ya est\xE1 disponible en Linux en fase\
  \ beta"
---
De momento solo podemos lanzar 'Stellaris', pero se añadirán más juegos en un futuro próximo

Tenemos un nuevo launcher en la ciudad. Gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/the-paradox-launcher-is-now-available-on-linux.12041) recibimos la noticia de que Paradox Interactive ha publicado su "lanzador" de videojuegos para Linux, adelantándose a otros esperados lanzadores, como el GOG Galaxy, el launcher de la tienda de videojuegos propiedad de CD Projeckt que no termina de llegar.


El anuncio lo hizo uno de los responsables de la plataforma [en uno de sus foros](https://forum.paradoxplaza.com/forum/index.php?threads/paradox-store-launcher-updates-and-changelogs.1076735/page-4#post-24416751) sobre las novedades del lanzador de Paradox:





> *"Tengo el placer de traeros las novedades que tras mucha anticipación [...] **la versión Linux de PDX Launcher está ya disponible para su descarga**. En este momento solo Stellaris está disponible para su descarga e instalación, pero el resto de nuestros juegos con soporte en Linux vendrán mas adelante (de una forma mas expeditiva que el mismo launcher ¡afortunadamente!)*



> *[...] Tanto como el lanzador está en calidad beta,la versión Linux ha tenido incluso menos testeo. Sin embargo anticipo que las personas interesadas en la versión Linux tendrán la capacidad de encontrar soluciones para los bugs no críticos.*



> *Como es usual, tenemos alguna salsa secreta en la cocina (esta es la industria de los juegos después de todo) pero todavía estamos tratado de obtener algunos parches [...] durante el otroño."*


Personalmente he estado echándole un vistazo al lanzador y aún está en un estado muy básico pero parece que algo se está moviendo cuando una editora como Paradox apuesta por Linux, aunque sea inicialmente. Esperemos que otras editoras y tiendas sigan el camino de Paradox y tengamos mas presencia en la industria del videojuego.

Si tienes curiosidad, puedes descargar el lanzador PDX [desde este enlace](http://launcher.paradoxplaza.com/linux_launcher).



¿Qué te parece el anuncio de Paradox? ¿Piensas probar el lanzador?
 

Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).



