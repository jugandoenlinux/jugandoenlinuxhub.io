---
author: Pato
category: "Acci\xF3n"
date: 2018-01-31 19:05:52
excerpt: "<p>A las reiteradas preguntas sobre el soporte en Linux, uno de los responsables\
  \ del t\xEDtulo ha aclarado Linux es solo para los servidores, no para el juego\
  \ en si.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2e79051f535b06583dfeaa47b4b5b74c.webp
joomla_id: 628
joomla_url: battalion-1944-no-llega-manana-en-acceso-anticipado-con-soporte-en-gnu-linux-steamos
layout: post
tags:
- accion
- acceso-anticipado
- fps
- mmo
- ii-guerra-mundial
title: "'Battalion 1944' (NO) llega ma\xF1ana en acceso anticipado con soporte en\
  \ GNU-Linux/SteamOS"
---
A las reiteradas preguntas sobre el soporte en Linux, uno de los responsables del título ha aclarado Linux es solo para los servidores, no para el juego en si.

**Actualización:** Al parecer y tal y como ha aclarado uno de los responsables del juego [en twitter](https://twitter.com/James_Tatum/status/959012458609496065) rompiendo el silencio del estudio(por fin!) a las reiteradas preguntas sobre el soporte en nuestro sistema,  el juego no estará disponible como tal en Linux, a pesar de los reportes de SteamDB, el icono de SteamOS en el juego y el apartado de requisitos para Linux en la página de Steam. todo parece indicar que el estudio ha puesto por error el icono de Linux/SteamOS y los requisitos, ya que tan solo hay soporte para los servidores del juego en Linux. Una lástima pero esto es lo que hay. Sentimos la confusión.




---


De nuevo toca hablar del esperado 'Battalion 1944', y es que como ya [anunciamos en su momento](index.php/homepage/generos/accion/item/723-battalion-1944-estrenara-acceso-anticipado-el-proximo-1-de-febrero) el juego llegará mañana mismo a nuestros sistemas para disfrute de todos los que echan de menos la acción FPS de los títulos bélicos de antaño con un remozado aspecto visual.


En un [extenso anuncio](http://steamcommunity.com/games/489940/announcements/detail/1680275500344771661) los chicos de Bulkhead Interactive nos explican lo que podemos esperar de su desarrollo:


* Pack de actualización "First to Fight": Para todo aquel que quiera apoyar (aún más) al desarrollo del juego podrá hacerse con este pack que contendrá aparte del juego, diferentes skins y mejoras visuales así como la banda sonora del juego al completo a un precio de 24,99$.
* Descuento de lanzamiento: Durante la fase de acceso anticipado podremos hacernos con el juego al módico precio de 14,99$ (10% de descuento)
* Matchmaking y ranking competitivo: Será lanzado el día 8 de febrero, por lo que los que adquieran el juego tendrán una semana de práctica para practicar en servidores arcade o sin ranking.
* Torneos y eventos en la primera semana: Estarán disponibles sin recompensas a modo de pruebas hosteados por la comunidad.
* Blitzkrieg Battle (EU): Evento hosteado por la comunidad y que será retransmitido por Twitch.


Además de esto, el estudio está en conversaciones con la ESL con el fin de incluirlo en un futuro cercano como e-sport. Como veis, el juego está muy enfocado a la escena competitiva. De hecho incluso se podrá [contratar un servidor dedicado](https://www.multiplaygameservers.com/game-servers/battalion/) si se quiere jugar de forma más "profesional".


Por otra parte, también anuncian la disponibilidad de montar un servidor propio bajo Linux, que aunque aún parece estar un poco en pañales en principio debería ser operativo mañana mismo. Podéis ver el modo de crear el servidor en [este enlace](http://35.189.104.46/Community_Servers). También hay disponible una wiki para el modo servidor que [podéis ver aquí](https://steamcommunity.com/linkfilter/?url=http://wiki.battaliongame.com).


Para terminar, os dejo un par de vídeos de armas en acción:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/o4m6sTdelDA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/qV2X_tGNwvk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>



 



¿Qué te parece 'Battalion 1944' y su enfoque competitivo? ¿Te gustaría montar un servidor en Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

