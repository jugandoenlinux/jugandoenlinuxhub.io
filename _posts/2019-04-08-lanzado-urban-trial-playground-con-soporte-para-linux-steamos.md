---
author: leillo1975
category: "Acci\xF3n"
date: 2019-04-08 13:20:16
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\">El juego, de @TateMultimedia , est\xE1\
  \ disponible en nuestro sistema desde el primer d\xEDa</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9f69834c2b52286f136e08fabd62f78b.webp
joomla_id: 1015
joomla_url: lanzado-urban-trial-playground-con-soporte-para-linux-steamos
layout: post
tags:
- dia-1
- unreal-engine-4
- motos
- urban-trial-playground
- tate-multimedia
title: Lanzado "Urban Trial Playground" con soporte para Linux/SteamOS
---
El juego, de @TateMultimedia , está disponible en nuestro sistema desde el primer día

Otra vez, gracias a [GamingOnLinux](https://www.gamingonlinux.com/articles/stunt-bike-game-urban-trial-playground-released-with-same-day-linux-support.13914), nos enteramos de la salida de "[**Urban Trial Playground**](http://urbantrialplayground.com/en/)". Recientemente la compañía polaca [Tate Multimedia](https://www.tatemultimedia.com/en/home/) ha lanzado este también juego de motos, presumiblemente reutilizando gran parte del trabajo realizado en [Steel Rats](http://steelratsgame.com), su anterior trabajo con el que consiguieron muy buenas críticas. En este caso la temática varía considerablemente, pasando de ser un arcade motorizado, a un juego donde tendremos que aplicarnos en conseguir hacer las cabriolas más locas con nuestra moto.


El juego está desarrolado con el motor **Unreal Engine** y según lo que podemos ver en la **descripción oficial** vamos a encontrar:


*![](/https://steamcdn-a.akamaihd.net/steam/apps/988240/extras/gif_small_1.webp)*



*Los niveles de Urban Trial Playground están diseñados para que ejecutes trucos y acrobacias, para que combines tus mejores movimientos y consigas valiosísimos combos.*  
  
*![](/https://steamcdn-a.akamaihd.net/steam/apps/988240/extras/gif_small_2.webp)*  
*Urban Trial Playground ha sido creado para proporcionar incontables horas de acción para los fans de la adrenalina y los locos del freestyle. ¡Combina trucos en combos únicos y virtualmente infinitos para propulsar tu puntuación y ascender a lo más alto de los marcadores!*  
  
*![](/https://steamcdn-a.akamaihd.net/steam/apps/988240/extras/gif_small_3.webp)*  
*Los modos Freestyle & Contrarreloj incluirán clasificaciones online\* en los más de 50 niveles. También puedes desafiar a tu fantasma en el modo Contrarreloj: observa tus mejores carreras y trata de superarte perfeccionando otras rutas más rápidas o ejecutando trucos aún más espectaculares.*  
  
*![](/https://steamcdn-a.akamaihd.net/steam/apps/988240/extras/gif_small_4.webp)*  
*Urban Trial Playground presenta seis motos personalizables que no solo podrás mejorar en cuanto a aspecto, sino también en cuanto a motor, frenos y otras partes fundamentales que afectarán a los controles de la moto. ¡Con todas estas opciones para personalizar tu moto y tu piloto, podrás crear tu propio estilo y mejorar tus récords al más puro estilo californiano!*  
  
  
*Ejecuta mortales desde tejados, palmeras y dunas de arena en Urban Trial Playground ¡y conviértete en el piloto acrobático definitivo!*  
  
*Esta nueva entrega de la serie de carreras de motos acrobáticas traslada la acción desde las calles de la ciudad hasta las soleadas y coloridas playas de California combinando los trucos, giros y combos más espectaculares hasta la fecha. ¡Es un juego de velocidad, equilibrio y estilo!*  
  
*¡Controlar el freno delantero y el trasero de forma independiente permite a los jugadores realizar invertidos y mortales hacia adelante además de derrapar y quemar rueda en la pista!*  
  
*Unos fondos vibrantes y bañados por el sol y una atmósfera desenfadada son el trasfondo para unas acrobacias realmente sobrecogedoras. Prepárate para hacer giros, trucos y piruetas sobre palmeras y dunas de arena dorada.*
 
El juego se encuentra traducido a nuestro idioma y tiene unos **requisitos mínimos de funcionamiento muy bajos** (**Procesador:** Intel Core 2 Duo E8400, **Memoria:** 4 GB de RAM, **Gráficos:** GeForce GT 630 compatible con Vulkan). Además de todo esto, debes saber que el juego a parte de tener un **precio realmente bajo**, en este momento está con una oferta de lanzamiento del **30% de descuento**, por lo que puedes hacerte con él por **menos de 5€** (4.89€) en [Steam](https://store.steampowered.com/app/988240/Urban_Trial_Playground/). Os dejamos con el trailer oficial del juego:<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/5d3OHsiCOFQ" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Qué os parece la nueva propuesta de Tate Multimedia? Por ese precio bien vale la pena hacer unos cuantos trucos en moto, ¿no? Dinos que te parece "Urban Trial Playground" en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).


