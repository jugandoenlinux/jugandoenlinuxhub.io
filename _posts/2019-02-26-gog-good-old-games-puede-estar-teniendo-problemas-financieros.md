---
author: Pato
category: Noticia
date: 2019-02-26 17:15:26
excerpt: "<p>La popular tienda \"sin DRM\" habr\xEDa despedido a una docena de empleados</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c435ffd4918b356927e6ff03a359e0db.webp
joomla_id: 982
joomla_url: gog-good-old-games-puede-estar-teniendo-problemas-financieros
layout: post
tags:
- gog
- gogcom
title: GOG (Good Old Games) puede estar teniendo problemas financieros
---
La popular tienda "sin DRM" habría despedido a una docena de empleados

Malas noticias para los que valoramos la posibilidad de comprar juegos sin DRM. Según cuentan en [kotaku.com](https://kotaku.com/facing-financial-pressures-gog-quietly-lays-off-at-lea-1832879826) la tienda de videojuegos online [gog.com](https://www.gog.com/) conocida por su política de no implementar ningún tipo de DRM en los títulos que publica parece estar en problemas. Según la publicación, **GOG habría despedido a una docena de empleados lo que supondría alrededor del 10% de la plantilla** en lo que sería una "reestructuración" de la compañía filial de CDProjekt, Pero a tenor de las declaraciones de uno de los despedidos al parecer la tienda viene teniendo dificultades financieras, al menos desde hace un par de meses.


Según las declaraciones, **"ha sido una decisión financiera"** ya que al parecer la tienda estaría cerca de los números rojos desde hace algunos meses. "*El mes de febrero ha sido un mal mes, pero por otra parte Enero fue excelente. Sin embargo **el movimiento del mercado hacia una mayor participación de los ingresos de los [desarrolladores] ha afectado o afectará** **también**. Quiero decir, ha sido una situación extraña, las cosas han ido a peor realmente rápido. [...] Estábamos en medio de una reestructuración general sin precedentes, moviendo algunos equipos. Pero despidos tan grandes nunca habían ocurrido antes "." (*Énfasis nuestro)


Aunque posteriormente desde GOG han comunicado que todo se debe a una reestructuración de equipos, y que desde 2018 han contratado a cerca del doble de empleados y mantienen ofertas de trabajo para 20 puestos, no dejan de ser significativas las declaraciones.


Obviamente, la referencia al reparto de ingresos a los desarrolladores viene a tenor de la **guerra abierta que mantienen Steam y la Epic Game Store** en cuanto al reparto de beneficios con la intención de atraer a los desarrolladores, con Steam [rebajando márgenes](https://areclosed.com/gaming/valve-announces-steam-revenue-cut-42642844) y cobrando entre un 30% y un 20% según tramos de ingresos y la Epic Store cobrando entre un 12% y un 18%. **GOG por su parte, mantiene su cuota del 30%** que viene siendo el estandar de la industria desde hace al menos 10 años. Si esto acaba afectando a otras tiendas como a GOG en este caso estamos a las puertas de un cambio en la industria del videojuego en PC.


Si además sumamos los **crecientes rumores sobre servicios de videojuegos vía streaming** por parte de compañías como [Google](https://www.muycomputer.com/2019/02/20/project-stream/), [Amazon](https://www.muycomputer.com/2019/01/11/streaming-de-juegos-de-amazon/) o [Microsoft](https://www.muycomputer.com/2018/10/09/project-xcloud/) donde ya no importará el hardware o el sistema que se ejecute por debajo, puede explicar las dificultades que empiezan a aflorar en estas tiendas. En cualquier caso, el que GOG esté en dificultades son malas noticias sin paliativos, puesto que es la tienda que mejores políticas tiene de cara al usuario, aunque esto les pone en serias dificultades para atraer a grandes desarrollos y estudios aparte de la propia CDProjekt Red.


¿Que piensas sobre las dificultades de GOG.com? ¿Crees que su política de "DRM Free" es acertada?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux)

