---
author: leillo1975
category: Terror
date: 2017-12-22 15:32:15
excerpt: "<p>D\xE1ndome un paseo por <a href=\"https://www.gamingonlinux.com/articles/first-person-sci-fi-game-tartarus-is-now-available-on-linux.10939\"\
  \ target=\"_blank\" rel=\"noopener\">GamingOnLinux</a> he encontrado la noticia\
  \ de este juego que ha capturado mi atenci\xF3n. Se trata de <a href=\"http://tartarusthegame.com/\"\
  \ target=\"_blank\" rel=\"noopener\">Tartarus</a>, un juego de terror en primera\
  \ persona desarrollado por compa\xF1\xEDa Turca <a href=\"http://abyssgameworks.com/\"\
  \ target=\"_blank\" rel=\"noopener\">Abyss Gameworks</a>. El juego tiene una calidad\
  \ gr\xE1fica notable y por lo que se puede adivinar en su trailer (al final del\
  \ art\xEDculo) resulta similar a juegos como Amnesia, Layers of Fear o incluso Alien\
  \ Isolation (ambientaci\xF3n). El t\xEDtulo ha sido lanzado ayer mismo (21-12-17)\
  \ para nuestro sistema y est\xE1 desarrollado ,entre otras herramientas, con Unreal\
  \ Engine.</p>\r\n<p>En el juego nos encotraremos encerrados en una nave minera,\
  \ la Tartarus, que orbita alrededor de Neptuno, acerc\xE1ndose cada vez m\xE1s a\
  \ la superficie este lejano planeta. La nave ha activado el protocolo de seguridad\
  \ y todas las puertas est\xE1n cerradas y los sistemas apagados. Nosotros somos\
  \ Cooper, el cocinero, y no tenemos ni idea de que ha pasado. Debemos buscar la\
  \ salida desbloqueando puertas y sistemas <strong>usando comandos</strong> en terminales\
  \ inform\xE1ticas y resolviendo puzzles. Aqu\xED os dejo el inquietante trailer\
  \ del juego:</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/sFxS3t0thms\"\
  \ width=\"800\" height=\"450\" style=\"display: block; margin-left: auto; margin-right:\
  \ auto;\" allowfullscreen=\"allowfullscreen\"></iframe></p>\r\n<p>&nbsp;</p>\r\n\
  <p>El juego se encuentra en este momento de oferta, al 60% de descuento. Si lo vais\
  \ a adquirir debeis saber que aun <strong>no se encuentra traducido al espa\xF1\
  ol</strong>. Podeis comprarlo en Steam:</p>\r\n<p><iframe src=\"https://store.steampowered.com/widget/608530/\"\
  \ width=\"646\" height=\"190\" style=\"display: block; margin-left: auto; margin-right:\
  \ auto;\"></iframe></p>\r\n<p>&nbsp;\xBFEstais dispuestos a salvar la \"Tartarus\"\
  ? \xBFQu\xE9 os parece este trabajo de Abyss Gameworks? Dejanos tus impresiones\
  \ en los comentarios o en nuestro grupo de <a href=\"https://t.me/jugandoenlinux\"\
  \ target=\"_blank\" rel=\"noopener\">Telegram</a>.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/326f0e6722d312575a00dfbe01ed99ed.webp
joomla_id: 584
joomla_url: tartarus-un-interesante-juego-de-ciencia-ficcion-disponible-en-linux-steamos
layout: post
tags:
- ciencia-ficcion
- terror
- unreal-engine
- tartarus
- abyss-gameworks
title: "Tartarus, un interesante juego de ciencia ficci\xF3n, disponible en Linux/SteamOS"
---
Dándome un paseo por [GamingOnLinux](https://www.gamingonlinux.com/articles/first-person-sci-fi-game-tartarus-is-now-available-on-linux.10939) he encontrado la noticia de este juego que ha capturado mi atención. Se trata de [Tartarus](http://tartarusthegame.com/), un juego de terror en primera persona desarrollado por compañía Turca [Abyss Gameworks](http://abyssgameworks.com/). El juego tiene una calidad gráfica notable y por lo que se puede adivinar en su trailer (al final del artículo) resulta similar a juegos como Amnesia, Layers of Fear o incluso Alien Isolation (ambientación). El título ha sido lanzado ayer mismo (21-12-17) para nuestro sistema y está desarrollado ,entre otras herramientas, con Unreal Engine.


En el juego nos encotraremos encerrados en una nave minera, la Tartarus, que orbita alrededor de Neptuno, acercándose cada vez más a la superficie este lejano planeta. La nave ha activado el protocolo de seguridad y todas las puertas están cerradas y los sistemas apagados. Nosotros somos Cooper, el cocinero, y no tenemos ni idea de que ha pasado. Debemos buscar la salida desbloqueando puertas y sistemas **usando comandos** en terminales informáticas y resolviendo puzzles. Aquí os dejo el inquietante trailer del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/sFxS3t0thms" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


El juego se encuentra en este momento de oferta, al 60% de descuento. Si lo vais a adquirir debeis saber que aun **no se encuentra traducido al español**. Podeis comprarlo en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/608530/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Estais dispuestos a salvar la "Tartarus"? ¿Qué os parece este trabajo de Abyss Gameworks? Dejanos tus impresiones en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

