---
author: Serjor
category: Noticia
date: 2019-04-13 16:06:10
excerpt: "<p>Qu\xE9 mejor forma de pasar estos d\xEDas, que echando unas partidas\
  \ con gente de la comunidad</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/81617031e9318e4f76b86b7f8109eaa5.webp
joomla_id: 1019
joomla_url: torneo-de-semana-santa
layout: post
tags:
- torneo
- semana-santa
- sonlink
title: Torneo de Semana Santa
---
Qué mejor forma de pasar estos días, que echando unas partidas con gente de la comunidad

Si de algo podemos estar totalmente orgullosos en jugandoenlinux.com es de nuestra comunidad, y SonLink, quién por ejemplo nos trajo el flatpak de [SpeeDreams](https://github.com/flathub/org.speed_dreams.SpeedDreams), nos trae una muestra más del porque, y es que nos propone participar en un torneo aprovechando el parón habitual por Semana Santa.


Si esta Semana Santa vas a tener un equipo con GNU/Linux cerca y te apetece pasar un rato entretenido, y quién sabe, quizás ganar el primer premio (que consiste en el reconocimiento público de tus cualidades), no lo dudes y apúntate.


Os dejamos con el mensaje en el foro que nos ha dejado SonLink:


Para esta semana santa se me ha ocurrido organizar uno, o un par, de torneo(s) aprovechando que la mayoría de la gente tendrá vacaciones. Esta es mi lista de juegos y los 2 más votados serán de los que se hagan el torneo.


Una vez votado deberás de escribir tus votos y confirmar que vas a jugar. Esto es así para evitar gente que vota y luego ni se presenta. Por favor, solo vota **SI** realmente vas a poder jugar.


En cuanto a los horarios sera del jueves 18 al domingo 21, a partir de las 18:00 horas española (UTC+2). La hora de alguno de ellos se puede ir acordando debido a las diferencias horarias.


Una vez terminada alguna ronda se deberá de subir al grupo de [Telegram](https://t.me/jugandoenlinux) o a [este hilo](index.php/foro/foro-general/148-torneos-semana-santa), para que se pueda ver el resultado final. De todos modos en juegos que admitan ver las partidas sin jugar alguno de los colaboradores trataría de entrar, incluso puede que transmitir la partida. Si vas a poder hacer esto último tú, deja el enlace a tu canal.


La selección de juegos son aquellos que son libres y/o gratuitos, para que así todo el mundo que quiera participar pueda descargarse los juegos en caso de no poder pagar algún juego en alguna de las plataformas disponibles (Steam, GoG, Humble Bundle, etc)


Normas
------


* En el caso de no poder participar en alguna ronda se debe de avisar con al menos media hora de antelación. En caso de no hacerlo, y dependiendo del juego o en que parte del torneo se este, se le podría, desde anular su participación en dicha ronda, hasta su expulsión.
* No estarán permitidos avatares y/o nicknames que inciten al odio, que resulten ofensivos, motivos políticos (especialmente debido a la cercanía de Elecciones Generales en España), etc, así como mensajes de estos tipos dentro del juego, independientemente del método usado.
* Ante todo, juego limpio. Si se detecta el uso cualquier herramienta, que no venga con el juego, que permita que un jugador obtenga una gran ventaja, dicho jugador sera eliminado. Esta norma incluye a los trucos y cheats
* En el caso de los 2 puntos anteriores, si no se encuentra en dicha partida un colaborador, sea jugando o como espectador, se deberá de enviar una serie de capturas de los mensajes.
* Para el chat de voz se usara el servidor de Mumble de JugandoEnLinux y no los de los juegos.


La lista de juegos entre los que elegir es (para votar entrar en el hilo):


* Super Tux Kart
* Open Arena
* Xonotic
* Battle for Wesnoth
* O A.D.
* Counter Strike: Global Offensive
* Otros (proponer en el hilo)
