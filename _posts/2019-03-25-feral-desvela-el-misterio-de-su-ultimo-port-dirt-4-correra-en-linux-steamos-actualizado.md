---
author: leillo1975
category: Carreras
date: 2019-03-25 16:59:51
excerpt: "<p>El juego de @feralgames llegar\xE1 en tan solo unos d\xEDas.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/70f7f16131bcee9413b03bd0296217c6.webp
joomla_id: 925
joomla_url: feral-desvela-el-misterio-de-su-ultimo-port-dirt-4-correra-en-linux-steamos-actualizado
layout: post
tags:
- feral-interactive
- codemasters
- dirt-4
- '2019'
title: "Feral desvela el misterio de su \xFAltimo port: Dirt 4 correr\xE1 en Linux/SteamOS\
  \ (ACTUALIZADO 2)"
---
El juego de @feralgames llegará en tan solo unos días.

**ACTUALIZACIÓN 25-3-19:** 


Acabamos de recibir la información por parte de Feral que tan solo en 3 días, el próximo día **28 de Marzo**, tendremos disponible DIRT 4, su último port, anticipándose a la fecha anteriormente facilitada y cumpliendo con creces los plazos de disponibilidad. También ha anunciado los **Requisitos Técnicos necesarios** para ejecutarlo en Linux, que podeis ver a continuación (en Inglés):


**Linux system requirements:**


* **OS:** Ubuntu 18.04
* **Processor:** Intel® Core™ i3-3225 3.3ghz​
* **GPU:** 2GB Nvidia 680, 2GB AMD R9 285 (GCN 3rd Gen and above) or better​ (See notes for more details)
* **System RAM:** 4 GB RAM
* **Storage:** 39GB


**Additional Notes:**


* Requires Vulkan
* Nvidia graphics cards require driver version 418.43 or later
* AMD graphics cards require Mesa drivers version 18.3.4 or later
* AMD GCN 3rd Gen graphics cards include the R9 285, 380, 380X, Nano, Fury, and Fury X
* Other modern drivers and distributions are expected to work but are not officially supported


Como veis son unos requisitos técnicos bastante comedidos teniendo en cuenta la calidad del título, lo que hace presagiar que Vulkan tiene gran parte de "culpa" en esto.  Podeis [pre-comprar el juego en la tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/dirt4/) para tenerlo listo tan pronto se active en nuestro sistema. Os dejamos en compañía del video que acaban de publicar en su canal de Youtube:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/E71irVMQ4Dc" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Por supuesto en JugandoEnLinux.com os ofreceremos toda la información sobre el juego en cuanto salga, y si nos es posible un Stream exclusivo del juego en nuestros canales de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux).




---


**ACTUALIZACIÓN 28-2-19:**  
Nos acaba de llegar un correo informándonos que la cuarta entrega de la saga DIRT, llegará a nuestros flamantes escritorios en más o menos un par de meses, por lo que según esto como muy tarde llegaría a finales de Abril. Aquí os dejamos la instantanea de la newsletter de este mes donde lo pone:


![DIRT4Newsletter](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DIRT4/DIRT4Newsletter.webp)


Continuaremos informando de todas las noticias que genere este lanzamiento como siempre.




---


**Noticia Original:**


La compañía británica Feral vuelve a la carga y tras los anuncios de [Shadow of Tomb Raider](index.php/homepage/generos/accion/5-accion/1027-shadow-of-the-tomb-raider-llegara-a-linux-steamos-el-proximo-ano), [Total War: Three Kingdoms](index.php/homepage/generos/estrategia/8-estrategia/981-total-war-three-kingdoms-llegara-a-linux-steamos-la-proxima-primavera) y [Life is Strange 2](index.php/homepage/noticias/39-noticia/986-life-is-strange-2-tendra-version-nativa-para-gnu-linux) para 2019, pondrá un pinguino en la lista de plataformas soportadas al titulo de [Codemasters](index.php/buscar?searchword=codemasters&ordering=newest&searchphrase=all&limit=20). Cuando muchos de nosotros apostábamos por **Dirt Rally 2**, Feral ha ido sobre seguro y se ha decantado por **DIRT 4**, un juego con una vertiente mucho más arcade y menos enfocada a la simulación. El anuncio nos llegaba a través del correo electrónico y sus cuentas en las redes sociales, tal y como podeis ver en el siguiente tweet:



> 
> DiRT 4 is on track for macOS and Linux in 2019.  
>   
> Next year, get behind the wheel of the most powerful machines ever made as you face the world's toughest environments and circuits in rally, rallycross, and landrush races. [pic.twitter.com/b1X95LV0oR](https://t.co/b1X95LV0oR)
> 
> 
> — Feral Interactive (@feralgames) [December 7, 2018](https://twitter.com/feralgames/status/1071056848525107202?ref_src=twsrc%5Etfw)







La nota de prensa que nos hacía llegar Feral dice lo siguiente:



> 
> *Feral Interactive ha anunciado hoy que DiRT® 4™, el aclamado juego de carreras de rallyes y todoterreno, saldrá a la venta en macOS y Linux en 2019. Originalmente desarrollado y publicado por Codemasters para PC y consolas, DiRT 4 es el último de los juegos de carreras de renombre mundial que Feral ha traído a macOS y Linux, tras el éxito de DiRT Rally®, GRID Autosport™ y F1™ 2017.*
> 
> 
> *DiRT 4 ofrece la emoción intensa de los deportes de motor todo terreno en una electrizante mezcla de disciplinas. Los jugadores competirán en carreras de Rallys punto a punto, competirán en eventos del Campeonato Mundial de Rallys de la FIA, empujarán camiones y buggies hasta el límite en emocionantes batallas de Landrush, y pondrán a prueba sus habilidades de precisión en la dirección en los desafíos de Joyride.*  
>    
> *La flota de DiRT 4, compuesta por más de 50 coches de rally, buggies, camiones y coches de carreras, incluye el Ford Fiesta R5, el Mitsubishi Lancer Evolution VI, el Subaru WRX STI NR4 y el Audi Sport quattro S1 E2. Los jugadores pondrán estas increíbles máquinas a prueba en las rutas de rally más exigentes del mundo, desde los caminos de tierra de Australia hasta los peligrosos ventisqueros de Suecia y las pistas de grava abiertas de Michigan USA.*  
>    
> *Por primera vez en la historia de la franquicia, DiRT 4 ofrece a los jugadores la oportunidad de crear sus propias rutas de rally con Your Stage, una potente herramienta que genera escenarios únicos basados en la elección de la ubicación y los parámetros de ruta de los jugadores*
> 
> 
> 


Como acabais de leer DIRT 4 no abarca solo los Rallyes, sinó que podremos conducir de una forma más desenfadada y menos exigente en diversos tipos de vehículos todoterreno y realizar carreras en los más bacheados circuitos. Como veis, **los aficionados al volante estamos una vez más de enhorabuena**, pues como leisteis esta misma mañana, [Virtual Programming traerá a su vez Gravel](index.php/homepage/generos/carreras/2-carreras/1045-virtual-programming-esta-portando-gravel-a-linux-y-hay-nuevos-rumores-de-a-hat-in-time), con lo que el año que viene queda claro que el año que viene nos vamos a ensuciar las ruedas. Como siempre desde JugandoEnLinux.com os informaremos puntualmente, como suele ser habitual con los títulos de Feral, de cualquier novedad que tengamos con respecto a este juego. Mientras tanto os dejamos con el trailer del anuncio:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/7WX506pA9LI" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Como os habeis quedado? ¿Esperabais que Feral portase DIRT 4? ¿Qué os parecen los juegos de coches? Deja tus respuestas en los comentarios o charla con nosotros sobre el tema en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

