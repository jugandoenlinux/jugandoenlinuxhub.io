---
author: leillo1975
category: "An\xE1lisis"
date: 2017-03-05 13:31:00
excerpt: "<p>Revisamos la \xFAltima entrega de este cl\xE1sico de la estrat\xE9gia\
  \ por turnos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3abb66d58aa91d2b7b16f08ee38a95c0.webp
joomla_id: 230
joomla_url: analisis-sid-meier-s-civilization-vi
layout: post
tags:
- estrategia
- aspyr-media
- civilization-vi
- turnos
title: "An\xE1lisis: Sid Meier's Civilization VI"
---
Revisamos la última entrega de este clásico de la estratégia por turnos

Hace más o menos un mes dabamos cuenta aquí, [en JugandoEnLinux.com](index.php/homepage/generos/estrategia/item/334-sid-meier-s-civilization-vi-ya-esta-disponible-en-linux-steamos), del soporte para nuestros sistemas de esta nueva versión de la conocida IP de 2K y Firaxis Games. En primer lugar me gustaría **dar las gracias a Aspyr**, la compañía que ha hecho el trabajo de portar el juego, por cedernos gratuitamente una copia para su análisis.


 


Pero primero vamos a ponernos en antecedentes. Civilization es una serie de juegos que se inaguró hace ya más de 25 años de la mano del genial [Sid Meier](https://es.wikipedia.org/wiki/Sid_Meier), padre también de juegos tan geniales como **F15 Strike Eagle**, **Railroad Tycoon** o **Pirates!**. Al poco tiempo de anunciar Steam su soporte para los usuarios del pingüino, uno de los primeros juegos AAA que llegaron a nuestros PC's fué la quinta entrega de esta saga. También, por que no decirlo, uno de mis primeros juegos comprados para Linux, ya que me encanta la estrategia por turnos. Lo primero que me llamó la atención fué lo bien "portado" que estaba, ya que funcionaba perfectamente en cualquier máquina, incluso muy por debajo de los requisitos teóricos. También era encomiable que apenas había diferencias de rendimiento entre la versión de Windows y la nuestra. Tras una buena cantidad de horas jugadas puedo decir que el quinto episodio de Civilization fué uno de mis juegos de estrategia favoritos.


 


![Ciudades](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisCiv6/Civ6_Ciudad.webp)*El detalle de las ciudades, sus distritos y maravillas es soberbio*


Y por fín llegó el momento de poder echarle las manos a su continuación. Civilization VI continua la estela del anterior, en la gran mayoría de los sentidos, principalmente en su jugabilidad y adicción endiabladas, en su riqueza de acciones a realizar, en las muy diferentes formas de jugar que podemos escoger a la hora de dominar el mapamundi, en las diversas perspectivas del juego... Todos los que hayais jugado alguna vez a Civilization sabeis que es muy difícil despegarse de él, ya que siempre queda una acción por realizar, un movimiento que hacer, una maravilla que terminar o una tecnología que describir... en fin, la ya muy manida expresión de "un turno más y lo dejo".


 


Y aunque su continuidad con anteriores entregas, principalmente con la quinta, es una de sus grandes bazas, también es cierto que introduce bastantes novedades que hacen que su adquisición esté más que justificada. Hablando de dichas novedades, estás van más allá del apartado gráfico, aunque ya que estamos, vamos a empezar por ello. Obviamente el paso de los años le ha otorgado a Civilization VI un aspecto mucho más pulido que su antecesor, aunque cierto es que esto no es lo más importante en este tipo de juegos, la sensación general es que se ha buscado mucho más el detalle. El aspecto, por ejemplo de los líderes está muy bien recreado, con unos gestos muy creibles.


![Civ6 lideres](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisCiv6/Civ6_lideres.webp)*El diseño de los personajes y sus gestos está muy detallado*
 


En cuanto al resto de elementos del juego, se ha cuidado mucho el detalle de las ciudades, donde ahora podemos observar con detalle si hacemos zoom con el ratón, no solo el centro, sino sus distritos, maravillas, explotación de recursos...   También las unidades de diferentes tipos que se mueven por el mapa están mucho más cuidadas. En cuanto a la niebla de guerra, esta cambia con respecto a CivV, ya que las zonas no descubiertas ahora son dibujos de mapa antiguo, y en el caso de las ya exploradas, adquieren un tono sepia. Todos estos detalles gráficos obviamente influyen en el rendimiento del ordenador, lo cual hace que la tasa de frames acuse un descenso considerable con respecto al título anterior. Es especialmente notable a medida que avanza la partida y vamos acumulando ciudades y unidades en el mapa. Además cabe decir que el proceso de adaptación a nuestro sistema, como suele ser en general con muchos ports, se ha dejado un porcentaje considerable de rendimiento con respecto a Windows. Con respecto a esto último he echo diversas pruebas de rendimiento gráfico con diferentes ajustes tanto en Linux; como en Windows, tanto en DirectX11 como DirectX12. Os dejo la tabla de las mediciones realizadas en mi equipo (**Intel i5-3550, 16GB DDR3, GTX950-2GB, Ubuntu 16.10 64Bits**). Estas mediciones están hechas "a ojo" viendo el minimo y el máximo, ya que los datos ofrecidos por los archivos .csv que genera el programa no son muy fáclles de interpretar. He decidido hacer los tests solo de los niveles de detalle y consumo de memoria **Bajos, Medios y Altos**, por que supongo que los los Mínimos y los Ultra, la gran mayoría de jugadores no los suelen utilizar.


 




|  |  |  |  |
| --- | --- | --- | --- |
| **Ajuste** | **GNU/Linux OpenGL** | **Windows DirectX11** | **Windows DirectX12** |
| **Bajo** |  23-41 fps |  46-96 fps |  49-109 fps |
| **Medio** |  20-38 fps |  44-93 fps |  40-80 fps |
| **Alto** |  17-33 fps |  35-65 fps |  23-46 fps |


 


Puedo decir, según mis tests, que la tasa de frames a 1080 en Linux es **bastante inferior** a la de Windows con DX11, incluso me atrevería a decir que la duplica. Con respecto a DX12, esta diferencia es menor, aunque aun así es considerable. Cabe decir que la prueba de rendimiento gráfico es bastante intensiva ya que posee multiud de elementos en pantalla, lo que ralentiza considerablemente el desempeño del juego. En una partida normal la tasa de frames es más alta, pero a medida que vamos teniendo más ciudades y unidades en pantalla los FPS van decreciendo. Recientemente Aspyr confirmó en los foros de Steam que [están trabajando en un parche que mejore esta situación](http://steamcommunity.com/app/289070/discussions/0/144512942755435118/?ctp=27#c135509124602029681), en el que [tampoco descartaban](http://steamcommunity.com/app/289070/discussions/0/144512942755435118/?ctp=28#c135509124602743302), aunque sin mencionarlo, el uso de otras "soluciones", lo que hace suponer que están sopesando el uso de Vulkan. Si en futuro este parche mejora el desempeño del juego, actualizaré los datos ofrecidos en este análisis.


 


Las novedades en el tema de la jugabilidad, aunque como comentaba antes el sistema de juego no varía mucho con respecto a Civilization V, son importantes. Por ejemplo, los obreros ahora no ejecutan su función de forma infinita y en determinados turnos, sino que lo hacen de forma inmediata, pudiendo realizar tres acciones construcción antes de desaparecer. Otra gran novedad es el poder agrupar unidades; por ejemplo, ahora podemos protejer con unidades militares a nuestro colonos u obreros. También es importante destacar que ahora muchos de los edificios y maravillas del juego se realizan en distritos que ocupan una casilla del territorio, y que estas obtienen bonificaciones si se colocan en determinados lugares, por lo que la administración de nuestro territorio cobra mayor énfasis y dificultad, y es tremendamente importante la planificación a la hora de escoger ya que dicho espacio es limitado. Para explicar un poco mejor esto, si colocamos un campus en nuestra ciudad, deberemos tener en cuenta que si tiene una montaña adyacente proporcionará un plus de producción de ciencia; también, por ejemplo, si no tenemos un rio o una montaña cerca, no podremos construir un acueducto.


 


![Principios](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisCiv6/Civ6_Principios.webp)*Existen dos árboles de progreso, Ciencia y Principìos*


Otra diferencia sustancial con respecto a su predecesor es que el arbol de evolución de nuestra civilización en el juego se duplica, teniendo por un lado tecnología, y por otro los principios, que engloban políticas, economía, y religión.Y es esto último también lo que conforma un cambio importante en el juego, ya que la influencia religiosa puede inclinar la balanza hacía la victoria en el juego. Si conseguimos que más de la mitad de los ciudadanos del mapa se convierta a nuestra religión conseguiremos ganar la partida. Para ello habrá "batallas de fe" entre las diferentes unidades religiosas. En cuanto a la politica, ahora se puede detallar mediante un sistema de cartas, teniendo la posibilidad de escoger las que más nos convengan según nos interese en cada momento de la partida. Estas se dividen en politicas militares, económicas y diplomáticas, teniendo la posibilidad de usar comodines. Dependiendo del modo de gobierno que escojamos y de la Civilización que guiemos, tendrán más preponderancia unas que otras. Este es otro detalle que añade más riqueza al juego, y otorga al jugador más posibilidades de elección.


 



![Política](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisCiv6/Civ6_Politicas.webp)*La política se decide por un sistema de cartas*

 


Existen 18 civilizaciones en el juego base, que se pueden ampliar con las expansiones, teniendo cada una un lider diferente. Cada una de estas tiene caracteristicas que permiten jugar aventajado en diferentes situaciones, siendo estos beneficios unidades y edificios especiales, o bonuses en determinadas áreas, lo que hace que en la práctica si las aprovechamos, obtengamos superioridad en esas areas con respecto a las otras civilizaciones y líderes. También es importante destacar que, en los primeros compases del juego, los barbaros nos jugarán más de una mala pasada, y nos amargarán la existencia con bastante frecuencia y aínco, lo que pondrá nuestro reto de crecer y evolucionar en dificultades. Se nota que Firaxis ha subido la dificultad y la insistencia de estas unidades respecto a entregas anteriores. Especialmente debemos tener cuidado con nuestros comerciantes y exploradores, ya que es frecuente que estos sean atacados. Hablando de los comerciantes, ahora ganan gran importancia ya que son los encargados de crear carreteras, no como antes que era responsabilidad exclusiva de los obreros. Si queremos desplazarnos por el mapa rapidamente, estas son importantisimas, por lo que si por cualquier razón necesitamos ir a determinado lugar es bueno mandar antes a un comerciante a una ciudad cercana para que vaya "allanando el terreno".


 


En la serie Civilization siempre ha tenido especial importancia la diplomacia, y en esta ocasión, si cabe, aun más. Ahora, además de estar mucho más claro y detallado todo, tenemos nuevas opciones, entre las que destaca el poder utilizar el **"Casus Belli"** previamente al inicio de una guerra, de forma que los efectos negativos que produce esta con respecto al resto de civilizaciones se reducen. Por norma general al resto de lideres no les suele gustar que seas belicoso y mostrarán su enfado cuando entres en guerra, aunque no sea con ellos directamente. También es posible utilizar **espias** para obtener información de nuestros vecinos, así como la **agenda oculta** que tiene cada uno de ellos. Gracias a esto último podremos saber que gusta u odia cada uno de los líderes para actuar en consecuencia. Es importante también la relación con las ciudades estado, que han ganado importancia y peso, pudiendo enviar delegaciones a medida que ganamos influencia en el mapa. Estas nos van a permitir diversos beneficios y si somos la potencia con más influencia nos convertiremos automáticamente en **suzeranos** de esa ciudad, lo que va implicar que se conviertan en aliadas y paguen tributos.


 


En cuanto al tema del sonido, las voces de los líderes de las civilizaciones están muy cuidadas, y es necesario destacar la calidad de las diferentes músicas del juego, que son conocidas canciones, propias de cada una de las diferentes facciones. Los efectos de sonido están tremendamente detallados y cada unidad tiene los suyos.El juego está completamente traducido al Español, tanto en textos como en voces, siendo la calidad del doblaje excelente. Es importante destacar que en la versión inglesa del juego las narraciones transcurren a cargo del conocido actor británico [Sean Bean](https://es.wikipedia.org/wiki/Sean_Bean).


 


![Derrota](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisCiv6/civ5-03.webp)*Esta estampa será más común de lo que crees en tus primeras partidas*
 


En el apartado **multijugador** podremos disponer de **3 opciones**, a través de **internet**, donde también podremos medirnos con jugadores de Mac, ya que con Windows aun no es posible. Este inconveniente se supone que se solucionará en el futuro a traves de alguna actualización. También podremos jugar a través de una red privada (**LAN**) o **usando el mismo ordenador**, alternando el uso de los controles. Hablando de controles, el más adecuado con diferencia es el ratón, aunque es perfectamente posible usar el Steam Controller si nos apetece jugar por ejemplo en el televisor desde el sofá usando un Steam Link.


 


En definitiva nos encontramos ante un juego grandiso, donde es dificil cuantificar su coste debido a su altísima rejugabilidad, ampliable incluso mediante DLC's, y donde jamás vamos a encontrar dos partidas iguales, pudiendo afrontar el juego no solo utilizando diferentes civilizaciones, sino también muy diversas tácticas,diferentes unas de otras; y marcandose nuevos retos constantemente. Si amamos la estrategia por turnos, como es mi caso, Civilization VI es una compra absolutamente obligada, una redifinición de un clásico que reune novedades y continuidad a partes iguales, y que no nos dejará despegarnos de nuestras sillas en mucho tiempo *...por los siglos de los siglos....*


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/5KdE0p2joJw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 Puedes adquirir Sid Meier's Civilization VI en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/289070/123215/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Puedes dejar tu opinión acerca de este análisis en los comentarios o usando nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).


 

