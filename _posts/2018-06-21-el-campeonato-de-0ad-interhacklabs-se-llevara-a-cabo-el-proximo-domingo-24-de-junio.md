---
author: Jugando en Linux
category: Noticia
date: 2018-06-21 08:01:09
excerpt: "<p>Un campeonato \"just for fun\" de 0AD. \xA1An\xEDmate a participar!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/eeca348660096e711cd17c1f61fb2519.webp
joomla_id: 780
joomla_url: el-campeonato-de-0ad-interhacklabs-se-llevara-a-cabo-el-proximo-domingo-24-de-junio
layout: post
tags:
- 0ad
- campeonato
title: "El campeonato de 0AD Interhacklabs se llevar\xE1 a cabo el pr\xF3ximo Domingo\
  \ 24 de Junio"
---
Un campeonato "just for fun" de 0AD. ¡Anímate a participar!

Recibimos notificación sobre este campeonato que reproducimos íntegra:


Interhacklabs es basicamente un domingo en el que competir a este maravilloso juego de estrategia al más puro estilo Age of Empires junto a gente de diferentes colectivos, proyectos como jugando en Linux, hacklabs, ...


Es un buen momento para charlar vía mumble con gente de diferentes sitios y al mismo tiempo pasar un buen rato.


La fecha del campeonato es el Domingo 24 de Junio desde las 12:30 UTC+1. El lugar es virtual, es decir, quedaremos en el servidor mumble mumble.elbinario.net en el puerto 60601. Es necesario tener instalado el juego 0AD versión 0.23 y tener instalado un cliente mumble para poder escuchar y hablar.


Inicialmente el mínimo de gente necesaria son 16 jugadores/as pero pueden ser más. Las reglas y más información pueden consultarse en la siguiente url: https://hacklab.ingobernable.net/2018/06/10/campeonato-0ad-interhacklabs-formulario-de-inscripcion-abierto/


Actualmente ya se han inscrito gente que estará representando a colectivos como Ingoberlab 301, Ruby Girls Venezuela, Elbinario, La brecha digital, TGNU, mastodon, ... y tú estás invitado a participar.


Para poder gestionar el campeonato se necesita rellenar un formulario de 3 cajitas (nick, correo y colectivo que quieras representar/proyecto/espacio/...) que servirá exclusivamente para avisar del campeonato.


Si te gusta GNU/Linux, juegas a juegos libres o no libres y te apetece pasar un buen rato es el momento.


