---
author: Son Link
category: Estrategia
date: 2020-06-09 15:31:37
excerpt: "<p>Nuestro colaborador @sonlink nos rese\xF1a un juego que apunta maneras</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Thefertilecrescent/thefertilecrescent.webp
joomla_id: 1231
joomla_url: the-fertile-crescent-estrategia-real-y-gratuita-en-la-edad-de-bronce-actualizado-3
layout: post
title: 'The Fertile Crescent: estrategia real y gratuita en la Edad de Bronce - ACTUALIZADO
  3'
---
Nuestro colaborador @sonlink nos reseña un juego que apunta maneras


**Actualización 18-06-2020**


Desde hace unas horas tenemos una nueva versión del juego, el cual ya soluciona los problemas detectados con el auto-actualizador, el sistema de sonido (usan FMOD) y otras correcciones mayores, como la eliminación de código antiguo en la búsqueda de rutas que podía congelar el juego, y otro que podría destruir una granja si esta esta situada debajo de una construcción que se acaba de destruir.




---


**Actualización 17-06-2020:**


Hace unas horas han sacado una nueva versión del juego cuyas novedades son el balanceo de las unidades del juego:


* Se ha reducido la vida del espadachín de 100 a 85.
* Se a incrementado la vida del hachero pesado de 70 a 80 y su tiempo de entrenamiento de 15 a 16 segundos.
* Se ha reducido la vida del lancero pesado de 80 a 70.
* Se ha reducido el entrenamiento del hondero de 15 a 11 segundos.
* La línea de visión de las unidades ahora es simétrica


Una de las novedades de la anterior versión, el lanzador con capacidad para auto-actualizar el juego esta fallando en Linux (fallos que reporte), por lo que esta versión hay que instalarla manualmente hasta que los desarrolladores del parche den con la solución lo antes posible.


[Nota completa de la versión](https://lincread.itch.io/the-fertile-crescent/devlog/154289/0603-unit-balance-changes)


[Nota del fallo del auto-actualizador](https://lincread.itch.io/the-fertile-crescent/devlog/154269/problems-with-self-patcher-for-linux) 


 




---


**Actualización 11-06-2020:**


Hace unas horas se ha publicado una versión del juego, la 0.6.0.2, con pocos cambios, pero 2 de ellos son bastante importantes:


* Auto-actualización: La principal novedad que trae es que desde ahora el juego trae un lanzador desde el cual ademas se instalaran de manera automática las actualizaciones, ahorrando así tiempo y ancho de banda (ya que solo bajara los ficheros necesarios). Ademas esto también facilitara que saquen versiones cada poco tiempo.
* Nuevo servidor para América del Sur: La otra gran novedad es que ahora cuenta con un quinto servidor para América del Sur. Sin duda una buena noticia para los que juguéis desde países situados en esa parte del continente americano.
* Cambios en el espadachín: Para mejorar el balance en el juego se ha reducido la salud y el daño.
* La investigación en curso no se muestra en el árbol de tecnologías.
* Varias correcciones.


Podéis ver el listado completo en <https://lincread.itch.io/the-fertile-crescent/devlog/153517/0602-self-update-launcher>




---


**Artículo original:**


**The Fertile Crescent** es un juego de estrategia en tiempo real indie inspirado en los **Age of Empires**, el cual nos sitúa en la **Edad de Bronce**. Este juego, aunque lleva 3 años en desarrollo, aun esta en una fase temprana pero sigue en activo. De hecho en el mes y algo desde que lo conocemos han ido sacando nuevas versiones y es bastante jugable tanto si jugamos solos como contra amigos a través del mutijugador. El juego esta disponible tambien para Windows y Mac y **es gratuito**.


Como en los AoE y el 0 A.D. empezaremos con algunos aldeanos y deberemos de recolectar distintos recursos para poder construir nuevas construcciones, reclutar tropas e investigar nuevas tecnologías. Aquí tendremos que recolectar **comida**, la cual tiene un papel más importante, **madera**, **ladrillos** y **metal**. Como en otros juegos similares la comida sera necesaria para reclutar tropas y tener más aldeanos, pero según los vayamos reclutando estos irán consumiendo una determinada cantidad de ella, por lo que es vital tener recolectores para que el nivel de satisfacción de los aldeanos no baje, y con ello que se revelen contra nosotros. Ademas tener niveles altos tienen sus ventajas, como que los reclutamientos sean más rápidos, y lo mismo a la hora de recolectar e investigar.


Aquí los mapas son generados de manera aleatoria, no dispondremos de un listado como en otros juegos, por lo que cada partida es única y no podremos ir memorizando la posición de los recursos, mejores zonas para levantar torres de vigilancia, etc.


Otro aspecto destacado son sus gráficos, ya que esto son pixel art dándole un aspecto retro el cual al menos para mi, tienen su encanto y son bastante correctos. También contamos con música de fondo, si bien se vuelve algo repetitiva, y cuenta con varios efectos de sonido.


Otro aspecto es que cuenta con modo multijugador, con 4 servidores disponibles a los que conectarnos dependiendo desde donde nos estemos conectados. Podemos o bien jugar **1 contra 1 o partidas a 4 jugadores**. A continuación podéis ver un vídeo de una partida multijugador entre un servidor y el colega **CansecoGPC** del [canal de Telegram](https://telegram.me/jugandoenlinux) de este blog.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/ZbPVeos1EHs" width="560"></iframe></div>


El juego dispone de un tutorial de unos 10 minutos donde aprender lo más básico del juego, el cual recomiendo, hayas jugado o no antes a juegos de este tipo.


Si bien el juego esta bastante bien tiene algunas carencias a la hora de publicar este articulo:


* Falta de civilizaciones: Solo hay una, basada en la Antigua Babilonia, pero tienen intención de añadir más.
* Más tecnologías y estructuras: Si bien el juego cuenta con varias y teniendo en cuenta en donde esta situado el juego hecho en falta alguna más.
* Poca variedad de alimentos: Solo podemos recolectar frutos de los arboles y con granjas, si bien en una encuesta que realizaron hace un mes una de las opciones era añadir animales y otra para la pesca.


Mi veredicto es que, aun estando en una fase temprana es bastante jugable y muy recomendable. Quizás se haga corto por la poca variedad que tiene, pero para echar unas partidas de vez en cuando, y retar a nuestros amigos y familiares en el multijugador, es un buen juego. Te animo a probarlo y darles algo de feedback a sus desarrolladores para mejorarlo.


Puedes descargar **The Fertile Crescent** en su [página de itch.io en este enlace](https://lincread.itch.io/the-fertile-crescent).

