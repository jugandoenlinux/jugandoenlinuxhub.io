---
author: Serjor
category: Ofertas
date: 2017-08-04 16:52:01
excerpt: <p>Fin de semana del editor con interesantes descuentos</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8ab790b9074fed051af256aa504a66b7.webp
joomla_id: 426
joomla_url: rebajas-en-el-catalogo-de-team-17
layout: post
tags:
- rebajas
- team-17
title: "Rebajas en el cat\xE1logo de Team 17"
---
Fin de semana del editor con interesantes descuentos

Qué gusto da abrir steam y que salgan noticias como esta:


![team17discount](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/team17discount.webp)


Y es que como podéis ver Team 17 ha decidido que este fin de semana es tan bueno cómo cualquier otro para hacer interesantes descuentos, y cómo es una empresa de bien, nos trae a Linux también estos descuentos, como Yooka-Laylee, The Escapists o Worms entre otros.


Podéis visitar la página de la oferta [aquí](http://store.steampowered.com/sale/team17_weekend/?snr=1_41_4__42).


Dinos con qué juegos te vas a hacer en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

