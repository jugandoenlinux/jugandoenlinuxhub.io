---
author: Pato
category: Noticia
date: 2017-10-10 17:27:21
excerpt: "<p>Arrojamos un poco de luz del por qu\xE9 no nos llegan sus nuevos juegos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/94ae6d3810d6b4d1b169793b928c343e.webp
joomla_id: 479
joomla_url: frozenbyte-no-descarta-portar-sus-proximos-juegos-a-linux-pero-depende-de-si-encuentran-la-manera-de-hacerlo-viable
layout: post
tags:
- frozenbyte
title: "Frozenbyte no descarta portar sus pr\xF3ximos juegos a Linux, pero depende\
  \ de si encuentran la manera de hacerlo viable"
---
Arrojamos un poco de luz del por qué no nos llegan sus nuevos juegos

Para ponernos en antecedentes, Frozenbyte es el estudio Finlandés que nos trajo a Linux hace algunos años la excelente [saga Trine](http://store.steampowered.com/bundle/721/Trinelogy/) (por cierto, ¡de oferta ahora!). Este año pasado también nos trajeron con desigual suerte [Shadwen](http://store.steampowered.com/app/425210/Shadwen/), un juego de sigilo que pasó sin pena ni gloria, pero a partir de ahí ya no hemos vuelto a saber nada sobre el desarrollo de sus próximos juegos para nuestro sistema favorito.


![Trine](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Trine.webp)


*Trine fue una verdadera obra de arte visual*


Tanto es así que su último juego '[Has been Heroes](http://store.steampowered.com/app/492290/HasBeen_Heroes/)' no tiene visos de llegar a Linux, y por lo que dijeron los desarrolladores [en uno de los típicos hilos](http://steamcommunity.com/app/492290/discussions/0/144513524088564058/?tscn=1507279138) preguntando por ello, *"los futuros ports a Linux no están descartados, pero desafortunadamente son muy improbables en este momento".*


Tirando un poco del hilo, y dado que los desarrolladores son bastante activos en los foros buscamos el motivo que ha llevado al estudio a no portar a Linux sus juegos. Es más, tampoco se plantean los ports a sistemas Mac, dado que sus dos últimos desarrollos solo tienen el logo de Windows. 


En otro hilo de su próximo desarrollo '[Nine Parchments](http://store.steampowered.com/app/471550/Nine_Parchments/)' los desarrolladores arrojan algo de luz al asunto afirmando que *"ya no tienen un equipo dedicado a realizar estos ports"*. Ante la persistencia en las peticiones, uno de los desarrolladores (Larja) que trabajó en ports a Linux [afirma](http://steamcommunity.com/app/471550/discussions/0/1473096694437220987/#c1519260397775560203) que las cifras de usuarios en Mac y Linux son las que son, y que aunque es difícil conocer el impacto en ventas de los ports de Linux la mejor opción para nosotros es que en un futuro se tomen en consideración realizar ports de los juegos a Mac bajo Opengl lo que haría mucho más sencillo portar también a Linux. Sin embargo Larja [abre una puerta](http://steamcommunity.com/app/471550/discussions/0/1473096694437220987/#c1488861734095876605) a la "externalización" de los desarrollos, afirmando que actualmente y con el trabajo de desarrollo de sus juegos para consolas no consideran posible desviar recursos a portar para nuestro sistema, pero que si alguien conoce algún desarrollador experimentado interesado en portar sus futuros juegos a Mac o Linux, pueden contactar con ellos a través de su página web para evaluarlo."


Dicho esto se abrió [un hilo en reddit](https://www.reddit.com/r/linux_gaming/comments/748fst/frozenbyte_recently_dropped_linux_porting_appears/) donde se ha estado comentando la situación.


Así pues, esta es la situación actual respecto a los próximos desarrollos de Frozenbyte para Linux. Esperemos que encuentren algún desarrollador (¿Ethan Lee?... ¿Ryan C. Gordon?...) que esté interesado y lleguen a algún acuerdo. Un estudio con el bagaje de Frozenbyte no es bueno que deje de dar soporte a Linux. ¿o piensas que si?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


Os dejo con los vídeos de los últimos juegos en desarrollo de Frozenbyte:


 Has Been Heroes:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/cNdQLneWp5M" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Nine Parchments (en desarrollo)


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/HlIHq3pLwTU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 

