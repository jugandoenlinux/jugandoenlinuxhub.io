---
author: leillo1975
category: Software
date: 2018-01-05 08:18:04
excerpt: "<p>La distribuci\xF3n base de las Steam Machines se renueva con cambios\
  \ sustanciales.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0bb2d4215d5de184c10e9f50ee1d9553.webp
joomla_id: 592
joomla_url: steamos-comienza-el-ano-con-una-nueva-version-beta
layout: post
tags:
- steamos
- amd
- intel
- mesa
- nvidia
- valve
- kernel
- debian
- steam-machines
title: "SteamOS comienza el a\xF1o con una nueva versi\xF3n Beta."
---
La distribución base de las Steam Machines se renueva con cambios sustanciales.

Gracias a la información ofrecida por [GamingOnLinux](https://www.gamingonlinux.com/articles/steamos-has-a-fresh-beta-update-with-some-major-package-updates.10995), nos acabamos de enterar que SteamOS acaba de publicar una Beta para su versión **Brewmaster**. El conocido Sistema Operativo desarrollado para las Steam Machines ha decidido estrenar este 2018 con una serie de novedades de calado que seguro que serán muy bien recibidas por sus usuarios, especialmente por los propietarios de soluciones gráficas de AMD. Su principales novedades son que han actualizado el **Kernel** a la versión **4.14** (con lo que ello supone), cambiada la versión de gráficos propietarios de **NVIDIA** a la **387.22**, y renovado **Mesa** a la **17.2.4**. A parte de esto hay también cambios menores, que podeis consultar en la [web](http://steamcommunity.com/groups/steamuniverse/discussions/1/1621726179565936999/) donde se anuncia.


Cuando se estaba especulando con la continuidad y viabilidad de SteamOS, Valve ha decido poner encima de la mesa esta nueva versión, que aunque Beta, demuestra que siguen trabajando en ella y no es un proyecto abandonado. Como sabeis SteamOS es un sistema operativo GNU-Linux enfocado a los videojuegos y base para el funcionamiento de la plataforma Steam. Con estos cambios **hace viable el crear máquinas (Steam Machines) con hardware AMD** con un rendimiento en este momento más que aceptable. Hasta hace unos meses SteamOS usaba los drivers propietarios de AMD, que no eran competitivos, lo que lastraba las posibilidades de utilizar equipos en base a gráficas AMD. Gracias a esta actualización se nos brinda una nueva posibilidad de **pensar de forma seria en utilizar otro tipo de hardware gráfico que no sea Nvidia** para adquirir o crear nuestra propia Steam Machine. Como punto negativo no se aprecian en la lista de cambios que se haya tocado nada con respecto a la Realidad Virtual o los controladores de periféricos. Estaría bien contar, por ejemplo, con un mejor soporte para los equipos VR, mandos y volantes.


Esta beta tiene desde nuestro punto de vista gran repercusión, porque a parte de lo comentado, podría permitir a Valve introducir un mayor abanico de posibilidades a la hora de pensar en crear nuevas Steam Machines, además de abaratar los costes, ya que es bien sabido que las soluciones de la compañía de Sunnyvale son por norma general más económicas que las ofrecidas por Intel-Nvidia.  Esperemos que este nuevo año nos traiga más y mejores noticias con respecto a SteamOS y las Steam Machines. El que este sistema tenga viabilidad, no solo es bueno para Valve y los usuarios de SteamOS, sinó que también repercutirá en la mejor calidad de los drivers y la posibilidad de más y mejores juegos para GNU-Linux.


Y tu, ¿eres usuario de SteamOS? ¿Has pensado en la posibilidad de adquirir o crear tu propia Steam Machine? Contéstanos en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

