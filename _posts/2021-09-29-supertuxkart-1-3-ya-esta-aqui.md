---
author: leillo1975
category: Carreras
date: 2021-09-29 07:40:47
excerpt: "<p>Los desarrolladores de este famoso juego libre acaban de liberar la nueva\
  \ versi\xF3n.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperTuxKart/supertuxkart.webp
joomla_id: 1370
joomla_url: supertuxkart-1-3-ya-esta-aqui
layout: post
tags:
- carreras
- open-source
- supertuxkart
title: "\xA1SuperTuxKart 1.3 ya est\xE1 aqu\xED!"
---
Los desarrolladores de este famoso juego libre acaban de liberar la nueva versión.


 Parece que en el equipo que está detrás de uno de los juegos más famosos del **software libre**, han pisado el acelerador y nos traén una nueva actualización de su proyecto. Si haceis memoria la última versión, [la 1.2]({{ "/posts/la-version-1-2-de-supertuxkart-ya-esta-disponible-para-su-descarga" | absolute_url }}), llegó el verano pasado, pero desde que lanzaron la versión 1.0 que trajo el tan de agradecer **modo Online**, se han sucedido una tras otra las actulizaciones, siempre cargadas de buenas noticias. El anuncio podíamos verlo en twitter ayer por la tarde:



> 
> SuperTuxKart 1.3 - stable release - is there!  
> ⏬Download now 👉 <https://t.co/lmo7J95PgX>  
> 👀Want to know more? 👉 <https://t.co/ASFh7x2hal> [pic.twitter.com/Wqwr5s3O6U](https://t.co/Wqwr5s3O6U)
> 
> 
> — SuperTuxKart (@supertuxkart) [September 28, 2021](https://twitter.com/supertuxkart/status/1442902924045144065?ref_src=twsrc%5Etfw)



Ciertamente esta versión final vino precedida de una [versión candidata](https://blog.supertuxkart.net/2021/08/supertuxkart-13-release-candiate.html) lanzada hace más o menos un mes, y las [diferencias](https://blog.supertuxkart.net/2021/09/supertuxkart-13-release.html) con esta son mínimas, siendo basicamente correcciones de bugs, optimizaciones y ajustes. Las [cambios](https://github.com/supertuxkart/stk-code/blob/1.3/CHANGELOG.md) principales con respecto a la versión 1.2 serían los siguientes:


***-Nuevas Arenas:** **Laberinto del Antiguo Coliseo** y **Señal alienígena**. En la primera nos encontraremos ante una arena de combate en un entorno oscuro inspirado en el coliseo de Roma. En la segunda nos encontraremos ante un mapa basado en una ubicación real del programa SETI. Ambas arenas son compatibles con el **modo Capturar la bandera**, lo que permite a los jugadores en línea tener una mayor variedad de pistas para elegir.*


*![SETI](https://1.bp.blogspot.com/-dITPazysQG8/YSjUDN_jH_I/AAAAAAAABpk/bhGFOKnDBTkrwY3Z5DYHXjA32nxH5fUPACLcBGAsYHQ/s1366/Screenshot_20210827_135750.webp)*


***- Nuevos Karts:** Se han hecho importantes trabajos para **renovar los karts existentes**. Sara la corredora ha sido sustituida por la simpática Pepper de Pepper&Carrot. Gnu tiene un nuevo aspecto, así como Sara la Maga, que ahora se llama Sara. Adiumy y Emule también han recibido un cambio de imagen.*


***-Mejoras en la interfaz gráfica de usuario**: La primera es la **Resolución de renderizado**, que es otra característica que mejora el rendimiento de STK y se puede activar a través de la "opción de resolución de renderizado" en la configuración de gráficos. Se puede bajar la resolución hasta un 50%, lo que redundará en una mejor tasa de fotogramas. Ahora está activada por defecto en la versión Android de SuperTuxKart. La segunda mejora es la **Nueva pantalla de selección de puntuaciones altas,** donde ahora puedes encontrar todas tus puntuaciones altas en el mismo lugar en un nuevo menú. Allí se muestran tus mejores tiempos en las carreras normales, las contrarrelojes y las cazas de huevos.*


***-Blender 2.8:** Este es el software oficial utilizado por los artistas de STK para crear pistas, karts, arenas y más. Blender 2.8 rompió la compatibilidad con 2.7, por lo que hay scripts actualizados de Blender para SuperTuxKart .*


***-Port Homebrew para Switch:** Aunque no tenga mucho que ver con nosotros, la conocida consola de Nintendo tiene ahora una versión funcional del juego. Gracias a la [implementación](https://github.com/supertuxkart/stk-code/pull/4491/files) de SDL2 realizada en 1.2, SuperTuxKart ha sido portado a Switch Homebrew.*


![amule](https://1.bp.blogspot.com/-RhwxbI7n03U/YS2t52dzXeI/AAAAAAAAATI/2_Saz-Jnap073L8zSuE31LSrvEESGQk2wCLcBGAsYHQ/s370/test.webp)


Como podeis ver las mejoras introducidas son cuantiosas y remarcables, y añaden un poco más de calidad a un proyecto que ya de por si va sobrado. Ahora solo queda que lo probeis descargándolo desde la página de [lanzamientos](https://github.com/supertuxkart/stk-code/releases/tag/1.3) de su proyecto. Os dejamos con el trailer de esta versión 1.3:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/ef9vYcuEDL4" title="YouTube video player" width="780"></iframe></div>


¿Os hace jugar unas partidillas de SuperTuxKart?  Si quieres puedes proponerlo a nuestra comunidad en nuestros grupo de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux) o nuestras salas de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org).

