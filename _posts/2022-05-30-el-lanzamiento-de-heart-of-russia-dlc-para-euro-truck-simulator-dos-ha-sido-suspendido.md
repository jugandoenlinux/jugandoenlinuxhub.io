---
author: leillo1975
category: "Simulaci\xF3n"
date: 2022-05-30 15:53:54
excerpt: "<p>Desde @SCSsoftware explican esta complicada decisi\xF3n.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/HeartOfRussia/Socmedia_1920x1080_ETS2_Heart_of_Russia_Statement.webp
joomla_id: 1470
joomla_url: el-lanzamiento-de-heart-of-russia-dlc-para-euro-truck-simulator-dos-ha-sido-suspendido
layout: post
tags:
- ets2
- dlc
- scs-software
- euro-truck-simulator-2
- heart-of-russia
- ucrania
title: El lanzamiento de "Heart of Russia DLC" para Euro Truck Simulator 2 ha sido
  suspendido
---
Desde @SCSsoftware explican esta complicada decisión.


 A la legión de aficionados de los productos de la compañía checa, no se nos escapaba la complicada situación por la que deben estar pasando. Como sabeis hace más de un año [os anunciábamos]({{ "/posts/heart-of-russia-sera-la-nueva-dlc-de-euro-truck-simulator-2" | absolute_url }}) que **la próxima expansión de mapa para ETS2 sería en territorio ruso**. A menos que vivais en un bunker, sabreis que desde finales del mes de Febrero, Ucrania ha sido agredida e invadida por el ejercito ruso, provocando miles de muertos, heridos, grandes daños materiales y un éxodo masivo de refugiados hacia el resto de paises europeos.


Esto ha provocado, como podeis leer en el título del artículo, que **el lanzamiento de Heart Of Russia DLC sea pospuesto indefinidamente** haste que "Ucrania se reconstruya y sanee", momento en el que verán cual será la mejor forma de hacer que este trabajo pueda servir para ese proceso de "curación".


Como veis un difícil momento para esa compañía, que suponemos que se ha visto entre la espada y la pared con respecto a este tema. Si lanzaban el producto podrían ser acusados de apoyar al pais invasor, con lo que sería una pésima publicidad para SCS, además de arriesgarse a que se boicoteara su producto por parte de los consumidores. Si no lo ponían en el mercado sería un año entero de trabajo perdido. Finalmente han tomado la que suponemos dolorosa decisión de no poner aun su producto en el mercado a la espera de que la situación en Ucrania se arregle definitivamente. Teneis [más información](https://blog.scssoft.com/2022/05/heart-of-russia-dlc-statement.html) sobre esto en su blog.

