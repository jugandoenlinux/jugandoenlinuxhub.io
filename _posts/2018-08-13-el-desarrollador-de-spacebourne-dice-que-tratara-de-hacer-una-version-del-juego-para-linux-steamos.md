---
author: Pato
category: Rol
date: 2018-08-13 15:19:15
excerpt: "<p>Se trata de un RPG espacial abierto donde podremos desempe\xF1ar diversos\
  \ roles. Algunos lo comparan con Star Citizen</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/140b52a229a0199a0b5b0f77e425d131.webp
joomla_id: 828
joomla_url: el-desarrollador-de-spacebourne-dice-que-tratara-de-hacer-una-version-del-juego-para-linux-steamos
layout: post
tags:
- indie
- proximamente
- rol
- exploracion
- simulacion
title: "El desarrollador de 'SpaceBourne' dice que \"tratar\xE1 de hacer\" una versi\xF3\
  n del juego para Linux/SteamOS"
---
Se trata de un RPG espacial abierto donde podremos desempeñar diversos roles. Algunos lo comparan con Star Citizen

'SpaceBourne' es un juego desarrollado por el estudio turco Burak Dabak y que tras ver el interés de los usuarios en el (típico) [hilo de peticiones](https://steamcommunity.com/app/732240/discussions/0/1742216566127593383/?ctp=2) para que el juego llegue a Linux, [ha comentado](https://steamcommunity.com/app/732240/discussions/0/1742216566127593383/?ctp=2#c1745594817429424893) en ese mismo foro que tratará de hacer una versión de su juego para nuestro sistema favorito.


SpaceBourne es un juego de exploración espacial abierto con elementos de rol. Su universo consta de diferentes facciones en guerra. Como pilotos expertos, tendremos que escoger nuestro bando o ir por libre y reclutar por tu cuenta. Carga tus armas, prepara tu nave y lánzate a la aventura:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/gMkO78_xXHg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Según su descripción, el universo de SpaceBourne **tendrá alrededor de 100 sistemas solares, 400 planetas y 37 estaciones espaciales**. El juego está diseñado para que el jugador tenga total libertad para hacer lo que quiera. Puede minar asteroides, participar en salvamentos, buscar tesoros, descubrir agujeros negros, convertirse en pirata, descubrir nuevos sistemas y mucho más, a la vez que seguir una historia, aceptar misiones y un largo etcétera.


No diremos que tiene mala pinta, pero la verdad es que se ve un desarrollo bastante ambicioso para lo que parece ser un pequeño estudio indie. Algunos usuarios incluso lo comparan con Star Citizen en ciertos aspectos, lo que ya son palabras mayores.


SpaceBourne tiene fecha de lanzamiento para este mismo mes de Agosto, pero dado que el desarrollador parece no tener experiencia con el desarrollo para Linux (el motor del juego es UE4, lo que tampoco es moco de pavo) , y que parece que ni siquiera ha comenzado a trabajar en dicha versión, seguramente pasarán unos cuantos meses hasta que tengamos alguna build del juego que podamos jugar.


Actualmente SpaceBourne está en fase de beta cerrada y en principio solo estará en inglés y turco. Si quieres saber mas, puedes visitar su [página web oficial](https://www.dbkgames.com/) o su [página de Steam](https://store.steampowered.com/app/732240/SpaceBourne/).


Si te gusta el planteamiento de SpaceBourne y quieres mostrar tu apoyo para que el juego llegue a Linux, pásate por el [hilo de la petición](https://steamcommunity.com/app/732240/discussions/0/1742216566127593383/) y déjale un comentario al desarrollador.

