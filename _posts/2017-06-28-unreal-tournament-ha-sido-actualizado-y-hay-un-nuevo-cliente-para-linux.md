---
author: Pato
category: "Acci\xF3n"
date: 2017-06-28 14:58:15
excerpt: <p>Epic ya no "esconde" el cliente para nuestro sistema</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/37e331b7a1b39f090b1249a069a513d3.webp
joomla_id: 390
joomla_url: unreal-tournament-ha-sido-actualizado-y-hay-un-nuevo-cliente-para-linux
layout: post
tags:
- accion
- acceso-anticipado
- unreal-tournament
title: Unreal Tournament ha sido actualizado y hay un nuevo cliente para Linux
---
Epic ya no "esconde" el cliente para nuestro sistema

Gracias al blog del desarrollo del juego nos llegan nuevas noticias de una actualización del nuevo Unreal Tournament que Epic tiene entre manos. 


Esta nueva actualización se enfoca sobre todo en el rendimiento del juego en equipos poco potentes y en mejoras en los shaders y texturas. Además, han añadido chat de voz con servidores dedicados y algunas nuevas armas y características.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/FnRp-cmOB-0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Por la parte que nos toca, ahora Epic hace mucho más sencillo acceder al cliente del juego para Linux. Anteriormente había que registrarse en el foro del juego y acceder a un hilo medio escondido donde además de otros enlaces había uno específico para Linux que había que buscar. Sin embargo, ahora tan solo con visitar el [post de su blog](https://www.epicgames.com/unrealtournament/blog/release-notes-june-28) ya tenemos el enlace al cliente, donde se supone que descargaremos ya la nueva actualización del juego, aún en fase de pruebas y también hay un enlace al marketplace donde hay mapas para Linux creados por la comunidad.


¿Qué te parece el nuevo Unreal Tournament? ¿Piensas jugarlo? ¿Te gustaría que organizásemos algo con este juego?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

