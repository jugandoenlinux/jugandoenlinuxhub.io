---
author: Pato
category: Aventuras
date: 2018-02-19 15:35:36
excerpt: <p>Se trata del nuevo juego de Grasshopper Manufacture junto a Suda51</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9b61c9add26ae66de11f78f97af7a160.webp
joomla_id: 649
joomla_url: the-25th-ward-the-silver-case-aparece-en-steamdb-para-linux
layout: post
tags:
- aventura
- rumor
- violento
- steamdb
title: '''The 25th Ward: The Silver Case'' aparece en SteamDB para Linux'
---
Se trata del nuevo juego de Grasshopper Manufacture junto a Suda51

Hoy nos hemos despertado con una reseña en SteamDB cuanto menos curiosa. Ha aparecido [listado para Linux](https://steamdb.info/app/697650/) 'The 25th Ward: The Silver Case' [[web oficial](http://nisamerica.com/games/the-25th-ward/)], juego que está siendo desarrollado por la compañía "Grasshopper Manufacture" junto a Suda51, un conocido autor japonés, que junto a la misma compañía ya desarrollaron juegos como el notable 'Killer is Dead'.


Se trata esta vez de una aventura de estilo violento ya característico en Suda51 que se desarrollaría 5 años tras los eventos sucedidos en "the Silver Case" de 1999, ambientado en el nuevo distrito 25 que surgió en el área costera de Kanto. Esto desencadena una serie de eventos aparentemente aleatorios que conectan a múltiples protagonistas, entre ellos, The Morishima, de The Silver Case. Con todos los puntos de vista ensamblados, emerge un patrón verdaderamente impactante...


 **Advertencia:** el vídeo lleva el sello violento personal de Suda51:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/de2VppkpeNM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


*Disponible por primera vez fuera de Japón, The 25th Ward: The Silver Case presenta assets en HD completamente reconstruidos y contenido adicional que no se encuentra en la versión original.*


En cuanto a la "precuela", "[The Silver Case](http://store.steampowered.com/app/476650/The_Silver_Case/)" está disponible para GNU-Linux/SteamOS en su página de Steam, y por lo que parece esta "secuela" también nos llegará en algún momento de este 2018, sin fecha concreta.


¿Que te parece este 'The 25th Ward:The Silver Case'? ¿Te gusta el estilo de juego particular de Suda51? ¿Te gustaría que llegaran más juegos de Grassohpper Manufacture? ¿Cual?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


A continuación te dejo un pequeño vídeo-trailer gameplay:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/f5we3UKCUDg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 

