---
author: leillo1975
category: Carreras
date: 2021-03-13 21:42:33
excerpt: "<p><span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@_VisionReelle_&nbsp;</span>\
  \ nos da un avance de la fecha de salida y activa p\xE1gina en Steam.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ClassicRacersElite/ClassicRacersElite.webp
joomla_id: 1289
joomla_url: classic-racers-estara-de-vuelta-con-su-version-elite-a1
layout: post
tags:
- unity3d
- classic-racers
- vision-reelle
- classic-racers-elite
title: "Classic Racers estar\xE1 de vuelta con su versi\xF3n \"Elite\" (ACTUALIZADO)\
  \  "
---
@_VisionReelle_  nos da un avance de la fecha de salida y activa página en Steam.


**ACTUALIZACIÓN 15-3-21:**


Hoy tenemos el placer de daros la noticia de que tendremos que esperar muy poco por este juego. Según hemos podido saber el juego se estrenará en Windows a **principios de Abril,** por lo que en principio **la versión de Linux no debería demorarse mucho** en el tiempo si todo va bien. Además **Jonathan** de [Vision Réelle](http://www.visionreelle.fr/) ha habilitado la [página en Steam](https://store.steampowered.com/app/1577080/Classic_Racers_Elite/), donde desde ya **podemos agregar el juego a nuestra lista de deseados** y seguir el juego. Os dejamos con el primer video de Classic Racers Elite:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://cdn.cloudflare.steamstatic.com/steam/apps/256825943/movie_max_vp9.webm" style="display: block; margin-left: auto; margin-right: auto;" type="video/webm" width="780"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1577080/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 




---


**NOTICIA ORIGINAL (13-3-21):**


Llevábamos más o menos un año sin recibir nuevas [noticias]({{ "/posts/classic-racers-finalmente-desembarca-en-linux-steamos-y-a-lo-grande" | absolute_url }}) de este desarrollador francés, [Vision Réelle](http://www.visionreelle.fr/)**,** siendo lo último que supimos que abandonaba el desarrollo del juego en PCs. Pero esta semana recibíamos una grata sorpresa al enterarnos que volvía a la carga, pero en esta ocasión con **Classic Racers Elite**, una especie de remake de su anterior juego con características muy superiores.


Por supuesto, en JugandoEnLinux, nos pusimos inmediatamente en contacto con él y tras la pregunta de rigor, entre otras muchas cosas, nos confirmó que el juego, al igual que el anterior, **tendrá versión nativa para nuestro sistema**, aunque muy posiblemente **llegará con un poco de retraso** con respecto a la fecha de lanzamiento en Windows, aunque conociéndole, no será mucho casi seguro. La cantidad de [novedades](https://store.steampowered.com/news/app/1037480/view/3017947494689303417) que encontraremos en esta nueva versión es enorme y podeis verla a continuación:


***Nuevas características:***  
*- Vista de cabina para todos los coches.*  
*- Remodelación y personalización de los mandos.*  
*- Nuevos efectos de imagen/post-procesamiento (Desenfoque de movimiento / Oclusión ambiental / Graduación de color / Bloom / Anti Aliasing).*  
*- Múltiples idiomas : Francés - Italiano - Inglés - Español - Alemán - Ruso - Polaco - Chino*


***Mejoras:***  
*- Nueva interfaz de usuario.*  
*- Gráficos mejorados (sombreado PBS completo) para un juego más atractivo.*  
*- Nueva configuración física para comportamientos más controlables y más realistas.*   
*- Nuevos modelos 3D y mejora de los antiguos.*  
*- Mejoras en la calidad de las texturas.*  
*- Rendimiento mejorado para un juego de 60fps sólido como una roca incluso con configuraciones modestas.*


***Nuevos contenidos:***  
*- 3 nuevos coches (de 13 a 16 en total) con una nueva categoría. Fórmula de los años 80.*  
*- Bohem 2112 turbo (grupo 2)*  
*- Fred Convoy MK1 (Grupo 2)*  
*- GH 25 (Fórmula)*  
*- Interior de alta calidad para los 16 coches.*  
*- 4 nuevas localizaciones:*  
*- Le Monsse (Pista cerrada larga)*  
*- Rebus (Pista cerrada de montaña larga)*  
*- Francis Pastis (Pista cerrada)*  
*- Mont Venteux (Subida de montaña larga)*  
*- 20 nuevas pistas. 31 originales para un total de 51 pistas.*


![CRElite](https://cdn.cloudflare.steamstatic.com/steamcommunity/public/images/clans/34603378/b5df3d68ec28f9e193309ec299e09ca5b09bd350.png)


Su desarrollador también ha pedido que le ayudeis a **decidir un [precio adecuado](https://store.steampowered.com/news/app/1037480/view/3017947494689303417) para su nuevo juego**. Classic Racers salió a la venta en un principio por 4.99€. Teniendo el cuenta que muchos lo conseguisteis y podeis conseguirlo completamente [gratis](https://store.steampowered.com/app/1037480/Classic_Racers/), desde JugandoEnLinux.com pensamos que el precio de "Classic Racers Elite" debería ser muy superior, pues mejora mucho el juego original y le añade mucho más contenido.


¿Qué os pareció Classic Racers y que opinais de lo que será su sucesor? ¿Qué precio le pondríais a esta versión "Elite"? Podeis dejarnos vuestras impresiones y opiniones en los comentarios en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

