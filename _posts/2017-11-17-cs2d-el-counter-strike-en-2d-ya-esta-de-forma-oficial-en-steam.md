---
author: Pato
category: "Acci\xF3n"
date: 2017-11-17 11:45:01
excerpt: "<p>Disparos gratuitos en este juego multijugador en l\xEDnea</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/793ea701476d5db2a4fd8cf1e0bf5880.webp
joomla_id: 543
joomla_url: cs2d-el-counter-strike-en-2d-ya-esta-de-forma-oficial-en-steam
layout: post
tags:
- accion
- indie
- free-to-play
- 2d
title: "'CS2D' el Counter Strike en 2D ya est\xE1 de forma oficial en Steam"
---
Disparos gratuitos en este juego multijugador en línea

Ya lo anunciamos [hace algún tiempo](index.php/homepage/generos/accion/item/623-cs2d-el-juego-gratuito-de-accion-en-vista-cenital-llegara-a-steam-15-de-noviembre), y ahora ya lo tenemos en Steam. 'CS2D' es un juego gratuito "clon" de Counter Strike" pero en 2D y vista cenital donde tendremos acción desenfadada y frenética a raudales.



> 
> *Dos equipos luchan entre si en encuentros llenos de acción. Con una variedad de modos de juego como plantar la bomba, rescate de rehenes, asesinato del V.I.P, captura la bandera, construcción, zombis, partida a muerte y partida a muerte por equipos. Tienes acceso a un enorme armamento - incluyendo cosas locas como portalguns, lasers, lanzamisiles y más. Juega en línea, en LAN o contra los bots. ¡Usa el editor de mapas incorporado para crear tus propios mapas en segundos o escribe programas de Lua para modificar y extender el juego!*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/0U5KmjuB2WU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Tal y como comentan los creadores, el juego es 100% gratuito. No hay forma de gastar dinero en el, por lo que "solo queda la diversión".


¿Que te parece esta propuesta? Por nuestra parte esperamos jugarlo en alguna de las próximas sesiones de juego de los viernes que organizamos aquí en Jugando en Linux.


Si quieres ir jugando, y de paso practicar para el multijugador lo tienes disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/666220/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

