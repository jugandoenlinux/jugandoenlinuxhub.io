---
author: leillo1975
category: Software
date: 2019-02-07 08:20:49
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\">@mdiluz ha incluido estos parches para\
  \ su revisi\xF3n</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/01d8b43f4f3fa760f12ff89aa5d9028b.webp
joomla_id: 966
joomla_url: gamemode-permitira-configurar-el-overclock-de-graficas
layout: post
tags:
- software
- amd
- nvidia
- feral
- gamemode
- overclock
title: "Gamemode permitir\xE1 configurar el Overclock de gr\xE1ficas."
---
@mdiluz ha incluido estos parches para su revisión

Si recordais, la primavera pasada [nos hacíamos eco](index.php/homepage/generos/software/12-software/830-feral-lanza-gamemode-una-utilidad-open-source-para-mejorar-el-rendimiento-en-los-juegos) de un desarrollo de Feral para potenciar nuestras sesiones de juego en GNU-Linux. Este software era en principio un demonio que se se ejecutaba en segundo plano y que entre otras cosas cambiaba el "governor" de "Ondemand" o "Powersave", a "Performance", proporcionando un extra de potencia cuando arrancamos los juegos. Para ello debíamos indicarlo en el arranque de estos, e incluso en los últimos desarrollos de Feral se hace automáticamente.


Para quien no lo conozca **Mark Di Luzio**, a pesar de [no trabajar actualmente en Feral](index.php/homepage/noticias/39-noticia/859-el-lider-del-grupo-de-desarrollo-para-linux-en-feral-interactive-deja-su-puesto-manana), sigue aportando como principal desarrollador de este proyecto libre, y en las últimas horas **ha incluido unos parches en el código para su revisión**, tal y como podeis ver en este tweet:



> 
> Alright, I'm looking for willing participants to try out my PR adding configurable GPU overclocks to GameMode on Linux. Works on Nvidia and AMD. I don't have anything more than internet kudos to offer I'm afraid.  
>   
> Pre-warning: overclocking can risk hurting your gpu, obviously.
> 
> 
> — Marc 2019 Edition ? (@mdiluz) [4 de febrero de 2019](https://twitter.com/mdiluz/status/1092478178172514304?ref_src=twsrc%5Etfw)



Estos parches permiten activar automaticamente el Overclock para subir la velocidad de reloj en **tarjetas AMD y NVIDIA** cuando arrancamos un juego, y volver a su estado normal cuando lo cerramos, optimizando el consumo de energía y la generación de calor. Para activar el de AMD necesitaremos trabajar con el driver del kernel AMDGPU, y en el caso de NVIDIA con el driver privativo y **activando la extensión Coolbits**.


Como comentamos, estos [parches](https://github.com/FeralInteractive/gamemode/pull/101) son experimentales y es necesario realizar bastantes pruebas antes de darle el visto bueno. **Si quereis utilizarlos, como siempre es necesario que lo hagais dentro de unos valores seguros y probados previamente**,  y por supuesto bajo vuestra responsabilidad.


¿Usais normalmente Gamemode? ¿Qué os parece que se incluya el tema del Overclock en este desarrollo? Deja tu opinión en los comentarios o charla sobre este tema en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

