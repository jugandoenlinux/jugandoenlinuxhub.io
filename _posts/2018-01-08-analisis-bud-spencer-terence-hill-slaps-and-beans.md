---
author: leillo1975
category: "An\xE1lisis"
date: 2018-01-08 19:00:09
excerpt: "<p>Recordamos sus pel\xEDculas con este divertido juego.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6d7013048d0c128a879704427738873c.webp
joomla_id: 573
joomla_url: analisis-bud-spencer-terence-hill-slaps-and-beans
layout: post
tags:
- bud-spencer
- terence-hill
- slaps-and-beans
- trinity-team
- retro
title: "An\xE1lisis: Bud Spencer & Terence Hill - Slaps And Beans."
---
Recordamos sus películas con este divertido juego.

Los que visitais asiduamente esta web sabreis que hace ahora un año [comenzamos a seguirle la pista](index.php/homepage/generos/arcade/item/282-bud-spencer-terence-hill-slaps-and-beans-llegara-a-gnu-linux) a este trabajo de la desarrolladora italiana [Trinity Team](http://www.trinityteamgames.com/). Como comentábamos en nuestro primer artículo sobre el juego, se trata de un título que rinde homenaje a estos dos actores italianos y sus largometrajes. Los que ya teneis unos cuantos añitos los conocereis de sobra, y sabreis que [Bud Spencer](https://es.wikipedia.org/wiki/Bud_Spencer) (Carlo Pedersoli) y [Terence Hill](https://es.wikipedia.org/wiki/Terence_Hill) (Mario Girotti) protagonizaron un buen puñado de ya [míticas películas](http://yofuiaegb.com/las-mejores-peliculas-de-bud-spencer-y-terence-hill/) sobre todo en las décadas de los 70 y 80, donde mezclaban la acción con el humor a partes iguales.


El juego está desarrollado con el motor **Unity3D**,  y gracias a ello ha podido ser lanzado simultaneamente en Windows, Mac y GNU-Linux. Antes de continuar es necesario aclarar que se encuentra en **Acceso Anticipado**, por lo que este detalle debe ser tenido muy en cuenta por los lectores, ya que este estado en el que se encuentra el juego puede condicionar en cierta medida la elaboración de este artículo. Algo de lo que escribamos aquí puede ser no aplicable a la versión final, que en principio se espera para principios de este recien estrenado 2018. Conviene resaltar también que desde el lanzamiento en Steam hasta ahora **ya han hecho unas cuantas actualizaciones** que han corregido pequeños problemas del juego y añadido algunas mejoras. De todas formas, es justo decir que la situación en la que se encuentra el desarrollo, no condicionará mucho este análisis, pues en este momento se puede disfrutar con una experiencia que a nuestro juicio pensamos no diferirá mucho de la versión definitiva.


![SlapsBeansMenu](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisSlapsAndBeans/SlapsBeansMenu.webp)


*En el menú podremos configurar las opciones gráficas, de sonido, colores de la ropa, idioma, teclas, etc*


Comencemos pues con el análisis en si mismo. Si recordais en la [entrevista](index.php/homepage/entrevistas/item/301-entrevista-con-trinity-team-bud-spencer-terence-hill-slaps-and-beans) que publicamos al poco de conocer la existencia del proyecto, **Marco Agricola**, de Trinity Team, nos desgranaba algunos detalles interesantes sobre el juego, donde destacaba su predilección sobre juegos como **Golden Axe** o **Batman Returns**, y de esas influencias nace este título que recoge toda la esencia de los arcades de scroll lateral de los 80 y 90, tanto en mecánicas como en aspecto. El juego, dibujado a golpe de sprite, nos va a recordar inmediatamente juegos como **Final Fight, Double Dragon o Streets of Rage**. Por supuesto incluye ciertas novedades jugables que lo diferencian y actualizan, y que resaltaremos más adelante.


Durante el juego iremos recorriendo fases que nos llevarán una a una a lo largo de las películas más importantes de su filmografía, todo ello usando una historia que será el hilo conductor entre ellas. Al poco de empezar **se nos presentará el juego como si de una película se tratase** y comenzaremos a repartir mamporros en el lejano oeste. A lo largo de este nos iremos moviendo entre los diferentes escenarios , genialmente inspirados y diseñados en sus peliculas, por lo que encontrar "localizaciones" de  "Le llamaban Trinidad", "Dos Super Esbirros" o "Y si no, nos enfadamos", entre otras, será la tónica general. Todo ello está aderezado con el **particular humor** del que hacían gala en los largometrajes, tanto a nivel de lo representado gráficamente, como en los diálogos.


![SlapsBeansInicio](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisSlapsAndBeans/SlapsBeansInicio.webp)


*El juego además de tratar de sus películas, tiene un aire muy cinematográfico.*


En cuanto a la jugabilidad, no difiere en ningún modo con lo que se espera de este tipo de juegos, aunque si **introduce algunas novedades interesantes**. Por ejemplo, el juego necesita de 5 teclas básicas diferentes para ser jugado, lo que se trata de un número considerable si lo comparamos con los dos o tres botones habituales, pero cierto es que los mandos actuales permiten usarlas con mucha más facilidad que antes. Podremos pues golpear de forma normal, golpear fuerte, defendernos, correr y agarrar enemigos e items. Por supuesto podremos realizar combinaciones de teclas para realizar movimientos especiales. En cuanto al golpe fuerte, podremos realizar 3 seguidos, pues cada uno que hacemos consume una barra que debemos rellenar a medida que vamos dando cachetes. Si nos coordinamos con nuestro compañero durante la partida para golpear a los enemigos podremos hacer combos que multiplicarán los puntos que conseguimos. **Podremos también hacer uso del entorno** pudiendo recoger elementos del suelo tanto para "alimentarnos" (cervezas, habas...), como para golpear a los enemigos, tales como palos, sartenes, sillas, jarras de cerveza.... incluso podremos golpear con los propios enemigos. También existen elementos movibles en el escenario que pueden servirnos en determinadas ocasiones.


Una cosa que llama mucho la atención es que **podremos alternar los personajes cuando queramos** durante la partida y jugar con el que más nos plazca o nos beneficie en el momento que estimemos necesario. Esto, aparte de sernos muy útil durante las peleas, nos permitirá resolver ciertas situaciones que ocurren durante el juego y que será necesario realizar para poder seguir avanzando. Podremos, por ejemplo usar la fuerza de Bud para mover objetos pesados en el escenario, o la agilidad de Terence para trepar en ciertos lugares. Por supuesto, este último punto es así si jugamos solos, ya que **el juego permite jugar con un compañero en el mismo ordenador**, lo cual no hemos probado, pero seguro que puede ser muy divertido.También es digno de mención la cantidad de **minijuegos** que incluye y que consiguen hacer más rico y variado el disfrute de [Slaps & Beans](http://www.slapsandbeans.com/). Ejemplos de esto son los "Slaps Burst" donde entraremos en una casa, habitación, tienda... y alternando teclas tendremos que dar leña a los enemigos hasta vaciar una barra. También tendremos otros en los que haremos punteria, conduciremos coches o tiraremos fruta a los enemigos. En ciertos momentos tendremos que enfrentarnos a "**Jefes**" que nos harán variar la forma de afrontar la situación, teniendo que realizar otro tipo de tareas para derrotarlos.


![SlapsBeansBuggie](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisSlapsAndBeans/SlapsBeansBuggie.webp)


*Los minijuegos hacen más rico y variado a Slaps and Beans*


Me gustaría resaltar el **excelente trabajo de las animaciones y coreografías** que veremos en el juego. Es este quizás el punto que más nos recuerda a sus películas, más incluso que los escenarios o las situaciones. Los golpes están genialmente recreados y la variedad de ellos es enorme. Es un placer, por ejemplo ver el aplauso o el puñetazo en el hombro de Bud, o los esquives y el pisotón de Terence, entre muchos otros movimientos. La tarea de reflejar todos estos movimientos y "bailes" es realmente encomiable y es bien seguro que han invertido gran cantidad de tiempo en ello, pero el resultado final es magnífico. Graficamente,por supuesto, permite resoluciones actuales, aunque para ello no se sacrifica el aspecto clásico de este tipo de arcades.


 En cuanto al apartado sonoro, el juego **incluye un buen catálogo de sonidos de golpes**, tomados directamente de sus films que adornan la acción de forma sobresaliente. También es digno de mención el **uso de la música**, ya que no son versiones pasadas a formato midi, como es habitual, si no que está **sacada directamente de las bandas sonoras de sus peliculas**. Reconoceremos enseguida, por ejemplo, la música de "Y si no, nos enfadamos" o "Le llamaban trinidad", transportandonos inmediatamente a dichas películas.


![SlapsBeansJefeZancudo](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisSlapsAndBeans/SlapsBeansJefeZancudo.webp)


Para poder superar a los jefes tendremos que modificar la forma habitual de afrontar a los enemigos comunes


Obviamente **no todo va a ser positivo**, pues el juego, a día de hoy tiene ciertos errores, que **probablemente sean subsanados con parches antes de llegar a su versión final**. Por poner ejemplos, en un par de ocasiones se cerró inesperadamente (podeis verlo en el video de abajo), y lo que es peor, se perdió todo el avance que habíamos hecho hasta el momento, lo cual es bastante molesto. También algunos enemigos se quedan a veces "trabados" en determinados lugares y parecen no saber salir de ahí. Incluso en alguna ocasión, jugando con Bud, Terence se quedó intentando recoger una cerveza del suelo sin conseguirlo. En otro momento los minijuegos no respondían correctamente, como en la carrera de coches que se ralentizaba sin ningún tipo de explicación, pues en este tipo de juegos la carga del sistema no es un problema. Como dígo, son errores que probablemente sean corregidos mediante actualizaciones.


Pero también hay algunas cosas que no se pueden cambiar pues forman parte del ADN del juego, y es que, en ciertos momentos Slaps and Beans **puede pecar de repetitivo**, pues los enemigos, a pesar de tener múltiples apariencias, no son muy variados en cuanto a su forma de pelear. Si quitamos los jefes finales, **parecen todos cortados por el mismo patrón**. Estaría bien, por ejemplo, que hubiese esbirros que lanzasen objetos como puñales, alguno más rápido y agil que fuese más dificil de alcanzar, o el típico gigantón con más fuerza y puntos de vida. El número de enemigos a nuestro juicio es demasiado elevado, y creemos que sería mejor que fuese menor, pero teniendo que emplearnos más con cada uno.


También sería adecuado disponer de una barra de vida al golpear a los enemigos comunes para hacernos una idea de cuanto les queda para derrotarlos. En cuanto a la parte multijugador como dijimos arriba solo podemos hacerlo en el mismo ordenador, y **no estaría de más poder completar fases con un amigo a través de internet**. Algo que también puede molestar un poco es **la música del juego, que se puede hacer repetitiva** al poner la misma canción varias veces en la misma fase. Aun así, los puntos positivos del juego superan ampliamente a estos últimos, por lo que estos dos últimos párrafos no deben desanimaros si realmente os gustan este tipo de juegos.


![SlapsBeansSuper](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisSlapsAndBeans/SlapsBeansSuper.webp)


*Los escenarios del juego son coloridos y están muy bien dibujados*


Como conclusión final, en JugandoEnLinux os aseguramos que Bud Spencer & Terence Hill: Slaps and Beans es un juego que **va a gustar tanto a los fans más acérrimos de estas películas, como a todos aquellos que disfrutan de los clásicos arcades de scroll lateral y juegos retro**. Trinity Team ha realizado con mucho cariño un bonito homenaje a estos dos grandes actores que tantos buenos momentos nos han hecho y hacen pasar con sus películas. Esperemos pronto tener nuevas noticias para informaros de los próximos proyectos de esta compañía italiana.


 En JugandoEnLinux grabamos el día de su lanzamiento en Acceso Anticipado un gameplay para [nuestro canal de Youtube](https://www.youtube.com/channel/UC4FQomVeKlE-KEd3Wh2B3Xw). En el podreis apreciar y entender muchas de las cosas que comentamos en este análisis. También nos gustaria aclarar que se grabó ya hace casi un mes y desde entonces el juego se ha actualizado en varias ocasiones corrigiendo muchos de los problemas que podemos ver el video. Podeis verlo aquí:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/9_lcMgGSgtA" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


  Podeis comprar Bud Spencer & Terence Hill: Slaps and Beans (Acceso Anticipado) en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/564050/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


...Y tu que, ¿estás dispuesto a soltar unos cuantos puñetazos? ¿Te apetece viajar al pasado y revivir sus películas? Déjanos tus impresiones sobre este juego en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


**NOTA:** La elaboración de este análisis está realizada gracias a la copia facilitada por [Trinity Team](http://www.trinityteamgames.com/). Tambén nos gustaría decir que JugandoEnLinux os informará debidamente cuando salga la versión final del juego, así como de sus cambios y correcciones definitivas al salir del acceso anticipado.

