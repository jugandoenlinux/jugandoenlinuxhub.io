---
author: Pato
category: Hardware
date: 2022-01-25 17:50:04
excerpt: "<p>A poco m\xE1s de un mes de la llegada de la m\xE1quina a los usuarios,\
  \ Valve acelera los anuncios en torno a su sistema</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/Steamdeckbatch.webp
joomla_id: 1413
joomla_url: novedades-steam-deck-la-ultraportatil-de-valve-suma-titulos-y-facilita-el-soporte-anti-trampas
layout: post
tags:
- steamos
- hardware
- easy-anti-cheat
- steam-deck
- anti-cheat
- anti-trampas
title: "Novedades Steam Deck: La ultraportatil de Valve suma t\xEDtulos y facilita\
  \ el soporte anti-trampas"
---
A poco más de un mes de la llegada de la máquina a los usuarios, Valve acelera los anuncios en torno a su sistema


Volvemos a hablar de Steam Deck, ya que la portatil de Valve está ya muy cerca de llegar a los primeros afortunados que consiguieron reservarla y ahora se van sucediendo los anuncios con cada vez más frecuencia.


Comenzamos con el anuncio de que el conocido sistema anti-trampas **Easy Anti-Cheat** (EAC) es ahora mucho más fácil de implementar en los juegos que quieran ofrecer soporte bajo Proton, ya que como sabemos este sistema anti trampas ya estaba disponible para poderse utilizar, pero su implementación era cuanto menos farragosa, ya que había que utilizar una versión específica del sistema y algunas verificaciones y archivos.


Ahora, con el [anuncio de Valve sobre la actualización](https://steamcommunity.com/groups/steamworks/announcements/detail/3137321254689909034) del soporte para EAC, los de Bellevue afirman que **el soporte se amplía a todos los SDK's y todas las versiones, sin necesidad de implementar siquiera archivos específicos**. De esta forma, desde ayer ya se están enviando los datos de las pruebas de los juegos que utilizan sistemas anti trampas de cara al programa de **Steam Deck Verified**. Ahora ya dependerá de los propios desarrolladores el integrar este soporte en los juegos para poder correr bajo la Deck títulos que hasta ahora no era posible jugar como Rust, Battlefield o Halo. Puedes ver los requisitos para implementar tanto EAC como Battleye para Proton en la página web dedicada a estos sistemas en la [página de steamworks para desarrolladores](https://partner.steamgames.com/doc/steamdeck/proton).


A raiz de esto, y continuando con el programa de verificación para Steam Deck, ya han comenzado a salir los primeros listados de juegos bajo dicho programa. En la conocida página sobre minado de datos de Steam [steamdb.com ya hay listados](https://steamdb.info/instantsearch/?refinementList%5Boslist%5D%5B0%5D=Steam%20Deck%20Verified) un buen número de juegos en los que aparece el icono de Steam Deck Verified, entre ellos algunas sorpresas con juegos que si bien no tienen versiones nativas si tendrán soporte mediante dicho programa de verificación. Como ejemplos: **Dishonored, Cuphead** o **Sekiro**. En el momento de escribir estas líneas hay 42 juegos es esta lista.


En otro orden de cosas, En la [última actualización](https://steamcommunity.com/games/1675180/announcements/detail/3122683923029138793) de Valve sobre la Steam Deck, afirman que todo va según lo previsto y que las máquinas comenzarán a llegar a los compradores en las fechas estipuladas. Estaremos atentos a las informaciones que vayan surgiendo al respecto.


¿Qué títulos esperas que estén disponibles para Steam Deck próximamente? ¿Te sorprende ver algún título concreto en la lista de Steam Deck Verified?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

