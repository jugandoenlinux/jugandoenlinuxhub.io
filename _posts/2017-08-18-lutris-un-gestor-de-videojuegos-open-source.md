---
author: Serjor
category: Editorial
date: 2017-08-18 16:38:12
excerpt: <p>El calibre de los videojuegos</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c910db2cadeb7dd44121f01e6d7b155d.webp
joomla_id: 437
joomla_url: lutris-un-gestor-de-videojuegos-open-source
layout: post
tags:
- steam
- lutris
title: Lutris, un gestor de videojuegos open source
---
El calibre de los videojuegos

Es de suponer que cómo linuxeros que somos, muchos conoceréis qué es Lutris, pero por si acaso, aquí va un breve artículo para explicarlo.


[Lutris](https://lutris.net/about/) es una plataforma de código abierto para gestionar nuestra biblioteca de videojuegos, con independencia de su origen. De alguna manera podemos compararlo con Steam, aunque no es del todo justo para esta última, ya que Lutris es más ambicioso.


Actualmente (todavía no han llegado a la versión 1.0), tras darnos de alta en la web, podemos logearnos en el cliente y tendremos la biblioteca sincronizada. De esta manera podemos navegar por la lista de juegos disponibles e instalarlos desde la web.


![lutris juegos resized](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Lutris/lutris-juegos_resized.webp)


Cierto es que esto último a mí no me ha funcionado porque en mi caso Firefox no reconoce el protocolo, pero de hacerlo se podría instalar directamente desde ahí. Para salir del paso con añadirlo a la biblioteca de juegos y luego instalarlo desde el cliente es suficiente, aunque menos cómodo.


![lutris instalar web resized](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Lutris/lutris-instalar-web_resized.webp)


También permite conectarnos a nuestra cuenta de Steam (para ello nuestro perfil de usuario debe estar en modo público, aunque sea solo durante la conexión de las cuentas), y una vez sincronizados ambos servicios, podemos jugar desde Lutris a casi cualquier juego de la biblioteca, ya sea para GNU/Linux o para Windows, porque sí, Lutris es capaz de instalar y ejecutar juegos de Windows para Steam. Lo hace a través de Wine, así que estaremos limitados por el soporte que Wine nos ofrezca, pero allí donde funcione sin problemas, es muy cómodo.


Para gestionar la biblioteca de juegos tenemos lo que llaman "Runners", que por lo que he podido entender son "motores" y entornos para poder ejecutar los juegos, por ejemplo Wine, MAME, Dolphin... Y una larga lista de emuladores que no conocía. Eso sí, para poder jugar a según qué juegos deberemos ser nosotros quienes proporcionemos el ejecutable del juego, ya que lo que Lutris no hace es facilitarlos. Si es Open Source nos lo descargará por nosotros, pero sino, tocará indicar dónde está la ROM o lo que que toque durante el proceso de instalación.


![lutris runners resized](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Lutris/lutris-runners_resized.webp)


Por ejemplo esta noche algunos miembros de la comunidad jugaremos a [Xonotic](http://xonotic.org/) (pásate por nuestro canal de Telegram para apuntarte), así que en mi caso lo he instalado desde Lutris sin tener que hacer nada especial, tan solo añadirlo a mi biblioteca y darle a instalar, nada de ppas o ir a la web de Xonotic a buscar el instalador para tener la última versión.


Lo que no tengo del todo claro es cómo gestiona las actualizaciones de aquellos juegos que instalamos. Los que sean de Steam supongo que el propio cliente de Steam será quién los actualice, pero en el caso de Xonotic o 0ad que son los que tengo vía Lutris no lo tengo claro, aunque como es él quién los ha instalado, supongo que sí.


Y "poco" más, entre sus planes de futuro están:


* Soportar los juegos que tengamos en nuestras cuentas de Humble Bundle y GOG (ahora hay que descargar los juegos a mano e instalarlos desde el disco duro)
* Gestión de las partidas guardadas
* Chat, listas de amigos, planificador de eventos
* Interfaz gráfica para la configuración de Joysticks


Esperemos que el proyecto avance y evolucione cómo dicen, porque la verdad es que poder gestionar toda nuestra biblioteca de videojuegos, independientemente de su origen, es genial, y aunque tiene algún que otro bug, cómo por ejemplo al cerrar un juego de Steam se queda consumiendo CPU pensando que el juego sigue ejecutándose cuando no es así, en su estado actual es funcional y desde luego recomendable.


Si os interesa podéis encontrar las instrucciones para instarlo en función de la distribución que uséis [aquí](https://lutris.net/downloads/).


Y tú, ¿Conocías Lutris? Cuéntanos qué es lo que más te gusta de este gestor de juegos en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

