---
author: Pato
category: Rol
date: 2017-01-25 15:07:28
excerpt: "<p>El juego ya super\xF3 su campa\xF1a en Greenlight. Esperan lanzar el\
  \ juego a comienzos del a\xF1o que viene</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/63955aa9869cf7707ada1662dbfb31e2.webp
joomla_id: 191
joomla_url: pixel-princess-blitz-un-sandbox-rpg-de-accion-con-un-impresionante-aspecto-artistico-esta-en-campana-en-kickstarter
layout: post
tags:
- accion
- indie
- rol
- kickstarter
- roguelike
title: "'Pixel Princess Blitz' un \"sandbox RPG de acci\xF3n\" con un impresionante\
  \ aspecto art\xEDstico est\xE1 en campa\xF1a en Kickstarter"
---
El juego ya superó su campaña en Greenlight. Esperan lanzar el juego a comienzos del año que viene

En la habitual ronda buscando nuevos desarrollos que lleguen a Linux he topado con la [campaña de Greenlight](http://steamcommunity.com/sharedfiles/filedetails/?id=771616134) ya finalizada de este 'Pixel Princess Blitz' [[web oficial](http://pixelprincessblitz.com/)]. Enseguida me puse a investigar por lo llamativo de su apartado artístico y se trata de un juego de tipo "Rol y acción mezclado con roguelike y sandbox" con un enfoque táctico muy interesante en un mundo de fantasía lleno de aventuras donde tendrás que sobrevivir frente a multitud de peligros con un vasto arsenal de armas dentro de una historia dinámica. Juzga por ti mismo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/EeBVzV5rWb0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Volviendo a lo que nos interesa, en los [comentarios de su campaña](http://steamcommunity.com/sharedfiles/filedetails/comments/771616134) un usuario preguntó por el soporte a Linux, a lo que el estudio respondió con contundencia:


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Externo/PPB.webp)



> 
> mindedie: ... Y el soporte en Linux es real o solo "lo lanzaremos en todas las plataformas de Steam" pero al final no tendremos nada, ni siquiera una peineta.
> 
> 
> lanzegames: Muchas gracias, mindedie! El soporte en Linux es real, no solo una promesa vacía!
> 
> 
> 


Ya en harina, buscando en twitter di con su anuncio de la campaña de Kickstarter:



> 
> Pixel Princess Blitz' [#Kickstarter](https://twitter.com/hashtag/Kickstarter?src=hash) has launched! Please [#retweet](https://twitter.com/hashtag/retweet?src=hash), share the news and back us! <https://t.co/GKmEcaQrm3>[#indiedev](https://twitter.com/hashtag/indiedev?src=hash) [#gaming](https://twitter.com/hashtag/gaming?src=hash) [pic.twitter.com/gwl1EYnXta](https://t.co/gwl1EYnXta)
> 
> 
> — Hepari (PPB Dev) (@hepari00) [11 de enero de 2017](https://twitter.com/hepari00/status/819122324393885697)



En los comentarios de la campaña dejan bien claro el soporte a Linux. Puedes verlo aquí:


<div class="resp-iframe"><iframe height="420" src="https://www.kickstarter.com/projects/lanzegames/pixel-princess-blitz-sandbox-roguelike-action-rpg/widget/card.html?v=2" width="220"></iframe></div>


 Como puedes ver en estos momentos a falta de 14 días para finalizar la campaña tienen muchas posibilidades de conseguir su objetivo. A mi desde luego la propuesta de este Pixel Princess Blitz me parece muy interesante.


¿Qué te parece a ti la propuesta de este 'Pixel Princess Blitz'? ¿Aportarás a su campaña?


Cuéntamelo en los comentarios, en el foro o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

