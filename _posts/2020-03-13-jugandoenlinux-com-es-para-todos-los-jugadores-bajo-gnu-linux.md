---
author: Jugando en Linux
category: Editorial
date: 2020-03-13 10:47:38
excerpt: ''
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/headers/Proyectologo.webp
joomla_id: 1189
joomla_url: jugandoenlinux-com-es-para-todos-los-jugadores-bajo-gnu-linux
layout: post
title: Jugandoenlinux.com es para todos los jugadores bajo GNU/Linux
---

 


Después de una amplia reflexión, los integrantes de jugandoenlinux.com hemos llegado a la conclusión de que quizás sería necesario escribir este editorial con el fin de aclarar nuestra posición respecto al juego en Linux.


Desde que fundamos jugandoenlinux.com tuvimos claro que debería ser un instrumento para fomentar y promover el videojuego en GNU/Linux en general y en su total amplitud, y en ese propósito no nos íbamos a imponer ningún límite. 


También tenemos claro que en nuestra comunidad debe primar la libertad, siempre **con el respeto a la postura personal de cada uno y siempre desde el respeto a los demás**, para lo cual debemos poner límites a nuestros propios comentarios. 


Desde jugandoenlinux.com y su comunidad debemos esforzarnos por respetar a los jugadores que juegan desde plataformas de distribución al igual que los que juegan en streaming. Los jugadores que prefieren los juegos libres y los jugadores que no tienen problemas con los juegos privativos. Los jugadores que solo juegan juegos nativos como a los jugadores que juegan a través de capas de compatibilidad, por que al final **todos estamos jugando bajo nuestro sistema favorito**. Y eso es lo que consideramos importante.


Podemos estar de acuerdo o no en si es mejor o peor el jugar de una manera o de otra, y podemos (y debemos) opinar libremente lo que nos parece mejor desde nuestro punto de vista personal, pero **eso no debe ser motivo de crítica, coacción o discriminación**, ya que como expusimos desde un principio en [nuestra normativa de comentarios](index.php/normas-de-uso) **no queremos excluir a nadie** por motivo de su elección o convicción a la hora de jugar, ya sea juegos libres, privativos, nativos o mediante capas. Si tienes un problema con la libertad de los demás a la hora de jugar videojuegos en Linux, quizás este no sea tu sitio.


Desde jugandoenlinux.com os damos a todos la bienvenida y no hacemos distinciones. Sois vosotros, los jugadores en Linux los que hacéis posible que sigamos aquí, y esperamos serviros a todos lo mejor que sepamos y podamos y siempre respetaremos vuestras decisiones.


Feliz Juego en Linux!!


Pato, Leillo y Serjor

