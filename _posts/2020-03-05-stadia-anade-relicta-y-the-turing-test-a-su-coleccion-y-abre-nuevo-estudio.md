---
author: Pato
category: Noticia
date: 2020-03-05 21:16:14
excerpt: "<p>&nbsp;Aventuras y puzzles llegan a la plataforma de streaming de Google</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/Stadia-Relicta-TheTuringTest.webp
joomla_id: 1181
joomla_url: stadia-anade-relicta-y-the-turing-test-a-su-coleccion-y-abre-nuevo-estudio
layout: post
tags:
- stadia
title: "Stadia a\xF1ade Relicta y The Turing Test a su colecci\xF3n, y abre nuevo\
  \ estudio "
---
 Aventuras y puzzles llegan a la plataforma de streaming de Google


Poco a poco, Stadia sigue engordando su catálogo para tratar de hacer mas atractivo su servicio de juego en la nube. 


Esta vez nos anuncian la llegada de dos nuevos juegos de aventuras y puzzles en primera persona como son los interesantes Relicta y The Turing Test.


*Relicta es un juego de rompecabezas basado en la física en el que debes combinar de forma creativa el magnetismo y la gravedad para desentrañar los secretos de la Base Chandra. Solo en las traicioneras profundidades de la Luna, tu mente científica es lo único que puede mantener viva a tu hija.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/77s8JfQH_pg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Algunas características de Relicta:


Mecánica alucinante: modifica la gravedad y el magnetismo a voluntad.  
Rompecabezas físicos complejos: combina tus habilidades de formas innovadoras para resolver acertijos físicos.  
Entornos detallados: cada rincón cuenta una historia, cada nuevo paso es un desafío.  
Historia de fondo profunda y enigmática: encuentra pistas y une detalles sobre la investigación de la anomalía de Relicta.  
Banda sonora original inmersiva: sumérgete en los paisajes sonoros de un thriller psicológico de ciencia ficción.


Por su parte, *The Turing Test es un desafiante juego de rompecabezas en primera persona ambientado en la luna de Júpiter, Europa. Eres Ava Turing, una ingeniera de la Agencia Espacial Internacional (ISA) enviada para descubrir la causa de la desaparición de la tripulación estacionaria.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/I9zeQUGCuH0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Algunas características de The Turing Test:


Aprovecha la capacidad de transferir potencia entre máquinas utilizando tu "Herramienta de manipulación de energía" que proporciona un giro único en la mecánica del juego y la lógica del rompecabezas.  
Aprende la importancia del control del jugador mientras cambias entre múltiples perspectivas para resolver los acertijos más desafiantes.  
Descubre una historia con múltiples capas de profundidad y conspiración; conceptos desafiantes de la moral humana y dar libertad a los jugadores para formar teorías sobre el destino de los miembros de la tripulación de ISA.


Estos dos juegos estarán disponibles para su compra próximamente en la plataforma, aunque sin una fecha concreta. Tienes todos los detalles en el [post del anuncio](https://community.stadia.com/t5/Stadia-Community-Blog/Solve-interstellar-puzzles-in-new-games-coming-soon-to-Stadia/ba-p/16527).


En otro orden de cosas, Google a través de Jade Raymond la responsable de videojuegos y entretenimiento de Stadia ha anunciado la apertura de un nuevo estudio de desarrollo en Playa Vista, California que estará dirigido por Shannon Studstill, mas conocida por haber sido la responsable de desarrollo en el estudio de Sony Santa Mónica responsable de desarrollos como God Of War.


Segçun nos cuenta, el nuevo estudio se centrará en desarrollar juegos exclusivos para la plataforma, aunque no ha dado pistas sobre ningún desarrollo concreto.


Veremos a ver qué novedades nos traen próximamente que sean interesantes para poder jugar desde nuestro sistema favorito.


Puedes ver el anuncio [en este enlace](https://www.blog.google/products/stadia/games-entertainment-studio-playa-vista/).

