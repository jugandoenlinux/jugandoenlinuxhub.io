---
author: Serjor
category: Puzzles
date: 2018-01-05 17:31:49
excerpt: <p>El estudio croata no deja de dar buenas noticias</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/839998e06c2f41419e3797450bd73178.webp
joomla_id: 595
joomla_url: the-talos-principle-sera-portado-al-motor-serious-fusion
layout: post
tags:
- croteam
- fusion
- the-talos-principle-vr
- the-talos-principle
title: "The Talos Principle ser\xE1 portado al motor Serious Fusion"
---
El estudio croata no deja de dar buenas noticias

Nos levantábamos esta mañana leyendo en [gamingonlinux](https://www.gamingonlinux.com/articles/the-talos-principle-going-fusion-croteam-dropping-opengl-serious-sam-4-still-coming.10996) que [Croteam](http://www.croteam.com/), uno de los estudios que más mima el mercado linuxero, sigue en sus trece de hacer las cosas bien y no estar aquí solamente por el dinero.


Y es que resulta que en los foros de steam han [respondido](http://steamcommunity.com/app/257510/discussions/0/412447331651720139/?ctp=35#c1621724915798233573) a una serie de preguntas hechas por los usuarios, y entre otras, han comentado por un lado que The Talos Principle, un juego que nació usando OpenGL, después lo actualizaron para usar Vulkan, y por último le [añadieron](index.php/homepage/generos/puzzles/item/614-lanzado-the-talos-principle-vr-con-soporte-para-gnu-linux-steamos) soporte para VR, será también portado al motor Fusion, que [recordemos](index.php/homepage/generos/accion/item/509-serious-sam-3-llega-a-serious-fusion-2017) es el que usa ahora también Serious Sam 3 y sobre el que correrá Serious Sam 4.


Además también han comentado que Fusion dejará OpenGl de lado para centrarse al 100% en Vulkan, aunque para Xbox usarán DX12.


La verdad, es que el que haya estudios que cuidan a los usuarios y siguen actualizando sus juegos a pesar de que ya lleven tiempo en el mercado, es siempre una gran noticia, y mejor cuando encima hay soporte para GNU/Linux detrás.


Y a ti, ¿qué te parece este movimiento? Déjanos qué te parecen estas actualizaciones en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

