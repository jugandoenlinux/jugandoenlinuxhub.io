---
author: Pato
category: Rol
date: 2018-03-16 09:20:23
excerpt: "<p>El juego lleg\xF3 ayer para Windows y est\xE1 confirmado para nuestro\
  \ sistema favorito</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9e9e1351d6a763151a8613c1dd10d427.webp
joomla_id: 682
joomla_url: soulblight-un-juego-de-rol-y-accion-con-un-enfoque-distinto-llegara-a-linux-proximamente
layout: post
tags:
- accion
- indie
- proximamente
- rol
title: "'Soulblight' un juego de rol y acci\xF3n con un enfoque distinto llegar\xE1\
  \ a Linux pr\xF3ximamente"
---
El juego llegó ayer para Windows y está confirmado para nuestro sistema favorito

'Soulblight' es otro interesante desarrollo en el que el enfoque del rol y acción para un jugador se centra en tus elecciones durante el juego, que fue lanzado ayer mismo para Windows y que según los desarrolladores comenzarán a trabajar en la versión Linux tan pronto como terminen con el trabajo relacionado con el lanzamiento de ayer, tal y como reconocen en un hilo [en sus foros de Steam](http://steamcommunity.com/app/530930/discussions/0/1495615865220641730/?tscn=1521136716#c1697167803862780605).


*Soulblight es un roguelike oscuro, implacable, en vista isométrica que te atrae para cruzar la delgada línea entre el bien y el mal. En lugar de puntos de experiencia, introduce una Mecánica de características. Al viajar a través de la realidad destrozada del Santuario, recibirás rasgos de personalidad basados en tus elecciones. Esta será la principal fuente de tu fortaleza. Para aprovecharlos, deberás actuar en consecuencia. Convertirte en un alcohólico significa que serás recompensado por emborracharte, pero desde ese punto estar sobrio puede resultar insoportable.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/ExotFeAmVGU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 En cuanto a las características:


* Mecánicas únicas basadas en características para estimular el juego de rol
* Montones de armas, objetos y utilidades
* Combate táctico dinámico construido sobre la gestión de Stamina y el control de distancias
* Aproximación opcional en sigilo que complementa a la lucha
* Niveles visualmente impactantes generados aleatoriamente
* Nivel de dificultad alto enfatizando en mecánicas de muerte permanente
* Jugabilidad basada en la narrativa


Antes de ingresar a cada nivel del Santuario, puedes elegir una característica que represente un determinado rasgo de personalidad. Esa elección dará forma a tus experiencias futuras a medida que cada Característica desbloquee una nueva forma de ganar poder. Algunos ejemplos:  



* Codicioso: cuanto mayor sea la fortuna que adquieras, más fuerte serás. Recibe una sanción por pagar en exceso.
* Acaparador: cuanto más variadas sean tus pertenencias, mayor será tu bonificación pasiva. Recibe la penalización de "Sensación de perder" después de perder un objeto.
* Feroz: recibe impulso de "rabia" después de haber sido herido. Recibe una bonificación pasiva permanente por matar enemigos.
* Deseo: recibes una sanción "sobria" si no bebes alcohol. En lugar de "Inebriated" recibes un impulso "Drunk" más poderoso.


'Soulblight' aún no tiene fecha de publicación ni requisitos mínimos para Linux/SteamOS, pero estaremos atentos a las noticias que surjan al respecto y os mantendremos informados. Si quieres saber más detalles, puedes visitar su [página web oficial](https://www.mynextgames.com/).


*Gracias por el aviso [@Nevertheless](https://twitter.com/G4LNevertheless)*

