---
author: Pato
category: "Acci\xF3n"
date: 2018-01-18 17:02:59
excerpt: "<p>Pronto veremos el juego en nuestras m\xE1quinas.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2c3125eb8597b442fad87e2c031423e6.webp
joomla_id: 611
joomla_url: ruiner-sigue-en-desarrollo-para-gnu-linux-steamos
layout: post
tags:
- accion
- indie
- proximamente
- gore
- violento
title: '''RUINER'' sigue en desarrollo para GNU-Linux/SteamOS (ACTUALIZADO)'
---
Pronto veremos el juego en nuestras máquinas.

**ACTUALIZACIÓN 22-3-18**: En los [foros de Steam](http://steamcommunity.com/app/464060/discussions/0/154644928862239593/?tscn=1521731427) los desarrolladores de este llamativo juego acaban de confirmar que si todos va según lo esperado, "Ruiner"  **llegará a Linux/SteamOS el próximo mes de Abril**, aunque no han dado el día exacto. Confiemos que todo vaya bien y pronto podamos tener la posibilidad de jugarlo en nuestros equipos.


 




---


**NOTICIA ORIGINAL**: Hace ya tiempo que esperamos confirmar el lanzamiento de 'RUINER' para Linux/SteamOS, sin embargo aún no podemos dar esa noticia. Tal y como [ya anunciamos en su momento](index.php/homepage/generos/accion/item/594-ruiner-llegara-a-linux-tras-su-lanzamiento-en-consolas), 'RUINER' sería lanzado en Linux una vez que se hubiese lanzado en consolas. Tras ese anuncio han pasado varios meses durante los cuales los desarrolladores declararon [tener dificultades con el port](http://steamcommunity.com/app/464060/discussions/0/154644928862239593/#c1473095331490571530), e incluso que no estaban trabajando en el, pidiendo poco después "[muestras de interés](http://steamcommunity.com/app/464060/discussions/0/154644928862239593/#c2765630416812607952)" en el juego a los usuarios de Linux.


Ahora, gracias a [Gaming on Linux](https://www.gamingonlinux.com/articles/the-developer-of-ruiner-confirms-the-linux-version-is-being-worked-on-now.11057) tenemos una nueva confirmación de manos de los desarrolladores, gracias a un tweet:


 



> 
> We have. It's work in progress. Can't say anything more.
> 
> 
> — RUINER (@ruinergame) [17 de enero de 2018](https://twitter.com/ruinergame/status/953429503447130112?ref_src=twsrc%5Etfw)



 *"Estamos trabajando en ello. No podemos decir nada más"*. Escueta respuesta que vuelve a confirmar que RUINER sigue en los planes de lanzamiento para nuestro sistema favorito... o puede que no. ¿Tu que piensas?


RUINER [[web oficial](http://ruinergame.com/)] es un emocionante y brutal juego de disparos ambientado en el año 2091 en la cibermetrópolis de Rengkok. Un sociópata trastornado se rebela contra el corrupto sistema para rescatar a su hermano secuestrado y descubrir la verdad guiado por una misteriosa amiga hacker.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/XmfMkdfrnKY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 En Jugando en Linux seguiremos tras la pista de este 'RUINER' y os informaremos de las novedades que surjan sobre el juego... si las hay.


¿Que te parece 'RUINER' y su "historial de desarrollo"?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

