---
author: Pato
category: Hardware
date: 2020-06-03 21:38:04
excerpt: "<p>Se estima que el resto de unidades podr\xE1n llegar durante el verano</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/Atari-VCS-Family.webp
joomla_id: 1230
joomla_url: las-primeras-500-unidades-de-la-atari-vcs-llegaran-a-sus-destinatarios-a-mediados-de-junio
layout: post
tags:
- atari
- atarivcs
title: "Las primeras 500 unidades de la Atari VCS llegar\xE1n a sus destinatarios\
  \ a mediados de Junio"
---
Se estima que el resto de unidades podrán llegar durante el verano


En un nuevo comunicado sobre el estado de desarrollo de la **"nueva" consola con corazón Linux** de **Atari**, la compañía nos anuncia que a pesar del confinamiento y las dificultades por la pandemia que padecemos la primera tirada de 500 unidades de su consola **Atari VCS** ya estarían en producción y se estima la llegada a sus destinatarios para mediados de este mes de Junio después de un pequeño retraso debido a ciertas partes de plástico que han resultado defectuosas.


Además, parece que **las unidades destinadas a desarrolladores y beta-testers también han llegado a su destino** salvo ciertas excepciones que pudieron ser recuperadas hace unos días y ya se habrían entregado.


Por otra parte, los desarrolladores de Atari han seguido trabajando remotamente durante el confinamiento con lo que el trabajo en la parte de software no ha sufrido apenas retrasos, y según la compañía, el feedback de desarrolladores y beta-testers está siendo muy positivo tanto con los juegos de la propia Atari como de terceras compañías, e incluso con los servicios de Streaming de contenido audiovisual como Netflix o Hulu, o de videojuegos como Antstream. 


Hablando de **streaming de videojuegos**, @Serjor nos dejó caer no hace mucho un comentario acerca del [movimiento que hizo Valve junto a Nvidia al respecto de Steam Cloud Play](index.php/homepage/noticias-news/1224-steam-cloud-play-el-servicio-de-valve-para-jugar-en-la-nube-entra-en-fase-beta) como servicio de videojuegos en la nube para otras empresas, y cabría preguntarse si los chicos de Atari no habrán pensado en aliarse también con Steam para ello... ¡sería una jugada maestra!


Todo esto además sucede cuando **ya se han cumplido dos años** de la exitosa campaña que Atari llevó a cabo en Indiegogo con tal repercusión que llegaron a colapsar los servidores de la campaña de mecenazgo. 


Por último, uno de los miembros del equipo de Atari ha estado grabando algunos vídeos donde puede verse la consola en acción con diversos juegos y servicios. Os pongo a continuación algunos...


Jugando a **Celeste**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/xY1vtJOdTfI" width="560"></iframe></div>


 


Jugando a **Unsung Warriors**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/GDyWrsCV6f8" width="560"></iframe></div>


 


Jugando a **Basketball Classics**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/3VeBHlLb3DM" width="560"></iframe></div>


 


Jugando a **Borderlands 2** (mediante el modo de arranque de otro sistema operativo de la consola):


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/uOW4cGVVHec" width="560"></iframe></div>


 


Jugando a **Call of Duty: Warzone** (mediante el modo de arranque de otro sistema operativo de la consola)


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/a9LODOUQh40" width="560"></iframe></div>


 


Probando los servicios de **Netflix y Disney +**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/lqMOXpLEvBU" width="560"></iframe></div>


 


Si bien es cierto que avisan que algunos de estos juegos se han ejecutado en una Atari VCS modificada con mas RAM y almacenamiento externo, no deja de ser interesante saber de la capacidad de la consola parar mover juegos no tan "indies" con cierta solvencia. Puedes ver toda esta información en el [último post que ha publicado la compañía en Medium](https://medium.com/@atarivcs/an-anniversary-production-updates-and-home-testing-underway-1061dc880703).


Sea como sea, parece que tras varios retrasos, modificaciones, revisiones y polémicas, la Atari VCS esta a la vuelta de la esquina. El tiempo dirá si Atari consigue volver por la puerta grande al mundo del hardware de entretenimiento y consigue estar a la altura de su mítico nombre, o todo queda en un gran fiasco.


Y tu ¿que piensas de la consola Atari VCS? ¿Piensas que tiene posibilidades de triunfar? ¿te gustaría hacerte con una de estas consolas sabiendo que corren bajo un sistema Linux-Debian?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

