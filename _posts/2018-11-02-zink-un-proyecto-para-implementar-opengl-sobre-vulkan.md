---
author: Serjor
category: Noticia
date: 2018-11-02 16:42:56
excerpt: "<p>Collabora presenta un proyecto para dar soporte a OpenGL a trav\xE9s\
  \ de Vulkan</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6783792c7419427295335dc3ce02b000.webp
joomla_id: 889
joomla_url: zink-un-proyecto-para-implementar-opengl-sobre-vulkan
layout: post
tags:
- vulkan
- opengl
- dxvk
- zink
title: Zink, un proyecto para implementar OpenGL sobre Vulkan
---
Collabora presenta un proyecto para dar soporte a OpenGL a través de Vulkan

Esta mañana nuestro compañero Odin nos informaba en nuestro canal de Telegram sobre un proyecto muy interesante que pretende ejecutar OpenGL sobre Vulkan cuando no hay driver OpenGL, un concepto similar a DXVK:







Cómo él se explica mejor que yo:







Leyendo más en detalle el [anuncio de collabora](https://www.collabora.com/news-and-blog/blog/2018/10/31/introducing-zink-opengl-implementation-vulkan/) podemos leer que la idea es poder tener aceleración hardware a través de Vulkan cuando no haya un driver que acceda directamente al hardware y se esté usando gallium para dar soporte a aplicaciones OpenGL.


![zink architecture](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/zink/zink-architecture.webp)


El proyecto está en sus inicios, y la implementación solo llega a OpenGL 2.1, por ejemplo. Además, como el propio autor comenta, en glxgears con Zink obtiene solamente 475fps, mientras que con el driver i965 obtiene 1750fps.


Proyecto prometedor que puede hacer que en el futuro podamos seguir jugando a juegos que están escritos en OpenGL en sistemas sin soporte para él.


Y tú, ¿qué opinas de este proyecto? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

