---
author: leillo1975
category: Estrategia
date: 2017-04-26 07:20:31
excerpt: "<p>Julian Gollop y su estudio b\xFAlgaro Snapshot Games buscan financiaci\xF3\
  n para este interesantisimo proyecto.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/91b1b90c684fd8e5c2ec1b7418ca380f.webp
joomla_id: 298
joomla_url: phoenix-point-interesantisimo-juego-del-creador-de-xcom-esta-en-crowfunding
layout: post
tags:
- crowdfunding
- phoenix-point
- snapshot-games
- julian-gollop
title: "Phoenix Point, interesant\xEDsimo juego del creador de XCOM est\xE1 en crowfunding\
  \ (ACTUALIZADO - Livestream)"
---
Julian Gollop y su estudio búlgaro Snapshot Games buscan financiación para este interesantisimo proyecto.

Siempre me ha gustado la estratégia por turnos, y en especial la sagas XCOM y UFO, y atónito me quedé ayer a última hora cuando vi el video de este juego en [GamingOnLinux.com](https://www.gamingonlinux.com/articles/phoenix-point-from-the-original-creator-of-x-com-is-now-crowdfunding-on-fig.9566). Julian Gollop, el que fuera creador de la XCOM, y David Caye, fundador de Gaming Insiders; nos ha sorprendido con este nuevo proyecto para el que está buscando fondos. Para ello han abierto una campaña en **[FIG](https://www.fig.co/campaigns/phoenix-point)** donde en este momento ya han recaudado cerca de 300000$, lo que supone más de la mitad de lo que necesitan (500000$). Teniendo en cuenta que aun le quedan por delante 42 dias, y lo interesante que parece el proyecto, todo parece augurar que la campaña será un éxito.


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/SnapshotGames.webp)


 


Phoenix Point relata la infestación mundial del virus alienigena Pandora, que ha devastado la vida en la tierra y ha convertido a la mayoría de los humanos en formas de vida peligrosas y abominables. En el juego formas parte de Project Phoenix, una organización secreta durmiente creada para combatir crisis mundiales. Para ello deberás tratar con los pocos humanos que quedan. que están organizados en **tres facciones diferentes**, los Discípulos de Anu, Nuevo Jericó y Synedrion. Cada una de estas tendrá formas diferentes de afrontar la situación por lo que tendrás que elegir a cual apoyar, y como lo que repercutirá en tu relación con las otras. Para ello podrás entablar relaciones diplomáticas, ayudarlas o no hacer incluso nada. En el caso de que elijas esta última opción, el juego serguirá su propio camino sin ti, tomando la iniciativa las otras facciones. Para combatir a los mutados formaremos escuadrones al estilo XCOM, que podremos personalizar y desarrollar. También podremos hacer uso de vehículos y el escenario tendrá elementos que se podrán destruir


 


Con respecto al enemigo, hay que decir que estos irán mutando poco a poco, volviendose más evolucionados e inteligentes según avanzamos, por lo que cada vez será más difícil afrontarlos. Estos incluso podrán robarte tu equipación y armas para su beneficio. Otro aspecto importante con respecto a los enemigos es que **habrá Jefes finales** que prometerán sorprendernos. Pero lo más adecuado para que os hagais una idea de todo lo que os explico es que veais un video del juego:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/iuN9g502cYM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 Ahora vamos a lo que nos interesa. El juego será lanzado a **finales del año 2018** y **tendrá versión para nuestro sistema**, asi como para Mac y Windows. Otro aspecto a destacar es que podrá adquirirse tanto en Steam como en GOG. Si quieres dejar tu impresión sobre este juego puedes utilizar los comentarios de este artículo o usar nuestros canales en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 


**ACTUALIZACIÓN:** Snapshot games nos acaba de confirmar vía email que el juego ha conseguido alcanzar con éxito el objetivo de los 500.000$ para su desarrollo. Esperemos que consigan las dos metas siguientes para dotar el juego de **vehículos conducibles en batallas tácticas** (meta de 650.000$) y una **base movible flotante** (850.000$). Hay que tener en cuenta que aún quedan 35 dias de campaña y el objetivo principal se ha cumplido en poco más de una semana.


 


**ACTUALIZACIÓN 2:** La **[campaña de FIG](https://www.fig.co/campaigns/phoenix-point)** ha terminado finalmente, alcanzando la cifra de 765.948$ al final de esta, pero aun es posible invertir en ella si se quiere. Esto implica que el juego dispondrá de vehiculos en las batallas. Para celebrarlo han hecho un LiveStream hace poco mostrándonos un gameplay de la fase de desarrollo actual del juego, que podeis ver aquí:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/izTGCth6CNk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


FUENTES: [GamingOnLinux.com](https://www.gamingonlinux.com/articles/phoenix-point-from-the-original-creator-of-x-com-is-now-crowdfunding-on-fig.9566), [FIG](https://www.fig.co/campaigns/phoenix-point), [IGN](http://latam.ign.com/phoenix-point/37547/news/el-creador-de-x-com-presenta-un-nuevo-videojuego-de-estrateg)

