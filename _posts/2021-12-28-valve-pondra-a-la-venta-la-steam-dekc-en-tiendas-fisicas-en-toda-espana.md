---
author: Pato
category: Noticia
date: 2021-12-28 18:44:02
excerpt: "<p>El pr\xF3ximo mes de Febrero va a ser movidito</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/steamdeckdevkit1.webp
joomla_id: 1401
joomla_url: valve-pondra-a-la-venta-la-steam-dekc-en-tiendas-fisicas-en-toda-espana
layout: post
title: "Valve pondr\xE1 a la venta la Steam Dekc en tiendas f\xEDsicas en toda Espa\xF1\
  a (INOCENTADA)"
---
El próximo mes de Febrero va a ser movidito


**Actualizado**: Este articulo fue una broma por el dia de los inocentes




---


Según un representante de Vavle, la popular PC-Consola de bolsillo Steam Dekc se podrá adquirir en tiendas físicas en España en próximas fechas. De hecho, ya se ha filtrado que el lanzamiento de la consola será el mismo día en que la mayoría de los que reservaron una unidad mediante Steam recibirán la suya en su domicilio, y será tan pronto como el próximo **29 de Febrero.**


En otros medios se habla de que esto puede ser debido a la pobre acogida que han tenido las reservas de la máquina, por lo que para dar salida al próximo stock acumulado los de Bellevue han decidido poner las unidades sobrantes en las estanterías de conocidas tiendas físicas.


Como es lógico, las primeras tiendas en recibir la consola físicamente serán conocidos markets estadounidenses que dispondrán de stock a finales de Enero, siendo España el primer país europeo en recibir unidades para tiendas físicas, adelantándose a Alemania, Francia y la Republica de Liechtenstain. 


Esperamos poder consegiur una consola en próximas fechas en el supermercado mas cercano para una próxima review. ¡Estar atentos!


Y tu... ¡Te acercarás a tu tienda de confianza para conseguir tu Deck? ¿Que modelo eligirás?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


Fuente: [Enlace](images/Articulos/INCT.JPG)

