---
author: Pato
category: Noticia
date: 2019-06-24 08:22:12
excerpt: "<p>Canonical cede y seguir\xE1 manteniendo las librer\xEDas 32bits necesarias.\
  \ Vamos a explicar la situaci\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3182b2a6002d7c6eae45301adb4cf349.webp
joomla_id: 1067
joomla_url: canonical-anuncio-que-retiraria-las-librerias-de-soporte-de-32-bits-a-partir-de-ubuntu-19-10-pero-ahora-matiza-que-solo-congela-sus-versiones-actualizado
layout: post
tags:
- steam
- gog
- wine
- valve
- ubuntu
- canonical
title: "Canonical anunci\xF3 que retirar\xEDa las librer\xEDas de soporte de 32 Bits\
  \ a partir de Ubuntu 19.10 pero ahora matiza que solo congela sus versiones (Actualizado)"
---
Canonical cede y seguirá manteniendo las librerías 32bits necesarias. Vamos a explicar la situación

**(Actualización 25/06/2019)**


Después de toda la polémica levantada durante todo el fin de semana, Canonical ha salido al paso con un comunicado oficial en el que explican que gracias a las respuestas tanto de la Comunidad, Ubuntu Studio y otras empresas afectadas (se sobreentiende cuales) Ubuntu seguirá manteniendo las librerías necesarias para asegurarse de no "romper" las aplicaciones de sus usuarios y empresas asociadas. En concreto afirman que abrirán un canal de comunicación para que todos los afectados puedan reportar los paquetes que serán necesarios mantener para así asegurarse de no dejar nada en el tintero. Este canal estará disponible tanto antes del lanzamiento de Ubuntu 19.10 como tras el lanzamiento de esta y la versión 20.04.


Eso sí, aún así aseguran que el objetivo a medio-largo plazo es el de retirar estas librerías, para lo cual aseguran que ya están trabajando con todos los actores implicados para estudiar el mejor método para asegurar que en el futuro nada se rompe, ya sea empaquetando los archivos necesarios en Snaps, utilizando contenedores u otro tipo de tecnologías que permitan salvaguardar el funcionamiento del software que necesite ser ejecutado en 32 bits.


Por parte de Valve y Wine aún no se han producido declaraciones, por lo que habrá que ver si este comunicado consigue volver las aguas a su cauce o si por el contrario terminan anunciando el cambio de soporte oficial a alguna otra distribución.


Permaneceremos atentos a lo que suceda.


Fuente: [Comunicado oficial Canonical](https://ubuntu.com/blog/statement-on-32-bit-i386-packages-for-ubuntu-19-10-and-20-04-lts)




---


**(Noticia original)**


La noticia que saltó el viernes cayó como una bomba sobre la comunidad linuxera. Y es que [Canonical anunció](https://discourse.ubuntu.com/t/i386-architecture-will-be-dropped-starting-with-eoan-ubuntu-19-10/11263) que **retiraría las librerías de soporte a los sistemas de 32 bits de los repositorios de Ubuntu a partir de su versión 19.10,** lo que supondría que cualquier programa que requiera de librerías de 32 bits dejará de funcionar a partir de esa versión y las siguientes. A partir de aquí se sucedieron los acontecimientos y las reacciones no se hicieron esperar. Pero, ¿por qué tanto revuelo si se supone que los sistemas de 32 bits están en vías de extinción, y ya se lleva tiempo hablando de la retirada de su soporte?


Desde Jugando en Linux vamos a intentar arrojar luz sobre esta polémica decisión por parte de Canonical y en qué puede afectarnos a los que somos jugadores sobre el sistema del pingüino en cualquier variante de Ubuntu y derivadas.


Lo primero que hay que dejar claro es que esto **no tiene nada que ver con que Ubuntu y sus derivadas lancen su sistema operativo compilado para procesadores y sistemas de 32 bits**, cosa que ya no hacen desde la versión 18.10, si no de que hasta ahora siempre ha sido posible ejecutar programas compilados para sistemas de 32 bits dentro de los sistemas Ubuntu de 64 bits dado que **las librerías de soporte para dichos programas de 32 bits se encontraban en los repositorios del sistema**, por lo que si cualquier programa lo requería era posible descargar las dependencias necesarias y ejecutar aplicaciones de 32 bits dentro de nuestros sistemas de 64 bits.


Según [las palabras de Will Cooke](https://discourse.ubuntu.com/t/i386-architecture-will-be-dropped-starting-with-eoan-ubuntu-19-10/11263), una de las cabezas visibles de Canonical, Ubuntu retiraría el soporte para la arquitectura de 32 bits a partir de Ubuntu 19.10, lo que **supondría la retirada de las librerías necesarias para la ejecución de programas de esa arquitectura de los repositorios**, y por consiguiente no sería posible ejecutar programas de 32 bits a no ser que nosotros mismos compilemos las librerías necesarias, cosa harto difícil para usuarios novatos o no duchos en la materia.


Hay que entender que si bien **desde un punto de vista de desarrollo y de costes de mantenimiento esta decisión puede tener mucho sentido** (mantener bibliotecas de una arquitectura que ya se considera obsoleta conlleva no pocos gastos de tiempo y dinero) también es cierto que hay aún muchos programas que se utilizan hoy día que dependen de estas bibliotecas para poder funcionar ya que son programas que por una u otra razón no pueden ser migrados a arquitectura de 64 bits. A continuación veremos tres ejemplos que nos afectan a los jugadores en Linux.


Para ponernos en perspectiva, creo que no descubrimos nada si afirmamos que Ubuntu es la distribución más popular en base a usuarios. Tanto es así que la mayoría de los usuarios que empiezan en Linux lo hacen en Ubuntu o en una de sus muchas derivadas. Así pues la base de usuarios instalada de sistemas Ubuntu o basados en este sobrepasa a cualquier otra distribución Linux hoy día, y entre otras cosas es por eso que Valve pensó en dar soporte oficial a Ubuntu cuando lanzó su cliente de escritorio de Steam para Linux allá por el año 2012.


Desde entonces son muchos los usuarios que han pasado por Ubuntu o han migrado al sistema de Canonical o a alguna de sus derivadas para asegurarse de que obtienen un buen soporte para sus juegos en Linux. Sin embargo, para Valve era primordial lanzar su cliente de Linux en una versión de 32 bits. ¿Por qué?


Pues por que así se aseguraba que al instalar su cliente de escritorio se podrían ejecutar los juegos antiguos desarrollados para arquitecturas de 32 bits pasase el tiempo que pasase ya que mientras que las librerías de compatibilidad de arquitectura de 32 bits estén disponibles en los repositorios sería posible ejecutarlos, y además esto no impediría que el cliente  de Steam para Linux pudiese lanzar los juegos de 64 bits, ya que el soporte nativo del sistema no depende del propio lanzador de Valve.


Con el paso del tiempo, era obvio que el desarrollo de los juegos fuese "migrando" para ser ejecutados en arquitectura de 64 bits, pero no podemos olvidar que desde el 2012 ha llovido mucho ya y **la base de juegos disponibles en Steam para Linux se pueden contar por miles**, y un buen porcentaje de ellos aún se ejecutan bajo arquitectura de 32 bits, con lo que la retirada del soporte de las librerías de 32 bits por parte de la distribución de referencia puede suponer un auténtico desastre para los usuarios que tenemos una abultada biblioteca de juegos en Linux desde hace años. Y si ya de por si Valve tiene problemas con el soporte para otros sistemas, el que de la noche a la mañana tenga a un buen número de usuarios linuxeros quejándose por que la mitad de su biblioteca ya no se ejecuta en un sistema con soporte oficial de Steam ya sería el colmo, y eso sin contar con que Valve está apostando fuerte con el desarrollo de Proton, que a su vez depende de Wine y otras tecnologías dependientes de 32 bits, cosa que veremos mas adelante.


La consecuencia obvia fue que Pierre Loup, el cabeza visible de Valve en cuanto al cliente de Steam en Linux anunciase a las pocas horas que si Ubuntu retiraba el soporte a la arquitectura de 32 bits, Steam dejaría de dar soporte oficial a Ubuntu para soportar otra distribución sin especificar cual, que sí ofreciera soporte para arquitectura de 32 bits. En ese momento mas de un usuario empezó a tener *versionitis*, y Canonical comenzó a tentarse la ropa.



> 
> Ubuntu 19.10 and future releases will not be officially supported by Steam or recommended to our users. We will evaluate ways to minimize breakage for existing users, but will also switch our focus to a different distribution, currently TBD.
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [June 22, 2019](https://twitter.com/Plagman2/status/1142262103106973698?ref_src=twsrc%5Etfw)



 


Con el paso de los años, Valve ha conseguido afianzarse como la referencia del videojuego en la comunidad linuxera a base de ofrecer soporte y desarrollo, y con una base de usuarios que a lo mejor dentro del global de jugadores de Steam puede no llegar a suponer ni el 0,9% de su base, pero que en números absolutos son muchos cientos de miles. El hecho de que Steam diga que deja de soportar a Ubuntu para soportar otra distro puede suponer un serio problema en el número de usuarios de Ubuntu y una alegría para la afortunada distro elegida.


A partir de aquí dentro de la comunidad comenzaron a surgir ciertas preguntas:


*¿Por qué Valve no desarrolla su cliente de Steam para arquitectura de 64 bits?*


Pues por que sí que podría, y de hecho es posible que la tenga hecha, pero eso no significaría ningún cambio ya que Steam es solo el lanzador para decenas de miles de juegos, muchos de los cuales solo se pueden ejecutar en arquitectura de 32 bits.


*¿Y por que entonces Steam no da soporte para esos juegos de 32 bits en su arquitectura de 64 bits?*


Pues por que Steam no es la dueña de los miles de juegos que vende, ni tiene el código fuente de todos ellos, por lo que es imposible para Steam o Valve realizar una recompilación o migración de esa envergadura. La única salida posible es la de seguir soportando arquitecturas de 32 bits.


*¿Y por que Valve no emplea alguna tecnología de emulación o virtualización para los juegos de 32 bits?*


Pues por que la emulación no es un sistema eficiente de cara a la optimización de recursos, puede suponer un quebradero de cabeza a la hora de hacer "*hardware passthrough"* y poder utilizar las ventajas del hardware para exprimir su rendimiento y además supondría introducir una capa más que hará que los requisitos para mover esos juegos con garantías aumenten. Si en Linux ya tenemos que lidiar con una capa de compatibilidad como es Proton que aunque **en algunos casos** es capaz de igualar o incluso superar el rendimiento de los juegos en Windows, en la mayoría de juegos supone una pequeña penalización en cuanto a requisitos de potencia para poder igualarlos, imaginar tener que ejecutar además una capa intermedia de emulación o virtualización que además haya que configurar correctamente. Para Valve no parece ser una solución viable.


*¿Y por qué Steam no presiona a los desarrolladores a migrar sus juegos de 32 bits a 64 bits?*


Pues por que es virtualmente imposible. Muchos de los juegos son tan antiguos que no es viable migrarlos por su propia naturaleza. Otros juegos ya no cuentan con soporte de sus desarrolladores por lo que estos no van a invertir dinero, tiempo y recursos en volver a programar, recompilar, testear y solucionar problemas para juegos que ya no van a suponer unos ingresos mínimos no ya solo en Linux, ni siquiera en Windows. Es más, dada la (escasa) base de usuarios de Linux, muchos desarrolladores sin duda preferirían dejar de soportar a Linux en sus juegos con tal de no incurrir en estos gastos y problemas.


Por otra parte, también hay juegos cuyas desarrolladoras han desaparecido, o los programadores ya no están, o ya no disponen del código fuente. Casos hay miles.


Es paradógico que en Linux tengamos a gala la compatibilidad, y acabe siendo Windows con su soporte legacy para aplicaciones win32 la que acabe haciéndose con la bandera de la retrocompatibilidad.


Pero Valve no solo tiene un problema con los juegos, **también lo tiene con Proton y Wine**.


Todos sabemos lo que ha supuesto la aparición de Proton en Steam con su modo Steam Play. Miles y miles de juegos de Windows ejecutándose sin muchos problemas en Linux, con lo que la viabilidad de nuestro sistema favorito como sistema para juegos dio un salto exponencial. Tanto es así que el desarrollo de Proton supuso un salto enorme para Wine y su compatibilidad, cosa que depende en gran medida de las bibliotecas de 32 bits. ¿Por que?


Pues por que Wine es una capa de traducción que permite ejecutar programas y aplicaciones de Windows en Linux de forma "nativa" tanto de 64 bits, como... de 32 bits. ¿Y en qué arquitectura están programados miles y miles de juegos y aplicaciones en Windows?... otra vez la respuesta es 32 bits.


Por esto Wine no se puede permitir dejar de lado la arquitectura de 32 bits por lo que casi al mismo tiempo que Valve decía que dejaría de dar soporte oficial a Ubuntu de llevar a cabo sus planes, los [responsables de Wine anunciaban](https://www.winehq.org/pipermail/wine-devel/2019-June/147869.html) que ellos también dejarían de dar soporte al sistema de Canonical. Vaya lío.


Aún podemos ver a otra perjudicada, y no es otra que **GOG**. La tienda libre de DRM por excelencia también se enfrentaría a un desastre de llevarse a cabo la retirada de las librerías de 32 bits, ya que buena parte de los juegos para Linux que vende son juegos antiguos (ya lo dice su nombre de sobras: Good Old Games) cuya ejecución depende de dichas librerías, por lo que se vería abocada a retirar las versiones de los juegos para Linux o dejar de dar soporte completo para nuestro sistema.


Como podéis ver, Canonical se ha metido como suele decirse en un jardín. Tanto es así que hace pocas horas que en un intento por calmar las aguas [han anunciado](https://discourse.ubuntu.com/t/i386-architecture-will-be-dropped-starting-with-eoan-ubuntu-19-10/11263/84?u=liamdawe) que no van a retirar las bibliotecas, si no que lo que quieren hacer es "congelarlas" para Ubuntu 19.10 y posteriores en las de la versión Ubuntu 18.04LTS. Sin embargo habrá que esperar a ver si esto es suficiente para Valve y Wine, o acaban anunciando que su soporte se va a otra distribución lejos de los caprichos de Canonical. Novias no les han de faltar, ya que por poner solo un ejemplo, **System76 y su Pop!_OS** ya han dicho que ellos están dispuestos a mantener ellos mismos el soporte 32 bits si fuera necesario.


Después de todo este tocho que os hemos atizado, ¿como veis el panorama videojueguil en Linux? ¿creéis que Canonical ha cometido un error y el daño ya está hecho? ¿Creéis que Valve, Wine o GoG deberían hacer algo mas? ¿Sufrís ya de versionitis?


Contármelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

