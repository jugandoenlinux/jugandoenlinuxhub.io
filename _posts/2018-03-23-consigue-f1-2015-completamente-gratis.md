---
author: leillo1975
category: Carreras
date: 2018-03-23 10:09:41
excerpt: "<p>Adem\xE1s F1 2017 gratis durante este fin de semana.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ca518bf183c339c376b4bdc5c275df01.webp
joomla_id: 690
joomla_url: consigue-f1-2015-completamente-gratis
layout: post
tags:
- steam
- gratis
- free-weekend
- humble-store
- f1-2017
- f1-2015
title: Consigue F1 2015 completamente gratis.
---
Además F1 2017 gratis durante este fin de semana.

Para celebrar el inicio del campeonato de F1 de este año, que en este momento se está celebrando en el circuito de Albert Park, en Melbourne (Australia), tenemos varias noticias que daros relacionadas con este apasionante mundo de las carreras de monoplazas. La primera y más importante es que podeis haceros durante este fin de semana (hasta el domingo a las 18:00 hora peninsular española) con **el juego F1 2015 completamente gratis y para siempre**. Para ello solo teneis que [pasaros por la tienda de Humble Bundle](https://www.humblebundle.com/store/f1-2015) y adquirirlo, y se os dará una clave canjeable en Steam, donde podeis descargar vuestra copia del juego.


También nos viene como anillo al dedo comentaros que **esta noche, a las 22:30,  jugaremos a F1 2017 en nuestra partida semanal online**, tal y como lo decidimos en votación durante estos últimos días en nuestro grupo de [Telegram](https://t.me/jugandoenlinux); y para ello podemos disfrutar de la última y flamante versión, [F1 2017, completamente gratis durante el fin de semana en Steam](http://store.steampowered.com/app/515220/F1_2017/). De esta forma si no teneis el juego, [cosa que no os recomiendo](index.php/homepage/analisis/item/660-analisis-f1-2017), podeis probarlo sin coste a partir de hoy hasta el lunes, pudiendo jugar con nosotros esta noche si os apetece (más info en [Telegram](https://t.me/jugandoenlinux)).


Si finalmente os gusta el juego podeis comprarlo con un 70% de descuento a 16.49€ tanto en la [tienda de Humble Bundle](https://www.humblebundle.com/store/f1-2017) como en [Steam](http://store.steampowered.com/app/515220/F1_2017/). Ya estais tardando en haceros con estos juegos de [@feralgames](https://twitter.com/feralgames) . Os dejamos con un espectacular video de la versión de 2015: 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/ao1z-tZHy18" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Te gusta la Formula 1? ¿Te animas esta noche a jugar con nosotros? Respondenos en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

