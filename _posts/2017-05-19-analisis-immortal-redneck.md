---
author: leillo1975
category: "An\xE1lisis"
date: 2017-05-19 16:00:21
excerpt: "<p>Analizamos el nuevo trabajo de la compa\xF1\xEDa espa\xF1ola Crema Games</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/57df6a0f34180006582f429068c6ac21.webp
joomla_id: 313
joomla_url: analisis-immortal-redneck
layout: post
tags:
- analisis
- roguelike
- crema-games
- immortal-redneck
- espana
title: "An\xE1lisis: Immortal Redneck"
---
Analizamos el nuevo trabajo de la compañía española Crema Games

Recientemente dábamos cuenta del lanzamiento de [Immortal Redneck](index.php/homepage/generos/accion/item/421-disponible-immortal-redneck-para-linux) en nuestra web. También os comentábamos la posibilidad de realizar un análisis en GNU/Linux de este título, y aqui lo teneis, aunque con un pelín de retraso. En primer lugar me gustaría agradecer a la compañía la copia recibida para realizar este artículo.


 


Para empezar vamos a ponernos en antecedentes hablando de sus desarrolladores. [Crema Games](http://cremagames.com/) es un **estudio madrileño** que desde 2009 hasta ahora ha desarrollado juegos para plataformas móviles como Android o iOS con notable éxito. Hace algún tiempo se embarcaron en el desarrollo de su primer juego de PC, y tras pasar airosos por [Steam Greelight](https://steamcommunity.com/sharedfiles/filedetails/?id=834223670), aquí tenemos su primer obra para esta plataforma.


 


![redneck1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisImRedneck/02.webp)*Plataformas y disparos en primera persona*
 


En un principio en el juego se aprecian referencias claras a otros títulos como Serious Sam o Doom, pero poco a poco según vayamos avanzando estas similitudes se verán bastante diluidas en una mecánica de juego propia. Para empezar, el argumento del juego es un tanto rocambolesco y simplemente sirve de excusa para justificar la mecánica de este. En el somos un paleto norteamericano (Redneck), que haciendo el cabra por las dunas del antiguo Egipto cae en una fosa y es convertido en una momia inmortal que debe derrotar a los seres que han invadido las pirámides.


 


El juego, aunque se puede englobar en la **accion 3D en primera persona**, tiene elementos **Roguelike** y **RPG**. Estos están perfectamente engarzados obteniendo un resultado sobresaliente. Nos encontraremos ante un juego, que a pesar de su aspecto desenfadado nos resultará tremendamente dificil de completar debido a que existen multitud de salas o mazmorras predefinidas, pero que se mezclan de forma aleatoria cada vez que morimos, por lo que la experiencia varía constantemente, lo que lo hace **muy rejugable**. En cuanto a esto último, caer en combate implica volver a empezar la pirámide de nuevo, y perder todo el oro que no podamos gastar en el arbol de habilidades o en la tienda. En un principio nos matarán a las primeras de cambio, pero según vayamos desbloqueando los elementos del arbol de habilidades, iremos aguantando mejor las embestidas de los variados enemigos, hasta por fin alcanzar la cima de la pirámide donde nos espera el Jefe Final. Si logramos superarlo, podremos acceder a otra pirámide............ y así hasta completar las 3 pirámides y por lo tanto los 3 jefes. De esta forma pasarán bastantes horas hasta poder terminar el juego.


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisImRedneck/03.webp)*Existe una gran variedad de mazmorras*
 


Para sortear las diferentes mazmorras deberemos vaciarlas de enemigos haciendo uso de las armas y habilidades predefinidas por el personaje que usemos. Esto lo realizaremos a gran velocidad, y realizando saltos imposibles para alcanzar las diferentes zonas de la estancia en la que estemos, encontrando en esta ultima caracteristica elementos vistos en los juegos de plataformas.


 


En su vertiente más de **RPG** encontramos un árbol de habilidades, donde podremos ir mejorando nuestro personaje en multitud de aspectos. También podemos desbloquear otros 8 personajes (dioses egipcios) con caracteristicas propias. Cada personaje tiene habilidades pasivas y activas que varian notablemente la hora de afrontar las mazmorras, pudiendo cada uno de estos adaptarse mejor a nuestro estilo de juego. Estos tambien tienen armas o poderes predefinidos. Podremos encontrar pergaminos que normalmente nos ofrecen ventajas a la hora de afrontar cada una de las mazmorras, aunque a veces también pueden ser negativos y hacernos la vida un poco más difícil. Si conseguimos desbloquear la tienda podremos comprar pergaminos o forjar medallones con las instrucciones que encontraremos en las pirámides.


 


![clases](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisImRedneck/Classes.webp)*Existen 9 clases diferentes de personajes con los que jugar, que se corresponden con dioses egipcios*
 


Durante nuestro avance en el juego dispondremos de una gran variedad de armas, más de 50. Estás guardan como es lógico el aspecto estético del juego y podremos encontrar desde las típicas pistolas, escopetas o subfusiles, a potentes lanzacohetes o "chainguns", pasando por originales armas mágicas y mitológicas, y alguna que otra "marca de la casa" como un lanzapatatas. Obviamente cada una tendrá unas caracteristicas diferentes, como el daño que realizan, su rapidez de carga, o el área de efecto. Cada personaje podrá llevar 3 o 4 armas, que serán intercambiables simplemente con pasar por encima.


 


El juego no dispone de apartado multijugador, lo cual es una pena, pues un modo Deathmach o por equipos podría ser muy divertido, pudiendo escoger entre los diferentes personajes, aunque para ello habría la necesidad de crear mapas especiales para ello o unir varios. También estaría muy bien poder disponer de un modo cooperativo que nos permitiera disfrutar del juego en compañía de algún amigo o amigos. Existe una posibilidad de "interactuar" con otros jugadores, y es el uso del chat de Twitch. Si retransmitimos la partida a través de este servicio, podremos enviar pergaminos al jugador, de forma que podemos ayudar o entorpecer la partida según gane una opción u otra en la votación.


 


![Jefe](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisImRedneck/04.webp)*Al llegar a lo alto de cada pirámide nos encontraremos con un jefe*


A parte de la endiablada jugabilidad es muy de destacar el apartado de gráfico, aprovechando muy bien las posibilidades del motor Unity. El apartado artístico destaca con un look muy "cartoon", con unos escenarios muy coloridos y a posta exagerados. Siguen esta misma premisa el diseño de los enemigos, que a pesar de su aspecto gracioso, creedme, hacen bastante pupa. Sobre el consumo de recursos el juego va bastante fluido dentro de las mazmorras, aunque cuando estamos en el exterior de las pirámides, antes de empezar la acción, la tasa de FPS baja bastante, supongo que por la amplitud y detalles del escenario. El juego presenta algunos errores gráficos en ciertos momentos, como una especie de parpadeo de colores que normalmente se presenta en las pantallas de carga y alguna vez durante el juego. Además no permite realizar Alt-Tab , ya que aunque podemos ver el escritorio u otras ventanas, el ratón se queda con el cursor del juego trabado y no permite clickar en nada. En muchos casos después de realizar esta acción el juego se queda colgado y no responde al teclado ni al ratón. También me fué imposible realizar capturas de pantalla, por lo que estoy usando las del presskit. Supongo que estos pequeños fallos se irán subsanando poco a poco con futuros parches.


 


![Redneck03](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisImRedneck/12.webp)*El juego está perfectamente ambientado y tiene un aspecto "Cartoon"*
 


En el apartado sonoro, el juego presenta unas molodías originales que concuerdan muy bien con el ambiente del juego, mezclando la música tipo Redneck, tocada con Banjos, con las melodías típicas que suelen recordar Egipto. Los efectos sonoros tales como disparos, pasos, enemigos, explosiones, etc... son correctos y no hay nada que destacar, ni positivo ni negativo. En cuanto a las voces, decir que están enteramente en Inglés (dispone de subtitulos) y resultan muy simpaticas, pudiendo escuchar la voz de Redneck o el resto de los personajes cada cierto tiempo haciendo algún comentario jocoso.


 


Como punto final a este análisis me gustaría decir que Immortal Redneck, a pesar de sus puntos negativos (falta de multijugador principalmente), es un excelente y adictivo título que mezcla con maestría diferentes géneros, y que nos entusiasmará y desesperará a partes iguales. Es, a mi juicio, una muy buena entrada de Crema Games en el sector de los juegos de PC. Yo, por mi parte, estoy deseando ver ya su próximo trabajo.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/VGyvt1Vk5bs" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Puedes comprar Inmortal Redneck en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/595140/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Te parece interesante la propuesta de Crema Games? ¿Estás de acuerdo con el análisis? Cuentanos esto o cualquier cosa relacionada con Inmortal Redneck en los comentarios de este artículo o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 


FUENTES: [Crema Games](http://cremagames.com/), [Immortal Redneck](http://www.immortalredneck.com/), [Steam](http://store.steampowered.com/app/595140/)

