---
author: leillo1975
category: "Acci\xF3n"
date: 2017-01-27 08:30:09
excerpt: <p>Adquiere este gran juego por menos de un euro en la conocida tienda</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7ab716354432ee12f19b58c60471093b.webp
joomla_id: 198
joomla_url: payday-2-por-1-en-humble-bundle
layout: post
tags:
- humble-bundle
- payday-2
title: Payday 2 por menos de un Euro en Humble Bundle
---
Adquiere este gran juego por menos de un euro en la conocida tienda

Si no tienes este divertido juego, como era mi caso, estás de suerte, que por muy poco dinero podrás adquirirlo con algunas de sus DLC's ("Heists", "John Wick Weapon Pack" y "Lycanwulf and The One Below Masks"). En caso de que queramos hacernos con la versión Game Of the Year y más mascaras tendremos que desembolsar como mínimo 5.61€. También podremos añadir al pack Dead by Daylight por poco más de 13€. Por último, por 23,36€ o más estaría también disponible el juego John Wick. Estas dos ultimas modalidades no nos interesarían a los Linuxeros, ya que se trata de dos juegos que en principio son para Windows, y en el caso del último que dispongan de HTCVive.


Ultimamente no soy muy fan de este tipo de juegos, digamos que soy más de juegos pausados y reflexivos, pero reconozco la factura de este impresionante título multijugador. En su día, al poco de ser portado a Linux, tuve la oportunidad de probarlo durante undos días que estuvo libre en Steam y la verdad es que es tremendamente divertido y adictivo. En él tenemos que hacer equipos de jugadores en la que la misión principal es robar dinero y escapar con él, luchando contra hordas de policias, SWATs, helicópteros.... Existen multitud de mapas, bastante diferentes entre si, donde en unos premia más la acción directa y en otros la estrategia. Podemos decir que en cierta medida recuerda a Left4Dead en cuanto a que es un multijugador cooperativo donde prima la colaboración entre jugadores. En cuanto a la calidad del port hay que decir que el juego se comporta muy bien mostrando una buena tasa de frames y sin mucho que envidiarle a la versión de Windows. Sus requisitos mínimos son muy bajos y funcionará sin problemas en casi cualquier configuración. Su precio nomal son 20€, por lo que yo no me lo pensaría dos veces.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/C7FRoHcctLc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 Si quieres comprar Payday 2 en alguna de estas modalidades simplemente pásate por [este enlace.](https://www.humblebundle.com/starbreeze-bundle-presents-john-wick)


Si quieres comentar cualquier cosa sobre el juego u organizar una partida, deja mensaje en los comentarios. También te puedes pasar por nuestro canal de [Telegram](https://t.me/jugandoenlinux) o usar [Discord](https://discord.gg/ftcmBjD)


FUENTES: [Steam](https://www.humblebundle.com/starbreeze-bundle-presents-john-wick), [Humble Bundle](https://www.humblebundle.com/starbreeze-bundle-presents-john-wick)

