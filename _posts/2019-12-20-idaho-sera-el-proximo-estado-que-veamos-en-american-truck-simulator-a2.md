---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-12-20 16:05:23
excerpt: "<p>@SCSSoftware lanzar\xE1 muy pronto su esperada DLC.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Idaho/ATS_Idaho_copy.webp
joomla_id: 1143
joomla_url: idaho-sera-el-proximo-estado-que-veamos-en-american-truck-simulator-a2
layout: post
tags:
- dlc
- american-truck-simulator
- ats
- scs-software
- idaho
title: "IDAHO ser\xE1 el proximo estado que veamos en American Truck Simulator (ACTUALIZADO\
  \ 2)"
---
@SCSSoftware lanzará muy pronto su esperada DLC.


**ACTUALIZACIÓN 11-7-20:** El estudio checo [acaba de anunciar la fecha escogida para para el lanzamiento](https://blog.scssoft.com/2020/07/idaho-release-date-announcement.html) de su última expansión de mapa para American Truck Simulator, que finalmente **podrá ser adquirida el próximo 16 de este mismo mes**. Por supuesto nosotros estaremos al tanto para ser de los primeros en anunciároslo y si nos es posible realizar como suele ser habitual algunos Streamings en nuestros canales de [Twitch](https://www.twitch.tv/jugandoenlinux) y [Youtube](https://www.youtube.com/c/jugandoenlinuxcom). Aquí os dejamos un pequeño trailer de lo que podremos encontrarnos la semana que viene:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/4e0uztR4Ydk" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


También nos gustaría resaltar **una nueva característica, los [viewpoints](https://blog.scssoft.com/2020/07/idaho-directors-cut.html)**, que encontraremos en esta expansión y que **nos permitirá disfrutar más tranquilamente de determinados lugares especiales en el mapa**, sin que para ello tengamos que arriesgar nuestro viaje mirando por la ventana o cambiando de vistas. Para ello encontraremos iconos en el mapa en los que podremos pararnos y ver una cinemática mostrándonos esos puntos de interés con todo detalle. En este video podeis ver más facilmente a lo que nos referimos:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/YGRpv1EJKKc" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**ACTUALIZACIÓN 27-5-20:** Dándome una vuelta por el [Blog de SCS](https://blog.scssoft.com/2020/05/idaho-sandpoint-to-lewiston-gameplay.html), me he topado con el **primer video "in game"** de lo que será la nueva expansión de Mapa para American Truck Simulator, [Idaho](https://store.steampowered.com/app/1209470/American_Truck_Simulator__Idaho/), a bordo de uno de los **nuevos Mack** que recientemente [os enseñamos en uno de nuestros directos](https://www.youtube.com/watch?v=Pu-0wwg35vY) de nuestro canal de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom). Como siempre, desde SCS nos advierten que están trabajando activamente en esta DLC y todo lo que veais puede que no se corresponda con el resultado final. Mientras seguimos esperando a la salida oficial de Idaho os dejamos con el video:  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/VbwgYyODHiw" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


Éramos pocos y parió la abuela.... El día va de anuncios, y es que SCS también  ha anunciado via twitter que **American Truck Simulator** continuará su expansión por el territorio Norteamericano con el estado de [Idaho](https://es.wikipedia.org/wiki/Idaho), volviendo una vez más a paisajes más verdes semejantes a los de sus vecinos [Oregon](index.php/component/k2/20-analisis/998-analisis-american-truck-simulator-dlc-oregon) y [Washington](index.php/component/k2/20-analisis/1196-analisis-american-truck-simulator-washington-dlc). Al igual que [Iberia](index.php/homepage/generos/simulacion/1142-la-proxima-expansion-de-euro-truck-simulator-2-sera-iberia), el anuncio se realizaba ayer en un Livestream especial por navidad, y prueba de ello es este tweet de su cuenta:



> 
> Have you heard the news? ?  
>   
> The next map expansion for [#ATS](https://twitter.com/hashtag/ATS?src=hash&ref_src=twsrc%5Etfw) has just been announced in our special Xmas live stream! ?  
>   
> The state of Idaho is coming! Put it to your Steam wishlist now! ?<https://t.co/qcMwVv9nFw> [pic.twitter.com/kKmioI9E7k](https://t.co/kKmioI9E7k)
> 
> 
> — SCS Software (@SCSsoftware) [December 19, 2019](https://twitter.com/SCSsoftware/status/1207706975796105216?ref_src=twsrc%5Etfw)



 Como en la noticia anterior, no tenemos ningún tipo de información sobre esta, más que se ha habilitado su correspondiente [página de Steam](https://store.steampowered.com/app/1209470/American_Truck_Simulator__Idaho/) para que vayais poniéndolo en deseados. Por supuesto , en cuanto tengamos más información sobre esta nueva DLC, iremos añadiendo la información a este artículo, como de costumbre, para que tengais de primera mano todo lo relacionado con ella.


![ATS Idaho 2 copy](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Idaho/ATS_Idaho_2_copy.webp)


¿Te gusta American Truck Simulator? ¿Qué te parece la elección de este nuevo territorio? Cuéntanoslo en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

