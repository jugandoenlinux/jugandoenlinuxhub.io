---
author: Serjor
category: Deportes
date: 2017-06-02 15:30:02
excerpt: "<p>La comunidad est\xE1 intentando a trav\xE9s de la plataforma change.org\
  \ que LoL llegue a Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f3ad4a234535b69ec9bf916a25462992.webp
joomla_id: 357
joomla_url: peticion-abierta-a-riot-para-que-traigan-league-of-leagens-a-linux
layout: post
tags:
- league-of-legends
- lol
- moba
title: "Petici\xF3n abierta a Riot para que traigan League Of Legends a Linux"
---
La comunidad está intentando a través de la plataforma change.org que LoL llegue a Linux

No podemos decir con exactitud si este tipo de iniciativas al final llegan a buen puerto o no, pero como dicen en mi pueblo, "el no ya lo tienes". Y eso ha debido de pensar Kalle Kadakas, quién se ha echado a sus espaldas la titánica tarea de que Riot porte a Linux su juego estrella, League Of Legends, y es que en Reddit ya comentaron que no [tenían ningún plan](https://www.reddit.com/r/leagueoflegends/comments/66k4oo/riot_support_on_linux_client_hopefully_in_the/dgj8f34/) de portar su juego a Linux alegando que el retorno de inversión no está garantizado, así que si quieres sumarte a la petición y demostrarles que hay el interés suficiente como para que les rente llevar a cabo la migración, pásate por [aquí](https://www.change.org/p/riot-games-petition-to-support-league-of-legends-linux-client?source_location=minibar) para firmar.


 


Para quién no lo sepa League Of Legends, o LoL como es más conocido, es un MOBA, como Dota 2, donde dos equipos se enfrentan en un mapa con tres carriles principales para llegar hasta la base enemiga y destruirla, y probablemente sea uno de los juegos con más usuarios del mundo, llegando a tener según la propia Riot 100 millones de usuarios activos cada mes.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/BGtROJeMPeE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Y tú, ¿has firmado ya? Coméntanos qué te parecen estas iniciativas, tanto en los comentarios como en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).


 

