---
author: leillo1975
category: Estrategia
date: 2021-03-01 08:52:04
excerpt: "<p>La versi\xF3n 4.0.0 del juego #OpenSource <span class=\"css-901oao css-16my406\
  \ r-poiln3 r-bcqeeo r-qvutc0\">@Warzone_2100 ya es oficial y p\xFAblica<br /></span></p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Warzone2100/Warzone_2100_1.webp
joomla_id: 1283
joomla_url: warzone-2100-tiene-ya-soporte-vulkan-a1
layout: post
tags:
- rts
- estrategia
- vulkan
- open-source
- warzone-2100
title: Warzone 2100 tiene ya soporte Vulkan (ACTUALIZADO)
---
La versión 4.0.0 del juego #OpenSource @Warzone_2100 ya es oficial y pública  



ACTUALIZACIÓN 5-4-21: Tras algo más de un mes en Beta pública, finalmente el equipo que está detrás de Warzone 2100 ha presentado la versión final, oficiosa y oficial 4.0.0, con los consabidos cambios que os indicamos la vez anterior y que podeis ver un poco más abajo. El anuncio se realizaba ayer por la noche tal y como podeis ver en este tweet:  
  




> 
> Warzone 2100 4.0.0 has been released! Vulkan support, "Factions", additional music, hundreds of bug fixes, 64-bit Windows builds, and more!<https://t.co/w2K1cBPRrN>
> 
> 
> — Warzone 2100 (@Warzone_2100) [April 4, 2021](https://twitter.com/Warzone_2100/status/1378812946869018624?ref_src=twsrc%5Etfw)


  





Podeis consultar los detalles del anuncio en [este link](https://forums.wz2100.net/viewtopic.php?f=1&t=16162), así como ver información sobre el juego, y los enlaces de descarga en su [página web](https://wz2100.net/). Ahora solo queda preguntaros si nos echamos unas partidillas.


 




---


**NOTICIA ORIGINAL:** Cuando va a hacer ya la friolera de 3 años y medio que [os anunciamos](index.php/listado-de-categorias/estrategia/500-warzone-2100-tendra-version-con-vulkan) que el juego utilizaría esta conocida **API gráfica**, nunca pensamos que pasaría tanto tiempo, y es que en el medio han pasado ya [unas cuantas versiones y revisiones]({{ "/tags/warzone-2100" | absolute_url }}) de este conocido juego de estrategia en tiempo real de código abierto. El anuncio de esta esperada noticia nos llegaba ayer por la noche a traves de las redes sociales:



> 
> Warzone 2100 4.0.0-beta1 is available for testing! Vulkan support, "Factions", additional music, hundreds of bug fixes, 64-bit Windows builds, +++<https://t.co/pqwLZVKiQ6>
> 
> 
> — Warzone 2100 (@Warzone_2100) [February 28, 2021](https://twitter.com/Warzone_2100/status/1366129319920164864?ref_src=twsrc%5Etfw)







El juego además viene acompañado de una buena cantidad de [mejoras y correcciones](https://wz2100.net/news/version-4-0-0-beta1/) entre las que destacan las que podeis consultar en la siguiente lista:


*-**Nuevo soporte de backend de gráficos:***  
*Vulkan 1.0+*  
*OpenGL ES 3.0 / 2.0*  
*DirectX (a través de libANGLE, OpenGL ES -> DirectX)*  
*Metal (a través de MoltenVK, Vulkan -> Metal)*  
*Además del soporte existente para*  
*OpenGL 3.0+ Core Profile (por defecto), OpenGL 2.1 Compatibility Profile*  
*Ver la nueva opción "Graphics Backend" en el menú de opciones de vídeo.*  
 ***-Nuevas "Facciones"** para el multijugador / escaramuza*  
 *-Texturas de terreno y fondos de **mayor resolución***  
*-Nuevo **gestor de música,** + el nuevo álbum de bandas sonoras de AlexTheDacian*  
 *-Soporte para **mapas "generados por script" / "aleatorios"** (y dos nuevos mapas incorporados que lo aprovechan: `6p-Entropy` y `10p-Waterloop`)*  
 ***-Chat de sala desplazable**, y muchas otras mejoras en la interfaz de usuario / widget*  
 *-Bots de **IA actualizados** / más inteligentes (Bonecrusher, Cobra)*  
 *-Nuevo modo "sin cabeza" (para `--autogame`, `--autohost`, `--skirmish`)*  
 *-Mejoras en la API JS, + un nuevo "depurador de scripts"*  
 *-Eliminación de Qt como dependencia, + un nuevo motor JS incrustado: QuickJS*  
 *-Mejoras en la calidad de vida / fluidez*  
 *-Cientos de correcciones de errores*


Recordad que el juego está en su primera beta y es muy susceptible de contener errores y elementos a mejorar, por lo que si encontrais algo, reportadlo en su página de [su proyecto](https://github.com/Warzone2100/warzone2100/issues). Podeis descargar el juego desde [Sourceforge](https://sourceforge.net/projects/warzone2100/files/releases/4.0.0-beta1/) o [Github](https://github.com/Warzone2100/warzone2100/releases/tag/4.0.0-beta1).


Os dejamos con un video-gameplay de hace algún tiempo de nuestro amigo @SonLink:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/CN_EGFteFCo" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>

