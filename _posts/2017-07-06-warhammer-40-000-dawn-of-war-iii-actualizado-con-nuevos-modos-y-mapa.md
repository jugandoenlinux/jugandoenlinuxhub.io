---
author: leillo1975
category: Estrategia
date: 2017-07-06 11:06:00
excerpt: <p>El juego incluye ahora nuevos modos multijugador y nuevas defensas.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7aa3cdbe521e8339c01d4c47e738f898.webp
joomla_id: 395
joomla_url: warhammer-40-000-dawn-of-war-iii-actualizado-con-nuevos-modos-y-mapa
layout: post
tags:
- feral-interactive
- actualizacion
- warhammer
- dawn-of-war
title: 'Warhammer 40,000: Dawn of War III actualizado con nuevos modos y mapa.'
---
El juego incluye ahora nuevos modos multijugador y nuevas defensas.

Hace poco menos de un mes [se estreno este potente título](index.php/homepage/generos/estrategia/item/486-disponible-warhammer-40-000-dawn-of-war-iii-en-gnu-linux-steamos) de estrategia en tiempo real en GNU-Linux/SteamOS. Esta misma mañana Feral Interactive nos informaba publicando el anuncio en su cuenta de twitter. Podeis verlo aquÍ:


 



> 
> Warhammer 40,000: Dawn of War III for macOS and Linux updated with new multiplayer modes and defenses. Learn more: <https://t.co/duONBUqHbV> [pic.twitter.com/UHuSFFGvso](https://t.co/UHuSFFGvso)
> 
> 
> — Feral Interactive (@feralgames) [6 de julio de 2017](https://twitter.com/feralgames/status/882903135760592896)



 


Según nos cuentan en la [sección de noticias de su página](http://www.feralinteractive.com/es/news/784/), esta actualización añade varias caracteristicas nuevas a este título, entre las que destacan las siguientes:


 


-**Dos nuevos modos multijugador**: *Juega al modo ‘Annihilation Classic’ para eliminar las estructuras principales de tus enemigos con la ayuda de doctrinas nuevas que te permiten construir torretas específicas para facciones. O escoge el modo ‘Annihilation with Defenses’ para empezar con varias torretas ya instaladas, lo que te dará más tiempo para preparar a tu ejército.*


 


-**Nuevo mapa multijugador**: *Mortis Vale es un increíble campo de batalla nuevo que alberga tus descomunales enfrentamientos. Cúbrete tras arcos y columnas góticas; Mortis Vale es el lugar perfecto para ejecutar emboscadas devastadoras...*


 


-**Nuevo paquete de Skins:** *El paquete de aspectos ‘Engines of Annihilation’ da un estilo especial a tus supersoldados de a pie con el aspecto ‘House Raven’ skin para el Caballero Imperial, con el aspecto ‘Exalted’ para el Caballero Espectral y con el aspecto ‘Deathskulls’ para el Gorkanaut.*


 


Aquí os dejo un video donde podeis ver el modo "Annihilation":


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/9JzNjHTZJzE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Como siempre, este nuevo contenido gratuito siempre es bienvenido y nos otorgará más horas de diversión si disponemos de este juego. Si estás interesados en comprarlo podeis hacerlo directamente en la [**tienda de Feral**](https://store.feralinteractive.com/es/mac-linux-games/dawnofwar3/) (recomendado, y ahora mismo más barato) o en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/285190/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Teneis ya Warhammer 40K: DOW3? ¿Que os parecen los nuevos modos y el nuevo mapa? Podeis dejar vuestras impresiones en los comentarios o en nuestro canal de [Telegram](https://telegram.me/jugandoenlinux).

