---
author: Pato
category: Plataformas
date: 2017-05-11 12:04:21
excerpt: "<p>El juego es obra del estudio espa\xF1ol \"Under the Bridge Studios\"\
  </p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/20e6d0dc79365d45f8620c21a172f633.webp
joomla_id: 315
joomla_url: illusoria-un-plataformas-muy-interesante-tiene-muchas-posibilidades-de-llegar-a-linux-steamos
layout: post
tags:
- indie
- plataformas
- aventura
- 2d
- un-jugador
title: '''Illusoria'' un plataformas muy interesante tiene muchas posibilidades de
  llegar a Linux/SteamOS'
---
El juego es obra del estudio español "Under the Bridge Studios"

Siempre es bueno ver que hay posibilidades de que nos lleguen nuevos desarrollos, y más si se tratan de juegos interesantes. En este caso, se trata de 'Illusoria' [[página oficial](http://badlandindie.com/illusoria/)] que está siendo editado por "Bad Land Games", la editora que nos ha traido juegos como "Anima: Gate of memories" o "DEX", y está siendo desarrollado por "Under the Bridge Studios", un estudio madrileño que inició una campaña en Greenlight, y que superó con éxito no hace mucho.


El desarrollo me pareció de lo más interesante y les pregunté por la posibilidad de que el juego llegase a Linux, a lo que respondieron:



> 
> *Nuestra intención es sacarlo en Linux y Mac. Seguramente una vez salga a la venta nos pondremos en ello, si. No os dejaremos colgados, tranquilos.*
> 
> 
> 


Podeis ver el post en su [página de Greenlight](http://steamcommunity.com/workshop/filedetails/discussion/868273099/133258593407694241/?tscn=1494314412).


#### Sobre 'Illusoria':


*llusoria es un juego de plataformas en 2D que hace un claro homenaje a todos aquellos juegos de plataformas que surgieron en la década de los noventa tales como Flashback, Heart of Darkness o Another World.*


*Illusoria es un videojuego donde el jugador podrá sumergirse en un mundo de fantasía lleno de personajes maravillosos y terribles monstruos. Un lugar donde la muerte acecha a cada paso y donde la paciencia y la pericia serán sus mejores aliados.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/nAEwPvMYCKo" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 ¿Que te parece este 'Illusoria'? ¿Te gustaría que llegase a Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

