---
author: Pato
category: "Acci\xF3n"
date: 2018-06-13 17:09:59
excerpt: "<p>El juego de @FinjiCo sigue poni\xE9ndonos los dientes largos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a8d0dd35ac3aeaea0c447d40f598ed01.webp
joomla_id: 767
joomla_url: tunic-vuelve-a-publicar-un-trailer-en-el-e3-2018-pero-sigue-sin-confirmar-oficialmente-su-salida-en-linux
layout: post
tags:
- accion
title: '''Tunic'' vuelve a publicar un trailer en el E3 2018 pero sigue sin confirmar
  oficialmente su salida en Linux'
---
El juego de @FinjiCo sigue poniéndonos los dientes largos

Hace ya un año, por el E3 del año pasado que nos hicimos eco de uno de los juegos que más llamó la atención. Se trata de '**Tunic**' [[web oficial](http://www.tunicgame.com/)] un juego de la mano de **Finji**, compañía que ya ha editado juegos en Linux como [Canabalt,](https://store.steampowered.com/app/358960/Canabalt/) [Feist](https://store.steampowered.com/app/327060/FEIST/) o el excelente [Night in the Woods](https://store.steampowered.com/app/481510/Night_in_the_Woods/), y del que aún no tenemos confirmación oficial de su llegada a Linux, y de hecho en su página de Steam sigue sin tener el logo ni los requisitos. Sin embargo, tal y como [ya te contamos el año pasado](index.php/homepage/generos/aventuras/item/495-tunic-el-juego-de-finji-tiene-posibilidades-de-llegar-a-linux), por la respuesta del estudio y el historial de lanzamientos para nuestro sistema favorito tiene muchas papeletas de salir para Linux/SteamOS.


Ahora, aprovechando el E3 de este año, el estudio no ha perdido la oportunidad para enseñar un poco más de lo que será este 'Tunic', y la verdad es que pinta realmente bien. Juzga tu mismo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/NCcaiBc4Mo0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 *TUNIC es una aventura de acción sobre un pequeño zorro en un mundo grande. Explora el desierto, descubre ruinas espeluznantes y lucha contra criaturas terribles de hace mucho tiempo.*


¿Que te parece 'Tunic'? ¿Te gustaría que llegase a Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

