---
author: Pato
category: Carreras
date: 2017-05-03 17:59:46
excerpt: "<p>Se trata de un proyecto indie con muy buen aspecto. Promete versi\xF3\
  n para Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3119c7be2ab58173062c39c6b8c72ed7.webp
joomla_id: 302
joomla_url: antigraviator-es-una-mezcla-de-carreras-de-naves-futurista-que-busca-financiacion-en-kickstarter
layout: post
tags:
- accion
- carreras
- indie
- kickstarter
- arcade
title: "'Antigraviator' es una mezcla de carreras de naves futurista que busca financiaci\xF3\
  n en Kickstarter"
---
Se trata de un proyecto indie con muy buen aspecto. Promete versión para Linux

Como siempre que se trate de Kickstarter, las financiaciones colectivas siempre hay que tomarlas con cuidado. Sin embargo este 'Antigraviator' [[web oficial](http://www.antigraviator.com/)] tiene una pinta excelente e incluso hay disponible una versión demo que los desarrolladores ofrecen diréctamente en su web o en la campaña de Kickstarter que han comenzado hace poco.


¿De que trata este 'Antigraviator'? 


Pues se trata de un arcade de carreras de naves futurista al estilo de F-Zero o Wipeout pero con elementos que lo distancian de estos, ya que durante las carreras tendremos que ir sorteando distintos obstáculos o trampas como minas, cohetes, misiles, desprendimientos, etc. que irán sucediéndose conforme los jugadores vayan "activándolos" mediante los items correspondientes. Me recuerda mucho a juegos como la saga "Sega All stars Racing" donde según los items que pudieras conseguir podías realizar distinos ataques o desplegar trampas o defensas. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/JU47S1UoWqs" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Además de esto, el juego ofrecerá la posibilidad de personalizar las naves ya sea en su apariencia, como en el armamento y opciones de manejo. También se podrá jugar a pantalla dividida en cooperativo local, y tendrá multijugador online para echar unas partidas con los amigos.


Como ya he dicho, los desarrolladores dejan claro su desarrollo para Linux tanto en su campaña de Kickstarter como en su ya finalizada campaña de Greenlight, donde consiguieron su objetivo en pocos días. Además, ofrecen una versión demos del juego, pero es solo para sistemas Windows, si bien es cierto que el motor que están usando es Unity 3D por lo que no debe ser dificil hacer una versión para nuestro sistema.


Si te van los juegos de carreras de naves y te gusta lo que ves, puedes ver la campaña y apoyar el proyecto en Kickstarter:


<div class="resp-iframe"><iframe height="420" src="https://www.kickstarter.com/projects/1304393209/antigraviator/widget/card.html?v=2" width="220"></iframe></div>


 ¿Qué te parece este 'Antigraviator'? ¿piensas apoyarlo? ¿te gustan los juegos de carreras de naves?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

