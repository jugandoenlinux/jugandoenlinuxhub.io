---
author: Serjor
category: Estrategia
date: 2017-10-20 18:10:11
excerpt: <p>La segunda guerra mundial nunca fue tan disparatada</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1f9467ed0ebd32e9dc822d63c55d5401.webp
joomla_id: 499
joomla_url: bomber-crew-disponible-para-gnu-linux
layout: post
tags:
- aviones
- estrategia
- simulacion
- wwii
- bomber-crew
title: Bomber Crew disponible para GNU/Linux
---
La segunda guerra mundial nunca fue tan disparatada

Este pasado 19 de octubre aparecía Bomber Crew en Steam, con soporte para GNU/Linux desde el primer día.


 ![Bomber Crew Action Shot](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/BomberCrew/Bomber_Crew-Action_Shot.webp)


Bomber Crew, un juego desarrollado por [Curve Digital](http://curve-digital.com/), es una mezcla entre un simulador y un juego de estrategia, con una estética que recuerda a los Miis de Nintendo, en la que para ganar los combates por un lado deberemos gestionar correctamente el avión que hayamos seleccionado, que además es personalizable, teniendo en cuenta armamento o combustible por ejemplo, y por otro lado tendremos que enfrentarnos a nuestros enemigos donde para ganar puede que nos veamos obligados a hacer que parte de la tripulación salga al exterior del avión a apagar algún incendio.


![Bomber Crew   Crew Select](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/BomberCrew/Bomber%20Crew%20-%20Crew%20Select.webp)


De alguna manera recuerda a FTL y Valkyria Chronicles, y a pesar de su aspecto infantil promete un juego profundo y difícil.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/lodeMgrbwkg" style="display: block; margin-left: auto; margin-right: auto; border: 0;" width="560"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/537800/" style="display: block; margin-left: auto; margin-right: auto; border: 0;" width="646"></iframe></div>


Y a ti, ¿qué te parece este Bomber Crew? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

