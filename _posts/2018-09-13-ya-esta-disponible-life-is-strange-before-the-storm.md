---
author: leillo1975
category: Aventuras
date: 2018-09-13 14:47:24
excerpt: "<p>El port de @feralgames est\xE1 a la venta</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2dd513482436e04a12ec78eb00a59ba5.webp
joomla_id: 854
joomla_url: ya-esta-disponible-life-is-strange-before-the-storm
layout: post
tags:
- feral-interactive
- opengl
- square-enix
- life-is-strange
- before-the-storm
- deck-nine
title: "Ya est\xE1 disponible \"Life is Strange: Before the Storm\""
---
El port de @feralgames está a la venta

Tal y como [os adelantábamos la semana pasada](index.php/homepage/generos/aventuras/item/973-life-is-strange-before-the-storm-estara-muy-muy-pronto-en-linux), y puntuales como siempre, la compañía de ports británica Feral Interactive acaba de lanzar ["Life is Strange: Before the Storm](http://www.feralinteractive.com/es/games/lisbeforethestorm/)", por lo que a partir de ya podeis haceros con esta estupenda aventura desarrollada por  [Deck Nine](http://deckninegames.com/) y editada por [Square Enix](https://square-enix-games.com/). El anuncio nos ha llegado como siempre a traves de su cuenta de twitter:



> 
> Life is Strange: Before the Storm is out now for macOS and Linux!  
>   
> Buy the Deluxe Edition from the Feral Store to get all the episodes including the bonus episode, “Farewell”: <https://t.co/li1F3VtTgA> [pic.twitter.com/cciVKmSRSL](https://t.co/cciVKmSRSL)
> 
> 
> — Feral Interactive (@feralgames) [September 13, 2018](https://twitter.com/feralgames/status/1040248513412706304?ref_src=twsrc%5Etfw)



El juego, al contrario que los últimos ports de Feral **no usa la API de Vulkan, sinó que utiliza OpenGL.** Los requisitos técnicos que tendreis que cumplir para poder disfrutar de este juego son los siguientes:




|  | Mínimos | recomendada |
| --- | --- | --- |
| Sistema Operativo: | Ubuntu 18.04 (64bit) |
| Procesador: | Intel Core i3-4130T 2,9 GHz | 3.4 GHz Intel Core i7-4770 3,2 GHz |
| RAM: | 4GB | 8GB |
| Disco Duro: | 28GB |
| Placa Gráfica: | AMD R9 270 de 2GB o superior, Nvidia GTX 680 de 2GB o superior | Nvidia GTX 970 de 4GB o superior, AMD RX 470 de 4GB o superior |
| Entrada: | Teclado, Teclado y ratón | Mando |



* AMD GPU requieren el controlador MESA 18.1.6, Nvidia GPU requieren el controlador Nvidia 396.54 o superior.
* Intel GPU no son compatibles.



 Podeis haceros con todos los episodios de esta aventura más uno extra en la versión deluxe que podeis adquirir  en la [Humble Bundle Store](https://www.humblebundle.com/store/life-is-strange-before-the-storm-deluxe-edition?partner=jugandoenlinux) (patrocinado) y en la propia [tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/lisbeforethestormdeluxe/). Os dejamos con el video de lanzamiento:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/NSX72mOQxvY" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 ¿Os gustan las aventuras de este tipo? ¿Habeis jugado ya a algún "Life is Strange"? Dejanos tus opiniones en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).


 

