---
author: Pato
category: "Acci\xF3n"
date: 2017-10-17 11:13:40
excerpt: <p>Lucha como un conejo contra otros conejos para liberarlos de los malvados
  tiranos</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/18325a37bf525b9d88d51a3441669c72.webp
joomla_id: 489
joomla_url: overgrowth-un-juego-de-artes-marciales-con-conejos-ya-disponible-en-linux-steamos
layout: post
tags:
- accion
- steamos
- indie
- steam
- lucha
title: '''Overgrowth'' un juego de artes marciales con conejos... ya disponible en
  Linux/SteamOS'
---
Lucha como un conejo contra otros conejos para liberarlos de los malvados tiranos

Quien lo diría. Un juego de lucha con conejos. Ese es el planteamiento "surrealista" de 'Overgrowth', un juego de lucha del estudio Wolfire Studio [[web oficial](http://www.wolfire.com/overgrowth)]. Según sus propias palabras, es el sucesor espiritual de Lugaru, juego open source desarrollado por el mismo estudio y del que toma su base para expandirlo.


En 'Overgrowth' tendremos que luchar como un conejo ninja para liberar a los conejos de sus malvados tiranos en la historia principal, podremos jugar la historia de la "pre-secuela" o probar los mods hechos por la comunidad durante los últimos años.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/o5l2xr5BBq8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Salta, patea, trepa... elige como afrontar cada lucha. ¿Quieres enfrentarte a los enemigos de uno en uno? ¿Prefieres atacar a varios en grupo? ¿o robar el arma mas poderosa y repartir mamporros a todo el mundo?


Sea como sea, 'Overgrowth' no está disponible en español, pero si eso no es impedimento lo tienes ya disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/25000/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


¿Lucharás como un conejo?... 


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

