---
author: P_Vader
category: Entrevistas
date: 2022-01-07 19:36:48
excerpt: "<p>Estudio argentino especialistas en buenas aventuras gr\xE1ficas.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SlenderThreads/Slender_Threads_portada.webp
joomla_id: 1406
joomla_url: entrevista-a-blyts-nos-presenta-slender-threads
layout: post
tags:
- indie
- demo
- aventura-grafica
- argentina
title: Entrevista a Blyts, nos presenta Slender Threads
---
Estudio argentino especialistas en buenas aventuras gráficas.


Hace unos meses probé una aventura gráfica que descubrí [gracias a su demo]({{ "/posts/v-rings-of-saturn-las-demos-tienen-que-cambiar" | absolute_url }}) a modo de prólogo. **Se trataba de Slender Threads y me agradó enormemente pues me recordaba a las buenas aventuras de antaño, especialmente a mi adorado [Monkey Island](https://es.wikipedia.org/wiki/Monkey_Island)**. Indagando un poco me tope con que Blyst, su desarrolladora, era un estudio argentino y nos pusimos en contacto para saber un poco mas de ellos y de su trabajo. A continuación os dejo la entrevista que le hicimos a Leandro de Blyst:


 


**JEL: Primeramente me gustaría que me contaseis un poco sobre vuestro estudio, quienes sois, trayectoria.**


**Leandro:** Blyts es un estudio independiente fundado en Argentina en el año 2010, comenzamos desarrollando juegos casuales para celulares y poco a poco nos fuimos volcando hacia los juegos de aventuras. Actualmente somos un equipo semi-fijo de 8 personas: tres desarrolladores, dos ilustradores, una story-line designer, un guionista y un tester. Esporadicamente se suman otros miembros, como productores de sonido y traductores cuando estamos en la etapa final de un proyecto.


Nuestra primera aventura "Kelvin and the Infamous Machine" fue [fundada en Kickstarter](https://www.kickstarter.com/projects/blyts/kelvin-and-the-infamous-machine) en el año 2014 y nos abrio un poco las puertas para seguir desarrollando otras aventuras como Nobodies y ahora Slender Threads.


![Slender Threads 1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SlenderThreads/Slender_Threads_1.webp)


**JEL: Parece que os habéis especializado en las aventuras gráficas. ¿Por qué ese género? ¿Cuales son las influencias de otros juegos a la hora de desarrollar vuestros juego?**


**Leandro:** Principamente por dos motivos: En nuestras infancias, los primeros acercamientos a la computadora fueron atraves de videojuegos y en especial, de aventuras gráficas. Por lo que estos juegos guardan un lugar especial en nuestro corazones. Y segundo, creo que las aventuras o juegos basados en la narrativa son una manera muy interesante de contar buenas historias.  
Los juegos que nos han marcado mucho fueron los de la empresa LucasArts, en particular Day of the Tentacle, Monkey Island o Sam & Max entre varios. Actualmente, jugamos aventuras mas modernas, pero tratamos de no ser influenciados para que no tengan tanto impacto en nuestros desarrollos.


 **JEL: ¿Qué tal el desarrollo para Linux? Por ejemplo comparando con Windows ¿os ha dado más o menos problemas? ¿Usáis algún motor como Unity, Unreal, Godot u otro?**


**Leandro:** Hasta el momento no nos ha traido muchos problemas. Seguramente puede haber alguna configuración de Linux donde pueda haber fallar, pero no hemos tenido muchos reportes. Usamos el motor Unity y nos facilita mucho las cosas para las distintas plataformas. Igualmente el porcentaje de personas jugando en Linux sigue siendo muy bajo, un poco mas del 3%.


![Slender Threads 2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SlenderThreads/Slender_Threads_2.webp)


 **JEL: ¿Qué os parece la escena de juegos en Linux actualmente?**


**Leandro:** Sinceramente no estamos tan interiorisados en el mundo Linux, en el estudio solemos utilizar MacOS y Windows. Pero me da la sensacion que ha crecido la cantidad de juegos ofrecidos en Linux, seguramente motores los motores tengan mucho que ver con esto.


 **JEL: ¿Os habéis planteado preparar vuestras aventuras gráficas para el nuevo PC portátil de Valve?**


**Leandro:** Si, nos encantaría tener Slender Threads para Steam Deck, es uno de los objetivos junto a Nintendo Switch!  
Los casos de Nobodies y Kelvin son un poco más complejos, son desarrollos más viejos y no fueron pensados para el uso del control.  
  



**JEL: Para finalizar nos gustaría que nos comentases algo sobre vuestra nueva aventura gráfica Slender Threads**


**Leandro**: Slender Threads, es una emocionante aventura de suspenso en la que te pones en la piel de Harvey Green, un vendedor ambulante de libros que accede a visitar la vieja y polvorienta Villa Ventana. El viaje pronto hará que Harvey se pregunte si existe el libre albedrío y si hay algo que pueda hacer para cortar los hilos que lo unen a la ciudad, sus habitantes y la oscuridad que se cierne sobre ellos.  
Estamos haciendo todo lo posible para mantener el corazón del género, pero también agregamos algunas características que creemos contribuyen a darle al juego una atmósfera única:


* Slender Threads representa personajes y objetos bidimensionales en un entorno tridimensional para imitar la apariencia de un diorama.Cada objeto está meticulosamente dibujado a mano para mejorar este efecto. Queremos que el juego se sienta como un libro de cuentos viviente.
* Un mundo inquietante pero atractivo, junto con su guión fuera de lugar, creará una experiencia intrigante e impredecible para que los jugadores esten ansiosos por ver su conclusión.
* La posibilidad de interactuar con una gran variedad de personajes excéntricos con intenciones inescrutables.
* Una secuencia de desafiantes acertijos a medida que te acercas cada vez más a la fuente de la oscura influencia de la ciudad.


 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/Wsh0xjuDkSo" title="YouTube video player" width="780"></iframe></div>


  Puedes disfrutar del **prólogo de su próxima aventura grafica totalmente gratis** y no olvides añadirle a tus deseados si te gustó:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1116480/" style="border: 0px;" width="646"></iframe></div>


Recordaros ademas que **disponen de otras dos aventuras: [Infamous Machine](https://store.steampowered.com/app/376520/Kelvin_and_the_Infamous_Machine/) y [Nobodies: Murder Cleaner](https://store.steampowered.com/app/1038280/Nobodies_Murder_Cleaner/).**


¿Os gustan las buenas aventuras gráficas? ¿Has probado ya este magnífico prólogo? Cuéntanoslo en nuestro canales habituales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

