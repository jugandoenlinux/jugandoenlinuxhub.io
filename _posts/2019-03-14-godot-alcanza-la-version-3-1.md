---
author: leillo1975
category: Software
date: 2019-03-14 08:58:14
excerpt: "<p>Las mejoras de este motor gr\xE1fico libre son sustanciales en esta nueva\
  \ versi\xF3n de @godotengine</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c117827dcb434d781be62e3d9512c6d2.webp
joomla_id: 995
joomla_url: godot-alcanza-la-version-3-1
layout: post
tags:
- open-source
- motor-grafico
- godot
- desarrollo
title: "Godot alcanza la versi\xF3n 3.1."
---
Las mejoras de este motor gráfico libre son sustanciales en esta nueva versión de @godotengine

El desarrollo de este software libre de creación de videojuegos es imparable, y en los últimos años su crecimiento ha sido exponencial, pasando de ser una herramienta con unas posibilidades limitadas a una **aplicación capaz de ofrecer al creador opciones que están muy cerca de competir de tu  a tu con gigantes del sector**, pero sin tener que usar soluciones privativas con licencias de uso confusas. Si recordabais a principios del año pasado [os hablábamos de la versión 3.0](index.php/homepage/generos/software/12-software/745-el-motor-de-juegos-godot-llega-finalmente-a-su-version-3-0) que ya mostraba un avance considerable con respecto a la anterior versión.


**Godot permite a los creadores desarrollar videojuegos 2D y 3D** para **multiples plataformas** tanto móviles (Android e iOS), de escritorio (Linux, Windows, MacOS), consolas e incluso web. Permite trabajar con **GDScript**, un lenguaje de programación propio basado en Python, y el **uso de nodos** tanto predefinidos como propios. Entre la gran  multitud de [novedades que incluye la versión 3.1](https://godotengine.org/article/godot-3-1-released) las más importantes a nuestro juicio son las siguientes:


-Soporte de renderizado para OpenGL ES 2.0


-Físicas 3D de "Soft-Body" y un nuevo sistema de "Rag-Doll" 3D


-Habilitación continua en torno a la realidad virtual (RV)


-Escritura opcional en GDScript


-El "inspector" ha sido escrito desde cero para ser más cómodo de usar


-Se han hecho cambios importantes en la interfaz de Godot, como en el editor 2D, en el de TileSet, en el editor de animaciones o en el dock del sistema de archivos


-Mejoras en las cinemáticas de los cuerpos en 2D y 3D


-Nodos para la creación de esqueletos 2D


-Mejoras en el tratamiento de las redes para los modos multijugador.


-Soporte C# mejorado desde la versión 3.0


 


Hablando sobre el **futuro de Godot**, se espera que se lance otra versión (suponemos que la 3.2) en la segunda mitad de este año, y más a largo plazo, a un año vista, la versión 4 que implementaría finalmente la **API de Vulkan**. Os dejamos con un video que os muestra visualmente estas novedades:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/P6nQ3E-Cyfk" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 Como sabeis Godot cuenta con licencia libre (MIT) y [está disponble](https://godotengine.org/download/linux) en multiples sistemas operativos, además de poder descargarse también en [Steam](https://store.steampowered.com/app/404790/Godot_Engine/) [(donde estará disponible la nueva versión en unos dias)](https://store.steampowered.com/app/404790/Godot_Engine/) e [Itch.io](https://godotengine.itch.io/godot). 


¿Sois desarrolladores de videojuegos o habeis hecho vuestros pinitos?¿Qué os parecen las novedades de Godot? Deja tu opinión en los comentarios, o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

