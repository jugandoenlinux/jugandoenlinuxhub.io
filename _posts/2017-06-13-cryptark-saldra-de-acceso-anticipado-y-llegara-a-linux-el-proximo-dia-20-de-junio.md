---
author: Pato
category: "Acci\xF3n"
date: 2017-06-13 17:18:47
excerpt: "<p>Se trata de un fren\xE9tico juego de acci\xF3n 2D con un aspecto fant\xE1\
  stico</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b3a54ecc0915f9347c3f53fa31d161fe.webp
joomla_id: 374
joomla_url: cryptark-saldra-de-acceso-anticipado-y-llegara-a-linux-el-proximo-dia-20-de-junio
layout: post
tags:
- accion
- indie
- proximamente
title: "Cryptark saldr\xE1 de acceso anticipado y llegar\xE1 a Linux el pr\xF3ximo\
  \ d\xEDa 20 de Junio (Actualizaci\xF3n: Ya disponible)"
---
Se trata de un frenético juego de acción 2D con un aspecto fantástico

Gracias a Ethan Lee nos llegan noticias de otro juego. En este caso se trata de Cryptark [[web oficial](http://www.cryptark.com/)], un frenético juego que se define como "*acción roguelike en 2D que desafiará a los jugadores atacando y neutralizando naves extraterrestres proceduralmente generadas para obtener beneficios para su empresa privada*".


Durante el juego tendremos que comprar equipamiento y armamento mejorado para vencer a los cada vez más poderosos enemigos:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/s2Wi2YVQgSo" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Algunas de sus características:


* Complejas naves alien proceduralmente generadas a las que derrotar.
* Desafiante campaña rejugable que probarán tu capacidad estratégica y táctica.
* Unas 60 armas y objetos, incluyendo cañones, rayos tractores y más...
* Ranking de puntuación para competir.
* Banda sonora agresiva perfecta para las intensas batallas.


El desarrollo corre a cargo de Lee Vermeulen y el port parece que es cosa de Ethan Lee a tenor de su comentario en Twitter (y es la que nos ha guiado para descubrir el juego):



> 
> Just sent the release builds of Cryptark... I still remember when it was using Capsized assets as placeholders! <https://t.co/GJZmkaNFYc>
> 
> 
> — Ethan Lee (@flibitijibibo) [June 13, 2017](https://twitter.com/flibitijibibo/status/874645164853276673)



**Actualización**: Cryptark ya está disponible en Linux/SteamOS. Si quieres saber más, puedes visitar la [Wiki del juego](http://cryptark.gamepedia.com/Cryptark_Wiki) o verlo y comprarlo en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/344740/" width="646"></iframe></div>


¿Qué te parece Cryptark? ¿Te gustan los juegos de disparos 2D?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

