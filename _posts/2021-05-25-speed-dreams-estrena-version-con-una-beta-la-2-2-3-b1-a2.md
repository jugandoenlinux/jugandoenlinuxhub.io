---
author: leillo1975
category: Carreras
date: 2021-05-25 13:47:07
excerpt: "<p>El equipo de desarrolladores del simulador acaba de publicarla.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/SD223b1-logo.webp
joomla_id: 1307
joomla_url: speed-dreams-estrena-version-con-una-beta-la-2-2-3-b1-a2
layout: post
tags:
- beta
- open-source
- racing
- speed-dreams
title: "Speed Dreams lanza la RC1 de su nueva versi\xF3n 2.2.3 (ACTUALIZADO) "
---
El equipo de desarrolladores del simulador acaba de publicarla.


****ACTUALIZACIÓN 15-7-21:**** Tras montones de cambios en el código, especialmente en lo que se refiere al **funcionamiento de la IA**, se ha llegado a la primera versión candidata, poniendo el proyecto más cerca de la recta final para el lanzamiento de la 2.2.3 oficial. Como acabamos de decir, se han corregido muchos errores en los robots del juego, principalmente en la **categoría "Supercars"**, por lo que ahora se saldrán menos de la pista y será más difícil superarlos. También permaneced atentos porque previsiblemente muy pronto haya AppImage (gracias a @SonLink) que encontrareis donde ya están los [archivos actualizados del juego](https://sourceforge.net/projects/speed-dreams/files/2.2.3/).  





---


 **ACTUALIZACIÓN 2-6-21:** Desde hace un ratito de nada se puede descargar el juego en **formato de AppImage**. Gracias al inestimable trabajo y esfuerzo de nuestro amigo **@SonLink**, miembro destacado de nuestra comunidad de JugandoEnLinux.com, **es posible testear y jugar a esta ultimísima versión beta de Speed Dreams**, sin necesidad de compilar o buscar repositorios.


Para quien no conozca este formato, se puede decir que es un tipo de **paquetería que permite ejecutar programas sin tener que instalarlos**, simplemente ejecutándolos haciendo doble click en ellos. Serían algo parecido a los programas portables de Windows. Esta forma de empaquetado **facilita la distribución de binarios entre las diferentes distribuciones** de Linux, pues están creadas para funcionar sin problemas sea cual sea la que usemos.


Podeis [descargar la AppImage](https://sourceforge.net/projects/speed-dreams/files/2.2.3/2.2.3-b1/) para **sistemas de 64 bits** del juego en la web del proyecto en SourceForge. Para que funcione simplementes debeis **marcar el archivo como ejecutable**. Normalmente en las propiedades del archivo existe el checkbox que lo permite, si no basta con aplicar el siguiente comando:




`*chmod u+x Speed-Dreams-2.2.3-b1-x86_64.AppImage*`


También nos gustaría decir que **@SonLink está trabajando en la versión para sistemas de 32 bit**, que encontrareis en unos días en el mismo sitio. Recordad una vez más que **se trata de una versión beta y que si encontrais fallos debeis reportarlos al equipo de desarrollo** como podeis ver un poco más abajo.




---


  
**NOTICIA ORIGINAL (25-5-21):**  
Hace más o menos año y medio desde que este **simulador de coches de carreras #OpenSource** lanzase su última versión oficial, la [2.2.2]({{ "/posts/speed-dreams-disponible-en-flatpak-1" | absolute_url }}), que tenía como principales novedades el tener soporte de Force Feedback, poder usar el nuevo motor de renderizado OpenSceneGraph o un webserver que registra nuestros tiempos. Desde entonces no ha habido noticias desde el equipo de desarrollo, pero eso no quiere decir que hayan estado quietos ni mucho menos, pues el trabajo realizado a lo largo de este tiempo ha sido intenso y doy fe de ello.


Pero finalmente, tras el arduo trabajo, la gente detrás de Speed Dreams ya tiene para nosotros esta nueva versión (beta), la 2.2.3-b1, en la que a parte de una **cantidad ingente de bugs corregidos y mejoras** en su software se destacan los siguientes cambios:


*-Nueva pista de Gran Premio, la de **Sao Paulo**, basada en el circuito José Carlos Pace, o más comunmente conocido como "Interlagos".*


*-Nueva categoría y colección de coches "**Monoposto 1"** (MP1) basada en los monoplazas de la Formula 1 de 2005.*


*-Nueva categoría y colección de coches "**1967 Grand Prix**" (67MP1), basada en la Formula 1 de 1967.*


*-Nuevos modelos en la categoría Supercars, el **Deckard Conejo RR** (basado en el Chevrolet Camaro SS de 2010) y el **Kanagawa Z35** (basado en Nissan 350z)*


*-Añadido soporte para **desgaste y degradación de neumáticos**, por ahora solo presente en Monoposto 1.*


*-**Nuevos robots de IA "shadow"** en múltiples categorias. Son más rápidos y arriesgan frenando más tarde que otros robots.*


*-Actualizado el código de los robots de IA "dandroid" y "USR".*


*-Se puede configurar el **tiempo real en los circuitos basándose en la meteorología** actual de las distintas localizaciones. (Por ejemplo, si en el momento justo llueve en Sao Paulo, el juego consulta el tiempo real en esa zona y lo reproduce en el juego).*


*-Nueva **sección de "setups" en los menús del garage** para configurar a nuestro gusto las diferentes opciones posibles en la mecánica de los automóbiles.*


Para quien no lo conozca, [Speed Dreams](http://www.speed-dreams.net) es un fork del también conocido y primigenio [TORCS](torcs.sourceforge.net), pero mucho más avanzado, donde el jugador disfrutará conduciendo montones de coches de diferentes categorias en múltiples pistas, y pudiendo disputar sesiones de **practica**, **carrera rápida**, **campeonatos** o incluso un **modo de carrera profesional**, además del modo **multijugador**, que permite el juego a **pantalla partida** y a través de la **LAN** (sobre internet por ahora es muy inestable).


![speed dreams 20 car ls1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/speed-dreams-20-car-ls1.webp)


Podeis descargar el [código de esta última versión](https://sourceforge.net/projects/speed-dreams/files/2.2.3/2.2.3-b1/) en su web del proyecto en Sourceforge, donde encontrareis tanto el **juego base**, como los diferentes **añadidos de coches y pistas**. También será posible para los **usuarios de Ubuntu y derivadas** descargar el juego usando el [PPA de Xtradeb]({{ "/posts/xtradeb-un-ppa-de-juegos-libres-para-ubuntu-y-derivadas" | absolute_url }}), y muy posiblemente, más adelante en formato **AppImage**. Recordad que en nuestra **sección de Tutoriales** de nuestros foros escribimos hace tiempo un [post donde os indicamos como compilar este juego](index.php/foro/tutoriales/108-como-compilar-la-ultima-version-de-speed-dreams-gracias-gnuxero26). También os recordamos que **el juego está aún en su primera beta y que es susceptible de albergar fallos** , por lo que si encontrais alguno os pedimos encarecidamente que lo reporteis en un [Ticket en la web de su proyecto](https://sourceforge.net/p/speed-dreams/tickets/), o a través de su sala de chat en [Jabber](https://chat.jabberfr.org/converse.js/speed-dreams-dev@chat.jabberfr.org).


También nos hacemos eco aquí de la [petición de ayuda realizada a través de Reddit](https://www.reddit.com/r/opensourcegames/comments/n3tln6/speed_dreams_needs_you_call_for_devs/) para conseguir más desarrolladores que ayuden en el proyecto, por lo que os sentís capacitados para colaborar, vuestro trabajo sería bien recibido. Os dejamos con un video que acabamos de grabar donde podeis ver todas estas novedades que os acabamos de describir:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/wwQxyzsAkUM" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


Os iremos informando de las sucesivas versiones de testeo, así como de la versión final aquí, en JugandoEnLinux.com. ¿Qué os parecen las mejoras introducidas en esta beta? ¿Habeis jugado con anterioridad a Speed Dreams? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux) o nuestras salas de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

