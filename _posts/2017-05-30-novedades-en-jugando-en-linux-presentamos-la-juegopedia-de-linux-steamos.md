---
author: Jugando en Linux
category: Editorial
date: 2017-05-30 12:27:07
excerpt: "<p>Tambi\xE9n comentamos las novedades que hemos implementado durante las\
  \ \xFAltimas semanas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0e8bfd1d071657cbc63f9ace1550f1f3.webp
joomla_id: 349
joomla_url: novedades-en-jugando-en-linux-presentamos-la-juegopedia-de-linux-steamos
layout: post
title: 'Novedades en Jugando en Linux: presentamos "La Juegopedia" de Linux/SteamOS'
---
También comentamos las novedades que hemos implementado durante las últimas semanas

Aunque parezca que no, en "Jugando en Linux" no nos podemos estar quietos. Es por esto que hemos creado una "Juegopedia" que permitirá visualizar lo mucho que hemos avanzado en cuestión de juegos en Linux/SteamOS, y además descubrir una multitud de juegos excelentes que seguro que no sospechabais que tenemos disponibles en nuestro sistema favorito. Ahora ya no nos podrán decir que no tenemos excelentes juegos en Linux/SteamOS, ¿verdad?. Gracias a nuestro colaborador "Daniel" tenéis ya la nueva sección en el menú principal, o [clica en este enlace](index.php/top-linux).


Por otra parte, durante las últimas semanas hemos puesto en marcha otras novedades como el [canal oficial de Twitch](https://www.twitch.tv/jugandoenlinux) de Jugando en Linux, hemos abierto un [canal oficial en Youtube](https://www.youtube.com/channel/UC4FQomVeKlE-KEd3Wh2B3Xw), hemos implementado mejoras en el sistema de acceso a la web y enlaces directos a los tutoriales donde podéis encontrar cómo instalar los últimos drivers gráficos. También hemos actualizado el sistema, y hemos implementado mejoras en la plantilla para que sea más adecuada a los que nos visitan vía tablets o móviles.


Aún tenemos mucho que mejorar. Queremos implementar mejoras a todos los niveles, pero como dice el dicho, despacito y buena letra. Muy pronto tendremos más novedades. Hasta entonces, disfrutar de vuestra web: [jugandoenlinux.com](index.php)

