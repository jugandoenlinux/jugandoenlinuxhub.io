---
author: leillo1975
category: Estrategia
date: 2017-05-15 10:24:29
excerpt: "<p>Feral Interactive nos vuelve a sorprender con otro t\xEDtulo de Creative\
  \ Assembly</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/acaf69a47291143b36612c5b483fc572.webp
joomla_id: 320
joomla_url: total-war-shogun-2-y-fall-of-the-samurai-el-23-de-mayo
layout: post
tags:
- feral-interactive
- total-war
- shogun-2
- fall-of-the-samurai
title: "\xA1\xA1\xA1Total War: Shogun 2 y Fall of the Samurai el 23 de Mayo!!! (ACTUALIZADO-Requisitos)"
---
Feral Interactive nos vuelve a sorprender con otro título de Creative Assembly

Se venía venir desde hace tiempo, pero aun así no deja de llamarnos la atención la cantidad de buenos títulos que podemos disfrutar en GNU-Linux/SteamOS gracias a [los chicos de Feral](https://www.feralinteractive.com/en/mac-games/shogun2twfots/). A pesar de que es un título con un importante bagaje a sus espaldas (recordemmos que salió por el 2011), seguro que una gran mayoría de nosotros se alegra de poder disfrutar una vez más con un título de Creative Assembly más en nuestras bibliotecas. Aquí tenemos el anuncio en Twitter:


 



> 
> A brave new Japan —Total War: SHOGUN 2 and Fall of the Samurai come to Linux on 23rd May. Discover the minisite: <https://t.co/xztigRweRB> [pic.twitter.com/Jj0of40Jvq](https://t.co/Jj0of40Jvq)
> 
> 
> — Feral Interactive (@feralgames) [May 15, 2017](https://twitter.com/feralgames/status/864059785490116609)



 


En este caso nos trasladaremos al antiguo Japón para dirigir nuestro clan y comandar ejercitos de Samurais. Tras una muy exitosa primera parte, repleta de buenas críticas, y horas y horas de juego, podremos al fin disfrutar en nuestros sistemas de este genial juego de estrategia. Como muchos sabreis la serie de juegos Total War nos da la posibilidad de gestionar nuestra facción (economía, producción, diplomacia) utilizando un sistema de turnos; y dirigir en tiempo real los combates entre los diversos ejercitos. También podremos disfrutar de su expansión Fall of the Samurai que saldrá conjuntamente.


 


La verdad es que [había muchas señales de que este lanzamiento se iba producir](index.php/homepage/editorial/item/433-el-radar-de-feral-interactive-vuelve-a-detectar-otro-lanzamiento-y-esta-vez-esta-muy-cerca), ya que hace poco nos daban pistas de este lanzamiento y los [movimientos en SteamDB](https://steamdb.info/app/34330/history/) así lo atestiguaban. El juego hará uso de OpenGL según [nos comentan en esta respuesta a su tweet](https://twitter.com/feralgames/status/864079350332882945) Por ahora no tenemos más datos que ofreceros tales como los requerimientos técnicos, pero según los vayamos confirmando os informaremos en este mismo artículo.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/vjBjvMb2LBA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


¿Qué te parece este nuevo lanzamiento de Feral? ¿Te gusta la serie Total War? Cuentanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)


 


ACTUALIZACIÓN: Feral acaba de comunicar mediante twitter que realizará un streaming a través de su canal de twitch para mostrarnos el juego. Dicha restransmisión será el miercoles a las 19:00 hora española. Aquí os dejo el tweet donde se anuncia:


 



> 
> 17th May, 6PM BST / 10AM PDT.[#FeralPlays](https://twitter.com/hashtag/FeralPlays?src=hash) Total War: SHOGUN 2 on Linux, mastering the art of war, live on Twitch. <https://t.co/cuzZ9AMVuq> [pic.twitter.com/7SRvPTIarz](https://t.co/7SRvPTIarz)
> 
> 
> — Feral Interactive (@feralgames) [May 15, 2017](https://twitter.com/feralgames/status/864118385541558272)



**ACTUALIZACIÓN - Requisitos:**


Feral acaba de publicar en su blog los requisitos del sistema para jugar a SHOGUN 2:


**Requisitos mínimos:**


Procesador 2GHz con SteamOS 2.0 / Ubuntu 16.04 o posterior  
4GB de RAM  
Tarjeta gráfica Nvidia 600 series / AMD 6000 series / Intel Iris Pro graphics de 1GB o superior


**Requisitos recomendados:**


Procesador 3GHz con SteamOS 2.0 / Ubuntu 16.04 o posterior  
4GB de RAM   
Tarjeta gráfica Nvidia 700 series / AMD R7 series de 2GB o superior


**Nota:** Las GPUs de NVIDIA requieren la versión del controlador 375.26 o posterior. Las GPUs de AMD e Intel requieren MESA 17.1.

