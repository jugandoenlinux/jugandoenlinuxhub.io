---
author: Pato
category: Noticia
date: 2019-12-19 21:36:24
excerpt: "<p>Miles de juegos para Linux rebajados por Navidad</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamoferta/Steamwintersale2019.webp
joomla_id: 1141
joomla_url: ya-estan-aqui-las-rebajas-de-invierno-de-steam
layout: post
title: "Ya est\xE1n aqu\xED las rebajas de invierno de Steam"
---
Miles de juegos para Linux rebajados por Navidad


Como ya viene siendo tradición, Steam vuelve a ponerse de rebajas para tentarnos y hacer sufrir a nuestras ya maltrechas carteras.


Para estas **rebajas de invierno**, Steam nos presenta el **mercado festivo de Steamville**, donde podremos obtener fichas ya sea comprando juegos o completando misiones para luego poder gastarlas en cromos, pegatinas, perfiles, emoticonos, fondos y otras cosas que no nos servirán de nada pero quedarán chulas en nuestros perfiles o chats.


Además, también podemos conseguir la típica insignia de las rebajas de invierno de Steam, y también podemos conseguir descuentos con nuestras fichas. Tenéis toda la información en <https://store.steampowered.com/holidaymarket>


Pero vamos a lo que importa... Algunas de las rebajas más interesantes que podemos encontrar:


[Northgard al 60% de descuento](https://store.steampowered.com/app/466560/Northgard/?snr=1_4_wintersale__617)


[Pillars of Eternity: Deadfire al 50% de descuento](https://store.steampowered.com/app/560130/Pillars_of_Eternity_II_Deadfire/?snr=1_4_wintersale__617)


[Life is Strange 2 (SI!) con un 75% de descuento!](https://store.steampowered.com/app/532210/Life_is_Strange_2/?snr=1_4_wintersale__617) (1,99€)


[XCOM 2 al 75% de descuento](https://store.steampowered.com/app/268500/XCOM_2/?snr=1_4_wintersale__617)


[Borderlands 2 al 75% de descuento](https://store.steampowered.com/app/49520/Borderlands_2/?snr=1_4_wintersale__617) 


[Everspace al 85% de descuento](https://store.steampowered.com/app/396750/EVERSPACE/?snr=1_4_wintersale__617)


Y muchos... muchos mas!!


¿Qué estáis esperando? las rebajas son en <store.steampowered.com>


![SteamGaveRebajas](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamoferta/SteamGaveRebajas.webp)


 

