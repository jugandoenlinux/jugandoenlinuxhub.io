---
author: leillo1975
category: Estrategia
date: 2019-09-02 09:48:17
excerpt: "<p>Importantes mejoras en este juego de c\xF3digo libre</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ea3d3fde3158404e736694c1ce93ec41.webp
joomla_id: 1107
joomla_url: warzone-2100-se-actualiza-a-la-version-3-3-0
layout: post
tags:
- rts
- estrategia
- open-source
- warzone-2100
- codigo-abierto
title: "\"Warzone 2100\" se actualiza a la versi\xF3n 3.3.0."
---
Importantes mejoras en este juego de código libre

Probablemente la mayoría de vosotros conozcais este magnífico juego de estrategia en tiempo real, pero por si acaso queda alguno que no tenga esa suerte, hay que decir que **es uno de los mejores exponentes que tenemos en los juegos de código abierto**. Warzone 2100 fué **lanzado como juego comercial a finales de los años 90** por la compañía Pumpkin Studios. para 5 años después, en el año 2004, ellos mismos liberarlo para que la comunidad Open Source continuase trabajando sobre él.


El juego en su lanzamiento original obtuvo una muy **buena recepcción por parte de la prensa**, presentando unos **gráficos 3D** muy cuidados y poco frecuentes en juegos de estrategia en aquel momento. En el podemos ver un mundo destruido por un cataclismo nuclear, donde los pocos humanos que quedan pelean entre si por recuperar el control de la tecnología y los recursos, existiendo dos bandos, el "Proyecto", que compone las fuerzas que comandas en la campaña, y "Nuevo Paradigma". El juego ofrece **modos de campaña, multijugador y escaramuza** para un solo jugador. Un **amplio árbol tecnológico** con más de 400 tecnologías diferentes, combinado con el **sistema de diseño de unidades**, permite una amplia variedad de unidades y tácticas posibles.


Hace un par de días, el equipo de desarrollo de este juego, comunicaba que trás tres años desde la última versión estable, finalmente alcanzaban la **versión 3.3.0**, y entre otras muchas cosas estos cambios incluyen el escalamiento de Display/UI, mejoras en el rendimiento, mejoras importantes en la campaña, el retorno de órdenes secundarias eliminadas en la versión anterior, la corrección de disparos a través de las paredes, el retorno de las builds de Macosx, y muchas, muchas otras cosas, como podeis ver en las [notas del lanzamiento en sus foros](http://forums.wz2100.net/viewtopic.php?f=1&t=15647).


Si os interesa jugar su última versión, podeis descargar el código fuente desde sus páginas en [Sourceforge](https://sourceforge.net/projects/warzone2100/files/releases/3.3.0/) y [Github](https://github.com/Warzone2100/warzone2100/releases/tag/3.3.0), o bajaros el **Snap** recien actualizado que acaba de ser puesto en [Snapcraft.io](https://snapcraft.io/warzone2100). Os dejamos con un gameplay del canal de Youtube de nuestro amigo **@SonLink**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/CN_EGFteFCo" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Habeis jugado ya a este clásico de la estrategia? ¿Os apuntaríais a unas partidas multijugador? Cuéntanoslo en los comentarios o deja tus mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

