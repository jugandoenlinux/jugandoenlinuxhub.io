---
author: leillo1975
category: Carreras
date: 2018-02-26 14:17:10
excerpt: <p>Ya tenemos fecha oficial de salida</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d1ed2bf3d11f7e7a6f189af32a153713.webp
joomla_id: 659
joomla_url: trailblazers-llegara-esta-primavera-a-gnu-linux-steamos
layout: post
tags:
- supergonk
- rising-star-games
title: "Trailblazers llegar\xE1 esta primavera a GNU-Linux/SteamOS (ACTUALIZACI\xD3\
  N)"
---
Ya tenemos fecha oficial de salida

 **ACTUALIZADO (24-4-18)**: Desde Twitter nos llega lo noticia de que el próximo **8 de Mayo** podremos disfrutar de este veloz y colorista juego. También han estrenado la página de la tienda en [Steam](https://store.steampowered.com/app/621970/Trailblazers/). Podeis ver el Tweet justo aquí debajo:


 The [#Trailblazers](https://twitter.com/hashtag/Trailblazers?src=hash&ref_src=twsrc%5Etfw) [#Steam](https://twitter.com/hashtag/Steam?src=hash&ref_src=twsrc%5Etfw) page is now live - [#Wishlist](https://twitter.com/hashtag/Wishlist?src=hash&ref_src=twsrc%5Etfw) for May 8th! <https://t.co/zm8Q2k9wF6> [#TrailblazersGame](https://twitter.com/hashtag/TrailblazersGame?src=hash&ref_src=twsrc%5Etfw) [#MadeWithUnity](https://twitter.com/hashtag/MadeWithUnity?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/cEq7hxMKNq](https://t.co/cEq7hxMKNq)



> — Trailblazers (@TrailblazersEN) [24 de abril de 2018](https://twitter.com/TrailblazersEN/status/988762443009708034?ref_src=twsrc%5Etfw)



 Os dejamos con un video gameplay del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/V8Df_HWYN8I" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL**: Imaginaos que mezclais Splatoon con Wipeout , pues más o menos eso es lo que nos vamos a encontrar en este juego desarrollado por [Supergonk](http://www.trailblazersgame.com/) (compañía formada por profesionales que han trabajado en Bizarre Creations, Codemasters, Lionhead y Criterion) y editado por [Rising Star Games](http://www.risingstargames.com/eu/). Recientemente han anunciado su llegada durante esta primavera, aunque aun no han aclarado su fecha definitiva. El la página web oficial del juego presenta un Tux (más claro agua), por lo que parece que las intenciones de lanzarlo para nosotros están más que demostradas, y también, por supuesto, es de agradecer.


![charactersalltogether](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Trailblazers/charactersalltogether.webp)


El juego, desarrollado con el motor gráfico **Unity**, posee una vertiente claramente multijugador donde podremos enfrentarnos en **equipos de 3 contra 3**, y donde además es posible incluso jugar con amigos **dividiendo la pantalla**. Podremos jugar tanto a través de una **LAN** o de forma **online** haciendo uso del **Matchmaking**. También **dispodrá de un modo historia para un solo jugador** con episodios con diferentes objetivos para completar. La mecánica del juego es lo más original, pues tendremos que pintar la pista para conseguir impulsarnos y conseguir más velocidad y puntos, por lo que una buena coordinación con otros miembros de nuestro equipo es básica para aventajar al contrario.


Presenta unos gráficos originales y cuidados que hacen uso de la técnica del **celshading**, recordando a títulos como Borderlands 2 o XIII, solo que con una **estética mucho más colorista y desenfadada**. Los personajes serán humanos, aliens y robotś psicopatas. En cuanto al apartado sonoro vendrá acompañado de temas de "Future Punk" de artistas indies. Esperemos que podamos jugarlo más pronto que tarde, pues la verdad es que el juego "pinta" (nunca mejor dicho) muy bien, y por lo que podremos apreciar en el el siguiente video puede ser extremadamente divertido:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/-iO71aM2BKs" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


En JugandoEnLinux.com estamos muy pendientes de este juego, y cualquier novedad que conozcamos os la comunicaremos lo antes posible. Puedes darnos tus impresiones sobre este "Trailblazers" en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

