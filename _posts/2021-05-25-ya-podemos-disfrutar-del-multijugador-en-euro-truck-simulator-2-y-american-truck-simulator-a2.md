---
author: leillo1975
category: "Simulaci\xF3n"
date: 2021-05-25 08:20:07
excerpt: "<p>Con la versi\xF3n 1.41, @SCSsoftware permite el juego en Convoys en sus\
  \ juegos.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/SCSATSConvoy900.webp
joomla_id: 1306
joomla_url: ya-podemos-disfrutar-del-multijugador-en-euro-truck-simulator-2-y-american-truck-simulator-a2
layout: post
tags:
- multijugador
- beta
- american-truck-simulator
- scs-software
- online
- euro-truck-simulator-2
title: "Ya podemos disfrutar del multijugador en Euro Truck Simulator 2 y American\
  \ Truck Simulator (ACTUALIZACI\xD3N)"
---
Con la versión 1.41, @SCSsoftware permite el juego en Convoys en sus juegos.


**ACTUALIZADO 13-7-21:** Por ahora solo es oficial en [American Truck Simulator,](https://twitter.com/SCSsoftware/status/1414889302157312003) pero muy probablemente en pocas horas también lo sea en Euro Truck Simulator 2. El hecho es que la [versión 1.41](https://blog.scssoft.com/2021/07/american-truck-simulator-141-release.html) ya se está instalando automáticamente en nuestros PCs a través de Steam, lo cual permite entre otras muchas cosas, disfrutar de los convoys multijugador de forma normal, sin instalar versiones beta. A partir de ahora hasta 8 jugadores  tendrán la oportunidad de unirse en la carretera, pero espera que en un futuro el número máximo de participantes en un convoy se amplie, y por ahora estas son las **características que se pueden disfrutar en este modo**:


*-Cualquier jugador puede organizar y crear sus propias sesiones privadas para un máximo de 8 personas*  
 *-Posibilidad de transportar juntos los mismos contratos/trabajos de la misma empresa y al mismo destino*  
 *-Tráfico sincronizado de la IA*  
 *-Hora y clima sincronizados*  
 *-Sincronización de trabajos de pintura, accesorios del camión, accesorios de la cabina, cargas y más*  
 *-Posibilidad de comunicarse con otros conductores a través de la radio CB (tecla X), respuestas rápidas (tecla Q) o chat de texto (tecla Y)*


También aun existen una serie de **problemas que no han sido solucionados** y que se espera corregir más adelante:


*-Los mods en el modo Convoy no son compatibles con la versión inicial.*  
 *-Los vehículos de la IA pueden desaparecer cuando un jugador se desconecta.*  
 *-Los puentes elevadores no están sincronizados. Serán estacionarios en la versión inicial de Convoy.*  
 *-Las invitaciones de Steam sólo funcionan a través de la superposición de Steam.*  
 *-La emisión de radio CB (tecla de acceso directo "X") interfiere con la línea de entrada de texto.*


Puedes ver el trailer que acaba de publicar en Youtube la compañía checa:


 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/Md8F1ZPRyIw" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


Ahora que ya será para todo el mundo no estaría mal poder disfrutar en grupo de nuestras carreteras favoritas, ¿verdad? Si quereis organizaros para disfrutar de uno de estos Convoys online podeis usar nuestro grupo de [Telegram](https://t.me/jugandoenlinux) o nuestras salas de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 




---


**NOTICIA ORIGINAL 25-5-21:**


Sabemos que muchos de los que estais leyendo esta noticia sois grandes aficionados a los juegos de la compañía checa [SCSsoftware](https://scssoft.com/), y disfrutais enormemente de conducir virtualmente por las carreteras de Europa y EEUU. Pues, ¿qué os parecería poder hacerlo en compañía de vuestros amigos? Esa es la nueva posibilidad que podemos empezar ya a probar en estos juegos.


Es posible que muchos ya conozcais la existencia de un **fantástico mod que permite el juego online**, [TruckersMP](https://truckersmp.com/), y que es posible jugarlo en Linux, aunque con bastante trabajo y no de forma nativa gracias a Wine y [TruckersMP-cli](https://github.com/truckersmp-cli/truckersmp-cli). Pues **desde ayer esta funcionalidad estará incluida de forma experimental si instalamos la beta 1.41**, empezando en este caso por American Truck Simulator (en ETS2 llegará más adelante).


![SCSIberiaConvoy900](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/SCSIberiaConvoy900.webp)


Según lo que podemos leer en este [mensaje oficial](https://forum.scssoft.com/viewtopic.php?f=214&t=300266) en los foros de SCS, en este nuevo modo de juego **podremos configurar o unirnos a sesiones creadas por nuestros amigos o partidas públicas**, hasta un **máximo de 8 camioneros.** Para ello se han hecho trabajos para sincronizar el tráfico de la IA, el tiempo y la meteorología, así como el trabajo realizado por todos, pudiéndose volver al progreso anterior al juego online, por si cualquier razón no se estuviese satisfecho con lo realizado durante el Convoy. Según la descripción dada por SCS esto es lo que nos vamos a encontrar en esta primera beta:


***La lista de funciones actual incluye:*** 


* *Cualquier jugador puede albergar y crear sus propias sesiones privadas para hasta 8 personas.*
* *Capacidad para transportar los mismos contratos / trabajos juntos desde la misma empresa y al mismo destino*
* *Tráfico de IA sincronizado*
* *Tiempo y clima sincronizados*
* *Trabajos de pintura sincronizados, accesorios para camiones, accesorios de cabina y más*
* *Capacidad para comunicarse con otros conductores a través de transmisión de radio CB (tecla X), respuestas rápidas (tecla Q) o chat de texto (tecla Y)*


***Problemas notables / aún no implementados:***


* *Los mods no son compatibles con la versión inicial.*
* *Los jugadores pueden unirse actualmente solo si tienen el mismo conjunto de DLC de carga y mapas instalados que el anfitrión.*
* *El tiempo no está completamente sincronizado (solo buen / mal tiempo).*
* *Los cables para remolques remotos no se renderizan.*
* *Al usar un remolque propio, es posible que haya un remolque de la empresa generado en el mismo lugar al que se supone que debe ir para cargar su carga.*
* *Los vehículos de IA pueden desaparecer cuando un jugador se desconecta.*
* *Los puentes de elevación no están sincronizados. Estarán estacionarios para el lanzamiento inicial de Convoy.*
* *Los sonidos para camiones de otros jugadores son un trabajo en progreso.*
* *Aún no se permite compartir los contratos externos de World of Trucks.*
* *Steam invita a trabajar solo a través de la superposición de Steam.*
* *La transmisión de radio CB (tecla de acceso rápido "X") interfiere con la línea de entrada de texto.*
* *La carga en una plataforma de propiedad a veces puede cruzarse con el remolque*
* *El mapa en pantalla en Convoy "M" se aleja junto con el desplazamiento del chat en el Asesor de ruta.*


***Cambios / limitaciones previstos durante la sesión de convoy:*** 


* *La tasa de progresión del tiempo no cambia al ingresar a una ciudad.*
* *El tiempo en una sesión de Convoy avanzará para coincidir con el tiempo del anfitrión, pero es solo visual: el tiempo económico no se ve afectado (si le quedaban 2 horas para terminar el trabajo antes de unirse al Convoy, aún debería tener el mismo tiempo restante para terminar su trabajo).*
* *No se permiten trabajos especiales de transporte. No puede unirse a una sesión de Convoy con un trabajo de este tipo activo.*
* *Las colisiones dinámicas están desactivadas para los vehículos estacionados (no puede empujar un vehículo estacionado, pero aún chocará con él).*
* *Las señales de tráfico derribadas no se reproducen visualmente para otros jugadores; sus colisiones se simulan localmente y funcionan solo en las proximidades del jugador.*
* *Las puertas de peaje no están sincronizadas, sin embargo, la apertura se basa en la proximidad como para los vehículos de IA.*
* *Los colores de los vehículos de IA pueden diferir después de un tiempo (por lo que los vehículos almacenados en caché se pueden reutilizar para limitar el consumo de memoria).*


***Otras características que vienen con 1.41:***


* *****Ajustes de tiempo y clima en modo foto*****
* *****Viaje rápido*****
* *****Reskin de California parte 1 que incluye 7 estaciones agrícolas y una remodelación del diseño de la carretera*****
* *****Nuevos iconos en la leyenda del mapa*****


Ahora solo falta que lo probeis y nos deis vuestra opinión en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux) o nuestras salas de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org). La verdad es que estaría genial que pudieramos quedar un día para echar unos "camiones" y viajar todos juntos con nuestros camiones. Os dejamos con este video donde podeis ver las novedades de esta versión 1.41 Beta:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/RlJmRHBLSm0" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


 Podeis comprar American Truck Simulator y Euro Truck Simulator en la [Humble Store](https://www.humblebundle.com/store/search?sort=bestselling&search=SCS%20Software&partner=jugandoenlinux) (**patrocinado**)


 

