---
author: Jugando en Linux
category: Editorial
date: 2017-10-26 17:54:03
excerpt: "<p>Desgranamos algunas claves analizando la situaci\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/97d1d3d17841d6d3353ed857f6573ac4.webp
joomla_id: 504
joomla_url: esta-creciendo-lo-suficiente-el-juego-en-linux
layout: post
tags:
- steamos
- steam
- valve
- linux
- steam-machines
title: "\xBFEst\xE1 creciendo lo suficiente el juego en Linux?"
---
Desgranamos algunas claves analizando la situación

"La que has liado pollito, la que has liado..." y direis, esto a cuento de que viene. Pues basicamente de un "inocente" tweet que publicaba ayer por la tarde un tal [Ryan C. Gordon](http://icculus.org/) (vease el tono irónico). Si queda alguno que no sepa quien es este señor, solo hay que decir que es [una celebridad en nuestro mundillo](https://en.wikipedia.org/wiki/Ryan_C._Gordon) y es conocido por portar innumerables juegos a nuestro sistema (Quake III Arena, Unreal Tournament, Super Meat Boy, Killing Floor, Braid, Sanctum 2....). El tweet en cuestión es este:


 



> 
> Sorry, we just don't think Linux is a big enough market. Instead, we redesigned the whole game for the four people with a Vive headset.
> 
> 
> — Ryan C. Gordon (@icculus) [25 de octubre de 2017](https://twitter.com/icculus/status/923201260819550209?ref_src=twsrc%5Etfw)



 


Lo que nadie se esperaba es que el mismisimo [Pierre Loup Griffais](https://twitter.com/Plagman2) entrase a trapo en la conversación y afirmara lo siguiente:


 



> 
> VR is already a bigger market. Maybe Linux users could try actually buying games if they wanted people to port them!
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [25 de octubre de 2017](https://twitter.com/Plagman2/status/923304477049810944?ref_src=twsrc%5Etfw)



 


Resumiendo en español, lo que Ryan dice con ironía es que las desarrolladoras no tienen dinero para invertir en Linux por que no hay suficiente mercado, pero en cambio si pueden rediseñar un juego completo para cuatro gatos que tienen un HTC Vive. La respuesta de Pierre da un dato muy controvertido aseverando que el mercado de Realidad Virtual es ya mayor que el de Linux y que los usuarios de Linux deberían comprar más juegos si quieren que la gente siga portándolos. El caso es que Pierre Loup Griffais es uno de los desarrolladores para Linux más importantes que tiene Valve, por lo que la relevancia de sus palabras es aun mayor.


 


A cuenta de todo esto la editorial de JugandoEnLinux tuvo un debate en el que se pusieron cartas muy interesantes sobre la mesa sobre la situación actual de los juegos en Linux, y sobre todo esto que hablamos va este artículo editorial. La autoría de este no corresponde a ningún redactor en concreto, si no a todos.


 


Una cosa está clara, mejor o peor, **si  estamos donde estamos es gracias a Steam**. Como se ha dicho en reiteradas ocasiones, si Valve no diese soporte a su cliente para nuestro sistema jamás llegaríamos a donde hemos llegado. Puede que muchos opinen que deberíamos estar en una situación más adelantada, pero es indudable que el avance es enorme. La mejora que hemos experimentado es evidente, y de pasar de contar los juegos con los dedos de la mano, pasamos ya hace tiempo de los 3000 títulos, lo que supone un porcentaje bastante importante del total de títulos de la plataforma. Pero esas mejoras también nos han llegado del soporte ofrecido en el Hardware, con mejoras enormes en drivers, dispositivos de control, tecnologías....A nadie se le escapa que podemos disfrutar del Steam Controller, Steam Link, HTC Vive, etc... gracias  a todo lo que se ha hecho en gran medida por parte de Valve, y del empujón para la comunidad y los desarrolladores que ha supuesto dicha implicación.


 


![steam machines controller vr link copy](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/steam-machines-controller-vr-link_copy.webp)


*La inversión en desarrollo de hardware innovador por parte de Valve ha sido considerable*


 


Pero no todo es positivo. Aunque todo lo comentado antes sea un antes y un después en lo que se refiere a los juegos y Linux, hay algunas sombras evidentes en todo ello. Desde hace algún tiempo **hay una "sensación" de que el mundo del gaming en nuestro sistema no acaba de despegar**. Las encuestas de Steam revelan una tendencia negativa del uso de Linux/SteamOS para la plataforma. Si bien estos datos pueden ser engañosos, pues no quiere decir que haya bajado el número de usuarios (los totales crecieron mucho), y además la encuesta no salta en SteamOS ni en Big Picture; no deja de preocupar esa tendencia. Además, es notable el parón que se percibe en el desarrollo de SteamOS. Todo ello si lo unimos a la falta de promoción de este último y su uso en las Steam Machines, no hace si no, acrecentar las dudas.


 


Si volvemos a la afirmación de Pierre Loup Griffais, en la que **da a entender que la culpa de que no haya más juegos en Linux es de los  usuarios**, la opinión de JugandoEnLinux es que basicamente es la pescadilla que se muerde la cola. Los compañías no desarrollan o portan juegos por que hay pocos usuarios, y los usuarios no pueden comprar juegos por las compañías no las portan. Es un punto de vista conservador y poco arriesgado por parte de estas últimas, que muy legitimamente invierten su dinero en mercados más poblados, pero cierto es también que **no puedes abrir un mercado si no pones la carne en el asador**. Concretamente el problema no es la cantidad de juegos, si no la "calidad" de estos. Entrecomillo calidad porque la enorme mayoría de nuestro catálogo se corresponde con juegos de los llamados Indies, donde encontramos grandes ejemplos de juegos impresionantes, pero obviamante no con el mismo tirón que los AAA. Y es que estos últimos son los que inclinan la balanza y los que hacen que la gran mayoria de los usuarios se pueda tomar en serio la decisión de elegir una plataforma/sistema u otra. Tal vez, y esto es una opinión, si tuvieramos más juegos AAA, el crecimiento nuestra plataforma sería semejante al de usuarios de Linux, que no ha parado de crecer en los últimos años. Se dice que este último ronda entre el 3 o 4%, por lo que lo normal es que el uso de Steam en Linux siguiese los mismos patrones.


 


El motivo principal, a nuestro juicio, de que Linux/SteamOS no consiga coger carrerilla que **no acabamos de atraer usuarios de perfil "gamer**", principalmente porque no hay nada que haga realmente atractivo el pasarse a Linux para jugar. El que pasa a linux es por que ha perdido la confianza o se ha hartado de Windows, muy pocos son los que llegan a Linux directamente para jugar. Así que es dificil captar jugadores para Linux. El segundo problema es derivado de este: no hay usuarios y por tanto las desarrolladoras no ven atractivo invertir en la plataforma. **Es necesario un "big punch" que ponga a Linux en la vanguardia**.


 


Este Big Punch se pensó en un primer momento que serían las Steam Machines y SteamOS, pero el problema es que cuando se lanzaron se acompañaron de una promoción muy potente, pero que no estaba acompañada de un Hardware y software que cumpliera con las espectativas y fuese competitivo. El problema es que estas se lanzaron para competir con las consolas pero con precios de PC,  y además no se acompañaba de un sistema operativo lo suficientemente maduro como para mirar de tu a la competencia. No se puede intentar colar a un usuario gamer un equipo que cuesta más de 600€ (cuando las consolas son sensiblemente más baratas) y que viene acompañado de un sistema pobre en drivers gráficos y con un catálogo de AAA tan limitado. Luego prometieron el oro y el moro con juegos que nunca llegaron (Rome II, The Witcher III, Street Fighter V....). Si aun encima le añadimos la casi nula promoción en los últimos tiempos, el resultado es el que se podría esperar. **Lo que está claro es que nadie va a comprar un hardware si no va acompañado de juegos que sean "capaces" de vender ese hardware**. Esto es algo básico que (casi)todas las compañías que se dedican a esto ya saben.


 


![steamos sale](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/steamos-sale.webp)


*La publicidad de SteamOs incluía hace años algunos juegos aun no han llegado a nuestro sistema*


 


Desde nuestro punto de vista, SteamOS debió ser más desarrollado antes de ser instalado en las Steam Machines y vendido a bombo y platillo. Probablemente un periodo más largo de Beta Abierta le hubiese sentado mejor. Quizás ahora sería el mejor momento para haberlas lanzado, ya que el soporte de controladores está mucho más avanzado, y podría resultar mucho más barato elaborar configuraciones competitivas gracias al empuje que ha recibido AMD gracias a sus Ryzen y el impresionante avance de su rendimiento gráfico gracias al desarrollo de los drivers libres. Pese a esto, **aún no es tarde** para "reconducir" la situación de SteamOS y las Steam Machines. **Tan solo hace falta que Valve le de una vuelta de tuerca al concepto aprovechando la ventaja adquirida con el hardware y los drivers actuales.**


 


Algo que también pensamos que es vital para el desarrollo de Linux como plataforma, y que es un rayo de esperanza,  es el desarrollo y **uso de Vulkan** por parte de las desarrolladoras. De todos es sabido que esta API gráfica tiene como principal ventaja, a parte de aprovechar mejor el multiproceso, el ser multiplataforma, con lo que esto conlleva. Para una compañía que la use sería bastante más fácil portar de un sistema a otro, por lo que la inversión y el tiempo necesarios para dotar a un juego de soporte para Linux sería menor. Esto implicaría que la decisión de vender en nuestro mercado, aunque **en principio** otorgase menos beneficios al ser dedicado a un número menor de personas, sería mucho menos traumática.


 


![Vulkan 500px Dec16](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Vulkan_vs_DX12/Vulkan_500px_Dec16.webp)


*Vulkan es una de las mejores bazas que tenemos actualmente para potenciar nuestro sistema*


 


Como veis, ni todo son luces, ni todo son sombras. Desde aquí os queremos alentar para que no desfallezcais y sigais apostando por GNU-Linux/SteamOS como plataforma de juegos, apoyando como siempre a las desarrolladores que apuestan e invierten en nosotros como consumidores. Ahora es vuestro turno.


 


¿Que opinión os merece este artículo? ¿Estais de acuerdo con lo que exponemos en él? ¿Pensais que nuestro sistema tiene futuro como plataforma de juegos? Déjanos tu opinión en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

