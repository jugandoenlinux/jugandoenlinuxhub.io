---
author: leillo1975
category: "An\xE1lisis"
date: 2017-11-14 09:45:12
excerpt: "<p>Repasamos una a una las novedades de esta expansi\xF3n de SCS Software.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7b17ec9e1ac0efe68976696eb1e50b37.webp
joomla_id: 534
joomla_url: analisis-american-truck-simulator-dlc-new-mexico
layout: post
tags:
- dlc
- american-truck-simulator
- ats
- expansion
- new-mexico
title: "An\xE1lisis: American Truck Simulator - DLC New Mexico."
---
Repasamos una a una las novedades de esta expansión de SCS Software.

Hace ya casi dos años la compañía checa de software lanzaba al mercado otro simulador de camiones. En este caso el juego se ambientaba en EEUU, más concretamente en la costa Oeste, ofreciendo al jugador de salida la posibilidad de conducir por las carreteras de California. El juego derrochaba ambientación y conseguía sumergirte en lo que se supone que tiene que ser conducir un camión americano por estas latitudes. Pero a pesar de esto, el juego acusó bastantes críticas por la brevedad de la experiencia, pues el territorio abarcado no era muy grande y rapidamente se podía completar. Conscientes de ello lanzaron las **expansiones gratuitas** de Nevada, en primer lugar, y después de Arizona. Cuando estaba a punto de cumplirse un año, [el juego se actualizó a la versión 1.5](index.php/homepage/generos/simulacion/item/248-american-truck-simulator-crece-con-la-actualizacion-1-5), donde el mapa creció considerablemente cambiando a una escala más amplia, entre otras mejoras. Con todo esto consiguieron paliar el hambre de Kilómetros de sus seguidores entre los que orgullosamente me incluyo.


 


A principios de este año, [SCS Software anunciaba que estaba trabajando en llevar la experiencia al estado de Nuevo México](index.php/homepage/generos/simulacion/item/328-american-truck-simulator-se-expandira-hacia-nuevo-mexico), y desde entonces han ido dejando caer poco a poco [información](index.php/component/search/?searchword=american%20truck%20mexico&ordering=newest&searchphrase=all&limit=20) sobre esta DLC para ir abriéndonos el apetito de carretera a los camioneros virtuales. Pero vamos al lio y pongámonos a desgranar lo que vamos a encontrar en esta expansión sobre la "Tierra de Encanto", lema de este estado, anteriormente perteneciente a México como su nombre indica.


 


![ATSNMCruce](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisATSNewMEx/ATSNMCruce.webp)


*Algunos elementos de tráfico son propios y caracteristicos del estado de Nuevo México.*


 


En ATS "New México", según la descripción inicial vamos a encontrar sobre **4000 millas de nuevas carreteras circulables**, **14 nuevas poblaciones** tales como Albuquerque, Santa Fe o Roswell; **11 areas de descanso** para aparcar y llenar el depósito; más de **600 recursos 3D completamente nuevos**, nuevas **intersecciones y confluencias de carreteras realistas**, **8 nuevas compañías**, nuevos monumentos tanto naturales como hechos por el hombre; y nuevos logros para completar. Así de buenas a primeras parace estos paracen argumentos más que suficientes para hacerse con esta expansión, pero como es lógico aquí vamos a intentar describir la sensación general que nos produce el circular por este estado. 


 


![ATSNMMapa](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisATSNewMEx/ATSNMMapa.webp)


*En este mapa podemos ver la superficie que amplía esta DLC*


 


En primer lugar conviene decir que esta es la primera expansión (de territorio) de pago que se saca para ATS. En lineas generales, no se aprecian grandes cambios con respecto al resto del territorio, sobre todo en lo referente al paisaje, pero **las diferencias, aunque sutiles están ahí** y los seguidores del juego enseguida las encontrarán. Me parece advertir que en esta ocasión la desolación de las extensas llanuras por las que vamos a conducir están más detalladas, adviertiendo elementos en la vegetación que antes no habíamos visto. En principio puede parecer que discurrir por este tipo de escenario puede ser aburrido y desagradable, pero se "siente" una belleza escondida en todo esto. De todas formas esto no quiere decir que nos encontremos con el mismo paisaje durante todo el camino, pues **hay lugares de interés facilmente reconocibles** como el **Rio Grande**, que cruza el estado de norte a sur; el **parque nacional Carlson,** al norte de Santa Fe; **Baylor Peak**, cerca de Las Cruces; o **Pyramid Rock** nada más entrar en el estado viniendo de Arizona. También es alucinante el paso por la **Musical Road**, al este de Albuquerque ,en plena ruta 66, donde al pisar las marcas sonoras en los bordes de la carretera nos llevaremos una agradable y singular sorpresa. En el terreno de las carreteras es muy destacable el trabajo hecho en las intersecciones y nudos de carreteras, siendo notable la **confluencia de vias al este de Albuquerque**, donde la complejidad y cantidad de estas es sencillamente abrumadora.


 


![ATSNMPaisaje](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisATSNewMEx/ATSNMPaisaje.webp)*La calidad de los paisajes en esta DLC está por encima de la media de ATS*


Si nos ponemos a sumar todo y describirlo en general, en principio puede parecer que se trata de una expansión que ofrece poco contenido, pocos lugares y ciudades llamativos, pero también es cierto que **el atractivo principal de Nuevo México es su propia naturaleza**. Obviamente no podemos comparar la sensación que produce movernos por San Francisco o Las Vegas, donde encontramos lugares famosos facilmente reconocibles por todas partes, o donde existen más núcleos de población característicos. Da la impresión que **SCS Software en esta ocasión quiere incidir más en las sensaciones** que produce el conducir por las amplias llanuras, algo que ya vimos en expansiones gratuitas como Nevada o Arizona. También es cierto, que esto dota de realismo al juego, pues al tratarse de un simulador no se busca "divertir" al usuario con elementos singulares cada dos minutos. En cierto modo, los camioneros virtuales venimos mal acostumbrados de Euro Truck Simulator 2, donde encontramos mayor variedad en los mapas, pero si el juego busca reflejar la realidad en este sentido lo consigue.


 


![ATSNMRoswell](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisATSNewMEx/ATSNMRoswell.webp)


*En esta expansión aparecen también lugares curiosos como [Roswell](https://es.wikipedia.org/wiki/Roswell_(Nuevo_M%C3%A9xico))*


 


Una cosa es cierta, y es que al tratarse de una expansión que abarca un solo estado, y aun encima no muy grande y variado, puede parecer que la desarrolladora haya sacado esta DLC para pasar por caja, algo que es totalmente legítimo, pero tenemos que tener en cuenta que ya se nos "regalaron" en su día Nevada y Arizona, por las razones que esgrimimos al principio. Siendo justos también hay que decir que **el precio es sensiblemente menor** (sobre 12€) al de expansiones como Scandinavia o Vive la France! en ETS2, donde debemos abonar sobre 18€ para adquirirlas.


 


Aunque no tenga que ver realmente con esta expansión, **sigue echándose de menos la inclusión de más marcas de camiones en el juego**. Por lo que sabemos están teniendo problemas para llegar a acuerdos con estas, por lo que esperemos que en un futuro podamos ver otros modelos de compañias como Mack, Western Star o  Freightliner. También habría que incidir en las **viejas diferencias de caracteristicas que tenemos que sufrir los jugadores de Linux con respecto a los de Windows**, tales como el **soporte de Force Feedback para volantes**, un funcionamiento adecuado del Steam Controller de forma nativa o la **inclusión de la Radio Online**.


 


![ATSNMInterseccion](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisATSNewMEx/ATSNMInterseccion.webp)


*El trabajo realizado en la intersección cerca de Albuquerque impresiona por su complejidad*


 


En resumen, New Mexico es una expansión que **va a gustar a todos los seguidores acérrimos** que tienen este este juego y que desean ampliar el territorio a recorrer y trabajar. Es una experiencia que permite al jugador jugar con sus sensaciones y lanzarse a una mirada introspectiva abrumado por la grandiosa naturaleza que le rodea. Esperamos también ansiosos ver pronto el trabajo que han hecho con la esperada nueva expansión para su hermano mayor europeo, y poder rodar lo antes posible por la [Bella Italia](index.php/homepage/generos/simulacion/item/552-bella-italia-sera-la-nueva-dlc-para-euro-truck-simulator-2).


En JugandoEnLinux.com hemos grabado dos videos para nuestro canal de Youtube donde podeis ver como nos adentramos en Nuevo México:


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/E9zsYlbdjqQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/JoXH9em1ZhM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


También es muy interesante que veais este video hecho por un fan donde podeis ver condensadas todas las caraceterísticas de esta expansión:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/lkBo0TnDRdA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Podeis comprar American Truck Simulator: New Mexico DLC en Steam:


 <div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/684630/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Habeis conducido ya por las desoladas carreteras de Nuevo México? ¿Que opinais de los simuladores de camiones? Déjanos tu opinión en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).


 

