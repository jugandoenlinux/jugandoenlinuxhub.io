---
author: Pato
category: Noticia
date: 2020-04-09 18:49:55
excerpt: "<p>Acci\xF3n, plataformas y aventuras vendr\xE1n de la mano de estos tres\
  \ nuevos juegos</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/Stadia-monsterboy.webp
joomla_id: 1205
joomla_url: monster-boy-republique-y-west-of-loathing-llegaran-proximamente-a-stadia
layout: post
title: "Monster Boy, R\xE9publique y West of Loathing llegar\xE1n pr\xF3ximamente\
  \ a Stadia"
---
Acción, plataformas y aventuras vendrán de la mano de estos tres nuevos juegos


Stadia sigue sumando títulos, y hoy nos anuncia la próxima llegada de **Monster Boy and the Cursed Kingdom**, **République** y **West of Loathing**.


Monster Boy and the Cursed Kingdom es un colorido juego de acción y aventuras que viene de la mano del creador de la serie original Wonder Boy.Nos ofrece una aventura basada en los juegos clásicos con animaciones dibujadas a mano.


 *¡Ayuda a nuestro joven héroe a derrotar a enemigos desafiantes, descubre lugares ocultos, mejora equipos poderosos y más! También desbloquearás formas especiales con habilidades únicas para abrir nuevos caminos donde te esperan poderosos jefes y tesoros secretos.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/mbYE6QfTLv8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Características:


* Seis formas increíbles con habilidades únicas de combate y plataformas para que tu aventura sea emocionante de principio a fin.
* Desbloquea nuevos caminos y secretos con equipos especiales: busca armas mágicas y objetos que abran gradualmente el mundo.
* Más de 15 horas de aventura épica: explora el nuevo mundo Monster en un vasto entorno interconectado.
* Animaciones dibujadas a mano: personajes y enemigos cobran vida con animaciones detalladas y divertidas expresiones faciales.


Por otra parte, también nos anuncian République, un juego desarrollado durante cinco años por veteranos de la industria, République es un juego de acción sigiloso y emocionante que explora los peligros de una república corporativa.


![Republique](https://community.stadia.com/t5/image/serverpage/image-id/1952i83A3302B9211D295/image-size/large)


*Recibes una llamada de Hope, una mujer atrapada dentro de un peligroso mundo futurista. Al piratear la elaborada red de vigilancia de la nación y tomar el control, guías a Hope a través de una red de engaño a través de cinco episodios emocionantes.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/RzAf9lw5flg" width="560"></iframe></div>


Por último, tendremos el juego de aventuras y comedia West of Loathing, un juego para un jugador ambientado en el salvaje oeste del Reino de Loathing. *Atraviesa las praderas infestadas de serpientes, golpea esqueletos con sombreros de vaquero, lucha con vacas demoníacas e investiga una amplia variedad de escupideras repugnantes. Explora un vasto mundo abierto y encuentre un colorido elenco de personajes, algunos de los cuales son buenos, muchos de los cuales son malos, y algunos de los cuales son feos.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/1lU0aFZr1R8" width="560"></iframe></div>


Por último, reseñar dos juegos más para jugar en compañía en estos tiempos que corren que se nos pasó reseñar la semana pasada y que llegarán también próximamente a Stadia, como son:


**Gunsport**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/HzYC5p_2Iyc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Y el shoot'em Up **Just Shapes & Beats:**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/M_56igDqM-Q" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


¿Qué te parecen los nuevos lanzamientos que llegarán próximamente a Stadia?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

