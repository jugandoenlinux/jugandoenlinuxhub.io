---
author: Serjor
category: Ofertas
date: 2018-10-04 19:14:36
excerpt: <p>Jugosas ofertas hasta el 8 de octubre</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/31975ef4b0d2e52f5e8351d894cbca08.webp
joomla_id: 869
joomla_url: fin-de-semana-de-paradox-en-humble-bundle
layout: post
tags:
- humble-bundle
- oferta
- paradox
- humble-store
- paradox-interactive
title: Fin de semana de Paradox en Humble Bundle
---
Jugosas ofertas hasta el 8 de octubre

Y es que cosas como esta le alegran a uno el fin de semana, aunque eso implique vaciar la cartera. En Humble Bundle Paradox ha puesto hasta el 8 de octubre un montón de sus títulos de rebajas, entre otros:


* Stellaris
* Pillars of Eternity
* Cities: Skylines
* Surviving Mars
* Age of Wonders III (el I y II también, pero creo que no están para GNU/Linux)


Junto con muchos otros, y además, muchos de sus DLCs también están rebajados. Ya solo Stellaris o Cities: Skylines son muy buena opción, pero en general el catálogo de Paradox es garantía de calidad asegurada.


Si os interesa alguno de sus juegos, podéis usar nuestro [link de referidos](https://www.humblebundle.com/store/promo/paradox-interactive-weekend?partner=jugandoenlinux) y ayudarnos así a sufragar los gastos del hosting.


Y tú, ¿estás interesado en alguno de sus títulos? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

