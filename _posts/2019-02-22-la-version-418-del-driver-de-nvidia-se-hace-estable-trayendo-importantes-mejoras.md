---
author: leillo1975
category: Software
date: 2019-02-22 19:45:50
excerpt: "<p>Los drivers 390 Legacy y el 410 de larga duraci\xF3n tambi\xE9n se actualizan.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e13c4c6b684536ecde3ac77213ca4d80.webp
joomla_id: 979
joomla_url: la-version-418-del-driver-de-nvidia-se-hace-estable-trayendo-importantes-mejoras
layout: post
tags:
- drivers
- nvidia
- long-live
- legacy
- estable
title: "La versi\xF3n 418 del driver de Nvidia se hace estable trayendo importantes\
  \ mejoras."
---
Los drivers 390 Legacy y el 410 de larga duración también se actualizan.

En el día de hoy, **Nvidia ha actualizado sus drivers para nuestro sistema**. Gracias a **Phoronix** nos enteramos [primero](https://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-418.43-Linux-Release) que la rama 418 dejaba de ser beta, y poco [después](https://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-390.116-410.104-Linux) que los drivers "Legacy" (390)  y "Long Live" (410) recibían sendas actualizaciones. Como sabeis, en el caso de este fabricante de chips gráficos, la instalación del Blob privativo es la mejor garantía de obtener el mejor resultado con su hardware, ya que la versión libre aun dista mucho de ser competitiva.


En el caso de **sus drivers más avanzados**, se ha alcanzado la [versión 418.43](https://www.nvidia.com/download/driverResults.aspx/142958/en-us), haciendo así estables las características que anteriormente estaban en fase de pruebas, entre las que destaca por encima de todo la **compatibilidad con G-SYNC** (también conocida como FreeSync / Adaptive-Sync) en determinados monitores con ese soporte y tarjetas gráficas Pascal, es decir, de la serie 10 en adelante. También se ha habilitado la compatiblidad con la novisima GeForce GTX-1660Ti, así como con la RTX 2070 y la RTX 2080 con diseño Max-Q, versión de bajo consumo para portátiles.


Obviamente todas las características anteriormente vistas en la versión beta las tendremos ahora de forma estable, pudiendo este driver ser instalado en el proximo **Linux 5.0**, la compatibilidad con el SDK 9.0 de NVIDIA Video Codec, o con presentaciones estéreo con Vulkan, varias correcciones en Vulkan y OpenGL,  compatibilidad inicial con NVIDIA Optical Flow, actualizaciones de VDPAU y muchas más correcciones menores.


En cuanto al **driver "Legacy"** llega a la versión [390.116](https://devtalk.nvidia.com/default/topic/1047708/b/t/post/5316745/#5316745), continuando el soporte para las tarjetas gráficas de la arquitectura Fermi, es decir, las gráficas de la ya veterana serie 400 y 500 de nvidia. En este caso se incluyen multitud de correcciones menores recogidas en los últimos meses, así como la compatibilidad con Linux 5.0 y X.org Server 1.20.


Del lado de los que prefieren un driver más estable y fiable en sus equipos nos encontramos con el **controlador con soporte de larga duración**, que llega a la versión [410.104](https://devtalk.nvidia.com/default/topic/1047709/unix-graphics-announcements-and-news/linux-solaris-and-freebsd-driver-410-104-long-lived-branch-release-/), añandiendo soporte para el modelo Tesla V100-SXM3-32GB-H, ofrece algunas correcciones de Vulkan, añade soporte para el núcleo de Linux 5.0 y otros arreglos.


Podeis encontrar estos novísimos drivers en los enlaces proporcinados en este artículo, pero siempre podeis esperar unos días para que los repositorios de vuestras distribuciones se actualicen ([PPA de Ubuntu](index.php/foro/drivers-graficas-nvidia/19-instalar-el-ultimo-driver-propietario-de-nvidia-en-ubuntu-y-derivadas)) para realizarlo de una forma más sencilla y segura.

