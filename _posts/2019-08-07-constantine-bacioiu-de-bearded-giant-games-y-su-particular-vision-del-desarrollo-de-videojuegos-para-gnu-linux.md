---
author: Serjor
category: Noticia
date: 2019-08-07 16:06:54
excerpt: <p>Un punto de vista contrario al mantra habitual de las empresas que desarrollan
  para GNU/Linux</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6db80d036c9beb1cc34d164970a61d94.webp
joomla_id: 1095
joomla_url: constantine-bacioiu-de-bearded-giant-games-y-su-particular-vision-del-desarrollo-de-videojuegos-para-gnu-linux
layout: post
tags:
- desarrollo
- bearded-giant-games
- jason-evangelho
title: "Constantine Bacioiu, de Bearded Giant Games, y su particular visi\xF3n del\
  \ desarrollo de videojuegos para GNU/Linux"
---
Un punto de vista contrario al mantra habitual de las empresas que desarrollan para GNU/Linux

Esta mañana en nuestro canal de [Telegram](https://t.me/jugandoenlinux) nos hacían llegar un interesante [artículo](https://www.forbes.com/sites/jasonevangelho/2019/08/07/porting-games-to-linux-is-a-waste-of-time-this-game-developer-says-youre-doing-it-wrong/) de [Jason Evangelho](@killyourfm@layer8.space) en la revista Forbes.


En dicho artículo Jason nos hace un resumen de la entrevista que tuvo con Constantine Bacioiu, único integrante de la desarrolladora [Bearded Giant Games](https://beardedgiant.games/), la cuál, como ya os [comentamos](index.php/homepage/generos/accion/5-accion/1216-space-mercs-ya-esta-disponible-para-linux-steamos), ha lanzado recientemente el juego Space Mercs con soporte para GNU/Linux.


Durante la entrevista, que podéis escuchar al completo en el [tercer episodio](https://linuxforeveryone.fireside.fm/3-manjaro-office-indie-games-community-voice) del podcast linux4everyone, Constantine da un punto de vista muy interesante del desarrollo de videojuegos para GNU/Linux. Comenta algo que muchos intuíamos, que la mayoría de las desarrolladoras usa el botón de exportar de Unity3D y sin mucha inversión de tiempo en testear la versión del pingüino, espera que funcione sin problemas, cuando esto a todas luces no es así, por lo que aunque reconoce que el problema del sobre coste del soporte de los ports es cierto y real, reprocha a estas desarrolladoras de no haber invertido tiempo y planificado debidamente desde el principio del desarrollo la versión para GNU/Linux, y que una vez que el producto está en la calle, cuando este da problemas, el coste de encontrarlos y solucionarlos consumen los beneficios.


Y no solamente eso, sino que gracias a su iniciativa particular de desarrollar los videojuegos [primero para GNU/Linux](https://beardedgiant.games/linux-first-initiative/) y después para otras plataformas le asegura que no usa herramientas que no tengan un buen soporte en GNU/Linux, ya que de esta manera tiene garantizado no usar middleware que únicamente funcione en Windows.


Constantine también indica cuál es otro de los problemas que las desarrolladoras se encuentran a la hora de vender su producto en GNU/Linux: La falta de contacto con la comunidad linuxera. Según sus palabras, él dedica mucho tiempo a comunicarse con la comunidad, y dice que de cara a depurar el producto, es de las mejores que existen, ya que lo normal es que un linuxero tenga un claro perfil técnico, con lo que interactuar con la comunidad linuxera no es solamente bueno para dar visibilidad a su producto, sino que es una oportunidad de validar su producto.


Todo esto ha conseguido que prácticamente puede subsistir con las ventas en GNU/Linux, y que en el caso de Space Mercs, en un mercado donde no representamos ni el 1% del total, el 35% de las ventas vengan de la versión para GNU/Linux . Simplemente sorprendente.


La verdad es que si el inglés no es un problema, es recomendable leerse el artículo original, y yo particularmente ya tengo el programa descargado para escuchar con detenimiento toda la entrevista.


Y tú, ¿piensas que el éxito de Constantine Bacioiu en GNU/Linux es fruto de su buen hacer o simplemente una suma de circunstancias? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)

