---
author: Pato
category: Plataformas
date: 2018-06-19 10:51:13
excerpt: "<p>El juego de @motiontwin es una interesante mezcla de g\xE9neros entre\
  \ Rogue-like y Metroidvania</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/04af3f34de2f909b68ae5d07ed6ca171.webp
joomla_id: 776
joomla_url: dead-cells-por-fin-llega-a-linux-steamos-en-fase-beta
layout: post
tags:
- accion
- plataformas
- beta
- roguevania
title: '''Dead Cells'' por fin llega a Linux/SteamOS en fase beta'
---
El juego de @motiontwin es una interesante mezcla de géneros entre Rogue-like y Metroidvania

Hace tiempo que venimos anunciando la llegada de Dead Cells a nuestro sistema favorito. Tras unos días donde el juego ha estado dando signos de movimiento en SteamDB ayer por fin tuvimos noticias del estudio anunciando que ya está disponible el juego para su descarga, eso sí, en la rama beta.



> 
> Mac and Linux users, we heard you! Dead Cells is now available in Beta on these platforms.  
> Oh, and the good ol' [#pixelart](https://twitter.com/hashtag/pixelart?src=hash&ref_src=twsrc%5Etfw) font is back.<https://t.co/BTC8f0XbKn>[#Steam](https://twitter.com/hashtag/Steam?src=hash&ref_src=twsrc%5Etfw) [#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/xUpoWRLN0g](https://t.co/xUpoWRLN0g)
> 
> 
> — Motion Twin (@motiontwin) [June 18, 2018](https://twitter.com/motiontwin/status/1008727952278900736?ref_src=twsrc%5Etfw)



 Tal y como ellos lo describen, Dead Cells es un juego tipo "roguevania" con plataformas y acción a raudales, inspirado en Castlevania y con esencias roguelike.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/KV6fBYuuPMg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Entre sus características principales se encuentran :


* Muerte permanente.
* Juego estilo "Metroidvania".
* Acción, aventura y plataformas.
* Mecánicas de combate avanzadas con multitud de armas.
* Elementos generados proceduralmente para que cada partida sea distinta. Nuevo castillo, nuevos enemigos, nuevos elementos, nuevos NPCs y un fuerte elemento de exploración hacia lo desconocido.


 Actualmente aún no tenemos los requisitos mínimos del juego pues tampoco aparece el logo de Linux en la tienda. Si queréis probar el juego tendréis que acceder a la rama beta del juego, dentro de las opciones del juego. Para ello hacer clic con el botón derecho del ratón sobre el título del juego en vuestra biblioteca de Steam y entrar en "propiedades", y ahí en la pestaña "betas" tendréis la opción de descargar la versión para Linux.


Tienes más información en el post del anuncio en Steam [en este enlace](https://steamcommunity.com/games/588650/announcements/detail/3082033094942752551).


Tienes Dead Cells disponible [en su página de Steam](https://store.steampowered.com/app/588650/Dead_Cells/) traducido al español.

