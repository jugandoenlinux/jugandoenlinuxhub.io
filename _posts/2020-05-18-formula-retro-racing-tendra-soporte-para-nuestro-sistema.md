---
author: leillo1975
category: Carreras
date: 2020-05-18 21:10:36
excerpt: "<p>Sus creadores, <a class=\"account-link link-complex\" href=\"https://twitter.com/Repixel8\"\
  \ target=\"_blank\" rel=\"noopener user\" data-user-name=\"Repixel8\"><span class=\"\
  username txt-mute\">@Repixel8</span></a> , nos lo han confirmado.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/FormulaRetroRacing/FormualRetroRacing.webp
joomla_id: 1221
joomla_url: formula-retro-racing-tendra-soporte-para-nuestro-sistema
layout: post
tags:
- indie
- retro
- formularetro-racing
- repixel8
- '90'
title: "Formula Retro Racing tendr\xE1 soporte para nuestro sistema"
---
Sus creadores, [@Repixel8](https://twitter.com/Repixel8) , nos lo han confirmado.


 Aun recuerdo con cariño aquel tiempo, mediados-finales de los 90, en que disfrutaba como un enano, aunque ya no lo era tanto, de mi querida [SEGA Saturn](https://es.wikipedia.org/wiki/Sega_Saturn), y por no variar sus juegos de coches eran por supuesto de mis favoritos. Quien no recuerda [SEGA Rally,](https://en.wikipedia.org/wiki/Sega_Rally_Championship) [Daytona USA,](https://es.wikipedia.org/wiki/Daytona_USA) [SEGA Touring Car](https://en.wikipedia.org/wiki/Sega_Touring_Car_Championship) ... y por supuesto [Virtua Racing](https://en.wikipedia.org/wiki/Virtua_Racing). Este último es exactamente lo que se me vino a la cabeza cuando hace algún tiempo me fijé en [Formula Retro Racing](https://repixel8.com/formula-retro-racing/) y decidí vigilarlo de cerca, y es que **sus influencias son claras y para nada las esconden**. El caso es que esta mañana decidimos preguntar a sus desarrolladores en twitter por si podríamos disfrutar nativamente de su juego en nuestro sistema, y sonó la campana:



> 
> We are planning a Linux release through Steam very soon. Just a couple of issues to fix first.
> 
> 
> — Repixel8 (@Repixel8) [May 18, 2020](https://twitter.com/Repixel8/status/1262464698894729218?ref_src=twsrc%5Etfw)


  





Formula Retro Racing está desarrollado por [Repixel8](https://repixel8.com/), una **compañía indie británica** que ha desarrollado otros títulos para otras plataformas en el pasado como [Gravity Chase](https://repixel8.com/gravity-chase/), [Velocity G](https://repixel8.com/velocity-g/), [Turbo Sprint](http://repixel8.com/turbo-sprint/), o **incluso juegos para sistemas retro** (Atari y Spectrum). Esta esla descripción oficial del juego:


*Formula Retro Racing es un juego de carreras arcade de estilo retro que está influenciado por los clásicos juegos de carreras arcade de principios de los 90.  
  
Con la jugabilidad rápida y emocionante por la que el género es conocido, Formula Retro Racing captura la esencia de los juegos de carreras de la vieja escuela con imágenes nítidas de "low poli", banda sonora retro y acción de alta velocidad.  
  
Corre a través de una amplia gama de pistas en varios modos de juego para superar tus mejores tiempos personales y competir en las tablas de clasificación de Formula Retro Racing.  
**Caracteristicas**  
-Acción de carreras de arcade retro Visualizaciones polivinílicas bajas en HD y 4K a 60 fps  
-8 pistas únicas cada una con dificultad variable  
-Modo clásico de juego de carreras de puntos de control de arcade  
-Nuevo modo de juego "Eliminator"  
-Autos destruibles con física locas de choques  
-Las mejores tiempos de clasificación en línea de vuelta y tiempo de carrera para cada pista*


Toda la información que tenemos sobre el futuro port de este juego es la que resume ese tweet, aunque por supuesto estaremos al tanto de cualquier novedad para ofrecerosla lo antes posible. Os dejamos como siempre con un trailer del juego:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/1VnZMi3XSPs" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>



¿Os gustan los juegos de conducción retro? ¿Habeis jugado a los clásicos de los 90? ¿Qué os parece este Formula Retro Racing? Podeis darnos vuestra opinión sobre este juego en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

