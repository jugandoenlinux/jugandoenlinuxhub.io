---
author: leillo1975
category: Ofertas
date: 2019-11-21 20:04:01
excerpt: "<p>@humble regala adem\xE1s una copia de \"Serial Cleaner\"</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HumbleBundle/HumbleFallSale19.webp
joomla_id: 1133
joomla_url: las-rebajas-de-otono-llegan-a-la-humble-store
layout: post
tags:
- humble-bundle
- oferta
- rebajas
- humble-store
- fall-sale
title: "Las Rebajas de Oto\xF1o llegan a la Humble Store."
---
@humble regala además una copia de "Serial Cleaner"


 Como siempre, a todos los que nos gustan los videojuegos, la temporada de rebajas es un sin vivir.... Halloween, rebajas de otoño, black friday, navidad .... El corazón lucha contra la razón, pero al final siempre gana el primero y acabamos comprando algo. También es cierto que es una buena oportunidad de comprar regalos (y autoregalos) y ahorrarnos unos buenos cuartos.


Una vez más, nuestra tienda favorita, la Humble Store, nos llega con las [Rebajas de Otoño](https://www.humblebundle.com/store?partner=jugandoenlinux) (Fall Sale) con un montonazo de juegos a precios superinteresantes, entre los que nosotros hemos escogido unos cuantos:



- [Shadow of the Tomb Raider: Definitive Edition](https://www.humblebundle.com/store/shadow-of-the-tomb-raider-definitive-edition?partner=jugandoenlinux): 23.99€ (-60%)


- [EVERSPACE™](https://www.humblebundle.com/store/everspace?partner=jugandoenlinux): 5.59€ (-80%)


- [Borderlands 2: Game of the Year](https://www.humblebundle.com/store/borderlands-2-game-of-the-year?partner=jugandoenlinux): 9.89€ (-78%)


- [Yooka-Laylee](https://www.humblebundle.com/store/yooka-laylee?partner=jugandoenlinux): 9.99€ (-75%)


- [Wasteland 2: Director's Cut - Digital Deluxe Edition](https://www.humblebundle.com/store/wasteland-2-directors-cut-digital-deluxe-edition?partner=jugandoenlinux): 9.49€ (-75%)


- [Rise of the Tomb Raider: 20 Year Celebration](https://www.humblebundle.com/store/rise-of-the-tomb-raider-20-year-celebration?partner=jugandoenlinux): 9.99€ (-80%)


- [Teslagrad](https://www.humblebundle.com/store/teslagrad?partner=jugandoenlinux): 0.99€ (-90%)


- [Outlast](https://www.humblebundle.com/store/outlast?partner=jugandoenlinux): 2.51€ (-85%)


- [Deus Ex: Mankind Divided™ - Digital Deluxe Edition](https://www.humblebundle.com/store/deus-ex-mankind-divided-digital-deluxe?partner=jugandoenlinux): 6.74€ (-85%)


- [Tomb Raider GOTY Edition](https://www.humblebundle.com/store/tomb-raider-goty-edition?partner=jugandoenlinux): 7.49€ (-75%)


- [BioShock Infinite](https://www.humblebundle.com/store/bioshock-infinite?partner=jugandoenlinux): 7.49€ (-75%)


- [Grim Fandango Remastered](https://www.humblebundle.com/store/grim-fandango-remastered?partner=jugandoenlinux): 5.24€ (-65%)



Por supuesto que hay muchos más que a nosotros se nos han escapado, pero puedes avisarnos en los comentarios de esta noticia si ves algo que valga la pena. También no debes olvidarte de "pillar" [Serial Cleaner completamente gratis](https://www.humblebundle.com/store/serial-cleaner-free-game?partner=jugandoenlinux). En este juego encarnarás un limpiador que trabaja para la mafia y tendrás que dejar como los chorros del oro sus "desaguisados".


Recordad también que con todas las compras que realiceis en esta tienda estais colaborando con diversas ONG's. Además sabed que con cada juego que compreis con los enlaces que os pasamos, JugandoEnLinux.com recibirá una pequeña parte que nos servirá para pagar los gastos de Hosting y Dominios sin que a vosotros os cueste ni un céntimo más.


¿Sucumbirás una vez más al influjo de las ofertas? ¿Qué oferta te ha gustado más? Cuéntanoslo en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

