---
author: leillo1975
category: Software
date: 2020-10-16 08:36:35
excerpt: "<div>Nueva versi\xF3n de esta popular herramienta .</div>\r\n<div></div>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Proton/valve-Proton.webp
joomla_id: 1260
joomla_url: steam-play-se-actualiza-con-proton-5-13-6
layout: post
tags:
- wine
- valve
- steam-play
- dxvk
- proton
- faudio
- vkd3d
- pierre-loup-griffais
title: 'Steam Play se actualiza con Proton 5.13-6. '
---
Nueva versión de esta popular herramienta .

********ACTUALIZADO 13-2-21:********


Desde la ultima revisión han pasado un mes de árduo trabajo en Steam Play/Proton, y si en aquella ocasión se buscaba mejorar la compatibilidad del polémico Cyberpunk 2077, ahora se continua en esa senda con nuevas correcciones para mejorar la experiencia con ese juego, aunque como vereis hay mucho más trabajo en otros títulos detrás. En esta [versión 5.13-6](https://github.com/ValveSoftware/Proton/wiki/Changelog#513-6) encontraremos las siguientes novedades:


-Anteriormente en Experimental: Arreglados los problemas de **sonido** del mundo de **Cyberpunk 2077**  
 -Anteriormente en Experimental: Se ha mejorado la compatibilidad con el **mando** y la conexión en caliente en **Yakuza Like a Dragon**, **Subnautica**, **DOOM** (2016) y **Virginia**  
 **-Nioh 2** ya es jugable  
 -Se ha corregido la pantalla negra al perder el enfoque en **DOOM Eternal** en **AMD**  
 -Se ha restaurado la compatibilidad con la **RV** en **No Man's Sky**  
 -El **chat de voz** en **Deep Rock Galactic** ya es funcional  
 -Se ha mejorado la compatibilidad con los **mandos** de **PlayStation 5**  
 -El **sonido** en **Dark Sector** ya funciona  
 -Se ha corregido el bloqueo de **Need for Speed (2015)** en **AMD**.  
 -Más correcciones para las **entradas de juego** que se activa mientras el "**overlay**" de Steam está en primer plano


Como podeis ver se han incluido las mejoras en las que se había estado trabajando en la [rama experimental](https://github.com/ValveSoftware/Proton/wiki/Changelog#available-in-proton-experimental), que como sabeis, podeis activar en la pestaña de Beta de Proton. Seguimos a la espera de nuevas revisiones y pendientes de que se integre la version 6.x de Wine .


 




---


 ****ACTUALIZADO 15-1-21:****Hace más un menos un mes os traíamos la última revisión de Steam Play/Proton que permitía disfrutar de **Cyberpunk 2077** en tarjetas AMD ([en NVIDIA funciona](https://youtu.be/9YAMm_Y0mGE) pero va a pedales). Hoy venimos con una nueva revisión que hace unas horas anunció como es habitual Pierre-Loup Griffais ([@Plagman2](https://twitter.com/Plagman2)):



> 
> Proton 5.13-5 just released, promoting some good stuff from Experimental and adding fresh new fixes! [pic.twitter.com/iguerldHNK](https://t.co/iguerldHNK)
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [January 15, 2021](https://twitter.com/Plagman2/status/1349965612395577345?ref_src=twsrc%5Etfw)


  





En la [lista de cambios](https://github.com/ValveSoftware/Proton/releases/tag/proton-5.13-5) podemos encontrar las siguientes novedades:


  
-Admite la nueva **API de realidad virtual OpenXR**. El modo VR de **Microsoft Flight Simulator** ahora se puede usar en hardware AMD  
 -Actualizado **vkd3d-proton a v2.1**.  
 -Se corrigieron los **sonidos** del mundo en **Cyberpunk 2077**.  
 -El j**uego en línea** ahora es funcional en **Red Dead Online** y **Read Dead Redemption 2**.  
 -Soluciona **cuelgues o cierres** en **Gears Tactics, Fallout 76, Kingdoms Reborn, Need For Speed Hot Pursuit y Conan Exiles**.  
 -La función de **captura de pantalla** ahora se puede usar en **Fallout 76 y Path of Exile**.  
 -La mayoría de los juegos dejarán de aceptar entradas mientras la superposición del juego Steam esté activa.  
 -Se corrigió el texto que faltaba en Lumberjack's Destiny.  
 -Soluciona el problema de resolución de pantalla en DLC Quest y otros juegos XNA.


Mientras tanto se sigue trabajando en la siguiente versión en la [Rama Experimental](https://github.com/ValveSoftware/Proton/wiki/Changelog#available-in-proton-experimental) que podeis probar si la seleccionais en la sección de Betas de Proton en Steam.


 




---


**ACTUALIZADO 9-12-20:** 


Cuando aun no hace ni dos días que os contábamos la llegada de la revisión anterior de Proton, llega un nuevo lanzamiento, el [5.13-4](https://github.com/ValveSoftware/Proton/releases/tag/proton-5.13-4), que permite la ejecución del esperadísimo y a punto de salir (en este momento) Cyberpunk 2077. Según lo que podemos leer en github es necesario disponer de una gráfica AMD y Mesa git, no quedando muy claro si funcionará con gráficas Nvidia.


Aunque no es habitual al ser un juego de Windows, os dejamos con el trailer de lanzamiento de Cyberpunk 2077:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/VhM3NRu23Sk" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>




---


**ACTUALIZADO 8-12-20:**


Estamos de vuelta con una nueva versión de este tan manido pero imprescindible software para todos los que disfrutamos de los videojuegos en Linux. Se acaba de publicar hace tan solo unas horitas una nueva revisión de este conjunto de herramientas que llega a la [versión 5.13-3](https://github.com/ValveSoftware/Proton/releases/tag/proton-5.13-3), y que tiene las siguientes mejoras:


-Restablecida la **conexión en caliente del controlador**.  
 -**Yakuza: Like A Dragon**, **Soulcalibur 6**, **Lords of the Fallen**, y **Hammerting** son ahora jugables.  
 -Arreglado el **sonido** en **Warframe** y **Ghostrunner**.  
 -Arreglados los **gráficos parpadeantes** en **Serious Sam 4**.  
 -Arreglados los f**allos de red** en **Call of Duty: World War II**.  
 -Arreglada el **cierre** de **Age of Empires II HD** en las salas multijugador.  
 -Arreglados los **lanzadores** de juegos **Paradox**.  
 -Actualizdo **FAudio** a [20.12](https://github.com/FNA-XNA/FAudio/releases/tag/20.12).  
-Actualizado **DXVK** a la versión [1.7.3](https://github.com/doitsujin/dxvk/releases/tag/v1.7.3).


 Bueno, pues yo os dejo que me voy a probar todos estos cambios.... vosotros ya estais tardando.




---


**ACTUALIZADO 14-11-20:**  
Acabamos de recibir una nueva notificación de que Valve ha liberado una nueva versión de Proton, en concreto la 5.13.2 con las siguientes novedades:


Actualizado vkd3d-proton a la versión 2.0, que mejora la compatibilidad con Direct3D 12.  
 Lobbies multijugador de Risk of Rain 2 fijos.  
 Corregido el bloqueo de Killer Instinct cuando termina la batalla.


Corregido Assetto Corsa Competizione y Summer Funland en modo VR.  
 Corregido los juegos que no se iniciaban en Uplay Connect y Origin fallaba al actualizar.  
 Corregido el cursor del mouse en Mount & Blade II: Bannerlord.  
 Corregido el bloqueo de SpellForce al iniciarse en algunos sistemas.  
  Corregidos los tiempos de carga muy largos en Warhammer 40k: Inquisitor - Martyr.  
 Atelier Ryza: Ever Darkness & the Secret Hideout se puede volver a jugar.  
 Corregidos Healer's Quest y Coloring Game 2 en AMD.  
 La resolución escalada ahora es funcional para Middle Earth: Shadow of War.  
 Restaurado el comportamiento de la entrada de desplazamiento horizontal de versiones anteriores de Proton.  
 El directorio de registro de proton ahora se puede configurar con PROTON_LOG_DIR.


Tienes toda la información en la [página del proyecto en Github](https://github.com/ValveSoftware/Proton/releases/tag/proton-5.13-2).




---


**NOTICIA ORIGINAL (15-10-20):**


Hacía tiempo que no teníamos noticias de Valve sobre Proton, su conocido desarrollo Open Source , que como sabeis reune diversos proyectos (Wine, DXVK, FAudio, VKD3D...) en uno para conformar el genial software que nos permite ejecutar en Steam juegos de Windows como si fuesen nativos. La verdad es que más de uno, entre los que personalmente me incluyo, estábamos un tanto preocupados, pues desde el mes de Junio, que salió [la ultima beta que permitía jugar a Death Stranding](https://github.com/ValveSoftware/Proton/issues/4070) no hubo nuevas revisiones, ni mucho menos versiones de ningún tipo. Pero ante ese rumor que empezaba a circular por la red, especialmente en reddit, finalmente **Pierre-Loup Griffais** hace unas horas anunció el lanzamiento de esta nueva versión:



> 
> Proton 5.13-1 is now available for testing! [pic.twitter.com/XGOQnYyP1U](https://t.co/XGOQnYyP1U)
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [October 15, 2020](https://twitter.com/Plagman2/status/1316841044881752064?ref_src=twsrc%5Etfw)


  





 Como podeis ver no se trata solo de una revisión de la anterior versión, si no que se trata de una **nueva versión**, la 5.13, que se sincroniza con su homóloga en Wine e incorpora todas sus correcciones y añadidos. Existe una larga [lista de cambios](https://github.com/ValveSoftware/Proton/releases/tag/proton-5.13-1b) entre los que no vamos a destacar ninguno en concreto, pues pensamos que todos tienen gran importancia, por lo que os dejamos el listado completo:



* Los juegos recientemente jugables incluyen:
* + Red Dead Redemption 2
	+ Horizon Zero Dawn
	+ DEATH STRANDING
	+ Metal Gear Solid 5: Ground Zeroes
	+ Final Fantasy XV
	+ Sea of Thieves
	+ Star Wars: Battlefront II
	+ Call of Duty: WWII
	+ Call of Duty: Infinite Warfare
	+ Call of Duty: Modern Warfare
	+ Asssassin's Creed: Rogue
	+ Assassin's Creed IV Black Flag
	+ South Park: The Fractured But Whole
	+ DiRT Rally 2
	+ Age of Empires II: Definitive Edition
	+ Age of Empires III
	+ Dragon Quest Builders 2
	+ Ashes of the Singularity: Escalation
	+ Tron 2.0
	+ AO Tennis 2
	+ Fight'N Rage
	+ Woolfe - The Red Hood Diaries
* Integración mejorada con el cliente Steam, que corrige muchos títulos de Call of Duty, así como Spelunky 2, Torchlight III, Path of Exile y RPG Maker MZ.
* Inicios de soporte real para todo tipo de reproducción de video. Los juegos que usan bibliotecas de video más antiguas deberían comenzar a trabajar con esta compilación. Estamos trabajando para mejorar el soporte para bibliotecas de videos más nuevas.
* Soporte mejorado para sistemas con múltiples monitores.
* Mejoras en la reproducción de audio en Halo 3, Beyond: Two Souls y Tomb Raider 2.
* Mejoras en el multijugador de rFactor 2 y en el multijugador de Call of Duty: Advanced Warfare.
* Representación de texto fijo en SD GUNDAM G GENERATION CROSS RAYS.
* Wine actualizado a la versión 5.13. 256 cambios en Proton 5.0 fueron actualizados o ya no son necesarios.
* DXVK actualizado a [v1.7.2](https://github.com/doitsujin/dxvk/releases/tag/v1.7.2) .
* Actualizado faudio a [20.10](https://github.com/FNA-XNA/FAudio/releases/tag/20.10) .
* Actualizado a la última [versión de vkd3d-proton](https://github.com/HansKristian-Work/vkd3d-proton) .
* Construido contra [la próxima generación de Steam Linux Runtime](https://steamcommunity.com/app/221410/discussions/0/1638675549018366706/) .
* Notas para los usuarios que compilan Proton: 
	+ Esta rama tiene nuevos submódulos. Por favor, usad `git submodule update --init --recursive`al actualizar.
	+ El sistema de compilación ahora depende de poder compilar código Rust.
	+ Si usais Vagrant VM, debeis destruirla y reconstruirla.


Como veis no se trata de moco de pavo, y **los cambios son muy importantes, por lo que ahora depende de nosotros es probar nuestros juegos y si encontramos problemas, reportarlos en la [web del proyecto](https://github.com/ValveSoftware/Proton/issues).** Yo personalmente pienso hacerlo este fin de semana con una buena ristra de títulos. ¿A que esperais vosotros? Como siempre os decirmos podeis dejarnos vuestros comentarios en este artículo o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


