---
author: leillo1975
category: Carreras
date: 2020-02-06 09:10:24
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >@Foppygames</span> ha a\xF1adido un nuevo escenario, la ciudad.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/FoppyGames/MaxDownforce_copy.webp
joomla_id: 1166
joomla_url: juega-gratis-a-max-downforce-un-juego-de-carreras-como-los-de-antes
layout: post
tags:
- retro
- open-source
- sonlink
- gpl
- max-downforce
- foppygames
- appimage
- robbert-prins
title: Max Downforce, un juego de carreras libre como los de antes (ACTUALIZADO 3)
---
@Foppygames ha añadido un nuevo escenario, la ciudad.


**ACTUALIZACIÓN 25-5-20:** Algún tiempo llevábamos sin tener noticias de este juego de carreras Open Source tan rápido y adictivo, y es que nos acaba de llegar la notificación a través de itch.io de que el juego acaba de ser actualizado a la versión 1.2, con lo que incluye novedades interesantes entre las que destaca la inclusión de una nueva pista, la ciudad, donde correremos freneticamente de noche entre farolas y edificios. El total de [novedades](https://foppygames.itch.io/max-downforce/devlog/148945/version-120-released) es el siguiente:


  
*-Se añadió una tercera pista llamada 'City'.*  
 *-El circuito incluye una nueva canción de PlayOnLoop.com*  
 *-El juego se muestra ahora en una resolución de pantalla ancha*  
 *-Los coches tienen luces traseras*




---


**ACTUALIZACIÓN 15-2-20**: Una vez más os traemos buenas noticias por parte de este proyecto, y es que la inclusión del proyecto en la licencia GNU v3.0 ha dado sus primeros frutos con la inclusión de **soporte de un montón de gamepads y joysticks**, una vez más gracias al trabajo de @[SonLink](https://twitter.com/sonlink), llegando a la [versión 1.1.1](https://github.com/Foppygames/max-downforce). El juego ahora es mucho más manejable al permitir usar estos dispositivos de control, lo que facilita mucho la conducción del coche. Además el creador del juego también ha puesto su juego en [Itch.io](https://foppygames.itch.io/max-downforce) (además de [GameJolt](https://gamejolt.com/games/max_downforce/286316)) para darle más visibilidad y quien prefiera esta tienda.




---


**ACTUALIZACION 11-2-20**: Nos complace en anunciar que el autor de Max Downforce ha añadido a su proyecto la [GNU GPL v3.0](https://github.com/Foppygames/max-downforce/issues/1) (excepto la música, que tiene una [licencia diferente](https://www.playonloop.com/) ), por lo que aunque hasta ahora se podía disponer del código en la web de su proyecto, ahora el hacerlo y modificarlo reporta más garantías tanto para su creador como cualquier persona que quiera colaborar o reutilizar su código fuente. Esperemos que este [añadido](https://github.com/Foppygames/max-downforce/commit/5f147259b6cd0b53a71238b2240177f81f64d02d) atraiga más colaboraciones y el proyecto crezca con mejoras y nuevas caracteríticas.




---


**NOTICIA ORIGINAL:**  
Hace algún tiempo, paseando por Youtube viendo videos de juegos antiguos, me encontré con uno que me llamó especialmente la atención. Se trataba de un juego con un aspecto que enseguida me recordó a aquel juego de recreativas, [Pole Position](https://en.wikipedia.org/wiki/Pole_Position) (Namco-Atari, 1982), un título que en su época se comió gran parte de mis escasas pagas. Este juego es [Max Downforce](https://gamejolt.com/games/max_downforce/286316). Inmediatamente me puse a buscar información sobre él y vi que se podía descargar en [GameJolt](https://gamejolt.com/games/max_downforce/286316). Por desgracia el juego solo tenía versión para Windows, y me fuí a otra cosa.


El caso es que hace unos días me volvió a picar la curiosidad y investigando un poco vi que estaba creado con [Löve](https://love2d.org/) y [Lua](https://www.lua.org/about.html), y que aunque no tiene ninguna licencia, el código se puede conseguir facilmente en [Github](https://github.com/Foppygames/max-downforce). Tras algunas preguntas en nuestro canal de [Telegram](https://t.me/jugandoenlinux), al final conseguimos ejecutarlo en Linux. Entonces se nos ocurrió que estaría bien que los Linuxeros tuviesemos un paquete para descargar igual que el que hay para Windows y enseguida aparececió la posibilidad de crear un **AppImage**. Raudo y veloz, el miembro de nuestra comunidad @[SonLink](https://twitter.com/sonlink), se puso manos a la obra y gracias a él tenemos una **versión completamente operativa e independiente** de dependencias del juego, apta para descargar y jugar en cualquier distribución GNU/Linux.


Hablando un poco del juego y su creador, como os dijimos al principio se trata del **típico arcade de carreras de principios de los 80**, donde tendremos que seguir un trazado adelantando coches y evitando chocar para completar el mayor número posible de vueltas a uno de los dos circuitos del que dispone actualmente el juego. Para completar cada vuelta tendremos que llegar a la meta antes de que se nos acabe el tiempo. El juego cuenta con un aspecto de juego de 8 bits y cuenta con unos efectos de sonido y música muy acordes. En cuanto al creador, **Robbert Prins** ([Foppygames)](https://github.com/Foppygames), es un desarrollador independiente de los Paises Bajos, y vemos que **ha desarrolado multitud de juegos de este estilo en diferentes lenguajes** y para muy diversas plataformas, por lo que os animamos a echar un ojo a su [página web](http://www.foppygames.nl/).


Ya podeis descargar Max Downforce para Linux en formato AppImage desde su página en [Gamejolt](https://gamejolt.com/games/max_downforce/286316). Si quereis tener una idea más aproximada de como es os dejamos con un video donde podeis ver su jugabilidad:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/XOwFG436viI" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


¿Os gustan este tipo de juegos de principios de los 80? ¿Qué os parece la iniativa de nuestra comunidad de "portar" este tipo de desarrollos a nuestro sistema? Puedes contarnos lo que quieras sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

