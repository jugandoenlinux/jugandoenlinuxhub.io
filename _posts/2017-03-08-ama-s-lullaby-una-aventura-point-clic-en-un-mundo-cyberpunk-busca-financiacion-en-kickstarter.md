---
author: Pato
category: Aventuras
date: 2017-03-08 22:22:07
excerpt: "<p>El juego promete versi\xF3n para Linux y sin DRM</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4a071c64184f6ed127d1b90fcde1a863.webp
joomla_id: 239
joomla_url: ama-s-lullaby-una-aventura-point-clic-en-un-mundo-cyberpunk-busca-financiacion-en-kickstarter
layout: post
tags:
- indie
- aventura
- kickstarter
- point-and-clic
title: "'Ama's Lullaby' una aventura point &clic en un mundo cyberpunk busca financiaci\xF3\
  n en Kickstarter"
---
El juego promete versión para Linux y sin DRM

Ama's Lullaby es un llamativo juego de tipo "Point & Clic" al estilo de juegos como el mítico "Blade Runner". El juego se desarrolla en un mundo cyberpunk donde jugarás como Ama, una muchacha con un alto potencial que adora las ciencias computacionales. Explorarás la colonia conociendo a sus habitantes tanto humanos como "no humanos". Tendrás que negociar y tomar decisiones que influirán en la historia. Gracias al talento de Ama con la programación podrás hackear cualquier red de la ciudad, conseguir información gracias a un sistema de hackeo y usarla en tu beneficio. Pero ten cuidado por que cada acción tendrá sus consecuencias.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/vs3jM1icXzk" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


El desarrollo del juego se llevará a cabo en unos ambientes de gran calidad atmosférica, con diálogos entre los diferentes personajes. Cada sesión de juego promete ser distinta.


Ama's Lullaby está en Kickstarter y a falta de 16 días lleva poco mas de 7.000 €.


Si quieres aportar al poryecto puedes hacerlo en el siguiente enlace:


<div class="resp-iframe"><iframe height="420" src="https://www.kickstarter.com/projects/1005143815/amas-lullaby-a-point-and-click-game-in-a-cyberpunk/widget/card.html?v=2" width="220"></iframe></div>

