---
author: Pato
category: Aventuras
date: 2018-01-30 18:18:41
excerpt: <p>Resuelve el misterio de un oscuro castillo junto a un amigo</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/035f0b772a03f455045ea960b0176ce7.webp
joomla_id: 626
joomla_url: we-were-here-too-una-aventura-cooperativa-online-llegara-a-linux-steamos-el-proximo-2-de-febrero
layout: post
tags:
- indie
- aventura
- casual
- cooperativo
title: "'We Were Here Too' una aventura cooperativa online llegar\xE1 a Linux/SteamOS\
  \ el pr\xF3ximo 2 de Febrero"
---
Resuelve el misterio de un oscuro castillo junto a un amigo

Más novedades que están por llegar. 'We Were Here Too' es una aventura cooperativa exclusivamente online en primera persona, ambientada en un castillo medieval ficticio. Un juego que trata sobre el trabajo en equipo a través de la comunicación, el descubrimiento y la inmersión.


'We Were Here Too' [[web oficial](http://www.totalmayhemgames.com/)] es la secuela de '[We Were Here](http://store.steampowered.com/app/582500/We_Were_Here/)', juego que también está disponible para nuestro sistema favorito y que podéis jugar gratuitamente.


*A medida que se acerca el momento decisivo, aparece una gran estructura en la lejanía, sobresaliendo por encima de la incansable tormenta. Sin tener otro sitio al que ir, decides entrar en este lugar misterioso para quedar atrapado en el interior… ¿Serás capaz de encontrar una manera de salir?*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Mwq8c68BvOM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 'We Were Here Too' es un juego para jugarlo junto a un amigo online por lo que se hace imprescindible el contar con un micrófono en el PC. sus características principales son:


* Trabajo en equipo basado en una comunicación desafiante – Juega al juego con amigos o haz amigos nuevos a través de un sistema de navegación de servidores nuevo y mejorado
* Papeles distintos – los dos jugadores experimentan una historia asimétrica
* Puzles y entornos completamente nuevos – Gracias a las soluciones generadas de manera aleatoria ¡jamás dos partidas serán iguales!
* Banda sonora a cargo del compositor original tras *We Were Here*
* Hay multitud de secretos que encontrar y descubrimientos que hacer
* Un misterio oscuro y perverso - ¿Fue tu sacrificio realmente necesario?


En cuanto a los requisitos, son:


**Mínimo:**  

+ **SO:** Ubuntu 12.04 / SteamOS
+ **Procesador:** Intel Core i3 2100 o equivalente
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** Nvidia GTX 460 o equivalente
+ **Red:** Conexión de banda ancha a Internet
+ **Almacenamiento:** 5 GB de espacio disponible
+ **Notas adicionales:** Un micrófono compatible PC


**Recomendado:**  

+ **SO:** Ubuntu 12.04 / SteamOS
+ **Procesador:** Intel Core i5 2500k o equivalente
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** Nvidia GTX 750ti o equivalente
+ **Red:** Conexión de banda ancha a Internet
+ **Almacenamiento:** 5 GB de espacio disponible
+ **Notas adicionales:** Un micrófono compatible PC


'We Were Here Too' estará disponible el próximo día 2 de Febrero con menús traducidos al español en su página de Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/677160/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>
