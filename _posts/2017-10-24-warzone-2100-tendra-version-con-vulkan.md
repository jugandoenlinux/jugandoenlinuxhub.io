---
author: Serjor
category: Estrategia
date: 2017-10-24 07:56:24
excerpt: "<p>Los desarrolladores han decidido modernizar este cl\xE1sico de la estrategia</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/64337ffeaae67237594d79a8ceda7ce6.webp
joomla_id: 500
joomla_url: warzone-2100-tendra-version-con-vulkan
layout: post
tags:
- rts
- vulkan
- warzone-2100
title: "Warzone 2100 tendr\xE1 versi\xF3n con Vulkan"
---
Los desarrolladores han decidido modernizar este clásico de la estrategia

Leemos en [phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Warzone-2100-Vulkan) que la gente del equipo de Warzone 2100, uno de los mejores RTS de código abierto que tenemos, [ha comenzado a migrar](http://forums.wz2100.net/viewtopic.php?f=32&t=13651&sid=2998caa2d6e6c972aa8431d6c0d0674e) su motor a Vulkan.


Por ahora esta migración solo funciona en Windows, aunque ya dicen que es algo temporal, así que esperamos con ganas el día que liberen la versión para GNU/Linux, porque si sus requerimientos ya eran muy modestos, con Vulkan entendemos que bajarán aún más o que podrán implementar algunos efectos sin afectar al rendimiento. Siempre es interesante cómo un juego originalmente propietario sigue evolucionando gracias a la comunidad cuando se libera su código, y casi 20 años después, sigue activo.


Estar atentos a la web, porque quién sabe, una versión de Warzone 2100 con Vulkan es un candidato firme a las partidas de la comunidad que organizamos los viernes a la noche.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/yB-Kt3VBlMQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Y tú, ¿Cómo ves esta migración por parte del equipo de Warzone 2100? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o Discord(<https://discord.gg/ftcmBjD>).

