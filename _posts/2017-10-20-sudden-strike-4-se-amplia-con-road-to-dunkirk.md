---
author: Pato
category: Estrategia
date: 2017-10-20 16:44:11
excerpt: "<p>La expansi\xF3n del juego ya est\xE1 disponible en Linux/SteamOS</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b84c5756f6a889fa332015e4458021f9.webp
joomla_id: 495
joomla_url: sudden-strike-4-se-amplia-con-road-to-dunkirk
layout: post
tags:
- accion
- rts
- estrategia
title: "'Sudden Strike 4' se ampl\xEDa con 'Road to Dunkirk'"
---
La expansión del juego ya está disponible en Linux/SteamOS

Buenas noticias para los amantes de la estrategia bélica en tiempo real. '[Sudden Strike 4](index.php/homepage/generos/estrategia/item/550-sudden-strike-4-disponible-en-gnu-linux-steamos)' recibe una expansión hoy mismo. Se trata de 'Sudden Strike - Road to Dunkirk' que como su nombre indica nos llevará a combatir frente a las costas francesas en la famosa batalla de Dunkerque.


*En **Sudden Strike 4: Road to Dunkirk**, revivirás las batallas que dieron lugar a una de las misiones de rescate más audaces de la historia. Tras la invasión de Francia en 1940, la Fuerza Expedicionaria Británica y parte del ejército francés se habían retirado a Dunkerque y esperaban la evacuación, mientras que las fuerzas alemanas los arrinconaban para machacarlas.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/F7DTfKRld0g" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


**Características:**


 


* **Sudden Strike 4: Road to Dunkirk** incluye dos nuevas minicampañas con cuatro misiones para un solo jugador basadas en eventos relacionados con el famoso rescate de los soldados aliados en el pueblo costero de Dunkerque durante la **Operación Dinamo**.
* En la campaña alemana, deberás abrirte paso a través de las aguerridas defensas francesas en la **Batalla de Lille**, tomar la ciudad de Dunkerque y seguir avanzando para destruir un **acorazado británico**. En la campaña aliada, lanzarás la contraofensiva británica durante la **batalla de Ypres-Comines** y deberás tratar de evacuar al mayor número posible de soldados durante la **Operación Dinamo**.
* Podrás elegir entre dos nuevos comandantes aliados: el héroe de guerra francés **Charles de Gaulle** y el comandante de la división de la Fuerza Expedicionaria Británica **Harold Alexander** formarán parte de tus opciones estratégicas para superar cada misión.
* **Sudden Strike 4: Road to Dunkirk** introduce en el juego más de 10 unidades completamente nuevas, como la artillería alemana **SIG 33 Bison**, el carro de batalla francés **Char B1**, el tanque crucero británico **A10 Cruiser Mk. II** y el destructor **HMS Ivanhoe**.


Si estás interesado en esta expansión, la tienes disponible en su página de Steam con un 10% de descuento por su lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/606080/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Piensas defender Dunkerque o atacarás a las fuerzas aliadas?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

