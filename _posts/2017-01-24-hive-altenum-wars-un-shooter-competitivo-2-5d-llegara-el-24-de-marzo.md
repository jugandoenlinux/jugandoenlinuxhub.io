---
author: Pato
category: "Acci\xF3n"
date: 2017-01-24 21:30:57
excerpt: "<p>El juego del estudio castellonense \"Catness Game Studios\" tiene anunciada\
  \ la versi\xF3n para Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d383d2a7f18b38f50f531c6f6759cc5a.webp
joomla_id: 188
joomla_url: hive-altenum-wars-un-shooter-competitivo-2-5d-llegara-el-24-de-marzo
layout: post
tags:
- accion
- indie
- proximamente
- steam
- 2-5d
- competitivo
title: "'Hive: Altenum Wars' un shooter competitivo 2,5D llegar\xE1 el 24 de marzo\
  \ - ACTUALIZADO"
---
El juego del estudio castellonense "Catness Game Studios" tiene anunciada la versión para Linux

'Hive: Altenum Wars' [[web oficial](http://hive.catnessgames.com/)] es de esos títulos que a poco que salga bien se hará un hueco entre los jugadores más dedicados a la competición. Desarrollado por Catness Game Studios pasó con éxito su campaña de Greenlight y ahora nos anuncian su lanzamiento, suponemos en fase beta:



> 
> Today is the day! We will play Hive: Altenum Wars on March 24, 2017!! [#indiegame](https://twitter.com/hashtag/indiegame?src=hash) [#madeinspain](https://twitter.com/hashtag/madeinspain?src=hash) [#heroshooter](https://twitter.com/hashtag/heroshooter?src=hash) [pic.twitter.com/ag6dsdfXew](https://t.co/ag6dsdfXew)
> 
> 
> — Catness Game Studios (@CatnessGames) [20 de enero de 2017](https://twitter.com/CatnessGames/status/822432681774501888)



 El concepto es muy similar a juegos competitivos de tipo "Heroes y disparos" al mas puro estilo 'Overwatch' pero en perspectiva 2,5D. Se trata de un frenético juego de acción multijugador (exclusivamente) online futurista con un enfoque competitivo en el que lucharás seleccionando un personaje de entre los héroes disponibles, cada uno con características propias.  Además, el juego contará con distintos "escenarios" conectados a modo de exágono donde las características de cada uno como la gravedad serán distintas con lo que las partidas prometen cambiar según el escenario en el que te encuentres. Juzga por ti mismo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/Qjj7QsSzngE" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 'Hive: Altenum Wars' promete versión para Linux tal y como puedes ver en su [página de Greenlight](http://steamcommunity.com/sharedfiles/filedetails/?l=spanish&id=655839737), aunque no sabemos si saldrá simultáneamente junto con los demás sistemas. Si averiguamos algo más actualizaremos el artículo.


**ACTUALIZACIÓN:**


Desde Catness Game Studios nos confirman que 'Hive: Altenum Wars' llegará más tarde a Linux:



> 
> [@JugandoenLinux](https://twitter.com/JugandoenLinux) Gracias!! Solo tenemos la fecha de lanzamiento confirmada para Windows. Mac y Linux tendrán que esperar un pelin mas!
> 
> 
> — Catness Game Studios (@CatnessGames) [25 de enero de 2017](https://twitter.com/CatnessGames/status/824202927627706368)



 No es extraño que los estudios se tomen su tiempo en portar sus desarrollos a otras plataformas, y de hecho para muchos estudios es lo habitual. Estaremos atentos a las noticias del estudio para informar de la fecha definitiva del lanzamiento en nuestro sistema favorito.


¿Qué te parece el concepto de 'Hive: Altenum Wars? ¿Te gustaría que llegase a Linux? ¿Te gustan los juegos competitivos?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

