---
author: leillo1975
category: Ofertas
date: 2018-11-16 09:17:16
excerpt: <p>@humble nos trae montones de ofertas</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b9def02b6d20f4f0adb6e889f99af491.webp
joomla_id: 903
joomla_url: rebajas-de-otono-en-la-humble-store
layout: post
tags:
- rebajas
- ofertas
- humble-store
- otono
title: "\xA1Rebajas de oto\xF1o en la Humble Store!"
---
@humble nos trae montones de ofertas

De nuevo en la fantástica Humble Store están dispuestos a dejarnos sin blanca, o a ahorrarnos bastante dinero, según como se mire. En esta ocasión nos traen como suele ser habitual las [Rebajas de Otoño](https://www.humblebundle.com/store?partner=jugandoenlinux), y en ellas podremos encontrar **montones de juegos a precios bajísimos**, entre los que por supuesto se encuentran juegos con soporte para [Linux/SteamOS](https://www.humblebundle.com/store/search?sort=discount&filter=onsale&page=12&platform=linux&partner=jugandoenlinux). Nosotros hemos hecho una selección de estas ofertas, sobre todo teniendo en cuenta la calidad y por supuesto los gustos de la gente de nuestra comunidad, poniendo **especial relevancia en los juegos a los que solemos jugar online**:


-[F1 2017](https://www.humblebundle.com/store/f1-2017?partner=jugandoenlinux) (-75%): **13.74€**


-[Torchlight II](https://www.humblebundle.com/store/torchlight-ii?partner=jugandoenlinux) (-80%): **3.79€**


-[Rocket League](https://www.humblebundle.com/store/rocket-league?partner=jugandoenlinux) (-50%): **9.99€**


-[Borderlands 2 GOTY](https://www.humblebundle.com/store/borderlands-2-game-of-the-year?partner=jugandoenlinux) (-78%): **9.89€**


-[Life Is Strange: Complete Season](https://www.humblebundle.com/store/life-is-strange-complete-season?partner=jugandoenlinux) (-80%): **3.99€**


-[Deus Ex Manking Divided](https://www.humblebundle.com/store/deus-ex-mankind-divided?partner=jugandoenlinux) (-85%): **4.99€**


-[DIRT Rally](https://www.humblebundle.com/store/dirt-rally?partner=jugandoenlinux) (-80%): **6.99€**


-[Tomb Raider GOTY Edition](https://www.humblebundle.com/store/tomb-raider-goty-edition?partner=jugandoenlinux) (-75%): **7.49€**


-[Bioshock Infinite](https://www.humblebundle.com/store/bioshock-infinite?partner=jugandoenlinux) (-75%): **7.49€**


-[Civilization VI Digital Deluxe Digital Deluxe](https://www.humblebundle.com/store/sid-meiers-civilization-6-digital-deluxe?partner=jugandoenlinux) (-75%): **19.99€**


-[XCOM 2](https://www.humblebundle.com/store/xcom-2?partner=jugandoenlinux) (-75%): **12.49€**


-[7 Days to Die](https://www.humblebundle.com/store/7-days-to-die?partner=jugandoenlinux) (-64%): **8.27€**


-[This War of Mine Humble Deluxe Edition](https://www.humblebundle.com/store/this-war-of-mine-humble-deluxe-edition?partner=jugandoenlinux) (-70%): **5.69€**


-[Slime Rancher](https://www.humblebundle.com/store/slime-rancher?partner=jugandoenlinux) (-50%): **8.84€**


-[Kerbal Space Program](https://www.humblebundle.com/store/kerbal-space-program?partner=jugandoenlinux) (-60%): **15.99€**


-[GRID Autosport](https://www.humblebundle.com/store/grid-autosport?partner=jugandoenlinux) (-75%): **9.99€**


-[The Talos Principle](https://www.humblebundle.com/store/the-talos-principle?partner=jugandoenlinux) (-80%): **7.99€**


-[Trine 3 : The Arctifacts of Power](https://www.humblebundle.com/store/trine-3-the-artifacts-of-power?partner=jugandoenlinux) (-75%): **5.49€**


-[System Shock 2](https://www.humblebundle.com/store/system-shock-2?partner=jugandoenlinux) (-75%): **2.20€**


-[Shadow Warrior](https://www.humblebundle.com/store/shadow-warrior?partner=jugandoenlinux) (-75€): **8.74€**


-[Serious Sam 3 BFE](https://www.humblebundle.com/store/serious-sam-3-bfe?partner=jugandoenlinux) (-75%): **9.24€**


Os recordamos que **todos los enlaces a Humble Bundle que veis en este artículo son patrocinados, por lo que con vuestras compras estareis colaborando en el mantenimiento de esta web sin que os salga ni un céntimo más caro**. Esta es solo una pequeña selección, pero podeis buscar esos juegos que tanto tiempo llevais esperando para comprar y ver si estan rebajados. También podeis dejarnos las ofertas que considereis más interesantes en los comentarios o en nuestro grupo de [Telegram.](https://t.me/jugandoenlinux)

