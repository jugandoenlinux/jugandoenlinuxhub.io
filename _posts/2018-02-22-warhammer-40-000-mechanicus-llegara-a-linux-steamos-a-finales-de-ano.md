---
author: Pato
category: Estrategia
date: 2018-02-22 19:13:33
excerpt: "<p>El desarrollo correr\xE1 a cargo de Bulwark Studios y Kasedo Games</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/37006d1f78a382af9c665654f8162781.webp
joomla_id: 657
joomla_url: warhammer-40-000-mechanicus-llegara-a-linux-steamos-a-finales-de-ano
layout: post
tags:
- proximamente
- estrategia
- turnos
- warhammer
title: "'Warhammer 40,000: Mechanicus' llegar\xE1 a Linux/SteamOS a finales de a\xF1\
  o"
---
El desarrollo correrá a cargo de Bulwark Studios y Kasedo Games

Un nuevo juego de la saga Warhammer 40,000 llegará a nuestro sistema favorito a finales de año. El anuncio fue hecho en la [web comunitaria](https://www.warhammer-community.com/2018/02/20/exclusive-video-game-announcement-mechanicus/) de la saga donde claramente aparece Linux como sistema soportado.


*Ponte al mando de las tropas humanas más tecnológicamente avanzadas del Imperum: el Adeptus Mechanicus. En la piel del Magos Dominus Faustinius, liderarás la expedición al nuevo planeta Silva Tenebris, que ha sido recientemente descubierto. Gestiona recursos, descubre tecnologías olvidadas, planifica operaciones tácticas con la tecnología de Noosphere y gestiona todos los movimientos de tus Tech-Priests.*  
  
*Toda decisión que tomes afectará al desarrollo de las misiones y, en última instancia, decidirá el destino de las tropas que tienes bajo tus órdenes. Será importante que selecciones el camino correcto, ya que el Imperium depende de ello.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/9gIMZ0WyY88" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Aparte de esto, poco más se sabe. Aún no tenemos información sobre los requisitos necesarios, así que esperamos ir recibiendo noticias del desarrollo en próximas fechas.


Si quieres más información, puedes visitar la web del juego [en este enlace](https://www.mechanicus40k.com/) o visitar su página de Steam, que ya está disponible:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/673880/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Que te parece 'Warhammer 40,000: Mechanicus'? ¿Te gustan los juegos de estrategia de la saga Warhammer 40,000?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

