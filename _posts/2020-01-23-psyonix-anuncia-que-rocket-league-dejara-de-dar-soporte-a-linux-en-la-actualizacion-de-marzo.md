---
author: Pato
category: Deportes
date: 2020-01-23 19:17:53
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >@PsyonixStudios</span> aclara el proceso de reembolso y nos explica la raz\xF3\
  n de su abandono</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/RocketLeague/Rocket-League-ENDSUPPORT.webp
joomla_id: 1153
joomla_url: psyonix-anuncia-que-rocket-league-dejara-de-dar-soporte-a-linux-en-la-actualizacion-de-marzo
layout: post
tags:
- soporte
- rocket-league
- psyonix
- epic-game-store
title: "Psyonix anuncia que Rocket League dejar\xE1 de dar soporte a Linux en la actualizaci\xF3\
  n de Marzo (ACTUALIZADO). "
---
@PsyonixStudios aclara el proceso de reembolso y nos explica la razón de su abandono


**ACTUALIZACIÓN 25-1-20**: Debido al [cabreo generalizado](https://steamcommunity.com/app/252950/eventcomments/1734384016497895529/?tscn=1579896286) por parte de la comunidad Linuxera por la desaparición en un par de meses de este juego, y recogiendo las últimas quejas que reclamaban la devolución del dinero, Psyonix ha publicado un [comunicado en Reedit](https://www.reddit.com/r/RocketLeague/comments/etiih3/update_on_refunds_for_macos_and_linux_players/) para **aclarar los pasos a seguir para solicitar el reembolso** del importe del juego a todos los usuarios de Linux y Mac.


Esto se produce debido a que estaban recomendando a los que creaban un ticket en su web de soporte, que solicitaran la devolución en Steam, y estos últimos estaban denegándola debido a que el juego había sido comprado hace más de dos semanas o teníamos más de dos horas de juego. Por lo que parece **se han coordinado con Valve para que permita la devolución a todos los usuarios de Linux y Mac**. El proceso es el siguiente:


-Ve a la página [web de Steam Support](https://help.steampowered.com/en/)


-Selecciona Compras (**Purchases**)


-Selecciona Rocket League . Es posible que tenga que seleccionar "Ver historial de compras completo" (**View complete purchasing history**)


-Selecciona "Deseo un reembolso" (**I would like a refund**), luego "deseo solicitar un reembolso" (**I'd like to request a refund**)


-En el menú desplegable del Motivo (**Reason**), seleccione "Mi problema no aparece en la lista" (**My issue isn’t listed**)


-En notas, escriba "Por favor, reembolse mi versión Mac/Linux de Rocket League, Psyonix dejará de dar soporte" (**Please refund my Mac/Linux version of Rocket League, Psyonix will be discontinuing support**)


Si por alguna razón el proceso no funciona, Psyonix nos recomienda que abramos un [ticket en Valve](https://help.steampowered.com/en/wizard/HelpWithPurchase), seleccionemos "Tengo una pregunta sobre esta compra", y ellos iniciarán manualmente el proceso de reembolso desde allí.


En el comunicado también aclaran cuales son las **razones para abandonar a los usarios de Linux y Mac,** y basicamente es que **van a usar DirectX11 en próximas versiones**, el cual no tiene soporte en estos sistemas, y que imposibilita utilizar la actual implementación basada en DirectX9 que está ligada a OpenGL. Esto implicaría tener que invertir recursos para convertir de DX11 a Vulkan/MoltenVK/Metal. Por desgracia, y según nos cuentan, **la base de jugadores de Linux y Mac juntos no llegan al 0.3% de jugadores activos** (lo cual nos parece exageradamente baja), y esto no justifica una inversión para crear y mantener clientes nativos, especialmente pudiendose usar Wine o Bootcamp.




---


**NOTICIA ORIGINAL:** 


Muy malas noticias para los muchos aficionados a Rocket League de la comunidad Linux que juegan al popular juego de Psyonix. 


Según leemos en su [último comunicado](https://steamcommunity.com/games/252950/announcements/detail/1709614218556357755), **Psyonix planea dejar de dar soporte para el juego online para las versiones de Rocket League de Linux y Mac** en la próxima actualización de Marzo, que será además su actualización final.


A partir de entonces el juego **ya no recibirá soporte ni actualizaciones y no será posible jugar con otros jugadores a través de internet.** La compañía afirma que en todo caso el juego seguirá funcionando como hasta ahora en su modo local para un jugador o a pantalla partida, y en todo caso remite a los jugadores que lo deseen a descargar la versión Windows si se quiere mantener el juego actualizado.


Como es sabido, tras la compra de Psyonix por parte de Epic, el movimiento que acaban de hacer se veía venir. De todos es sabido que la compañía detrás de Fortnite no presta atención a nuestro sistema favorito.


Por otra parte, nos queda el consuelo de intentar ejecutar el juego desde Wine/Proton, ya que **Rocket League** parece estár funcionando bien con capas de compatibilidad, taly como podemos ver en [protondb.com](https://www.protondb.com/app/252950).


Duros tiempos nos tocan con estas decisiones.


¿Qué piensas de esta decisión de Psyonix con Rocket League? 


Cuéntamelo en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

