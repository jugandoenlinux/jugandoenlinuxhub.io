---
author: leillo1975
category: Rol
date: 2017-03-17 08:33:14
excerpt: "<p>Ya podemos comprar la \xFAltima expansi\xF3n en Steam</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a28321ba8f3530fc856783a3123dd44c.webp
joomla_id: 255
joomla_url: nuevo-dlc-para-deus-ex-mankind-divided-a-criminal-past
layout: post
tags:
- feral-interactive
- deus-ex
- rpg
- mankind-divided
- square-enix
title: 'Nuevo DLC para Deus Ex Mankind Divided: A Criminal Past'
---
Ya podemos comprar la última expansión en Steam

En el día de ayer Feral Interactive anunció en su cuenta de Twitter que ya estaba disponible para GNULinux/SteamOS la última aventura de Adam Jensen. Square Enix lo lanzó hace poco menos de un mes en Windows, y ahora también podemos disfrutarlo nosostros.


 



> 
> Deus Ex: Mankind Divided - A Criminal Past DLC now on Linux! Go undercover deep inside a hostile prison.<https://t.co/uJchPoyjil> [pic.twitter.com/w4umEZwaYi](https://t.co/w4umEZwaYi)
> 
> 
> — Feral Interactive (@feralgames) [16 de marzo de 2017](https://twitter.com/feralgames/status/842414132842381313)



 


En esta expansión se narran hechos ocurridos antes de lo ocurrido en Mankind Divided, relatándose la primera misión de Adam para el GO29. En ella deberá infiltrarse en una prisión de alta seguridad para aumentados para recopilar información sobre otro agente desaparecido. Por supuesto, si no tienes [Deus Ex: Mankind Divided](index.php/homepage/generos/accion/item/69-deus-ex-mankind-divided-ya-disponible-para-linux-steamos), te recomendamos que te hagas con él, pues es un juego altamente recomendable, sobre todo si te gustan los juegos de Rol y sigilo.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/6SnKzFwYsvg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Si quieres comprar Deus Ex: Mankind Divided - A Criminal Past DLC pásate por Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/413370/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Si quieres decir cualquier cosa sobre esta nueva DLC puedes hacer un comentario en esta noticia, o usar nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

