---
author: Serjor
category: Carreras
date: 2018-11-12 21:08:44
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\"></span><a class=\"ProfileHeaderCard-screennameLink\
  \ u-linkComplex js-nav\" href=\"https://twitter.com/supertuxkart\"><span class=\"\
  username u-dir\" dir=\"ltr\">@<b class=\"u-linkComplex-target\">supertuxkart</b></span></a>\
  \ llega la primera Versi\xF3n Candidata</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/dd5c623449414fc88253ba0a5cd4d3e1.webp
joomla_id: 900
joomla_url: los-desarrolladores-de-supertuxkart-buscan-betatesters-para-el-juego-en-red
layout: post
tags:
- beta
- super-tux-kart
- supertuxkart
title: "Los desarrolladores de SuperTuxKart buscan betatesters para el juego en red\
  \ (ACTUALIZACI\xD3N 2)"
---
[@**supertuxkart**](https://twitter.com/supertuxkart) llega la primera Versión Candidata

**ACTUALIZACIÓN 5-4-19:** Ya queda menos.... poco a poco la gente que desarrolla SuperTuxKart va acercándose cada vez más a la **esperadisima versión 0.10** del juego, que como todos sabeis tiene como principal novedad el **apartado multijugador**, el cual llevamos meses probando con éxito. En esta ocasión, tal y como [hemos podido leer en su blog](http://blog.supertuxkart.net/2019/04/supertuxkart-010-release-candidate-1.html) nos avisan que el juego ha alcanzado su primera "**Release Candidate**". SuperTuxKart 0.10 RC1 incluye, además de las numerosas correcciones que han hecho desde la última vez, la **posibilidad de jugar sobre una LAN**, con lo que el apartado Multijugador está completo al añadirse esta última característica a la ya testeada modalidad online.


También **se han hecho cambios y añadidos en los circuitos**, por lo que la antigua pista de la mansión ha sido reemplazada por la nueva pista mejorada de la **mansión Ravenbridge**; y el **circuito Black forest** es ahora también parte del set de pistas oficial de SuperTuxKart.


![ravenbridge mansion 2019](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperTuxKart/ravenbridge_mansion-2019.webp)


Podeis **descargar este juego** para nuestro sistema (así como Windows, Mac y Android) en su [página del proyecto en Sourceforge](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.10-rc1/), donde encontrareis tanto los binarios como el código.... Por supuesto, que duda cabe que [si encontrais errores debeis reportarlos](https://github.com/supertuxkart/stk-code/issues) para que la versión final salga lo más libre de bugs posible.


 




---


**ACTUALIZACIÓN 11-1-19:** A dos meses tras el anuncio que realizaron buscando Betatesters (Noticia Original abajo), los desarrolladores de SuperTuxKart acaban de anunciar hace un ratito que ya su juego es lo suficientemente estable para lanzar una beta con las novedades que anteriormente vimos. Pese a ser la [primera versión beta](http://blog.supertuxkart.net/2019/01/supertuxkart-networking-010-beta-release.html), están lo suficientemente seguros de que puede estar ya muy próxima en calidad a una versión candidata. Comparándola con la última versión estable (0.9.3) sus novedades más significativas (muchas ya las conocemos de la Alpha)  son las siguientes:


-Ahora puedes competir contra tus amigos o contra gente de todo el mundo en carreras online, ya sea en juegos LAN o WAN.  
 -Puedes crear un servidor en tu propia computadora, todo manejado por SuperTuxKart, encender un servidor autónomo, o conectarse a servidores que ya están funcionando.  
 -Una RaspberryPi es lo suficientemente potente como para actuar como un servidor de juegos (aunque se necesita un binario sólo para servidores). Disponemos de varios servidores de juegos en VPS y Pis que están en constante evolución.  
 -En los llamados'servidores clasificados' se mantiene una clasificación global de los jugadores en línea. Puedes ver el ranking actual [aquí](http://addons.supertuxkart.net/rankings.php).


-Lo nuevo en esta beta comparada con nuestra alfa (además de las correcciones de errores y mejoras en la interfaz gráfica) es la posibilidad de unirse a un servidor mientras una carrera está en progreso: te convertirás en un espectador y podrás seguir la carrera en progreso. Te unirás a la diversión de las carreras cuando empiece la próxima carrera.


Recordad que podeis dejar feedback en sus [Foros](https://forum.freegamedev.net/viewforum.php?f=16), y  si encontrais errores en esta versión podeis reportarlos en [Github](https://github.com/supertuxkart/stk-code/issues). Aquí os dejamos una muestra de una partida online que jugamos hace poco donde vereis lo divertido que es:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/IHAr0kpU-NI" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Podeis descargar el código fuente de esta primera beta de SuperTuxKart en [Sourceforge](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.10-beta1/). Seguiremos informando....


 




---


**NOTICIA ORIGINAL:** Gracias a [reddit](https://www.reddit.com/r/linux_gaming/comments/9wg6u4/supertuxkart_needs_testers_for_onlinemultiplayer/) vemos que en el blog de SuperTuxKart están [buscando betatesters](http://blog.supertuxkart.net/2018/11/supertuxkart-networking-looking-for.html) ¡para el juego en red!


La verdad es que es una grandísima noticia, ya que tal y cómo comentan están cerca de sacar una nueva versión, y que han llegado a ese momento donde necesitan hacer más pruebas, pero han debido estar invitando a gente de su canal de IRC a la beta, y no esperan grandes errores.


Con el juego online vienen algunos modos de juego que prometen bastante (y que probaremos en jugandoenlinux, por supuesto):


* Carrera normal
* Time trial
* Modo batalla
* Soccer (tiembla Rocket League)
* Capturar la bandera
* Free-for-all, que dicho así parece un modo todo vale muy loco


También nos dicen que tienen 20 servidores por todo el mundo, pero que si queremos podemos montarnos nuestro propio servidor, que puede funcionar incluso en una Raspberry Pi 3, ahí es nada.


La verdad es que para los que llevamos muchos años jugando en linux, mucho antes incluso de que Steam aterrizara, SuperTuxKart es un juego al que la gran mayoría le tenemos mucho cariño y el juego en red ha sido una de las grandes peticiones por parte de la comunidad.


Y tú, ¿te apuntarás a probar esta beta? Si lo haces cuéntanos qué tal funciona en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

