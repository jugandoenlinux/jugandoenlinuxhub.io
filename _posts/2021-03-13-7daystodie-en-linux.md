---
author: chema
category: Terror
date: 2021-03-13 21:30:17
excerpt: "<p>. @josemariadoom nos habla de su experiencia con este juego.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/7daystodie/7daystodie.webp
joomla_id: 1288
joomla_url: 7daystodie-en-linux
layout: post
tags:
- exploracion
- supervivencia
- 7-days-to-die
- the-fun-pimps
title: "7 Days To Die en Linux, la opini\xF3n de un lector"
---
. @josemariadoom nos habla de su experiencia con este juego.


Hola a todos, soy **Chema** un miembro de esta comunidad de JugandoEnLinux.com y me gustaría contaros un poco mi experiencia en [7 Days To Die](https://www.humblebundle.com/store/7-days-to-die?partner=jugandoenlinux), ya que llevo **más de 60 Horas jugadas** y digamos que he tenido, a partes iguales, la experiencia mas agradable y terrorífica de mi vida. En todo caso solo he de decir que este juego ofrece todo lo que un aficionado a los videojuegos le puede llegar a gustar, pues mezcla diversos elementos que lo convierten en un título más que interesante, como son el misterio, el terror, la exploración, la creación y destrucción, los zombis, el transporte, las armas etc...


Empecemos con lo bueno:


**1-Exploración:** la exploración es maravillosa ya que te ofrece un mapa bastante grande que se siente muy vivo (entre los muertos), con casas, árboles, vegetación y zombis de todo tipo .


**2-Construcción:** La construcción es algo genial por que puedes, desde construir tu propia casa desde cero, hasta reconstruir las que encontraras dentro del juego.


**3-Crafting:** la creación de objetos puede ser simple y complicada a la vez, por que según que necesitas crear existen recursos limitados que solo puedes obtener explorando o comprando.


**4-Tienda:** el tender@ te ofrecerá desde los objetos mas comunes, a los mas raros y eso cambia según el día, y también misiones que puedes hacer.


**5-Recursos:** existen distintas maneras de conseguir recursos, desde ir al tendero a comprar o recogerlos con tus propias herramientas.


**6-Ciclo día/noche:** La transición entre el día y noche es algo configurable, aunque si lo dejas por defecto, cada 3 segundos en el juego pasa un 1 minuto, lo cual obliga a tener cuidado y vigilar la llegada del anochecer, pues los zombis se vuelven más rápidos.


**7. Los 7 días:** si empezaste una partida normal sin tocar ninguna configuración prepárate, por que cada 7 días los muertos llegaran a tu puerta mas rápidos, mas cabreados y fuertes que normalmente.


 ![7d2d-1](https://cdn.cloudflare.steamstatic.com/steam/apps/251570/ss_573fbb7a06c0b269de2c1e562b0129412f42792f.1920x1080.jpg)


Vamos ahora con lo malo:


**1-La optimización:** al ser un juego en Acceso Anticipado, que continua en desarrollo , la optimización no es lo mejor del mundo , aunque es aceptable. En mi caso lo tengo en lo mas bajo y el juego reacciona bien.


**2-Banda Sonora:** en ciertos momentos, la música puede llegar a aburrir un poco.


**3- La historia:** es casi inexistente (después de un ataque nuclear, estamos en un mundo abierto devastádo por la guerra, y en un lugar de EEUU), y no tiene mucho más que sobrevivir y pasarte misiones.


El equipo en el que he disfrutado de este título tiene las siguientes especificaciones:


**Sistema:** Ubuntu


**Procesador**: Intel Core i5-3550 a 3.30GHz × 4


**Memoria RAM**: 16GB


**Gráfica:** Nvidia GT-710


**Disco Duro**: SSD


Y hasta aquí poco más tengo mas que contar. Desde mi punto de vista se trata de un buen juego, altamente recomendable. Me gustaría agradecer a JugandoEnLinux y  @Laegnur la recomendación de este juego. Os dejo con un trailer del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/TCI12fe2s3I" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


Podeis comprar 7 Days To Die con soporte nativo para GNU-Linux en la **[Humble Store](https://www.humblebundle.com/store/7-days-to-die?partner=jugandoenlinux) (patrocinado)** o en [Steam](https://store.steampowered.com/app/251570/7_Days_to_Die/).

