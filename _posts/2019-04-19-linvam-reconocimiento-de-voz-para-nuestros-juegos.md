---
author: Serjor
category: Software
date: 2019-04-19 14:50:32
excerpt: <p>Una alternativa libre y nativa a VoiceAttack</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/abf584041f321d5f52601632cbc05493.webp
joomla_id: 1025
joomla_url: linvam-reconocimiento-de-voz-para-nuestros-juegos
layout: post
tags:
- voiceattack
- linvam
title: LinVam, reconocimiento de voz para nuestros juegos
---
Una alternativa libre y nativa a VoiceAttack

Hace ya bastante un amigo mio me enseño en su casa dos cosas que me dan bastante envidia (sana, eso sí) de los windowseros, y no es otra cosa que el combo Elite Dangerous y VoiceAttack. Afortunadamente viendo que el Elite Dangerous en [protondb](https://www.protondb.com/app/359320) está calificado como plata, por lo que puede que algún día lo juegue (pero desde que tengo el No Man's Sky no me llama tanto, la verdad), así que eso está superado, pero lo de VoiceAttack reconozco que he estado buscando alternativas, e incluso plantearme hacerlo yo mismo (pero ni idea), desde hace tiempo y sin suerte... Hasta hoy :-)


Y es que navegando por Reddit, me he encontrado este [post](https://www.reddit.com/r/linux_gaming/comments/beykcf/linvam_linux_based_voice_activated_macro_tool/), donde el propio desarrollador de [LinVam](https://github.com/aidygus/LinVAM) (Linux based Voice activated macro tool) nos presenta esta interesante herramienta, y motivado por la falta de algo parecido a VoiceAttack (por suerte él si ha hecho algo).


No lo he podido probar aún, y él mismo dice que está en una fase muy temprana de desarrollo, pero esperemos que avance y evolucione poco a poco y se convierta en una herramienta lo suficientemente estable como para poder usarla habitualmente durante nuestras partidas.


Os dejo con el vídeo de demostración con el que nos presenta su herramienta, curiosamente, con Elite Dangerous:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/hXB9eQmcGfQ" width="560"></iframe></div>


Y tú, ¿tienes alguna utilidad que usas durante tus sesiones de juego? Coméntanos cuál en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

