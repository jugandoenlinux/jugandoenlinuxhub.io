---
author: P_Vader
category: "An\xE1lisis"
date: 2021-09-25 13:04:16
excerpt: "<p>Probamos este BRUTAL juego de @southeastgames</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/paint_the_town_red/paint_the_town_red.webp
joomla_id: 1368
joomla_url: analisis-paint-the-town-red
layout: post
tags:
- roguelike
- fps
- voxel
title: "AN\xC1LISIS: Paint the Town Red "
---
Probamos este BRUTAL juego de @southeastgames


Hemos estado probando este juego recientemente lanzado por [southeastgames](https://www.southeastgames.com/) que amablemente nos ha hecho llegar una copia para que lo probemos y compartamos con vosotros que nos ha parecido. Este estudio Australiano independiente compuesto por un desarrollador y un artista, ha conseguido estrenar  su juego en julio de 2021, después de casi 6 años de desarrollo. Un juego que nació en una [competición](http://7dayfps.com/) de desarrollo de videojuegos tipo Jam.


Paint the Red Town es un **FPS muy caótico que se permite la licencia de llevar la violencia al mayor extremo**, es cierto que se "suaviza" en base a su estilo Voxel, pese a todo personalmente lo encasillo en juegos para adultos y no lo veo adecuado para crios. Cuando digo que es brutal, es que es MUY muy brutal. Esto es el club de la lucha con voxeles.


La versión para PC tiene el añadido de tener un editor de niveles, un modo sandbox y modos multijugador en linea. Este último lo he probado y no he conseguido encontrar partidas que me permitan entrar a jugar.


![PTTR BikerBar 01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/paint_the_town_red/PTTR_BikerBar_01.webp)


El juego principalmente se puede jugar de tres modos diferentes, según lo que guste o te apetezca.


 


**LAS PROFUNDIDADES.**   
Este quizás sea el modo de juego principal de juego. Es un roguelike o mazmorras con una historia mas o menos a seguir. Puedes escoger seis personajes con diferentes habilidades, por ejemplo un brujo, un policía de prisiones o un espectro, como ves de lo mas vario pinto.


![PTTR Ruins 03](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/paint_the_town_red/PTTR_Ruins_03.webp)  
Hay un gran abanico de enemigos que te vas a cruzando, desde calaveras de fuego flotando (¿Doom?), zombis, arañas y un largo etcétera. Y para completarlo debes enfrentarte a tres jefes "dioses" que te van a costar un ojo de la cara acabar con ellos.  
Este modo es intenso a ratos y se desvia de ese juego "todo violencia" y nada mas. En este caso el juego es un poco mas profundo, con cierta historia.  
El mapa de este modo se compone de diferentes "biomas".


![PTTR Necromancer 02](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/paint_the_town_red/PTTR_Necromancer_02.webp)  
Personalmente no soy un fan de los roguelike y aunque me ha tenido enganchado este modo, personalmente prefiero los otros modos mas viscerales, de repartir tortas y no complicarme demasiado la vida.


 


**ESCENARIOS**.  
 Quizás es el mas gamberro y entretenido para mi. Es un tipo de juego para pasar el rato, "descargar" estrés y divertirse un rato (¿dije *el club de la lucha*?). Tienes diferentes localizaciones de lo mas propicias para que se lie una tangana. Los diferentes escenarios que podemos elegir **nos sitúa en un bar de moteros, en una prisión, un un salón del viejo oeste, en una discoteca de los 70 o en la taberna de un puerto pirata**. Claramente sitios donde un simple estornudo puede dar lugar a empezar a repartir tortas.  
Este modo se trata de que inicies la pelea asestando un golpe a cualquier pobre criatura que se cruce en tu camino y a partir de ese momento empiezas a liquidar gente y que no te liquiden, así de simple.


![PTTR PirateCove 01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/paint_the_town_red/PTTR_PirateCove_01.webp)  
Para ello te ayudas de tus puños y sobre todo la patada que es bastante dañina, claro que la potencia de la misma debe recargarse cada vez que la usas.Lo cachondo del juego es que los escenarios están repletos de objetos y armas que te ayudan muy mucho a conseguir tu objetivo, como **el típico cuchillo, la jarra de cerveza, bola de billar, un taburete, una guitarra eléctrica... todo se puede usar para matar**, con mayor o menor efectividad.  
Tiene el plus que tiene una serie de potenciadores que se activan conforme vas acumulando defunciones en tu marcador, tipo onda expansiva, puño de dios.... te puede imaginar la masacre que producen.


![PTTR Prison 01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/paint_the_town_red/PTTR_Prison_01.webp)  
Por ultimo, las partidas de escenarios **tienen modificadores con decenas de cambios en el estilo del juego, tanto físicos como estéticos**, armamento ilimitado, modos zombies, etc. El que mas que ha llamado la atención es **el modo SUPERHOT** que copia el estilo blanco y la fisica del famoso juego. Está muy bien para ir variendolos y no cansarte de jugar siempre la misma mecanica.


 


**ARENA**.


![PTTR EndlessArena 01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/paint_the_town_red/PTTR_EndlessArena_01.webp)  
 Bueno, como su propio nombre indica esto es un salvase quien pueda pero **en este caso nos traslada a un coliseo, donde lógicamente solo puede quedar uno en pie** y mejor que seas tu. Este modo se compone de diferentes rondas, donde en cada una de ellas deberás de eliminar a todos tus enemigos de la arena. La primera ronda no es demasiado complicada, pero rápidamente la cosa se tuerce y empieza a incrementarse la dificultad. Desde bandas de moteros, Yakuza, o una manada de boxeadores nada fáciles de tumbar. Ademas tiene el modo infinito, enemigos sin fin, buena suerte con eso!


En el aspecto gráfico no hay nada que reprochar, con un estilo Voxel y desenfadado cumple perfectamente para meterte en el papel y no ser tan explicito. Todos los escenarios y personajes están bastantes bien cuidados. **Las animaciones son muy buenas, llegando a sorprenderme en alguna ocasión y sacándome alguna carcajada de lo mas sádica**.   
Solo hay una cosa que realmente no se si es un fallo o falta de pericia y es que no consigo enganchar ciertos golpes, por ejemplo, si el enemigo a caído al suelo a veces es difícil acertarle otro golpe para rematarlo sobre el suelo.


Su **banda sonora es muy buena y cumple perfectamente con lo que se espera de cada escenario con cambios de ritmo perfectos** en según que momentos. La verdad que la música y los efectos ayudan en la inmersión del momento. Muy buen trabajo en este apartado.


Y aquí os dejamos un video jugando una partida a Paint y analizando un poco en detalle:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/dOzCeL_sV3Q" title="YouTube video player" width="780"></iframe></div>


 


Lo podéis **encontrar en Steam** ademas incluye una demo para probarlo:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/337320/" style="border: 0px;" width="646"></iframe></div>


 


¿Que os parece el juego? Un poco gore pero bastante entretenido. Cuentanos tu opinión en nuestro canales habituales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

