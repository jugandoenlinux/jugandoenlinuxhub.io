---
author: leillo1975
category: Software
date: 2019-02-01 18:36:15
excerpt: "<p>Tras meses de arduo trabajo la esperada versi\xF3n de <span class=\"\
  username u-dir\" dir=\"ltr\">@LutrisGaming ha sido liberada.</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2f2766fb7d5da7e6231d9ac592175eb3.webp
joomla_id: 964
joomla_url: lutris-0-5-ya-esta-finalmente-disponible
layout: post
tags:
- gog
- wine
- lutris
- software-libre
title: "\xA1Lutris 0.5 ya est\xE1 finalmente disponible!"
---
Tras meses de arduo trabajo la esperada versión de @LutrisGaming ha sido liberada.

Después de [varias betas](index.php/homepage/noticias/39-noticia/1063-lutris-se-prepara-para-la-conquista-de-la-galaxia-con-una-beta-que-anade-integracion-con-gog) y provocando una **expectación completamente justificada** en cada una de ellas, acaba de publicarse oficialmente la **versión 0.5**, siendo esta sin duda la más ambiciosa de cuantas hayamos podido ver. Reconozco que hasta hace unos pocos meses no conocía este estupendo gestor para los jugones del pingüino. No era por ninguna razón especial, simplemente era desconocimiento y que en ese momento [PlayOnLinux](index.php/homepage/generos/software/12-software/1030-playonlinux-llega-a-la-version-4-3), otro magnífico software, llenaba mis necesidades. Quizás todo vino a raiz del bendito **DXVK** y las dificultades que me producía en ciertos momentos su instalación, algo que con Lutris se reduce a simplemente marcar una casilla.


Pero Lutris **no se limita tan solo a gestionar la instalación de nuestros juegos con Wine**, sinó que va muchísimo más allá, siendo un completo **centro de todo nuestro software lúdico** desde un mismo programa. En él, a parte de nuestros juegos "emulados" con Wine, podemos incluir, gracias a sus múltiples "**Runners**",  todos aquellos **juegos nativos** de nuestro equipo, así como otros **lanzadores** como Scumm, ZDoom, DOSBox, o **emuladores** tan conocidos como MAME, Dolphin o PCSX2. Tan solo tendremos que enlazar y configurar estos y aparecerán todos en nuestra biblioteca de forma ordenada y sin tener que andar buscándolos. Además de esto, por si fuera poco, la comunidad crea **instaladores que permiten que la configuración de nuestros juegos se simplifique al máximo**, y simplemente tengamos que ejecutarlos desde su [página](https://lutris.net/) para poder usarlos.


El esfuerzo realizado en los últimos meses por sus desarrolladores a llegado a su fin y ahora el programa luce con un **aspecto completamente renovado**. siendo mucho más intuitivo y completo. Pero esto no se queda ahí por que **ahora podemos enlazar nuestra Biblioteca de juegos de GOG**, algo muy esperado por la comunidad que ya habíamos ido atisbando en las versiones de prueba. La lista de cambios va muchísimo más allá y la versión 0.5 luce los siguientes [cambios](https://github.com/lutris/lutris/releases/tag/v0.5.0):


*-Modernizada la interfaz de usuario de Gtk, gracias a las mejoras realizadas por @TingPing*  
 *-Añadido soporte GOG, permitiendo a los usuarios acceder a su cuenta, importar juegos y descargar los archivos del juego automáticamente durante la instalación.*  
 *-Agregadas opciones de importación de juegos más precisas, permitiendo importaciones de diferentes terceros, como Steam, GOG y juegos instalados localmente.*  
 *-Reestructurado el monitor de procesos. Esto corrige los problemas con los juegos que salen prematuramente. Muchas gracias a @AaronOpfer por sus parches!*  
 *- Ahora se pueden lanzar varios juegos al mismo tiempo sin perder el control del primer juego.*  
 *- La información y las acciones del juego se muestran ahora en un panel en el lado derecho. La búsqueda de portadas para el panel se añadirá en una versión futura, hasta entonces los archivos de imágenes de la portada se pueden colocar en ~/.local/share/lutris/coverart/[game-identifier].jpg*  
 *-Los juegos de lutris.net se pueden buscar e instalar desde el propio cliente.*  
 *-Nuevo comando install_cab_component installer para juegos basados en Media Foundation.*  
 *-Añadida una caché de descarga para reutilizar los archivos entre instalaciones.*  
 *-Se muestran controladores gráficos y GPU al inicio*  
 *-Rediseño del selector de instalador.*  
 *-Añadido un botón para mostrar los scripts del instalador antes de la instalación.*  
 *-Añadida una opción de limitador de FPS cuando libstrangle esté disponible (<https://gitlab.com/torkel104/libstrangle>)*  
*-Re-diseño de varias partes de la aplicación (vistas, detección de características de linux, clase de juego principal, ....)*


También hemos visto que **se ha rediseñado [su página web](https://lutris.net/) con un aspecto más actual y profesional**. Desde JugandoEnLinux os recomendamos que actualiceis cuanto antes (ya está disponible en los [repositorios oficiales](https://lutris.net/downloads/)), y si aun no lo habeis probado.... pues no perdais un segundo más, por que os aseguramos que vale la pena. Nosotros os dejamos aquí por que nos vamos a probar todas las novedades de este imprescindible "pedazo" de software.


¿Y tu que opinas de Lutris? ¿Lo sueles usar con frecuencia? ¿Conocias ya sus novedades? Opina en los comentarios o charla sobre Lutris en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

