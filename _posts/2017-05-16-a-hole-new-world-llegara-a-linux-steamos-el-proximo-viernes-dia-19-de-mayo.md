---
author: Pato
category: "Acci\xF3n"
date: 2017-05-16 17:50:38
excerpt: <p>El juego es obra de los gaditanos "MadGearGames"&nbsp;</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/be76d1a55ee5ffb3b2dc895570c95b36.webp
joomla_id: 325
joomla_url: a-hole-new-world-llegara-a-linux-steamos-el-proximo-viernes-dia-19-de-mayo
layout: post
tags:
- accion
- indie
- proximamente
title: "'A Hole New World' llegar\xE1 a Linux/SteamOS el pr\xF3ximo Viernes d\xED\
  a 19 de Mayo"
---
El juego es obra de los gaditanos "MadGearGames" 

Todo aquel que ha vivido la época dorada de los 8 bits recuerda los juegos de aquella época y sus característicos gráficos. Pues "MadGearGames" se ha basado en esa estética y jugabilidad para desarrollar este 'Hole New World' [[web oficial](http://madgeargames.com/ahnw/)] que nos llegará este próximo día 19 con soporte para Linux desde su lanzamiento.


Se trata de un juego de acción de la vieja escuela "facil de jugar, dificil de dominar" donde tendremos que luchar contra el mal hasta derrotarlo. Simple, ¿no?


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/xOfvmuEpMOY" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Todo me recuerda a aquellos viejos juegos de las NES o Master Sistem... ¡que recuerdos!


#### **Sobre el juego:**



> 
> ¡La ciudad está siendo invadida por monstruos del Mundo Invertido! Encarna al Potion Master y acaba con el mal por ti mismo, sin tutoriales ni "modo fácil", ¡sólo con la ayuda de tu compañera, el hada Fäy y tus pociones!  
> Salta y dispara como en los viejos tiempos de los arcades. Lucha contra enemigos en tu mundo y en el Mundo Invertido. ¡Derrota a gigantescos jefes finales y consigue sus poderes!
> 
> 
> 


#### **Características:**


¡Ya sabes jugar! El reto está en el juego, no en complicados controles.  
¡Modo Historia con cinco mundos distintos, Modo Historia +, Modo Jefes Finales, Modo Reto y distintos finales!  
¡Más de 30 enemigos diferentes, 7 batallas con jefes finales y un montón de personajes secretos por buscar!


'A Hole New World" estará disponible en español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/434160/" width="646"></iframe></div>


¿Qué te parece? ¿Te gustan los juegos "de la vieja escuela"?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

