---
author: leillo1975
category: Estrategia
date: 2018-01-12 09:36:36
excerpt: "<p>La actualizaci\xF3n finalmente recae en nuestro sistema.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a955a3de9afa2794fbaf8cc2473586cb.webp
joomla_id: 604
joomla_url: llega-a-linux-steamos-el-fall-update-para-civilization-vi
layout: post
tags:
- aspyr-media
- actualizacion
- civilization-vi
- firaxis
title: Llega a Linux/SteamOS el 'Fall Update' para Civilization VI
---
La actualización finalmente recae en nuestro sistema.

Hace 3 meses os dábamos la [noticia](index.php/homepage/generos/estrategia/item/616-civilization-vi-nos-traera-dos-nuevas-facciones) de que Firaxis Games estaba a punto de lanzar una actualización "de otoño" para su gran [Civilization VI](index.php/homepage/analisis/item/352-analisis-sid-meier-s-civilization-vi). Como sabeis este juego ha sido uno de los grandes lanzamientos del pasado año y en esta web intentamos cubrir todas las novedades que generó, que no son pocas. Según acabamos de leer en [GamingOnLinux](https://www.gamingonlinux.com/articles/the-fall-2017-update-for-civilization-vi-has-finally-made-it-to-linux.11034), en esta ocasión, [**Aspyr Media**](http://www.aspyr.com/), que es la compañía responsable del port para Linux/SteamOS ha conseguido con un pelín de retraso traernos todas las mejoras y contenido de esta última actualización. Recordad que en la anterior actualización, "[summer update](index.php/homepage/generos/estrategia/item/584-por-fin-ha-llegado-el-summer-update-de-civilization-vi)", también se demoró un poco.


En cuanto al contenido de esta actualización destaca la inclusión de las **facciones de Indonesia y los Jemeres**, con sus correspondientes Líderes ([Dyah Gitarja](https://en.wikipedia.org/wiki/Tribhuwana_Wijayatunggadewi) y [rey Jayavarman VII](https://es.wikipedia.org/wiki/Jayavarman_VII) respectivamente), unidades especiales y mejoras exclusivas. Se añade el nuevo escerario "Camino al Nirvana" ambientado en el Océano Índico, ajustes en todo lo relaccionado con la religión para mejorar su uso, mejoras y ajustes en la inteligencia artificial, mejoras en la estabilidad del modo multijugador, cambios en el balanceo entre las diferentes facciones, y por supuesto montones de correcciones de errores. Podeis consultar la lista de cambios en este [documento](https://docs.google.com/document/d/1i0xyMjphQECXGcKJLzoZmChGaIlqsRIj8aEjl6u0koQ/edit) facilitado por Aspyr Media si quereis más detalles.


![CivilizationVI FallUpd Domrey](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisCiv6/CivilizationVI_FallUpd_Domrey.webp)


*Podremos hacer uso, entre otras unidades, de los Domrey (elefantes con Balista montada en su lomo)*


En cuanto a la **compatibilidad del multijugador** entre las plataformas, Aspyr ha [manifestado](http://steamcommunity.com/app/289070/discussions/0/144512942755435118/?ctp=76#c1635237606654684474) hace unos días que **tienen grandes problemas de desincronización** entre diferentes sistemas, además de estar enfocados en la preparación del jugoso próximo DLC [Rise and Fall](index.php/homepage/generos/estrategia/item/678-rise-fall-sera-la-proxima-gran-expansion-de-civilization-vi), del que esperamos informaros de su salida proximamente. Como veis una actualización indispensable para todos los que disfrutamos del rey de la estrategia por turnos. Esperemos que Rise and Fall esté listo, sinó en su salida en Windows, con menos retraso que en esta ocasión. Aún así agradecemos el esfuerzo hecho por Aspyr para traernos esta actualización.


¿Habeis jugado a Civilization VI? ¿Probareis las nuevas facciones? Respóndenos en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

