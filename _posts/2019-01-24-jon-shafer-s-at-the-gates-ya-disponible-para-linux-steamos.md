---
author: Pato
category: Estrategia
date: 2019-01-24 12:28:19
excerpt: "<p>Jon Shafer @JonShaferDesign es el dise\xF1ador de Civilization 5</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ee711835a5b8be9e56ead006e30e58e2.webp
joomla_id: 956
joomla_url: jon-shafer-s-at-the-gates-ya-disponible-para-linux-steamos
layout: post
tags:
- indie
- estrategia
title: Jon Shafer's At the Gates ya disponible para Linux/SteamOS
---
Jon Shafer @JonShaferDesign es el diseñador de Civilization 5

¿Hambriento de más estrategia? Bueno, la estrategia nunca está de mas, por lo que la llegada de este 'Jon Shafer's At the Gates' son buenas noticias para los estrategas de pro en Linux. Y es que gracias a [Gamingonlinux](https://www.gamingonlinux.com/articles/jon-shafers-at-the-gates-the-indie-strategy-game-from-the-designer-of-civilization-5-is-out.13419) nos llegaba la noticia de que el diseñador de Civilization 5 acaba de publicar su nuevo juego de estrategia junto a su estudio Conifer Games.


*"At the Gates es un juego de estrategia independiente de Jon Shafer, diseñador de Civilization 5. Eres un señor de la edad oscura que construye un reino para reemplazar al desmoronamiento del Imperio Romano. Administra tus clanes, explora el paisaje a tu alrededor, cosecha sus recursos y construye una poderosa máquina económica y militar."*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Dr3iL49ay4E" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Como características tendremos mapas dinámicos, gestión de personajes y clanes, elementos roguelike y supervivencia, una interfaz original y un estilo artístico basado en acuarelas.


Hay que decir que según las reviews de usuarios el juego tiene ideas muy buenas y originales, pero parece tener algún problema de estabilidad o rendimiento, pero realmente nada que no se pueda solucionar con futuros parches.


Jon Sahfer's At the Gates' **no está traducido al español**, pero si no es problema para ti y quieres saber mas, puedes visitar su [página web oficial](https://www.atthegatesgame.com/) . Puedes comprarlo en la [**Humble Store** (enlace patrocinado)](https://www.humblebundle.com/store/jon-shafers-at-the-gates?partner=jugandoenlinux) o su [página de Steam](https://store.steampowered.com/app/241000/Jon_Shafers_At_the_Gates/) , **en ambos con un  10% de descuento por su lanzamiento.**


¿Que te parece el planteamiento de Jon Shafer's At the Gates? ¿Le darás una oportunidad?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux)

