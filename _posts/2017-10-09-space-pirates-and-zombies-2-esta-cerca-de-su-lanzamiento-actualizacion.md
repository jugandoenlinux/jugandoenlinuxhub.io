---
author: Pato
category: "Acci\xF3n"
date: 2017-10-09 16:29:39
excerpt: "<p>\xA1El t\xEDtulo de acci\xF3n y exploraci\xF3n espacial ya est\xE1 disponible\
  \ en acceso anticipado!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1c16c516e210dd8881751624cc1315d7.webp
joomla_id: 477
joomla_url: space-pirates-and-zombies-2-esta-cerca-de-su-lanzamiento-actualizacion
layout: post
tags:
- accion
- indie
- espacio
- estrategia
title: "'Space Pirates and Zombies 2' est\xE1 cerca de su lanzamiento (Actualizaci\xF3\
  n 07-11-2017)"
---
¡El título de acción y exploración espacial ya está disponible en acceso anticipado!

**[Actualización 07-11-2017]** 'Space Pirates And Zombies 2' ya está disponible para su compra en acceso anticipado.




---


**[Actualización]** Los desarrolladores de 'Space Pirates And Zombies 2' han comunicado [en su última actualización](http://steamcommunity.com/games/252470/announcements/detail/2515684195369916032) que el juego saldrá de acceso anticipado y estará disponible oficialmente el próximo día 7 de Noviembre.




---


**[Artículo original]** Volvemos con más títulos que están a punto de llegar. Bueno, este en concreto ya lo tenemos disponible en acceso anticipado. Se trata de 'Space Pirates and Zombies 2' [[web oficial](http://minmax-games.com/SpacePiratesAndZombies/about.php)] que aunque el nombre lo sugiera, los desarrolladores dejan claro desde un primer momento que no se trata de una secuela directa del primer "SPAZ" tal y como puede leerse en la web:


*“Nota Importante: SPAZ 2 no es una secuela directa de SPAZ1. Es un juego nuevo con mecánicas de juego nuevas pero desarrollado dentro del mismo universo y con el mismo reparto. SPAZ 2 tiene como tema la exploración y la supervivencia, y las mecánicas reflejan esto. Aquellos que busquen una secuela del primer SPAZ no lo encontrarán aquí. Aquellos que busquen añlgo nuevo, ciertamente lo encontrarán."*


Tal y como anunciaron en su [último comunicado de desarrollo](http://steamcommunity.com/games/252470/announcements/detail/1435941902397112740), la versión de Linux está en desarrollo, aunque les ha supuesto un desafío por el (según sus palabras) desconocimiento de la plataforma, pero afirman que el desarrollo va a buen ritmo a medida que aprenden, y esperan tener lista la versión 1.0 del juego para dentro de poco, pero no especifican fechas.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Cuol96JX9W4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 'Space Pirates and Zombies' no está disponible en español, pero si te va la temática y no es problema para ti puedes hacerte con el juego en acceso anticipado a un precio inferior a como saldrá en versión definitiva, en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/252470/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


¿Piensas surcar el espacio en busca de aventuras?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 


 

