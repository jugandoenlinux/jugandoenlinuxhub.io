---
author: P_Vader
category: "Exploraci\xF3n"
date: 2022-11-01 13:01:59
excerpt: "<p>Vuelve por segundo a\xF1o la competici\xF3n para crear juegos con Minetest.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Minetest/minetest_game_jam_2022.webp
joomla_id: 1502
joomla_url: minetest-game-jam-2022
layout: post
tags:
- concurso
- minetest
- jam
title: Minetest Game Jam 2022
---
Vuelve por segundo año la competición para crear juegos con Minetest.


Por segundo año consecutivo vuelve este divertido concurso al estilo Jam, donde tendrás que crear un juego con la base del cada vez mas famoso Minetest.


  
Recordaros que **Minetest parece un clon software libre de Minecraft, aunque es similar en su aspecto realmente Minetest es muy diferente en el fondo.** Minetest es una base simple, juego tipo voxeles de contrucción y creación mediante bloques en un mundo abierto, hasta aquí el parecido. Lo grande es que **es posible ampliarlo con cientos de mods, incluso cambiar la mecánica con los "juegos" (<https://content.minetest.net/>)**, que son un conjunto de modificaciones y añadidos en forma de paquete y que lo pueden convertir en experiencias de juego muy diferentes y divertidas como juegos de laberintos, carreras de coches, pvp o captura la bandera. **Minecraft no facilita ni permite estas modificaciones** (aunque se hacen extraoficialmente).


  
Este año **el tema para la Jam** serán tres a elegir uno de ellos:  
**ESPACIO - SECRETOS - CUENTO**


La jam comienza el martes 1 de noviembre y tienes hasta el 21 para hacer tu juego. La clasificación se llevará a cabo el 28 y finalmente los resultados se anunciarán 30 de noviembre.


Todos los datos de la Jam la puedes encontrar en su nueva web: <https://jam.minetest.net/>  
O en su foro oficial: <https://forum.minetest.net/viewtopic.php?f=18&t=28802>


  
Aqui os dejo un video que grabamos el año pasado con los ganadores:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/Ir9GrkaWCUM" title="YouTube video player" width="720"></iframe></div>


Para la creación de mods y juegos, [Minetest dispone de una API](https://dev.minetest.net/Modding_Intro) haciendo uso de [LUA](https://www.lua.org/manual/5.1/), un lenguaje de programación relativamente sencillo de aprender e ideal para entornos educativos.


   
¿Te animas a participar? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)


 

