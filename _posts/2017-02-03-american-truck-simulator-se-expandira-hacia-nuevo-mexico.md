---
author: leillo1975
category: "Simulaci\xF3n"
date: 2017-02-03 10:38:31
excerpt: <p>SCS Software celebra el primer Aniversario de su simulador con este anuncio.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0847d5211daa2cfcfde28d825aa07d5d.webp
joomla_id: 206
joomla_url: american-truck-simulator-se-expandira-hacia-nuevo-mexico
layout: post
tags:
- american-truck-simulator
- ats
- scs-software
- nuevo-mexico
title: "American Truck Simulator se expandir\xE1 hacia Nuevo M\xE9xico"
---
SCS Software celebra el primer Aniversario de su simulador con este anuncio.

Con un escueto mensaje de Twitter y sin más detalles, SCS Software acaba de anunciar  que la siguiente expansión de su conocido juego American Truck simulator será Nuevo México



> 
> Yesterday was one year anniversary of American Truck Simulator's release. What's next for the game?  
>   
> New Mexico.<https://t.co/aL7tm3PcWE> [pic.twitter.com/T7fcU2fZ7w](https://t.co/T7fcU2fZ7w)
> 
> 
> — SCS Software (@SCSsoftware) [3 de febrero de 2017](https://twitter.com/SCSsoftware/status/827463754082287616)



Tompoco en su blog se dan más informaciones sobre dicha expansión, por lo que no sabemos su fecha de lanzamiento, y si será gratuita o de pago; aunque yo apuesto más por lo segundo, ya que tanto Nevada como Arizona acompañaron a precio cero a California (juego base), después [crecieron con el reescaldo](index.php/item/248-american-truck-simulator-crece-con-la-actualizacion-1-5), y como es lógico toca recuperar la inversión. Viendo anteriores trabajos de expansiones de SCS Software estoy seguro que será un trabajo de gran calidad como suele ser norma de la casa.


Personalmente ya apostaba a que la proxima expansión de ATS sería esta, ya que en el mapa Arizona se adentraba un poco en Nuevo México y daba la impresión de que dejaron preparada la entrada sur en el estado.


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATSNewMex2.webp)


Sentimos no poder dar más detalles sobre esta expansión, pero a medida que tengamos más datos os iremos informando. Podeis dejar vuestras impresiones sobre la noticia en los comentarios, así como en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).


 


**ACTUALIZACION:** Me acaba de confirmar el departamento de prensa de SCS Software que será una expansión de pago


 


 


**FUENTES**: [Twitter](https://twitter.com/SCSsoftware/status/827463754082287616), [Blog SCSSoft](http://blog.scssoft.com/2017/02/american-truck-simulator-heads-towards.html)

