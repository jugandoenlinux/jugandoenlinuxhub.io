---
author: Pato
category: "Acci\xF3n"
date: 2020-09-21 18:11:41
excerpt: "<p>The Game Kitchen&nbsp;@BlasphemousGame cumple su palabra para que podamos\
  \ disfrutar su <em>\xF3pera prima</em></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Blasphemous/blasphemousthemiracle.webp
joomla_id: 1257
joomla_url: blasphemous-ya-esta-disponible-de-forma-oficial-en-linux-steamos
layout: post
tags:
- accion
- indie
- plataformas
- metroidvania
- blasphemous
title: "Blasphemous ya est\xE1 disponible de forma oficial en Linux/SteamOS"
---
The Game Kitchen @BlasphemousGame cumple su palabra para que podamos disfrutar su *ópera prima*


 


Lo anunciábamos el jueves, y hoy ya tenemos disponible **Blasphemous** para disfrute y... ¿penitencia? de todo aquel que desee adentrarse en el oscuro mundo de Cvstodia. Poco podemos adelantaros que no sepáis ya. Tan solo deleitarnos con un juego de acción y plataformas de la más impecable factura pixel art, ambientado en un universo distóptico donde la locura y el fanatismo de su religión nos llevará a disfrutar de su endiablada jugabilidad y dificultad. Todo ello aderezado con violencia y destrucción en un juego en el que tomaremos el rol del Penitente, de la Orden del Lamento Mudo para salvar al mundo de la maldición del Milagro.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/RpN0WeQPixU" width="560"></iframe></div>


Una aventura de acción y plataformas, con mecánicas endiabladamente ajustadas y una jugabilidad digna de un Souls es lo que vamos a encontrar en Blasphemous, el magnífico juego del estudio Sevillano *The Game Kitchen*. 


Además, recibimos el DLC gratuito "**The Stir of Dawn**" **y las voces traducidas al español**. ¿Qué más se puede pedir?


Ya tienes Blasphemous disponible en [Humble Bundle](https://www.humblebundle.com/store/blasphemous?partner=jugandoenlinux) (enlace patrocinado) o en [Steam](https://store.steampowered.com/app/774361/Blasphemous/).


Desde jugandoenlinux.com os anunciamos que **estamos preparando una retransmisión** para esta misma noche para que podáis ver **Blasphemous** corriendo en nuestro sistema favorito.


Recuerda: El Milagro lo domina todo.

