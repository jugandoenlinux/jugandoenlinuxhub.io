---
author: Pato
category: Editorial
date: 2017-09-04 14:06:34
excerpt: "<p>\xA1Reg\xEDstrate en nuestra web y participa!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fe8bfc1b99c9ede76699e9aaec65452f.webp
joomla_id: 451
joomla_url: segundo-sorteo-del-aniversario-de-jugandoenlinux-com
layout: post
title: Segundo sorteo del aniversario de jugandoenlinux.com
---
¡Regístrate en nuestra web y participa!

Seguimos celebrando nuestro primer aniversario, y para ello ¿qué mejor que sortear juegos?.


En esta ocasión tan solo pedimos que respondáis en los comentarios de este artículo y nos contéis a qué juego o juegos habéis estado jugando en Linux últimamente, y si os ha gustado o no.


Eso es todo. Entraréis en el sorteo de estos dos juegazos:


Intrusion 2:


<http://store.steampowered.com/app/214970/Intrusion_2/> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/BJZAzNKZBew" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Guns of Icarus online:


<http://store.steampowered.com/app/209080/Guns_of_Icarus_Online/> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/GWRhvIY-GI0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Además, esta vez vamos a alargar el plazo, ya que el sorteo se llevará a cabo el próximo jueves día 7 a las 22:30h (GTM+2) así que tenéis tiempo. Y si no estás registrado en la web, ¡ya estás tardando!


Recordar que las bases de los sorteos están [en este artículo](index.php/homepage/editorial/item/567-bases-de-los-sorteos-de-videojuegos-del-aniversario-de-jugandoenlinux-com).


¡Mucha suerte a todos!

