---
author: Serjor
category: Software
date: 2019-03-09 10:26:09
excerpt: "<p>Peque\xF1as mejoras para aumentar la compatibilidad</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a71eb08b350426b2891f71e7eadd0246.webp
joomla_id: 992
joomla_url: nueva-version-de-proton-3-16-8
layout: post
tags:
- steam
- valve
- steam-play
- dxvk
- proton
title: "Nueva versi\xF3n de Proton, 3.16-8"
---
Pequeñas mejoras para aumentar la compatibilidad

Nos guste o no, proton ha venido para quedarse, y Valve sigue añadiendo cambios y mejoras para aumentar la compatibilidad con las versiones para Windows. Es por esto que hace unas horas aparecía la versión [3.16-8](https://github.com/ValveSoftware/Proton/wiki/Changelog#316-8) en GitHub, con los siguientes cambios:


* Corregido un fallo que hacía que en algunos juegos hechos con Unity el cursor se fuera a la parte inferior derecha de la pantalla.
* DXVK ha sido actualizado a la versión 1.0.
* Correcciones en el sistema de red para algunos juegos, mencionan en concreto Sword Art Online: Fatal Bullet.
* Mejorado el soporte para la API de steamworks en juegos antiguos, y en algunos relativamente nuevos, como Battlerite.
* Mejoras para algunos juegos que usan DX9 en determinados hardwares, mencionan en concreto Final Fantasy XI.


No es que sea una lista muy extensa de cambios, y es de suponer que la rama 3.16 está en modo mantenimiento, de ahí que quitando la actualización a DXVK 1.0, el resto sean correcciones o mejoras.


Y tú, ¿qué opinas de Proton? Cuéntanoslo en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

