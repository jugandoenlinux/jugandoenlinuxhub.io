---
author: leillo1975
category: Ofertas
date: 2018-07-13 07:25:19
excerpt: <p>Importantes descuentos en los juegos de esta conocida editora</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a47bdb2ae5a99dc68185bc94a1d4224c.webp
joomla_id: 803
joomla_url: grandes-ofertas-de-titulos-de-square-enix-en-humble-store
layout: post
tags:
- square-enix
- ofertas
- humble-store
title: "Grandes ofertas de t\xEDtulos de Square Enix en Humble Store."
---
Importantes descuentos en los juegos de esta conocida editora

Ayer por la noche nos leíamos un correo electrónico donde se nos informaba de esta promoción de Square Enix en la [tienda de Humble Bundle](https://www.humblebundle.com/store?partner=jugandoenlinux). En JugandoEnLinux.com hemos recopilado algunos de los juegos que han sido lanzados con soporte para nuestra plataforma (gracias a [Feral](http://www.feralinteractive.com/es/)), y si seguís leyendo vereis que son juegos de calidad contrastada, y que normalmente no defraudan a nadie, por lo que estais ante una buena oportunidad de haceros con ellos:


-[Rise Of Tomb Raider: 20 Year Celebration](https://www.humblebundle.com/store/rise-of-the-tomb-raider-20-year-celebration?partner=jugandoenlinux): **16.49€** (-67%)


-[Deus Ex - Mankind Divided](https://www.humblebundle.com/store/deus-ex-mankind-divided?partner=jugandoenlinux): **4.49€** (-85%) ([Season Pass](https://www.humblebundle.com/store/deus-ex-mankind-divided-season-pass?partner=jugandoenlinux): **3.74€**, [DLC System Rift](https://www.humblebundle.com/store/deus-ex-mankind-divided-system-rift?partner=jugandoenlinux): **3.95€** )


-[Life Is Strange - Complete Season](https://www.humblebundle.com/store/life-is-strange-complete-season?partner=jugandoenlinux): **4.99€** (-75%)


-[Tomb Raider](https://www.humblebundle.com/store/tomb-raider?partner=jugandoenlinux): **4.99€** (-75%) ([GOTY Edition](https://www.humblebundle.com/store/tomb-raider-goty-edition?partner=jugandoenlinux): **7.49€**)


 


Como sabeis, a menudo publicamos enlaces patrocinados de este sitio, pues nos reporta algún dinerillo (no mucho, no os penseis) que nos sirve de ayuda para pagar los gastos de Hosting, dominios, etc... y que todo esto siga funcionando. Lo mejor de todo esto es que a vosotros seguir nuestros enlaces no os cuesta ni un centimo más y a nosotros nos haceis un gran favor.

