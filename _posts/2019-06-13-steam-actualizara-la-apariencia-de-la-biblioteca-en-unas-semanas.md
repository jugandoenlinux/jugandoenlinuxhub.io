---
author: Pato
category: Noticia
date: 2019-06-13 17:39:43
excerpt: <p>La tienda de Valve sigue anunciando su necesitado remodelado, de momento
  para la beta de Steam</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d45a54d5d947891dd1cd75d25cbe05d0.webp
joomla_id: 1064
joomla_url: steam-actualizara-la-apariencia-de-la-biblioteca-en-unas-semanas
layout: post
tags:
- steam
- valve
title: "Steam actualizar\xE1 la apariencia de la biblioteca en unas semanas"
---
La tienda de Valve sigue anunciando su necesitado remodelado, de momento para la beta de Steam

Hace ya un tiempo que Valve [viene avisando](index.php/homepage/generos/software/12-software/1125-gdc-valve-muestra-el-remodelado-de-cara-de-steam) de que están trabajando en la remodelación de Steam, y ahora llegan noticias de que en unas semanas empezarán a llegar los cambios a la biblioteca. En un comunicado dirigido a los desarrolladores en Steam se anuncia la remodelación y se ofrecen datos, consejos y herramientas para que puedan adaptar la apariencia de los juegos dentro de la biblioteca de los usuarios, haciéndola más atractiva y funcional.


Sobre este artículo puedes ver cómo será la apariencia de la biblioteca una vez la remodelación tenga lugar, con una apariencia mas cercana a lo que es por ejemplo la tienda de GOG, con las carátulas de los juegos situadas en el cuerpo de la pantalla y un estilo visual más limpio.


Una vez seleccionemos un juego podremos ver la información relevante relacionada con todo detalle, incluyendo eventos de los desarrolladores, la comunidad, tiempo de juego, logros etc. de forma que quede todo mas claro y podamos tener una mayor cantidad de información de un vistazo:


![steambiblio2](/https://steamcdn-a.akamaihd.net/steamcommunity/publichttps://gitlab.com/jugandoenlinux/imagenes/-/raw/main/clans/4145017/b781fccc000c6153ee49423457d48536b156a562.webp)


Por otra parte, para todos los juegos de los desarrolladores que no puedan/quieran realizar las modificaciones que se anuncian por parte de Valve se les aplicará una plantilla genérica de forma automática para que no pierdan el formato una vez que se apliquen estos cambios, aunque como es normal el resultado no será lo óptimo que podría ser si se adaptasen a estos cambios.


Todo esto **estará disponible según anuncian dentro de unas semanas** y por supuesto **en la versión beta del cliente de Steam**, llegando al cliente estable una vez se hayan pulido los posibles problemas que pudieran surgir mas adelante.


Si quieres saber mas, puedes visitar el anuncio en el [blog dedicado a desarrolladores](https://steamcommunity.com/groups/steamworks/announcements/detail/1597002662762032240) donde está toda la información y la forma de maquetar todo lo necesario para los cambios que se avecinan.


Ya sabes que si quieres probar estos cambios de primera mano te toca pasar al cliente beta de Steam. En Jugando en Linux estaremos atentos a los cambios que vayan surgiendo al respecto.

