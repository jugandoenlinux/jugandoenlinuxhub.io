---
author: Serjor
category: Estrategia
date: 2020-03-21 14:44:21
excerpt: "<p>Y nos dicen que a partir del lunes 23 nos dar\xE1n nuevas noticias sobre\
  \ el juego</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Artifact/artifact-wip.webp
joomla_id: 1194
joomla_url: artifact-quiere-agradecer-publicamente-los-mensajes-de-apoyo-que-estan-recibiendo
layout: post
tags:
- valve
- artifact
title: "Artifact quiere agradecer p\xFAblicamente los mensajes de apoyo que est\xE1\
  n recibiendo"
---
Y nos dicen que a partir del lunes 23 nos darán nuevas noticias sobre el juego


Ayer leíamos en Steam un interesante anuncio, y es que de Valve se ha puesto [manos a la obra](https://playartifact.com/news/1821083382556531528) con este juego.


Copiamos y pegamos el contenido de su anuncio:



> 
> En primer lugar queremos agradeceros a todos vosotros vuestros tweets, mensajes y publicaciones. El interés continuo por Artifact ha sido muy alentador; también os agradecemos de todo corazón los comentarios que habéis estado enviando.  
>    
> Pronto empezaréis a ver algunos cambios: estamos haciendo pruebas en nuestro sistema e infraestructura. Estos cambios no deberían afectar a la versión existente de Artifact; pero, no obstante, queríamos mencionarlo.  
>    
> ¡Habrá más noticias en cuanto se lance Half-Life: Alyx!
> 
> 
> 


Y es que parece que Valve no ha abandonado este juego que en el momento de su lanzamiento enganchó a muchos aficionados de este género pero que casi con total seguridad el aspecto económico hizo que no llegara a despegar, ya que de lanzamiento costaba unos 20 euros cuando el resto de juegos de este estilo ofrecen la experiencia inicial de manera gratuita, y además, al igual que en el resto, había que pagar por nuevas cartas, con lo que creó una barrera de entrada a un juego que tampoco llegó a cumplir con las demandas de aquellos que sí apostaron por él.


Esperemos que en este nuevo lanzamiento, que algunos ya llaman Artifact 2, hayan escuchado a la comunidad y se hayan dejado aconsejar por aquellos que son jugadores profesionales para que esta vez sí cale en tanto en el circuito profesional, y sin duda en el amateur. Ahora, a esperar a que este 23 de marzo salga Half-life:Alyx para ver cuales son las novedades de este ¿Artifact 2?


Os dejamos con un vídeo que pudimos hacer en su momento gracias a Jaime:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/N0CxQ2gYj-8" width="560"></iframe></div>


Y tú, ¿tienes ganas de ver qué traerá esta nueva versión de Artifact? Cuéntanoslo en los comentarios o en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux)

