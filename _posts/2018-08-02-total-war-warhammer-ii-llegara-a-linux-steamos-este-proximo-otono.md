---
author: Pato
category: Estrategia
date: 2018-08-02 18:24:31
excerpt: "<p>Ya est\xE1 cerca el nuevo port de @feralgames</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ed665975b819d9e4bff8f3321152810d.webp
joomla_id: 816
joomla_url: total-war-warhammer-ii-llegara-a-linux-steamos-este-proximo-otono
layout: post
tags:
- proximamente
- feral-interactive
- estrategia
title: "Total War: WARHAMMER II llegar\xE1 a Linux/SteamOS este pr\xF3ximo oto\xF1\
  o"
---
Ya está cerca el nuevo port de @feralgames

Nuevas noticias sobre el nuevo desarrollo de la saga Total War para nuestro sistema favorito. Gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/total-war-warhammer-ii-to-release-for-linux-in-early-autumn.12255) nos llega la noticia de que el lanzamiento del nuevo titulo de la franquicia que está siendo portado por Feral Interactive llegará a nuestro sistema favorito el próximo otoño.


Esto es lo que han hecho público mediante un comunicado:


*¡El Nuevo Mundo te llama! Nuestro objetivo es lanzar Total War: WARHAMMER II para MacOS y Linux a principios de otoño.*


Suma y sigue por parte de Feral. Si tienes más curiosidad, puedes ver [el anuncio](index.php/homepage/generos/estrategia/item/895-total-war-warhammer-ii-tendra-version-linux-de-la-mano-de-feral-interactive) que hicimos en su momento del juego, o puedes visitar [la web de Feral](http://www.feralinteractive.com/en/games/warhammer2tw/) donde puedes ver mas información.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/WeDQQBwxRTU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 

