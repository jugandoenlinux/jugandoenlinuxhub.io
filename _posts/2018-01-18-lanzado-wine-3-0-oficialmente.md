---
author: leillo1975
category: Software
date: 2018-01-18 16:13:03
excerpt: <p>El conocido "emulador" trae soporte para Direct3D 10 y 11.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f59fd3a46f2adbbd9dd6269010353971.webp
joomla_id: 610
joomla_url: lanzado-wine-3-0-oficialmente
layout: post
tags:
- wine
- direct3d-10
- direct3d-11
title: Lanzado Wine 3.0 oficialmente.
---
El conocido "emulador" trae soporte para Direct3D 10 y 11.

Tal y como se había prometido y después de varias "Release Candidates", finalmente [acaba de salir la versión final y oficial de Wine 3.0](https://www.winehq.org/news/2018011801).  Como [habíamos comentado](index.php/homepage/generos/software/item/635-la-version-3-0-de-wine-se-espera-para-finales-de-ano-con-direct3d-11) hace unos meses, este lanzamiento viene cargado de importantes características, que aunque no son ninguna novedad, si son importantes pues pasan a estar soportadas de forma oficial. Tras un año de trabajo, el equipo de Wine ha dado forma a su desarrollo dotándolo con numerosos novedades, entre las que destacan:



> 
> -Soporte de Direct3D 10 y 11 (especialmente con el tema de los shaders)
> 
> 
> -Secuencia de comandos de Direct3D
> 
> 
> -Controlador de gráficos  de Android
> 
> 
> -Compatibilidad mejorada con DirectWrite y Direct2D
> 
> 
> 


Los trabajos encaminados a dar soporte de DirectX12 y Vulkan, así de como OpenGLES para activar el soporte de Direct3D en Android, han sido pospuestos para el siguiente ciclo de desarrollo de Wine. Como sabeis, Wine es un programa que permite ejecutar en Linux (y Mac) programas diseñados para Windows, y que es especialmente útil para poder disfrutar de muchos títulos que tristemente no tienen soporte para nuestro sistema, de hay su importancia en el mundo de los juegos en Linux.


¿Sois usuarios de Wine? ¿Cuales son los ultimos juegos y programas que habeis podido ejecutar gracias a él? Contesta en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

