---
author: Pato
category: Noticia
date: 2023-03-16 19:53:30
excerpt: "<p>Desde el 16 al 23 de Marzo miles de juegos rebajados con aniversario\
  \ de la Deck incluido</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamoferta/rebajasprimavera23.webp
joomla_id: 1529
joomla_url: comienzan-las-rebajas-de-primavera-de-steam-incluyendo-la-steam-deck
layout: post
title: Comienzan las rebajas de primavera de Steam, incluyendo la Steam Deck
---
Desde el 16 al 23 de Marzo miles de juegos rebajados con aniversario de la Deck incluido


Comienzan las rebajas de primavera de Steam un año más, y **desde hoy hasta el día 23** de este mes tendremos miles de juegos rebajados disponibles para comprar, incluyendo una sección de rebajas con los mejores juegos disponibles para la ultraportatil de Valve. 


Pero eso no es todo, ya que por estas fechas **se cumple un año desde que la Steam Deck llegó al mercado**, y Valve lo quiere celebrar con un vídeo de lo más curioso:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/ELtzcpb_j38" title="YouTube video player" width="560"></iframe></div>


Pero eso no es todo, ya que aprovechando las rebajas y el aniversario, **Steam está de rebajas también con la Steam Deck**. De ese modo, cualquiera que quiera adquirir la máquina de Valve lo puede hacer **con un descuento del 10% sobre el valor normal**, pasando la versión básica con 64GB de capacidad de valer 419€ a **377,10€**, la versión intermedia con 256GB de capacidad pasa de valer 549€ a **494,10€,**y la versión con 512GB de capacidad, de valer 679€ a valer **611,10€.** Puedes ver las ofertas [en este enlace](https://store.steampowered.com/sale/decktop100).


![ofertaDeck032023](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/ofertaDeck032023.webp)


En otro orden de cosas, también a través de [gamingonlinux.com](https://www.gamingonlinux.com/2023/03/steam-deck-hitting-retail-in-hong-kong-taiwan-and-later-japan-and-south-korea/) nos llegan noticias de que gracias a la colaboración del partner de Valve en Asia Komodo, junto a Acer Gaming **van a llevar la Steam Deck a las tiendas físicas** de esta última en Hong Kong, Japón, Taiwan y Corea del Sur para que puedan comprarse directamente, sin tener que hacer la compra online. ¿Llegaremos a ver un movimiento así en alguna cadena de tiendas físicas en Europa?


En cualquier caso, está claro que la máquina de Valve está siendo todo un éxito, y con medidas como estas, aún lo será más.


Como siempre, tienes toda la información sobre las rebajas en <https://store.steampowered.com/>

