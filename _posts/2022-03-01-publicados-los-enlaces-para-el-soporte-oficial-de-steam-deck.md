---
author: Pato
category: Hardware
date: 2022-03-01 10:39:48
excerpt: "<p>Las primeras unidades ya est\xE1n llegando a manos de los usuarios, y\
  \ algunas han sido entregadas por un mensajero especial</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/valve_steamdeck_photo_skus.webp
joomla_id: 1440
joomla_url: publicados-los-enlaces-para-el-soporte-oficial-de-steam-deck
layout: post
tags:
- steam
- steam-deck
title: Publicados los enlaces para el soporte oficial de Steam Deck
---
Las primeras unidades ya están llegando a manos de los usuarios, y algunas han sido entregadas por un mensajero especial


Las primeras **Steam Deck** ya están en manos de los primeros afortunados que la reservaron, y como es normal en cualquier producto nuevo pueden surgir dudas o problemas. Por eso Valve ha publicado los enlaces al soporte oficial de la Steam Deck.


En un [post oficial](https://steamcommunity.com/app/1675200/discussions/0/3186864498792143401/), el propio diseñador Lawrence Yang publica los enlaces junto a una lista de notas que pueden servir de ayuda a los primeros poseedores de una Deck:


Enlaces de interés:


* [Necesito soporte con mi Steam Deck](https://help.steampowered.com/en/wizard/HelpWithSteamDeck)
* [Quiero reportar un fallo](https://steamcommunity.com/app/1675200/discussions/1/)
* [Tengo una pertición de característica para Steam Deck](https://steamcommunity.com/app/1675200/discussions/2/)
* [Donde puedo saber más sobre Steam Deck?](https://www.steamdeck.com/)
* [Qué es el programa de verificación de Deck?](https://www.steamdeck.com/verified)
* [Como puedo utilizar el modo escritorio de Steam Deck?](https://help.steampowered.com/en/faqs/view/671A-4453-E8D2-323C)


Notas:


1. Enchúfala antes de arrancar para sacar la Steam Deck del modo de envío.
2. Felicidades! Tienes una de las primeras Steam Deck salidas de la fábrica. Siendo este el caso, verás algunas actualizaciones iniciales. Por ejemplo, el software del sistema (específicamente la BIOS de Steam Deck) requiere una actualización.
3. La actualización de la BIOS se llevará a cabo en pantalla técnica, lateral, pero no te preocupes, esto es completamente normal. Simplemente deja que haga lo suyo y estarás ante una experiencia mejorada de Steam Deck. Las actualizaciones futuras no se verán así (y los clientes en el futuro cercano no verán esto en absoluto).
4. Para otras preguntas, recursos y enlaces, consulte la lista anterior.
5. Que te diviertas! El equipo se está divirtiendo mucho enviando este producto, y esperamos que te encante Steam Deck tanto como a nosotros.


En otro orden de cosas, algunos afortunados estadounidenses están recibiendo su Steam Deck **de manos del mismísimo Gabe Newel!!**. Además, las unidades que entrega van firmadas de su puño y letra. Genial! no?


Valve ha publicado un vídeo promocional al respecto:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/9Dy-KWjp-m0" title="YouTube video player" width="560"></iframe></div>


Los afortunados viven en el estado de Wasington, en Bellevue cerca de las oficinas centrales de Valve obviamente, pero no deja de ser un gesto (y un buen punto de márketing, obviamente).


¿Qué te parece la campaña de marketing de Steam Deck? ¿Te gustaría ver a Gabe Newell en la puerta de tu casa para entregarte una?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix.](https://matrix.to/#/+jugandoenlinux.com:matrix.org)

