---
author: Pato
category: Software
date: 2018-07-03 15:04:01
excerpt: "<p>Con una impresionante demo de acci\xF3n en tercera persona</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ce43e0138300eb98aad587492745826b.webp
joomla_id: 791
joomla_url: godot-engine-nos-muestra-su-potencial-3d-en-un-video
layout: post
tags:
- software
- godot
title: "Godot Engine nos muestra su potencial 3D en un v\xEDdeo"
---
Con una impresionante demo de acción en tercera persona

Godot se está convirtiendo en un motor polivalente y una referencia dentro del sector del desarrollo de videojuegos. Tanto es así que en poco tiempo ha pasado de ser un motor para desarrollos mas "indie" a una opción muy a tener en cuenta para proyectos de gran envergadura. Y como se suele decir, para muestra un botón. Uno de los desarrolladores principales de Godot Engine ha publicado un vídeo de una demo que según sus palabras será publicada con la próxima versión de Godot, la 3.1:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/btazdd8jNEc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Desde luego no tiene desperdicio. Lo cierto es que se seguir así, será una alternativa seria para competir con Unity. ¿No crees?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


*Gracias a @**OdinTdh** por el aviso.*

