---
author: Odin
category: Software
date: 2023-03-01 16:55:19
excerpt: "<p>Tras <strong>17 betas</strong> y <strong>6 releases candidate</strong>\
  \ nos llega por fin la publicaci\xF3n de <strong>Godot 4.0</strong>.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Godot4/jel-godot.webp
joomla_id: 1526
joomla_url: la-espera-ha-terminado-godot-engine-4
layout: post
tags:
- software
- godot
- software-libre
title: La espera ha terminado... Godot Engine 4
---
Tras **17 betas** y **6 releases candidate** nos llega por fin la publicación de **Godot 4.0**.


Han tenido que pasar varios años de desarrollo para que podamos disfrutar de la nueva versión de este increíble motor de videojuegos. Para quien no lo conozca, [Godot Engine](https://godotengine.org) es la alternativa software libre al popular motor Unity3d.


Esta nueva versión viene realmente cargada de novedades ya que los desarrolladores han modernizado la arquitectura y han renovado grandes partes del código. A continuación te explicamos algunas de las mejoras:


 **[Renderizado](https://godotengine.org/article/godot-4-0-sets-sail/#3d-general-rendering-overhaul)**


Incluye una completa modernización de la arquitectura de renderizado que incluye 2 modos, uno orientado a la compatibilidad basado en **OpenGL** y que permitirá ejecutarse en ordenadores y móviles antiguos y otro orientado a gráficos de calidad, implementado en **Vulkan**, que aprovecha el hardware más moderno. Con respecto a este último modo incluye uso avanzados de materiales, niebla volumétrica, escalado mediante **AMD FSR** y varias técnicas de iluminación global como Voxel GI, **SDGFI**, entre otras.


 **[Físicas](https://godotengine.org/article/godot-4-0-sets-sail/#physics)**


Se ha mejorado el motor nativo de físicas de Godot, dando soporte a **soft bodies** y dándole capacidad de ejecución **multihilo**. Además permite la integración en tiempo de ejecución de otros motores, con lo que sería posible integrar motores de terceros como PhysX, Jolt y Box2D.


 **[Programación](https://godotengine.org/article/godot-4-0-sets-sail/#scripting)**


También han mejorado el lenguaje GDScript añadiéndole soporte para lambdas y además se ha aumentado su rendimiento. Otra novedad es la inclusión del sistema GDExtension que permite añadir nuevas funcionalidades a Godot en múltiples tipos de lenguaje sin necesidad de recompilar el motor. Actualmente, además del soporte para C++, se puede usar GDExtension con **Python, Rust y Go**. Con respecto al soporte para C#, ahora Godot utiliza NET SDK en vez de Mono, también se ha modernizado el API y se le da soporte a **NET 6** y **C# 10**.


 **[Multijugador](https://godotengine.org/article/godot-4-0-sets-sail/#networking-multiplayer)**


Se han añadido mejoras y ahora es más sencillo la creación de juegos multijugador ya que se ha reescrito el **API** y se da soporte a la replicación y sincronización de escenas mediante la adición de dos nodos adicionales **MultiplayerSpawner** y **MultiplayerSynchronizer**


 **Soporte Enterprise**


El fundador de Godot, junto con otros miembros activos de la comunidad de Godot, ha creado la empresa [**W4Games**](https://w4games.com/) que va a dar soporte comercial a empresas y desarrolladoras interesadas en exportar sus videojuegos hechos en Godot a videoconsolas como **Nintendo Switch, XBOX Series o PlayStation**. Esto va a incrementar la popularidad de Godot en medianos y grandes estudios que no lo usaban anteriormente por carecer de soporte comercial.


 **Otros cambios**


Debido a la gran cantidad de novedades es difícil explicar todas, no obstante, no queremos cerrar el artículo sin comentar al menos por encima que se ha hecho un gran trabajo en mejorar la usabilidad del editor, el soporte de mapas de tiles, realidad aumentada, mejoras en el audio y en el pathfinding.  
  
En resumen, como se puede ver la nueva versión de Godot viene cargada de novedades. Esperemos que cada vez más desarrolladores usen este motor ya que tiene un excelente soporte para Linux y esperamos con ganas los juegos nativos que se irán publicando con Godot 4.


  
Mientras tanto os dejamos con un video, creado por [**GdQuest**](https://www.gdquest.com/), que explica las novedades esta versión:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/chXAjMQrcZk" title="YouTube video player" width="720"></iframe></div>


¿Qué te parece [la nueva versión de Godot](https://godotengine.org)? Cuéntanos acerca de este increible motor de videojuegos en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

