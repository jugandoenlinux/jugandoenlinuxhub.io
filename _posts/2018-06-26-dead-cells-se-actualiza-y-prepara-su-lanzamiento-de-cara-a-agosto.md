---
author: Pato
category: Plataformas
date: 2018-06-26 21:21:30
excerpt: "<p>El juego de @motiontwin comienza a dar soporte a mods y anuncia una pr\xF3\
  xima subida de precio</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f8f336d27957a1b2f13b2daec1d1b4b0.webp
joomla_id: 785
joomla_url: dead-cells-se-actualiza-y-prepara-su-lanzamiento-de-cara-a-agosto
layout: post
tags:
- accion
- indie
- plataformas
- acceso-anticipado
title: '''Dead Cells'' se actualiza y prepara su lanzamiento de cara a Agosto'
---
El juego de @motiontwin comienza a dar soporte a mods y anuncia una próxima subida de precio

Si hace tan solo unos días te anunciábamos la llegada de 'Dead Cells' a nuestro sistema favorito, ahora nos hacemos eco de la última actualización del juego donde el estudio Motion Twin acaba de introducir el **soporte para mods**, eso sí, de manera experimental pues de momento solo permite modificar algunos aspectos básicos, y además **ya no hay que optar a la beta en las opciones del juego** puesto que nuestra versión ya está oficialmente en acceso anticipado.


Por otra parte, el estudio anuncia que ya están preparando la salida oficial del juego para el mes de Agosto, y con ese motivo **el juego sufrirá una subida de precio de 5$** (€?) justo cuando terminen las actuales rebajas de verano de Steam, por lo que si estás interesado en comprarlo, o conoces a alguien que lo quiera comprar este es el momento.


Para más información, u obtener la guía para modificar el juego puedes visitar el enlace al [post del anuncio](https://steamcommunity.com/games/588650/announcements/detail/1687043580915116210) en Steam.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/KV6fBYuuPMg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Si quieres conseguir Dead Cells, puedes comprarlo [en su página de Steam](https://store.steampowered.com/app/588650/Dead_Cells/) donde está en español y en acceso anticipado con un 40% de descuento por las rebajas.

