---
author: leillo1975
category: Estrategia
date: 2021-03-25 15:59:45
excerpt: "<p><span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@feralgames</span>\
  \ vuelve a la carga con la saga #TotalWar</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/TotalWar-Rome-Remasted/TWRR_Key-Art900.webp
joomla_id: 1293
joomla_url: total-war-rome-remastered-se-lanzara-en-linux
layout: post
tags:
- feral-interactive
- total-war
- creative-assembly
- rome-remastered
title: "\"Total War: Rome Remastered\" se lanzar\xE1 en Linux"
---
@feralgames vuelve a la carga con la saga #TotalWar


He de decir que me esperaba y no me esperaba a la vez esta noticia por parte de [Feral](https://www.feralinteractive.com/es/). Esto que puede parecer un trabalenguas tiene una sencilla explicación, y es que "me esperaba" que lanzasen una vez más un juego de la **saga Total War,** como suele ser habitual (o exclusivamente, me atrevería a decir). Lo que "no me esperaba" es que fuese una **remasterización del mítico Rome del año 2004**. Después de haber llorado lo indecible por las esquinas para que alguien lanzase una versión nativa de su segunda parte (2013), al menos podré quitarme el mono de romanos con este lanzamiento. La noticia nos la acaba de confirmar hace escasos minutos por correo la propia Feral Interactive, y también ha tenido su replica en las redes sociales, como podeis ver en twitter:  
  




> 
> Rome will rise again in Total War: ROME REMASTERED – coming to Windows, macOS & Linux on April 29th.  
>   
> The defining game of the award-winning series enters the modern era, now with 4K visuals, enhanced gameplay and modern features.  
>   
> Pre-purchase on Steam: <https://t.co/OENac19A8l> [pic.twitter.com/P9Ig2dXXyf](https://t.co/P9Ig2dXXyf)
> 
> 
> — Feral Interactive (@feralgames) [March 25, 2021](https://twitter.com/feralgames/status/1375101767600508933?ref_src=twsrc%5Etfw)







Como veis tendremos que esperar tan solo un mes, **al 29 de Abril**, para poder disfrutar de este clásico renovado. En cuanto a lo que nos vamos a encontrar, por lo que parece y los screenshots que podemos ver en la red, parece que no será un simple lavado del cara, y encontraremos una remasterización con las siguientes características:


*Total War: ROME REMASTERED te permite volver a experimentar el legado que cimentó la galardonada serie de videojuegos de estrategia. Esta es una remasterización en toda regla; además de añadir mejoras gráficas y de jugabilidad para modernizar la nueva versión, la esencia sigue siendo la misma que la del juego original.*


### *Mejora de aspectos visuales*


*ROME REMASTERED da nueva vida al clásico Rome: Total War con una serie de mejoras visuales:*


* *Optimización 4K*
* *Compatibilidad con resoluciones nativas UHD y pantallas ultra-widescreen*
* *Nuevos modelos de alta calidad para las unidades, los edificios y otros objetos*
* *Los efectos del clima y de la luz han sido actualizados*
* *Un mapa de campaña renovado con modelos de alta resolución*


![Rome_1](https://cdn.cloudflare.steamstatic.com/steam/apps/885970/ss_5e6c6227f94fc4cf355f840afa5dd8ead7f0e792.1920x1080.jpg)


### *Características modernas*


*La jugabilidad de Rome: Total War ha sido adaptada a los tiempos modernos con una serie de mejoras:*


* *Nuevos consejos y descripciones y una wiki dentro del juego*
* *Una interfaz de usuario con nuevos e intuitivos iconos*
* *Batallas 3D con nuevos iconos de unidad, marcadores de distancia de proyectiles y un nuevo mapa táctico*
* *Controles de batalla modernizados, familiares para todos los jugadores de títulos Total War recientes*
* *Un mapa estratégico de campaña lleno de información útil*
* *Listas rápidas de tus ejércitos, flotas, agentes y enclaves*


![Rome_2](https://cdn.cloudflare.steamstatic.com/steam/apps/885970/ss_1e290f9839c139afd77235f754f94d8123127713.1920x1080.jpg)


### *Mecánicas de juego renovadas*


*Hemos desbloqueado 16 facciones con sus ejércitos, lo que hace un total de 38 facciones disponibles entre las distintas expansiones. Además, y por primera vez en Total War, hemos añadido multijugador "cross-platform": podrás jugar con jugadores que usen un sistema distinto al tuyo, ya sea Windows, macOS o Linux. Por si esto fuera poco, ROME REMASTERED también introduce un nuevo agente al plantel con los mercaderes. Estos maestros de las finanzas recorren el mundo entero creando relaciones de comercio, dándote acceso a nuevos recursos y sobornando a sus rivales, de manera que te enriquezcas y establezcas tu gran poder económico.*


![Rome_3](https://cdn.cloudflare.steamstatic.com/steam/apps/885970/ss_b6eb84ba1301caa65a5ffccc568c0cd0c5fe80f0.1920x1080.jpg)


Podeis ir reservando el juego en la [Feral Store](https://store.feralinteractive.com/en/mac-linux-games/rometwremastered/), o en [Steam](https://store.steampowered.com/app/885970/Total_War_ROME_REMASTERED/), donde actualmente tiene un descuento del 50%. Mientras esperamos por nuevos detalles sobre el lanzamiento, que por supuesto reflejaremos en nuestra web, os dejamos con el Trailer de Feral que anuncia su próxima llegada:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/ED3yHK_mnAc" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


Que, ¿como se os ha quedado el cuerpo? ¿Esperabais este lanzamiento? ¿Habeis jugado al título original? Podeis dejarnos vuestras impresiones y opiniones sobre el lanzamiento de este juego en los comentarios en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

