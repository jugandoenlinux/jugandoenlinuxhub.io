---
author: Pato
category: Noticia
date: 2021-07-21 11:29:42
excerpt: "<p>El conocido programador con m\xE1s de 80 ports a Linux a sus espaldas\
  \ ve una amenaza en la herramienta que \xE9l mismo ayud\xF3 a crear</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Proton/valve-Proton.webp
joomla_id: 1329
joomla_url: ethan-lee-se-plantea-retirarse-de-la-programacion-debido-a-proton
layout: post
tags:
- ethan-lee
- ryan-c-gordon
- proton
- steam-deck
title: "Ethan Lee se plantea retirarse de la programaci\xF3n debido a Proton"
---
El conocido programador con más de 80 ports a Linux a sus espaldas ve una amenaza en la herramienta que él mismo ayudó a crear


Ethan Lee es sinónimo de ports a Linux. Más de 80 juegos han pasado por sus manos y ahora podemos disfrutarlos de forma nativa. Su trabajo respecto a nuestro sistema favorito es indiscutible y sus desarrollos son reconocidos por toda la industria. También ha trabajado en herramientas para que fuese factible traer videojuegos a Linux, como FNA o incluso fue contratado por Valve para que echase una mano en el desarrollo de ciertas partes de Proton.


Sin embargo, gracias a una entrevista en [nuclearmonster.com](https://nuclearmonster.com/2021/07/ethan-flibitijibibo-lee-may-retire-from-programming-due-to-valves-proton/) sabemos que está pensando en la retirada, y la "culpa" no es otra que la herramienta que él mismo ayudó a crear. Los hechos son los siguientes:


En esa entrevista, Ethan afirma que **Valve está promocionando Proton por encima de los ports nativos** en su página de Steam Deck dedicada a los desarrolladores. ¿Como?... afirmando que no es necesario un port nativo, si no que se pueden centrar en ofrecer compatibilidad a través de Proton. Incluso sospecha que Valve está contactando con desarrolladores "de forma agresiva" para que utilicen Proton como base en vez de buscar ports nativos. Puedes ver la página de Steam Deck para desarrolladores a la que hace referencia [aquí](https://partner.steamgames.com/doc/steamdeck/faq).


**Esto puede tener dos lecturas distintas**. De hecho, el portal incluso hizo la pregunta a otro conocido programador como es **Ryan "Icculus" C. Gordon**, y este respondió que el movimiento de Valve con Proton y Steam Deck puede tener como primera consecuencia **atraer a los desarrollos actuales hacia la máquina** haciendo que más y más público tenga su primera experiencia con el juego en Linux, **lo que tendrá como consecuencia que con la llegada de nuevos usuarios la viabilidad de desarrollos nativos sea posible rompiendo así el circulo vicioso en el que estamos metidos** desde hace años, de forma que los ports nativos deberían aumentar con el tiempo.


Por su parte, **Ethan** lo ve desde otro punto de vista. **Para el,** **Proton debería ser más que una herramienta para desplazar los desarrollos nativos, una forma de "preservar" desarrollos pasados** y que nunca van a llegar a ser portados a Linux. Una especie de compatibilidad "legado" con la que asegurar la compatibilidad "hacia atrás", con lo que los desarrollos actuales deberían ser promocionados hacia un desarrollo nativo.


De esta forma, Ethan ve como su negocio de ports se ve amenazado por Proton, ya que las compañías, al no ver una necesidad a la hora de hacer ports nativos (y confiar en que todo el trabajo lo haga Proton) pueden dejar de necesitar programadores que hagan ese trabajo, y por tanto él no ve perspectivas de futuro.


Ethan Lee además está en proceso de delegar la administración de FNA en otros desarrolladores y en última instancia, afirma tener aún contratos que terminar. Más allá de eso, se plantea la salida del mundo de la programación hacia otros horizontes laborales.


Por nuestra parte, Ya hemos hablado largo y tendido sobre lo que supone Proton. Lo cierto es que si bien es entendible la postura de Ethan, **sin Proton nunca hubiéramos llegado a donde estamos**. Y por otra parte, **por culpa de Proton los estudios no se han dignado (al menos hasta ahora) a mirar al juego nativo para linux como algo a tener en cuenta al dejar todo el peso del lado de Valve y el usuario**, y si no funciona lavarse las manos sin coste alguno y sin necesidad de ofrecerle soporte oficial.


En cuanto al circulo vicioso en el que estamos, está claro que sin un movimiento como el que está haciendo Valve nunca vamos a salir de donde estamos. Los juegos indie siguen llegando (si bien no todos los que deberían), pero los grandes desarrollos de hace unos años se ha parado casi en seco mal que nos pese a nosotros, y a Ethan Lee. Está muy bien tener juegos indie, pero el jugador medio busca jugar las novedades, tener cosas frescas, nuevos desarrollos que ilusionen. Si Linux quiere triunfar en la industria de los videojuegos n**o puede vivir eternamente de unos pocos ports**.


Como veis, también nosotros tenemos sentimientos encontrados. Solo el futuro definirá hacia dónde nos dirigimos, y si el juego nativo en Linux explotará o terminaremos dependiendo de Proton para jugar a todo en un futuro próximo. El resultado lo veremos en unos años.


Mientras tanto... ¿piensas que la apuesta de Valve promocionando Proton a los desarrolladores terminará trayendo en última instancia nuevos ports nativos o piensas que las compañías se lavarán las manos y necesitaremos Proton para todo matando de facto el desarrollo nativo?


 Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

