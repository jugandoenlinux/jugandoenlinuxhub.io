---
author: Son Link
category: "Acci\xF3n"
date: 2022-07-01 21:40:05
excerpt: "<p>Tras cinco a\xF1os sin una nueva versi\xF3n, esta revisi\xF3n \"menor\"\
  \ est\xE1 repleta de cambios muy interesantes</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Xonotic/xonotic_logo.webp
joomla_id: 1480
joomla_url: nueva-version-de-xonotic-0-8-5
layout: post
tags:
- multijugador
- fps
- xonotic
- multiplataforma
- codigo-abierto
title: "Nueva versi\xF3n de Xonotic: 0.8.5"
---
Tras cinco años sin una nueva versión, esta revisión "menor" está repleta de cambios muy interesantes


![screen01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Xonotic/screen01.webp)


Si hablamos de juegos de código abierto y del genero FPS (Firts Person Shooter), sin duda uno de los que nos vienen a la cabeza es **Xonotic**.


Para los que no conozcan este juego es un FPS del estilo de **Quake III Arena**, de hecho, como otros FPS de código abierto conocidos, como **Open Arena**, usa el motor del antes mencionado (ioquake3) con varias modificaciones, y como en el tenemos varios modos de juego, desde el típico **Todos Contra Todos**, hasta **Capturar la Bandera**.


Han pasado 5 años desde la ultima versión estable (la 0.8.2) y la lista de cambios que trae es muy grande, así que voy a resumir algunos de esos cambios:


### Balance:


Se han balanceado varias de las armas.


* La fuerza de empuje de **Vortex** se ha reducido de **400** a **200**, así que los jugadores alcanzados por ella no volaran hasta el otro extremo del mapa.
* Los pernos primarios de **Electro** ahora son capaces de detonar orbes secundarios en proximidad. Esto añade soporte para los combos en el aire, pero está desactivado por defecto.
* **Crylink** ahora tiene una propiedad, **linkexplode**, que hace que todos los balines exploten a la vez cuando alguno de ellos golpea a un jugador. Esto hace que el daño sea más consistente, ya que cuando sólo impactaba uno, los demás solo causaban daño por salpicadura.


### Duel


**Duel** (Duelo) es uno nuevo tipo de juego dedicado para 2 jugadores, por lo que ya no necesario crear un deathmatch con un número máximo de 2 jugadores. Esto permite una configuración de juego específica para el duelo y un manejo más limpio por parte de **XonStat**.


### Miscelaneo


* Los espectadores ya no se ven obligados a unirse al último hombre en pie.
* Los objetos como **Fuerza** y **Escudo** ahora aparecen inicialmente al mismo tiempo.
* Se permite soltar potenciadores al morir, aunque por defecto esta desactivado.
* Se ha rediseñado el código de los potenciadores y ahora la **Velocidad** y la **Invisibilidad** se implementan como potenciadores en lugar de buffs.
* Las cajas de impacto de las armas y la munición son más altas para que no saltes directamente sobre ellas sin adquirirlas.


### Apariencia de los modelos


Todos los modelos de jugador tienen una visibilidad mejorada por defecto, y hay varias correcciones:


* Equilibrado de los colores primario (**camisa**) y secundario (**brillo**) en algunos modelos, especialmente en **Erebus** y modelos derivados.
* Se ha corregido el color de resplandor quemado reduciendo el número de colores disponibles de **15** a **9**.
* Un efecto secundario de esto es que el color del resplandor del equipo azul es **azul-cían** (color original) en lugar de **cían**.
* Los demás colores del equipo no han cambiado.
* Se ha corregido la confusión de colores primarios y secundarios en algunos modelos.


### Mapas


* Se han añadido 2 nuevos mapas: **Bromine** y **Opium.**
* Se han corregido y actualizado visualmente varios mapas, como zonas mejor iluminadas, nuevas texturas, etc.
* Los mapas **Droin** y **Oil Rig** han sido ocultados en el menú y serán borrados en el próximo lanzamiento.


### Otros cambios


* Las armas **Crylink** y **Electro** tienen un nuevo diseño
* 4 nuevos monstruos: **Wyvern**, **Golem**, **Mage** y **Spider**.
* Nuevos diseños en la interfaz del juego, como el HUD y la tabla de puntuaciones.
* Se han añadido nuevas traducciones: **Japones**, **Portugués** (Brasil) y **Turco**.


La lista completa de cambios lo tenéis en [esta entrada](https://xonotic.org/posts/2022/xonotic-0-8-5-release/) de su blog.


Si os gusta este tipo de juegos, y este aun no lo habéis probado, os recomiendo que lo probéis. Y si quieres echar unas partidas con nosotros, pásate por el canal de Telegram o la sala A jugar en Matrix y Discord ;)

