---
author: Jugando en Linux
category: Editorial
date: 2018-12-24 16:18:00
excerpt: "<p>En estas fechas tan se\xF1aladas...</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d72b92e7a132ff40fb2049dc48920977.webp
joomla_id: 937
joomla_url: jugando-en-linux-os-desea-a-todos-feliz-navidad-y-muchos-juegos-en-linux
layout: post
title: Jugando en Linux os desea a todos Feliz Navidad y muchos juegos en Linux
---
En estas fechas tan señaladas...

Parece que fue ayer cuando **jugandoenlinux.com** echó a andar, pero este es el segundo año que celebramos la Navidad con todos vosotros. Sabemos que durante todo este tiempo hemos tenido momentos excelentes, y otros manifiestamente mejorables, sabemos también que a veces se nos quedan muchos artículos e información por publicar, pero los que hacemos "Jugando en Linux" tenemos la firme intención de seguir informando de todo lo que suceda en el mundo del videojuego en Linux, seguir escribiendo artículos, análisis y tutoriales y seguir jugando con todos vosotros.


Trataremos de mejorar todo lo posible y hacemos firme propósito de dar lo mejor de nosotros para que sigáis teniendo la mejor y mas actual información respecto a nuestro sistema de juego favorito.


Sin más, **¡Feliz Navidad y felices fiestas a todos!**


El equipo de jugandoenlinux.com


**Leo, Serjor y Pato**


PD: Pasa y felicítanos la Navidad en los comentarios o en nuestros canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD). ¡Os esperamos!

