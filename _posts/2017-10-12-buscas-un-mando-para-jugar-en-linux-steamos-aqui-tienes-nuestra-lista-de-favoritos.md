---
author: Pato
category: Hardware
date: 2017-10-12 15:00:00
excerpt: "<p>Aprovechando el magn\xEDfico art\xEDculo de Yodefuensa, yo tambi\xE9\
  n le doy un repaso a los que considero los mejores mandos para jugar en nuestro\
  \ sistema</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fd2436c23111d947c615d80fa5115911.webp
joomla_id: 385
joomla_url: buscas-un-mando-para-jugar-en-linux-steamos-aqui-tienes-nuestra-lista-de-favoritos
layout: post
tags:
- hardware
- mandos
title: "\xBFBuscas un mando para jugar en Linux/SteamOS? Aqu\xED tienes mi lista de\
  \ favoritos"
---
Aprovechando el magnífico artículo de Yodefuensa, yo también le doy un repaso a los que considero los mejores mandos para jugar en nuestro sistema

En los tiempos que corren jugar con teclado y ratón sigue siendo una posibilidad, e incluso para ciertos géneros como los FPS es lo más recomendado dada su versatilidad, agilidad y precisión a la hora de moverse y apuntar. Sin embargo, desde hace ya tiempo que si quieres jugar en condiciones y sobre todo ciertos títulos y géneros es necesario contar con un buen mando de juegos, a no ser que quieras dejarte los dedos en el teclado. Ya si utilizas SteamOS, un sistema "pensado" para jugar como si de una consola se tratase un mando es poco menos que imprescindible. Ahora la pregunta es: ¿Qué mando es el más recomendado para jugar en Linux/SteamOS?


Pues me he puesto manos a la obra. Durante mi vida videojueguil he probado unas cuantas opciones (bastantes mas de las que expongo aquí desde luego) y he hecho mi particular "ranking" de mandos para jugar en nuestro sistema, y te lo presento de "más favorito" a "menos favorito", dejando claro de antemano que esta es "mi lista", que cada uno tiene sus gustos y que cualquiera de las opciones que expongo cumplirán su función de jugar en Linux con garantías. Comenzamos por:


#### **1- Steam Controller**


![steamcontroller](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Gamepads/SteamController.webp)


 A estas alturas creo que sobran las presentaciones. Cuando Valve perfiló sus "Steam Machines" y quiso darle un empujón al modo Big Picture de Steam tuvo claro que necesitaba diseñar un mando cuya versatilidad pudiera competir en prestaciones con un teclado y un ratón. Para ello diseñaron el Steam Controller, un mando configurable hasta niveles enfermizos y totalmente compatible con el estandar "de facto" como es el de Xbox. Más aún, liberó las especificaciones y su API para que todo el que quisiera pudiera programar para el, y de hecho ya hay algún que otro controlador para programarlo y utilizarlo fuera de Steam.


Su peculiar diseño le permite tener, además de un stick analógico dos trackpads hápticos que incluyen además sendos pulsadores, y con los que se puede conseguir un nivel de precisión similar al de un ratón, a la vez que se puede recibir "información" sensorial a través de las vibraciones. El mando se completa con seis botones frontales, dos superiores, dos gatillos progresivos con pulsador y dos accionadores en la parte inferior. Gracias a estas características, para mi es con mucho el mejor mando con el que se puede jugar en PC. Es cierto que al principio cuesta un poco "cogerle el tacto", pero una vez te has acostumbrado ya no hay vuelta atrás. El único pero que se le puede poner es que necesita pilas para funcionar, cosa que para algunos es además una ventaja. En el apartado precio, solo se puede conseguir en Steam y el precio con portes incluidos sale algo caro, pero por suerte Steam hace rebajas de hardware de vez en cuando por lo que si tienes paciencia no es difícil comprarlo por una parte del precio que tiene normalmente.


#### **2- Xbox360**


![Xbox360Gamepad](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Gamepads/Xbox360Gamepad.webp) 


 ¿Qué decir del estandar de facto? El mando que Microsoft se sacó de la chistera fue magistral. Tanto es así que en menos de un año ya era "**el mando**" que había que tener si querías jugar en condiciones en PC. La API de este mando (Ximput) es el estandar que se utiliza hoy día para los juegos de PC aparte de la consola de Microsoft, y su distribución y agarre han sido copiados por otros fabricantes hasta la saciedad, muestra de que en confort y ergonomía pocos peros se le pueden sacar.


El mando consta de dos sticks analógicos con pulsador, una cruceta, seis botones frontales, dos superiores y dos gatillos progresivos con pulsador. No es difícil conseguirlo por 25 o 30 euros, o incluso menos si lo encuentras seminuevo.


#### **3- Logitech F710**


![Logitechf710](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Gamepads/Logitechf710.webp)


 Logitech entró con fuerza en el mundillo del hardware para PC con este mando. El F710 fue en su momento un soplo de aire fresco dentro de un apartado dominado hasta entonces por Microsoft y su mando de Xbox. Las características del mando incluyen dos modos de funcionamiento, uno X-Imput compatible con Xbox y otro Direct Imput que hacían de este mando un periférico a tener en cuenta sobre todo si tenías juegos antíguos que utilizasen esta API. Además de esto la distribución de botones es la clásica de Xbox salvo los dos sticks analógicos situados simétricamente en la parte inferior, cosa que "obliga" a coger el mando de una forma algo diferente a como lo haríamos con el mando estandar de Microsoft. El mando se compone de seis botones frontales, dos de modo (uno de ellos para activar o desactivar la vibración), dos superiores, dos gatillos progresivos con pulsador, aparte de una cruceta y los dos sticks analógicos con pulsador.


La superficie de agarre en goma le da un "extra" de sujeción, pero con el tiempo y el desgaste típico puede convertirse en algo "pegajoso". Con todo y con esto, en conjunto es un muy buen mando, además inalámbrico (funciona con pilas) y que incluso llegó a ser el elegido por Steam como mando promocionado antes de la llegada del Steam Controller. Es fácil encontrarlo en torno a 30 euros.


#### **4- Krom Kenshu**


![KromKenshu](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Gamepads/KromKenshu.webp) Nox es prácticamente un recién llegado al mundillo de los mandos, pero desde mi punto de vista ha hecho una buena apuesta. El Kenshu es un mando compatible con el estandar Ximput en PC además de con PS3, y cuya distribución es similar al mando de Xbox. Lo peculiar del mando es su funcionamiento inalámbrico (2,4GHz) y la utilización de baterías en vez de pilas. También posee vibración y su particular tamaño, algo más pequeño que la mayoría lo hacen recomendable para aquellos con manos pequeñas. Por otra parte, los que tienen dedos y manos grandes puede que lo encuentren algo menos cómodo.


El mando tiene dos superficies laterales rugosas que ayudan al agarre y le dan una sensación de menos "plástico". El conjunto se compone de dos sticks analógicos, una cruceta, seis botones frontales, uno de modo turbo, dos botones superiores y dos gatillos progresivos con pulsador.


Es fácil encontrarlo en un rango de precios de entre 20 y 25 euros, por lo que es ideal para aquellos con bolsillos más ajustados. En cuanto a Linux, el mando es detectado en la mayoría de distribuciones como mando de Xbox 360 por lo que no suele dar problemas.


Y hasta aquí mi propuesta sobre mandos para Linux.


¿Qué te parece mi lista? ¿Añadirías algún otro? ¿Cambiarías el orden de favoritos? ¿Cual es tu favorito?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

