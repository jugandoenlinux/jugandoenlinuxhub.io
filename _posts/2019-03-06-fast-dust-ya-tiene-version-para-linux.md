---
author: leillo1975
category: Carreras
date: 2019-03-06 19:47:50
excerpt: "<p>Disfruta de las carreras m\xE1s salvajes</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fd276238890ff4fd4fd30b85b5b67400.webp
joomla_id: 987
joomla_url: fast-dust-ya-tiene-version-para-linux
layout: post
tags:
- carreras
- demo
- postapocaliptico
- fast-dust
- binary-giants
title: "Fast Dust ya tiene versi\xF3n para Linux."
---
Disfruta de las carreras más salvajes

En el día de hoy, los polacos [Binary Giants](http://www.binarygiants.com/#) han lanzado finalmente la [versión para Linux](https://steamcommunity.com/games/891330/announcements/detail/1806412440488338515) (y MacOS) de su juego [Fast Dust](http://www.fastdust.com/). A principios de Agosto del pasado año, Liam de GamingOnLinux preguntaba en los [foros de Steam](https://steamcommunity.com/app/891330/discussions/0/1739964683501885268/?tscn=1533197145), a lo que rapidamente sus desarrolladores contestaron que efectivamente tendría soporte Linux en dos o tres meses. Con bastante retraso, a día de hoy ya tenemos una versión para nuestro sistema de este juego elaborado con Unity 3D.


El juego que te permitirá conducir coches de aspecto postapocalíptico por multitud de pistas,  [se puede comprar en Steam](https://store.steampowered.com/app/891330/Fast_Dust/) por el fantástico precio de **16.79€,** aunque también podeis probarlo antes ya que tiene disponible una **demo**. Os dejamos la **descripción oficial**:


*Fast Dust es un juego de carreras excepcional en el que el jugador tiene a su disposición 10 coches únicos, proyectados especialmente para este título. Estos coches no son nuevos ni brillantes. Son vehículos con carácter, fuertemente golpeados con una estructura especialmente reforzada, que resistieron muchas colisiones y aún tienen potencia bajo el capó.*  
*Hay 70 rutas para recorrer, con un total de 430 km (270 millas) en un área variada. Como piloto de estas máquinas increíbles, tendrás la oportunidad de probarte en las carreteras del bosque, en el cañón de arena y en la antigua ciudad industrial. El tiempo en el camino no siempre será bueno, prepárate para la lluvia, nieve, niebla espesa y conducción nocturna. En algunas rutas, descubrirás atajos que te permitirán ganar ventaja sobre tus oponentes y vencer más de una carrera.*  
  
*MODO CARRERA*  
*Como un nuevo conductor, comienzas a construir tu posición en el ranking de los 155 mejores pilotos. Antes de cada carrera, tu amigo Harry, que está guiando tu carrera, te envía información sobre el número de adversarios, condiciones climáticas y posibles atajos en la ruta. Al ganar la carrera, no solo ganas una posición más alta en el ranking, sino también dinero (virtual), con el que pagas la tasa de entrada que debe pagarse antes de cada carrera.*  
  
*Una fuente adicional de dinero son las apuestas en las que puedes participar antes de la carrera. El premio en la apuesta es dinero o un coche. Debes tener en cuenta que también puedes perder la apuesta. Para tener un coche adicional en el garaje, tienes que ganarlo en la apuesta, entonces el riesgo a veces vale la pena.*  
*Ganando la carrera, desbloqueas la compra de varias mejorías, tales como:*


* *tuneo de velocidad*
* *tuneo de aceleración*
* *turbo*
* *mapas con atajos*


*Hay 70 rutas para recorrer (430km / 270millas), en 3 diferentes ubicaciones:*


* *ciudad antigua fábrica*
* *bosque*
* *cañón*


*Diferentes condiciones climáticas:*


* *lluvia*
* *neblina*
* *nieve*
* *día*
* *noche*


*Comienzas de la última posición y hay 154 conductores delante de ti, a los que tienes que vencer.*  
*Hay cuatro tipos de carrera:*  
*1) estándar - una carrera con varios pilotos, el objetivo es simple: llegar primero;*  
*2) eliminación - una carrera con varios pilotos, hay checkpoints en la ruta, el que alcanza el checkpoint como penúltimo, elimina de la carrera el competidor en la última posición, para ganar tienes que ser primero en el último checkpoint;*  
*3) desafío del checkpoint - vas solo, hay checkpoints en la ruta que tienes que alcanzar en un tiempo determinado, vencer te permite continuar la competición;*  
*4) cuenta regresiva - vas solo y tienes que llegar a la meta en un tiempo determinado, la victoria es la prueba de que tus habilidades son suficientes para competir con mejores pilotos;*  
  
*MODO DE CARRERA RÁPIDA*  
*En este modo puedes elegir cualquier coche del garaje, cualquier ruta y el modo de carrera preferido (estándar, eliminación, checkpoint, cuenta).*  
  
*MODO MULTIPLAYER*  
*En este modo puedes correr con tus amigos (sesión privada) o con los jugadores actualmente disponibles en línea. En total, 7 personas como máximo pueden participar en la carrera.*


En cuanto a los requisitos técnicos para jugar a Fast Dust necesitarás:


* **SO:** Ubuntu 16.04 o posterior, SteamOS 2.0
* **Procesador:** Intel® Core™2 Quad Q9550 o AMD equivalente
* **Memoria:** 8 GB de RAM
* **Gráficos:** Geforce GTX 460
* **Red:** Conexión de banda ancha a Internet
* **Almacenamiento:** 8 GB de espacio disponible


Os dejamos como siempre con un video, en este caso el trailer del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/5clWiBkX3F4" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


**NOTA:** La demo tiene el conocido bug de los juegos desarrollados con Unity3D cuando se ejecutan a pantalla completa. Para corregirlo basta con añadir a los Parámetros de Lanzamiento "-screen-fullscreen 0" en las propiedades del juego. También os informamos que pronto realizaremos un Streaming enseñándoos las características del juego completo gracias a la copia facilitada por Binary Giants.

