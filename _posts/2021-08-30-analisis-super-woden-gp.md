---
author: leillo1975
category: "An\xE1lisis"
date: 2021-08-30 08:31:26
excerpt: "<p>Revisamos en profundidad el t\xEDtulo de @ViJuDaGD</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP/analisis/SWGP_Logo_portb.webp
joomla_id: 1350
joomla_url: analisis-super-woden-gp
layout: post
tags:
- carreras
- indie
- analisis
- retro
- vijuda
- super-woden-gp
title: "AN\xC1LISIS: Super Woden GP"
---
Revisamos en profundidad el título de @ViJuDaGD


  
Normalmente involucrarte en un proyecto te hace ser más parcial y tener opiniones más subjetivas sobre él, pero también es cierto que te permite conocerlo mejor y ver algunas de las complejidades que encierra el proceso de creación de un videojuego. En el caso del título que hoy nos ocupa, creo que es justo informaros que **en JugandoEnLinux hemos estado haciendo el testeo de la versión nativa de Linux** desde finales del año pasado, lo cual nos hace sentirnos participes en el desarrollo, pero también nos da cierta responsabilidad sobre la calidad del producto final. Lo que si os queremos asegurar es que haremos este análisis  todo lo más objetivo que podamos para que, vosotros lectores, os hagais una opinión lo mejor formada posible sobre este videojuego.


 


Antes de empezar a analizar del juego en si, siempre nos gusta hablar un poco sobre los desarrolladores, especialmente en el caso de los independientes, pues normalmente son más desconocidos para la mayor parte del público. En esta ocasión el título ha sido creado por [ViJuDa](https://twitter.com/ViJuDaGD), un **desarrollador autodidacta** afincado en Vigo, que comenzó hace unos tres años creando algunas cosas para móviles, pero que se estrenó en el mundo del PC con el primer [Woden GP]({{ "/posts/woden-gp-un-arcade-de-carreras-retro-tendra-version-linux-a1" | absolute_url }}), título que sería el germen del que hoy se lanza al mercado. **Victor Justo Dacruz**, que asi se llama la persona detrás de las siglas, es además músico, lo cual también deja su impronta en el juego, teniendo la autoría de algunas de las melodías que escucharemos. Es también un gran aficionado al mundo del automovilismo y los coches clásicos, algo que por supuesto ha influido en la creación del juego. Hechas las presentaciones comenzamos a hablar largo y tendido sobre Super Woden GP.


![SWGP Carrera2b](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP/analisis/SWGP_Carrera2b.webp)


 


Aventurándonos en nuestra opinión personal y sin saber concretamente las razones del desarrollador para crearlo, para nosotros SWGP no es una mera continuación ni una expansión del título anterior, sinó, a nuestro jucio, todo aquello que quedó en tintero en su predecesor; conviertiendolo en **un juego más ambicioso,  evolucionado, y mucho mejor en todos los aspectos**. Con todos los respetos a su creador, se podría decir, que Woden GP fué (aunque no por ello deja de ser un juego muy válido y divertido) un entrenamiento para desarrollar la secuela, pero teniendo todo mucho más estructurando, de forma que permitiese al juego crecer, y por supuesto facilitando que fuese más sencillo el portarlo a otros sistemas. Gracias a esta claridad de ideas, el juego es lo que es actualmente, y podemos jugarlo en Linux.


 


Yendo al grano, y para quien no sepa aun de que va **Super Woden GP, es un juego 3D de coches de carreras con perspectiva isométrica y jugabilidad retro**, donde nada más empezar nos encontraremos de bruces con un **mapa al más puro estilo de los primeros Gran Turismo,** juego del que vereis más adelante que tomará más referencias; y en el que podremos movernos de forma sencilla entre los diferentes elementos que lo conforman. Entre estos elementos, el central y fundamental sería **nuestro Garaje**, donde **iremos acumulando los automóviles que a lo largo de nuestra trayectoria vayamos comprando o consiguiendo**.... Y es en este aspecto de acumular coches donde encontramos uno de los grandes alicientes para jugar y rejugar a este título. El puro coleccionismo de tener todos los modelos nos llevará a querer disputar todas las competiciones para conseguir créditos para comprar el bólido más potente, que luego nos servirá para superar las siguientes pruebas, así como conseguir modelos exclusivos al superar ciertas competiciones. Continuando en nuestro garaje, **en él podremos encontrar nuestros vehículos en propiedad, además de sus características principales** como los PP (Performance Points), la clase, el caballaje, kilometraje, así como sus gráficas de Potencia, Agarre y Manejo.


 


Volviendo al menú-mapa principal, alrededor de nuestro garaje tendremos los **concesionarios donde comprar nuestros más preciados bienes**. Se dividen en **6 marcas que se corresponden con las diferentes nacionalidades** de los modelos. En cada uno de ellos encontraremos **coches clásicos con nombres ficticios, pero altamente reconocibles al estar basados en modelos de marcas reales**. Por poner un ejemplo, en **AALIA**, el concesionario Italiano, encontraremos "homenajes" a coches de FIAT, Ferrari, Lancia, Lamborghini... Los otros concesarios serían **NANWOLF** para los vehículos Británicos, **MYLENE** para los franceses, **RAVEN** para los japoneses, **SOOP** para los Americanos, y por supuesto **CINDER** para los Alemanes. Quizás podrímos pedir "algo más" e incluir un concesionario "multimarca" donde encontrar coches icónicos de otras nacionalidades que no se ajusten a los existentes, o rarezas, pero eso ya sería rizar el rizo... 


![SWGP Mapa](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP/analisis/SWGP_Mapa.webp)


 


Siguiendo con los elementos del menú tedremos un **Taller de Pintura** que nos permitirá **cambiar la apariencia de nuestros coches**, siempre que el modelo lo permita. Hay que tener en cuenta que ciertos automóviles muy icónicos solo tienen un "skin", y no es posible cambiarlo, pues estos van indiscutiblemente asociados a esa estética. También nos será posible "sacarles brillo", ya que con el tiempo irán acumulando "roña", en el **Autolavado**, con un **minijuego muy sencillito que consistirá en pulsar repetidamente el botón A para quitar la suciedad,** al más puro estilo [Track and Field](https://es.wikipedia.org/wiki/Track_%26_Field). En la **Sala de Música** encontraremos todas las melodías que sonarán en el juego, con una divertida animación de una cinta de casete que nos recordará a muchos nuestro pasado ¿reciente?. Una de las últimas adiciones durante el desarrollo del juego ha sido la sección de **Estadísticas**, en donde podremos ver montones de datos sobre los circuitos y clases de coches, así como los mejores tiempos de todos los jugadores "arround the world". Por supuesto tenemos un menú de **Opciones** donde podremos configurar el juego a nuestro gusto (Idioma, Graficos, Sonido....)


 


Pero vamos ya a adentrarnos en las carreras, que a fin de cuentas es a lo que hemos venido, y aquí es justo empezar por **Competiciones**, pues el apartado donde pasaremos la mayor parte del tiempo. Antes de continuar conviene aclarar como está estructurado **el sistema de desbloqueo de las pruebas, que se basa en el número de estrellas que vamos acumulando a lo largo de nuestra trayectoria**. Cuanto mejor sea nuestra posición en carrera, obviamente más estrellas conseguiremos, permitiéndonos ir accediendo a otras más complejas y exigentes. También conseguiremos **premios en metálico basados en la posición que obtengamos**, los cuales nos servirán para adquirir otros modelos de coches que nos facilitarán la tarea de ganar en competiciones posteriores. Otro elemento que coartará el acceso a ciertas pruebas será el modelo, pues **algunas exigirán uno o varios modelos determinados para poder acceder a ellas**. Otras por ejemplo solo estarán accesibles tras haber ganado un coche exclusivo en una competición concreta. Como veis, todo va encaminado a que adquiramos más y más coches... Hay que recalcar que las pruebas están todas limitadas por un numero concreto de puntos de rendimiento, de forma que no podamos tener una ventaja injusta frente al resto de competidores de la IA.


![SWGP Campeonatos](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP/analisis/SWGP_Campeonatos.webp)


 


La sección de competiciones está divididida en cuatro tipos diferentes, los **Fines de Semana**, donde tendremos que competir en uno o dos circuitos en cada prueba, siendo estas independientes unas de otras. En el caso de los **Campeonatos SWGP**, siempre serán 2 o más circuitos en los que compitamos para ganar puntos y estar en lo más alto de la clasificación. Además necesitaremos ganar el campeonato precedente para desbloquear el siguiente y así llegar a la cima del automovilismo con la consecución del Mundial. Otro tipo de competiciones serán las de **Resistencia**, que nos obligarán a disputar interminables sesiones de muchas vueltas, algo que modificará drasticamente la forma de afrontarlas, pues **tendremos que dosificar nuestros esfuerzos para no acabar con la integridad de nuestro vehículo**.


 


Por último, el destacable modo **Rally**, que cambiará completamente la dinámica de las pruebas, donde encontraremos solos ante el tramo, siendo necesario superar determinados tiempos para conseguir las mejores posiciones. Para estas pruebas **estaremos limitados a usar exclusivamente coches de Rally**, cobrando más que nunca especial importancia la memorización del trazado, así como la concentración para obtener el mejor tiempo. Hay que destacar la **inclusión de las indicaciones de las curvas, así como un fantástico trabajo con las voces de los copilotos.** Hay que resaltar que las carreras no serán planas, pues además de los diferentes modificadores que introducen el tipo de rivales o el circuito, tenemos que tener en cuenta que **podremos disputar pruebas y carreras tanto de noche como de día, con lluvía, tormentas... e incluso con meteorología cambiante**, algo que creedme, es sorprendente lo bien resuelto que está, produciéndose estas variaciones de forma progresiva y natural, y no de golpe, como vemos incluso en otros juegos con más renombre.


![SWGPDemo](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP/analisis/SWGP_RALLY.webp)


 


Además del modo Competiciones, está el de **Carrera Libre**, donde será posible la **personalización hasta el extremo de las pruebas**, pudiendo modificar además del circuito o el número de vueltas, valores tales como la hora, la meteorología, la transición día/noche, o el numero o dificultad de los rivales. Pero es aquí donde encontraremos una posibilidad que **nos permitirá disputar carreras con otros jugadores**, y es que el juego, a pesar de no tener modo multijugador online, **permite las partidas a pantalla partida hasta con cuatro amigos de forma local**. Esto nos brinda una posibilidad muy interesante que permite la tecnología [Remote Play Together](index.php/homepage/noticias-news/1118-steam-anadira-una-nueva-caracteristica-para-jugar-juegos-con-multijugador-local-llamada-remote-play-together) de Steam, pues podremos picarnos con nuestros colegas a través de internet gracias a ello, como si estuviesen a nuestro lado en el sofá o en la silla de nuestro escritorio. Por último, y no menos importante, **competiremos con otros jugadores del mundo** en **Eventos de Temporada**, donde cada cierto tiempo se nos presentará una prueba con un coche en un circuito o tramo deteminados, y tendremos que hacer nuestro mejor crono para intentar dejarlo estampado en lo más alto de la lista. Con respecto a esto último, durante todas las sesiones de juego que hagamos, sea cual sea el tipo de competición que disputemos, **nuestros tiempos quedarán registrados y serán comparados con los de otros jugadores**, lo cual provocará el "pique" constante para ser el mejor, algo que favorece completamente la rejugabilidad de título.


 


Hay muchas cosas destacables en SWGP, pero si una es reseñable es la **jugabilidad**. Como podeis ver en las instantaneas, el juego recuerda inmediatamente a títulos de antaño como [World Rally Championship](https://videojuegos.fandom.com/es/wiki/World_Rally_Championship_(Gaelco)) o [Neo Drift Out](https://en.wikipedia.org/wiki/Neo_Drift_Out:_New_Technology), y es esa forma de jugar de los arcades de los 90, la que consigue introducir y mejorar ViJuDa en su proyecto. En aquella época, debido a las características técnicas, quizás las animaciones eran más rudas, y todo parecía ir sobre railes, siendo las pistas más estrechas y permitiendo menos espacio a la destreza del jugador; por decirlo de alguna manera, era todo más mecánico. Ahora la acción transcurre de una forma mucho más suave, haciendo crucial el saber trazar las curvas siguiendo la trayectoria adecuada, el tomar puntos de frenado, el conocerse al dedillo los circuitos y los coches para tomar ventajas en la pista... **Los controles responden perfectamente a lo que queremos hacer y conjuntamente con las físicas de los vehículos forman un tandem excepcional**. Es una auténtica virguería ver como al tomar una curva cerrada el vehículo derrapa; y **produce verdadera adicción entrar a alta velocidad, ver como sigue la trayectoria adecuada y pasa con el morro rozando el vertice para salir a toda velocidad haciéndote sentir el dueño de la pista**. Algo a destacar es que **se pueden "sentir" las caracteristicas de los vehículos, conduciéndose cada uno de ellos de forma diferente**, y no siendo simplemente el mismo coche con otras apariencias, algo achacable incluso a algunos juegos afamados.


![SWGP Multib](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP/analisis/SWGP_Multib.webp) 


En cuanto al **funcionamiento de la IA**, su conducción es básica y predecible, aunque **en un título de estas características es lo que esperamos y queremos encontrar**. Comenzando siempre desde el fondo de la parrilla, tendremos que ir sobrepasando a nuestros competidores hasta alcanzar la primera posición, siendo los primeros sencillos de adelantar, pero complicándose a medida que nos acercamos a la cabeza. Sobre la **dificultad**, está muy **bien equilibrada a lo largo del juego,** existiendo la opción de un **modo fácil** si vemos que se nos complica la cosa. En este aspecto hay que comentar que los más viejos del lugar quizás juguemos con ventaja al estar acostumbrados a este tipo de perspectiva y manejo, común en los juegos de antaño, pero pensamos que no supondrá un problema para casi nadie con un poco de práctica.


 


**A nivel gráfico** nos encontramos ante un título que siendo **realizado en 3D**, **consigue,** como dijimos antes, **emular la estética 2D** de finales del siglo pasado, pero obviamente con una mayor calidad y detalle acorde con los tiempos que corren. El juego presenta en sus **más de 20 variadas pistas**, unos escenarios coloridos repletos de detalles y bastante bien resueltos. Pero **es en el modelado de los coches donde el juego destaca**, pues a pesar de no tener un resolución elevada, el desarrollador consigue hacerlos facilmente reconocibles y lucir de maravilla en la pista. Habiendo **más de 80 modelos distintos**, encontraremos los más variados vehículos de diferentes épocas, pudiendo conducir desde los más pequeños utilitarios, hasta los coches de carreras más rápidos, pasando por espectaculares deportivos y alguna que otra sorpresa.


![SWGP Concesionario](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP/analisis/SWGP_Concesionario.webp)


 


**Gracias al uso de filtros tendremos la posibilidad de cambiar la apariencia** y emular el aspecto que tenían en las recreativas, poner scanlines o apariencia de dibujo a lapiz; lo que nos permitirá recordar aun más ese pasado en el que tanto se inspira este título.  **El juego tiene además la posibilidad de mover la cámara, pudiendo tanto alejarla como acercarla, así como regular su altura.** En cuanto a la información en pantalla, esta se muestra de forma clara y no se advierte que falte ni sobre nada, estando **los elementos del HUD correctamente colocados sin estorbar en ningún momento**. Por último, **en esta versión de Linux**, hay decir que no hemos encontrado nada reseñable, al menos en nuestra configuración de prueba (i7-3770, 16GB de RAM, Nvidia GTX-1060), **funcionando todo sin problemas y con un buen rendimiento** en todo momento. Suponemos que en configuraciones más bajas, dada la naturaleza de SWGP, no habrá grandes problemas, y más teniendo la posibilidad de ajustar las opciones gráficas.


 


En el **apartado sonoro**, todos los efectos que encontramos en el juego cumplen su función tal y como se espera de ellos, no encontrando ninguno fuera de lugar. Hay que destacar que **el sonido de los motores de los coches es diferente según el vehículo que conduzcamos**, facilitando aun más, si cabe, la inmersión. Es reseñable el esfuerzo que se ha hecho para la **introducción de las voces de copiloto en los diversos lenguajes a los que está traducido el juego**, y que acompañando a los textos, llega a 16 localizaciones distintas, incluyendo los otros idiomas cooficiales del estado español, como el Gallego, Catalán, Euskera y Valenciano.


 


No nos olvidamos por supuesto de una parte muy importante que nos acompaña todo el rato, como es su **banda sonora** compuesta por **26 piezas diferentes** que encajan como anillo al dedo con el juego. **Los temas tienen sonido e inspiración retro**, trasladándonos inmediatamente a los años 90 a aquellos juegos de conducción de entonces, y todo ello sin hacerse monótonas ni aburridas. Hay que resaltar la autoría en su mayor parte del **compositor y dibujante** [Franikku](https://twitter.com/franikku), que además es también autor de el **video de introducción del juego**, las **imagenes de los títulos de crédito**, y algunos de los skins de los coches.


![SWGP Carrerab](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP/analisis/SWGP_Carrerab.webp)


 


Realmente nos cuesta encontrar aspectos negativos en el juego, pues obviamente todo depende con que se compare o si tenemos en cuenta las circunstancias y objetivos de cada proyecto. Estando ante un título inspirado en el pasado y pretendidamente retro, y siendo además desarrollado por una sola persona (sin olvidarnos de la colaboración de Franikku), creemos que el resultado es sobresaliente, y más si tenemos en cuenta la relación calidad/precio. Quizás **podríamos decir que le vendría bien un modo online**, algo tan demandado en estos tiempos, pero **se entiende que ese es un añadido muy complejo de implementar en un equipo de desarrollo tan pequeño**. En este aspecto hay que decir que la ayuda indirecta que facilita el cliente Steam con su [tecnología Remote Play Together](https://store.steampowered.com/remoteplay?l=spanish) aplaca el el ansia que puede tener el jugador de SWGP de competir contra otros. 


 


En definitiva, Super Woden GP es un **videojuego indie hecho con cariño y pasión hacia el automovilismo**, y eso exuda por cada poro del juego. Creo que sobra decir que desde JugandoEnLinux lo recomendamos sin ningún lugar a dudas, no solo a los fans de los de coches y lo retro, sinó a cualquier jugador que desee sumergirse en un título que le robará horas y horas, y las convertirá en pura diversión. No podemos despedirnos sin mencionar su **ajustadísimo precio** que lo convierte en una compra obligada, ya que podeis encontrarlo en [Steam](https://store.steampowered.com/app/1534180/Super_Woden_GP/) por tan **solo 10.95€ a precio normal**, pudiendose adquirir durante esta **primera semana con un descuento **adicional** del 40%.** Sinceramente, creemos que muy pocos juegos ofrecen tanto contenido por tan poco:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1534180/" width="646"></iframe></div>


 


Quizás este análisis te haya parecido largo (eso como mínimo), pero no queríamos dejar ninguna de sus muchos detalles y características en el tintero. Aún así esperamos que te haya gustado y te haya servido para hacerte una opinión sobre el juego. Todo esto que os hemos contado sobre este fantástico Super Woden GP podeis verlo más o menos resumido en este **gameplay** que hemos grabado para todos vosotros. Esperamos que os guste:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/v-Zwd82IBVQ" title="YouTube video player" width="780"></iframe></div>


 


 ¿Os ha impresionado Super Woden GP? ¿Os gustan los juegos de coches retro? ¿Qué os parece el trabajo que ha hecho el desarrollador? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux) o nuestras salas de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

