---
author: P_Vader
category: "Acci\xF3n"
date: 2021-12-15 18:52:28
excerpt: "<p>Lo har\xE1 usando una vez mas DXVK Native en Linux y ser\xE1 experimental.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/csgo/csgo_front.webp
joomla_id: 1396
joomla_url: csgo-anade-soporte-para-vulkan
layout: post
tags:
- vulkan
- csgo
- dxvk-native
title: "CSGO a\xF1ade soporte para Vulkan"
---
Lo hará usando una vez mas DXVK Native en Linux y será experimental.


**Ya estaban tardando en [anunciar](https://blog.counter-strike.net/index.php/2021/12/36627/) que CSGO, el juego estrella de Valve, empezase a utilizar Vulkan** como ya habían hecho con Portal 2, [Half Life 2]({{ "/posts/half-life-2-se-actualiza-a-vulkan-dxkv" | absolute_url }}) o [L4D2]({{ "/posts/l4d2-se-actualiza-con-sorpote-para-vulkan" | absolute_url }}). Una vez mas **van a hacer uso de DXVK Native, esa capa añadida al motor que directamente va a traducir desde DirectX a Vulkan en lugar de usar OpenGL**, como hace ahora mismo.


**El uso de Vulkan llega de forma experimental exclusivamente para Linux**. Para activarlo solo tienes que introducir abrir las propiedades del juego en steam y añadir *"-vulkan"* en los parámetros de lanzamiento:


![csgo vulkan](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/csgo/csgo_vulkan.webp)


Al ser experimental puede que te encuentres algún problema. **Yo lo he estado probando y en general va muy fluido pero en ciertos momentos (supongo que cargando shaders) da unos tirones considerables**. Supongo que en breve Valve irá cacheando los shaders y el rendimiento se verá incrementado conforme los vayamos descargando.


Tampoco he notado el fallo que reportó el usuario *qwerty qazws* en Telegram y que lo hacia injugable, tanto con OpenGL como con Vulkan. Supongo que habrá sido solucionado.


Me imagino que todo estas novedades en el uso de Vulkan en los juegos de Valve, viene establecido en el mapa de ruta de cosas que tenían que estar funcionando para cuando se lance la Steam Deck.


¿Te atreves a jugar competitivo en modo Vulkan? Yo me lo estoy pensando aún. Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 


 

