---
author: Jugando en Linux
category: Apt-Get Update
date: 2022-03-04 21:26:56
excerpt: ''
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Valve/Djob_core_thumbsup.webp
joomla_id: 1443
joomla_url: nuevos-juegos-con-soporte-nativo-04-03-2022
layout: post
title: Nuevos juegos con soporte nativo 04/03/2022
---

Vamos con una nueva ronda de juegos y novedades para esta semana. Como complemento, un par de ofertas que pensamos que pueden interesaros. Allá vamos!: 


 


### Bomb Club Deluxe


Desarrollador: Antoine Latour


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/9Z3Ovn-baZk" title="YouTube video player" width="720"></iframe></div>


[Página de la tienda](https://store.steampowered.com/app/1771440/Bomb_Club_Deluxe/)




---


###  Aperture Desk Job


DESARROLLADOR: Valve
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/mVDFJRM6F9k" title="YouTube video player" width="720"></iframe></div>


 [Página de la tienda](https://store.steampowered.com/app/1902490/Aperture_Desk_Job/)
 


---


###  Paladin's Oath


DESARROLLADOR: Fire Biscuit Games
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/JJHkLJ_ddRc" title="YouTube video player" width="720"></iframe></div>


[Página de la tienda](https://store.steampowered.com/app/1671590/Paladins_Oath/)


---


### 


###  **EN OFERTA**


 
###  Loop Hero



DESARROLLADOR: Four Quarters
 
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/7P58L0AVIEM" title="YouTube video player" width="720"></iframe></div>  
 
 [Página de la tienda](https://store.steampowered.com/app/1282730/Loop_Hero/)
 


---


### Overcooked! 2


DESARROLLADOR: Ghost Town Games Ltd., Team17
 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/qpzmirQllT0" title="YouTube video player" width="720"></iframe></div>


[Página de la tienda](https://store.steampowered.com/app/728880/Overcooked_2/)