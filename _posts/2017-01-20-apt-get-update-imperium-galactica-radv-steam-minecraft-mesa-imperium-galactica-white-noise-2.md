---
author: Pato
category: Apt-Get Update
date: 2017-01-20 18:48:53
excerpt: "<p>Volvemos a repasar la semana con todo lo que no nos di\xF3 tiempo a publicar</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f9bbdeb62248e2fc4418a6935e64cf4c.webp
joomla_id: 185
joomla_url: apt-get-update-imperium-galactica-radv-steam-minecraft-mesa-imperium-galactica-white-noise-2
layout: post
title: Apt-Get Update Imperium Galactica & Radv & Steam & Minecraft & Mesa & Imperium
  Galactica & White Noise 2...
---
Volvemos a repasar la semana con todo lo que no nos dió tiempo a publicar

Nueva semana, nuevo Apt-Get Update para tener la información al día. Comenzamos:


Vamos a intentar estructurar este nuevo Apt-Get Update para mostrar la información según los sitios.


Desde [gamingonlinux.com](https://www.gamingonlinux.com/):


- Los drivers Open Source Mesa 17-rc1han sido publicados con multitud de cambios, incluyendo Vulkan. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/mesa-17-rc1-released-its-a-massive-update-for-open-source-graphics.8946).


- Los drivers Open Source Radv para AMD ahora soportan múltiples GPUs (varias gráficas). Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/the-open-source-vulkan-driver-for-amd-radv-now-supports-using-multiple-gpus.8920).


- El Cliente de Steam ha sido actualizado y ahora soporta cambiar la ubicación de los juegos. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/the-open-source-vulkan-driver-for-amd-radv-now-supports-using-multiple-gpus.8920).


- Minecraft ahora tiene un nuevo lanzador que ya no depende de Java (El juego sin embargo sigue necesitándolo). Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/minecrafts-new-launcher-is-now-available-on-linux.8939).


- Total War: WARHAMMER tiene a Grombindal disponible para todos los jugadores vía DLC gratuito. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/total-war-warhammer-makes-grombrindal-the-white-dwarf-legendary-lord-available-to-all-players.8954).


Seguimos desde [linuxgameconsortium.com](https://linuxgameconsortium.com/):


- Imperium Galactica II ya está disponible en Linux, e Imperium Galactica I se puede jugar desde Wine. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/imperium-galactica-1-2-franchise-release-strategy-available-linux-mac-pc-46429/).


- White Noise 2 tiene disponibleun nuevo nivel y nuevas características. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/white-noise-2-horror-game-gets-new-level-alvira-shelter-linux-pc-46119/). Vídeo a continuación:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/eHIH-Kag08Y" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


- El juego de Rol 'Galaxy of Pen and Paper' de Behold Studios ha sido anunciado y llegará a Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/galaxy-pen-paper-rpg-announced-behold-studios-46401/). Vídeo a continuacón:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/-iobfJpkv7s" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


- El juego de plataformeo y aventuras en 3D 'Liquid Metal' ya ha comenzado su campaña en Kickstarter. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/liquid-metal-3d-platformer-adventure-hits-kickstarter-linux-mac-pc-46275/).


- 'Torn Tales', un juego de Rol en tiempo real para un jugador ya está disponible. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/torn-tales-single-player-real-time-rpg-releases-steam-linux-mac-pc-46244/). Vídeo a continuación:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/5IOzuQgDtrU" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


- 'The Exiled', un MMO RPG estará disponible el 23 de Febrero en acceso anticipado. 7 Días de juego gratis y no es Pay to Win. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/exiled-rpg-mmo-launch-steam-linux-mac-pc-46212/).Vídeo a continuación:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/CjJQIeKwSMA" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


- El "Beat 'Em Up" 'I am the Hero' ya está disponible en Steam. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/i-am-the-hero-beat-em-up-releases-steam-linux-mac-pc-46145/). Vídeo a continuación:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/2mTTd1uXD7Y" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Esto es todo por hoy. Desde luego este ha sido un Apt-Get Update más completo que los anteriores. ¿Qué te ha parecido? De todos modos seguro que me he dejado cosas en el tintero. 


Qué tal si me lo cuentas en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD). Te espero la semana que viene en otro Apt-Get Update.

