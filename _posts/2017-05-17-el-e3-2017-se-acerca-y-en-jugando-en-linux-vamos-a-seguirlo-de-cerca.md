---
author: Pato
category: Editorial
date: 2017-05-17 19:29:03
excerpt: "<p>Devolver Digital ofrecer\xE1 una conferencia de prensa por primera vez\
  \ en la feria de videojuegos por excelencia.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/da89514e409822180ac867ab6712269d.webp
joomla_id: 327
joomla_url: el-e3-2017-se-acerca-y-en-jugando-en-linux-vamos-a-seguirlo-de-cerca
layout: post
tags:
- proximamente
- e3-2017
title: El E3 2017 se acerca, y en Jugando en Linux vamos a seguirlo de cerca
---
Devolver Digital ofrecerá una conferencia de prensa por primera vez en la feria de videojuegos por excelencia.

Seamos sinceros: El juego en Linux es una realidad. Y por supuesto en Jugando en Linux ya somos parte de este mundillo de los videojuegos para ordenador, por tanto, ¿por qué no seguir la que en mi opinión es la mayor feria de videojuegos del Mundo?.


Pues eso mismo es lo que nos vamos a proponer, y a proponeros a vosotros, por supuesto. Además y aprovechando la coyuntura, La editora "Devolver Digital" que tantos títulos nos ha traído a Linux ha anunciado que ofrecerá una conferencia de prensa própia en el E3:



> 
> Devolver Digital will have an E3 press conference this year. Please stay tuned for a time and date. [pic.twitter.com/WoHiOaio4G](https://t.co/WoHiOaio4G)
> 
> 
> — Devolver Digital (@devolverdigital) [16 de mayo de 2017](https://twitter.com/devolverdigital/status/864601666221420545)



 Devolver Digital es la responsable de juegos como [Shadow Warrior](http://store.steampowered.com/app/233130/Shadow_Warrior/), [The Thalos Principle](http://store.steampowered.com/app/257510/The_Talos_Principle/), [Hotline Miami](http://store.steampowered.com/app/219150/Hotline_Miami/), o [Broforce](http://store.steampowered.com/app/274190/Broforce/) entre otros, por lo que no se puede decir que no apoye al juego en Linux.


El E3 2017 se llevará a cabo los próximos días 13, 14 y 15 de Junio en Los Angeles, California, pero como no vamos a poder asistir allí en vivo (pero no por nada, es que esos días estamos comprometidos con nuestros fans y no nos cuadra la agenda) nos proponemos seguir las conferencias más interesantes para nosotros, los que jugamos en Linux a través de las retransmisiones online que se harán desde allí. ¿Donde?... ya os lo iremos anunciando próximamente, por lo que tendréis que estar atentos.


De momento, os proponemos las dos conferencias a las que "asistiremos" (o lo intentaremos) y cuando serán:


- PC Gaming Show - Lunes 12 de Junio a las 19:00 h (GTM+2)


- Devolver Digital - Todavía por confirmar


¡Animaros a seguirlas con nosotros! 


¿Os interesa cualquier otra conferencia? Proponerla en los comentarios  o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD) y haremos lo posible por asistir. Podéis ver las conferencias y toda la información del E3 en su web oficial: 


<https://www.e3expo.com/>

