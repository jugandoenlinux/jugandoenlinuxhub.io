---
author: Pato
category: Apt-Get Update
date: 2017-03-12 11:15:05
excerpt: <p>Esta semana vamos con un poco de retraso... pero solo un poco</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e213534406f5e673030b12a49a117407.webp
joomla_id: 242
joomla_url: apt-get-update-2
layout: post
title: Apt-Get update Day of Infamy & World of One & Streets of Rogue & Lightspeed
  Frontier...
---
Esta semana vamos con un poco de retraso... pero solo un poco

Volvemos una semana más a repasar todo lo que nos han deparado estos últimos siete días y no hemos publicado... aunque sea tarde.


Desde [www.gamingonlinux.com:](http://www.gamingonlinux.com)


- World of One, un interesante plataformas y puzzles indie con una atmósfera de lo más oscura llegará el próximo día 21 de Abril. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/world-of-one-a-fantastic-looking-and-dark-indie-puzzle-platformer.9248).


- Day of Infamy el FPS bélico recibe nuevas opciones y características, y anuncia su lanzamiento oficial para el 23 de Marzo. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/day-of-infamy-the-wwii-fps-has-a-massive-feature-filled-update.9277).


- Liam nos descubre una utilidad llamada "SDL2 Gamepad Tool" que nos servirá para mapear nuestros controladores preferidos. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/sdl2-gamepad-tool-an-alternative-to-steam-big-picture-configurator.9269).


 


Desde [www.linuxgameconsortium.com](http://www.linuxgameconsortium.com):


- Meganoid, un plataformas con jugabilidad de la vieja escuela se puede pre-comprar y se lanzará este próximo día 16. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/meganoid-challenging-platformer-available-pre-order-release-date-49570/).


- Streets of Rogue es un shooter-RPG-Brawler que ya está disponible en acceso anticipado y pinta muy bien. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/streets-of-rogue-rogue-lite-co-op-stealth-shooter-rpg-brawler-launches-early-access-49407/).


. Lightspeed Frontier es un sandbox de acción y aventuras espaciales ya disponible en acceso anticipado. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/lightspeed-frontier-sci-fi-sandbox-adventure-now-available-early-access-49459/).


 


Vamos con los vídeos de la semana:


World of One


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/pzYnRGFHH9E" width="640"></iframe></div>


 Day of Infamy


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/q0JbGNhP7TI" width="640"></iframe></div>


 Streets of Rogue


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/BadKmGOu5pQ" width="640"></iframe></div>


 Lightspeed Frontier


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/Z1fcuDNFZKg" width="640"></iframe></div>


 Meganoid


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/643dHvxO4RY" width="640"></iframe></div>


 Esto es todo por este Apt-Get Update. ¿Que te ha parecido?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

