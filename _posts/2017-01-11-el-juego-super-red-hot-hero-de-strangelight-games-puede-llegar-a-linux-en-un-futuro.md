---
author: Pato
category: Plataformas
date: 2017-01-11 12:12:51
excerpt: "<p>El juego necesita un \xFAltimo empuj\xF3n en Kickstarter</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/98786352ab62965a07c516ba5d449e8a.webp
joomla_id: 172
joomla_url: el-juego-super-red-hot-hero-de-strangelight-games-puede-llegar-a-linux-en-un-futuro
layout: post
tags:
- accion
- indie
- plataformas
- kickstarter
title: El juego 'Super Red-Hot Hero' de Strangelight Games puede llegar a Linux en
  un futuro
---
El juego necesita un último empujón en Kickstarter

Después del "relativo" fiasco que supuso el lanzamiento de Mighty No 9 siempre es bueno encontrar desarrollos como este 'Super Red-Hot Hero' [[web oficial](http://superredhothero.com/)] con un concepto similar tratando de mejorar las fórmulas de este estilo de juegos. En este caso se trata de un juego desarrollado por el estudio español StrangeLight Games [[web oficial](http://strangelightgames.com/)] en el que se mezclarán acción y plataformas, y en el que las influencias reconocidas por ellos mismos serán Super Mario Bros, Super Meat Boy y Megaman. Como puedes ver en el siguiente teaser el juego promete bastante:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/xvarJB4ftLI" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Pero lo que nos interesa a nosotros es: ¿llegará a Linux?.


Tras revisar la web y el Kickstarter no vimos referencias a Linux por lo que nos pusimos en contacto con el estudio. La respuesta la teneis a continuación:



> 
> [@JugandoenLinux](https://twitter.com/JugandoenLinux) ya lo creo que si!! ;)
> 
> 
> — StrangeLight Games (@StrangeLightGMS) [10 de enero de 2017](https://twitter.com/StrangeLightGMS/status/818847136351735809)



 Como siempre decimos, hay que tomarlo con cautela ya que no se trata de algo confirmado al 100%. En su página no hay referencias o requisitos mínimos para Linux, pero sí da confianza saber que el estudio ya tiene publicado otro juego en Steam con soporte en Linux: Magma Tsunami [[Steam](http://store.steampowered.com/app/477290/)]


En estos momentos Super Red-Hot Hero ya ha superado su campaña de Greenlight en Steam y se encuentra finalizando su campaña de Kickstarter, donde se puede incluso descargar una demo temprana del juego para probarlo, eso sí en Windows. Si confías en el proyecto y quieres aportar puedes hacerlo en su página de Kickstarter donde necesitan un pequeño empujón final para llegar a la meta:


<div class="resp-iframe"><iframe height="420" src="https://www.kickstarter.com/projects/strangelightgames/super-red-hot-hero/widget/card.html?v=2" style="display: block; margin-left: auto; margin-right: auto;" width="220"></iframe></div>


¿Qué te parece la propuesta de Super Red-Hot Hero? ¿Piensas apoyarlo en Kickstarter? ¿Te gustaría que llegara a Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

