---
author: Pato
category: Hardware
date: 2019-06-05 18:10:34
excerpt: "<p>El \"PC portatil\" muestra un v\xEDdeo ejecutando algunos t\xEDtulos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7a228714f4820898f86713f5349e7364.webp
joomla_id: 1058
joomla_url: smach-z-estara-presente-en-el-e3-2019-y-da-una-muestra-de-su-potencial
layout: post
tags:
- portatil
- hardware
- smach-z
title: "SMACH Z estar\xE1 presente en el E3 2019 y da una muestra de su potencial"
---
El "PC portatil" muestra un vídeo ejecutando algunos títulos

Ya hemos hablado [en otras ocasiones](index.php/buscar?searchword=smach&ordering=newest&searchphrase=all) del "PC-consola portatil" **SMACH Z**, un proyecto tortuoso de una "start up" española que prometía mucho y que con el paso del tiempo se ha ido diluyendo. A modo de resumen, la SMACH Z **contará con un procesador Ryzen Embedded serie 1000 con gráficos Radeon** y mandos hápticos al estilo de los Steam Controller, ofreciendo varias configuraciones a elegir con características distintas como memoria RAM, capacidad de almacenamiento y **sistema operativo Windows o un Linux personalizado por la compañía** a un precio base de **699 dólares (629,10€** según su [web oficial](https://www.smachz.com/shop/)), precio que a todas luces parece desproporcionado si quieren tener opción a vender la SMACH Z al gran público.


Sin embargo la compañía sigue decidida a sacar su producto adelante y ha presentado un escueto vídeo donde se puede ver la consola funcionando y ejecutando varios juegos, y un escueto mensaje que dice **"un vistazo a lo que viene al E3":**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/YbQddtvBrr0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tal y como se ve, la consola parece poder ejecutar los varios juegos con solvencia aunque se mantiene la incógnita de a qué resoluciones, calidad gráfica y FPS se están moviendo en la portátil. Por lo visto será posible ver en acción a la SMACH Z en el E3 y según se han hecho eco en otros medios como [gamingonlinux](https://www.gamingonlinux.com/articles/remember-the-smach-z-handheld-its-apparently-going-to-be-at-e3-this-year.14280) o [muycomputer](https://www.muycomputer.com/2019/06/05/smach-z-debutara-en-el-e3/) parece que la compañía **quiere lanzar el producto durante este mismo año 2019**.


¿Qué te parece la SMACH Z? ¿Te parece una portátil con la que te gustaría jugar?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

