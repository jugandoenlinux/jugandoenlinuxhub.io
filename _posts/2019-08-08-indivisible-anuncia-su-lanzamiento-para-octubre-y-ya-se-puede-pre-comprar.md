---
author: Pato
category: "Acci\xF3n"
date: 2019-08-08 16:29:10
excerpt: "<p>El juego de&nbsp;<span class=\"attribution txt-mute txt-sub-antialiased\
  \ txt-ellipsis vertical-align--baseline\">@LabZeroGames</span> <span class=\"css-901oao\
  \ css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\">@IndivisibleRPG mezcla acci\xF3\
  n y rol con plataformeo</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/29b357c25e35ffc462632f932608528b.webp
joomla_id: 1097
joomla_url: indivisible-anuncia-su-lanzamiento-para-octubre-y-ya-se-puede-pre-comprar
layout: post
tags:
- accion
- indie
- plataformas
- rol
title: Indivisible anuncia su lanzamiento para Octubre y ya se puede pre-comprar
---
El juego de @LabZeroGames @IndivisibleRPG mezcla acción y rol con plataformeo

El estudio Lab Zero Games, creadores del excelente juego de lucha *Skullgirls* que también podemos disfrutar en Linux/SteamOS [acaba de anunciar](https://steamcommunity.com/games/421170/announcements/detail/1594756573589209958) que su próximo lanzamiento "**Indivisible"** estará disponible de forma oficial el próximo día 8 de Octubre, y ya se puede pre-comprar.


***Indivisible** es un juego RPG de acción y plataformas dibujado a mano por Lab Zero, creadores de la aclamada Skullgirls. Ambientada en un enorme mundo de fantasía, Indivisible cuenta la historia de Ajna, una niña valiente con una racha rebelde que se embarca en una búsqueda para salvar todo lo que sabe de la destrucción.*


 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/nZ9h2SJIY78" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>
 
Indivisible nos ofrecerá un destacable apartado artístico con dibujos y animaciones hechos a mano con mecánicas de combate en tiempo real. Nos veremos envueltos en un mundo de fantasía con seres inspirados en diferentes mitologías y donde la narrativa tendrá mucha importancia.
 
En cuanto a requisitos, no disponemos de datos concretos para la versión de Linux, pero atendiendo a los requisitos para Windows y dado el apartado gráfico que Lab Zero suele emplear, es de suponer que el juego se podrá ejecutar casi en cualquier máquina.
 
Por otra parte, para incentivar la campaña de pre-compra, todo el que reserve el juego recibirá el bono "Follow Me Roti!" para hacer que el adorable tapir mascota de Ajna, Roti, siga su aventura para salvar el mundo de Loka.
Si quieres saber mas puedes visitar la [página web oficial](https://indivisiblegame.com/) de Indivisible o puedes reservarlo [en su página de Steam](https://store.steampowered.com/app/421170/Indivisible/) al precio de 39,99€.