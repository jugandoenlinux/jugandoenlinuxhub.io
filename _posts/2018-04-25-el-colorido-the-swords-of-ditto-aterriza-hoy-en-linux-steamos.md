---
author: yodefuensa
category: Rol
date: 2018-04-25 09:25:41
excerpt: <p>Devolver Digital vuelve a traernos otro buen indie</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5189f3286915715b50cff89ddded1008.webp
joomla_id: 721
joomla_url: el-colorido-the-swords-of-ditto-aterriza-hoy-en-linux-steamos
layout: post
tags:
- accion
- indie
- aventura
- rol
- roguelike
title: El colorido "The swords of Ditto" aterriza hoy en Linux/SteamOS
---
Devolver Digital vuelve a traernos otro buen indie

Gracias a las novedades de Steam me hago eco de "The swords of Ditto". Desarrollado por "onebitbeyond" y editado por Devolver Digital, que nos ha traído ya bastantes títulos a nuestro sistema favorito, nos encontramos ante un Rogue-like bonito como ninguno.


Sinopsis:



> 
> The Swords of Ditto es un compacto juego de rol de acción que crea una aventura única para cada nuevo héroe de leyenda en la incansable lucha contra la malvada Mormo. Explora un maravilloso y peligroso mundo alternativo de mazmorras solo apto para los más valientes, y ayuda a tu héroe a mejorar en un pueblecito encantador durante tu misión para erradicar la maldición que asola la isla. ¡Desata el poder de la mística Espada de Ditto y juega con un amigo en modo cooperativo para vivir una aventura inolvidable llena de personajes asombrosos, botines extraordinarios y batallas heroicas!
> 
> 
> "Aventuras únicas vinculadas: Cada aventura se convierte en su propia leyenda, diferente a todas las anteriores, pero, a la vez, parte del legado heroico que las vincula. Las hazañas, los éxitos y los fracasos de cada héroe tienen consecuencias para los que vengan detrás, por ejemplo respecto a encontrar armas y recuperar botines de héroes legendarios que hayan sucumbido.
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/sudVZsrLKYY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Requisitos:


MÍNIMO


* **SO:** Ubuntu 14.04 LTS
* **Procesador:** Intel Core i5-3210M (2 \* 2500) o equivalente
* **Memoria:** 4 GB de RAM
* **Gráficos:** Intel HD 4000
* **Almacenamiento:** 1 GB de espacio disponible
* **Notas adicionales:** Ubuntu 16.04 / Mint 18.3/Ubuntu 17.10 x64 Se han testeado OK, pero no están soportadas oficialmente por GameMaker Studio. Algunas gráficas integradas tienen un problema con la memoria y puede que el juego no se lance.


RECOMENDADO:


* **SO:** Ubuntu 14.04 LTS
* **Procesador:** Intel Pentium G3250 (2 \* 3200) o AMD Phenom 9850 Quad-Core (4 \* 2500) o equivalente
* **Memoria:** 8 GB de RAM
* **Gráficos:** GeForce GTX 460 (1024 MB) o Radeon HD 6850 (1024 MB)
* **Notas adicionales:** Ubuntu 16.04 / Mint 18.3/Ubuntu 17.10 x64 Han sido testeados, pero no están soportadas oficialmente por GameMaker Studio


Con multijugador local y a pantalla dividida sin duda podemos estar ante el que puede ser el juego favorito de los peques en casa.


Podéis encontrar The Swords of Ditto en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/619780/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

