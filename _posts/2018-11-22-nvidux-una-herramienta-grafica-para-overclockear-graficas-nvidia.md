---
author: Serjor
category: Hardware
date: 2018-11-22 18:05:34
excerpt: "<p>Lleva tu gr\xE1fica NVIDIA al m\xE1ximo de capacidad</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5ba0ce4fb25e6baf97c680536d74046a.webp
joomla_id: 911
joomla_url: nvidux-una-herramienta-grafica-para-overclockear-graficas-nvidia
layout: post
tags:
- nvidia
- nvidux
- overclock
title: "Nvidux, una herramienta gr\xE1fica para overclockear gr\xE1ficas NVIDIA"
---
Lleva tu gráfica NVIDIA al máximo de capacidad

Leemos en [Linuxuprising](https://www.linuxuprising.com/2018/11/nvidiux-nvidia-gpu-overclocking-and.html) que existe una aplicación gráfica llamada [Nvidux](https://github.com/RunGp/Nvidiux), la cuál permite hacer overclock en gráficas NVIDIA.


La herramienta es de código libre escrita en python, y permite ajustar desde gráficas GTX 400 en adelante:


* Velocidad de los ventiladores
* Hacer overclock automáticamente durante el arranque
* Monitorización gráfica de temperatura, carga de la GPU, % de la velocidad del ventilador y % de uso de la memoria gráfica
* Sobrevoltaje


Lamentablemente, y dado el estado de los drivers libres por falta de documentación de NVIDIA, la aplicación solamente funciona con los drivers privativos.


Y tú, ¿vas a poner tu gráfica a máximo rendimiento? Si lo pruebas, coméntanos los resultados en los comentarios, en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD), y sobretodo en nuestro [hilo del foro sobre benchmarks](index.php/foro/foro-general/46-benchmarks-de-tus-videojuegos-en-linux?start=0)

