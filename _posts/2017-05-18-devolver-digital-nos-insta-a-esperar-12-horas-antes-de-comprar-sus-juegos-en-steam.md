---
author: Pato
category: Ofertas
date: 2017-05-18 19:28:34
excerpt: <p>Jugosos descuentos de hasta el 90% durante este fin de semana</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9e02f79f72eca3ca589ae757d97a7173.webp
joomla_id: 328
joomla_url: devolver-digital-nos-insta-a-esperar-12-horas-antes-de-comprar-sus-juegos-en-steam
layout: post
tags:
- ofertas
- devolver-digital
title: Devolver Digital nos insta a esperar 12 horas antes de comprar sus juegos en
  Steam
---
Jugosos descuentos de hasta el 90% durante este fin de semana

Actualización: Todas las apuestas daban por sentado que las 12 horas de espera eran en realidad una cuenta atrás para descuentos varios en su catálogo, y así ha sido. Durante todo el fin de semana encontraremos ofertas de hasta el 90%, por lo que es un buen momento para revisar nuestro crédito disponible y echar cuentas.


 


Os dejamos con el vídeo presentación de estas ofertas:


 



> 
> The Devolver Digital Publisher Weekend is live - up to 90% off games & [@DevolverPA](https://twitter.com/DevolverPA) streaming right on the sale page! <https://t.co/xLs0BGTtFJ> [pic.twitter.com/VLHha2RdzI](https://t.co/VLHha2RdzI)
> 
> 
> — Devolver Digital (@devolverdigital) [18 de mayo de 2017](https://twitter.com/devolverdigital/status/865255320548855808)



 


Además, si entraís en la página de las ofertas de steam podéis encontrar un streaming en directo de uno de los miembros de Devolver Digital.


 


Artículo original:


Si ayer nos acostábamos con la noticia de que Devolver Digital va a tener [su conferencia de prensa](index.php/homepage/editorial/item/449-el-e3-2017-se-acerca-y-en-jugando-en-linux-vamos-a-seguirlo-de-cerca) en el próximo E3, hoy nos levantamos con un más que misterioso Tweet:



> 
> Nobody buy any Devolver Digital games on Steam for the next 12 hours. [pic.twitter.com/gDC1edeNbE](https://t.co/gDC1edeNbE)
> 
> 
> — Devolver Digital (@devolverdigital) [18 de mayo de 2017](https://twitter.com/devolverdigital/status/865062067325349888)







 **"Que nadie compre ningún juego de Devolver Digital en Steam durante las próximas 12 horas"** reza el mensaje. ¿Qué se traerán entre manos? ¿Se avecinan ofertas?...


Cualquier hipótesis nos la puedes comentar en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

