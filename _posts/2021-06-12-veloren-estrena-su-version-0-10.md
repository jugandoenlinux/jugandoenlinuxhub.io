---
author: P_Vader
category: "Exploraci\xF3n"
date: 2021-06-12 15:51:53
excerpt: "<p>@velorenproject publica su nueva alpha 0.10 con toneladas de novedades.</p>\r\
  \n"
image: https://i.redd.it/19ei6mg9uk371.png
joomla_id: 1315
joomla_url: veloren-estrena-su-version-0-10
layout: post
tags:
- pixel
- exploracion
- mundo-abierto
- veloren
title: Veloren estrena su version 0.10
---
@velorenproject publica su nueva alpha 0.10 con toneladas de novedades.


[Veloren](https://veloren.net/) es un proyecto libre, que a pesar de ser relativamente joven, **está empezando a tener bastante repercusión por su gran calidad**, aun siendo todavía una versión alpha por difícil que parezca a veces. Esta especie de **RPG en un mundo abierto pixelado con aires de Zelda y Minecraft, con la posibilidad de jugarlo en modo multijugador** tiene todos los ingredientes para convertirse en un gran videojuego.

Para celebrar su lanzamiento **van a hacer una mega partida, a modo de fiesta multitudinaria, en su servidor oficial el 12/6/2021 a las 20:00 (UTC+2)**. Descarga su lanzador [Airship](https://github.com/songtronix/airshipper/releases/latest/download/airshipper-linux.tar.gz) y no olvides hacerte una [cuenta en su web](https://veloren.net/account). **Jugandoenlinux estará en la fiesta para probar esta nueva versión ¡acompáñanos!**

**Entrando en detalle de esta nueva versión [0.10 viene con una inetrminable](https://gitlab.com/veloren/veloren/-/releases/v0.10.0) cantidad de cambios, añadidos y arreglos**, que sinceramente no vamos a poder abarcar en este artículo, asi que voy a resumiros lo mas destacado desde la anterior versión:


**Se han añadido:**


* Picos para picar la piedra y minerales.
* Efectos de sonido para el daño.
* Los aldeanos y guardias tienen pociones y cuidado que saben usarlas.
* Música de combate en las mazmorras.
* Andate silencioso, o los [NPCs](https://es.wikipedia.org/wiki/Personaje_no_jugador) te escucharan.
* Muchos comandos nuevos para los admins.
* Nuevas estaciones de "crafteo" para equiparte bien.
* ¡Cuidado que el fuego de las fogatas ahora quema!
* Los viajantes ahora siguen su camino o carretera.
* En algunos lugares los admins pueden construir.
* Baneos temporales e historial de baneos.
* Muchos nuevos marcadores para el mapa.
* Sistema de materiales para hacer armadura y armas.
* Nuevas escaleras para las mazmorras.
* Bloqueos usando tus armas.
* Una música exclusiva a las aldeas.
* Nuevas armas de una mano, puedes llevar una en cada mano.
* Nuevos aspectos visuales para las cuevas.
* Menas de minerales en las cuevas.


**Y se ha cambiado:**


* Menos gravedad.
* Recuperas la energía mientras planeas.
* Retocado el arco.
* Cambiado los ataques del minotauro, el yeti y el golem de arcilla.
* El agua quita el efecto de fuego.
* Cambios en el daño de caída.


En definitiva en esta ultima versión **se han centrado en desarrollar el crafteo en el juego**, con la inclusión de la obtención de minerales y las estaciones de crafteo. También se han **desarrollado mucho los [NPC](https://es.wikipedia.org/wiki/Personaje_no_jugador), llegando incluso al mercadeo** con ellos.


¿Qué te parece Veloren? ¿Qué te gusta? ¿Qué le falta? Venga, cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

