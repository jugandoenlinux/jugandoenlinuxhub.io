---
author: P_Vader
category: "Acci\xF3n"
date: 2021-06-04 08:44:34
excerpt: "<p><span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\"><span\
  \ class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@Steam</span> cambia\
  \ el modo de obtener Prime en @CSGO</span> a exclusivamente de pago.</p>\r\n"
image: https://steamcdn-a.akamaihd.net/apps/csgo/blog/images/prime.png
joomla_id: 1314
joomla_url: csgo-se-carga-el-prime-gratuito
layout: post
tags:
- competitivo
- fps
- csgo
title: CSGO se carga el Prime gratuito
---
@Steam cambia el modo de obtener Prime en @CSGO a exclusivamente de pago.

Como [nos explican en su blog](https://blog.counter-strike.net/index.php/2021/06/34385/), **CSGO pasó hace ya mas de 3 años a ser gratuito** y para las cuentas de pago les otorgo cuentas Prime con ciertos beneficios, rangos, niveles de habilidades, obtención de objetos y un emparejamiento con otros Prime, dando un estatus privilegiado y en teoría atractivo. A partir de entonces si eras un nuevo jugador, Prime lo podías obtener pagando o con cierto esfuerzo de manera gratuita, llegando a unos determinados puntos de experiencia (unas 100 horas de juego).


Con el paso del tiempo es evidente que CSGO se ha convertido en un campo minado de tramposos, bots o cuentas secundarias (smurf), incluso obteniendo el estatus de Prime, muchas veces de manera gratuita.


Según podemos leer en su nota, la medida para acabar con esta mala experiencia de jugadores tóxicos es eliminar la obtención del Prime gratuito y dejar a todos los nuevos jugadores en un modo "sin rango". **A partir de ahora los nuevos jugadores gratuitos de CSGO no podrán obtener experiencias, rangos, objetos ni jugar competitivo con rango, y solo puedes obtener Prime pagando**. Esto no quita que **los jugadores Prime tienen una nueva opción de jugar al modo sin rango y que no les afecte a su rango competitivo**, algo positivo para jugar con otros sin preocuparte de que te arruinen tu rango.


¿Que se consigue de esta manera? **Dejar dos claros grupos de jugadores de CSGO, los gratuitos y los de pago**. ¿Es esta la medida correcta para acabar con estos jugadores tramposos? Sinceramente tengo algunas dudas, quizás se pueda acabar con la obtención de cuentas Prime con granjas de bots, o limitar la facilidad de tener cuentas secundarias o smurf.


Parece que los jugadores de pago tendrán menos posibilidades de que se crucen con un tramposo, pero **¿qué experiencia va a quedar en el modo "sin rango"?** Esperemos que los nuevos jugadores que se acerquen a probar CSGO no salgan espantados por una mala experiencia de un modo gratuito atrofiado.


Pese a las continuas críticas a CSGO de que fue un error pasar a modo gratuito, Valve mantiene que fue y es un acierto y eso no va a cambiar. Evidentemente **esta medida debería de venir acompañada de otras igualmente profundas para darle solución al juego competitivo estrella de Valve.**


¿Que opinas de esta media de Valve? ¿Crees que servirá para algo? Cuéntanos tu opinión en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)

