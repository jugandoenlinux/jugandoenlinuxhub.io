---
author: Pato
category: Apt-Get Update
date: 2017-06-23 17:37:00
excerpt: "<p>Volvemos a dar un repaso a lo que se nos qued\xF3 por el camino</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3ddc7c1342202ff1939ae99b11098ec6.webp
joomla_id: 386
joomla_url: apt-get-update-two-worlds-ii-planet-nomads-planetary-anihilation-kona-geoid
layout: post
title: Apt-Get Update Two Worlds II & Planet Nomads & Planetary Anihilation & Kona
  & Geoid...
---
Volvemos a dar un repaso a lo que se nos quedó por el camino

Hemos estado un tiempo sin Apt-Gets ya que la información mas relevante ya la hemos cubierto en jugandoenlinux.com, pero ya va siendo hora de actualizarnos, y aunque después de la avalancha del E3 hemos tenido una semana muy tranquila vamos a ver algunas cosas interesantes:


Desde gamingonlinux.com:


- El juego de rol 'Two Worlds II' ha sufrido un retraso por problemas con el port. Esperan tenerlos resueltos en un futuro próximo. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/two-worlds-ii-rpg-delayed-for-linux.9847).


- 'Planet Nomads' el "sandbox" de supervivencia ha recibido un parche con nuevas mejoras como el soporte de mandos, cambios en el campo de visión, mapeo de teclas y mucho más. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/planet-nomads-expands-again-with-gamepad-support-customizable-key-bindings-fov-slider-and-more.9850).


- Los desarrolladores de 'Sombrero: Spaguetti Western' buscan beta testers para un próximo lanzamiento en Linux. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/the-developers-of-sombrero-spaghetti-western-mayhem-are-looking-for-linux-testers.9869).


- 'Planetary Anihilation: TITANS' recibe una actualización que añade Comandantes gratuitos y mucho más. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/planetary-annihilation-titans-updated-and-still-alive-adds-new-free-commanders-and-more.9868).


- La última actualización de 'Kona' debería resolver la mayoría de problemas en Linux. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/the-latest-kona-update-might-fix-a-few-major-linux-issues-and-crashes.9874).


- El juego de supervivencia 'The Culling' parece que ha sido actualizado corrigiendo los problemas que sufría en Linux. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/the-culling-survival-game-now-works-on-linux-sort-of.9880).


- 'Machinarium' recibe una nueva actualización para eliminar Flash que debería estar disponible para Linux próximamente. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/machinarium-updated-to-remove-flash-new-linux-build-is-planned-too.9884).


Desde linuxgameconsortium.com:


- El plataformas basado en físicas 'Geoid' ya está disponible en Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/geoid-physics-based-platform-now-steam-gaming-2017-53987/).


- 'Hidden Folks' un juego de tipo "donde está Wally" ya está disponible en Steam. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/hidden-folks-hidden-object-adventure-steam-gaming-2017-53981/).


Para terminar con las reseñas, ¿sabías que hay cientos de juegos open source en Github? Nos lo cuentan nuestros amigos de [muylinux.com](http://www.muylinux.com). Puedes verlo [en este enlace](http://www.muylinux.com/2017/06/02/juegos-open-source-en-github).


Vamos con los vídeos de la semana:


 Machinarium:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/uwZBdWRSBRs" width="640"></iframe></div>


 Kona


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/iHdcOzC1UJc" width="640"></iframe></div>


Sombrero: Spaguetti Western Mayhem:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/8RxvFEm2t44" width="640"></iframe></div>


 Two Worlds II


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/6ArD4vxKq2E" width="640"></iframe></div>


 Planet Nomads:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/O3Vj9chfjpQ" width="640"></iframe></div>


Geoid:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/N9wmDz21O24" width="640"></iframe></div>


Hidden Folks:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/kYw_tw__7ow" width="640"></iframe></div>


 


Es todo por este Apt-Get. ¿Qué te ha parecido?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

