---
author: CansecoGPC
category: Plataformas
date: 2021-12-19 13:26:14
excerpt: "<p>&nbsp;Two Tribes libera el c\xF3digo con licencia libre.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/toki-tori-2.webp
joomla_id: 1397
joomla_url: linux-nativo-motor-gpl
layout: post
tags:
- motor-grafico
- gpl
title: Liberado el codigo fuente del motor de RIVE y Toki Tori 2
---
 Two Tribes libera el código con licencia libre.


La decisión del estudio Two Tribes de liberar el código fuente del motor ttengine con licencia GPLv2, supone una gran noticia para los usuarios de RIVE y Toki Tori 2, sobretodo para preservación, arreglos o incluso mejoras.


![rive escena](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/rive-escena.webp)


Los assets incluidos (imágenes, animaciones, sonidos, música, etc), así como los juegos siguen teniendo copyright, pero con el motor se podrán crear nuevos juegos con assets propios o con licencias copyleft.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/0fVr3bBKJzY" title="YouTube video player" width="780"></iframe></div>


Fuente:


<https://github.com/TwoTribesGames/ttengine>


 

