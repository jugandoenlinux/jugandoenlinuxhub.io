---
author: leillo1975
category: Software
date: 2019-07-22 15:15:52
excerpt: "<p>El desarrollo \"Open Source\" de @feralgames se vuelve a actualizar</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/37d163b88a4522bd852de06260df3d98.webp
joomla_id: 1079
joomla_url: gamemode-alcanza-la-version-1-4
layout: post
tags:
- feral-interactive
- software-libre
- gamemode
title: "Nueva versi\xF3n de la app \"Gamemode\" (ACTUALIZACI\xD3N)."
---
El desarrollo "Open Source" de @feralgames se vuelve a actualizar


**ACTUALIZADO 22-1-20**: Nos acaba de llegar la notificación de que esta utilidad de Feral para optimizar nuestros PC's para sacar el mayor rendimiento posible en nuestros juegos favoritos, se acaba de actualizar una vez más, en esta ocasión con pocos cambios, pero no por ello menos importantes. Con respecto a la versión anterior, en la [versión 1.5](https://github.com/FeralInteractive/gamemode/releases/tag/1.5) de Gamemode vamos a encontrar:


*-Introducir un nuevo conjunto de APIs de D-Bus basadas en pidfd (#[173](https://github.com/FeralInteractive/gamemode/pull/173))*  
 *-Cambia dinámicamente el gobernador en las GPU integradas para mejorar el rendimiento (#[179](https://github.com/FeralInteractive/gamemode/pull/179))*  
 *-Otros arreglos y mejoras.*


Quizás el más importante de estos cambios sea el segundo, que basicamente permite que en determinadas ocasiones, cuando se usa una iGPU, podamos usar otro gobernador alternativo en vez de "performance", ya que este último con ciertos juegos provoca el efecto contrario al mandar demasiados recursos a la gráfica en vez de al procesador.




---


**NOTICIA ORIGINAL:** Seguramente muchos de vosotros ya esteis al tanto de esta noticia, pues realmente ya tiene un par de dias "en el candelero", pero no podíamos dejar pasar la oportunidad de hablar nuevamente de GameMode. Gracias a la **información suministrada por nuestra comunidad** nos hemos enterado que este **proyecto de Software Libre impulsado por Feral Interactive** (si, ellos otra vez) se ha vuelto a actualizar recientemente trayendo importantes novedades que seguro que más de uno estará deseando probar.


Ya [hemos hablado varias veces](index.php/buscar?searchword=gamemode&ordering=newest&searchphrase=all) de este software, pero por si alguno aun no lo conoce, es un **conjunto de demonio (servicio) y librerias** que lo que hacen basicamente es **cambiar parametros de nuestro Sistema Operativo, como por ejemplo el  gobernador del kernel, para conseguir el máximo rendimiento** de este este cuando ejecutamos un juego (o aplicación). Estando instalado en nuestros equipos funciona automáticamente si ejecutamos algunos de los últimos juegos de Feral, como [DiRT 4](index.php/homepage/analisis/20-analisis/1143-analisis-dirt-4), [Rise of the Tomb Raider](index.php/homepage/generos/accion/5-accion/835-rise-of-tomb-raider-20-aniversario-disponible-en-gnu-linux-steamos), [Total War: Three Kingdoms](index.php/homepage/generos/estrategia/8-estrategia/1168-disponiible-total-war-three-kingdoms-para-linux-steamos), [Total War: WARHAMMER II](index.php/homepage/generos/estrategia/8-estrategia/1028-total-war-warhammer-ii-ya-disponible-en-linux-steamos) y [Total War Saga: Thrones of Britannia;](index.php/homepage/generos/estrategia/8-estrategia/879-total-war-saga-thrones-of-britannia-ya-esta-en-gnu-linux-steamos) pero mediante un sencillo comando podemos apliacarla en cualquier software. La última actualización, [la 1.3](index.php/homepage/generos/software/12-software/1119-lanzada-oficialmente-la-version-1-3-de-gamemode), data de mediados de Marzo, por lo que ya se echaban de menos novedades por parte de este desarrollo. Entre las novedades que tiene esta [versión 1.4](https://github.com/FeralInteractive/gamemode/releases/tag/1.4) encontramos las siguientes:


*-Añadidos nuevos métodos/propiedades de D-Bus para su uso con herramientas externas como la [extensión *GameMode* de GNOME Shell](https://github.com/gicmo/gamemode-extension/)  .*  
 *-Arreglada la prioridad de E/S y las optimizaciones de niceness para aplicarlas a todo el proceso en lugar de sólo a la tarea que solicita GameMode .*  
 *-"gamemoded" ahora recargará automáticamente el archivo de configuración cuando se cambie y actualizará las optimizaciones de los clientes actuales.*  
 *-Añadido soporte para usar la biblioteca de clientes dentro de Flatpak comunicándose con el demonio a través de un portal.*  
 *-La biblioteca del cliente ahora usa libdbus en lugar de sd-bus.*  
 *-Arreglado "gamemoderun" para usar la ruta correcta de la librería dependiendo de si la aplicación es de 32 o 64 bits.*  
 *-Soporta la variable de entorno "GAMEMODERUNEXEC" para especificar un comando de envoltura adicional para juegos lanzados con "gamemoderun" (por ejemplo, una envoltura de GPU híbrida como "optirun").*  
 *-Varias correcciones y mejoras.*


Teneis más información sobre Gamemode y su instalación en la [página de su proyecto](https://github.com/FeralInteractive/gamemode) en Github. ¿Sois usuarios de Gamemode? ¿Habeis notado mejoría en el rendimiento de los juegos usando esta herramienta? Cuéntanoslo en los comentarios o deja tus mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

