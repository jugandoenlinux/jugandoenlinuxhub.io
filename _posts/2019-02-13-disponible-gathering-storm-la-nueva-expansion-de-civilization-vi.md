---
author: leillo1975
category: Estrategia
date: 2019-02-13 20:12:15
excerpt: AspyrMedia nos trae esta DLC al mismo tiempo que en Windows
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/67dcc46237b8f15ce4fdbadcc79dc8ea.webp
joomla_id: 971
joomla_url: disponible-gathering-storm-la-nueva-expansion-de-civilization-vi
layout: post
tags:
- aspyr-media
- dlc
- civilization-vi
- firaxis
- turnos
- gathering-storm
title: "Disponible \"Gathering Storm\", la nueva expansi\xF3n de Civilization VI."
---
@AspyrMedia nos trae esta DLC al mismo tiempo que en Windows

Aun no han pasado 3 meses desde que os [anunciamos](index.php/homepage/generos/estrategia/8-estrategia/1031-gathering-storm-sera-la-nueva-dlc-de-civilization-vi) la llegada de esta nueva expansión para el aclamado [Civilization VI](index.php/homepage/analisis/item/352-analisis-sid-meier-s-civilization-vi), y hoy, día de los enamorados, finalmente **Firaxis Games** y **Aspyr Media**, nos traen "**Gathering Storm**". Cuando os dábamos esta noticia **no confiábamos que llegase al mismo tiempo que en Windows**, pero en esta ocasión los integrantes de Aspyr se han puesto las pilas y también **podremos disfrutarlo hoy mismo**. Recordad que normalmente los ports de las DLC's de Civilization llegaban con retraso con respecto a las de Windows, incluso teniendo que esperar casi dos años por el [Multijugador entre diferentes plataformas](index.php/homepage/generos/estrategia/8-estrategia/1049-civilization-vi-estrena-multijugador-multiplataforma). Hace un momento, orgullosos (y con razón), Firaxis publicaba este Tweet anunciando esta expansión:



> 
> Civilization VI: Gathering Storm is OUT NOW on PC, Mac and Linux! Are you ready to take [#OneMoreTurn](https://twitter.com/hashtag/OneMoreTurn?src=hash&ref_src=twsrc%5Etfw)?  
>   
> Buy now: <https://t.co/8WjpjLb1xx> [pic.twitter.com/zKMFWwP3ad](https://t.co/zKMFWwP3ad)
> 
> 
> — Civilization VI: Gathering Storm (@CivGame) [14 de febrero de 2019](https://twitter.com/CivGame/status/1095914408046297088?ref_src=twsrc%5Etfw)


Os recordamos cuales son las **caracterísiticas principales** de le juego según la descripción de Steam:


*En Gathering Storm, la segunda expansión de Civilization VI, el mundo que te rodea está más vivo que nunca.*   
  
*Traza un camino hacia la victoria para tu pueblo desarrollando nuevas tecnologías avanzadas, diseñando proyectos y negociando cuestiones vitales con la comunidad global en el Congreso Mundial.*   
  
*Las decisiones que tomes en el juego influirán en el ecosistema del mundo y pueden tener un impacto en el futuro de todo el planeta. Los desastres naturales como las inundaciones, las tormentas o los volcanes pueden saquear o destruir tus mejoras y distritos, pero también revitalizar y enriquecer la tierra a su paso.*  
  
*Además de estos nuevos sistemas, Civilization VI: Gathering Storm incorpora ocho civilizaciones adicionales y nueve líderes más. Hay siete maravillas del mundo que se pueden construir, además de una gran variedad de nuevas unidades, distritos, edificios y mejoras.*


##### *EFECTOS MEDIOAMBIENTALES*


*volcanes, tormentas (ventiscas, tormentas de arena, tornados y huracanes), cambio climático, inundaciones y sequías.*


##### *ENERGÍA Y RECURSOS CONSUMIBLES*


*los recursos estratégicos tienen su propio papel en Gathering Storm. Estos recursos se consumen ahora en centrales eléctricas para producir electricidad para las ciudades. En un principio, necesitarás quemar recursos basados en el carbono, como el petróleo y el carbón, para suministrar energía a tus edificios más avanzados, pero también desbloquearás energías renovables a medida que avances en las tecnologías actuales. Las elecciones que hagas sobre la utilización de recursos afectarán directamente a la temperatura del mundo y pueden hacer que los casquetes polares se derritan y suba el nivel del mar.*


##### *DISEÑO DE PROYECTOS*


*conforma el mundo que rodea tu imperio para superar condiciones desfavorables del terreno con mejoras como canales, presas, túneles y ferrocarriles. Al fundar ciudades, no ignores el riesgo de inundación de las zonas de bajío costero, pero piensa que en las fases finales del juego, tendrás nuevas tecnologías —como los Muros de contención— para protegerlas.*


##### *CONGRESO MUNDIAL*


*haz que tu voz se escuche entre los demás líderes del mundo. Gánate el Favor diplomático mediante Alianzas, influyendo en las ciudades-estado, compitiendo en los Juegos mundiales y mucho más. Usa el Favor diplomático para conseguir que los demás líderes te hagan promesas, votar en Resoluciones, convocar una Sesión especial para solucionar una emergencia y aumentar el peso de tus votos en la búsqueda por conseguir la nueva victoria diplomática.*


##### *PRINCIPIOS Y TECNOLOGÍAS DEL S. XXI*


*se ha añadido una nueva época al árbol tecnológico y de principios. Combate los nuevos efectos medioambientales con ideas especulativas, como reubicar la población en ciudades flotantes y desarrollar tecnologías para recapturar las emisiones de carbono.*


##### *NUEVOS LÍDERES Y CIVILIZACIONES*


*se han añadido nueve líderes de ocho civilizaciones más, cada una con sus propias bonificaciones y maneras de jugar, así como un total de nueve unidades, cuatro edificios, tres mejoras, dos distritos y un gobernador, todos ellos exclusivos.*


##### *NUEVOS ESCENARIOS*


***La Peste negra:***   
*la Peste negra asoló Europa y Asia Occidental a mediados del siglo XIV, llevándose por delante a más población que ningún otro evento de la historia. La pandemia acabó con millones de personas, arruinó economías, echó abajo dinastías políticas y transformó la faz del mundo occidental. Tu tarea consiste en liderar a tu nación a través de la desgracia: mantén vivos a los ciudadanos y fuerte tu economía, y que no decaiga la fe en un mundo arrasado por el terror y la desesperación.*   
  
***Maquinaria bélica:***   
*a comienzos de la PGM, el ejército imperial alemán tenía un plan osado: invadir Bélgica, que era neutral, y desde ahí adentrarse en territorio francés antes de que estos pudieran movilizarse. Si lo conseguían, las fuerzas alemanas tomarían París en un mes y acabarían con su resistencia para siempre. Para contrarrestarlo, el mando francés trazó el Plan 17, una acometida sin cuartel pensada para ir al encuentro del ejército alemán y detenerlo. Cuando se declaró la guerra, ambos ejércitos se pusieron en movimiento y protagonizaron una de las campañas militares más increíbles y desconcertantes de la historia. En este escenario multijugador, los jugadores tomarán las riendas de una de estas dos grandes potencias en este punto concreto. Si eliges Alemania, tu misión será capturar París. Si eliges Francia, deberás impedirlo. El tiempo avanza y el enemigo también. ¡En marcha!*


##### *MÁS CONTENIDO NUEVO*


*se han añadido siete maravillas del mundo y siete maravillas naturales, 18 unidades, 15 mejoras, 9 edificios, 5 distritos, 2 conjuntos de edificios, 9 tecnologías y 10 principios, todos ellos nuevos.*


##### *SISTEMA DE JUEGO MEJORADO*


*el sistema de espionaje se ha visto mejorado con nuevas opciones, se han actualizado las victorias cultural y científica, se han añadido nuevos Momentos históricos y se han introducido más mejoras en los sistemas existentes.*

Estos son los **requisitos necesarios** para poder ejecutarla, los mismos que el juego Base:


+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 16.04 and 18.04
+ **Procesador:** Intel Core i3 530 2.93 Ghz or AMD A8-3870 2.93 Ghz
+ **Memoria:** 6 GB de RAM
+ **Gráficos:** 1 GB GPU Minimum - GeForce GTX 650M
+ **Almacenamiento:** 25 GB de espacio disponible
+ **Notas adicionales:** Algunos procesadores Intel i3 pueden requerir una partición swap de 2 GB adicional. AVISO IMPORTANTE: Los chipsets ATI e INTEL NO son compatibles para ejecutar Civilization VI LINUX. ¿No cumple con los requisitos anteriores? ¿Corriendo en una distribución única? Eso no significa que su configuración no funcionará en Civ VI! Visite la página de la comunidad de Civilization VI para compartir su experiencia con otros jugadores de Linux y aprender cómo enviar errores a Aspyr. Sus comentarios nos ayudarán a mejorar Civ VI Linux y futuras versiones de AAA Linux!


Os informamos que también **realizaremos un Stream especial** para nuestros canales de [Twitch](https://www.twitch.tv/jugandoenlinux) y [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) para que veais de primera mano las novedades de "Gathering Storm". **El Stream será hoy mismo a las 22:00**. Podreis comentar y preguntarnos lo que querais a través del chat en directo. También os adelantamos que **en unas semanas vereis publicado un completo análisis de esta DLC** tal y como hemos hecho en anteriores ocasiones. Podeis comprar Civilization VI: Gathering Storm en la **[Humble Store](https://www.humblebundle.com/store/sid-meiers-civilization-vi-gathering-storm?partner=jugandoenlinux) (Patrocinado)** y en [Steam](https://store.steampowered.com/app/947510/Sid_Meiers_Civilization_VI_Gathering_Storm/). Os dejamos con el video oficial que explica las novedades del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/EZ8XRJNitCE" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>
 


¿Qué os parecen las novedades que trae esta expansión? ¿Habeis jugado ya a Civilization VI? Cuentanoslo en los comentarios o charla con nosotros sobre ello en nuetro grupo de [Telegram](https://t.me/jugandoenlinux).


