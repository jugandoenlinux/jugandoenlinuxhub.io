---
author: Son Link
category: Estrategia
date: 2022-09-26 19:31:04
excerpt: "<p>Por fin tenemos nueva versi\xF3n de este gran juego de estrategia que\
  \ trae muchas novedades y una nueva civilizaci\xF3n.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/0ad/alpha26/portada.webp
joomla_id: 1488
joomla_url: 0-a-d-alpha-26-zhuangzi
layout: post
tags:
- software-libre
- 0ad
- estrategia-en-tiempo-real
title: '0 A.D. Alpha 26: Zhuangzi'
---
Por fin tenemos nueva versión de este gran juego de estrategia que trae muchas novedades y una nueva civilización.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/cDcSXqlDcbA" title="0 A. D. Empires Ascendant — Alpha 26 Zhuangzi Trailer" width="720"></iframe></div>


Tras varios meses de desarrollo por fin tenemos nueva Alpha del juego de estrategia libre 0 A.D., la **XXVI (26)** y nombre en clave **Zhuangzi**, nombre que salio elegido tras una votación y que corresponde al antiguo filosofo chino **Zhuang Zhou** (más conocido como Zhuangzi en occidente, y que se cree que vivió entre los años 369 y 290 a. C), la cual trae varias novedades, ademas de correcciones y mejoras.


La principal novedad es la inclusión de una nueva civilización, los **Han**, la segunda dinastía imperial china (206 a.C - 220 d.C), la cual no es nueva ya que iba a ser originalmente parte del mod **Rise of the East** y luego incluida en el mod **Delenda Est**, y durante ese tiempo la comunidad ha ayudado a mejorarla, llegando a tener un nivel similar al resto de civilizaciones. Nuevo arte, tecnologías únicas, nuevos edificios, etc, nos esperan para disfrutar jugando con ella.


Ademas se incluyen dos nuevos mapas para este civilización:


**Tarim basin**:


![02](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/0ad/alpha26/02.webp)


**Yangtze**:


![03](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/0ad/alpha26/03.webp)


 


### Mejoras


Los cambios que más notaremos son:


* Mejoras en el movimiento de la formación, especialmente en los giros y en la navegación a través y alrededor de los objetos, para una mayor fluidez.
* Mejoras en la interfaz del juego:
	+ Lo más importante: un control deslizante para ajustar el tamaño de los elementos de la interfaz
* Instalación más fácil de mods mediante arrastrar y soltar
* Mejoras en la interfaz y el rendimiento del Editor de Atlas
* Mejoras en la IA.


### Interfaz


* Se ha añadido un buscador de jugadores en el vestíbulo para facilitar la búsqueda de un jugador especifico
* Se ha añadido un botón adicional para ver la pantalla de resumen cuando se abandona la sesión del juego. Puedes saltarte el resumen y volver directamente al menú principal
* Se han añadido varios tooltips en la pantalla del gestor de mods para ayudar a entender que el orden de ellos importa y quizás ayudar a resolver conflictos entre ellos
* Se han mejorado los tooltips y la visibilidad en el juego.
* La escala de la interfaz gráfica de usuario es ahora redimensionable, mediante un menú desplegable. También hay un cuadro de confirmación con una cuenta atrás para revertir dichos cambios.


### Arte


* Muchas mejoras en las texturas, mejores retratos, ajustes de color y correcciones
* Muchas mejoras en los modelos 3D y mejores animaciones, nuevos retratos
* Reestructuración completa y mejoras en los Han antes de su integración en la versión
* 26 nuevas pistas musicales, incluyendo dos temáticas para los Han


### Jugabilidad


Ahora hay una casilla de verificación en la configuración del juego para permitir a los jugadores que la Cartografía se investigue automáticamente desde el comienzo de la partida: significa que los aliados pueden compartir las partes del mapa que cada uno descubrió si la opción está seleccionada.


Ahora las formaciones pueden seleccionarse como un todo con un solo clic, lo que las convierte en un "batallón" más que antes. Ten en cuenta que este comportamiento se puede ajustar en las opciones. Utilización de normas (específicas de la civilidad).


Ahora también es posible empujar elementos al frente de la cola de producción. Esto significa que ahora se puede encargar a una unidad que haga una tarea inmediatamente y luego volver a las tareas que tenía antes, sin repetir las órdenes. Esto se puede hacer utilizando las teclas de acceso rápido, así que asegúrate de revisar el editor de teclas de acceso rápido.


Ahora también hay un botón de "Llamada a las armas", que permite a los jugadores, con un solo clic, ordenar a sus soldados que depositen recursos en el lugar de entrega más cercano y luego se desplacen al destino (y ataquen todo lo que haya en el camino) con un solo botón.


El número de trabajadores inactivos se muestra ahora en el botón correspondiente.


El olivo es ahora una fuente de madera (ya que era difícil ver que era un suministro de alimentos).


Esta versión introduce el mod oficial de la comunidad para la Alfa 26 que se [encuentra aquí](https://wildfiregames.com/forum/topic/83784-introducing-the-official-community-mod-for-alpha-26/#comment-504268), donde los cambios de equilibrio y de juego, si se introducen, se trasladarán a la próxima versión de 0ad.


### Ajustes de equilibrio


Con la ayuda de varios de los mejores jugadores del lobby de A25b y el equipo de equilibrio, se han realizado muchos ajustes de equilibrio en el último año. Estos ajustes responden en su mayoría a las preocupaciones de la comunidad, por ejemplo:


* La disminución del ataque de la caballería de fuego ibérica.
* El aumento del coste de la caballería mercenaria.
* Las naves son menos eficaces contra los humanos.
* La caballería con lanza es ahora más efectiva contra otra caballería.
* Los piqueros tienen menos armadura.
* Reestructuración de la Reforma Ipicratea (atenienses).
* Las catapultas son más fuertes.


El resultado final es una experiencia de juego más elegante y, esperemos, más equilibrada en A26 en comparación con A25b. Además de los cambios que se hicieron para resolver las preocupaciones, también hay otros cambios de equilibrio, el más grande es la adición de los Han, pero también:


* Las unidades ahora tienen una ligera aceleración (algunas más, otras menos).
* La fruta y el pescado tienen ahora una pequeña regeneración mientras no se agoten.
* El elefante trabajador mauriciano puede construir (de nuevo).
* Se ha normalizado la experiencia del botín animal.
* Aumento del límite de guarnición del arsenal.
* Bonificación macedonia: tiempo de investigación de tecnología de almacenes instantáneo.
* Ajustar las pirámides kushitas para que sean más utilizadas.
* Eliminados los límites de torres y fortalezas.
* Muchos otros pequeños ajustes y correcciones.


### Mejoras en el motor


Esta versión introduce la calidad de las texturas y el filtrado anisotrópico en el renderizador: puedes elegir entre la calidad de las texturas baja, media y alta y el filtrado anisotrópico de 1x a 16x.


Tenéis todos los detalles de esta actualización en el [anuncio oficial](https://play0ad.com/new-release-0-a-d-alpha-26-zhuangzi/)


¿Qué te parece esta nueva actualización? ¿La has probado ya? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

