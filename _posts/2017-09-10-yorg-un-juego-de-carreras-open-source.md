---
author: Serjor
category: Carreras
date: 2017-09-10 16:53:46
excerpt: "<p>No ser\xE1 micromachines, pero es aut\xE9ntico</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/592ec39b7f46885c43e42ca2926a95cd.webp
joomla_id: 458
joomla_url: yorg-un-juego-de-carreras-open-source
layout: post
tags:
- open-source
- yorg
title: YORG, un juego de carreras open source
---
No será micromachines, pero es auténtico

Hace poco a través del canal de Telegram un miembro de la comunidad de JEL nos pasaba un enlace a [YORG](http://www.ya2.it/yorg/) (acrónimo de Yorg's an Open Racing Game), un clon **open source** del famoso juego Micromachines.


Y es que precisamente esta diversión arcade es la que nos ofrece este título. Sin grandes premisas, correr en uno de sus circuitos que están emplazados en los sitios más disparatados y acelerar para ser el más rápido. No obstante, si por el motivo que sea nos somos los más rápidos siempre, podemos hacer uso de cualquiera de las armas que podemos ir recogiendo por la pista o de alguno de los power-ups.


En este momento están en la versión 0.7, y se nota que es un juego en desarrollo, ya que en mi caso en la segunda carrera que iba a disputar el juego se ha cerrado solo, pero es más que comprensible en un juego que aún no ha llegado a la versión 1.


De camino a esa versión 1 sabemos que en su roadmap tienen planeado en la versión 0.8 el modo campeonato, porque por ahora solamente se pueden correr carreras de una en una, y para la versión 0.9 tienen la intención de incluir un modo multiplayer, que esperamos que sea on-line para poder incluir este juego en nuestras partidas con la comunidad de los viernes.


Podéis descargarlo desde itch.io:


<div class="resp-iframe"><iframe height="167" src="https://itch.io/embed/133201" width="552"></iframe></div>


Y podéis encontrar su código fuente en [GitHub](https://github.com/cflavio/yorg), el cuál está liberado con la licencia GPL3, y que está siendo desarrollado con Panda3D, GIMP, Blender, python, y Bullet, que por lo que podéis ver todo herramientas de código libre, cosa que nos alegra bastante.


Os dejamos con un gameplay de nuestro compañero Leo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/6AgXGZyasss?rel=0" width="560"></iframe></div>


Y tú, ¿qué opinas de este YORG? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

