---
author: Pato
category: "Acci\xF3n"
date: 2017-10-04 16:52:27
excerpt: <p>Se trata de un sucesor espiritual del Road Rush</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1d05de4b50ede21b7617d543f5f98a74.webp
joomla_id: 475
joomla_url: road-redemption-sale-del-acceso-anticipado-y-llega-a-linux-hoy-dia-de-lanzamiento
layout: post
tags:
- accion
- carreras
- indie
- multijugador
- arcade
title: "'Road Redemption' sale del Acceso Anticipado y llega a Linux hoy d\xEDa de\
  \ lanzamiento"
---
Se trata de un sucesor espiritual del Road Rush

¡Sigue la racha de lanzamientos! Esta vez el que nos llega es 'Road Rush', que tras un largo periodo en acceso anticipado por fin llega y con soporte en Linux hoy día de lanzamiento.


Road Redemption realizó una exitosa [campaña en Kickstarter](https://www.kickstarter.com/projects/darkseasgames/road-redemption/description) en 2013 y [otra en Greenlight](http://store.steampowered.com/app/300380/?snr=1_7_7_151_150_1), con lo que consiguieron el empujón necesario para llevar el proyecto a cabo.


Road Redemption es un juego de carreras de acción en el que lideras tu pandilla de motociclistas en un viaje épico por todo el país en esta épica aventura de combate en la carretera. Campaña enorme, decenas de armas, pantalla completa de 4 jugadores y pantalla multijugador en línea.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/j-ZYwbJtQ9g" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 'Road Redemption' llega traducido al español, y está disponible en su página de Steam con un 10% de descuento por su lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/300380/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Qué te parece 'Road Rush? ¿Piensas echarte a la carretera a repartir mamporros sobre un hierro?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

