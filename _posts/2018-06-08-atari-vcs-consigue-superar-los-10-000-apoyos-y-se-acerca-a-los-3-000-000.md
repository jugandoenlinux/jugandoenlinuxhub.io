---
author: Pato
category: Hardware
date: 2018-06-08 12:23:50
excerpt: "<p>\xC9xito \"inesperado\" con tan solo 8 d\xEDas de campa\xF1a</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/01eec10693d9896b4d757174d0f20dd9.webp
joomla_id: 762
joomla_url: atari-vcs-consigue-superar-los-10-000-apoyos-y-se-acerca-a-los-3-000-000
layout: post
tags:
- atari
- atarivcs
title: Atari VCS consigue superar los 10.000 apoyos y se acerca a los 3.000.000$
---
Éxito "inesperado" con tan solo 8 días de campaña

Quién lo diría. El fenómeno Atari VCS parece que ha comenzado con buen pie. Hace poco más de una semana que [recogíamos la noticia](index.php/homepage/hardware/item/870-atari-vcs-abrira-su-campana-de-pre-reservas-el-proximo-miercoles-actualizado) de que comenzaba la campaña de la nueva consola de Atari con un objetivo "simbólico" de 100.000$ y con el objetivo "real" de ver el alcance y apoyo de los fans de la mítica marca de videojuegos.


Muchos (youtubers sobre todo) no veían, y siguen sin ver el atractivo de esta nueva consola para invertir los cerca de **200 dólares** que hacen falta para hacerse con una de estas máquinas en su configuración "Onix", sin incluir mandos ni frontal en madera pero lo cierto es que en el momento de escribir estas líneas más de 10.000 personas han apoyado con algo más de 2.700.000$ para que esta nueva plataforma nazca con buen pie. Y esto solo en 8 días y sin grandes campañas de publicidad. Puedes ver como va la campaña en el siguiente enlace:


<div class="resp-iframe"><iframe height="445px" src="https://www.indiegogo.com/project/atari-vcs-game-stream-connect-like-never-before-computers-pc/embedded/18828265" style="display: block; margin-left: auto; margin-right: auto;" width="222px"></iframe></div>


Cuando **el primer día colapsaron la plataforma Indiegogo** ellos solos, estaba claro que algo estaba pasando. La propia marca confiesa estar sorprendida del soporte que están recibiendo en el [último update de la campaña](https://www.indiegogo.com/projects/atari-vcs-game-stream-connect-like-never-before-computers-pc/x/18828265#/updates/all), y afirman tener planes ambiciosos aunque siguen sin desvelar nada sobre el sistema, nuevas características o estudios, partners y videojuegos que piensan lanzar en un futuro. Tan solo un vago "en próximos meses tendreis noticias". ¿Que es lo que ha llevado a Atari a conseguir semejante éxito?


En primer lugar, tenemos una "consola" que aunque es cierto que en un principio parece estar apoyada en la nostalgia de los juegos retro de la propia Atari, a diferencia de las "mini-retro" consolas de otras marcas que han preferido "encerrar en un emulador" un catálogo mas o menos nostálgico de máquinas del pasado en una bonita caja y venderla "tal cual", Atari **ha tenido la visión de lanzar un "nuevo" sistema apoyado en un ecosistema abierto y actual como es Linux** sobre arquitectura PC, y abrir su consola al futuro con la **posibilidad de lanzar nuevos juegos y desarrollos en ella**. La posibilidad de poder programar directamente sobre la propia consola además de albergar a los estudios que quieran lanzar sus desarrollos en su tienda no hacen más que hacer atractiva la Atari VCS. En este caso, parece que Atari tomó buena nota del concepto de Ouya y apoyada por el creciente valor añadido del mercado de videojuegos en Linux, han sabido enfocar sus esfuerzos para "allanar" el camino a los desarrolladores.


Por otra parte, el anuncio de las posibilidades de **visualizar los streamings de las más conocidas plataformas** es algo de lo que la propia Valve, su SteamOS y sus Steam Machines pueden aprender. Hoy día un aparato que se conecte a la TV y no tenga opción a acceder a retransmisiones ya sea de videojuegos o multimedia en general tiene poco sentido, y en Atari parece que tienen las ideas claras en este aspecto.


En tercer lugar, la opción de **añadir "extensiones"** al sistema de la consola. Al ser una versión modificada de Ubuntu, será posible instalar programas que puedan correr sobre Linux de forma nativa. La propia Atari afirma que **será posible instalar Steam en la Atari VCS** y esto ya de por si supone un puntal para una consola que quiere ser de verdad un sistema de videojuegos basada en arquitectura de PC. De golpe, todos los juegos que tengamos en nuestra biblioteca y tenga soporte para Linux estará disponible en esta futura consola. Otra cosa es que luego el procesador o la grafica dé de si para poder mover los juegos mas punteros, pero de entrada esto hace subir muchos enteros a un aparato que te permitirá disfrutar de Steam por 200$.


Además de esto, **no estamos obligados a comprar los periféricos** de la propia consola. Esto, aunque parezca trivial es otro punto a tener en cuenta. Por un lado ayuda a "abaratar" el coste de la consola y por otra parte da la opción de poder utilizar **cualquier periférico con soporte en Linux**. Tal cual.


Por último, estamos hablando de Atari. Es cierto que la compañía ya no es, ni tiene que ver con la que era hace décadas, pero el nombre Atari aún significa mucho para mucha gente que aún recuerda con cariño los videojuegos con los que disfrutó en la infancia. En Atari parece que lo saben y parece que quieren aprovecharlo para levantar su nuevo proyecto tras llevar muchos años a la deriva.


A poco que sepan llevar su campaña, y les den a los fans lo que les demandan (muchos afirman estar esperando nuevas noticias y nuevos objetivos en la campaña de financiación para realizar o ampliar su apoyo) pueden tener en las manos el levantar una nueva y exitosa plataforma de videojuegos.


Y sobre todo, es un sistema Linux. En este aspecto sobran las palabras.


¿Y tu que piensas sobre la Atari VCS y su campaña de financiación? ¿Piensas que terminará triunfando? ¿Será un buen apoyo para el videojuego en Linux?


Y sobre todo, **¿Quieres que sigamos publicando y hablando de la Atari VCS en Jugando en Linux?**


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/vcbUBYJoqHw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 

