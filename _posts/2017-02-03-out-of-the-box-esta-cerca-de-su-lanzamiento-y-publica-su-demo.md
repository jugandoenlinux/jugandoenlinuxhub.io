---
author: Pato
category: Aventuras
date: 2017-02-03 12:37:44
excerpt: "<p>La demo de los espa\xF1oles \"Nuclear Tales\" ya se puede disfrutar en\
  \ Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c75601cf4b798b9bb038a5b73c93d358.webp
joomla_id: 207
joomla_url: out-of-the-box-esta-cerca-de-su-lanzamiento-y-publica-su-demo
layout: post
tags:
- indie
- aventura
- casual
- steam
title: "'Out Of the Box' est\xE1 cerca de su lanzamiento y publica su demo"
---
La demo de los españoles "Nuclear Tales" ya se puede disfrutar en Linux

'Out Of the Box' [[web oficial](http://www.nucleartales.com/es/)] es de esos juegos de los que a poco que caigas en sus redes puedes perder horas sin que apenas te des cuenta.


Con un apartado gráfico "dibujado a mano" y muy buenas ideas el juego te lleva a situaciones cuanto menos inesperadas. Yo mismo lo he comprobado en mis propias carnes.


Eres Warren, un exconvicto que al salir de la calle necesita un trabajo legal que le permita subsistir durante un tiempo sin meterse en líos. Dada su reputación, acude a pedir ayuda a su antiguo jefe, que le da un trabajo como portero del club mas selecto de la ciudad propiedad suya, y donde tendrás que vértelas con la "crem de la crem" del paisaje nocturno. Ya sean drogadictos, borrachos, o gente buscando guerra tendrás que mantener la puerta a salvo y dejar entrar a la clientela para que el local sea rentable, y a su vez rechazar a los menores de edad, los indeseables y repartir algún mamporro que otro... si es que puedes.


Tendrás que ir tomando decisiones que afectarán a tu trabajo y tu posición, intentarán chantajearte, sobornarte... ¿quién no quiere un trabajo así?


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/QuPF5nc36-0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 En fin, si aceptas el trabajo el juego estará disponible en una fecha aún por anunciar, pero ya puedes ejercer de portero de 'Out of The Box' el local mas selecto de la ciudad en su demo, que además está en español en su web de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/576960/" width="646"></iframe></div>


 

