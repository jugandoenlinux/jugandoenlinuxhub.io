---
author: Pato
category: "Simulaci\xF3n"
date: 2017-11-09 12:15:56
excerpt: <p>El dato se mantiene estable y se ajusta a lo que reportan otros estudios</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3c484c65a2d021a349aae5bdf03a26b7.webp
joomla_id: 524
joomla_url: los-desarrolladores-de-xplane-11-reportan-que-un-1-4-de-sus-usuarios-vuela-en-gnu-linux
layout: post
tags:
- simulador
- simulacion
title: Los desarrolladores de XPlane 11 reportan que un 1,4% de sus usuarios vuela
  en GNU/Linux
---
El dato se mantiene estable y se ajusta a lo que reportan otros estudios

Nos hacemos eco de una noticia de [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=X-Plane-Linux-Usage-Statistics) en la que encontramos un dato revelador. Los desarrolladores de Xplane 11 del que [ya os hemos informado en otros artículos](index.php/component/search/?searchword=xplane&ordering=newest&searchphrase=all&limit=20&areas[0]=k2) han hecho públicos algunos datos de uso de este simulador de vuelo. 


En concreto, [en su blog de desarrollo](https://developer.x-plane.com/2017/11/usage-data-update-for-november-2017/) informan, entre otros datos que la base de usuarios que ejecutan Xplane 11 en Linux es del 1,4%, como puede verse en la gráfica que han hecho pública:


![Xplane11usage](https://developer.x-plane.com/wp-content/uploads/2017/11/Oct_2017_OS-768x474.png)


Contrasta desde luego con los datos de otros estudios de videojuegos que anteriormente han reportado que la base de usuarios en Linux está en torno al 1%, aunque también hay que reseñar que atendiendo a la misma estadística de Xplane del mes de febrero, el dato se ha mantenido estable:


![Xplaneusagefeb](https://developer.x-plane.com/wp-content/uploads/2017/02/X-Plane_11_Operating_System-768x449.png)


Curiosamente, donde si se ve una evolución es en la base de usuarios que utilizan Mac, con una subida del 3,1%. Hay que tener en cuenta que Xplane 11 pertenece a un segmento "minoritario" dentro del videojuego como es la simulación aérea "pura", y de la que tenemos la suerte de tener a este Xplane 11 como máximo exponente. Habrá que ver como evolucionan estos números cuando Xplane de el salto a la API Vulkan, en lo que están trabajando tal y como [anunciaron hace unos meses](index.php/homepage/generos/simulacion/item/534-los-desarrolladores-de-xplane-11-estan-trabajando-para-implementar-vulkan-en-el-simulador).


 [Xplane 11](index.php/homepage/generos/simulacion/item/392-xplane-11-sale-de-la-fase-beta-y-ya-despega-en-linux) *es el simulador detallado, real y moderno que has estado esperando.*  
*El nuevo X-PLANE 11, es un simulador completamente rediseñado, con una interfaz de usuario intuitiva que hará la configuración y edición de vuelo más fácil que nunca.*  
*Cabinas de mando en 3-D y modelos exteriores con un sensacional diseño en alta definición para todas las aeronaves disponibles que harán su experiencia más real.*  
*Un nuevo motor de efectos para iluminación, sonidos y explosiones.*  
*Aviónicas y sistemas de navegación realistas: todas las aeronaves están preparadas para volar IFR desde el inicio.*  
*Aeropuertos “con vida”, ocupados, donde se encontrará vehículos de remolque, camiones de combustible, que están listos para prestar servicio a todas las aeronaves del simulador, incluyendo la suya.*  
*Nuevas edificaciones, calles, avenidas que simulan mejor las ciudades europeas.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Ov0Sm9GDha4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Si te interesa la simulación aérea mas pura, tienes Xplane 11 disponible el su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/269950/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


¿Qué te parecen las estadísticas que han hecho públicas? ¿Piensas que Linux mejorará su base de usuarios?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

