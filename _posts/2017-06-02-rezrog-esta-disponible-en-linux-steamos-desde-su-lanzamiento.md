---
author: Pato
category: Estrategia
date: 2017-06-02 07:37:35
excerpt: "<p>Se trata de un juego de rol y acci\xF3n con \"fichas\" y est\xE9tica\
  \ de juego de mesa</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5ab529a5ebdeee2caf2312423d91adb3.webp
joomla_id: 352
joomla_url: rezrog-esta-disponible-en-linux-steamos-desde-su-lanzamiento
layout: post
tags:
- indie
- rol
- estrategia
- turnos
title: "'Rezrog' est\xE1 disponible en Linux/Steamos desde su lanzamiento"
---
Se trata de un juego de rol y acción con "fichas" y estética de juego de mesa

'Rezrog' recuerda a esas partidas de rol de tablero, fichas y mesa camilla. Gracias a los chicos de [linuxgamenews.com](http://linuxgamenews.com/post/161327241949/rezrog-releases-today-on-linux-mac-and-pc#.WTEFA3XyiHs) nos llegan noticias del lanzamiento de este singular juego en el que tendremos que reunir a un grupo de héroes y lanzarnos a derrotar a todas las fuerzas malignas que quieren sumir las tierras de Rezrog en el caos.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/9CNqVMw1N9o" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


#### **Sinopsis de Steam:**


*Las tierras de Rezrog están sumidas en el caos. Magos malvados y criaturas horripilantes se han adentrado en las oscuras profundidades de la Tierra, donde cosechan el poder de las potentes gemas que se encuentran allí. Tendrás que reunir a tu equipo de héroes y derrotar a todo lo que se interponga en tu camino en esta aventura en pos de restaurar la paz en Rezrog.*


#### **Características:**


*Un sistema de grupos único: tendrás a tu disposición a un grupo de valerosos guerreros, cada uno de ellos con su propio aspecto y sus propias virtudes. ¡Pero solo uno podrá irse de aventuras! ¡Y además, si un héroe cae presa del enemigo y lo capturan, tendrás que rescatarlo con otro miembro!*


*Combates tácticos por turnos: planifica meticulosamente tus acciones durante el combate, conserva tus recursos y aprovecha las debilidades del enemigo.*


*No hay dos mazmorras iguales: las mazmorras se crean procedimentalmente para lograr que cada jugador tenga una experiencia única. Son mazmorras interesantes, con un gran número de objetos y eventos que afectarán a la exploración.*


*Muerte permanente: si capturan a todo tu equipo, el viaje llegará a su fin.*


*Progreso persistente: deja un legado a las futuras generaciones de héroes, incluso tras tu muerte permanente, mediante la adquisición y la forja de gemas preciosas. Dichas gemas conservarán puntos de estadísticas que se podrán usar en una nueva partida.*


*Estética de juego de mesa: imita el estilo de un juego tradicional de papel y tablero.*


*De aventureros a héroes: comparte todo el equipo y las habilidades de tu equipo para amoldarlas a tu estilo de juego… ¡pero hazlo de forma equitativa! Si no, te meterás en un lío si capturan a tu héroe más fuerte.*


*Exploración de mazmorras: prepárate para eventos imprevistos, tales como trampas, encuentros letales o cualquier otro desafío que te encontrarás en las mazmorras.*


*Sistema de habilidades: descubre habilidades y usa el sistema de mejoras para irlas potenciando a medida que avanzas.*


*Combates contra jefes: habrá complejos jefes finales esperándote para poner a prueba tu habilidad de combate. ¡Un movimiento erróneo y podrías sufrir una dura derrota*!


Si te van los sesudos juegos de tablero, puedes encontrar 'Rezrog' en español con en su página de [GOG](https://www.gog.com/game/rezrog) o en Steam con un 10% de descuento por su lanzamiento hasta el día 7 de este mes:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/548370/" width="646"></iframe></div>


¿Qué te parece 'Rezrog'? ¿Te gustan los juegos de tablero?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

