---
author: leillo1975
category: Plataformas
date: 2018-12-03 09:32:47
excerpt: "<p>@supertux_team anuncia finalmente la versi\xF3n 0.6.0 definitiva </p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9f61d10d254b0dd13d20a8b04bc2ce8a.webp
joomla_id: 916
joomla_url: el-equipo-de-supertux-necesita-testers-para-su-version-0-6
layout: post
tags:
- beta
- open-source
- codigo-abierto
- supertux
- test
title: "El equipo de SuperTux necesita testers para su versi\xF3n 0.6 (ACTUALIZADO)"
---
@supertux_team anuncia finalmente la versión 0.6.0 definitiva 

**ACTUALIZACIÓN 23-12-18:** A principios de mes podiais ver en la noticia original de este artículo que los desarrolladores de este divertido juego de plataformas pedían ayuda para testear la Beta recién sacada. Hoy, trás tres semanas se complacen en anunciarnos la publicación de la versión definitiva. La noticia nos llegaba a través de Twitter:



> 
> ? ? ? SuperTux 0.6.0 available!!! Download here <https://t.co/TFj50LJkrE> More infos here: <https://t.co/ZBgfgdB4Vs> Let's party! ? ? ?
> 
> 
> — SuperTux Team (@supertux_team) [December 23, 2018](https://twitter.com/supertux_team/status/1076940658621992961?ref_src=twsrc%5Etfw)


  





Podeis comprobar la lista definitiva de cambios en el [siguiente link](https://www.supertux.org/news/2018/12/23/0.6.0). También podeis descargar el juego para nuestro sistema en **formato AppImage**, así como para otros sistemas en su [página de descargas](https://www.supertux.org/download.html). Ya estais tardando en haceros con esta nueva versión y ver los frutos de dos años de desarrollo de [SuperTux](https://www.supertux.org).




---


**NOTICIA ORIGINAL:** Hace algunos días otro gran proyecto con solera como es el de **SuperTuxKart** [pedía ayuda para probar sus opciones multijugador](index.php/homepage/generos/carreras/2-carreras/1022-los-desarrolladores-de-supertuxkart-buscan-betatesters-para-el-juego-en-red) (nosotros como sabreis corrimos raudos y veloces en su ayuda como podeis ver en este [video de Youtube](https://youtu.be/IHAr0kpU-NI)). Ayer mismo, otro veteranos en la escena de los juegos libres, y que también **tiene a Tux como protagonista** pedían publicamente ayuda para probar y traducir lo que será la próxima versión de SuperTux, tal y como podeis ver en el siguiente tweet (gracias a [@GnuXero26](https://twitter.com/GnuXero26) por el chivatazo):



> 
> SuperTux 0.6.0 Beta 1 released. Go to <https://t.co/8Dxv87JOU5> and help us test! Binary packages will become available in the next half hour or so. Translators: Please test your strings in-game!
> 
> 
> — SuperTux Team (@supertux_team) [2 de diciembre de 2018](https://twitter.com/supertux_team/status/1069244064640450567?ref_src=twsrc%5Etfw)







Para quien no conozca [SuperTux](https://www.supertux.org/), se trata de un juego de **plataformas 2D de desplazamiento lateral** claramente inspirado por el genial Super Mario Bros de la gran N. Su protagonista,  Tux, la mascota de Linux,  tendrá que abrirse paso a través de muchos niveles diferentes para alcanzar y rescatar a Penny, que fue capturada por el malvado Nolok. En esta próxima versión, tras dos años de desarrollo, las novedades  con respecto a la versión 0.5 son importantes y son las siguientes:



> 
> *-Renovación completa de nuestro motor de renderizado, el juego debería ser mucho más rápido de lo que era antes.*  
>  *-Ahora soportamos OpenGL 3.3 Core y OpenGL ES 2.0, lo que permite que SuperTux se ejecute en el Raspberry Pi, y potencialmente en el WebGL.*  
>  *-Se han actualizado algunos gráficos y se han añadido efectos:*  
> *.La campana de salvación fue modificada (¡Gracias a Raghavendra "raghukamath" Kamath!)*  
> *.Mejoras en los grandes gráficos y animaciones de Tux (Gracias a Alzter)*  
> *.Varios efectos y sombreadores (Gracias a Grumbel)*  
>  *-Soporte para idiomas de derecha a izquierda a través de fuentes vectoriales. Esto también corregirá algunos caracteres no ASCII, que a menudo causaban problemas antes en las traducciones.*  
>  *-Mapa del mundo forestal rediseñado con nuevos niveles y otros rediseñados (Gracias, RustyBox y Serano)*  
>  *-Muchos otros cambios y correcciones de errores bajo la cubierta*  
>  *-Binarios oficiales de Linux*
> 
> 
> 


Como veis vale la pena jugarlo, tanto si ya lo habeis hecho con anterioridad para disfrutar de las nuevas características; como si sois nuevos y quereis descubrir **uno de los mejores juegos OpenSource** que tenemos desde hace muchos años. Por supuesto vuestra ayuda sera bienvenida si también quereis colaborar con sus desarrolladores reportando fallos o traduciéndolo. Podeis descargarlo desde la página de su proyecto en <https://github.com/SuperTux/supertux/releases/tag/v0.6.0-beta.1>. Os dejamos con un video reciente del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/1I4U81eacE4" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 Seguramente la gran mayoría de vosotros ha jugado más de una vez a este referente de los juegos en Linux. ¿Qué opinión teneis de él? ¿Qué os parecen las novedades de esta próxima versión? Déjanos tus impresiones sobre SuperTux en los comentarios o charla con nosotros en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

