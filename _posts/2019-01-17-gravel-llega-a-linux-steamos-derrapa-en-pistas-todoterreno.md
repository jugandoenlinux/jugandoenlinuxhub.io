---
author: Pato
category: Carreras
date: 2019-01-17 18:45:46
excerpt: <p>El juego de Milestone @MilestoneItaly nos llega gracias al port de @virtualprog
  Virtual Programming</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/649c1b234f9e05214b0a8859fe37993f.webp
joomla_id: 953
joomla_url: gravel-llega-a-linux-steamos-derrapa-en-pistas-todoterreno
layout: post
tags:
- deportes
- carreras
- arcade
- velocidad
- todoterreno
title: 'Gravel llega a Linux/SteamOS: derrapa en pistas todoterreno'
---
El juego de Milestone @MilestoneItaly nos llega gracias al port de @virtualprog Virtual Programming

Ya os hemos hablado [anteriormente](index.php/homepage/generos/carreras/2-carreras/1045-virtual-programming-esta-portando-gravel-a-linux-y-hay-nuevos-rumores-de-a-hat-in-time) de que Gravel, el juego de carreras todoterreno de Milestone estaba siendo portado a Linux/SteamOS gracias a Virtual Programming. En este caso, estamos ante el segundo juego que el estudio porta de esta compañía dedicada a los juegos de velocidad.


*Gravel es la experiencia todoterreno definitiva. ¡El juego de carreras más extremo, que te permitirá disfrutar con increíbles acrobacias en los lugares más salvajes del planeta!*  
*Pura diversión, paisajes espectaculares y una competición sin cuartel donde cada carrera resulta una batalla memorable.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/ElC2Viri9LM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Características:


Recorre el mundo para descubrir una variedad de entornos extremos y salvajes que te ofrecerán una experiencia todoterreno completa.  
Hay cuatro disciplinas, donde puedes competir con los coches más potentes.  
  



* Cross Country: zonas enormes con diferentes trazados, perfectas para competir en pruebas de Checkpoint en los paisajes más evocadores
* Wild Rush: compite en pruebas de vueltas en las localizaciones más salvajes
* Speed Cross: carreras en los entornos más hermosos del mundo
* Stadium: ¡escenarios reales y ficticios, llenos de saltos y trazados espectaculares!


Si bien es cierto que las reseñas de los usuarios en Steam no son nada alagueñas, no podemos descartar que el juego acabe siendo divertido. Lo cierto es que no abundan mucho los juegos de este tipo en nuestro sistema favorito.


En cuanto a los requisitos del sistema son:


* Requiere un procesador y un sistema operativo de 64 bits
* **SO:** Ubuntu 16.04 o superior, SteamOS 2.0
* **Procesador:** Intel Core i5-2500K; AMD FX-6350 o equivalente
* **Memoria:** 4 GB de RAM
* **Gráficos:** Intel Core i5-2500K; AMD FX-6350 o equivalente
* **Almacenamiento:** 15 GB de espacio disponible
* **Notas adicionales:** AMD - mesa 18.1 o superior / Nvidia - nvidia 390.48 o superior / Intel Graphics no soportados


Tenéis toda la información en la página web oficial, en el [anuncio](https://store.steampowered.com/app/558260?snr=2_groupannouncements_detail__apphubheader) y en la [página del juego de Steam](https://store.steampowered.com/app/558260?snr=2_groupannouncements_detail__apphubheader) o en [Humble Bundle](https://www.humblebundle.com/store/gravel?partner=jugandoenlinux) (enlace afiliado) donde tiene un descuento ahora mismo del 60%.


¿Qué te parece el nuevo juego portado por Virtual Programming? Si te atreves a probarlo y derrapar por las pistas de Gravel no dejes de contarnos tu experiencia en nuestro canal de [Telegram](https://telegram.me/jugandoenlinux) o en los comentarios.

