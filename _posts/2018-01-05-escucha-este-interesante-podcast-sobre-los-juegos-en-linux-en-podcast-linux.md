---
author: leillo1975
category: Editorial
date: 2018-01-05 13:58:37
excerpt: <p>@DioxCorp habla sobre el pasado, presente y futuro del gaming en nuestro
  sistema.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/dfa7f6322712614e6e049fc346875c0a.webp
joomla_id: 594
joomla_url: escucha-este-interesante-podcast-sobre-los-juegos-en-linux-en-podcast-linux
layout: post
tags:
- linux
- podcast
- podcast-linux
- alvaro-nova
- dioxcorp
- gaming
title: Escucha este interesante Podcast sobre los juegos en Linux en Podcast Linux.
---
@DioxCorp habla sobre el pasado, presente y futuro del gaming en nuestro sistema.

Esta misma mañana, uno de los usuarios de nuestro grupo de Telegram (gracias **@Koxmoz**) nos sugirió a que escucharamos este episodio de [Podcast Linux](https://avpodcast.net/podcastlinux/). Presentado por Juan Febles, este podcast toca temas relaccionados con el mundo de Linux, Tecnología, Cultura Libre, Informática, etc. El audio en cuestión dura más de una hora y media, y en el se entrevista a **Alvaro Nova**, conocido como [@DioxCorp,](https://twitter.com/DioxCorp) youtuber, fotografo, podcaster y por supuesto gamer. En él desgrana genialmente temas relaccionados con la evolución de los juegos en Linux, desde sus inicios hasta la actualidad, tocando también temas tan interesantes como RaspberryPi, OLPC, Arduino...


Sin duda un audio que todos los que amamos los juegos en Linux debemos escuchar pues ofrece mucha y muy buena información por alguien que sabe de lo que habla y que ha vivido todas las épocas que ha pasado el gaming en nuestro sistema operativo. Os dejo el enlace con el podcast en cuestión:


[![PodcastLinux42](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/PodcastLinux/PodcastLinux42.webp)](https://avpodcast.net/podcastlinux/alvaronova/)


### [#42 Linux Connexion con Alvaro Nova](https://avpodcast.net/podcastlinux/alvaronova/)



¿Que os ha parecido el podcast? ¿Llevais mucho tiempo jugando en Linux y os suenan mucho lo aquí comentado? Espero que os haya gustado tanto como a nosotros. Dejanos tus impresiones en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).




