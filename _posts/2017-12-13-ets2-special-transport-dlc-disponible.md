---
author: leillo1975
category: "Simulaci\xF3n"
date: 2017-12-13 15:07:06
excerpt: <p>SCS Software acaba de lanzarla en Steam</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e2038313f12263de225294bb4b49bfba.webp
joomla_id: 574
joomla_url: ets2-special-transport-dlc-disponible
layout: post
tags:
- dlc
- scs-software
- transporte-especial
- special-transport
title: ETS2 Special Transport DLC Disponible
---
SCS Software acaba de lanzarla en Steam

Vuelvo a decirlo, lo de esta compañía es extraordinario. Hace poco más de un mes podíamos ver la nueva expansión de [Nuevo México](index.php/homepage/analisis/item/656-analisis-american-truck-simulator-dlc-new-mexico) para American Truck Simulator, y la semana pasada recibiamos la espectacular DLC "[Italia](index.php/homepage/generos/simulacion/item/687-disponible-italia-la-nueva-dlc-para-euro-truck-simulator-2)", a la cual aun le debemos un completo análisis que vereis en los próximos dias. Ahora vuelven a la carga con otra expansión que nos permitirá conducir con transportes especiales por las carreteras de Europa. Podeis ver el anuncio que han realizado en twitter para avisarnos de su disponibilidad:



> 
> Special Transport DLC for Euro Truck Simulator 2 is now out on Steam! <https://t.co/LYJhDh3urX> [pic.twitter.com/KiG6IKLvFB](https://t.co/KiG6IKLvFB)
> 
> 
> — SCS Software (@SCSsoftware) [December 13, 2017](https://twitter.com/SCSsoftware/status/940991136751112197?ref_src=twsrc%5Etfw)



 


En esta expansión SCS Software han ido un paso más allá de lo visto en [Heavy Cargo Pack](index.php/homepage/analisis/item/492-analisis-american-truck-simulator-dlc-heavy-cargo-pack) y ha aumentado la dificultad y el tamaño de las cargas. Además vamos a contar con dos vehículos que nos van a acompañar por las vias por las que circulemos, uno en la parte trasera y otro delante nuestro. Podremos encontrar las siguientes características:


* 11 nuevas cargas especiales (hasta 70 toneladas, 6 metros de ancho, 5 de alto y más de 20 metros de largo)
* 4 nuevos trailers
* 17 rutas únicas con trabajos de transportes especiales (12 en mapa base, 2 en Scandinavia, 2 en Going East!, 1 en Vive la France !). La lista crecerá con el tiempo
* Las combinaciones permitirán hasta 80 trabajos distintos
* Los vehiculos escolta tendrán su propia inteligencia artificial despejando el camino, parando el tráfico, dando avisos, empujando a los vehiculos contrarios hacia los lados, etc.
* Introducciones especiales y cinemáticas relativas a las cargas, misiones o lugares a través de las rutas.
* Nuevos Modelos, personajes y animaciones de gente alrededor de la escena.
* Peatones, periodistas o compañeros de trabajo a través de las rutas, sin importar si es día,  noche o hace mal tiempo.
* Posibilidad de fallar la misión si no cumples unos estrictos reglamentos.
* Nuevo sistema de checkpoints para guardar tu progreso en un transporte especial de forma segura.
* Nuevas animaciones en las balizas de alerta.
* Nuevos Logros.
* Alta Experiencia y rentabilidad por la el completado satisfactorio de este tipo de trabajos.


Os dejo con un trailer (nunca mejor dicho)  de esta expansión:


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/-35ivRwsLZs" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


También podeis vernos probando esta mágnífica expansión en un viaje de Bruselas a Amsterdam:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/3mGYjc67buk" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Como veis se trata de una experiencia totalmente única que encantará a todos los camioneros virtuales ansiosos de probar nuevos retos. Podeis comprar esta DLC en Steam:


 <div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/558245/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 ¿Os atreveis a maniobrar con este tipo de transportes? ¿Que os parece esta nueva expansión de ETS2? Deja tus impresiones en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

