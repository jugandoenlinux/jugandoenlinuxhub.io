---
author: Pato
category: Editorial
date: 2017-04-07 16:05:57
excerpt: "<p>\xBFTe atreves a apostar con nosotros cual puede ser?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/391d45802a606be64095bd7b66c67316.webp
joomla_id: 283
joomla_url: el-radar-de-feral-vuelve-a-detectar-un-nuevo-juego-no-identificado
layout: post
tags:
- proximamente
- feral-interactive
title: El radar de Feral vuelve a detectar un nuevo juego no identificado
---
¿Te atreves a apostar con nosotros cual puede ser?

Hace ya algún tiempo que no teníamos noticias de los chicos de Feral, y hoy por fin nos hemos encontrado con una nueva "sorpresa":



> 
> Eerie pings are coming from the Feral Radar… a new game for Mac and Linux is on its way, but what could it be?<https://t.co/znuEbShcXs>
> 
> 
> — Feral Interactive (@feralgames) [7 de abril de 2017](https://twitter.com/feralgames/status/850334095502704640)



 Exáctamente, lo que se puede ver en el radar de la web de Feral es esto:


 


![Feralteaser3](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Feral/Feralteaser3.webp)


En el objeto pone "Made to Wade" y dentro se puede ver lo que parece unas letras o palabra en un idioma "sospechoso"... 


Puedes verlo en su página web: <http://www.feralinteractive.com/es/upcoming/>


¿qué puede ser?... ¿Te atreves a aventurarlo?... ¿tienes alguna teoría?


Cuéntanoslo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

