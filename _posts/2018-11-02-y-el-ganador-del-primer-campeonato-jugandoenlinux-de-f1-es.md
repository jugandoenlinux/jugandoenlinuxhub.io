---
author: leillo1975
category: Carreras
date: 2018-11-02 14:03:56
excerpt: "<p>Tach\xE1n, tach\xE1n....</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d95ac1e7d6d78552526cbaae66d68531.webp
joomla_id: 888
joomla_url: y-el-ganador-del-primer-campeonato-jugandoenlinux-de-f1-es
layout: post
tags:
- campeonato
- f1
- jugandoenlinuxcom
title: '...y el ganador del primer campeonato JugandoEnLinux de F1 es....'
---
Tachán, tachán....

Como muchos de vosotros sabreis, a lo largo de este año llevamos disputando paralelamente un [campeonato alternativo](index.php/homepage/generos/carreras/2-carreras/815-ayudanos-a-elegir-la-version-del-juego-de-f1-para-el-torneo-de-jugandoenlinux-com) al de F1, donde miembros de nuestra comunidad nos enfrentamos poco después de cada Gran Premio en el mismo circuito y con las mismas condiciones meteorológicas que en la vida real.


En esta primera edición hemos completado ya un total de 19 Grandes Premios, donde con más o menos afluencia lo hemos pasado en grande a lo largo y ancho del mundo (virtual) en las diferentes pruebas. Hemos corrido, derrapado, chocado, nos hemos comido muros, destrozamos cientos de alerones, e incluso hemos reñido; pero en definitiva todo esto es lo que le da "chicha" al mundial, un poquito de competencia y piques sanos. También hay que decir que ante todo lo hemos pasado bien, y es lo primero que tenemos que valorar. También esperamos que el año que viene repitamos experiencia y os apunteis muchos más.


Pero vamos  a lo que vamos.... **El ganador del primer Campeonato Mundial JeL de F1 es: @Y0ooo**, también conocido como "**juegaenlinux**", que ha hecho una temporada excelente y a pesar de unirse con el campeonato ya empezado, ha demostrado sus magníficas dotes de conducción *"...y sin ayudas"*. Desde JugandoEnLinux.com nos gustaría felicitarle y emplazarlo para la próxima temporada, al igual que todos vosotros.


También nos gustaría recordaros que esta temporada aun no ha terminado y aun nos quedan dos carreras, Interlagos y Abu Dabi, en las que se decidirá el segundo y tercer puesto en la clasificación, por lo que no debeis perdéroslas. La clasificación a día de hoy de nuestro Campeonato está de esta manera (pinchad para ampliar):


![ClasifGeneral19](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/F1%202015/ClasifGeneral19.webp)


También os recordamos que podeis consultar todas clasificaciones y videos de las carreras disputadas en [este tema de nuestro foro](index.php/foro/campeonato-f1-2018/104-mundial-f1-jugandoenlinux-com?start=0). Os dejamos con el video de la última carrera disputada ayer mismo en México (Autódromo Hermanos Rodríguez):


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/A3NN36hcGKI" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 

